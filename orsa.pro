
TEMPLATE = app
CONFIG += thread warn_on 
DEPENDPATH += src/liborsa src/libxorsa src/orsa src/liborsa/misc
INCLUDEPATH += src src/liborsa src/liborsa/misc src/libxorsa src/icons src/orsa

## DEFINES += BYTEORDER="1234" ## 1234 = LIL_ENDIAN, 4321 = BIGENDIAN
DEFINES += HAVE_GL
DEFINES += RELEASE_DATE="\"Jan 11 2005\""


win32 {
        UI_DIR = _win32
        MOC_DIR = _win32
        OBJECTS_DIR = _win32

        TARGET   = winorsa
        DEFINES += WIN32 HAVE_LIBZ
        HEADERS += src/support.h
        SOURCES += src/support.cc
        DEFINES += BYTEORDER="1234" ## 1234 = LIL_ENDIAN, 4321 = BIGENDIAN
        QMAKE_CXXFLAGS += -P
        INCLUDEPATH += $$(QTDIR)/src/3rdparty/zlib
}

unix {
        CONFIG += opengl

        UI_DIR = _tmp
        MOC_DIR = _tmp
        OBJECTS_DIR = _tmp

	TARGET   = xorsa
        DEFINES += HAVE_LIBZ

	!macx {
	        DEFINES += BYTEORDER="1234" ## 1234 = LIL_ENDIAN, 4321 = BIGENDIAN
	}
}

unix:macx {
        TARGET   = macorsa
        DEFINES += BYTEORDER="4321" ## 1234 = LIL_ENDIAN, 4321 = BIGENDIAN
        LIBS    += -lz
}

gsl {
	DEFINES += HAVE_GSL

	HEADERS += src/liborsa/orsa_orbit_gsl.h \
                   src/libxorsa/xorsa_objects_generator_cartesian.h \
                   src/libxorsa/xorsa_objects_generator_keplerian.h \
                   src/libxorsa/xorsa_objects_generator_keplerian_covariance_file.h \
                   src/libxorsa/xorsa_close_approaches_dialog.h

	SOURCES += src/liborsa/orsa_orbit_gsl.cc \
                   src/libxorsa/xorsa_objects_generator_cartesian.cc \
                   src/libxorsa/xorsa_objects_generator_keplerian.cc \
                   src/libxorsa/xorsa_objects_generator_keplerian_covariance_file.cc \
                   src/libxorsa/xorsa_close_approaches_dialog.cc

        LIBS    += -lgsl -lgslcblas
}

mpi {
	DEFINES += HAVE_MPI

        QMAKE_CXX  = mpiCC
        QMAKE_LINK = mpiCC

	SOURCES += src/liborsa/orsa_interaction_MPI.cc
}

mpi::macx {

########### LAM/MPI under MACOSX ##############
        QMAKE_LINK = mpic++ 
        QMAKE_CXX  = mpic++
}

# Input
HEADERS += src/liborsa/orsa_analysis.h \
           src/liborsa/orsa_body.h \
           src/liborsa/orsa_common.h \
           src/liborsa/orsa_config.h \
           src/liborsa/orsa_coord.h \
           src/liborsa/orsa_error.h \
           src/liborsa/orsa_file.h \
           src/liborsa/orsa_file_jpl.h \
           src/liborsa/orsa_frame.h \
           src/liborsa/orsa_integrator.h \
           src/liborsa/orsa_interaction.h \
           src/liborsa/orsa_orbit.h \
           src/liborsa/orsa_secure_math.h \
           src/liborsa/orsa_units.h \
           src/liborsa/orsa_universe.h \
           src/liborsa/orsa_version.h \
           src/libxorsa/gl2ps.h \
           src/libxorsa/xorsa_about.h \
           src/libxorsa/xorsa_all_objects_info.h \
           src/libxorsa/xorsa_all_objects_listview.h \
           src/libxorsa/xorsa_all_objects_popup.h \
           src/libxorsa/xorsa_config.h \
           src/libxorsa/xorsa_date.h \
           src/libxorsa/xorsa_extended_types.h \
           src/libxorsa/xorsa_download.h \
           src/libxorsa/xorsa_export_integration.h \
           src/libxorsa/xorsa_import_astorb_objects.h \
           src/libxorsa/xorsa_import_JPL_objects.h \
           src/libxorsa/xorsa_import_TLE_objects.h \
           src/libxorsa/xorsa_integrations_info.h \
           src/libxorsa/xorsa_integrator_combo.h \
           src/libxorsa/xorsa_interaction_combo.h \
           src/libxorsa/xorsa_location_selector.h \
           src/libxorsa/xorsa_new_integration_dialog.h \
           src/libxorsa/xorsa_new_object_cartesian_dialog.h \
           src/libxorsa/xorsa_new_object_keplerian_dialog.h \
           src/libxorsa/xorsa_new_universe_dialog.h \
           src/libxorsa/xorsa_object_selector.h \
           src/libxorsa/xorsa_objects_combo.h \
           src/libxorsa/xorsa_opengl.h \
           src/libxorsa/xorsa_plot_area.h \
           src/libxorsa/xorsa_plot_tool_II.h \
           src/libxorsa/xorsa_plottype_combo.h \
           src/libxorsa/xorsa_units_combo.h \
           src/libxorsa/xorsa_units_converter.h \
	   src/libxorsa/xorsa_wrapper.h \
           src/orsa/xorsa.h \
           src/liborsa/misc/jpl_int.h \
           src/liborsa/misc/jpleph.h \
           src/liborsa/misc/sdncal.h

SOURCES += src/liborsa/orsa_analysis.cc \
           src/liborsa/orsa_body.cc \
           src/liborsa/orsa_config.cc \
           src/liborsa/orsa_coord.cc \
           src/liborsa/orsa_error.cc \
           src/liborsa/orsa_file.cc \
           src/liborsa/orsa_file_jpl.cc \
           src/liborsa/orsa_file_mercury.cc \
           src/liborsa/orsa_frame.cc \
           src/liborsa/orsa_integrator.cc \
#           src/liborsa/orsa_integrator_bulirsch_stoer.cc \
           src/liborsa/orsa_integrator_ra15.cc \
           src/liborsa/orsa_integrator_runge_kutta.cc \
           src/liborsa/orsa_integrator_stoer.cc \
           src/liborsa/orsa_interaction.cc \
           src/liborsa/orsa_interaction_tree.cc \
           src/liborsa/orsa_orbit.cc \
           src/liborsa/orsa_secure_math.cc \
           src/liborsa/orsa_units.cc \
           src/liborsa/orsa_universe.cc \
           src/libxorsa/gl2ps.c \
           src/libxorsa/xorsa_about.cc \
           src/libxorsa/xorsa_all_objects_info.cc \
           src/libxorsa/xorsa_all_objects_listview.cc \
           src/libxorsa/xorsa_all_objects_popup.cc \
           src/libxorsa/xorsa_config.cc \
           src/libxorsa/xorsa_date.cc \
           src/libxorsa/xorsa_extended_types.cc \
           src/libxorsa/xorsa_download.cc \
           src/libxorsa/xorsa_export_integration.cc \
           src/libxorsa/xorsa_import_astorb_objects.cc \
           src/libxorsa/xorsa_import_JPL_objects.cc \
           src/libxorsa/xorsa_import_TLE_objects.cc \
           src/libxorsa/xorsa_integrations_info.cc \
           src/libxorsa/xorsa_integrator_combo.cc \
           src/libxorsa/xorsa_interaction_combo.cc \
           src/libxorsa/xorsa_location_selector.cc \
           src/libxorsa/xorsa_new_integration_dialog.cc \
           src/libxorsa/xorsa_new_object_cartesian_dialog.cc \
           src/libxorsa/xorsa_new_object_keplerian_dialog.cc \
           src/libxorsa/xorsa_new_universe_dialog.cc \
           src/libxorsa/xorsa_object_selector.cc \
           src/libxorsa/xorsa_objects_combo.cc \
           src/libxorsa/xorsa_opengl.cc \
           src/libxorsa/xorsa_plot_area.cc \
           src/libxorsa/xorsa_plot_tool_II.cc \
           src/libxorsa/xorsa_plottype_combo.cc \
           src/libxorsa/xorsa_units_combo.cc \
           src/libxorsa/xorsa_units_converter.cc \
           src/libxorsa/xorsa_wrapper.cc \
           src/orsa/xorsa.cc \
           src/orsa/xorsa_main.cc \
           src/liborsa/misc/dow.c \
           src/liborsa/misc/gregor.c \
           src/liborsa/misc/jpleph.c \
           src/liborsa/misc/julian.c
