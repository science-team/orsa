#! /bin/sh

# Run this to generate all the auto-generated files needed by the GNU
# configure program

set -x

aclocal -I macros
libtoolize --automake --force --copy
autoheader
automake --add-missing --copy
autoconf
