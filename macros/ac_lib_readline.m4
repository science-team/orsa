dnl @synopsis AC_LIB_READLINE
dnl
dnl Searches for a readline compatible library.  If found, defines
dnl `HAVE_LIBREADLINE'.  If the found library has the `add_history'
dnl function, sets also `HAVE_READLINE_HISTORY'.  Also checks for the
dnl locations of the necessary include files and sets `HAVE_READLINE_H'
dnl or `HAVE_READLINE_READLINE_H' and `HAVE_READLINE_HISTORY_H' or
dnl 'HAVE_HISTORY_H' if the corresponding include files exists.
dnl
dnl The libraries that may be readline compatible are `libedit',
dnl `libeditline' and `libreadline'.  Sometimes we need to link a termcap
dnl library for readline to work, this macro tests these cases too by
dnl trying to link with `libtermcap', `libcurses' or `libncurses' before
dnl giving up.
dnl
dnl Here is an example of how to use the information provided by this
dnl macro to perform the necessary includes or declarations in a C file:
dnl
dnl   #include <config.h>
dnl
dnl   #ifdef HAVE_LIBREADLINE
dnl   #if defined(HAVE_READLINE_READLINE_H)
dnl   #include <readline/readline.h>
dnl   #elif defined(HAVE_READLINE_H)
dnl   #include <readline.h>
dnl   #else /* !defined(HAVE_READLINE_H) */
dnl   extern char *readline ();
dnl   #endif /* !defined(HAVE_READLINE_H) */
dnl   char *cmdline = NULL;
dnl   #else /* !defined(HAVE_READLINE_READLINE_H) */
dnl     /* no readline */
dnl   #endif /* HAVE_LIBREADLINE */
dnl
dnl   #ifdef HAVE_READLINE_HISTORY
dnl   #if defined(HAVE_READLINE_HISTORY_H)
dnl   #include <readline/history.h>
dnl   #elif defined(HAVE_HISTORY_H)
dnl   #include <history.h>
dnl   #else /* !defined(HAVE_HISTORY_H) */
dnl   extern void add_history ();
dnl   extern int write_history ();
dnl   extern int read_history ();
dnl   #endif /* defined(HAVE_READLINE_HISTORY_H) */
dnl     /* no history */
dnl   #endif /* HAVE_READLINE_HISTORY */
dnl
dnl
dnl @version $Id: ac_lib_readline.m4,v 1.4 2003/02/05 14:26:50 tricaric Exp $
dnl @author Ville Laurikari <vl@iki.fi>
dnl
AC_DEFUN([AC_LIB_READLINE], [
  AC_SUBST(READLINE_LIBS)
  AC_CACHE_CHECK([for a readline compatible library],
                 ac_cv_lib_readline, [
    ORIG_LIBS=$LIBS
    for readline_lib in readline edit editline; do
      for termcap_lib in "" ncurses curses termcap; do
        if test -z "$termcap_lib"; then
          TRY_LIB="-l$readline_lib"
        else
          TRY_LIB="-l$readline_lib -l$termcap_lib"
        fi
	LIBS="$ORIG_LIBS $TRY_LIB"
        AC_TRY_LINK_FUNC(readline, ac_cv_lib_readline="$TRY_LIB")
        if test -n "$ac_cv_lib_readline"; then
          break
        fi
      done
      if test -n "$ac_cv_lib_readline"; then
   	READLINE_LIBS="$TRY_LIB"
	break
      fi
    done
    LIBS=$ORIG_LIBS
    if test -z "$ac_cv_lib_readline"; then
      ac_cv_lib_readline="no"
    fi
  ])

  if test "$ac_cv_lib_readline" != "no"; then
    AC_DEFINE(HAVE_LIBREADLINE, 1,
              [Define if you have a readline compatible library])
    AC_CHECK_HEADERS(readline.h readline/readline.h)
    AC_CACHE_CHECK([whether readline supports history],
                   ac_cv_lib_readline_history, [
#ac_cv_lib_readline_history="no"
    ORIG_LIBS=$LIBS
#LIBS="$ORIG_LIBS $READLINE_LIBS"
    for history_lib in history ""; do
      TRY_LIB="-l$history_lib"
      LIBS="$ORIG_LIBS $READLINE_LIBS $TRY_LIB"
      AC_TRY_LINK_FUNC(add_history, ac_cv_lib_readline_history="yes")
      if test -n "$ac_cv_lib_readline_history"; then
        break
      fi
    done
    ])
    if test "$ac_cv_lib_readline_history" = "yes"; then
      AC_DEFINE(HAVE_READLINE_HISTORY, 1,
                [Define if your readline library has \`add_history'])
      AC_CHECK_HEADERS(history.h readline/history.h)
      READLINE_LIBS="$READLINE_LIBS $TRY_LIB"
    fi
    LIBS=$ORIG_LIBS
  fi
])
