dnl @synopsis AC_HAVE_FFTW
dnl 
AC_DEFUN([AC_HAVE_FFTW],
    AC_SUBST(FFTW_LIBS)
    [AC_CACHE_CHECK(
        [whether the FFTW library is installed],
        ac_cv_have_fftw,
    [AC_TRY_COMPILE(
    [
    #include <fftw.h>
    ],
    [fftw_plan p; fftw_complex z;],
    ac_cv_have_fftw=yes,
    ac_cv_have_fftw=no)])
    if test "$ac_cv_have_fftw" != "no"; then
        ac_save_LIBS="$LIBS"       
        LIBS="-lfftw $ac_save_LIBS"
        AC_TRY_LINK(
           [
           #include <fftw.h>
           ],
           [fftw_plan p; fftw_complex z;],
           [LIBS="$ac_save_LIBS"
            AC_DEFINE(HAVE_FFTW,1,[Define if the FFTW library is installed])
            FFTW_LIBS="-lfftw"
            ac_cv_have_fftw=yes],
           [LIBS="-ldfftw $ac_save_LIBS"
            AC_TRY_LINK(
               [
               #include <fftw.h>
               ],
               [fftw_plan p; fftw_complex z;],
               [LIBS="$ac_save_LIBS"
                AC_DEFINE(HAVE_FFTW,1,[Define if the FFTW library is installed])
                FFTW_LIBS="-ldfftw"
                ac_cv_have_fftw=yes],
               [LIBS="$ac_save_LIBS"
                ac_cv_have_fftw=no])
               ]
         )
    fi]
)
