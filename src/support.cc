#include "support.h"
#include <windows.h>
#include <cmath>

using namespace std;

#ifndef __GNUC__
double rint(double x)
{
	int xp = int(x + 0.5);
	int xn = int(x + ((int)x-1) + 0.5) + ((int)x-1);
	return x >= 0.0 ? xp : xn;
}

double copysign(double x, double y)
{
    double r;
    r = fabs(x);
    if (y < 0.0) r *= -1.0;
    return r;
}

double cbrt(double x)
{
	return pow(x, 1.0/3.0);
}
#endif

void sleep(int secs)
{
	Sleep(secs * 1000);
}

