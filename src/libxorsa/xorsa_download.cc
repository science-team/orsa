/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_download.h"

#include <orsa_config.h>
#include <orsa_file.h>

#include <qlayout.h> 
#include <qlabel.h>
#include <qprogressbar.h> 
#include <qlineedit.h> 
#include <qcheckbox.h> 
#include <qtabwidget.h> 
#include <qnetwork.h>
#include <qurloperator.h> 
#include <qpushbutton.h> 
#include <qnetworkprotocol.h> 
#include <qurlinfo.h> 
#include <qfileinfo.h> 
#include <qftp.h>
#include <qhttp.h>
#include <qdir.h>

#include <iostream>

using namespace std;
using namespace orsa;

XOrsaDownloadItemType Type(const ConfigEnum e) {
  
  XOrsaDownloadItemType type = ASTEROID;
  
  switch (e) {
  case JPL_EPHEM_FILE:
    type = PLANET;
    break;
  case MPC_COMET:
  case JPL_DASTCOM_COMET:
    type = COMET;
    break;
  case JPL_DASTCOM_NUM:
  case JPL_DASTCOM_UNNUM:
  case LOWELL_ASTORB:
  case MPC_MPCORB:	
  case MPC_NEA:
  case MPC_DAILY:
  case MPC_DISTANT:
  case MPC_PHA:
  case MPC_UNUSUALS:
  case ASTDYS_ALLNUM_CAT:
  case ASTDYS_ALLNUM_CTC:
  case ASTDYS_ALLNUM_CTM: 
  case ASTDYS_UFITOBS_CAT:
  case ASTDYS_UFITOBS_CTC:
  case ASTDYS_UFITOBS_CTM:
  case NEODYS_CAT:
  case NEODYS_CTC:
    type = ASTEROID;
    break;
  case TLE_NASA:
  case TLE_GEO:
  case TLE_GPS:
  case TLE_ISS:
  case TLE_KEPELE:
  case TLE_VISUAL:
  case TLE_WEATHER:
    type = ARTIFICIAL_SATELLITE;
    break;
  case TEXTURE_SUN:
  case TEXTURE_MERCURY:
  case TEXTURE_VENUS:
  case TEXTURE_EARTH:
  case TEXTURE_MOON:
  case TEXTURE_MARS:
  case TEXTURE_JUPITER:
  case TEXTURE_SATURN:
  case TEXTURE_URANUS:
  case TEXTURE_NEPTUNE:	
  case TEXTURE_PLUTO:
    type = TEXTURE;
    break;
  case OBSCODE:
    type = OTHER;
    break;
  case NO_CONFIG_ENUM:
    break;
  }
  
  return type;
}


XOrsaDownloadTabPage::XOrsaDownloadTabPage(QWidget *parent) : QWidget(parent) {
  QVBoxLayout *vlay = new QVBoxLayout(this,3);
  grid = new QGridLayout(vlay,1,4,3);
  vlay->addStretch();
}

XOrsaDownloadEntry::XOrsaDownloadEntry(const XOrsaDownloadItem &i, QWidget *w) : QObject(w), XOrsaDownloadItem(i) {
  http = 0;
  ftp = 0;
  la  = new QLabel(Label(i.config_enum).c_str(),w);
  le  = new QLineEdit(i.URL.protocol()+"://"+i.URL.host()+i.URL.path(),w);
  bar = new QProgressBar(w);
  bar->setMaximumWidth(120);
  pb  = new QPushButton("download",w);
  connect(pb,SIGNAL(clicked()),this,SLOT(pb_clicked()));
}

void XOrsaDownloadEntry::setProgress(int done, int total) {
  // cerr << "setProgress called with done=" << done << " and total=" << total << endl;
  if (total < 0) {
    if (size > 0) bar->setProgress(done,size);
  } else {
    bar->setProgress(done,total);
  }
}

void XOrsaDownloadEntry::pb_clicked() {
  
  pb->setEnabled(false);
  le->setReadOnly(true);
  
  // more checks?
  download();
}

void XOrsaDownloadEntry::post_download(bool) {
  
  // this slot is called when a download is finished and the done signal is emitted
  
  setProgress(100,100);
  
  file->close();
  
  QDir saved_wd = QDir::current();
  QDir wd = QDir(OrsaPaths::work_path());
  QDir::setCurrent(OrsaPaths::work_path());
  
  /* updated, because now file->name() returns all the path
     QString fn_tmp = file->name();
     QString fn     = file->name(); fn.remove("_tmp");
  */
  
  QString fn_tmp = QFileInfo(file->name()).fileName();
  QString fn     = fn_tmp; fn.remove("_tmp");
  
  if (fn != fn_tmp) {
    QFile::remove(fn);
    wd.rename(fn_tmp, fn); // rename could have failed if destination file existed
  }
  
  // special cases
  // if ((config_enum==MPC_MPCORB) && (fn.endsWith(".ZIP"))) {
  if ((config_enum==MPC_MPCORB) && (fn=="MPCORB.ZIP")) {
#ifndef _WIN32
    system("unzip MPCORB.ZIP");  
    system("mv data/ftp/pub/MPCORB/.incoming/MPCORB.DAT .");
    system("gzip -f MPCORB.DAT");
    system("rmdir -p data/ftp/pub/MPCORB/.incoming/");
    system("rm -f MPCORB.ZIP");
    fn = "MPCORB.DAT.gz";
#else
    // FIXME: needs to properly call the unzip library
    // For windows, this file will be kept uncompressed (for now)
    fn = "MPCORB.DAT";
#endif
  }
  
  QString dest = QString(OrsaPaths::work_path()) + fn;
  
  /* 
     Config conf;
     //
     OrsaConfigFile ocf(&conf);
     ocf.Read();
     //
     conf.paths[config_enum]->SetValue(dest.latin1());
     //
     ocf.Write();
     ocf.Close();
  */
  
  config->paths[config_enum]->SetValue(dest.latin1());
  config->write_to_file();
  
  // get back
  QDir::setCurrent(saved_wd.absPath());
  
  delete file;
  if (ftp != 0) {
    delete ftp;
    ftp = 0;
  }
  if (http != 0) {
    delete http;
    http = 0;
  }
  
}

void XOrsaDownloadEntry::download() {
  QUrl proto_url(le->text());
  ftp = 0;
  http = 0;
  if (proto_url.protocol() == "ftp") {

    ftp = new QFtp;
    file = new QFile(OrsaPaths::work_path() + proto_url.fileName() + "_tmp");
    file->open(IO_WriteOnly);
    //
    ftp->connectToHost(proto_url.host());
    ftp->login("anonymous","orsa_user@orsa.sf.net");
    ftp->cd(proto_url.dirPath());
    //
    ftp->get(proto_url.fileName(),file);
    // WARNING: don't close the file HERE!!
    //
    ftp->close();
    
    // progress
    // connect(ftp,SIGNAL(dataTransferProgress(int,int)),page->entries[k]->bar,SLOT(setProgress(int,int)));
    connect(ftp,SIGNAL(dataTransferProgress(int,int)),this,SLOT(setProgress(int,int)));
    
    connect(ftp,SIGNAL(done(bool)),this,SLOT(post_download(bool)));
    
  } else if (proto_url.protocol() == "http") {
    
    http = new QHttp;
    file = new QFile(OrsaPaths::work_path() + proto_url.fileName() + "_tmp");
    file->open(IO_WriteOnly);
    //
    http->setHost(proto_url.host());
    http->get(proto_url.path(true),file);
    
    // progress
    // connect(ftp,SIGNAL(dataTransferProgress(int,int)),page->entries[k]->bar,SLOT(setProgress(int,int)));
    connect(http,SIGNAL(dataReadProgress(int,int)),this,SLOT(setProgress(int,int)));
    
    connect(http,SIGNAL(done(bool)),this,SLOT(post_download(bool)));
    
  } else {
    cerr << "only ftp and http protocols supported for the moment..." << endl;
  }
}

void XOrsaDownloadTabPage::InsertItem(const XOrsaDownloadItem &i) {
  
  XOrsaDownloadEntry *e = new XOrsaDownloadEntry(i,this);
  //
  grid->addWidget(e->la, entries.size(),0);
  grid->addWidget(e->le, entries.size(),1);
  grid->addWidget(e->bar,entries.size(),2); 
  grid->addWidget(e->pb, entries.size(),3); 
  //
  entries.push_back(e);
}

// XOrsaDownload::XOrsaDownload(vector<XOrsaDownloadItem> &items, QWidget *parent) : QWidget(parent,0,Qt::WType_TopLevel | Qt::WDestructiveClose) {
XOrsaDownload::XOrsaDownload(vector<XOrsaDownloadItem> &items, QWidget *parent) : QWidget(parent,0,Qt::WType_TopLevel) {
  
  if (items.size()==0) return;
  
  qInitNetworkProtocols();
  
  QVBoxLayout *vlay = new QVBoxLayout(this,3);
  
  tab = new QTabWidget(this);
  vlay->addWidget(tab);
  
  vlay->addStretch();
  
  planets_w   = new XOrsaDownloadTabPage(this);
  asteroids_w = new XOrsaDownloadTabPage(this);
  comets_w    = new XOrsaDownloadTabPage(this);
  art_sat_w   = new XOrsaDownloadTabPage(this);
  textures_w  = new XOrsaDownloadTabPage(this);
  others_w    = new XOrsaDownloadTabPage(this);
  
  map < XOrsaDownloadItemType, XOrsaDownloadTabPage* > map_tab;
  map_tab[PLANET]               = planets_w;
  map_tab[ASTEROID]             = asteroids_w;
  map_tab[COMET]                = comets_w;
  map_tab[ARTIFICIAL_SATELLITE] = art_sat_w;
  map_tab[TEXTURE]              = textures_w;
  map_tab[OTHER]                = others_w;
  
  unsigned int k=0;
  while(k<items.size()) {
    map_tab[Type(items[k].config_enum)]->InsertItem(items[k]);
    ++k;
  }
  
  tab->addTab(planets_w,  "Planets");
  tab->addTab(asteroids_w,"Asteroids");
  tab->addTab(comets_w,   "Comets");
  tab->addTab(art_sat_w,  "Artificial Satellites");
  tab->addTab(textures_w, "Textures");
  tab->addTab(others_w,   "Other Data");
  
  pages.push_back(planets_w);
  pages.push_back(asteroids_w);
  pages.push_back(comets_w);
  pages.push_back(art_sat_w);
  pages.push_back(textures_w);
  pages.push_back(others_w);
  
  // push buttons
  QHBoxLayout * hok = new QHBoxLayout(vlay,3);
  
  hok->addStretch();
  //
  // QPushButton *okpb = new QPushButton("Download",this);
  // hok->addWidget(okpb);
  // connect(okpb,SIGNAL(clicked()),this,SLOT(download()));
  //
  // hok->addStretch();
  //
  QPushButton * cancpb = new QPushButton("close",this);
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  // hok->addStretch();
  
  tab->setCurrentPage(1); // 1 -> asteroids
}

void XOrsaDownload::cancel_pressed() {
  // close();
  hide(); // finishes to download in background..
}
