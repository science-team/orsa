/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

// define QT_CLEAN_NAMESPACE to avoid glx.h inclusion probems
#define QT_CLEAN_NAMESPACE

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#include "xorsa_opengl.h"
#include "xorsa_objects_combo.h"
#include "xorsa_units_combo.h"
#include "xorsa_plot_area.h"
#include "xorsa_wrapper.h"
#include "xorsa_location_selector.h"

#include <orsa_orbit.h>
#include <orsa_file.h>
#include <orsa_error.h>
#include <orsa_secure_math.h>

#ifdef HAVE_GSL
#include <orsa_orbit_gsl.h>
#endif

#include <sdncal.h>

#include <qlayout.h> 
#include <qcheckbox.h> 
#include <qpushbutton.h> 
#include <qstring.h>
#include <qframe.h>
#include <qlabel.h>
#include <qmenubar.h>
#include <qapplication.h>
#include <qdockwindow.h>
#include <qvbox.h> 
#include <qstatusbar.h> 
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <qfiledialog.h> 
#include <qpngio.h> 
#include <qtoolbutton.h> 
#include <qtooltip.h> 

#include <iostream>
#include "support.h"

// icons
// #include "tmp.xpm"
#include "orbits.xpm"
#include "png.xpm"
#include "film.xpm"
#include "ps.xpm"
#include "pdf.xpm"
#include "bright_positive_z.xpm"
#include "moid.xpm"

// the latest header!
#if !defined(_WIN32) && !defined(Q_OS_MACX)
#include <GL/glx.h>
#endif

using namespace std;
using namespace orsa;

double coefficient_scale_label(const double x, const unsigned int round_digits=1) {
  double l = secure_log10(fabs(x));
  double t = secure_pow(10.0,floor(l)-round_digits+1);
  if (x < 0) t *= -1;
  const double c = fabs(x/t);
  
  if (c <= 1.0) return (1.0 * t);
  if (c <= 2.0) return (2.0 * t);
  if (c <= 5.0) return (5.0 * t);
  if (c <= 10.) return (10. * t);
  if (c >  10.) return (20. * t);
  
  // should never happen
  return x;
}

double coefficient_scale_60_label(const double x) {
  if (x <=  1.0) return ( 1.0);
  if (x <=  2.0) return ( 2.0);
  if (x <=  5.0) return ( 5.0);
  if (x <= 10.0) return (10.0);
  if (x <= 15.0) return (15.0);
  if (x <= 20.0) return (20.0);
  if (x <= 30.0) return (30.0);
  if (x >  30.0) return (60.0);
  
  // should never happen
  return x;
}

double coefficient_angle_label(const double x) {
  // x in degrees!
  if (x >= 1.0) {
    return coefficient_scale_60_label(x);
  } else if (x >= 1.0/60.0) {
    return (1.0/60.0*coefficient_scale_60_label(x*60.0));
  } else {
    return (1.0/3600.0*coefficient_scale_60_label(x*3600.0));
  }
}

static void identity(GLdouble m[16]) {
  m[0+4*0] = 1; m[0+4*1] = 0; m[0+4*2] = 0; m[0+4*3] = 0;
  m[1+4*0] = 0; m[1+4*1] = 1; m[1+4*2] = 0; m[1+4*3] = 0;
  m[2+4*0] = 0; m[2+4*1] = 0; m[2+4*2] = 1; m[2+4*3] = 0;
  m[3+4*0] = 0; m[3+4*1] = 0; m[3+4*2] = 0; m[3+4*3] = 1;
}

GLboolean invert(GLdouble src[16], GLdouble inverse[16]) {
  
  double t;
  int i, j, k, swap;
  GLdouble tmp[4][4];
  
  ::identity(inverse);
  
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      tmp[i][j] = src[i*4+j];
    }
  }
  
  for (i = 0; i < 4; i++) {
    /* look for largest element in column. */
    swap = i;
    for (j = i + 1; j < 4; j++) {
      if (fabs(tmp[j][i]) > fabs(tmp[i][i])) {
	swap = j;
      }
    }
    
    if (swap != i) {
      /* swap rows. */
      for (k = 0; k < 4; k++) {
	t = tmp[i][k];
	tmp[i][k] = tmp[swap][k];
	tmp[swap][k] = t;
        
	t = inverse[i*4+k];
	inverse[i*4+k] = inverse[swap*4+k];
	inverse[swap*4+k] = t;
      }
    }
    
    if (tmp[i][i] == 0) {
      /* no non-zero pivot.  the matrix is singular, which
	 shouldn't happen.  This means the user gave us a bad
	 matrix. */
      return GL_FALSE;
    }
    
    t = tmp[i][i];
    for (k = 0; k < 4; k++) {
      tmp[i][k] /= t;
      inverse[i*4+k] /= t;
    }
    for (j = 0; j < 4; j++) {
      if (j != i) {
	t = tmp[j][i];
	for (k = 0; k < 4; k++) {
	  tmp[j][k] -= tmp[i][k]*t;
	  inverse[j*4+k] -= inverse[i*4+k]*t;
	}
      }
    }
  }
  return GL_TRUE;
}


// XOrsaLabelsModeCombo

XOrsaLabelsModeCombo::XOrsaLabelsModeCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("massless");
  insertItem("massive");
  insertItem("all");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetLabelsMode(int)));
  
  // sync!
  setCurrentItem(1);
  activated(1);
}

void XOrsaLabelsModeCombo::SetLabelsMode(int i) {
  // look at the combobox for the right order
  switch (i) {
  case 0: m = MASSLESS; break;
  case 1: m = MASSIVE; break;
  case 2: m = ALL;     break;
  }  
  emit ModeChanged(m);
}

void XOrsaLabelsModeCombo::SetLabelsMode(OpenGL_Labels_Mode lm) {
  // look at the combobox for the right order
  switch (lm) {
  case MASSLESS: setCurrentItem(0); SetLabelsMode(0);  break;
  case MASSIVE:  setCurrentItem(1); SetLabelsMode(1);  break;
  case ALL:      setCurrentItem(2); SetLabelsMode(2);  break;
  }
}

OpenGL_Labels_Mode XOrsaLabelsModeCombo::GetLabelsMode() {
  return m;
}

//

// test program, return the index of the most perturbing object,
void AutoOrbitIndex(const Frame &f, vector<int> &index) {
  unsigned int j,k;
  index.resize(f.size());
  for (j=0;j<index.size();++j) index[j]=0;
  double tmp,min;
  // double tmp_a,tmp_e; // debug only
  const double min_init=-1;
  Orbit orbit;
  for (j=0;j<f.size();++j) {
    min=min_init;
    
    for (k=0;k<f.size();++k) {
      if (f[k].mass() == 0) continue;
      if (j==k) continue;
      orbit.Compute(f[j],f[k]);
      tmp = orbit.a*(1.0+orbit.e);
      if (((tmp < min) || (min==min_init)) && (orbit.e < 1.0) && (f[k].mass() > f[j].mass())) {
	index[j] = k;
	min = tmp;
	// tmp_a = orbit.a;
	// tmp_e = orbit.e;
      }
    }
  }
}


// XOrsaOpenGLEvolutionTool

XOrsaOpenGLEvolutionTool::XOrsaOpenGLEvolutionTool(QWidget *parent) : QMainWindow(parent,0,Qt::WType_TopLevel | Qt::WDestructiveClose) {
  opengl = new XOrsaOpenGLEvolutionWidget(this);
  setCentralWidget(opengl);
  init_toolbars();
}

XOrsaOpenGLEvolutionTool::XOrsaOpenGLEvolutionTool(int w, int h, QWidget *parent) : QMainWindow(parent,0,Qt::WType_TopLevel | Qt::WDestructiveClose) {
  opengl = new XOrsaOpenGLEvolutionWidget(w,h,this);
  setCentralWidget(opengl);
  init_toolbars();
}

XOrsaOpenGLEvolutionTool::~XOrsaOpenGLEvolutionTool() {
  delete opengl;
}

void XOrsaOpenGLEvolutionTool::SetEvolution(const Evolution *e) {
  
  opengl->SetEvolution(e);
  
  oc_orbits->Set(opengl->bodies(),true);
  oc_orbits->SetObject(HEX_AUTO);
  
  oc_moid_1->Set(opengl->bodies(),false);
  oc_moid_2->Set(opengl->bodies(),false);
}

void XOrsaOpenGLEvolutionTool::init_toolbars() {
 
  new XOrsaAnimationToolBar(opengl,this);
  
  new XOrsaLagrangePointsToolBar(opengl,this);
  
  new XOrsaCameraToolBar(opengl,this);
  
  {
    // export images
    QToolBar * exportTools = new QToolBar(this);
    exportTools->setLabel("Export Tools");
    
    new QToolButton(QIconSet(png_xpm),"Export Image to PNG File",
		    QString::null,opengl,SLOT(export_png()),   exportTools);
    
    new QToolButton(QIconSet(ps_xpm),"Export Image to PostScript File",
		    QString::null,opengl,SLOT(export_ps()),    exportTools);
    
    new QToolButton(QIconSet(pdf_xpm),"Export Image to PDF File",
		    QString::null,opengl,SLOT(export_pdf()),   exportTools);
    
    new QToolButton(QIconSet(film_xpm),"Export All the Frame PNG Images to a Directory (movie)",
		    QString::null,opengl,SLOT(export_movie()), exportTools);
  }
  
  {
    // orbit
    QToolBar * orbitTools = new QToolBar(this);
    orbitTools->setLabel("Orbit Tools");
    //
    // plot_orbits_tb = new XOrsaBoolToolButton(&opengl->draw_orbits,QIconSet(tmp_xpm),"Plot Orbits",orbitTools); 
    plot_orbits_tb = new XOrsaBoolToolButton(&opengl->draw_orbits,QIconSet(orbits_xpm),"Plot Orbits",orbitTools); 
    //
    // oc_orbits = new XOrsaAutoObjectsCombo(orbitTools); 
    oc_orbits = new XOrsaImprovedObjectsCombo(HEX_AUTO,orbitTools);
    //
    bright_positive_z_tb = new XOrsaBoolToolButton(&opengl->bright_positive_Z,QIconSet(bright_positive_z_xpm),"Bright Positive Z",orbitTools); 
    //
    connect(oc_orbits,SIGNAL(ObjectChanged(int)),this,SLOT(slot_set_orbit_reference_body_index(int)));
    //
    // connect(plot_orbits_tb,SIGNAL(toggled(bool)),oc_orbits,SLOT(setEnabled(bool)));
    // connect(plot_orbits_tb,SIGNAL(toggled(bool)),bright_positive_z_tb,SLOT(setEnabled(bool)));
    // connect(plot_orbits_tb,SIGNAL(toggled(bool)),bright_positive_z_tb,SLOT(widgets_enabler()));
    //
    oc_orbits->setEnabled(false);
    //
    bright_positive_z_tb->setEnabled(false);
    //
    QToolTip::add(oc_orbits,"Orbits Reference Body");
    
    orbitTools->addSeparator();
    
    // MOID
    // ..remember: MOID needs GSL!
    moid_tb = new XOrsaBoolToolButton(&opengl->draw_MOID,QIconSet(moid_xpm),"MOID - Minimum Orbital Intersection Distance",orbitTools); 
    //
    oc_moid_1 = new XOrsaImprovedObjectsCombo(orbitTools);
    oc_moid_2 = new XOrsaImprovedObjectsCombo(orbitTools);
    //
    connect(oc_moid_1,SIGNAL(ObjectChanged(int)),this,SLOT(slot_set_moid_body_1_index(int)));
    connect(oc_moid_2,SIGNAL(ObjectChanged(int)),this,SLOT(slot_set_moid_body_2_index(int)));
    
    moid_tb->setEnabled(false);
    
    oc_moid_1->setEnabled(false);
    oc_moid_2->setEnabled(false);
    
    QToolTip::add(oc_moid_1,"MOID Body 1");
    QToolTip::add(oc_moid_2,"MOID Body 2");
    
    connect(plot_orbits_tb, SIGNAL(toggled(bool)),this,SLOT(widgets_enabler()));
    connect(moid_tb,        SIGNAL(toggled(bool)),this,SLOT(widgets_enabler()));
    
  }
}

void XOrsaOpenGLEvolutionTool::widgets_enabler() {
  
  // orbits
  if (plot_orbits_tb->isOn()) {
    oc_orbits->setEnabled(true);
    bright_positive_z_tb->setEnabled(true);
#ifdef HAVE_GSL	
    moid_tb->setEnabled(true);
#endif // HAVE_GSL	
  } else {
    oc_orbits->setEnabled(false);
    bright_positive_z_tb->setEnabled(false);
    moid_tb->setEnabled(false);
  }
  
  // moid
  if (moid_tb->isEnabled() && moid_tb->isOn()) {
#ifdef HAVE_GSL	
    oc_moid_1->setEnabled(true);
    oc_moid_2->setEnabled(true);
#endif // HAVE_GSL
  } else {
    oc_moid_1->setEnabled(false);
    oc_moid_2->setEnabled(false);
  }
}

void XOrsaOpenGLEvolutionTool::slot_set_orbit_reference_body_index(int i) {
  opengl->orbit_reference_body_index = i;
}

void XOrsaOpenGLEvolutionTool::slot_set_moid_body_1_index(int i) {
  opengl->MOID_body_1 = i;
}

void XOrsaOpenGLEvolutionTool::slot_set_moid_body_2_index(int i) {
  opengl->MOID_body_2 = i;
}

// XOrsaOpenGLWidget: a rewrite for XOrsaOpenGL

XOrsaOpenGLWidget::XOrsaOpenGLWidget(QWidget * parent, WFlags f) : QGLWidget(parent,0,0,f) {
  setMinimumSize(300,200);
  init();  
}

XOrsaOpenGLWidget::XOrsaOpenGLWidget(int w, int h, QWidget * parent, WFlags f) : QGLWidget(parent,0,0,f) {
  setMinimumSize(w,h);
  init();
}

void XOrsaOpenGLWidget::init() {
  
  bool_animate = false;
  animation_delay_ms = 40;
  bool_already_animating = false;
  last_fps_frame_index = 0;
  fps.reset();
  
  projection = OGL_PERSPECTIVE;
  
  center_X_rotation = center_Y_rotation = center_Z_rotation = 0.0;
  eye_X_rotation = eye_Y_rotation = eye_Z_rotation = 0.0;
  center_rotation_impulse = 0.1;
  eye_rotation_impulse    = 0.1;
  
  near_and_far_limit_on_distance = true;
  
  bool_near_internal_change = bool_far_internal_change = false;
  bool_near_limits_internal_change = bool_far_limits_internal_change = false;
  
  FOV.SetMinMax(1.0/3600.0,150.0);
  
  center_X_rotation.SetMinMax(0.0,360.0);
  center_Y_rotation.SetMinMax(0.0,360.0);
  center_Z_rotation.SetMinMax(0.0,360.0);
  
  eye_X_rotation.SetMinMax(0.0,360.0);
  eye_Y_rotation.SetMinMax(0.0,360.0);
  eye_Z_rotation.SetMinMax(0.0,360.0);
  
  // fonts
  
#if !defined(_WIN32) && !defined(Q_OS_MACX)
  QFont default_font = font();
  default_font.setFamily("fixed");
  default_font.setRawMode(true);
  default_font.setFamily("fixed");
  default_font.setStyleStrategy(QFont::OpenGLCompatible);
  default_font.setPixelSize(10);
  setFont(default_font);
#endif
  
  ORSA_ERROR("font raw name: %s",font().rawName().latin1());
  
  /* 
     const QGLContext * gl_context = context(); 
     if (gl_context) {
     listBase = glGenLists(256);
     gl_context->generateFontDisplayLists(default_font,listBase); 
     }
  */
  
  init_signals_slots();
}

void XOrsaOpenGLWidget::init_signals_slots() {
  // animation
  connect(&animation_timer,    SIGNAL(timeout()), this, SLOT(animate()));
  connect(&bool_animate,       SIGNAL(changed()), this, SLOT(slot_bool_animate_changed()));
  connect(&animation_delay_ms, SIGNAL(changed()), this, SLOT(slot_animation_delay_changed()));
  // camera
  connect(&distance,       SIGNAL(changed()),this,SLOT(updateGL()));  
  connect(&distance,       SIGNAL(changed()),this,SLOT(slot_near_and_far_limit_on_distance_changed()));  
  //
  connect(&FOV,            SIGNAL(changed()),this,SLOT(slot_update_eye_rotation_impulse()));
  connect(&FOV,            SIGNAL(changed()),this,SLOT(slot_call_resizeGL()));
  connect(&FOV,            SIGNAL(changed()),this,SLOT(updateGL()));
  //
  connect(&near,           SIGNAL(limits_changed()),this,SLOT(slot_near_limits_changed()));
  connect(&near,           SIGNAL(changed()),this,SLOT(slot_call_resizeGL()));
  connect(&near,           SIGNAL(changed()),this,SLOT(slot_near_changed()));
  //
  connect(&far,            SIGNAL(limits_changed()),this,SLOT(slot_far_limits_changed()));
  connect(&far,            SIGNAL(changed()),this,SLOT(slot_call_resizeGL()));
  connect(&far,            SIGNAL(changed()),this,SLOT(slot_far_changed()));
  //
  connect(&near_and_far_limit_on_distance, SIGNAL(changed()),this,SLOT(slot_near_and_far_limit_on_distance_changed()));  
  //
  connect(&projection,         SIGNAL(changed()),this,SLOT(slot_call_resizeGL()));
  //
  connect(&ortho_xy_scale,     SIGNAL(changed()),this,SLOT(slot_call_resizeGL()));
  //
  connect(&ortho_z_near_scale, SIGNAL(changed()),this,SLOT(slot_call_resizeGL()));
  //
  connect(&ortho_z_far_scale,  SIGNAL(changed()),this,SLOT(slot_call_resizeGL()));
  // center rotation
  connect(&center_X_rotation, SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&center_Y_rotation, SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&center_Z_rotation, SIGNAL(changed()),this,SLOT(updateGL()));
  // eye rotation
  connect(&eye_X_rotation, SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&eye_Y_rotation, SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&eye_Z_rotation, SIGNAL(changed()),this,SLOT(updateGL()));
}

void XOrsaOpenGLWidget::slot_bool_animate_changed() {
  if (bool_animate) {
    animation_timer.start(animation_delay_ms);
  } else {
    animation_timer.stop();
  }
}

void XOrsaOpenGLWidget::slot_animation_delay_changed() {
  // cerr << "new animation delay: " << animation_delay_ms << endl;
  fps.reset();
  animation_timer.stop();
  if (bool_animate) {
    animation_timer.start(animation_delay_ms);
  }
}

void XOrsaOpenGLWidget::slot_update_eye_rotation_impulse() {
  eye_rotation_impulse = FOV/MIN(width(),height());
}

void XOrsaOpenGLWidget::slot_near_changed() {
  if (!near_and_far_limit_on_distance) {
    if (!bool_near_internal_change) {
      bool_near_internal_change = true;
      far.SetMin(near);
      bool_near_internal_change = false;
    }
  }
}

void XOrsaOpenGLWidget::slot_far_changed() {
  if (!near_and_far_limit_on_distance) {
    if (!bool_far_internal_change) {
      bool_far_internal_change = true;
      near.SetMax(far);
      bool_far_internal_change = false;
    }
  }
}

void XOrsaOpenGLWidget::slot_near_limits_changed() {
  if (near_and_far_limit_on_distance) {
    if (!bool_near_limits_internal_change) {
      bool_near_limits_internal_change = true;
      near.SetMax(distance);
      bool_near_limits_internal_change = false;
    }
  }
}

void XOrsaOpenGLWidget::slot_far_limits_changed() {
  if (near_and_far_limit_on_distance) {
    if (!bool_far_limits_internal_change) {
      bool_far_limits_internal_change = true;
      far.SetMin(distance);
      bool_far_limits_internal_change = false;
    }
  }
}

void XOrsaOpenGLWidget::slot_near_and_far_limit_on_distance_changed() {
  if (near_and_far_limit_on_distance) {
    near.SetMax(distance);
    far.SetMin(distance);
  } else {
    near.SetMax(far);
    far.SetMin(near);
  }
}

void XOrsaOpenGLWidget::initializeGL() {
  
#if !defined(_WIN32) && !defined(Q_OS_MACX)
  {    
    const GLubyte * vendor      = glGetString(GL_VENDOR);
    const GLubyte * renderer    = glGetString(GL_RENDERER);
    const GLubyte * version     = glGetString(GL_VERSION);
    const GLubyte * extensions  = glGetString(GL_EXTENSIONS);
    
    Display * dpy = glXGetCurrentDisplay();
    int GLX_major, GLX_minor;
    glXQueryVersion(dpy,&GLX_major,&GLX_minor);
    
    ORSA_ERROR("OpenGL Vendor: %s",(char *)vendor);
    ORSA_ERROR("OpenGL Renderer: %s",(char *)renderer);
    ORSA_ERROR("OpenGL Version: %s",(char *)version);
    ORSA_ERROR("GLX Version: %i.%i",GLX_major,GLX_minor);
    ORSA_ERROR("OpenGL Estensions: %s",(char *)extensions);
  }
#endif
  
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_NORMALIZE);
  
  // point anti-aliasing
  glEnable(GL_POINT_SMOOTH); 
  glHint(GL_POINT_SMOOTH_HINT,GL_NICEST);
  
  /* 
     { 
     // debug
     GLfloat values[2];
     glGetFloatv (GL_LINE_WIDTH_GRANULARITY, values);
     printf ("GL_LINE_WIDTH_GRANULARITY value is %3.1f\n", values[0]); 
     glGetFloatv (GL_LINE_WIDTH_RANGE, values); 
     printf ("GL_LINE_WIDTH_RANGE values are %3.1f %3.1f\n", values[0], values[1]);
     }	
  */
  
  // line anti-aliasing
  /* glEnable(GL_LINE_SMOOTH); // bad results (lines too big), maybe blending would solve?
     glEnable(GL_BLEND);
     // glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
     // the right one has to be called when needed, not here
     // glBlendFunc(GL_SRC_ALPHA,GL_ONE);
     glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);
  */
  
  // glHint(GL_POLYGON_SMOOTH_HINT,GL_NICEST);
}

void XOrsaOpenGLWidget::resizeGL(int width, int height) {
  
  // cerr << "inside resizeGL()..." << endl;
  
  // This call to makeCurrent() is needed to avoid 
  // some odd problems when using more than one XOrsaOpenGLWidget
  // from the same application
  makeCurrent();
  
  glViewport(0,0,width,height);
  
  GLdouble w, h;
  
  if (width < height) {
    w = 1.0;
    h = (double) height / (double) width;
  } else {
    w = (double) width / (double) height;
    h = 1.0;
  }
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  //
  switch (projection) {
  case OGL_ORTHO:  
    glOrtho(-ortho_xy_scale*w, ortho_xy_scale*w, -ortho_xy_scale*h, ortho_xy_scale*h, -ortho_z_near_scale, ortho_z_far_scale);
    break;
  case OGL_PERSPECTIVE:
    gluPerspective(FOV,w/h,near,far);
    break;
  }
  
  slot_update_eye_rotation_impulse();
}

void XOrsaOpenGLWidget::paintGL() {
  
  // cerr << "inside paintGL()..." << endl;
  
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  
  glMatrixMode(GL_MODELVIEW);
  
  glLoadIdentity();
  
  glRotated(eye_X_rotation, 1.0, 0.0, 0.0 );
  glRotated(eye_Y_rotation, 0.0, 1.0, 0.0 );
  glRotated(eye_Z_rotation, 0.0, 0.0, 1.0 );
  
  if (projection==OGL_PERSPECTIVE) glTranslated(0,0,-distance);
  // if (projection==OGL_PERSPECTIVE) glTranslatef(0,0,-(float)distance);
  
  glRotated(center_X_rotation, 1.0, 0.0, 0.0 );
  glRotated(center_Y_rotation, 0.0, 1.0, 0.0 );
  glRotated(center_Z_rotation, 0.0, 0.0, 1.0 );
  
  draw();
  
  if (bool_animate) {
    fps.sample();
    // cerr << "estimated FPS: " << fps << endl;
  }
  
  // cerr << "out of paintGL()..." << endl;
}

void XOrsaOpenGLWidget::animate() {
  
}

void XOrsaOpenGLWidget::showEvent(QShowEvent *e) {
  if (bool_animate && (!animation_timer.isActive()))
    animation_timer.start(animation_delay_ms);
  QGLWidget::showEvent(e);
}

void XOrsaOpenGLWidget::hideEvent(QHideEvent *e) {
  bool_animate = animation_timer.isActive();
  animation_timer.stop();
  QGLWidget::hideEvent(e);
}

void XOrsaOpenGLWidget::mousePressEvent(QMouseEvent *e) {
  e->accept();
  old_mouse_position = e->pos();
}

void XOrsaOpenGLWidget::mouseReleaseEvent(QMouseEvent *e) {
  e->accept();
  old_mouse_position = e->pos();
}

void XOrsaOpenGLWidget::mouseMoveEvent(QMouseEvent *e) {
  
  const double dx = e->x() - old_mouse_position.x();
  const double dy = e->y() - old_mouse_position.y();
  
  e->accept();
  
  old_mouse_position = e->pos();
  
  /* 
     const double rx = dx / width();
     const double ry = dy / height();
  */
  //
  /* 
     const double window_parameter = MAX(1.0,MIN(width(),height()));
     //
     const double rx = dx / window_parameter;
     const double ry = dy / window_parameter;
  */
  //
  const double rx = dx;
  const double ry = dy;
  
  if (e->state() & Qt::LeftButton) {
    // rotate the center
    if (e->state() & Qt::KeyButtonMask) {
      center_X_rotation += center_rotation_impulse * ry;
      center_Y_rotation += center_rotation_impulse * rx;
    } else {
      center_X_rotation += center_rotation_impulse * ry;
      center_Z_rotation += center_rotation_impulse * rx; 
    }
  } else if (e->state() & Qt::RightButton) {
    switch (projection) {
    case OGL_ORTHO:  
      // rotate the center
      if (e->state() & Qt::KeyButtonMask) {
	center_X_rotation += center_rotation_impulse * ry;
	center_Z_rotation += center_rotation_impulse * rx;
      } else {
	center_X_rotation += center_rotation_impulse * ry;
	center_Y_rotation += center_rotation_impulse * rx; 
      }
      break;
    case OGL_PERSPECTIVE:
      // rotate the eye
      if (e->state() & Qt::KeyButtonMask) {
	eye_X_rotation -= eye_rotation_impulse * ry;
	eye_Z_rotation -= eye_rotation_impulse * rx;
      } else {
	eye_X_rotation -= eye_rotation_impulse * ry;
	eye_Y_rotation -= eye_rotation_impulse * rx;
      }
      break;
    }
  } 
}

void XOrsaOpenGLWidget::wheelEvent(QWheelEvent *e) {
  e->accept();
  const double factor = pow(1.05,(e->delta()/120));
  switch (projection) {
  case OGL_ORTHO:  
    ortho_xy_scale *= factor;
    break;
  case OGL_PERSPECTIVE:
    FOV *= factor;
    break;
  }
}

/* 
   void XOrsaOpenGLWidget::renderText(int x, int y, const QString &str, const QFont &fnt) const {
   ORSA_ERROR("XOrsaOpenGLWidget::renderText(...) code not ready...");
   //
   QGLWidget::renderText(x,y,"",fnt);
   const int fontsize=MAX(fnt.pixelSize(),fnt.pointSize());
   gl2psText(str,"Times-Roman",fontsize);
   QGLWidget::renderText(x,y,str,fnt);
   }
*/

/* 
   void XOrsaOpenGLWidget::renderText(double x, double y, double z, const QString &str, const QFont &fnt) const {
   ORSA_ERROR("XOrsaOpenGLWidget::renderText(...) code not ready...");
   //
   QGLWidget::renderText(x,y,z,"",fnt);
   const int fontsize=MAX(fnt.pixelSize(),fnt.pointSize());
   gl2psText(str,"Times-Roman",fontsize);
   QGLWidget::renderText(x,y,z,str,fnt);
   }
*/

void XOrsaOpenGLWidget::export_file(const QString &s_filename, const OpenGL_Export_File file_type) {
  
  FILE * fp = fopen(s_filename.latin1(), "wb");
  
  GLint buffsize = 0;
  GLint state    = GL2PS_OVERFLOW;
  
  GLint viewport[4];
  
  glGetIntegerv(GL_VIEWPORT, viewport);
  
  while (state==GL2PS_OVERFLOW) { 
    
    buffsize += 1024*1024;
    
    gl2psBeginPage ("ORSA OpenGL view","ORSA - http://orsa.sourceforge.net",viewport,
		    file_type, GL2PS_BSP_SORT,
		    GL2PS_SILENT | GL2PS_DRAW_BACKGROUND | GL2PS_BEST_ROOT | GL2PS_OCCLUSION_CULL,
		    GL_RGBA, 0, 0, 0, 0, 0, buffsize,
		    fp, s_filename.latin1());
    
    paintGL();
    
    state = gl2psEndPage();
  }
  
  fclose(fp);
}

void XOrsaOpenGLWidget::export_png() {
  
  makeCurrent();
  
  QImage *tmpImage = new QImage(grabFrameBuffer());
  
  if (!tmpImage) {
    ORSA_ERROR("XOrsaOpenGLWidget::export_png(): unexpected problem...");
    return;
  }
  
  const QString s = QFileDialog::getSaveFileName(QString::null,QString::null,this,0,"save image as:");
  
  if (!(s.isEmpty())) {
    tmpImage->save(s,"PNG");
  }
  
  delete tmpImage;
}

void XOrsaOpenGLWidget::export_ps() {
  const QString s = QFileDialog::getSaveFileName(QString::null,QString::null,this,0,"save PostScript file as:");
  if (!(s.isEmpty())) {
    export_file(s, OGL_PS_FILE);
  } 
}

void XOrsaOpenGLWidget::export_pdf() {
  const QString s = QFileDialog::getSaveFileName(QString::null,QString::null,this,0,"save PDF file as:");
  if (!(s.isEmpty())) {
    export_file(s, OGL_PDF_FILE);
  } 
}

////////////

// XOrsaOpenGLEvolutionWidget

XOrsaOpenGLEvolutionWidget::XOrsaOpenGLEvolutionWidget(QWidget *parent, WFlags f) : XOrsaOpenGLWidget(parent,f) {
  init();  
}

XOrsaOpenGLEvolutionWidget::XOrsaOpenGLEvolutionWidget(int w, int h, QWidget *parent, WFlags f) : XOrsaOpenGLWidget(w,h,parent,f) {
  init();
}

void XOrsaOpenGLEvolutionWidget::init() {
  
  max_circle_index = 10; // never more than 20, eats too much memory
  
  frame_changed_from_animation = false;
  
  // signals_and_slots
  connect(&range,        SIGNAL(changed()),this,SLOT(range_changed()));
  //
  connect(&distance,     SIGNAL(changed()),this,SLOT(distance_changed()));
  //
  connect(&evol_counter, SIGNAL(changed()),this,SLOT(evol_counter_changed()));
  connect(&evol_counter, SIGNAL(changed()),this,SLOT(update_distance()));
  connect(&evol_counter, SIGNAL(changed()),this,SLOT(updateGL()));
  //
  connect(&center_body,  SIGNAL(changed()),this,SLOT(center_body_changed()));
  connect(&center_body,  SIGNAL(changed()),this,SLOT(update_distance()));
  connect(&center_body,  SIGNAL(changed()),this,SLOT(distance_changed()));
  connect(&center_body,  SIGNAL(changed()),this,SLOT(updateGL()));
  //
  connect(&eye_body,     SIGNAL(changed()),this,SLOT(eye_body_changed()));
  connect(&eye_body,     SIGNAL(changed()),this,SLOT(update_distance()));
  connect(&eye_body,     SIGNAL(changed()),this,SLOT(updateGL()));
  //
  connect(&eye_on_body,  SIGNAL(changed()),this,SLOT(eye_on_body_changed()));
  connect(&eye_on_body,  SIGNAL(changed()),this,SLOT(update_distance()));
  connect(&eye_on_body,  SIGNAL(changed()),this,SLOT(updateGL()));
  //
  connect(&rotation_body,             SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&rotate_with_rotation_body, SIGNAL(changed()),this,SLOT(updateGL()));
  //
  connect(&draw_orbits,                SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&orbit_reference_body_index, SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&bright_positive_Z,          SIGNAL(changed()),this,SLOT(updateGL()));
  //  
  connect(&draw_MOID,   SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&MOID_body_1, SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&MOID_body_2, SIGNAL(changed()),this,SLOT(updateGL()));
  //
  connect(&draw_Lagrange_points, SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&Lagrange_body_1,      SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&Lagrange_body_2,      SIGNAL(changed()),this,SLOT(updateGL()));
  //
  connect(&draw_tracks,     SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&absolute_tracks, SIGNAL(changed()),this,SLOT(updateGL()));
  //
  connect(&draw_reference_system, SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&draw_XY_grid,          SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&draw_ra_dec_grid,      SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&draw_Z_bars,           SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&draw_scale_bar,        SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&light_time_correction, SIGNAL(changed()),this,SLOT(updateGL()));
  //
  connect(&draw_labels, SIGNAL(changed()),this,SLOT(updateGL()));
  connect(&labels_mode, SIGNAL(changed()),this,SLOT(updateGL())); 
  
  evolution = 0;
  //
  center_body = 0;
  //
  eye_body = 0;
  eye_on_body = false;
  //
  rotation_body = 0;
  rotate_with_rotation_body = false;
  
  evol_counter.SetSize(1);
  evol_counter = 0;
  //
  range = 0.0;
  last_size_checked = 0;
  
  projection = OGL_ORTHO;
  //
  bool_animate = false;
  //
  center_rotation_impulse.SetMinMax(0.01,100.0);
  center_rotation_impulse = 0.1;
  //
  eye_rotation_impulse.SetMinMax(1.0e-8,100.0); // small minimum limit, useful with big zooms...
  eye_rotation_impulse    = 0.1;
  //
  distance.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU));
  distance = FromUnits(4.0,AU);
  //
  FOV.SetMinMax(1.0/3600.0,150.0);
  FOV = 50.0;
  //
  near.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU));
  near = FromUnits(0.1,AU);
  //
  far.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU)); 
  far = FromUnits(20.0,AU);
  //
  ortho_xy_scale.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU)); 
  ortho_xy_scale = FromUnits(3.0,AU);
  //
  ortho_z_near_scale.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU)); 
  ortho_z_near_scale  = FromUnits(10.0,AU);
  //
  ortho_z_far_scale.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU)); 
  ortho_z_far_scale  = FromUnits(10.0,AU);
  
  // object_names_base_list = 0;
}

static ConfigEnum texture_config(const JPL_planets p) {
  ConfigEnum c = NO_CONFIG_ENUM;
  switch (p) {
  case SUN:     c = TEXTURE_SUN;     break;
  case MERCURY: c = TEXTURE_MERCURY; break;
  case VENUS:   c = TEXTURE_VENUS;   break;
  case EARTH:   c = TEXTURE_EARTH;   break;
  case MOON:    c = TEXTURE_MOON;    break;
  case MARS:    c = TEXTURE_MARS;    break;
  case JUPITER: c = TEXTURE_JUPITER; break;
  case SATURN:  c = TEXTURE_SATURN;  break;
  case URANUS:  c = TEXTURE_URANUS;  break;
  case NEPTUNE: c = TEXTURE_NEPTUNE; break;
  case PLUTO:   c = TEXTURE_PLUTO;   break;
    //
  default:      c = NO_CONFIG_ENUM;  break;
  }
  return c;
}

static bool planet_present_in_frame(const JPL_planets p, const Frame & f) {
  bool present = false;
  for (unsigned int k=0;k<f.size();++k) {
    if (f[k].JPLPlanet() == p) {
      present = true;
      break;
    }
  }
  return present;
}

static GLint planet_texture(const JPL_planets p, GLuint & planet_texture_name, const GLint sphere) {
  
  QImage buffer;
  
  if (!buffer.load(config->paths[texture_config(p)]->GetValue().c_str())) {
    ORSA_ERROR("no texture file....");
    const GLint planet_int = glGenLists(1);
    glNewList(planet_int, GL_COMPILE);
    // GLUquadricObj * qobj = gluNewQuadric();
    const double planet_radius = radius(p);
    /* // if not here, where?
       {
       const QColor c = planet_color(p);
       const double factor = 1.0/256.0;
       glColor3d(factor*c.red(),
       factor*c.green(),
       factor*c.blue());
       }
    */
    //
    {
      const QColor c = planet_color(p);
      const double factor = 1.0/256.0;
      glColor3d(factor*c.red(),
		factor*c.green(),
		factor*c.blue());
    }
    // gluQuadricDrawStyle(qobj,GLU_SILHOUETTE);
    /* 
       gluQuadricDrawStyle(qobj,GLU_FILL);
       gluQuadricNormals(qobj,GLU_SMOOTH);
       gluSphere(qobj,planet_radius,72,36);
       gluDeleteQuadric(qobj);
    */
    glScaled(planet_radius,planet_radius,planet_radius);
    glCallList(sphere);
    // N-S axis [test]
    /* glLineWidth(2.0);
       glColor4d(0.0,0.0,1.0,0.8);	
       glBegin(GL_LINES);
       glVertex3d(0,0, 1.1*planet_radius);
       glVertex3d(0,0, 1.0*planet_radius);
       glVertex3d(0,0,-1.0*planet_radius);
       glVertex3d(0,0,-1.1*planet_radius);
       glEnd();
    */
    glEndList();
    
    return planet_int;
  }
  
  glGenTextures(1,&planet_texture_name);
  glBindTexture(GL_TEXTURE_2D,planet_texture_name);
  //
  { 
    GLclampf priority = 1.0;
    glPrioritizeTextures(1,&planet_texture_name,&priority);
  }
  
  QImage planet_texture = QGLWidget::convertToGLFormat(buffer);  // flipped 32bit RGBA
  
  /* glTexImage2D( GL_TEXTURE_2D, 0, 3, planet_texture.width(), planet_texture.height(), 0,
     GL_RGBA, GL_UNSIGNED_BYTE, planet_texture.bits() );
  */
  
  gluBuild2DMipmaps(GL_TEXTURE_2D, 3, planet_texture.width(), planet_texture.height(),
		    GL_RGBA, GL_UNSIGNED_BYTE, planet_texture.bits());
  
  // glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  // glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  
  const GLint planet_int = glGenLists(1);
  glNewList(planet_int, GL_COMPILE);
  //
  // glEnable(GL_TEXTURE_2D);
  /* glTexImage2D( GL_TEXTURE_2D, 0, 3, planet_texture.width(), planet_texture.height(), 0,
     GL_RGBA, GL_UNSIGNED_BYTE, planet_texture.bits() );
  */
  //
  // GLUquadricObj * qobj = gluNewQuadric();
  //
  const double planet_radius = radius(p);
  //
  /* gluQuadricTexture(qobj,GL_TRUE);
     gluSphere(qobj,planet_radius,72,36);
     gluDeleteQuadric(qobj);
  */
  glScaled(planet_radius,planet_radius,planet_radius);
  glCallList(sphere);
  // glDisable(GL_TEXTURE_2D);
  // N-S axis [test]
  /* glLineWidth(2.0);
     glColor4d(0.0,0.0,1.0,0.8);	
     glBegin(GL_LINES);
     glVertex3d(0,0, 1.1*planet_radius);
     glVertex3d(0,0, 1.0*planet_radius);
     glVertex3d(0,0,-1.0*planet_radius);
     glVertex3d(0,0,-1.1*planet_radius);
     glEnd();
  */
  //
  glEndList();
  
  return planet_int;
}

static void check_planet_texture(GLint & planet, const JPL_planets p, GLuint & planet_texture_name, const GLint sphere) {
  if (planet==0) planet = planet_texture(p,planet_texture_name,sphere);
}

void XOrsaOpenGLEvolutionWidget::check_and_call_list(const JPL_planets p) {
  switch (p) {
  case SUN:     check_planet_texture(sun,SUN,sun_texture,sphere);             glBindTexture(GL_TEXTURE_2D,sun_texture);     glCallList(sun);     break;
  case MERCURY: check_planet_texture(mercury,MERCURY,mercury_texture,sphere); glBindTexture(GL_TEXTURE_2D,mercury_texture); glCallList(mercury); break;
  case VENUS:   check_planet_texture(venus,VENUS,venus_texture,sphere);       glBindTexture(GL_TEXTURE_2D,venus_texture);   glCallList(venus);   break;
  case EARTH:   check_planet_texture(earth,EARTH,earth_texture,sphere);       glBindTexture(GL_TEXTURE_2D,earth_texture);   glCallList(earth);   break;
  case MOON:    check_planet_texture(moon,MOON,moon_texture,sphere);          glBindTexture(GL_TEXTURE_2D,moon_texture);    glCallList(moon);    break;
  case MARS:    check_planet_texture(mars,MARS,mars_texture,sphere);          glBindTexture(GL_TEXTURE_2D,mars_texture);    glCallList(mars);    break;
  case JUPITER: check_planet_texture(jupiter,JUPITER,jupiter_texture,sphere); glBindTexture(GL_TEXTURE_2D,jupiter_texture); glCallList(jupiter); break;
  case SATURN:  check_planet_texture(saturn,SATURN,saturn_texture,sphere);    glBindTexture(GL_TEXTURE_2D,saturn_texture);  glCallList(saturn);  break;
  case URANUS:  check_planet_texture(uranus,URANUS,uranus_texture,sphere);    glBindTexture(GL_TEXTURE_2D,uranus_texture);  glCallList(uranus);  break;
  case NEPTUNE: check_planet_texture(neptune,NEPTUNE,neptune_texture,sphere); glBindTexture(GL_TEXTURE_2D,neptune_texture); glCallList(neptune); break;
  case PLUTO:   check_planet_texture(pluto,PLUTO,pluto_texture,sphere);       glBindTexture(GL_TEXTURE_2D,pluto_texture);   glCallList(pluto);   break;
    //
  default: break;
  }
  
  /* 
     {
     // test
     GLboolean b;
     const bool resident = glAreTexturesResident(1,&earth_texture,&b);
     if (resident) {
     cerr << "Earth texture resident!" << endl;
     } else {
     cerr << "Earth texture NOT resident..." << endl;
     }
     }
  */
  
}

void XOrsaOpenGLEvolutionWidget::initializeGL() {
  
  XOrsaOpenGLWidget::initializeGL();
  
  // unitary XY circles
  circle = glGenLists(max_circle_index);
  int list_index=0;
  while (list_index < max_circle_index) {
    glNewList(circle+list_index, GL_COMPILE);
    glBegin(GL_LINE_LOOP);
    const int n_points = 1<<list_index; // 2^k points
    double theta;
    double s,c;
    const double theta_incr = twopi/n_points;
    int count = 0;
    while (count < n_points) {
      theta = count * theta_incr;
#ifdef HAVE_SINCOS
      sincos(theta,&s,&c);
#else // HAVE_SINCOS
      s = sin(theta);
      c = cos(theta);
#endif // HAVE_SINCOS
      glVertex3d(c,s,0);
      ++count;
    }
    glEnd();
    glEndList();
    ++list_index;
  }
  
  // unitary XY on_circles: circles with the (0,0) point on the circle
  {
    on_circle = glGenLists(max_circle_index);
    int list_index=0;
    while (list_index < max_circle_index) {
      glNewList(on_circle+list_index, GL_COMPILE);
      glPushMatrix();
      glTranslated(-1,0,0);
      glCallList(circle+list_index);
      glPopMatrix();
      glEndList();
      ++list_index;
    }
  }
  
  reference_system = glGenLists(1);
  glNewList(reference_system, GL_COMPILE);
  glLineWidth(2.0);
  gl2psLineWidth(2.0);
  if (max_circle_index > 8) {
    glCallList(circle+8);
  } else {
    glCallList(circle+max_circle_index-1);
  }
  glBegin(GL_LINES);
  glVertex3d(0,0,0);
  glVertex3d(0,0,1);
  glEnd();
  glBegin(GL_LINES);
  glVertex3d(0,0,0);
  glVertex3d(1,0,0);
  glEnd();
  glLineWidth(1.0);
  gl2psLineWidth(1.0);
  glEndList();
  
  // X-Y grid
  xy_grid = glGenLists(1);
  glNewList(xy_grid, GL_COMPILE);
  glLineWidth(1.0);
  gl2psLineWidth(1.0);
  int l;
  for (l=-100;l<=100;++l) {
    glBegin(GL_LINES);
    glVertex3d(l,-100,0);
    glVertex3d(l,+100,0);
    glEnd();
    //
    glBegin(GL_LINES);
    glVertex3d(-100,l,0);
    glVertex3d(+100,l,0);
    glEnd();
  }
  glEndList();
  
  // sphere
  
  sphere = glGenLists(1);
  glNewList(sphere, GL_COMPILE);
  GLUquadricObj * qobj = gluNewQuadric();
  gluQuadricDrawStyle(qobj,GLU_FILL);
  gluQuadricTexture(qobj,GL_TRUE);
  gluQuadricNormals(qobj,GLU_SMOOTH);
  gluSphere(qobj,1.0,72,36);
  gluDeleteQuadric(qobj);
  // N-S axis
  glDisable(GL_LIGHTING);
  //
  glLineWidth(2.0);
  glColor4d(0.0,0.0,1.0,0.8);	
  glBegin(GL_LINES);
  glVertex3d(0,0, 1.1);
  glVertex3d(0,0, 1.0);
  glVertex3d(0,0,-1.0);
  glVertex3d(0,0,-1.1);
  glEnd();
  glEndList();
  
  // planets
  
  sun = mercury = venus = earth = moon = mars = jupiter = saturn = uranus = neptune = pluto = 0;
  
  sun_texture = mercury_texture = venus_texture = earth_texture = moon_texture = mars_texture = jupiter_texture = saturn_texture = uranus_texture = neptune_texture = pluto_texture = 0;
  
  /* 
     sun     = planet_texture(SUN);
     mercury = planet_texture(MERCURY);
     venus   = planet_texture(VENUS);
     earth   = planet_texture(EARTH);
     moon    = planet_texture(MOON);
     mars    = planet_texture(MARS);
     jupiter = planet_texture(JUPITER);
     saturn  = planet_texture(SATURN);
     uranus  = planet_texture(URANUS);
     neptune = planet_texture(NEPTUNE);
     pluto   = planet_texture(PLUTO);
  */
  
  /* 
     {
     // earth
     
     QImage buffer;
     
     if (!buffer.load(config->paths[TEXTURE_EARTH]->GetValue().c_str())) {
     ORSA_ERROR("no Earth texture file....");
     QImage dummy(128,128,32);
     dummy.fill(Qt::blue.rgb());
     buffer = dummy;
     }
     
     QImage earth_texture = QGLWidget::convertToGLFormat(buffer);  // flipped 32bit RGBA
     
     glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
     glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
     
     earth = glGenLists(1);
     glNewList(earth, GL_COMPILE);
     //
     glEnable(GL_TEXTURE_2D);
     glTexImage2D( GL_TEXTURE_2D, 0, 3, earth_texture.width(), earth_texture.height(), 0,
     GL_RGBA, GL_UNSIGNED_BYTE, earth_texture.bits() );
     //
     GLUquadricObj * qobj = gluNewQuadric();
     //
     gluQuadricTexture(qobj,GL_TRUE);
     gluSphere(qobj,radius(EARTH),180,90);
     gluDeleteQuadric(qobj);
     glDisable(GL_TEXTURE_2D);
     // N-S axis [test]
     glLineWidth(2.0);
     glColor4d(0.0,0.0,1.0,0.8);	
     glBegin(GL_LINES);
     glVertex3d(0,0, 1.1*radius(EARTH));
     glVertex3d(0,0, 1.0*radius(EARTH));
     glVertex3d(0,0,-1.0*radius(EARTH));
     glVertex3d(0,0,-1.1*radius(EARTH));
     glEnd();
     //
     glEndList();
     
     }
     
     {
     // moon
     
     QImage buffer;
     
     if (!buffer.load(config->paths[TEXTURE_MOON]->GetValue().c_str())) {
     ORSA_ERROR("no Moon texture file....");
     QImage dummy(128,128,32);
     dummy.fill(Qt::white.rgb());
     buffer = dummy;
     }
     
     QImage moon_texture = QGLWidget::convertToGLFormat(buffer);  // flipped 32bit RGBA
     
     glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
     glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
     
     moon = glGenLists(1);
     glNewList(moon, GL_COMPILE);
     //
     glEnable(GL_TEXTURE_2D);
     glTexImage2D( GL_TEXTURE_2D, 0, 3, moon_texture.width(), moon_texture.height(), 0,
     GL_RGBA, GL_UNSIGNED_BYTE, moon_texture.bits() );
     //
     GLUquadricObj * qobj = gluNewQuadric();
     //
     gluQuadricTexture(qobj,GL_TRUE);
     gluSphere(qobj,radius(MOON),180,90);
     gluDeleteQuadric(qobj);
     glDisable(GL_TEXTURE_2D);
     glEndList();
     }
  */
  
}

void XOrsaOpenGLEvolutionWidget::draw() {
  
  // the eye position vector must be updated here and only here
  update_EyePosition();
  
  // cerr << "XOrsaOpenGLEvolutionWidget::draw() called..." << endl;
  
  // cerr << "distance: " << distance << endl;
  
  /* 
     { 
     //debug
     const Vector eye_position = EyePosition();
     cerr << "eye position: " << eye_position.x << " " << eye_position.y << " " << eye_position.z << endl;
     }
  */
  
  glPushMatrix();
  
  if (evolution) {
    if (evolution->size()) {
      
      // reference frame rotation HERE!
      if (rotate_with_rotation_body) {
	
	const Vector dir_pos = RotationBodyPosition() - CenterBodyPosition();
	
	const double phi   = secure_atan2(dir_pos.y,dir_pos.x);
	const double theta = secure_atan2(dir_pos.z,secure_sqrt(dir_pos.x*dir_pos.x+dir_pos.y*dir_pos.y)); 
	
	glRotated(-theta*(180.0/pi),   0.0,1.0,0.0);
	glRotated(180.0-phi*(180.0/pi),0.0,0.0,1.0);
      }	
      
      /* 
	 {
	 // test: sky grid
	 
	 // CHECK CAREFULLY THIS CODE!!
	 
	 glPushMatrix();	
	 
	 // back-rotate, translate and rotate again
	 if (rotate_with_rotation_body) {
	 const Vector dir_pos = RotationBodyPosition() - CenterBodyPosition();
	 const double phi   = secure_atan2(dir_pos.y,dir_pos.x);
	 const double theta = secure_atan2(dir_pos.z,secure_sqrt(dir_pos.x*dir_pos.x+dir_pos.y*dir_pos.y)); 
	 
	 glRotated(phi*(180.0/pi)-180.0,0.0,0.0,1.0);
	 glRotated(theta*(180.0/pi),    0.0,1.0,0.0);
	 
	 glTranslate(EyePosition()-CenterBodyPosition());
	 
	 glRotated(-theta*(180.0/pi),   0.0,1.0,0.0);
	 glRotated(180.0-phi*(180.0/pi),0.0,0.0,1.0);
	 
	 } else {
	 glTranslate(EyePosition()-CenterBodyPosition());
	 }	
	 
	 // glDepthMask(GL_FALSE);   
	 // glDisable(GL_DEPTH_TEST);
	 
	 // glLineStipple(1,0x8888);
	 glLineStipple(1,0x5555);
	 glEnable(GL_LINE_STIPPLE);
	 
	 glColor3d(0,0.7,0);	
	 glLineWidth(1.0);
	 gl2psLineWidth(1.0);
	 
	 {
	 GLUquadricObj * qobj = gluNewQuadric();
	 gluQuadricDrawStyle(qobj,GLU_SILHOUETTE);
	 // gluQuadricDrawStyle(qobj,GLU_FILL);
	 gluQuadricNormals(qobj,GLU_SMOOTH);
	 gluSphere(qobj,range,72,36);
	 gluDeleteQuadric(qobj);
	 }
	 
	 glDisable(GL_LINE_STIPPLE);
	 
	 // restore depth function
	 // glDepthFunc(saved_depth_func);
	 
	 // glDepthMask(GL_TRUE);   
	 // glEnable(GL_DEPTH_TEST);
	 
	 glPopMatrix();
	 }
      */
      
      /* 
	 {
	 // debug: how is transate really working?
	 glPushMatrix();
	 //
	 double m_pre[16];
	 glGetDoublev(GL_MODELVIEW_MATRIX,m_pre);
	 //
	 double inverted_pre[16];
	 invert(m_pre,inverted_pre);
	 //
	 double v[3];
	 //
	 v[0] = -CenterBodyPosition().x;
	 v[1] = -CenterBodyPosition().y;
	 v[2] = -CenterBodyPosition().z;
	 //
	 fprintf(stderr,"v: %20.16f %20.16f %20.16f\n",v[0],v[1],v[2]);
	 //
	 glTranslated(v[0],v[1],v[2]);
	 //
	 double m_post[16];
	 glGetDoublev(GL_MODELVIEW_MATRIX,m_post);
	 //
	 double inverted_post[16];
	 invert(m_post,inverted_post);
	 //
	 // -----------------
	 // output now!
	 // cerr << "translation in m_pre:  " << m_pre[12]  << " " << m_pre[13]  << " " << m_pre[14]  << endl;
	 // cerr << "translation in m_post: " << m_post[12] << " " << m_post[13] << " " << m_post[14] << endl;
	 fprintf(stderr,"translation in m_pre:  %20.16f %20.16f %20.16f\n",m_pre[12], m_pre[13], m_pre[14]);
	 fprintf(stderr,"translation in m_post: %20.16f %20.16f %20.16f\n",m_post[12],m_post[13],m_post[14]);
	 //
	 double w[3];
	 //
	 w[0] = v[0]*m_pre[0]+v[1]*m_pre[4]+v[2]*m_pre[8];
	 w[1] = v[0]*m_pre[1]+v[1]*m_pre[5]+v[2]*m_pre[9];
	 w[2] = v[0]*m_pre[2]+v[1]*m_pre[6]+v[2]*m_pre[10];
	 //
	 fprintf(stderr,"w: %20.16f %20.16f %20.16f\n",w[0],w[1],w[2]);
	 // back to the original v = z
	 double z[3];
	 //
	 w[0] = m_post[12]-m_pre[12];
	 w[1] = m_post[13]-m_pre[13];
	 w[2] = m_post[14]-m_pre[14];
	 //
	 z[0] = w[0]*inverted_pre[0]+w[1]*inverted_pre[4]+w[2]*inverted_pre[8];
	 z[1] = w[0]*inverted_pre[1]+w[1]*inverted_pre[5]+w[2]*inverted_pre[9];
	 z[2] = w[0]*inverted_pre[2]+w[1]*inverted_pre[6]+w[2]*inverted_pre[10];
	 //
	 fprintf(stderr,"z: %20.16f %20.16f %20.16f\n",z[0],z[1],z[2]);
	 //
	 fprintf(stderr,"v-z: %20.16f %20.16f %20.16f\n",v[0]-z[0],v[1]-z[1],v[2]-z[2]);
	 // note: v-z is the difference between the translation requested and 
	 //       the real OpenGL translation: the two differ for many reasons,
	 //       especially because OpenGL internally uses floats... :(
	 //
	 glPopMatrix();
	 }
      */
      
      // [x]
      // glTranslate(-CenterBodyPosition());
      
      if (draw_orbits || draw_MOID) {
	if (orbit_reference_body_index==-HEX_AUTO) {
	  if (orbit_reference_body_index_vector[evol_counter].size()==0) {
	    vector<int> ref_index;
	    AutoOrbitIndex(evol_frame,ref_index);
	    orbit_reference_body_index_vector[evol_counter] = ref_index;
	  }
	}
      }
      
      internal_draw_bodies_and_labels();
      
      // transparent lines rendering should be the last one
      if (draw_orbits) {
	// internal_draw_orbit_ellipse(); // OLD method
   	internal_draw_orbit_on_ellipse();
      }
      
      if (draw_Lagrange_points) {
	internal_draw_Lagrange_points();
      }
      
      if (draw_MOID) {
	internal_draw_MOID();
      }
      
      // last one...
      internal_draw_OSD();
      
      // test: transparent reference plane
      /* 
	 {
	 glPushMatrix();
	 glDepthMask(GL_FALSE); // depth-buffer read-only
	 // glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	 glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	 // glBlendFunc(GL_SRC_ALPHA,GL_DST_COLOR);
	 // glBlendFunc(GL_ONE,GL_ZERO);
	 // glBlendFunc(GL_SRC_ALPHA,GL_ZERO);
	 // glColor4d(0.2,0.4,1.0,0.5);
	 glColor4d(0.2,0.4,1.0,0.6);
	 // wto quads are needed!
	 // visible from z>0
	 glBegin(GL_QUADS);
	 glVertex3d(+ortho_z_scale,+ortho_z_scale,0);
	 glVertex3d(-ortho_z_scale,+ortho_z_scale,0);
	 glVertex3d(-ortho_z_scale,-ortho_z_scale,0);
	 glVertex3d(+ortho_z_scale,-ortho_z_scale,0);
	 glEnd();
	 //
	 glColor4d(0.2,0.4,1.0,0.2);	
	 //
	 // visible from z<0
	 glBegin(GL_QUADS);
	 glVertex3d(+ortho_z_scale,+ortho_z_scale,0);
	 glVertex3d(+ortho_z_scale,-ortho_z_scale,0);
	 glVertex3d(-ortho_z_scale,-ortho_z_scale,0);
	 glVertex3d(-ortho_z_scale,+ortho_z_scale,0);
	 glEnd();
	 //
	 // adding lighting can be nice...
	 //
	 glDepthMask(GL_TRUE);
	 glPopMatrix(); 
	 }
      */
      
    }
  } 
  
  glPopMatrix();
}

void XOrsaOpenGLEvolutionWidget::animate() {
  
  if (bool_animate==false) return;
  
  if (bool_already_animating) {
    return;
  }
  
  bool_already_animating = true;
  
  if (evolution == 0) return;
  
  // cerr << "increasing evol_counter..." << endl;
  //
  ++evol_counter;
  //
  // cerr << "evol_counter increased... " << endl;
  
  // IMPORTANT! This call to qApp->processEvents()
  // prevents a lot of problems when using more
  // than one OpenGL window in the same session
  // qApp->processEvents(10000);
  // qApp->processEvents(10);
  qApp->processEvents(1);
  
  /* check not needed if evol_counter is a SizeObject...
     if (evol_counter == (int)evolution->size()) {
     evol_counter = 0;
     } 
  */
  // evol_frame should be automatically updated
  // evol_frame = (*evolution)[evol_counter];
  
  orbit_reference_body_index_vector.resize(evolution->size());
  orbit_cache_vector.resize(evolution->size());
  
  // automatically called...
  // updateGL();
  
  frame_changed_from_animation = true;
  emit frame_changed(evol_counter);
  frame_changed_from_animation = false;
  bool_already_animating = false;
}

void XOrsaOpenGLEvolutionWidget::SetEvolution(const Evolution * evol_in) {
  if (evol_in == 0) {
    return;
  } else {
    evolution = evol_in;
    const unsigned int e_size = evolution->size();
    evol_counter.SetSize(e_size);
    evol_counter = 0;
    // need to call evol_counter_changed() manually 
    // here, because maybe evol_counter was already
    // equal to zero, so no signal is emitted and
    // evol_counter_changed() called.
    //
    evol_counter_changed();
    
    last_size_checked = 0;
    
    {
      const XOrsaEvolution * const oe = dynamic_cast <const XOrsaEvolution * const> (evolution);
      if (oe) {
	oe->add_event_receiver(integration_step_done_event_type,this);
	oe->add_event_receiver(integration_started_event_type,this);
	oe->add_event_receiver(integration_finished_event_type,this);
	oe->add_event_receiver(evolution_modified_event_type,this);
      } else {
	ORSA_LOGIC_ERROR("");
      }
    }
    
    orbit_reference_body_index_vector.clear();
    orbit_reference_body_index_vector.resize(e_size);
    
    orbit_cache_vector.clear();
    orbit_cache_vector.resize(e_size);
    
    // generate_object_names();
    
    // needed
    update_range();
    
    emit evolution_changed();
  }
}

void XOrsaOpenGLEvolutionWidget::customEvent(QCustomEvent *e) {
  switch (e->type()) {
  case integration_step_done_event_type: 
    break;
  case integration_started_event_type:
    break;
  case integration_finished_event_type: 
    break;
  case evolution_modified_event_type: 
    update_sizes();
    update_range();
    break;
  case universe_modified_event_type:
    break;
  default:
    break;
  }
}

Vector XOrsaOpenGLEvolutionWidget::BodyPosition(const int i) const {
  if (i >= 0) {
    if (i < (int)evol_frame.size()) {
      return evol_frame[i].position();
    } else {
      ORSA_ERROR("problem in XOrsaOpenGLEvolutionWidget::BodyPosition()...");
      return Vector(0,0,0);
    }	
  } else {
    if (i == -HEX_ORIGIN) {
      return Vector(0,0,0);
    } else if (i == -HEX_CENTER_OF_MASS) {
      return evol_frame.Barycenter();
    } else {
      ORSA_ERROR("problem in XOrsaOpenGLEvolutionWidget::BodyPosition()...");
      return Vector(0,0,0);
    }
  }
}

string XOrsaOpenGLEvolutionWidget::BodyName(const int i) const {
  if (i >= 0) {
    if (i < (int)evol_frame.size()) {
      return evol_frame[i].name();
    } else {
      ORSA_ERROR("problem in XOrsaOpenGLEvolutionWidget::BodyName()...");
      return "";
    }	
  } else {
    if (i == -HEX_ORIGIN) {
      return "";
    } else if (i == -HEX_CENTER_OF_MASS) {
      return "center of mass";
    } else {
      ORSA_ERROR("problem in XOrsaOpenGLEvolutionWidget::BodyName()...");
      return "";
    }
  }
}

Vector XOrsaOpenGLEvolutionWidget::CenterBodyPosition() const {
  return BodyPosition(center_body);
}

string XOrsaOpenGLEvolutionWidget::CenterBodyName() const {
  return BodyName(center_body);
}

Vector XOrsaOpenGLEvolutionWidget::EyeBodyPosition() const {
  if (!eye_on_body) ORSA_ERROR("Hmmm... something funny here...");
  return BodyPosition(eye_body);
}

string XOrsaOpenGLEvolutionWidget::EyeBodyName() const {
  if (!eye_on_body) ORSA_ERROR("Hmmm... something funny here...");
  return BodyName(eye_body);
}

Vector XOrsaOpenGLEvolutionWidget::RotationBodyPosition() const {
  if (!rotate_with_rotation_body) ORSA_ERROR("Hmmm... something funny here...");
  return BodyPosition(rotation_body);
}

string XOrsaOpenGLEvolutionWidget::RotationBodyName() const {
  if (!rotate_with_rotation_body) ORSA_ERROR("Hmmm... something funny here...");
  return BodyName(rotation_body);
}

void XOrsaOpenGLEvolutionWidget::update_EyePosition() {
  if (projection != OGL_PERSPECTIVE) return;
  //
  double m[16];
  glGetDoublev(GL_MODELVIEW_MATRIX,m);
  //
  double inverted[16];
  invert(m,inverted);
  //
  double v[3];
  //
  v[0] = -m[12];
  v[1] = -m[13];
  v[2] = -m[14];
  //
  double w[3];
  //
  w[0] = v[0]*inverted[0]+v[1]*inverted[4]+v[2]*inverted[8];
  w[1] = v[0]*inverted[1]+v[1]*inverted[5]+v[2]*inverted[9];
  w[2] = v[0]*inverted[2]+v[1]*inverted[6]+v[2]*inverted[10];
  //
  eye_position.x = w[0];
  eye_position.y = w[1];
  eye_position.z = w[2];
  //
  eye_position += CenterBodyPosition();
}

void XOrsaOpenGLEvolutionWidget::update_sizes() {
  const unsigned int e_size = evolution->size();
  //
  orbit_reference_body_index_vector.resize(e_size);
  orbit_cache_vector.resize(e_size);
  evol_counter.SetSize(e_size);
}

void XOrsaOpenGLEvolutionWidget::update_range() {
  
  double x_min,x_max,y_min,y_max,z_min,z_max;
  //
  x_min=x_max=(*evolution)[0][0].position().x;
  y_min=y_max=(*evolution)[0][0].position().y;
  z_min=z_max=(*evolution)[0][0].position().z;
  //
  const unsigned int current_evolution_size = evolution->size();
  for (unsigned int j=last_size_checked;j<current_evolution_size;++j) {
    const Frame f = (*evolution)[j];
    for(unsigned int body=0;body<f.size();++body) {
      const double _x = f[body].position().x;
      x_min = MIN(x_min,_x);
      x_max = MAX(x_max,_x);
      const double _y = f[body].position().y;
      y_min = MIN(y_min,_y);
      y_max = MAX(y_max,_y);
      const double _z = f[body].position().z;
      z_min = MIN(z_min,_z);
      z_max = MAX(z_max,_z);
    }
  }
  //
  const double delta_x = x_max - x_min;
  const double delta_y = y_max - y_min;
  const double delta_z = z_max - z_min;
  //
  const double old_range = range;
  range = MAX(old_range,MAX(delta_x,MAX(delta_y,delta_z)));
  //
  last_size_checked = current_evolution_size;
}

void XOrsaOpenGLEvolutionWidget::export_movie() {
  
  const QString s_dir = QFileDialog::getExistingDirectory(QString::null,this,0,"save images into:",true);
  
  if (!(s_dir.isEmpty())) {
    
    makeCurrent();
    
    const unsigned int old_evol_counter = evol_counter;
    
    QString s_filename;
    unsigned int j=0;
    while (j < total_frames()) {
      
      s_filename.sprintf("frame-%06i.png",j);
      s_filename.prepend(s_dir);
      
      // set_frame(j);
      evol_counter = j;
      
      QImage * tmpImage = new QImage(grabFrameBuffer());
      
      if (!tmpImage) {
	ORSA_ERROR("XOrsaOpenGLWidget::export_png(): unexpected problem...");
	return;
      }
      
      tmpImage->save(s_filename,"PNG");
      
      delete tmpImage;
      
      ++j;
    }
    
    evol_counter = old_evol_counter;
  }
}

void XOrsaOpenGLEvolutionWidget::internal_draw_bodies_and_labels() {
  
  for (unsigned int j=0;j<evol_frame.size();++j) {
    
    glPushMatrix();
    
    // IMPORTANT: this difference (pos - center) is REALLY needed:
    // allows to have the central body in (0,0,0), avoiding
    // some annoying shaking effects.
    //
    const Vector p = evol_frame[j].position() - CenterBodyPosition();
    
    /* 
       if (evol_frame[j].mass() != 0) {
       glPointSize(1);
       gl2psPointSize(1);
       glColor3d(1,0,0);
       } else {
       glPointSize(1);
       gl2psPointSize(1);
       glColor3d(0,0,1);
       }
    */
    
    if (evol_frame[j].radius() > 0) {
      
      // [x]
      // glTranslate(p);
      // glTranslate(p-CenterBodyPosition());
      glTranslate(p);
      
      /* 
	 if (evol_frame[j].name() == "Earth") {
	 
	 GLfloat whiteDir[4] = {2.0, 2.0, 2.0, 1.0};
	 GLfloat whiteAmb[4] = {1.0, 1.0, 1.0, 1.0};
	 //
	 // GLfloat lightPos[4] = {30.0, 30.0, 30.0, 1.0};
	 GLfloat lightPos[4];
	 {
	 // light-time delay?
	 Vector sunpos;
	 {
	 unsigned int q = 0;
	 while (q < evol_frame.size()) {
	 if (evol_frame[q].name() == "Sun") {
	 sunpos = evol_frame[q].position();
	 break;
	 }
	 ++q;
	 }
	 }
	 sunpos -= evol_frame[j].position(); // sunpos relative to Earth position
	 //
	 lightPos[0] = sunpos.x;
	 lightPos[1] = sunpos.y;
	 lightPos[2] = sunpos.z;
	 //
	 lightPos[3] = 1.0;
	 }
	 
	 glEnable(GL_LIGHTING);
	 glEnable(GL_LIGHT0);
	 glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
	 glLightModelfv(GL_LIGHT_MODEL_AMBIENT, whiteAmb);
	 
	 glMaterialfv(GL_FRONT, GL_DIFFUSE, whiteDir);
	 glMaterialfv(GL_FRONT, GL_SPECULAR, whiteDir);
	 glMaterialf(GL_FRONT, GL_SHININESS, 20.0);
	 
	 glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDir);  // enable diffuse
	 glLightfv(GL_LIGHT0, GL_SPECULAR, whiteDir);	// enable specular
	 glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	 
	 glPushMatrix();	
	 //
	 if (universe->GetReferenceSystem() == ECLIPTIC) {
	 glRotated(-(180.0/pi)*obleq_J2000().GetRad(),1,0,0);
	 }
	 glRotated((180.0/pi)*gmst(evol_frame).GetRad(),0,0,1);
	 glRotated(90,0,0,1); // ADD an extra rotation of 90 degrees when using a texture object!!
	 glCallList(earth); 
	 // 
	 glPopMatrix();
	 
	 glDisable(GL_LIGHT0);
	 glDisable(GL_LIGHTING);
	 
	 } else if (evol_frame[j].name() == "Moon") {
	 
	 GLfloat whiteDir[4] = {2.0, 2.0, 2.0, 1.0};
	 GLfloat whiteAmb[4] = {1.0, 1.0, 1.0, 1.0};
	 //
	 // GLfloat lightPos[4] = {30.0, 30.0, 30.0, 1.0};
	 GLfloat lightPos[4]; // relative position of the Sun with respect to the planet
	 {
	 // light-time delay?
	 Vector sunpos;
	 {
	 unsigned int q = 0;
	 while (q < evol_frame.size()) {
	 if (evol_frame[q].name() == "Sun") {
	 sunpos = evol_frame[q].position();
	 break;
	 }
	 ++q;
	 }
	 }
	 sunpos -= evol_frame[j].position(); // sunpos relative to Moon position
	 //
	 lightPos[0] = sunpos.x;
	 lightPos[1] = sunpos.y;
	 lightPos[2] = sunpos.z;
	 //
	 lightPos[3] = 1.0;
	 }
	 
	 Body Earth;
	 Vector EarthPosition;
	 {
	 unsigned int q = 0;
	 while (q < evol_frame.size()) {
	 if (evol_frame[q].name() == "Earth") {
	 Earth = evol_frame[q];
	 EarthPosition = evol_frame[q].position();
	 break;
	 }
	 ++q;
	 }
	 EarthPosition -= evol_frame[j].position(); // sunpos relative to Moon position
	 }
	 
	 glEnable(GL_LIGHTING);
	 glEnable(GL_LIGHT0);
	 glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
	 glLightModelfv(GL_LIGHT_MODEL_AMBIENT, whiteAmb);
	 
	 glMaterialfv(GL_FRONT, GL_DIFFUSE, whiteDir);
	 glMaterialfv(GL_FRONT, GL_SPECULAR, whiteDir);
	 glMaterialf(GL_FRONT, GL_SHININESS, 20.0);
	 
	 glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDir);  // enable diffuse
	 glLightfv(GL_LIGHT0, GL_SPECULAR, whiteDir);	// enable specular
	 glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	 
	 glPushMatrix();	
	 //
	 // NOTE: all the Moon rotations have to be checked!!!
	 // the angles seem to be quite good, but probably
	 // some fine-tuning is needed
	 //
	 {
	 Angle inclination;
	 inclination.SetDPS(1,32,32.7);
	 glRotated(-(180.0/pi)*inclination.GetRad(),1,0,0);
	 }
	 //
	 if (universe->GetReferenceSystem() == EQUATORIAL) { 
	 glRotated((180.0/pi)*obleq_J2000().GetRad(),1,0,0);
	 }
	 
	 // other (better?) way: use the angles of the orbit
	 {
	 Orbit orbit;
	 orbit.Compute(evol_frame[j],Earth);
	 
	 // M seems to be better than E...
	 glRotated(90+180+
	 (180.0/pi)*(orbit.omega_node+orbit.omega_pericenter+orbit.M),0,0,1); // 90 for the texture...
	 // glRotated(90+180+(180.0/pi)*
	 // (orbit.omega_node+orbit.omega_pericenter+orbit.GetE()),0,0,1); // 90 for the texture...
	 }
	 //
	 glCallList(moon);
	 //
	 glPopMatrix();
	 
	 glDisable(GL_LIGHT0);
	 glDisable(GL_LIGHTING);
	 
	 } else {
      */
      
      GLfloat whiteDir[4] = {2.0, 2.0, 2.0, 1.0};
      GLfloat whiteAmb[4] = {1.0, 1.0, 1.0, 1.0};
      //
      // GLfloat lightPos[4] = {30.0, 30.0, 30.0, 1.0};
      GLfloat lightPos[4]; // relative position of the Sun with respect to the planet
      {
	// light-time delay?
	Vector sunpos;
	{
	  unsigned int q = 0;
	  while (q < evol_frame.size()) {
	    if (evol_frame[q].name() == "Sun") {
	      sunpos = evol_frame[q].position();
	      break;
	    }
	    ++q;
	  }
	}
	sunpos -= evol_frame[j].position(); // sunpos relative to Earth position
	//
	lightPos[0] = sunpos.x;
	lightPos[1] = sunpos.y;
	lightPos[2] = sunpos.z;
	//
	lightPos[3] = 1.0;
      }
      
      if (evol_frame[j].JPLPlanet() != SUN) {	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, whiteAmb);
	
	glMaterialfv(GL_FRONT, GL_DIFFUSE, whiteDir);
	glMaterialfv(GL_FRONT, GL_SPECULAR, whiteDir);
	glMaterialf(GL_FRONT, GL_SHININESS, 20.0);
	
	glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDir);  // enable diffuse
	glLightfv(GL_LIGHT0, GL_SPECULAR, whiteDir);	// enable specular
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
      }
      
      glPushMatrix();	
      //
      if (universe->GetReferenceSystem() == ECLIPTIC) {
	glRotated(-(180.0/pi)*obleq_J2000().GetRad(),1,0,0);
      }
      //
      Angle alpha,delta,W;
      alpha_delta_meridian(evol_frame[j].JPLPlanet(),evol_frame,alpha,delta,W);
      glRotated(90.0+(180.0/pi)*alpha.GetRad(),0,0,1);
      glRotated(90.0-(180.0/pi)*delta.GetRad(),1,0,0);
      // glRotated((180.0/pi)*W.GetRad(),0,0,1);
      // glRotated(90,0,0,1); // ADD an extra rotation of 90 degrees when using a texture object!!
      glRotated(90.0+(180.0/pi)*W.GetRad(),0,0,1);
      //
      /* 
	 switch (evol_frame[j].JPLPlanet()) {
	 case SUN:     glCallList(sun);     break;
	 case MERCURY: glCallList(mercury); break;
	 case VENUS:   glCallList(venus);   break;
	 case EARTH:   glCallList(earth);   break;
	 case MOON:    glCallList(moon);    break;
	 case MARS:    glCallList(mars);    break;
	 case JUPITER: glCallList(jupiter); break;
	 case SATURN:  glCallList(saturn);  break;
	 case URANUS:  glCallList(uranus);  break;
	 case NEPTUNE: glCallList(neptune); break;
	 case PLUTO:   glCallList(pluto);   break;
	 //
	 default: break;
	 }
      */
      //
      
      // texture handling here
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      // glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      // glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
      //
      glEnable(GL_TEXTURE_2D);
      //
      check_and_call_list(evol_frame[j].JPLPlanet());
      //
      glDisable(GL_TEXTURE_2D);
      //
      glPopMatrix();
      
      if (evol_frame[j].JPLPlanet() != SUN) {	
	glDisable(GL_LIGHT0);
	glDisable(GL_LIGHTING);
      }      
      
      // test
      if (0) {
	{
	  Angle alpha,delta,meridian;
	  Vector planet_axis;
	  Vector planet_x_axis;
	  if (evol_frame[j].JPLPlanet() == NONE) continue;
	  alpha_delta_meridian(evol_frame[j].JPLPlanet(),evol_frame,alpha,delta,meridian);
	  //
	  planet_axis.Set(0,0,3.0*evol_frame[j].radius());
	  planet_x_axis.Set(3.0*evol_frame[j].radius(),0,0);
	  //
	  // glRotated(90.0+(180.0/pi)*meridian.GetRad(),0,0,1);
	  planet_axis.rotate(meridian.GetRad(),0,0);
	  planet_x_axis.rotate(meridian.GetRad(),0,0);
	  // glRotated(90.0-(180.0/pi)*delta.GetRad(),1,0,0);
	  planet_axis.rotate(0,halfpi-delta.GetRad(),0);
	  planet_x_axis.rotate(0,halfpi-delta.GetRad(),0);
	  // glRotated(90.0+(180.0/pi)*alpha.GetRad(),0,0,1);
	  planet_axis.rotate(halfpi+alpha.GetRad(),0,0);
	  planet_x_axis.rotate(halfpi+alpha.GetRad(),0,0);
	  //
	  if (universe->GetReferenceSystem() == ECLIPTIC) {
	    // glRotated(-(180.0/pi)*obleq_J2000().GetRad(),1,0,0);
	    planet_axis.rotate(0,-obleq_J2000().GetRad(),0);
	    planet_x_axis.rotate(0,-obleq_J2000().GetRad(),0);
	  }
	  
	  glColor3d(0,1,1);
	  glLineWidth(1.0);
	  gl2psLineWidth(1.0);
	  glBegin(GL_LINES);
	  glVertex3d(0,0,0);
	  glVertex3d(planet_axis.x,
		     planet_axis.y,
		     planet_axis.z);
	  glEnd();
	  //
	  glBegin(GL_LINES);
	  glVertex3d(0,0,0);
	  glVertex3d(planet_x_axis.x,
		     planet_x_axis.y,
		     planet_x_axis.z);
	  glEnd();
	}
      }
      
      // standard code...
      /* 
	 GLUquadricObj * qobj = gluNewQuadric();
	 gluQuadricDrawStyle(qobj,GLU_SILHOUETTE);
	 // gluQuadricDrawStyle(qobj,GLU_FILL);
	 gluQuadricNormals(qobj,GLU_SMOOTH);
	 gluSphere(qobj,evol_frame[j].radius(),36,18);
	 gluDeleteQuadric(qobj);
      */
      
      // }
      
      glPointSize(1);
      gl2psPointSize(1);
      //
      // glColor3d(1,0,0);
      //
      {
	const QColor c = planet_color(evol_frame[j].JPLPlanet());
	const double factor = 1.0/256.0;
	glColor3d(factor*c.red(),
		  factor*c.green(),
		  factor*c.blue());
      }
      //
      glBegin(GL_POINTS);
      glVertex3d(0,0,0);
      glEnd();
      
    } else {
      
      glPointSize(1);
      gl2psPointSize(1);
      glColor3d(0,0,1);
      glBegin(GL_POINTS);
      glVertex(p);
      glEnd();
    }
    
    glPopMatrix();
    
    /* if (draw_labels) {
       if (((evol_frame[j].mass() > 0) && (labels_mode == MASSIVE)) || 
       (labels_mode == ALL)) {
    */
    // glColor3d(0,0.7,0);
    
    // check, using the feedback rendering mode,
    // which labels I should print
    // ..OK, should use makers, using glPassThrough()...
    // this implementation is _too_ slow, and doesn't
    // really work as I expected: a point behind an object
    // is rendered
    /* 
       GLfloat feedback_buffer[1024];
       glFeedbackBuffer(1024,GL_3D,feedback_buffer);
       glRenderMode(GL_FEEDBACK);
       glBegin(GL_POINTS);
       glVertex(p);
       glEnd();
       const int size = glRenderMode(GL_RENDER);
       // cerr << "size: " << size << endl;
       if (size > 0) {
    */
    //
    QString text;
    text.sprintf(" %s",evol_frame[j].name().c_str());
    if ((int)j==center_body) {
      QFont fnt(font());
      fnt.setPixelSize(14);
      glColor3d(0.1,0.9,0.1);
      renderText(p,text,fnt);
    } else {
      glColor3d(0,1.0,0);
      renderText(p,text); 
    }
    // }
  }
}

/* 
   void XOrsaOpenGLEvolutionWidget::internal_draw_orbit_ellipse() {
   
   glEnable(GL_LINE_SMOOTH);
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA,GL_ONE);
   glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);
   glDepthMask(GL_FALSE);
   
   Orbit orbit;
   
   glPushMatrix();
   
   for (unsigned int j=0;j<evol_frame.size();++j) {
   
   if (evol_frame[j].mass() > 0.0) {
   glLineWidth(2.5);
   gl2psLineWidth(2.5);
   } else {
   glLineWidth(1.5);
   gl2psLineWidth(1.5);
   }
   
   if (orbit_reference_body_index==-HEX_AUTO) {
   if (((int)j)==orbit_reference_body_index_vector[evol_counter][j]) continue;
   } else {
   if (((int)j)==orbit_reference_body_index) continue;
   }
   
   Vector v_diff;
   if (orbit_reference_body_index==-HEX_AUTO) {
   // v_diff = evol_frame[orbit_reference_body_index_vector[evol_counter][j]].position()-center;
   v_diff = evol_frame[orbit_reference_body_index_vector[evol_counter][j]].position();
   } else {
   // v_diff = evol_frame[orbit_reference_body_index].position()-center;
   v_diff = evol_frame[orbit_reference_body_index].position();
   }
   
   // const GLdouble equation_z_plus[4]  = {0,0, 1,-v_diff.z};
   // const GLdouble equation_z_minus[4] = {0,0,-1, v_diff.z};
   const GLdouble equation_z_plus[4]  = {0,0, 1,-v_diff.z+CenterBodyPosition().z};
   const GLdouble equation_z_minus[4] = {0,0,-1, v_diff.z-CenterBodyPosition().z};
   //
   if (bright_positive_Z) {
   glClipPlane(GL_CLIP_PLANE0,equation_z_plus);
   glClipPlane(GL_CLIP_PLANE1,equation_z_minus);
   }
   
   glPushMatrix();
   
   // [x]
   // glTranslate(v_diff);
   glTranslate(v_diff-CenterBodyPosition());
   
   int ref_body_index;
   if (orbit_reference_body_index==-HEX_AUTO) {
   ref_body_index = orbit_reference_body_index_vector[evol_counter][j];
   } else {
   ref_body_index = orbit_reference_body_index;
   }
   
   if ( (orbit_cache_vector[evol_counter][j].set) &&
   (orbit_cache_vector[evol_counter][j].ref_body_index == ref_body_index) ) {
   
   // cerr << "usign orbit cache!" << endl;
   
   orbit = orbit_cache_vector[evol_counter][j].orbit;
   
   } else {
   
   OrbitCache orbit_cache;
   
   orbit.Compute(evol_frame[j],evol_frame[ref_body_index]);
   
   orbit_cache.orbit          = orbit;
   orbit_cache.ref_body_index = ref_body_index;
   orbit_cache.set            = true;
   
   orbit_cache_vector[evol_counter][j] = orbit_cache;
   }
   
   if (orbit.e<1.0) {
   
   glPushMatrix();	      
   //
   glRotated(orbit.omega_node*(180.0/pi),0,0,1);
   glRotated(orbit.i*(180.0/pi),1,0,0);
   glRotated(orbit.omega_pericenter*(180.0/pi),0,0,1);
   glTranslated(-orbit.a*orbit.e,0.0,0.0);
   glScaled(orbit.a,orbit.a*secure_sqrt(1-orbit.e*orbit.e),orbit.a);
   glRotated(orbit.GetE()*(180.0/pi),0,0,1);
   
   // select the appropriate circle
   //
   const int max_pixels_per_circle_element = 4; // should be tuned; the smaller this value, the better quality and more CPU
   // const int max_pixels_per_circle_element = 6; // should be tuned; the smaller this value, the better quality and more CPU
   // const int max_pixels_per_circle_element = 10; // should be tuned; the smaller this value, the better quality and more CPU
   //
   int appropriate_circle_index=0;
   switch(projection) {
   case OGL_ORTHO:
   // appropriate_circle_index = (int)rint(secure_log(orbit.a*(1.0+orbit.e)*pi/(max_pixels_per_circle_element*pixel_length))/secure_log(2.0)); // log base 2
   appropriate_circle_index = (int)rint(secure_log(2.0*pi*orbit.a*(1.0+orbit.e)/(max_pixels_per_circle_element*PixelLength()))/log(2.0)); // log base 2
   break;
   case OGL_PERSPECTIVE:
   
   const double eye_to_orbit_center_distance = (evol_frame[ref_body_index].position()-eye_position).Length();
   //
   // naif method, doesn't take into account real orbit inclination and orientation...
   double minimum_eye_to_orbit_distance = 0.0;
   const double q = (orbit.a*(1.0-orbit.e));
   const double Q = (orbit.a*(1.0+orbit.e));
   if (eye_to_orbit_center_distance < q) {
   minimum_eye_to_orbit_distance = q - eye_to_orbit_center_distance;
   } else if (eye_to_orbit_center_distance > Q) {
   minimum_eye_to_orbit_distance = eye_to_orbit_center_distance - Q;
   }
   //
   // cerr << "minimum_eye_to_orbit_distance: " << minimum_eye_to_orbit_distance << endl;
   //
   // debug
   // if (evol_frame[j].name() == "Moon") {
   
   if (minimum_eye_to_orbit_distance > 0.0) {
   appropriate_circle_index = (int)rint(secure_log(2.0*pi*orbit.a*(1.0+orbit.e)/(minimum_eye_to_orbit_distance*max_pixels_per_circle_element*(pi/180.0)*PixelLength()))/log(2.0)); // log base 2
   } else {
   appropriate_circle_index=max_circle_index-1;
   }
   
   break;
   }
   
   if (appropriate_circle_index <  0) appropriate_circle_index=0;
   if (appropriate_circle_index >= max_circle_index) appropriate_circle_index=max_circle_index-1;
   // 
   // cerr << evol_frame[j].name() << " ellipse index: " << appropriate_circle_index << endl;
   
   if (appropriate_circle_index > 1) {
   
   const GLint appropriate_circle = circle+appropriate_circle_index;
   
   if (bright_positive_Z) {
   glColor4d(0.2,0.2,0.2,1.0);
   glEnable(GL_CLIP_PLANE0);
   glCallList(appropriate_circle);
   glDisable(GL_CLIP_PLANE0);
   
   glColor4d(0.1,0.1,0.1,1.0);
   glEnable(GL_CLIP_PLANE1);
   glCallList(appropriate_circle);
   glDisable(GL_CLIP_PLANE1);
   } else {
   glColor4d(0.2,0.2,0.2,1.0);
   glCallList(appropriate_circle);
   }
   
   }
   
   glPopMatrix();
   }
   
   glPopMatrix();
   } 
   
   glPopMatrix();
   
   glDepthMask(GL_TRUE);   
   glDisable(GL_BLEND);
   glDisable(GL_LINE_SMOOTH);
   }
*/

void XOrsaOpenGLEvolutionWidget::internal_draw_orbit_on_ellipse() {
  
  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE);
  glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);
  glDepthMask(GL_FALSE);
  
  Orbit orbit;
  
  glPushMatrix();
  
  for (unsigned int j=0;j<evol_frame.size();++j) {
    
    if (evol_frame[j].mass() > 0.0) {
      glLineWidth(2.5);
      gl2psLineWidth(2.5);  
    } else {
      glLineWidth(1.5);
      gl2psLineWidth(1.5);
    }
    
    if (orbit_reference_body_index==-HEX_AUTO) {
      if (((int)j)==orbit_reference_body_index_vector[evol_counter][j]) continue;
    } else {
      if (((int)j)==orbit_reference_body_index) continue;
    }
    
    Vector v_diff;
    if (orbit_reference_body_index==-HEX_AUTO) {
      v_diff = evol_frame[orbit_reference_body_index_vector[evol_counter][j]].position();
    } else {
      v_diff = evol_frame[orbit_reference_body_index].position();
    }
    
    // const GLdouble equation_z_plus[4]  = {0,0, 1,-v_diff.z};
    // const GLdouble equation_z_minus[4] = {0,0,-1, v_diff.z};
    const GLdouble equation_z_plus[4]  = {0,0, 1,-v_diff.z+CenterBodyPosition().z};
    const GLdouble equation_z_minus[4] = {0,0,-1, v_diff.z-CenterBodyPosition().z};
    //
    if (bright_positive_Z) {
      glClipPlane(GL_CLIP_PLANE0,equation_z_plus);
      glClipPlane(GL_CLIP_PLANE1,equation_z_minus);
    }
    
    glPushMatrix();
    
    // [x]
    // glTranslate(v_diff);
    //
    // THIS WAS USED!
    // glTranslate(v_diff-CenterBodyPosition());
    //
    // test
    glTranslate(evol_frame[j].position()-CenterBodyPosition());
    
    int ref_body_index;
    if (orbit_reference_body_index==-HEX_AUTO) {
      ref_body_index = orbit_reference_body_index_vector[evol_counter][j];
    } else {
      ref_body_index = orbit_reference_body_index;
    }
    
    if ( (orbit_cache_vector[evol_counter][j].set) &&
	 (orbit_cache_vector[evol_counter][j].ref_body_index == ref_body_index) ) {
      
      // cerr << "usign orbit cache!" << endl;
      
      orbit = orbit_cache_vector[evol_counter][j].orbit;
      
    } else {
      
      OrbitCache orbit_cache;
      
      orbit.Compute(evol_frame[j],evol_frame[ref_body_index]);
      
      orbit_cache.orbit          = orbit;
      orbit_cache.ref_body_index = ref_body_index;
      orbit_cache.set            = true;
      
      orbit_cache_vector[evol_counter][j] = orbit_cache;
    }
    
    if (orbit.e<1.0) {
      
      glPushMatrix();	      
      //
      glRotated(orbit.omega_node*(180.0/pi),0,0,1);
      glRotated(orbit.i*(180.0/pi),1,0,0);
      glRotated(orbit.omega_pericenter*(180.0/pi),0,0,1);
      // glTranslated(-orbit.a*orbit.e,0.0,0.0);
      glScaled(orbit.a,orbit.a*secure_sqrt(1-orbit.e*orbit.e),orbit.a);
      glRotated(orbit.GetE()*(180.0/pi),0,0,1);
      
      // select the appropriate circle
      //
      const int max_pixels_per_circle_element = 4; // should be tuned; the smaller this value, the better quality and more CPU
      // const int max_pixels_per_circle_element = 6; // should be tuned; the smaller this value, the better quality and more CPU
      // const int max_pixels_per_circle_element = 10; // should be tuned; the smaller this value, the better quality and more CPU
      //
      int appropriate_on_circle_index=0;
      switch(projection) {
      case OGL_ORTHO:
	// appropriate_circle_index = (int)rint(secure_log(orbit.a*(1.0+orbit.e)*pi/(max_pixels_per_circle_element*pixel_length))/secure_log(2.0)); // log base 2
	appropriate_on_circle_index = (int)rint(secure_log(2.0*pi*orbit.a*(1.0+orbit.e)/(max_pixels_per_circle_element*PixelLength()))/log(2.0)); // log base 2
	break;
      case OGL_PERSPECTIVE:
	
	const double eye_to_orbit_center_distance = (evol_frame[ref_body_index].position()-eye_position).Length();
	//
	// naif method, doesn't take into account real orbit inclination and orientation...
	double minimum_eye_to_orbit_distance = 0.0;
	const double q = (orbit.a*(1.0-orbit.e));
	const double Q = (orbit.a*(1.0+orbit.e));
	if (eye_to_orbit_center_distance < q) {
	  minimum_eye_to_orbit_distance = q - eye_to_orbit_center_distance;
	} else if (eye_to_orbit_center_distance > Q) {
	  minimum_eye_to_orbit_distance = eye_to_orbit_center_distance - Q;
	}
	//
	// cerr << "minimum_eye_to_orbit_distance: " << minimum_eye_to_orbit_distance << endl;
	//
	// debug
	// if (evol_frame[j].name() == "Moon") {
	/* if ((int)j == center_body) {
	   cerr << "name: " << evol_frame[j].name() << endl;
	   cerr << "ref_body_index: " << ref_body_index << endl;
	   cerr << "ref_body name: " << evol_frame[ref_body_index].name() << endl;
	   //
	   {
	   // debug
	   const Vector ref_body_pos = evol_frame[ref_body_index].position();
	   cerr << "ref_body position: " << ref_body_pos.x << " " << ref_body_pos.y << " " << ref_body_pos.z << endl;
	   }
	   //
	   cerr << "distance: " << distance << endl;
	   cerr << "eye_to_orbit_center_distance: " << eye_to_orbit_center_distance << endl;
	   cerr << "minimum_eye_to_orbit_distance: " << minimum_eye_to_orbit_distance << endl;
	   }
	*/
	//
	if (minimum_eye_to_orbit_distance > 0.0) {
	  appropriate_on_circle_index = (int)rint(secure_log(2.0*pi*orbit.a*(1.0+orbit.e)/(minimum_eye_to_orbit_distance*max_pixels_per_circle_element*(pi/180.0)*PixelLength()))/log(2.0)); // log base 2
	} else {
	  appropriate_on_circle_index=max_circle_index-1;
	}
	
	break;
      }
      
      if (appropriate_on_circle_index <  0) appropriate_on_circle_index=0;
      if (appropriate_on_circle_index >= max_circle_index) appropriate_on_circle_index=max_circle_index-1;
      
      if (appropriate_on_circle_index > 1) {
	
	const GLint appropriate_on_circle = on_circle+appropriate_on_circle_index;
	
	if (bright_positive_Z) {
	  glColor4d(0.2,0.2,0.2,1.0);
	  glEnable(GL_CLIP_PLANE0);
	  glCallList(appropriate_on_circle);
	  glDisable(GL_CLIP_PLANE0);
	  
	  glColor4d(0.1,0.1,0.1,1.0);
	  glEnable(GL_CLIP_PLANE1);
	  glCallList(appropriate_on_circle);
	  glDisable(GL_CLIP_PLANE1);
	} else {
	  glColor4d(0.2,0.2,0.2,1.0);
	  glCallList(appropriate_on_circle);
	}
	
      }
      
      glPopMatrix();
    }
    
    glPopMatrix();
  } 
  
  glPopMatrix();
  
  glDepthMask(GL_TRUE);   
  glDisable(GL_BLEND);
  glDisable(GL_LINE_SMOOTH);
}

void XOrsaOpenGLEvolutionWidget::internal_draw_MOID() {
#ifdef HAVE_GSL	
  
  if (draw_orbits)
    if (evolution != 0)
      if (evolution->size() != 0)
     	if ((evol_frame.size() >= 3) && (MOID_body_1!=MOID_body_2)) {
	  
	  if (orbit_reference_body_index==-HEX_AUTO) {
	    if (orbit_reference_body_index_vector[evol_counter].size()==0) return;
	    if (MOID_body_1==orbit_reference_body_index_vector[evol_counter][MOID_body_1]) return;
	    if (MOID_body_2==orbit_reference_body_index_vector[evol_counter][MOID_body_2]) return;
	  } else {
	    if (MOID_body_1==orbit_reference_body_index) return;
	    if (MOID_body_2==orbit_reference_body_index) return;
	  }
	  
	  // gl* stuff here, after all the possible returns...  
	  glEnable(GL_LINE_SMOOTH);
	  glEnable(GL_BLEND);
	  glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	  glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);
	  glDepthMask(GL_FALSE);
	  
	  Orbit  o_1, o_2;
	  Vector r_1, r_2;
	  
	  if (orbit_reference_body_index==-HEX_AUTO) {
	    o_1.Compute(evol_frame[MOID_body_1],evol_frame[orbit_reference_body_index_vector[evol_counter][MOID_body_1]]);
	    o_2.Compute(evol_frame[MOID_body_2],evol_frame[orbit_reference_body_index_vector[evol_counter][MOID_body_2]]);
	  } else {
	    o_1.Compute(evol_frame[MOID_body_1],evol_frame[orbit_reference_body_index]);
	    o_2.Compute(evol_frame[MOID_body_2],evol_frame[orbit_reference_body_index]);
	  }
	  
	  // really needed the eccentricity check?	
	  // if ((o_1.e < 1.0) && (o_2.e < 1.0)) {
	  
	  double _MOID;
	  if ((orbit_reference_body_index==-HEX_AUTO) && (orbit_reference_body_index_vector[evol_counter][MOID_body_1]!=orbit_reference_body_index_vector[evol_counter][MOID_body_2])) { 
	    _MOID = MOID2RB(evol_frame[orbit_reference_body_index_vector[evol_counter][MOID_body_1]].position(),
			    evol_frame[orbit_reference_body_index_vector[evol_counter][MOID_body_2]].position(),
			    o_1,o_2,r_1,r_2);
	  } else {
	    _MOID = MOID(o_1,o_2,r_1,r_2);
	  }	
	  
	  // cerr << "MOID between bodies 1 and 2: " << _MOID << endl;
	  // cerr << "r1: " << r1.x << "  " << r1.y << "  " << r1.z << endl; 
	  // cerr << "r2: " << r2.x << "  " << r2.y << "  " << r2.z << endl; 
	  
	  Vector z1,z2;
	  if (orbit_reference_body_index==-HEX_AUTO) {
	    // z1 = evol_frame[orbit_reference_body_index_vector[evol_counter][MOID_body_1]].position() - center;
	    // z2 = evol_frame[orbit_reference_body_index_vector[evol_counter][MOID_body_2]].position() - center;
	    z1 = evol_frame[orbit_reference_body_index_vector[evol_counter][MOID_body_1]].position();
	    z2 = evol_frame[orbit_reference_body_index_vector[evol_counter][MOID_body_2]].position();
	  } else {
	    // z1 = z2 = evol_frame[orbit_reference_body_index].position() - center;
	    z1 = z2 = evol_frame[orbit_reference_body_index].position();
	  }
	  
	  r_1 += z1;
	  r_2 += z2;
	  
	  // [x]
	  r_1 -= CenterBodyPosition();
	  r_2 -= CenterBodyPosition();
	  
	  glColor3d(0,1,1);
	  glLineWidth(1.0);
	  gl2psLineWidth(1.0);
	  // glLineStipple(1, 0x00FF);
	  // glEnable(GL_LINE_STIPPLE);
	  glBegin(GL_LINES);
	  glVertex3d(r_1.x,r_1.y,r_1.z);
	  glVertex3d(r_2.x,r_2.y,r_2.z);
	  glEnd();
	  // glDisable(GL_LINE_STIPPLE);
	  
	  char t[1024];
	  // sprintf(t," MOID: %.3g %s",_MOID,LengthLabel().c_str());
	  
	  length_unit lu = AutoLengthUnit(_MOID);
	  // sprintf(t," MOID: %.3g %s",_MOID,LengthLabel().c_str());
	  sprintf(t," MOID: %.3g %s",FromUnits(_MOID,lu,-1),units->label(lu).c_str());
	  
	  // string dpy_name = t;
	  Vector m = (r_1+r_2)/2;
	  // glRasterPos3d(m.x,m.y,m.z);
	  // glCallLists(dpy_name.size(),GL_BYTE,(GLubyte*) dpy_name.c_str());
	  // renderText(m.x,m.y,m.z,t,default_font); 
	  renderText(m,t); 
	  
	  // } 
	}
  
  glDepthMask(GL_TRUE);   
  glDisable(GL_BLEND);
  glDisable(GL_LINE_SMOOTH);
  
#endif // HAVE_GSL   
}

/* 
   void XOrsaOpenGLEvolutionWidget::generate_object_names() {
   // test (needed?)
   // renderText(0,0,"",default_font);
   //
   const Frame * b = bodies();
   const unsigned int size = b->size();
   string display_name;
   object_names_base_list = glGenLists(size);
   cerr << "object_names_base_list: " << object_names_base_list << endl;
   for (unsigned int k=0;k<size;++k) {
   display_name = " " + (*b)[k].name();
   glNewList(object_names_base_list+k, GL_COMPILE);
   glCallLists(display_name.size(),GL_BYTE,(GLubyte*) display_name.c_str());
   glEndList();
   cerr << "created list: " << display_name << endl;
   }
   }
*/


void XOrsaOpenGLEvolutionWidget::internal_draw_Lagrange_points() {
  
  if (Lagrange_body_1 == Lagrange_body_2) return;
  
  const double m_1 = evol_frame[Lagrange_body_1].mass();
  const double m_2 = evol_frame[Lagrange_body_2].mass();
  
  // assume m1 > m2
  double m1, m2;
  Vector tmp_x,tmp_y;
  Vector x1;
  if (m_1>m_2) {
    m1 = m_1;
    m2 = m_2;
    // x1 = - dir_pos_mod - evol_frame[Lagrange_body_1].position();   
    x1 = CenterBodyPosition() - evol_frame[Lagrange_body_1].position();   
    tmp_x  = evol_frame[Lagrange_body_2].position() - evol_frame[Lagrange_body_1].position();
    tmp_y  = evol_frame[Lagrange_body_2].velocity() - evol_frame[Lagrange_body_1].velocity();
    tmp_y /= tmp_y.Length();
    double tmp = tmp_y*tmp_x/tmp_x.Length();
    tmp_y -= tmp*tmp_x/tmp_x.Length();
    tmp_y /= tmp_y.Length();
    tmp_y *= tmp_x.Length();
    // cerr << " diff: " << tmp << " --> " << tmp_y*tmp_x << endl;
  } else {
    m1 = m_2;
    m2 = m_1;
    // x1 = - dir_pos_mod - evol_frame[Lagrange_body_2].position();
    x1 = CenterBodyPosition() - evol_frame[Lagrange_body_2].position();
    tmp_x  = evol_frame[Lagrange_body_1].position() - evol_frame[Lagrange_body_2].position();
    tmp_y  = evol_frame[Lagrange_body_1].velocity() - evol_frame[Lagrange_body_2].velocity();
    tmp_y /= tmp_y.Length();
    double tmp = tmp_y*tmp_x/tmp_x.Length();
    tmp_y -= tmp*tmp_x/tmp_x.Length();
    tmp_y /= tmp_y.Length();
    tmp_y *= tmp_x.Length();
    // cerr << " diff: " << tmp << " --> " << tmp_y*tmp_x << endl;
  }
  
  const Vector x = tmp_x;
  const Vector y = tmp_y;
  
  const double mu_1  = m1/(m1+m2); 
  const double mu_2  = m2/(m1+m2); 
  const double mu_21 = mu_2/mu_1;
  const double alpha = secure_pow(mu_2/(3*mu_1),1.0/3.0);
  
  double r1, r2;
  
  // L1
  r2 = alpha - (1.0/3.0)*secure_pow(alpha,2) - (1.0/9.0)*secure_pow(alpha,3) - (23.0/81.0)*secure_pow(alpha,4); 
  r1 = 1.0 - r2;
  const Vector L1 = r1 * x - x1;
  
  // L2 
  r2 = alpha + (1.0/3.0)*secure_pow(alpha,2) - (1.0/9.0)*secure_pow(alpha,3) - (31.0/81.0)*secure_pow(alpha,4); 
  r1 = 1.0 + r2;
  const Vector L2 = r1 * x - x1;
  
  // L3
  const double beta = - (7.0/12.0)*mu_21 + (7.0/12.0)*secure_pow(mu_21,2) - (13223.0/20736.0)*secure_pow(mu_21,3);
  r1 = 1.0 + beta;
  r2 = 2.0 + beta;
  const Vector L3 = - r1 * x - x1;
  
  // L4
  const Vector L4 = (0.5-mu_2)*x + secure_sqrt(3.0)/2.0*y - x1;
  
  // L5
  const Vector L5 = (0.5-mu_2)*x - secure_sqrt(3.0)/2.0*y - x1;
  
  // render Lagrangian points
  string L_name;
  
  // glColor3d(0,0.7,0);
  // glColor3d(0.6,0.4,0.0);
  glColor3d(1.0,0.6,0.0);
  glPointSize(2);
  gl2psPointSize(2);
  
  QString text;
  
  L_name = ("L1");
  glBegin(GL_POINTS);
  glVertex3d(L1.x, L1.y, L1.z);
  glEnd();
  // glRasterPos3d(L1.x,L1.y,L1.z);
  // glCallLists(L_name.size(),GL_BYTE,(GLubyte*) L_name.c_str());
  text.sprintf(" %s",L_name.c_str());
  // renderText(L1.x, L1.y, L1.z,text,default_font); 
  renderText(L1,text); 
  
  L_name = ("L2");
  glBegin(GL_POINTS);
  glVertex3d(L2.x, L2.y, L2.z);
  glEnd();
  // glRasterPos3d(L2.x,L2.y,L2.z);
  // glCallLists(L_name.size(),GL_BYTE,(GLubyte*) L_name.c_str());
  text.sprintf(" %s",L_name.c_str());
  // renderText(L2.x, L2.y, L2.z,text,default_font); 
  renderText(L2,text); 
  
  L_name = ("L3");
  glBegin(GL_POINTS);
  glVertex3d(L3.x, L3.y, L3.z);
  glEnd();
  // glRasterPos3d(L3.x,L3.y,L3.z);
  // glCallLists(L_name.size(),GL_BYTE,(GLubyte*) L_name.c_str());
  text.sprintf(" %s",L_name.c_str());
  // renderText(L3.x, L3.y, L3.z,text,default_font); 
  renderText(L3,text); 
  
  L_name = ("L4");
  glBegin(GL_POINTS);
  glVertex3d(L4.x, L4.y, L4.z);
  glEnd();
  // glRasterPos3d(L4.x,L4.y,L4.z);
  // glCallLists(L_name.size(),GL_BYTE,(GLubyte*) L_name.c_str());
  text.sprintf(" %s",L_name.c_str());
  // renderText(L4.x, L4.y, L4.z,text,default_font); 
  renderText(L4,text); 
  
  L_name = ("L5");
  glBegin(GL_POINTS);
  glVertex3d(L5.x, L5.y, L5.z);
  glEnd();
  // glRasterPos3d(L5.x,L5.y,L5.z);
  // glCallLists(L_name.size(),GL_BYTE,(GLubyte*) L_name.c_str());
  text.sprintf(" %s",L_name.c_str());
  // renderText(L5.x, L5.y, L5.z,text,default_font); 
  renderText(L5,text); 
}

void XOrsaOpenGLEvolutionWidget::internal_draw_OSD() {
  OSD osd(this);
  osd.draw();
}

// OSD

void OSD::draw() {
  
  GLint saved_depth_func;
  glGetIntegerv(GL_DEPTH_FUNC,&saved_depth_func);
  glDepthFunc(GL_ALWAYS);
  
  // OSD text color
  // glColor3d(0,1.0,0);  
  glColor3d(0.5,1.0,1.0);  
  
  // set writing zone
  zone = TOP_LEFT;
  
  // time
  {
    QString time_label;
    //
    switch (universe->GetUniverseType()) {
    case Real: {
      // FineDate(time_label,opengl->evol_frame.GetDate());
      FineDate_HMS(time_label,opengl->evol_frame.GetDate());
      time_label.prepend("time: ");
      time_label.append(" ");
      time_label.append(TimeScaleLabel(universe->GetTimeScale()).c_str());
      break;
    }
    case Simulated: 
      time_label.sprintf("time: %-6g %s",opengl->evol_frame.Time(),TimeLabel().c_str());
      break;
    }
    //
    write(time_label);
  }
  
  // center body
  {
    QString text;
    text.sprintf("central object: %s",opengl->CenterBodyName().c_str());
    write(text); 
    
    if (opengl->projection == OGL_PERSPECTIVE) {
      length_unit lu = AutoLengthUnit((double)opengl->distance);
      text.sprintf("distance from %s: %g %s",opengl->CenterBodyName().c_str(),FromUnits((double)opengl->distance,lu,-1),units->label(lu).c_str());
      write(text); 
    }
  }
  
  // eye
  {
    QString text;
    if (opengl->eye_on_body) {
      text.sprintf("observing from %s",opengl->EyeBodyName().c_str());
    } else {
      text.sprintf("observer position: free");
    }
    write(text); 
  }
  
  zone = TOP_RIGHT;
  QString text;
  text.sprintf("FPS: %.1f",opengl->get_fps());
  write(text); 
  
  glDepthFunc(saved_depth_func);
}

//

// default colors, when a texture is not available
QColor planet_color(const orsa::JPL_planets p) {
  
  QColor c(255,255,255);
  
  switch(p) {
  case SUN:     c.setRgb(255,255,  0); break;
  case MERCURY: c.setRgb(255,  0,  0); break;
  case VENUS:   break;
  case EARTH:   c.setRgb( 46,108,216); break;
  case MARS:    c.setRgb(216, 38,  8); break;
  case JUPITER: c.setRgb(255,185, 39); break;
  case SATURN:  break;
  case URANUS:  break;
  case NEPTUNE: c.setRgb(105,170,224); break;
  case PLUTO:   break;
    //
  case MOON:    c.setRgb(235,235,235); break;
    //
  default: break;
  }
  
  return c;
}
