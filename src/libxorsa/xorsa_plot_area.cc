/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_plot_area.h"

#include <qcursor.h>
#include <qpainter.h>
#include <qdrawutil.h> 
#include <qpixmap.h> 
#include <qprinter.h>
#include <qpaintdevicemetrics.h> 
#include <qpoint.h> 
#include <qcolor.h> 
#include <qlayout.h> 
#include <qcheckbox.h> 
#include <qframe.h>
#include <qpushbutton.h>
#include <qfontmetrics.h>
#include <qsimplerichtext.h> 
#include <qregion.h> 
#include <qfiledialog.h> 
#include <qtoolbutton.h> 

#include <orsa_secure_math.h>
#include <orsa_version.h>
#include <orsa_error.h>

#include <iostream>

#include <cmath>

#ifdef _WIN32
#include "support.h"
#endif

// icons
#include "filesave.xpm"
#include "print.xpm"

using namespace std;
using namespace orsa;

void FineDate(QString & label, const UniverseTypeAwareTime & t, bool plot_label) {
  if (universe->GetUniverseType() == Real) {
    int     y,m,d;
    Date    date = t.GetDate();
    date.GetGregor(y,m,d);
    //
    // NOTE: check for the accuracy in the printf's below!! (i.e. 1.0e-5 for 5 fractional day digits)
    // if ((1.0-date.GetDayFraction()) < fabs(date.GetTime())*std::numeric_limits<double>::epsilon()) {
    // if ((1.0-date.GetDayFraction()) < MAX(1.0e-5,fabs(date.GetTime())*std::numeric_limits<double>::epsilon())) {
    if ( (date.GetDayFraction() < 0) ||
	 (date.GetDayFraction() >= 1.0) ) {
      ORSA_ERROR("Hmmmm.... strange day fraction: %i %i %i     date.GetDayFraction() = %e",y,m,d,date.GetDayFraction());
    }
    
    /* 
       if ((1.0-date.GetDayFraction()) < MAX(1.0e-5,fabs(date.GetTime())*std::numeric_limits<double>::epsilon())) {
       ORSA_WARNING("correcting date: %i %i %i     (1.0-date.GetDayFraction()): %e      fabs(date.GetTime())*std::numeric_limits<double>::epsilon(): %e",y,m,d,(1.0-date.GetDayFraction()),fabs(date.GetTime())*std::numeric_limits<double>::epsilon());
       date.SetGregor(y,m,d+1);
       }
    */
    
    // five decimal points
    if (plot_label) {
      
      if (d<10) {
	// label.sprintf("%i %s %.6g",y,MonthNameShort[m],d+date.GetDayFraction());
   	label.sprintf("%i %s %.6g",y,MonthNameShort[m],d+1.0e-5*((int)floor(100000*date.GetDayFraction())));
      } else {
	label.sprintf("%i %s %.7g",y,MonthNameShort[m],d+1.0e-5*((int)floor(100000*date.GetDayFraction())));
      }
      
    } else {
      // label.sprintf("%i %s %.5f",y,MonthNameShort[m],d+date.GetDayFraction());
      label.sprintf("%i %s %i.%05i",y,MonthNameShort[m],d,(int)floor(100000*date.GetDayFraction())); // to avoid Dec 32s...
    }
  } else {
    // label.sprintf("%g",t.Time());
    FineLabel(label, t.Time());
  }
}

void FineDate_HMS(QString & label, const orsa::UniverseTypeAwareTime & t) {
   if (universe->GetUniverseType() == Real) {
    int     y,m,d;
    Date    date = t.GetDate();
    date.GetGregor(y,m,d);
    if ( (date.GetDayFraction() < 0) ||
	 (date.GetDayFraction() >= 1.0) ) {
      ORSA_ERROR("Hmmmm.... strange day fraction: %i %i %i     date.GetDayFraction() = %e",y,m,d,date.GetDayFraction());
    }
    
    unsigned int frac_ui = date.GetDayFraction_unsigned_int();
    const unsigned int max_frac_ui = TimeStep::max_day_fraction();
    //
    const unsigned int H_mod = max_frac_ui / 24;
    const unsigned int M_mod = max_frac_ui / 1440;
    const unsigned int S_mod = max_frac_ui / 86400;
    //
    const unsigned int H = frac_ui / H_mod;
    frac_ui -= H * H_mod;
    //
    const unsigned int M = frac_ui / M_mod;
    frac_ui -= M * M_mod;
    //
    const unsigned int S = frac_ui / S_mod;
    // frac_ui -= S * S_mod;
    
    label.sprintf("%i %s %i %02i:%02i:%02i",y,MonthNameShort[m],d,H,M,S);
    
   } else {
     FineLabel(label, t.Time());
   }
}

length_unit AutoLengthUnit(const double x) {
  list<length_unit> ll;
  ll.push_back(MPARSEC);
  ll.push_back(KPARSEC);
  ll.push_back(PARSEC);
  ll.push_back(LY);
  ll.push_back(AU);
  ll.push_back(EARTHMOON);
  ll.push_back(REARTH);
  // ll.push_back(RMOON);
  ll.push_back(KM);
  ll.push_back(M);
  
  list<length_unit>::iterator ii_it = ll.begin();
  length_unit lu = (*ii_it);
  
  length_unit min_lu = lu;
  double      min    = fabs(secure_log10(FromUnits(fabs(x),lu,-1)));
  double tmp;
  ++ii_it;
  do {
    lu = (*ii_it);
    tmp = fabs(secure_log10(FromUnits(fabs(x),lu,-1)));
    if (tmp < min) {
      min    = tmp;
      min_lu = lu;
    }
    ++ii_it;    
  } while (ii_it != ll.end());
  
  return min_lu;
}


time_unit AutoTimeUnit(const double x) {
  list<time_unit> tl;
  tl.push_back(YEAR);
  tl.push_back(DAY);
  tl.push_back(HOUR);
  tl.push_back(MINUTE);
  tl.push_back(SECOND);
  
  list<time_unit>::iterator ii_it = tl.begin();
  time_unit tu = (*ii_it);
  
  time_unit min_tu = tu;
  double min = fabs(secure_log10(FromUnits(fabs(x),tu,-1)));
  double tmp;
  ++ii_it;
  do {
    tu = (*ii_it);
    tmp = fabs(secure_log10(FromUnits(fabs(x),tu,-1)));
    if (tmp < min) {
      min    = tmp;
      min_tu = tu;
    }
    ++ii_it;    
  } while (ii_it != tl.end());
  
  return min_tu;
}


mass_unit AutoMassUnit(const double x) {
  list<mass_unit> ml;
  ml.push_back(MSUN);
  ml.push_back(MJUPITER);
  ml.push_back(MEARTH);
  ml.push_back(MMOON);
  ml.push_back(KG);
  ml.push_back(GRAM);
  
  list<mass_unit>::iterator ii_it = ml.begin();
  mass_unit mu = (*ii_it);
  
  mass_unit min_mu = mu;
  double min = fabs(secure_log10(FromUnits(fabs(x),mu,-1)));
  double tmp;
  ++ii_it;
  do {
    mu = (*ii_it);
    tmp = fabs(secure_log10(FromUnits(fabs(x),mu,-1)));
    if (tmp < min) {
      min    = tmp;
      min_mu = mu;
    }
    ++ii_it;    
  } while (ii_it != ml.end());
  
  return min_mu;
}


bool operator == (const XOrsaPlotPoint &a, const XOrsaPlotPoint &b) {
  if (a.x != b.x) return false;
  if (a.y != b.y) return false;
  return true;
}


double coefficient(const double x, const unsigned int round_digits=1) {
  const double l = secure_log10(x);
  // const double l = secure_log10(fabs(x));
  const double t = secure_pow(10.0,floor(l)-round_digits+1);
  const double c = x / t;
  return (ceil(c)*t);  
}


// for log axis
double coefficient_log_axis_label(const double x, const unsigned int round_digits=1) {
  const double l = secure_log10(fabs(x));
  double t = secure_pow(10.0,floor(l)-round_digits+1);
  if (x < 0) t *= -1;
  const double c = fabs(x/t);
  
  // cannot return 1.0!!
  if (c <= 10.) return (10.  * t);
  if (c >  10.) return (100. * t);
  
  // should never happen
  return x; 
}


double coefficient_axis_label(const double x, const unsigned int round_digits=1) {
  const double l = secure_log10(fabs(x));
  double t = secure_pow(10.0,floor(l)-round_digits+1);
  if (x < 0) t *= -1;
  const double c = fabs(x/t);
  
  if (c <= 1.0) return (1.0 * t);
  if (c <= 2.0) return (2.0 * t);
  if (c <= 5.0) return (5.0 * t);
  if (c <= 10.) return (10. * t);
  if (c >  10.) return (20. * t);
  
  // should never happen
  return x;
}


// for date axis
/* 
   double coefficient_date_axis_label(const double x, const unsigned int round_digits=1) {
   
   UniverseTypeAwareTime utat(x);
   Date date = utat.GetDate();
   //
   int     y,m,d;
   date.GetGregor(y,m,d);
   date.SetGregor(y,1,1);
   //
   utat.SetDate(date);
   //
   date.GetGregor(y,m,d);
   cerr << "coefficient_date_axis_label: x=" << x << " date passed " << y << " " << m << " " << d << " time: " << utat.Time() << endl;
   //
   return (utat.Time());
   
   // should never happen
   return x; 
   }
*/

/* 
   double coefficient_date_axis_label_step(const double x, const unsigned int round_digits=1) {
   
   const double y = FromUnits(x,YEAR,   -1);
   const double m = FromUnits(x*12,YEAR,-1);
   const double d = FromUnits(x,DAY,    -1);
   const double H = FromUnits(x,HOUR,   -1);
   const double M = FromUnits(x,MINUTE, -1);
   const double S = FromUnits(x,SECOND, -1);
   
   cerr << "coefficient_date_axis_label_step: y=" << y << "  m=" << m << "  d=" << d << endl;
   
   const double l = secure_log10(fabs(x));
   double t = secure_pow(10.0,floor(l)-round_digits+1);
   if (x < 0) t *= -1;
   const double c = fabs(x/t);
   
   if (y >= 1.0) {
   
   if (c <= 1.0) return (1.0 * t);
   if (c <= 2.0) return (2.0 * t);
   if (c <= 5.0) return (5.0 * t);
   if (c <= 10.) return (10. * t);
   if (c >  10.) return (20. * t);
   
   } else {
   
   if (m>=1) return (1.0 * t)/(12);
   if (d>=1) return (1.0 * t)/(365.25);
   if (H>=1) return (1.0 * t)/(365.25*24);
   if (M>=1) return (1.0 * t)/(365.25*24*60);
   if (S>=1) return (1.0 * t)/(365.25*24*60*60);
   
   if (S<1)  return (1.0 * t)/(365.25*24*60*60);
   }
   
   // should never happen
   return x;
   }
*/

// utility function
XOrsaPlotAxisDateStepUnit select_date_step(double &s, double &os) {
  
  // const double s_DAY  = FromUnits(s,DAY,-1);
  // const double s_YEAR = FromUnits(s,YEAR,-1);
  const double s_DAY  = coefficient_axis_label(FromUnits(s,DAY,-1));
  const double s_YEAR = coefficient_axis_label(FromUnits(s,YEAR,-1));
  
  // cerr << "s_DAY: " << s_DAY << "  s_YEAR: " << s_YEAR << endl;
  
  if (s_DAY <= 0.5) {
    // minimum: 1 second:
    if (s_DAY < 1.0e-5) {
      os = 1.0e-5;
    } else {
      os = s_DAY;
    }
    // os = coefficient_axis_label(s_DAY);
    // os = s_DAY;
    s  = FromUnits(os,DAY);
    // cerr << "select_date_step: DS_FRAC_DAY" << endl;
    return DS_FRAC_DAY;
  } else if (s_DAY <= 1.0) {
    os = 1.0;
    s  = FromUnits(os,DAY);
    // cerr << "select_date_step: DS_DAY" << endl;
    return DS_DAY;
  } else if (s_DAY <= 15.0) {
    // os = coefficient_axis_label(s_DAY); // should build a coefficient_date_axis_label? (1,2,3,5,7,10,15)
    os = s_DAY;
    s  = FromUnits(os,DAY);
    // cerr << "select_date_step: DS_DAY" << endl;
    return DS_DAY;
  } else if (s_DAY <= 31.0) {
    os = 1.0;
    s  = FromUnits(os*31,DAY); // approximative...
    // cerr << "select_date_step: DS_MONTH" << endl;
    return DS_MONTH;
  } else if (s_YEAR <= 0.5) {
    // os = coefficient_axis_label(s_YEAR);
    os = coefficient_axis_label(12*s_YEAR);
    s  = FromUnits(os,YEAR);
    // cerr << "select_date_step: DS_MONTH" << endl;
    return DS_MONTH;
  } else if (s_YEAR <= 1.0) {
    os = 1.0;
    s  = FromUnits(os,YEAR);
    // cerr << "select_date_step: DS_YEAR" << endl;
    return DS_YEAR;
  } else {
    // os = coefficient_axis_label(s_YEAR);
    os = s_YEAR;
    s  = FromUnits(os,YEAR);
    // cerr << "select_date_step: DS_YEAR" << endl;
    return DS_YEAR;
  }  
}


double date_tick(const int tick, XOrsaPlotAxisDateStepUnit ds, double x, double os) {
  Date date;
  date.SetTime(x);
  int y,m,d;
  date.GetGregor(y,m,d);
  double fd=date.GetDayFraction();
  /* if ((1.0-date.GetDayFraction())<FromUnits(1,SECOND)) {
     fd=0.0;
     if (m==12) {
     date.SetGregor(y+1,1,1);
     } else {
     date.SetGregor(y,m,d+1);
     }
     date.GetGregor(y,m,d);
     }
  */
  /* ***
     if ((1.0-fd)<FromUnits(1.0e-5,DAY)) {
     date.SetGregor(y,m,d);
     date.SetTime(date.GetTime()+FromUnits(1,DAY));
     date.GetGregor(y,m,d);
     fd=date.GetDayFraction();
     }
  */
  // double jd=date.GetJulian();
  // cerr << "date_tick: y=" << y << " m=" << m << " d=" << d+fd << endl; 
  // cerr << "date_tick: x=" << x << " os: " << os << " tick: " << tick << endl;
  switch (ds) {
  case DS_YEAR:
    y+=(int)rint(tick*os);
    m=1;
    d=1;
    fd=0.0;
    break;
  case DS_MONTH:
    m+=(int)rint(tick*os); 
    while (m>12) { ++y; m -= 12; }
    while (m<1)  { --y; m += 12; }
    d=1;
    fd=0.0;
    break;
  case DS_DAY:
    date.SetTime(x+FromUnits(tick*os,DAY));
    date.GetGregor(y,m,d);
    fd=0.0;
    break;
  case DS_FRAC_DAY:
    date.SetTime(x+FromUnits(tick*os,DAY));
    date.GetGregor(y,m,d);
    fd=date.GetDayFraction();
    // check for 'almost' next day
    /* ***
       if ((1.0-fd)<FromUnits(1.0e-5,DAY)) {
       date.SetGregor(y,m,d);
       date.SetTime(date.GetTime()+FromUnits(1,DAY));
       date.GetGregor(y,m,d);
       fd=date.GetDayFraction();
       }
    */
    break;
  }
  
  date.SetGregor(y,m,d+fd);
  //
  date.GetGregor(y,m,d);
  fd=date.GetDayFraction();
  // cerr << "date_tick: returning " << date.GetTime() << "  y=" << y << " m=" << m << " d=" << d+fd << endl; 
  //
  return date.GetTime();
}

/* 
   double date_center(XOrsaPlotAxisDateStepUnit ds, double x, double os) {
   Date date;
   date.SetTime(x);
   int y,m,d;
   date.GetGregor(y,m,d);
   double fd=date.GetDayFraction();
   switch (ds) {
   case DS_YEAR:
   m=1;
   d=1;
   fd=0.0;
   break;
   case DS_MONTH:
   d=1;
   fd=0.0;
   break;
   case DS_DAY:
   fd=0.0;
   break;
   case DS_FRAC_DAY:
   fd=rint(x/os)*os;
   break;
   }
   
   date.SetGregor(y,m,d+fd);
   return date.GetTime();
   }
*/

double date_center(XOrsaPlotAxisDateStepUnit ds, double x, double os) {
  // cerr << "date_center..." << endl;
  Date date;
  date.SetTime(x);
  int y,m,d;
  date.GetGregor(y,m,d);
  double fd=date.GetDayFraction();
  // check for 'almost' next day
  /* if ((1.0-fd)<FromUnits(1,SECOND)) {
     fd=0.0;
     if (m==12) {
     date.SetGregor(y+1,1,1);
     } else {
     date.SetGregor(y,m,d+1);
     }
     date.GetGregor(y,m,d);
     }
  */
  /* ***
     if ((1.0-fd)<FromUnits(1.0e-5,DAY)) {
     date.SetGregor(y,m,d);
     date.SetTime(date.GetTime()+FromUnits(1,DAY));
     date.GetGregor(y,m,d);
     fd=date.GetDayFraction();
     }
  */
  // double jd=date.GetJulian();
  // cerr << "date_center: y=" << y << " m=" << m << " d=" << d+fd << endl; 
  // cerr << "date_center: x=" << x << " os: " << os << endl;
  switch (ds) {
  case DS_YEAR:
    y=(int)rint(rint(y/os)*os);
    m=1;
    d=1;
    fd=0.0;
    break;
  case DS_MONTH:
    m=1;
    d=1;
    fd=0.0;
    break;
  case DS_DAY:
    fd=0.0;
    break;
  case DS_FRAC_DAY:
    fd=rint(fd/os)*os;
    // check for 'almost' next day
    /* if ((1.0-fd)<FromUnits(1,SECOND)) {
       fd=0.0;
       if (m==12) {
       date.SetGregor(y+1,1,1);
       } else {
       date.SetGregor(y,m,d+1);
       }
       date.GetGregor(y,m,d);
       }
    */
    /* ***
       if ((1.0-fd)<FromUnits(1.0e-5,DAY)) {
       date.SetGregor(y,m,d);
       date.SetTime(date.GetTime()+FromUnits(1,DAY));
       date.GetGregor(y,m,d);
       fd=date.GetDayFraction();
       }
    */
    break;
  }
  
  date.SetGregor(y,m,d+fd);
  //
  date.GetGregor(y,m,d);
  fd=date.GetDayFraction();
  // cerr << "date_center: returning " << date.GetTime() << "  y=" << y << " m=" << m << " d=" << d+fd << endl; 
  //
  return date.GetTime();
}

/* 
   int minor_ticks_date(XOrsaPlotAxisDateStepUnit ds, double step) {
   int m;
   switch (ds) {
   case DS_FRAC_DAY:
   case DS_YEAR:
   m = (int)(fabs(step/secure_pow(10,floor(secure_log10(fabs(step))))));
   break;
   case DS_DAY:
   break;
   }
   }
*/

bool XOrsaPlotArea::TicksAndLabelsComputations_DATE(XOrsaPlotAxis &axis, QPainter *paint) {
  
  QPaintDeviceMetrics pdm(paint->device());
  const int width  = pdm.width();
  const int height = pdm.height();
  
  double scale_range, center=0, dummy;
  
  if (axis.IsLogScale()) {
    scale_range = coefficient_axis_label(axis.GetMax(),1);
  } else {
    if (axis.GetOrientation()==HORIZONTAL) {
      scale_range = coefficient_axis_label((width-left_border-right_border)*axis.GetPixelLength(),1);
    } else { // VERTICAL
      scale_range = coefficient_axis_label((height-top_border-bottom_border)*axis.GetPixelLength(),1);
    }
  }
  
  if (axis.GetOrientation()==HORIZONTAL) {
    dummy = y(height/2);
  } else { // VERTICAL
    dummy = x(width/2); 
  }
  
  int max_num_major_ticks = 20;
  int     num_major_ticks;
  int     num_minor_ticks;
  
  int max_width=0, max_height=0;
  
  double step, position, other_units_step;
  // vector<QString> labels;
  QString label;
  int k, pix;
  
  num_major_ticks = max_num_major_ticks;
  step = 1.0;
  bool done=false;
  while (done!=true) {
    
    num_major_ticks--;
    if (num_major_ticks<0) break;
    
    if (axis.IsLogScale()) {
      // FIXME: use axis type even for log scale...
      step *= 10.0;
      num_minor_ticks = (int)(rint(secure_log10(step))-1);
      if (step==10.0) num_minor_ticks = 9;
    } else {
      
      step = scale_range/num_major_ticks;
      axis.date_step = select_date_step(step,other_units_step); // step is modified here
      
      /* 
	 const int i = (int)(fabs(step/secure_pow(10,floor(secure_log10(fabs(step))))));
	 switch (i) {
	 case 1:  num_minor_ticks = 1; break;
	 case 2:  num_minor_ticks = 1; break;
	 case 5:  num_minor_ticks = 4; break;
	 default: num_minor_ticks = 0; break;
	 }
      */
    } 
    
    if (axis.GetOrientation()==HORIZONTAL) {
      center = date_center(axis.date_step,x(width/2),other_units_step);
    } else { // VERTICAL
      center = date_center(axis.date_step,y(height/2),other_units_step);
    }
    
    // create all the potential labels for measurement tests
    axis.labels.clear();
    XOrsaPlotAxisLabel axis_label;
    for (k=-num_major_ticks;k<=num_major_ticks;k++) {
      
      if (axis.GetOrientation()==HORIZONTAL) {
	// position = date_tick(k,axis.date_step,x(width/2),other_units_step);
  	position = date_tick(k,axis.date_step,center,other_units_step);
      } else { // VERTICAL
	// position = date_tick(k,axis.date_step,y(height/2),other_units_step);
   	position = date_tick(k,axis.date_step,center,other_units_step);
      }
      
      // cerr << "position: " << position << "  step: " << step << "  other_units_step: " << other_units_step << endl;
      
      if (axis.GetOrientation()==HORIZONTAL) {
	pix = p(position,dummy).x();
      } else { // VERTICAL
	pix = p(dummy,position).y();
      }
      
      if (((axis.GetOrientation()==HORIZONTAL) && isInsideBorder(pix,height/2,width,height)) ||
	  ((axis.GetOrientation()==VERTICAL)   && isInsideBorder(width/2,pix,width,height))) {
	WriteLabel(label,position,axis);
	axis_label.label = label;
	axis_label.position = position;
	axis.labels.push_back(axis_label);
      }
    }
    
    // cerr << "labels: " << axis.labels.size() << endl;
    
    max_width=0;
    max_height=0;
    QString longest_label;
    unsigned int m;
    for (m=0;m<axis.labels.size();m++) {
      QSimpleRichText rich_label(axis.labels[m].label,paint->font());
      rich_label.setWidth(paint,width);
      // rich_label.setWidth(paint,left_border);
      // rich_label.adjustSize();
      // cerr << "rich label size: " << rich_label.widthUsed() << "x" << rich_label.height() << endl;
      if (rich_label.widthUsed() > max_width) {
	longest_label = axis.labels[m].label;
	max_width = rich_label.widthUsed();
      }
      if (rich_label.height() > max_height) {
	max_height = rich_label.height();
      }
    }
    
    if (axis.GetOrientation()==HORIZONTAL) {
      if ((max_width*1.2)*axis.labels.size() < (width-left_border-right_border)) done=true;
    } else { // VERTICAL
      if ((max_height*1.2)*axis.labels.size() < (height-top_border-bottom_border)) done=true;
    }
  }
  
  if (done) {
    
    // cerr << "done!" << endl;
    
    axis.center = center;
    axis.step   = step;
    
    axis.SetMajorTicks(num_major_ticks);
    // axis.SetMinorTicks(num_minor_ticks);
    axis.SetMinorTicks(0);
    
    // NOTE: maybe max_width and max_height are too big...
    
    if ((axis.GetOrientation()==VERTICAL) && (max_width!=0)) {
      const int candidate_left_border = (int)(coefficient((1.2*max_width)/ width, 2)*width );
      // if (candidate_left_border>=left_border) SetBorder(LEFT,candidate_left_border);
      SetBorder(LEFT,candidate_left_border);
    }
    
    if ((axis.GetOrientation()==HORIZONTAL) && (max_height!=0)) {
      const int candidate_bottom_border = (int)(coefficient((1.2*max_height)/height,2)*height);
      // if (candidate_bottom_border>=bottom_border) SetBorder(BOTTOM,candidate_bottom_border);
      SetBorder(BOTTOM,candidate_bottom_border);
    }
    
  }
  
  return (done);
}

bool XOrsaPlotArea::TicksAndLabelsComputations(XOrsaPlotAxis &axis, QPainter *paint) {
  
  if (axis.GetType() == AT_DATE) {
    return TicksAndLabelsComputations_DATE(axis,paint);
  }
  
  QPaintDeviceMetrics pdm(paint->device());
  const int width  = pdm.width();
  const int height = pdm.height();
  
  double scale_range, center=0, dummy;
  
  if (axis.IsLogScale()) {
    scale_range = coefficient_axis_label(axis.GetMax(),1);
  } else {
    if (axis.GetOrientation()==HORIZONTAL) {
      scale_range = coefficient_axis_label((width-left_border-right_border)*axis.GetPixelLength(),1);
    } else { // VERTICAL
      scale_range = coefficient_axis_label((height-top_border-bottom_border)*axis.GetPixelLength(),1);
    }
  }
  
  if (axis.GetOrientation()==HORIZONTAL) {
    dummy = y(height/2);
  } else { // VERTICAL
    dummy = x(width/2); 
  }
  
  int max_num_major_ticks = 20;
  int     num_major_ticks;
  int     num_minor_ticks=0;
  
  int max_width=0, max_height=0;
  
  double step, position;
  // vector<QString> labels;
  QString label;
  int k, pix;
  
  num_major_ticks = max_num_major_ticks;
  step = 1.0;
  bool done=false;
  while (done!=true) {
    
    num_major_ticks--;
    if (num_major_ticks<0) break;
    
    if (axis.IsLogScale()) {
      // FIXME: use axis type even for log scale...
      step *= 10.0;
      num_minor_ticks = (int)(rint(secure_log10(step))-1);
      if (step==10.0) num_minor_ticks = 9;
    } else {
      if (axis.GetType() == AT_DATE) {
	// step = coefficient_date_axis_label_step(scale_range/num_major_ticks,1);
      } else {
	step = coefficient_axis_label(scale_range/num_major_ticks,1);
      }
      const int i = (int)(fabs(step/secure_pow(10,floor(secure_log10(fabs(step))))));
      switch (i) {
      case 1:  num_minor_ticks = 1; break;
      case 2:  num_minor_ticks = 1; break;
      case 5:  num_minor_ticks = 4; break;
      default: num_minor_ticks = 0; break;
      }
    } 
    
    if (axis.GetOrientation()==HORIZONTAL) {
      if (axis.IsLogScale()) {
	// FIXME: use axis type even for log scale...
	// center = coefficient_log_axis_label(x(width/2),1);
	if (axis.GetType() == AT_DATE) {
	  // center = coefficient_date_axis_label(x(width/2),1);
	} else {
	  center = coefficient_log_axis_label(x(width/2),1);
	}
      } else {
	if (axis.GetType() == AT_DATE) {
	  // center = coefficient_date_axis_label(x(width/2),1);
	} else {
	  center = rint(x(width/2)/step)*step;
	}
      }	
    } else { // VERTICAL
      if (axis.IsLogScale()) {
	// FIXME: use axis type even for log scale...
	// center = coefficient_log_axis_label(y(height/2),1);
      	if (axis.GetType() == AT_DATE) {
	  // center = coefficient_date_axis_label(y(height/2),1);
	} else {
	  center = coefficient_log_axis_label(y(height/2),1);
	}
      } else {
	if (axis.GetType() == AT_DATE) {
	  // center = coefficient_date_axis_label(y(height/2),1);
	} else {
	  center = rint(y(height/2)/step)*step;
	}
      }	
    }
    
    // create all the potential labels for measurement tests
    axis.labels.clear();
    XOrsaPlotAxisLabel axis_label;
    for (k=-num_major_ticks;k<=num_major_ticks;k++) {
      
      if (axis.IsLogScale()) {
	position = center*secure_pow(step,k);
      } else {
	position = rint((center+k*step)/step)*step;
      }
      
      if (axis.GetOrientation()==HORIZONTAL) {
	pix = p(position,dummy).x();
      } else { // VERTICAL
	pix = p(dummy,position).y();
      }
      
      if (((axis.GetOrientation()==HORIZONTAL) && isInsideBorder(pix,height/2,width,height)) ||
	  ((axis.GetOrientation()==VERTICAL)   && isInsideBorder(width/2,pix,width,height))) {
	WriteLabel(label,position,axis);
	axis_label.label = label;
	axis_label.position = position;
	axis.labels.push_back(axis_label);
      }
    }
    
    max_width=0;
    max_height=0;
    QString longest_label;
    unsigned int m;
    for (m=0;m<axis.labels.size();m++) {
      QSimpleRichText rich_label(axis.labels[m].label,paint->font());
      rich_label.setWidth(paint,width);
      // rich_label.setWidth(paint,left_border);
      // rich_label.adjustSize();
      // cerr << "rich label size: " << rich_label.widthUsed() << "x" << rich_label.height() << endl;
      if (rich_label.widthUsed() > max_width) {
	longest_label = axis.labels[m].label;
	max_width = rich_label.widthUsed();
      }
      if (rich_label.height() > max_height) {
	max_height = rich_label.height();
      }
    }
    
    if (axis.GetOrientation()==HORIZONTAL) {
      if ((max_width*1.2)*axis.labels.size() < (width-left_border-right_border)) done=true;
    } else { // VERTICAL
      if ((max_height*1.2)*axis.labels.size() < (height-top_border-bottom_border)) done=true;
    }
  }
  
  if (done) {
    
    axis.center = center;
    axis.step   = step;
    
    axis.SetMajorTicks(num_major_ticks);
    axis.SetMinorTicks(num_minor_ticks);

    // NOTE: maybe max_width and max_height are too big...
    
    if ((axis.GetOrientation()==VERTICAL) && (max_width!=0)) {
      const int candidate_left_border = (int)(coefficient((1.2*max_width)/ width, 2)*width );
      // if (candidate_left_border>=left_border) SetBorder(LEFT,candidate_left_border);
      SetBorder(LEFT,candidate_left_border);
    }
    
    if ((axis.GetOrientation()==HORIZONTAL) && (max_height!=0)) {
      const int candidate_bottom_border = (int)(coefficient((1.2*max_height)/height,2)*height);
      // if (candidate_bottom_border>=bottom_border) SetBorder(BOTTOM,candidate_bottom_border);
      SetBorder(BOTTOM,candidate_bottom_border);
    }
    
  }
  
  return (done);
}


XOrsaPlotArea::XOrsaPlotArea(QWidget *parent) : QWidget(parent), X(HORIZONTAL), Y(VERTICAL) {
  setMinimumSize(500,500);  
  Init();
}


XOrsaPlotArea::XOrsaPlotArea(int w, int h, QWidget *parent) : QWidget(parent), X(HORIZONTAL), Y(VERTICAL) {
  setMinimumSize(w,h);  
  Init();
}


XOrsaPlotArea::~XOrsaPlotArea() {
  // cerr << "XOrsaPlotArea::~XOrsaPlotArea() called..." << endl;
}


void XOrsaPlotArea::Init() {
  
  QCursor cursor(Qt::CrossCursor);
  setCursor(cursor);
  
  setFocusPolicy(QWidget::StrongFocus);
 
  // reduces flicker, very nice flags
  setWFlags(Qt::WRepaintNoErase);
  setBackgroundMode(Qt::NoBackground);
  
  pixmap = new QPixmap();
  tmp_pixmap = new QPixmap();
  bool_pixmap_needs_update=true;
  
  printer = new QPrinter(QPrinter::HighResolution);
  printer->setOrientation(QPrinter::Landscape);
  printer->setCreator("ORSA " ORSA_VERSION);
  
  active_paint_device = this;
  
  curves = new vector<XOrsaPlotCurve>;
  
  // can be set from outside the widget with setMouseTracking(true);
  // grabMouse();
  
  // zooming must be set false 
  zooming = false;
  
  // preferences
  // log_x_axis          = false;
  // log_y_axis          = false;
  same_scale_x_y      = false;
  bool_connect_points = false;
  bool_stack          = false;
  
  bool_fix_x_range = bool_fix_y_range = false;
  
  // borders
  SetBorder(TOP,    0);
  SetBorder(BOTTOM, 0);
  SetBorder(LEFT,   0); 
  SetBorder(RIGHT,  0);
  
  Init_signals_slots();
}


void XOrsaPlotArea::Init_signals_slots() {
  
  connect(&X,SIGNAL(RangeChanged()),this,SLOT(slot_axis_range_changed()));
  connect(&Y,SIGNAL(RangeChanged()),this,SLOT(slot_axis_range_changed()));
  
  connect(&X,SIGNAL(PixelLengthChanged()),this,SLOT(slot_pixel_length_changed()));
  connect(&Y,SIGNAL(PixelLengthChanged()),this,SLOT(slot_pixel_length_changed()));
  
  connect(&X,SIGNAL(LogScaleChanged(bool)),this,SLOT(slot_log_scale_changed()));
  connect(&Y,SIGNAL(LogScaleChanged(bool)),this,SLOT(slot_log_scale_changed()));
  
  connect(&X,SIGNAL(TicksChanged()),this,SLOT(slot_ticks_changed()));
  connect(&Y,SIGNAL(TicksChanged()),this,SLOT(slot_ticks_changed()));
  
  connect(&X,SIGNAL(TypeChanged()),this,SLOT(slot_type_changed()));
  connect(&Y,SIGNAL(TypeChanged()),this,SLOT(slot_type_changed()));
  
  connect(this,SIGNAL(BorderChanged(BORDER)),this,SLOT(slot_borders_changed(BORDER)));
  
}

void XOrsaPlotArea::mouseMoveEvent(QMouseEvent *me) {
  
  // for mouse position tracking
  emit mouse_moved(me);
  
  // printf("mouse position: %.20g %.20g\n",x(me->pos()),y(me->pos()));
  
  if (zooming) {
    mouse_pos = me->pos();
    
    // draws the rect...
    update();
  }
  
}

void XOrsaPlotArea::slot_axis_range_changed() {
  ComputeOriginPosition(active_paint_device);
  pixmap_needs_update();
  update();
}

void XOrsaPlotArea::slot_pixel_length_changed() {
  pixmap_needs_update();
  update();
}

void XOrsaPlotArea::slot_log_scale_changed() {
  if (X.IsLogScale() || Y.IsLogScale()) SetSameScale(false);
  ComputeOriginPosition(active_paint_device);
  pixmap_needs_update();
  update();  
}

void XOrsaPlotArea::slot_type_changed() {
  pixmap_needs_update();
  update();  
}

void XOrsaPlotArea::slot_ticks_changed() {
  pixmap_needs_update();
  update();  
}

void XOrsaPlotArea::slot_borders_changed(BORDER) {
  ComputeOriginPosition(active_paint_device);
  pixmap_needs_update();
  update();  
}

void XOrsaPlotArea::mouseReleaseEvent(QMouseEvent *me) {
  
  if ((me->button() == LeftButton) && zooming) {
    
    zoom_stop = me->pos();
    
    if ((zoom_stop.x() != zoom_start.x()) &&
        (zoom_stop.y() != zoom_start.y()) ) {
      
      SetBothAxisRange(x(zoom_start),x(zoom_stop),y(zoom_start),y(zoom_stop));
      pixmap_needs_update();
      update();  
    }
    
    zooming = false;
  }
}


void XOrsaPlotArea::mousePressEvent(QMouseEvent *me) {
  
  if (me->button() == LeftButton) { // zoom starting button
    zoom_start = me->pos();
    mouse_pos  = me->pos();
    zooming = true;
  }
  
  if (me->button() == RightButton) { // autoscale
    ComputeLimits();
  }
}


void XOrsaPlotArea::paintEvent(QPaintEvent*) {
  
  // call paintPixmap() only here!
  if (bool_pixmap_needs_update) paintPixmap();
  
  if (zooming) {
    bitBlt(tmp_pixmap,0,0,pixmap,0,0,pixmap->width(),pixmap->height(),Qt::CopyROP); //Qt::CopyROP
    QPainter paint(tmp_pixmap);
    paint.setPen(Qt::black);
    paint.setPen(Qt::DotLine);
    paint.setRasterOp(Qt::CopyROP);
    int w_rect = mouse_pos.x()-zoom_start.x();
    int h_rect = mouse_pos.y()-zoom_start.y();
    if (w_rect != 0) { if (w_rect > 0) w_rect++; else w_rect--; }
    if (h_rect != 0) { if (h_rect > 0) h_rect++; else h_rect--; }
    paint.drawRect(zoom_start.x(),zoom_start.y(),w_rect,h_rect);
    bitBlt(this,0,0,tmp_pixmap,0,0,tmp_pixmap->width(),tmp_pixmap->height(),Qt::CopyROP);
  } else {
    bitBlt(this,0,0,pixmap,0,0,pixmap->width(),pixmap->height(),Qt::CopyROP);
  }
}


void XOrsaPlotArea::paintPixmap() {
  
  active_paint_device=this;
  
  // sync size
  if (pixmap->size() != size()) {
    pixmap->resize(size());
    tmp_pixmap->resize(size());
  }
  
  QPainter paint(pixmap);
  // paint.setBrush( colorGroup().foreground() ); 
  paint.setPen(Qt::black);
  { 
    QBrush brush(Qt::white,Qt::SolidPattern);
    paint.setBrush(brush);
  }
  
  QPaintDeviceMetrics pdm(paint.device());
  const int width  = pdm.width();
  const int height = pdm.height();
  
  QBrush brush = paint.brush();
  qDrawPlainRect(&paint,0,0,width,height,Qt::black,1,&brush);
  
  update_font_size(&paint);
  DrawAxis(&paint);
  DrawArea(&paint);
  
  bool_pixmap_needs_update=false;
}

void XOrsaPlotArea::SaveData() {
  QString s = QFileDialog::getSaveFileName("","",this,"Save data to file","Choose a file" );
  if (!(s.isEmpty())) {
    
    FILE *file = fopen(s.latin1(),"w");
    
    if (file!=0) {
      
      XOrsaPlotCurve::iterator         it;
      vector<XOrsaPlotCurve>::iterator it_curve;
      
      it_curve = curves->begin();
      while (it_curve != curves->end()) {
	
	if ((*it_curve).size() == 0) { 
	  ++it_curve;
	  continue;
	}
	
	double x,y;
	
	it      = (*it_curve).begin();
	while (it != (*it_curve).end()) {
	  
	  // fprintf(file,"%22.16g    %22.16g\n",(*it).x,(*it).y);
	  
	  x = (*it).x;
	  y = (*it).y;
	  
	  // convert to Julian date
	  if (X.GetType() == AT_DATE) x = FromUnits(x,DAY,-1);
	  if (Y.GetType() == AT_DATE) y = FromUnits(y,DAY,-1);
	  
	  fprintf(file,"%22.16f  %22.16f\n",x,y);
	  
	  ++it;
	}
	
	++it_curve;
      }
      
      fclose(file);
      
    } else {
      // can't open file...
    }
  }
}

void XOrsaPlotArea::PrintArea() {
#ifndef QT_NO_PRINTER
  if (printer->setup(this)) {
    
    active_paint_device=printer;
    
    // ComputeLimits();
    ComputeOriginPosition(printer);  
    
    QPainter paint(printer);
    paint.setPen(Qt::black);
    QBrush brush(Qt::white,Qt::SolidPattern);
    paint.setBrush(brush);
    
    update_font_size(&paint);
    DrawAxis(&paint);
    DrawArea(&paint);
    
    // restore...
    active_paint_device=this;
    ComputeOriginPosition(this);  
    pixmap_needs_update();
    // update();
  }
#endif // QT_NO_PRINTER
}

void XOrsaPlotArea::resizeEvent(QResizeEvent*) {
  ComputeOriginPosition(active_paint_device);
  pixmap_needs_update();
  update();
}


void XOrsaPlotArea::ComputeOriginPosition(QPaintDevice *paintdevice) {
  
  QPaintDeviceMetrics pdm(paintdevice);
  const int width  = pdm.width();
  const int height = pdm.height();
  
  const int w = width-left_border-right_border;
  const int h = height-top_border-bottom_border;
  
  double range;
  
  // when determining x and y -pixel_length
  // we have to keep into account null ranges (flat curves)
  // and pixel length too small compared to the mean value of the curve
  
  double c;
  
  if (same_scale_x_y) {
    
    // min
    unsigned int p_min = w; if (h < w) p_min = h;
    
    // max
    // range = x_range; if (y_range > range) range = y_range;
    range = X.GetRange(); if (Y.GetRange() > range) range = Y.GetRange();
    
    if (range > 0) {
      // x_pixel_length = y_pixel_length = coefficient(1.02*(range/((double)p_min)),2);
      // x_pixel_length = y_pixel_length = coefficient(1.02*(range/((double)p_min)),2);
      c = coefficient(1.02*(range/((double)p_min)),2);
      X.SetPixelLength(c);
      Y.SetPixelLength(c);
    } else {
      // range = (x_max + x_min); if ((y_max + y_min) > range) range = (y_max + y_min);
      range = (X.GetMax() + X.GetMin()); if ((Y.GetMax() + Y.GetMin()) > range) range = (Y.GetMax() + Y.GetMin());
      // x_pixel_length = y_pixel_length = coefficient(1.02*(range/((double)p_min)),2);
      c = coefficient(1.02*(range/((double)p_min)),2);
      X.SetPixelLength(c);
      Y.SetPixelLength(c);
    }
    
  } else {
    
    if (X.GetRange() > 0) { 
      // x_pixel_length = coefficient(1.02*(x_range/((double)w)),2);
      X.SetPixelLength(coefficient(1.02*(X.GetRange()/((double)w)),2));
    } else {
      // range = (x_max + x_min);
      range = X.GetMax() + X.GetMin();
      // x_pixel_length = coefficient(1.02*(range/((double)w)),2);
      X.SetPixelLength(coefficient(1.02*(range/((double)w)),2));
    }
    
    if  (Y.GetRange() > 0) { 
      // y_pixel_length = coefficient(1.02*(y_range/((double)h)),2);
      Y.SetPixelLength(coefficient(1.02*(Y.GetRange()/((double)h)),2));
    } else {
      range = (Y.GetMax() + Y.GetMin());
      // y_pixel_length = coefficient(1.02*(range/((double)h)),2);
      Y.SetPixelLength(coefficient(1.02*(range/((double)h)),2));
    }
    
  }
  
  if (X.IsLogScale()) X.SetPixelLength(secure_pow((X.GetMax()/X.GetMin()),(1.04/(double)w)));
  if (Y.IsLogScale()) Y.SetPixelLength(secure_pow((Y.GetMax()/Y.GetMin()),(1.04/(double)h)));
  
  orig.setX(w/2);
  orig.setY(h/2);
  
  orig_pp.x = (X.GetMax() + X.GetMin())/2.0;
  orig_pp.y = (Y.GetMax() + Y.GetMin())/2.0;
  
  if (X.IsLogScale()) orig_pp.x = secure_sqrt(X.GetMin()*X.GetMax());
  if (Y.IsLogScale()) orig_pp.y = secure_sqrt(Y.GetMin()*Y.GetMax());
  
  // cerr << "orig:    " << orig.x()  << "  " << orig.y()  << endl;
  // cerr << "orig_pp: " << orig_pp.x << "  " << orig_pp.y << endl;
}


void XOrsaPlotArea::SetConnectPoints(bool b) {
  if (b != bool_connect_points) {
    bool_connect_points = b;
    emit ConnectChanged(b);
    pixmap_needs_update();
    update();    
  }  
}


void XOrsaPlotArea::SetSameScale(bool b) {
  
  if (b != same_scale_x_y) {
    same_scale_x_y = b;
    emit SameScaleChanged(b);
    ComputeOriginPosition(this);
    pixmap_needs_update();
    update();
  }  
}


double XOrsaPlotArea::x(QPoint p) {
  if (X.IsLogScale()) return (orig_pp.x*secure_pow(X.GetPixelLength(),(p.x()-orig.x()-left_border)));
  return (orig_pp.x+X.GetPixelLength()*(p.x()-orig.x()-left_border));
}


double XOrsaPlotArea::y(QPoint p) {
  if (Y.IsLogScale()) return (orig_pp.y*secure_pow(Y.GetPixelLength(),(orig.y()-p.y()+top_border)));
  return (orig_pp.y+Y.GetPixelLength()*(orig.y()-p.y()+top_border));
}


double XOrsaPlotArea::x(const int i_x) {
  if (X.IsLogScale()) return (orig_pp.x*secure_pow(X.GetPixelLength(),(i_x-orig.x()-left_border)));
  return (orig_pp.x+X.GetPixelLength()*(i_x-orig.x()-left_border));
}


double XOrsaPlotArea::y(const int i_y) {
  if (Y.IsLogScale()) return (orig_pp.y*secure_pow(Y.GetPixelLength(),(orig.y()-i_y+top_border)));
  return (orig_pp.y+Y.GetPixelLength()*(orig.y()-i_y+top_border));
}


QPoint XOrsaPlotArea::p(const XOrsaPlotPoint xpp) {
  return p(xpp.x,xpp.y);
}


QPoint XOrsaPlotArea::p(const double x, const double y) {
  
  double px,py;
  
  if (X.IsLogScale()) {
    px = rint(left_border+orig.x()+log(x/orig_pp.x)/log(X.GetPixelLength()));
  } else {
    px = rint(left_border+orig.x()+(x-orig_pp.x)/X.GetPixelLength());
  }
  
  if (Y.IsLogScale()) {
    py = rint(top_border+orig.y()-log(y/orig_pp.y)/log(Y.GetPixelLength()));
  } else {
    py = rint(top_border+orig.y()-(y-orig_pp.y)/Y.GetPixelLength());
  }
  
  return QPoint((int)px,(int)py);
}


// test if the QPoint is located within the plotting area, or so
const int pixel_limit = 10000;
bool XOrsaPlotArea::isRegularQPoint(const QPoint p, const int width, const int height) {
  return isRegularQPoint(p.x(), p.y(), width, height);
}


bool XOrsaPlotArea::isRegularQPoint(const int x, const int y, const int width, const int height) {
  if (x < -pixel_limit)          return false;
  if (y < -pixel_limit)          return false;
  if (x >= width  + pixel_limit) return false;
  if (y >= height + pixel_limit) return false;
  
  return true;
}


bool XOrsaPlotArea::isInsideBorder(const QPoint p, const int width, const int height) {
  return isInsideBorder(p.x(), p.y(), width, height);
}


bool XOrsaPlotArea::isInsideBorder(const int x, const int y, const int width, const int height) {
  if (x < left_border)             return false;
  if (y < top_border)              return false;
  if (x >= width  - right_border)  return false;
  if (y >= height - bottom_border) return false;
  return true;  
}

void XOrsaPlotArea::WriteLabel(QString &label, const double &x, const XOrsaPlotAxis &axis) {
  if (axis.IsLogScale()) {
    // only 10^?? values...
    // FIXME: use axis type even for log scale...
    label.sprintf("10<sup>%i</sup>",(int)(rint(secure_log10(x))));
  } else {
    switch (axis.GetType()) {
    case AT_DATE:
      FineDate(label,x);
      // cerr << "FineDate: " << label << endl;
      break;
    default:
      FineLabel(label,x);
      break;
    }
  }
}

void XOrsaPlotArea::DrawTicksAndLabels(const XOrsaPlotAxis &axis, QPainter *paint) {
  
  // cerr << "doing real draw..." << endl;
  
  QPaintDeviceMetrics pdm(paint->device());
  const int width  = pdm.width();
  const int height = pdm.height();
  
  const int major_tick_len = width/80;         // pixels
  const int minor_tick_len = major_tick_len/2; // pixels
  
  double scale_range, dummy=0;
  
  if (axis.IsLogScale()) {
    scale_range = coefficient_axis_label(axis.GetMax(),1);
  } else {
    if (axis.GetOrientation()==HORIZONTAL) {
      scale_range = coefficient_axis_label((width-left_border-right_border)*axis.GetPixelLength(),1);
    } else { // VERTICAL
      scale_range = coefficient_axis_label((height-top_border-bottom_border)*axis.GetPixelLength(),1);
    }
  }
  
  // const int num_major_ticks = axis.GetMajorTicks();
  const int num_minor_ticks = axis.GetMinorTicks();
  
  const double step   = axis.step;
  // const double center = axis.center;
  
  QFontMetrics fm(paint->font());
  const int back_space=fm.width('\0');
  
  unsigned int k;
  int pix, minor;
  double minor_step=0.0;
  if (num_minor_ticks!=0) minor_step = step/(num_minor_ticks+1);
  //
  // for (k=-num_major_ticks;k<=num_major_ticks;k++) {
  for (k=0;k<axis.labels.size();++k) {
    for (minor=0;minor<=num_minor_ticks;minor++) {
      
      if (axis.GetOrientation()==HORIZONTAL) {
	pix = p(axis.labels[k].position,dummy).x();
      } else { // VERTICAL
	pix = p(dummy,axis.labels[k].position).y();
      }
      
      if (axis.GetOrientation()==HORIZONTAL) {
	if (isInsideBorder(pix,height/2,width,height)) {
	  if (minor==0) {
	    // tick labels
	    QSimpleRichText rich_label(axis.labels[k].label,paint->font());
	    rich_label.setWidth(paint,width);
	    // QRect rect(pix+1-rich_label.widthUsed()/2,height-bottom_border,rich_label.widthUsed(),rich_label.height());
	    // QRect rect(pix+1-(rich_label.widthUsed()-back_space)/2,height-bottom_border,rich_label.widthUsed()-back_space,rich_label.height());
	    QRect rect(pix-(rich_label.widthUsed()-back_space)/2,height-bottom_border,rich_label.widthUsed(),rich_label.height());
	    // paint->drawRect(rect);
	    rich_label.draw(paint,rect.left(),rect.top(),rect,colorGroup());
	    // major tick
	    paint->drawLine(pix,height-bottom_border-1,pix,height-bottom_border-1-major_tick_len);
	  } else {
	    paint->drawLine(pix,height-bottom_border-1,pix,height-bottom_border-1-minor_tick_len);
	  }
	}
      } else { // VERTICAL
	if (isInsideBorder(width/2,pix,width,height)) {
	  if (minor==0) {
	    // tick labels
	    QSimpleRichText rich_label(axis.labels[k].label,paint->font());
	    rich_label.setWidth(paint,width);
	    // QRect rect((left_border-rich_label.widthUsed())*2/3,pix-rich_label.height()/2,rich_label.widthUsed(),rich_label.height());
	    QRect rect((left_border-(rich_label.widthUsed()-back_space))*2/3,pix-rich_label.height()/2,rich_label.widthUsed(),rich_label.height());
	    // paint->drawRect(rect);
	    rich_label.draw(paint,rect.left(),rect.top(),rect,colorGroup());
	    // major tick
	    paint->drawLine(left_border,pix,left_border+major_tick_len,pix);
	  } else {
	    paint->drawLine(left_border,pix,left_border+minor_tick_len,pix);
	  }
	}
      }
    }
  }
}

void XOrsaPlotArea::update_font_size(QPainter *paint) {
  
  QPaintDeviceMetrics pdm(paint->device());
  const int width  = pdm.width();
  const int height = pdm.height();
  
  QFont font(paint->font());
  font.setPixelSize(QMIN(width,height)/32);
  paint->setFont(font);
}

void XOrsaPlotArea::DrawAxis(QPainter *paint, QPaintEvent*) {
  
  QPaintDeviceMetrics pdm(paint->device());
  const int width  = pdm.width();
  const int height = pdm.height();
  
  // cerr << "paint device size: " << width << "x" << height << " pixels" << endl;
  // cerr << "paint device size: " << pdm.widthMM() << "x" << pdm.heightMM() << " mm" << endl;
  // cerr << "paint device dpi:  " << pdm.logicalDpiX() << "x" << pdm.logicalDpiY() << endl;
  
  paint->save();
  
  paint->setPen(Qt::black);
  
  // paint->drawRect(border,border,width-2*border,height-2*border);
  // paint->setPen(Qt::black);
  // qDrawPlainRect(paint,0,0,width,height,Qt::black,1,&paint->brush());
  // paint->drawRect(left_border,top_border,width-left_border-right_border,height-top_border-bottom_border);
  
  // title
  QSimpleRichText rich_label(title,paint->font());
  // cerr << "title font point size: " << paint->font().pointSize() << endl;
  // cerr << "title font pixel size: " << paint->font().pixelSize() << endl;
  // const int old_w = rich_label.widthUsed();
  // const int old_h = rich_label.height();
  // cerr << "title label size (I):  " << rich_label.widthUsed() << "x" << rich_label.height() << endl;
  rich_label.setWidth(paint,width-left_border-right_border);
  // cerr << "printer_font_scale candidates: " << ((double)rich_label.widthUsed()/(double)old_w) << " and " << ((double)rich_label.height()/old_h) << endl;
  // cerr << "title font point size: " << paint->font().pointSize() << endl;
  // cerr << "title font pixel size: " << paint->font().pixelSize() << endl;
  
  const int border = (int)(coefficient(1.02*rich_label.height()/height,2)*height);
  // SetBorder(TOP,  (int)(coefficient(1.02*rich_label.height()/height,2)*height));
  // SetBorder(RIGHT,(int)(coefficient(1.02*rich_label.height()/height,2)*height));
  SetBorder(TOP,    border);
  SetBorder(BOTTOM, border);
  SetBorder(LEFT,   border);
  SetBorder(RIGHT,  border);
  
  QRect rect((width-rich_label.widthUsed())/2,(top_border-rich_label.height())/2,rich_label.widthUsed(),rich_label.height());
  // paint->drawRect(rect);
  // rich_label.adjustSize();
  // cerr << "title label size (II):  " << rich_label.widthUsed() << "x" << rich_label.height() << endl;
  rich_label.draw(paint,rect.left(),rect.top(),rect,colorGroup());
  // cerr << "title label size (II): " << rich_label.widthUsed() << "x" << rich_label.height() << endl;
  
  int count=0;
  bool both=false;
  int old_left_border,old_right_border,old_top_border,old_bottom_border;
  while ((count<10) && (!both)) {
    
    count++;
    
    old_left_border   = left_border;
    old_right_border  = right_border;
    old_top_border    = top_border;
    old_bottom_border = bottom_border;
    
    // two more calls
    // TicksAndLabelsComputations(X,paint);
    // TicksAndLabelsComputations(Y,paint);
    
    both = (TicksAndLabelsComputations(X,paint) && TicksAndLabelsComputations(Y,paint));
    both = ( (both) &&
	     (old_left_border   == left_border) &&
	     (old_right_border  == right_border) && 
	     (old_top_border    == top_border) &&
	     (old_bottom_border == bottom_border) );
    
    // printf("count: %i  ---  left: %i (old: %i)   right: %i (old: %i)   top: %i (old: %i)   bottom: %i (old: %i)\n",count,left_border,old_left_border,right_border,old_right_border,top_border,old_top_border,bottom_border,old_bottom_border);
    
  }
  
  // two more calls
  TicksAndLabelsComputations(X,paint);
  TicksAndLabelsComputations(Y,paint);
  
  if (both) {
    paint->drawRect(left_border,top_border,width-left_border-right_border,height-top_border-bottom_border);
    DrawTicksAndLabels(X,paint);
    DrawTicksAndLabels(Y,paint);
  } else {
    cerr << "problems found in ticks and labels computations..." << endl;
  }
  
  paint->restore();
}

// use this function to change the borders value
void XOrsaPlotArea::SetBorder(BORDER b, int value) {
  switch(b) {
  case TOP:    if (value!=top_border)    { top_border=value;    emit BorderChanged(TOP);    } break;
  case BOTTOM: if (value!=bottom_border) { bottom_border=value; emit BorderChanged(BOTTOM); } break;
  case LEFT:   if (value!=left_border)   { left_border=value;   emit BorderChanged(LEFT);   } break;
  case RIGHT:  if (value!=right_border)  { right_border=value;  emit BorderChanged(RIGHT);  } break;
  }
}

void XOrsaPlotArea::DrawArea(QPainter *paint, QPaintEvent*) {
  
  QPaintDeviceMetrics pdm(paint->device());
  const int width  = pdm.width();
  const int height = pdm.height();
  
  if (curves == 0) return;
  
  if (curves->size() == 0) return;
  
  paint->save();
  
  // set clip region
  // paint->setClipRect(left_border,top_border,width-left_border-right_border,height-top_border-bottom_border);
  paint->setClipRect(left_border+1,top_border+1,width-left_border-right_border-2,height-top_border-bottom_border-2);
  
  // TODO: write both cases together
  if (bool_connect_points) {
    
    XOrsaPlotCurve::iterator         it, it_plus;
    vector<XOrsaPlotCurve>::iterator it_curve;
    
    it_curve = curves->begin();
    while (it_curve != curves->end()) {
      
      if ((*it_curve).size() == 0) { 
        it_curve++;
        continue;
      }
      
      paint->setPen(it_curve->color);
      
      it      = (*it_curve).begin();
      it_plus = (*it_curve).begin(); it_plus++;
      while (it_plus != (*it_curve).end()) {
        
        if ((*it) == (*it_plus)) { 
          it++; it_plus++;
          continue;
        } 
        
	if ( (isRegularQPoint(p(*it),width,height)) && 
	     (isRegularQPoint(p(*it_plus),width,height)) ) paint->drawLine(p(*it),p(*it_plus));
        
        it++; it_plus++;
      }
      
      it_curve++;
    }
    
  } else {
    
    XOrsaPlotCurve::iterator         it;
    vector<XOrsaPlotCurve>::iterator it_curve;
    
    QPoint last_point;
    it_curve = curves->begin();
    while (it_curve != curves->end()) {
      
      if ((*it_curve).size() == 0) { 
        it_curve++;
        continue;
      }
      
      paint->setPen(it_curve->color);
      
      it      = (*it_curve).begin();
      while (it != (*it_curve).end()) {
        
        if (p(*it) == last_point) {
          it++;
          continue;
        } 
        
        // if (isRegularQPoint(p(*it),width,height)) paint->drawPoint(p(*it));
	if (isInsideBorder(p(*it),width,height)) paint->drawPoint(p(*it));
        
        last_point = p(*it);
        it++;
      }
      
      it_curve++;
    }
    
  }
  
  paint->restore();
  
}


void XOrsaPlotArea::ComputeLimits() {
  
  if (bool_fix_x_range && bool_fix_y_range) {
    
    return;
    
  } else if (bool_fix_x_range) {
    
    // if only one axis is fixed, compute limits of the other axis
    // considering only points in the range of the fixed axis
    
    double y_min=0,y_max=0;
    
    if ((curves == 0) || (curves->size() == 0)) return;
    
    bool entered_range_at_least_once=false;
    
    double x, y;
    
    XOrsaPlotCurve::iterator         it;
    vector<XOrsaPlotCurve>::iterator it_curve;
    
    it_curve = curves->begin();
    while (it_curve != curves->end()) {
      
      it      = (*it_curve).begin();
      while (it != (*it_curve).end()) {
	
	x = (*it).x;
	y = (*it).y;
	
	if ((x>=X.GetMin()) && (x<=X.GetMax())) {
	  
	  if (entered_range_at_least_once == false) {
	    // first time
	    y_min = y_max = y;
	  } else {
	    if (y < y_min) y_min = y;  
	    if (y > y_max) y_max = y;
	  }
	  
	  entered_range_at_least_once=true;
	}
	
	it++;      
      }
      
      it_curve++;
    }
    
    if (entered_range_at_least_once) Y.SetRange(y_min,y_max);
    
  } else if (bool_fix_y_range) {
    
    // if only one axis is fixed, compute limits of the other axis
    // considering only points in the range of the fixed axis
    
    double x_min=0,x_max=0;
    
    if ((curves == 0) || (curves->size() == 0)) return;
    
    bool entered_range_at_least_once=false;
    
    double x, y;
    
    XOrsaPlotCurve::iterator         it;
    vector<XOrsaPlotCurve>::iterator it_curve;
    
    it_curve = curves->begin();
    while (it_curve != curves->end()) {
      
      it      = (*it_curve).begin();
      while (it != (*it_curve).end()) {
	
	x = (*it).x;
	y = (*it).y;
	
	if ((y>=Y.GetMin()) && (y<=Y.GetMax())) {
	  
	  if (entered_range_at_least_once == false) {
	    // first time
	    x_min = x_max = x;
	  } else {
	    if (x < x_min) x_min = x;  
	    if (x > x_max) x_max = x;
	  }
	  
	  entered_range_at_least_once=true;
	}
	
	it++;      
      }
      
      it_curve++;
    }
    
    if (entered_range_at_least_once) X.SetRange(x_min,x_max);
    
  } else {
    
    double x_min,x_max;
    double y_min,y_max;
    
    // if ((curves == 0) || (curves->size() == 0)) return(SetDefaultRanges());
    if ((curves == 0) || (curves->size() == 0)) return;
    
    x_min = x_max = (*curves)[0][0].x;
    y_min = y_max = (*curves)[0][0].y;
    
    double x, y;
    
    XOrsaPlotCurve::iterator         it;
    vector<XOrsaPlotCurve>::iterator it_curve;
    
    it_curve = curves->begin();
    while (it_curve != curves->end()) {
      
      it      = (*it_curve).begin();
      while (it != (*it_curve).end()) {
	
	x = (*it).x;
	y = (*it).y;
	
	if (x < x_min) x_min = x;  
	if (x > x_max) x_max = x;
	if (y < y_min) y_min = y;  
	if (y > y_max) y_max = y;
	
	it++;      
      }
      
      it_curve++;
    }
    
    // if (!bool_fix_x_range) X.SetRange(x_min,x_max);
    // if (!bool_fix_y_range) Y.SetRange(y_min,y_max);
    
    X.SetRange(x_min,x_max);
    Y.SetRange(y_min,y_max);
    
    // cerr << "XOrsaPlotArea::ComputeLimits()  X range: " << x_min << "  " << x_max << "  " << x_max-x_min << endl;
    // cerr << "XOrsaPlotArea::ComputeLimits()  Y range: " << y_min << "  " << y_max << "  " << y_max-y_min << endl;
  }
  
} 


void XOrsaPlotArea::SetAxisRange(XOrsaPlotAxis &axis, const double range_start, const double range_stop) {
  // axis.SetRange(range_start,range_stop);
  if ((&axis==&X) && (!bool_fix_x_range)) X.SetRange(range_start,range_stop);
  if ((&axis==&Y) && (!bool_fix_y_range)) Y.SetRange(range_start,range_stop);
}


void XOrsaPlotArea::SetBothAxisRange(const double x_range_start, const double x_range_stop, const double y_range_start, const double y_range_stop) {
  if (!bool_fix_x_range) X.SetRange(x_range_start,x_range_stop);
  if (!bool_fix_y_range) Y.SetRange(y_range_start,y_range_stop);
}


void XOrsaPlotArea::SetData(vector<XOrsaPlotCurve> *curves_in, bool autoscale) {
  
  if (!bool_stack) {
    *curves = *curves_in;
  } else {
    vector<XOrsaPlotCurve>::iterator it_curve = curves_in->begin();
    while (it_curve != curves_in->end()) {
      curves->push_back(*it_curve);
      ++it_curve; 
    }
  }
  
  if (autoscale) { 
    ComputeLimits();
    ComputeOriginPosition(this);
  }
  
  pixmap_needs_update();
  update();
}


void XOrsaPlotArea::SetTitle(QString t) {
  title = t;  
  pixmap_needs_update();
  update();
}


// Extended plot area

XOrsaExtendedPlotArea::XOrsaExtendedPlotArea(int w, int h, QWidget *parent) : QWidget(parent,0,Qt::WDestructiveClose) {
  QVBoxLayout *vlay = new QVBoxLayout(this,2);
  area = new XOrsaPlotArea(w,h,this);
  vlay->addWidget(area);
  Init(vlay);
}

XOrsaExtendedPlotArea::XOrsaExtendedPlotArea(QWidget *parent) : QWidget(parent,0,Qt::WDestructiveClose) {
  QVBoxLayout *vlay = new QVBoxLayout(this,2);
  area = new XOrsaPlotArea(this);
  vlay->addWidget(area);
  Init(vlay);
}

XOrsaExtendedPlotArea::~XOrsaExtendedPlotArea() {
  // cerr << "XOrsaExtendedPlotArea::~XOrsaExtendedPlotArea() called..." << endl;
  // delete area;
}

void XOrsaExtendedPlotArea::TryLogX(bool b) { 
  area->X.SetLogScale(b);
  if (area->X.IsLogScale()!=b) log_x->setChecked(area->X.IsLogScale());
}

void XOrsaExtendedPlotArea::TryLogY(bool b) { 
  area->Y.SetLogScale(b);
  if (area->Y.IsLogScale()!=b) log_y->setChecked(area->Y.IsLogScale());
}

// da reinserire...
/* 
   void XOrsaExtendedPlotArea::syncLogCheckBox(XOrsaPlotAxis axis, bool b) {
   if (axisGetOrientation==) log_x->setChecked(b);
   if (axis==Y) log_y->setChecked(b);
   }
*/

void XOrsaExtendedPlotArea::syncLogCheckBox(bool) {
  log_x->setChecked(area->X.IsLogScale());
  log_y->setChecked(area->Y.IsLogScale());
}

void XOrsaExtendedPlotArea::Init(QVBoxLayout *vlay) {
  
  // QHBoxLayout *buttons_lay = new QHBoxLayout(vlay);
  
  QFrame *buttons_frame = new QFrame(this);
  // buttons_frame->setLineWidth(1);
  // buttons_frame->setMidLineWidth(2);
  buttons_frame->setFrameStyle( QFrame::Box | QFrame::Raised );
  
  QHBoxLayout *buttons_lay = new QHBoxLayout(buttons_frame,5);
  
  QCheckBox *connect_points = new QCheckBox(buttons_frame);
  connect_points->setText("connect points");
  buttons_lay->addWidget(connect_points);
  connect(connect_points,SIGNAL(toggled(bool)),area,SLOT(SetConnectPoints(bool)));
  connect(area,SIGNAL(ConnectChanged(bool)),connect_points,SLOT(setChecked(bool)));
  // connect_points->setChecked(true);
  //
  QCheckBox *same_scale = new QCheckBox(buttons_frame);
  same_scale->setText("scale 1:1");
  buttons_lay->addWidget(same_scale);
  connect(same_scale,SIGNAL(toggled(bool)),area,SLOT(SetSameScale(bool)));
  connect(area,SIGNAL(SameScaleChanged(bool)),same_scale,SLOT(setChecked(bool)));
  // connect_points->setChecked(true);
  //
  log_x = new QCheckBox(buttons_frame);
  log_x->setText("log X");
  buttons_lay->addWidget(log_x);
  connect(log_x,SIGNAL(toggled(bool)),this,SLOT(TryLogX(bool)));
  connect(&(area->X),SIGNAL(LogScaleChanged(bool)),this,SLOT(syncLogCheckBox(bool)));
  //
  log_y = new QCheckBox(buttons_frame);
  log_y->setText("log Y");
  buttons_lay->addWidget(log_y);
  connect(log_y,SIGNAL(toggled(bool)),this,SLOT(TryLogY(bool)));
  connect(&(area->Y),SIGNAL(LogScaleChanged(bool)),this,SLOT(syncLogCheckBox(bool)));
  //
  stack = new QCheckBox("stack",buttons_frame);
  buttons_lay->addWidget(stack);
  connect(stack,SIGNAL(toggled(bool)),area,SLOT(SetStack(bool)));
  //
  cb_x_range = new QCheckBox("[X]",buttons_frame);
  buttons_lay->addWidget(cb_x_range);
  connect(cb_x_range,SIGNAL(toggled(bool)),area,SLOT(SetFixXRange(bool)));
  //  
  cb_y_range = new QCheckBox("[Y]",buttons_frame);
  buttons_lay->addWidget(cb_y_range);
  connect(cb_y_range,SIGNAL(toggled(bool)),area,SLOT(SetFixYRange(bool)));
  //
  buttons_lay->addStretch();
  
  QPushButton *save_data_pb = new QPushButton(QIconSet(filesave),"save",buttons_frame);
  buttons_lay->addWidget(save_data_pb);
  connect(save_data_pb,SIGNAL(clicked()),area,SLOT(SaveData()));
  
  QPushButton *print_pb = new QPushButton(QIconSet(print_xpm),"print",buttons_frame);
  buttons_lay->addWidget(print_pb);
  connect(print_pb,SIGNAL(clicked()),area,SLOT(PrintArea()));
  
  QSizePolicy csp = buttons_frame->sizePolicy();
  csp.setVerData(QSizePolicy::Fixed);
  buttons_frame->setSizePolicy(csp);
  
  vlay->addWidget(buttons_frame);
}
