/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_NEW_OBJECT_KEPLERIAN_DIALOG_H_
#define _XORSA_NEW_OBJECT_KEPLERIAN_DIALOG_H_

#include <qdialog.h>

#include <vector>

class QLineEdit;
class QLabel;
class QRadioButton;
class QButtonGroup;

#include "xorsa_units_combo.h"
#include "xorsa_objects_combo.h"
#include "xorsa_date.h"

#include <orsa_orbit.h>
#include <orsa_units.h>

class XOrsaJPLPlanetsCombo;

class XOrsaNewObjectKeplerianDialog : public QDialog {
  
  Q_OBJECT
  
 public:
  
  // new
  XOrsaNewObjectKeplerianDialog(std::vector<orsa::BodyWithEpoch>&, QWidget *parent=0);
  
  // edit
  XOrsaNewObjectKeplerianDialog(std::vector<orsa::BodyWithEpoch>&, orsa::BodyWithEpoch&, QWidget *parent=0);
  
 public:
  orsa::BodyWithEpoch GetBody();
  
 private slots:
  void ok_pressed();
  void cancel_pressed();
  void fill_kepler_fields(const orsa::Orbit&);
  //
  void compute_orbit_from_body(orsa::Orbit&);
  void read_orbit_from_interface(orsa::Orbit&);
  void read_orbit_from_interface_and_update_body();
  void ref_body_changed();
  //
  void update_P();
  void update_M_from_P();
  //
  void action_changed();
  
 private:
  void init_draw();
  void init_values();
  
 private:
  QLineEdit *le_name;
  
  XOrsaImprovedObjectsCombo * ref_body_combo;
  XOrsaJPLPlanetsCombo * jpl_planets_combo;
  
  QLineEdit *le_ka, *le_ke, *le_ki, *le_knode, *le_kperi, *le_km;
  
  QLineEdit *le_mass;
  
  QPushButton *okpb;
  QPushButton *cancpb;
  
  LengthCombo *spacecb;
  MassCombo   *masscb;
  
  QRadioButton *M_rb, *P_rb;
  XOrsaDatePushButton *epoch, *P_epoch;
  
 private:
  bool filling_kepler_fields;
  
 public:
  bool ok; // wheter the ok button has been pressed
  
 private:
  orsa::BodyWithEpoch b, ref_body;
  
 private:
  bool internal_change;
  std::vector<orsa::BodyWithEpoch> &list;
  
  QButtonGroup *keplerian_elements_gb;
  
  const bool edit_mode;
  
  QRadioButton *rb_action_modify, *rb_action_convert;
  
 private:
  std::vector<orsa::Body> bodies_for_combo;
};

#endif // _XORSA_NEW_OBJECT_KEPLERIAN_DIALOG_H_
