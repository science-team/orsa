/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <orsa_error.h>
#include <orsa_units.h>

#include "xorsa_units_converter.h"

#include <qlineedit.h> 
#include <qvalidator.h> 
#include <qlayout.h>
#include <qtabwidget.h> 
#include <qvalidator.h> 
#include <qlabel.h>
#include <qhbox.h> 

using namespace std;
using namespace orsa;

// Length

LengthConverter::LengthConverter(QWidget *parent) : QWidget(parent) {
  
  QGridLayout *grid_lay = new QGridLayout(this,3,3,3,3);
  
  grid_lay->addWidget(new QLabel("from:",this),0,0,Qt::AlignRight);
  grid_lay->addWidget(new QLabel("to:",this),  1,0,Qt::AlignRight);
  
  line_from = new QLineEdit(this);
  line_from->setAlignment(Qt::AlignRight); 
  connect(line_from,SIGNAL(textChanged(const QString &)),this,SLOT(update()));
  grid_lay->addWidget(line_from,0,1);
  
  line_to = new QLineEdit(this);
  line_to->setAlignment(Qt::AlignRight); 
  line_to->setReadOnly(true); 
  grid_lay->addWidget(line_to,1,1);
  
  combo_from = new LengthCombo(this);
  connect(combo_from,SIGNAL(activated(int)),this,SLOT(update()));
  grid_lay->addWidget(combo_from,0,2);
  
  combo_to = new LengthCombo(this);
  connect(combo_to,SIGNAL(activated(int)),this,SLOT(update()));
  grid_lay->addWidget(combo_to,1,2);
  
  QDoubleValidator *valid = new QDoubleValidator(this);
  line_from->setValidator(valid);
  line_to->setValidator(valid);
  
  // init
  line_from->setText("1.0");
  combo_from->SetUnit(orsa::AU);
  combo_to->SetUnit(orsa::KM);
  update();
}

void LengthConverter::update() {  
  QString s;
  s.sprintf("%.12g",FromUnits(FromUnits(line_from->text().toDouble(),combo_from->GetUnit()),combo_to->GetUnit(),-1));
  line_to->setText(s);
}

// Mass

MassConverter::MassConverter(QWidget *parent) : QWidget(parent) {
  
  QGridLayout *grid_lay = new QGridLayout(this,3,3,3,3);
  
  grid_lay->addWidget(new QLabel("from:",this),0,0,Qt::AlignRight);
  grid_lay->addWidget(new QLabel("to:",this),  1,0,Qt::AlignRight);
  
  line_from = new QLineEdit(this);
  line_from->setAlignment(Qt::AlignRight); 
  connect(line_from,SIGNAL(textChanged(const QString &)),this,SLOT(update()));
  grid_lay->addWidget(line_from,0,1);
  
  line_to = new QLineEdit(this);
  line_to->setAlignment(Qt::AlignRight); 
  line_to->setReadOnly(true); 
  grid_lay->addWidget(line_to,1,1);
  
  combo_from = new MassCombo(this);
  connect(combo_from,SIGNAL(activated(int)),this,SLOT(update()));
  grid_lay->addWidget(combo_from,0,2);
  
  combo_to = new MassCombo(this);
  connect(combo_to,SIGNAL(activated(int)),this,SLOT(update()));
  grid_lay->addWidget(combo_to,1,2);
  
  QDoubleValidator *valid = new QDoubleValidator(this);
  line_from->setValidator(valid);
  line_to->setValidator(valid);
  
  // init
  line_from->setText("1.0");
  combo_from->SetUnit(orsa::MSUN);
  combo_to->SetUnit(orsa::KG);
  update();
}

void MassConverter::update() {  
  QString s;
  s.sprintf("%.12g",FromUnits(FromUnits(line_from->text().toDouble(),combo_from->GetUnit()),combo_to->GetUnit(),-1));
  line_to->setText(s);
}

// Time

TimeConverter::TimeConverter(QWidget *parent) : QWidget(parent) {
  
  QGridLayout *grid_lay = new QGridLayout(this,3,3,3,3);
  
  grid_lay->addWidget(new QLabel("from:",this),0,0,Qt::AlignRight);
  grid_lay->addWidget(new QLabel("to:",this),  1,0,Qt::AlignRight);
  
  line_from = new QLineEdit(this);
  line_from->setAlignment(Qt::AlignRight); 
  connect(line_from,SIGNAL(textChanged(const QString &)),this,SLOT(update()));
  grid_lay->addWidget(line_from,0,1);
  
  line_to = new QLineEdit(this);
  line_to->setAlignment(Qt::AlignRight); 
  line_to->setReadOnly(true); 
  grid_lay->addWidget(line_to,1,1);
  
  combo_from = new TimeCombo(this);
  connect(combo_from,SIGNAL(activated(int)),this,SLOT(update()));
  grid_lay->addWidget(combo_from,0,2);
  
  combo_to = new TimeCombo(this);
  connect(combo_to,SIGNAL(activated(int)),this,SLOT(update()));
  grid_lay->addWidget(combo_to,1,2);
  
  QDoubleValidator *valid = new QDoubleValidator(this);
  line_from->setValidator(valid);
  line_to->setValidator(valid);
  
  // init
  line_from->setText("1.0");
  combo_from->SetUnit(orsa::YEAR);
  combo_to->SetUnit(orsa::DAY);
  update();
}

void TimeConverter::update() {  
  QString s;
  s.sprintf("%.12g",FromUnits(FromUnits(line_from->text().toDouble(),combo_from->GetUnit()),combo_to->GetUnit(),-1));
  line_to->setText(s);
}

// PhysicalConstantsConverter

PhysicalConstantsConverter::PhysicalConstantsConverter(QWidget * parent) : QWidget(parent) {
  
  QGridLayout *grid_lay = new QGridLayout(this,3,3,3,3);
  
  grid_lay->addWidget(new QLabel("units:",this),0,0,Qt::AlignRight);
  grid_lay->addWidget(new QLabel("G:",    this),1,0,Qt::AlignRight);
  grid_lay->addWidget(new QLabel("c:",    this),2,0,Qt::AlignRight);
  
  QHBox * units_box = new QHBox(this);
  units_box->setSpacing(3);
  //
  length_combo = new LengthCombo(units_box);
  connect(length_combo,SIGNAL(activated(int)),this,SLOT(update()));
  //
  mass_combo   = new MassCombo(units_box);
  connect(mass_combo,  SIGNAL(activated(int)),this,SLOT(update()));
  //
  time_combo   = new TimeCombo(units_box);
  connect(time_combo,  SIGNAL(activated(int)),this,SLOT(update()));
  //
  grid_lay->addWidget(units_box,0,1);
  
  line_G = new QLineEdit(this);
  line_G->setAlignment(Qt::AlignRight); 
  line_G->setReadOnly(true); 
  //
  grid_lay->addWidget(line_G,1,1);
  
  line_c = new QLineEdit(this);
  line_c->setAlignment(Qt::AlignRight); 
  line_c->setReadOnly(true); 
  //
  grid_lay->addWidget(line_c,2,1);
  
  QDoubleValidator * valid = new QDoubleValidator(this);
  line_G->setValidator(valid);
  line_c->setValidator(valid);
  
  update();
}

void PhysicalConstantsConverter::update() {
  
  QString s;
  
  s.sprintf("%.12g",FromUnits(FromUnits(FromUnits(GetG(),
						  length_combo->GetUnit(),-3),
					mass_combo->GetUnit(),1),
			      time_combo->GetUnit(),2));
  line_G->setText(s);
  
  s.sprintf("%.12g",FromUnits(FromUnits(GetC(),
					length_combo->GetUnit(),-1),
			      time_combo->GetUnit(),1));
  line_c->setText(s);
}

// UnitsConverter

UnitsConverter::UnitsConverter(QWidget *parent) : QWidget(parent) {
  
  setCaption("units converter");
  
  setMinimumWidth(350);
  
  QVBoxLayout *vlay = new QVBoxLayout(this,3);
  
  QTabWidget *tab = new QTabWidget(this);
  vlay->addWidget(tab);
  
  tab->addTab(new LengthConverter, "Length");
  tab->addTab(new MassConverter,   "Mass");
  tab->addTab(new TimeConverter,   "Time");
  //
  tab->addTab(new PhysicalConstantsConverter, "Physical Constants");
  
  QSizePolicy csp = tab->sizePolicy();
  csp.setVerData(QSizePolicy::Fixed);
  tab->setSizePolicy(csp);
}
