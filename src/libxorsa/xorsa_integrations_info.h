/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_INTEGRATIONS_INFO_H_
#define _XORSA_INTEGRATIONS_INFO_H_

#include <vector>

#include <qgroupbox.h>
#include <qlistview.h>
#include <qpopupmenu.h>
#include <qtimer.h>

#include <orsa_universe.h>

#include <xorsa_wrapper.h>

class XOrsaIntegrationsInfo;

class XOrsaIntegrationItem : public QObject, public QListViewItem {
  
  Q_OBJECT
  
 public:
  XOrsaIntegrationItem(orsa::Evolution *e, QListView *parent, QString label1 = QString::null, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null);
  // XOrsaIntegrationItem(Evolution *e, XOrsaIntegrationsInfo *parent, QString label1 = QString::null, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null);
  ~XOrsaIntegrationItem();
  
 public:
  inline int compare(QListViewItem *i, int col, bool ascending) const {
    
    // integers
    // if (col == 0) return (atoi(key( col, ascending ).latin1()) - atoi( i->key(col, ascending).latin1()));
    
    // doubles
    if (col == 2 || col == 3 || col == 4 || col == 5 || col == 6) {
      using std::atof;
      const double d = (atof(key( col, ascending ).latin1()) - atof( i->key(col, ascending).latin1()));
      if (d < 0.0) return (-1);
      if (d > 0.0) return (+1);
      return (0);
    }
    
    // default
    return key( col, ascending ).compare( i->key(col, ascending));
  };
  
 private:
  void paintCell(QPainter*, const QColorGroup&, int, int, int);
  
 private:
  void customEvent(QCustomEvent*);
  
 private slots:
  void print_item();
  
 public slots:
  void plot_tool();
  void opengl_tool();
  void analyse_tool();
  void export_tool();
  void integration_copy();
  void delete_evolution();  
  void stop_integration();
  
 public:
  bool integrating() const { return e_ptr->integrating(); }
  
 private:
  QTimer timer_evolution;
  int    timer_evolution_msec;
  
 signals:
  bool closing_universe();
  void new_evolution();
  
 private:
  const orsa::Evolution * const e_ptr;
};

class XOrsaIntegrationsPopupMenu : public QPopupMenu {
  
  Q_OBJECT
  
 public:
  XOrsaIntegrationsPopupMenu(XOrsaIntegrationsInfo*, QWidget *parent=0);
  
 private slots:
  void widgets_enabler();
  
 private:
  int id_new, id_3D, id_2D, id_analyse, id_integration_copy, id_stop_integration, id_export, id_select_all, id_delete;
  
 public:
  XOrsaIntegrationsInfo *evolution_info;
};

class XOrsaIntegrationsInfo : public QWidget {
  
  Q_OBJECT
 
 public:
  XOrsaIntegrationsInfo(QWidget *parent=0);
  ~XOrsaIntegrationsInfo();
  
 public slots:
  void universe_modified();
  void popup(QListViewItem *, const QPoint &, int);
  void clear();
  // 
  void slot_new();
  void slot_opengl();
  void slot_plot();
  void slot_analyse();
  void slot_integration_copy();
  void slot_stop_integration();
  void slot_export();
  void slot_delete();
  void slot_select_all();
  
 private:
  void customEvent(QCustomEvent*);
  
 public:
  bool at_least_one_selected() const;
  bool at_least_one() const;
  bool at_least_one_selected_is_integrating() const;
  
 signals:
  bool closing_universe();
  void selectionChanged();
  void new_integration();
  void stopped_integration();
  
 public:
  QListView *listview;
  
 private:
  XOrsaIntegrationsPopupMenu *menu;
};

#endif // _XORSA_INTEGRATIONS_INFO_H_
