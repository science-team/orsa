/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_PLOT_TOOL_II_H_
#define _XORSA_PLOT_TOOL_II_H_

#include <orsa_universe.h>
#include <orsa_orbit.h>
#include <orsa_body.h>
#include <orsa_analysis.h>

class QPoint;
class QGroupBox;
class QStatusBar;
class QLabel;
class QLineEdit;
class QString;
class QTabWidget;
class QCheckBox;

#include "xorsa_plottype_combo.h"
#include "xorsa_objects_combo.h"
#include "xorsa_plot_area.h"

class XOrsaPlotTool_II : public QWidget {
  
  Q_OBJECT
  
 public:
  XOrsaPlotTool_II(const orsa::Evolution*, QWidget *parent=0);
  
 private:
  // QGroupBox *control_gb;
  QWidget *control;
  XOrsaKeplerPlotTypeCombo *kepler_type_combo;
  XOrsa2DPlotTypeCombo *twoD_type_combo;
  // XOrsaObjectsCombo *objects_combo, *robjects_combo_kepler, *robjects_combo_twoD;
  XOrsaImprovedObjectsCombo *objects_combo, *robjects_combo_kepler, *robjects_combo_twoD;
  XOrsaPlotArea *area, *area_kepler, *area_twoD;
  // QStatusBar *status_bar;
  QLabel     *status_bar_label;
  // QLineEdit *kepler_ref_body_label;
  // QPushButton *xospb;
  QTabWidget *tab;
  // XOrsaObjectsCombo *direction_combo;
  XOrsaImprovedObjectsCombo *direction_combo;
  
  private slots:
  void update_body(int);
  void update_rbody(int);
  void status_bar_plot_coords(QMouseEvent*);
  // void slot_connect_points(bool);
  void ComputeOrbitVector();
  void SetBodiesIndex();
  void SetPlotType();
  void SetArea(QWidget*);
  void FillPlotAreaData();
  void update_area(XOrsaPlotType type);
  //
  void slot_ref_body_fixed(bool);
  void slot_use_direction(bool);
  //
  void UpdateTitle();
  void update_dirbody(int);
  
 private: 
  unsigned int  body_index;
  unsigned int rbody_index;
  
 private:  
  orsa::OrbitStream os;
  
  std::vector<XOrsaPlotCurve> *curves;
  
  XOrsaPlotType plot_type;
  
  QCheckBox *use_direction;
  
  bool ref_body_is_fixed;
  bool use_direction_body;
  
 private:
  std::vector<orsa::Body> bodies;
  
 private:
  const orsa::Evolution *evol;
};

#endif // _XORSA_PLOT_TOOL_II_H_
