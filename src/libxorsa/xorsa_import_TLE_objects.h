/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_IMPORT_TLE_OBJECTS_H_
#define _XORSA_IMPORT_TLE_OBJECTS_H_

#include <vector>

#include <qwidget.h>
#include <qdialog.h>
#include <qapplication.h>
#include <qpushbutton.h> 
#include <qcombobox.h>

#include <orsa_body.h>
#include <orsa_file.h>

#include <xorsa_config.h>

class XOrsaTLEFileTypeCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  XOrsaTLEFileTypeCombo(QWidget * parent=0);
  
 private slots:
  void SetFileType(int);
  
 public slots:
  void SetFileType(orsa::ConfigEnum);
  
 public:  
  orsa::ConfigEnum GetFileType();
  
 private:
  orsa::ConfigEnum ft;
};

class XOrsaTLEFile : public orsa::TLEFile {
 public:
  void read_progress(int) {
    qApp->processEvents(100);
  }
};

class XOrsaImportTLEObjectsDialog : public QDialog {
  
  Q_OBJECT
  
 public:
  XOrsaImportTLEObjectsDialog(std::vector<orsa::BodyWithEpoch>&, QWidget *parent=0);
  
 public:
  bool ok;
  
 private slots:
  void update_file_entry();
  void ok_pressed();
  void cancel_pressed();
  
 private:
  XOrsaTLEFileTypeCombo * file_type_combo;
  XOrsaFileEntry        * fe;
  QPushButton           * okpb;
  QPushButton           * cancpb; 
  
 private:
  std::vector<orsa::BodyWithEpoch> &bodies;
};

#endif // _XORSA_IMPORT_TLE_OBJECTS_H_
