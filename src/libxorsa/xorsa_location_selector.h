/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_LOCATION_SELECTOR_H_
#define _XORSA_LOCATION_SELECTOR_H_

#include <qlistview.h>
#include <qdialog.h> 
#include <qpushbutton.h> 

class QPushButton;
class QListViewItem;

#include <orsa_file.h>

class XOrsaLocationSelector : public QDialog {
  
  Q_OBJECT

 public:
  XOrsaLocationSelector(QWidget *parent=0, const bool modal=false);
  
 private slots:
  void ok_pressed();
  void cancel_pressed();
  void fill_listview();
  void slot_enable_ok_button();
  
 private:
  QListView *listview;
  
 private:
  QPushButton *okpb;
  QPushButton *cancpb; 
  
 public:
  orsa::Location location_selected;
  
 public:
  bool ok; // wheter the ok button has been pressed
};

class XOrsaLocationPushButton : public QPushButton {
  
  Q_OBJECT
    
 public:
  XOrsaLocationPushButton(QWidget *parent=0);
  XOrsaLocationPushButton(orsa::Location&, QWidget *parent=0);
  
 signals:
  void LocationChanged();
  
 private slots:
  void slot_change_location(); 
  
 private slots:
  void update_label(); 
  
 public:
  orsa::Location location;
};

#endif // _XORSA_LOCATION_SELECTOR_H_
