/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_wrapper.h"

#include <qtextedit.h>
#include <qpushbutton.h>
#include <qapplication.h>
#include <qlayout.h> 
#include <qevent.h> 
#include <qdatetime.h> 

// XOrsaCustomEventManager

void XOrsaCustomEventManager::insert(const int event_type, QObject *obj) {
  assert(obj != 0);
  /* 
  ORSA_DEBUG("..adding QObject* to event list: %i",event_type);
  */
  receivers_map[event_type].push_back(obj);
  receivers_map[event_type].unique(); // this is a hack, but so what...
  connect(obj, SIGNAL(destroyed(QObject*)), this, SLOT(destructionNotify(QObject*)));
}

void XOrsaCustomEventManager::remove(const int event_type, QObject *obj) {
  if (!obj) return;
  receivers_map[event_type].remove(obj);
}

void XOrsaCustomEventManager::destructionNotify(QObject * obj) {
  for (map_type::iterator it = receivers_map.begin(); it != receivers_map.end(); ++it) {
    (*it).second.remove(obj);
  }
}

void XOrsaCustomEventManager::post_event(const int event_type) const {
  map_type::const_iterator list_it = receivers_map.find(event_type);
  if (list_it == receivers_map.end()) {
    // pair not found
    return;
  }
  list_type::const_iterator it = (*list_it).second.begin();
  while(it != (*list_it).second.end()) {
    QApplication::postEvent(*it, new QCustomEvent(event_type));
    ++it;
  }
}

// XOrsaDebugWidget

XOrsaDebugWidget::XOrsaDebugWidget(QWidget *parent) : QWidget(parent) {
  
  setCaption("Debug Window");
  
  QVBoxLayout *vlay = new QVBoxLayout(this,3,3); 
  
  te = new QTextEdit(this);
  te->setTextFormat(LogText);
  vlay->addWidget(te);
  
  QHBoxLayout *hbl = new QHBoxLayout(vlay);
  //
  hbl->addStretch();
  //
  QPushButton *clear_pb = new QPushButton("Clear",this);
  connect(clear_pb,SIGNAL(clicked()),te,SLOT(clear()));
  hbl->addWidget(clear_pb);
  //
  QPushButton *close_pb = new QPushButton("Close",this);
  connect(close_pb,SIGNAL(clicked()),this,SLOT(hide()));
  hbl->addWidget(close_pb);
}

void XOrsaDebugWidget::append(const QString &text) {
  show();
  te->append(text);
  qApp->processEvents(100);
}

void XOrsaDebugWidget::customEvent(QCustomEvent *e) {
  if ( e->type() == debug_event_type) {
    XOrsaDebugEvent *de = (XOrsaDebugEvent*)(e);
    append(de->DebugMessage());
  }
}

// XOrsaDebug

class XOrsaDebug::P {
public:
  XOrsaDebugWidget *dw;
  std::string msg;
  const char * file;
  int line;
};


void XOrsaDebug::construct(XOrsaDebugWidget * dw) {
  if (! m_instance) m_instance = new XOrsaDebug(dw);
}


XOrsaDebug::XOrsaDebug(XOrsaDebugWidget * dw) :
  orsa::Debug(),
  d(new P)
{ 
  d->dw = dw;
}


XOrsaDebug::~XOrsaDebug()
{
  delete d;
}


void XOrsaDebug::set(const char *msg, const char *file, const int line)
{
  if (doDefaultOutput) inherited::set(msg, file, line);
  d->msg = msg;
  d->file = file;
  d->line = line;
  doTrace = true;
}

void XOrsaDebug::vtrace(const char *fmt, std::va_list ap)
{
  if (doDefaultOutput) inherited::vtrace(fmt, ap);
  char str[1024];
  std::vsnprintf(str, sizeof(str) - 1, fmt, ap);
  char msg[1024];
  std::snprintf(msg, sizeof(msg) - 1, "[%s][%s:%i] %s %s\n", 
    QTime::currentTime(Qt::LocalTime).toString("hh:mm:ss").latin1(),d->file,d->line, d->msg.c_str(), str);
  QString s = msg;
  XOrsaDebugEvent *de = new XOrsaDebugEvent(s);
  QApplication::postEvent(d->dw,de);
}
