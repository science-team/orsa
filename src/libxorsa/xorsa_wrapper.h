/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_WRAPPER_H_
#define _XORSA_WRAPPER_H_

#include <list>
#include <map>

#include <orsa_universe.h>
#include <orsa_file.h>
#include <orsa_error.h>

#include <qapplication.h>
#include <qwidget.h>
#include <qobject.h>
#include <qmutex.h>

const int integration_step_done_event_type = QEvent::User + 101;
const int integration_started_event_type   = QEvent::User + 102;
const int integration_finished_event_type  = QEvent::User + 103;
const int evolution_modified_event_type    = QEvent::User + 104;
const int universe_modified_event_type     = QEvent::User + 105;

class XOrsaCustomEventManager : public QObject {
  
  Q_OBJECT
    
 public:
  void insert(const int event_type, QObject * obj);
  void remove(const int event_type, QObject * obj); // optional
  void post_event(const int event_type) const;
  
 public slots:
  void destructionNotify(QObject*);  
  
 private:
  typedef std::list<QObject*> list_type;
  typedef std::map<int,list_type> map_type;
  map_type receivers_map;
};

class XOrsaEvolution : public QObject, public orsa::Evolution {
  
  Q_OBJECT
    
 public:
  XOrsaEvolution() : QObject(), orsa::Evolution() { post_event(evolution_modified_event_type); };
    
 public:
  unsigned int size() const {
    mutex.lock();
    unsigned int s = std::vector<orsa::Frame>::size();
    mutex.unlock();
    return s;
  }
  
  void push_back(const orsa::Frame &f) { 
    mutex.lock();
    std::vector<orsa::Frame>::push_back(f); 
    post_event(evolution_modified_event_type);
    mutex.unlock();
  }
  
  reference operator[](size_type n) {
    return *(this->begin() + n);
  }
  
  const_reference operator[](size_type n) const {
    return *(this->begin() + n);
  }
  
  void clear() { 
    mutex.lock();     
    std::vector<orsa::Frame>::clear();
    post_event(evolution_modified_event_type);
    mutex.unlock();
  }
  
 public:
  iterator begin() { 
    mutex.lock();     
    iterator it = orsa::Evolution::begin();
    post_event(evolution_modified_event_type);
    mutex.unlock();
    return it;
  }
  
  iterator end() { 
    mutex.lock();
    iterator it = orsa::Evolution::end();
    post_event(evolution_modified_event_type);
    mutex.unlock();
    return it;
  }
  
  iterator erase(iterator position) { 
    mutex.lock();     
    iterator it = std::vector<orsa::Frame>::erase(position);
    post_event(evolution_modified_event_type);
    mutex.unlock();
    return it;    
  }
  
  const_iterator begin() const { 
    mutex.lock();     
    const_iterator it = orsa::Evolution::begin();
    mutex.unlock();
    return it;
  }
  
  const_iterator end() const { 
    mutex.lock();
    const_iterator it = orsa::Evolution::end();
    mutex.unlock();
    return it;
  }
  
 private:
  mutable XOrsaCustomEventManager event_manager;
  
 public:
  void add_event_receiver(const int event_type, QObject *obj) const {
    event_manager.insert(event_type,obj);
  }
  
  void remove_event_receiver(const int event_type, QObject *obj) const {
    event_manager.remove(event_type,obj);
  }
  
 private:
  void post_event(const int event_type) const {
    event_manager.post_event(event_type);
  }
  
 public slots:
  void stop_integration() { 
    orsa::Evolution::stop_integration();
  }
  
 public:
  /* 
     void step_done(double time_start, double time_stop, double timestep, orsa::Frame &frame_stop, bool &continue_integration) {
     if (mutex_step.tryLock()) {
     Evolution::step_done(time_start,time_stop,timestep,frame_stop,continue_integration);
     post_event(integration_step_done_event_type);
     mutex_step.unlock();
     }
     };
  */
  void step_done(const orsa::UniverseTypeAwareTime & time_start,
		 const orsa::UniverseTypeAwareTime & time_stop,
		 const orsa::UniverseTypeAwareTimeStep & timestep,
		 const orsa::Frame & frame_stop,
		 bool & continue_integration) { 
    if (mutex_step.tryLock()) {
      orsa::Evolution::step_done(time_start,time_stop,timestep,frame_stop,continue_integration);
      post_event(integration_step_done_event_type);
      mutex_step.unlock();
    }
  };
  
  void integration_started() {
    orsa::Evolution::integration_started();
    post_event(integration_started_event_type);
  };
  
  void integration_finished() {
    orsa::Evolution::integration_finished();
    post_event(integration_finished_event_type);
  };
  
 private:
  QMutex mutex_step;
  mutable QMutex mutex;
};

class XOrsaUniverse : public QObject, public orsa::Universe {
  
  Q_OBJECT
  
 public:
  XOrsaUniverse() : QObject(), orsa::Universe() { 
    post_event(universe_modified_event_type);
  };
  
  XOrsaUniverse(orsa::length_unit lu, orsa::mass_unit mu, orsa::time_unit tu, orsa::UniverseType ut=orsa::Simulated, orsa::ReferenceSystem rs=orsa::ECLIPTIC, orsa::TimeScale ts=orsa::ET) : QObject(), orsa::Universe(lu,mu,tu,ut,rs,ts) { 
    post_event(universe_modified_event_type);
  };
    
 public:
  unsigned int size() const {
    mutex.lock();
    unsigned int s;
    s = std::vector<orsa::Evolution*>::size();
    mutex.unlock();
    return s;
  }
  
  void push_back(orsa::Evolution * const e) { 
    mutex.lock();
    std::vector<orsa::Evolution*>::push_back(e); 
    post_event(universe_modified_event_type);
    mutex.unlock();
  }
  
  reference operator[](size_type n) {
    return *(begin() + n);
  }
  
  const_reference operator[](size_type n) const { 
    return *(begin() + n);
  }
  
  void clear() { 
    mutex.lock();     
    std::vector<orsa::Evolution*>::clear();
    post_event(universe_modified_event_type);
    mutex.unlock();
  }
  
  iterator begin() { 
    mutex.lock();     
    iterator it = orsa::Universe::begin();
    post_event(universe_modified_event_type);
    mutex.unlock();
    return it;
  }

  const_iterator begin() const {
    mutex.lock();
    const_iterator it = orsa::Universe::begin();
    // post_event(universe_modified_event_type);
    mutex.unlock();
    return it;
  }
  
  iterator end() { 
    mutex.lock();
    iterator it = orsa::Universe::end();
    post_event(universe_modified_event_type);
    mutex.unlock();
    return it;
  }

  const_iterator end() const {
    mutex.lock();
    const_iterator it = orsa::Universe::end();
    // post_event(universe_modified_event_type);
    mutex.unlock();
    return it;
  }
  
  iterator erase(iterator position) { 
    mutex.lock();     
    iterator it = std::vector<orsa::Evolution*>::erase(position);
    post_event(universe_modified_event_type);
    mutex.unlock();
    return it;    
  }
  
 private:
  mutable XOrsaCustomEventManager event_manager;
  
 public:
  void add_event_receiver(const int event_type, QObject *obj) const {
    event_manager.insert(event_type,obj);
  }
  
  void remove_event_receiver(const int event_type, QObject *obj) const {
    event_manager.remove(event_type,obj);
  }
  
 private:
  void post_event(const int event_type) const {
    event_manager.post_event(event_type);
  }
  
 private:
  mutable QMutex mutex;
};

class XOrsaFile : public QObject, public orsa::OrsaFile {
  
  Q_OBJECT
  
 public:
  XOrsaFile() : QObject(), orsa::OrsaFile() { };
  
 private:
  void make_new_universe(orsa::Universe **u, orsa::length_unit lu, orsa::mass_unit mu, orsa::time_unit tu, orsa::UniverseType ut, orsa::ReferenceSystem rs, orsa::TimeScale ts) {
    if (*u) delete (*u);    
    (*u) = new XOrsaUniverse(lu,mu,tu,ut,rs,ts);
  }
  
  void make_new_evolution(orsa::Evolution **e) {
    delete (*e);
    (*e) = new XOrsaEvolution;
  }  
};

const int debug_event_type = QEvent::User + 9;

class XOrsaDebugEvent : public QCustomEvent {
 public:
  XOrsaDebugEvent(const QString &s) : QCustomEvent(debug_event_type), msg(s) { }
  
 public:
  QString DebugMessage() { return msg; }
  
 private:
  QString msg;
};

class XOrsaDebugWidget : public QWidget {
  
  Q_OBJECT
  
 public:
  XOrsaDebugWidget(QWidget *parent=0);
  
 public slots:
  void append(const QString &text);
  
 public:
  void customEvent(QCustomEvent*);
  
 private:
  class QTextEdit *te;
};

class XOrsaDebug : public orsa::Debug {
 public:
  static void construct(XOrsaDebugWidget*);
  ~XOrsaDebug();
  void set(const char *msg, const char *file, const int line);
  void vtrace(const char *fmt, std::va_list ap); 
 private:
  XOrsaDebug(XOrsaDebugWidget*); // this class is not to be inherited from
  typedef orsa::Debug inherited;
  class P;
  P * d;
};

#endif // _XORSA_WRAPPER_H_
