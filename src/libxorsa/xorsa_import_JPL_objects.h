/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_IMPORT_JPL_OBJECTS_H_
#define _XORSA_IMPORT_JPL_OBJECTS_H_

#include <qdialog.h>
#include <qgroupbox.h>
#include <qhgroupbox.h>
#include <qvgroupbox.h>
#include <qcheckbox.h>
#include <qvbuttongroup.h> 
#include <qhbuttongroup.h> 
#include <qradiobutton.h> 
#include <qgrid.h> 
#include <qtable.h>

#include <orsa_frame.h>
#include <orsa_file.h>
#include <orsa_body.h>

#include "xorsa_date.h"

#include <vector>
#include <iostream>

class XOrsaEarthCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  XOrsaEarthCombo(QWidget *parent=0);
  
 private slots:
  void SetPlanet(int i);
  
 public slots:
  void SetPlanet(orsa::JPL_planets p);
  
 public:  
  orsa::JPL_planets GetPlanet() const;
  
 private:
  orsa::JPL_planets p;  
};


class XOrsaJPLPlanetsCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  XOrsaJPLPlanetsCombo(QWidget *parent=0);
  
 private slots:
  void SetPlanet(int i); 
  
 public slots:
  void SetPlanet(orsa::JPL_planets p);
  
 public:  
  orsa::JPL_planets GetPlanet() const;
  
 private:
  orsa::JPL_planets p;
};


class XOrsaJPLPlanetsTable : public QTable {
  
  Q_OBJECT
  
 public:
  XOrsaJPLPlanetsTable(QWidget *parent=0);   
  
 public:
  void AddSelectedPlanets(orsa::Frame &frame, bool bool_sun=true);
  
};

class XOrsaJPLPlanetsWidget : public QWidget {
  
  Q_OBJECT
    
 public:
  XOrsaJPLPlanetsWidget(QWidget *parent=0);   
  
 public:
  void AddSelectedPlanets(orsa::Frame&, const bool bool_sun=true);
  void AddSelectedPlanets(const orsa::Date&, orsa::Frame&, const bool bool_sun=true);
  void AddSelectedPlanets(const orsa::Date&, std::vector<orsa::BodyWithEpoch>&, const bool bool_sun=true);
  void GetSelectedPlanets(std::vector<orsa::JPL_planets>&, const bool bool_sun=true);
  
 public:
  void SetBodies(const std::vector<orsa::JPL_planets>&);
  
 public:
  void ClearBodies();
  void SetBody(orsa::JPL_planets);
  
 private:
  QCheckBox *cb_mercury, *cb_venus, *cb_earth, *cb_mars, *cb_jupiter, *cb_saturn, *cb_uranus, *cb_neptune, *cb_pluto;
  XOrsaEarthCombo * earth_combo;
};

/* 
   class XOrsaImportJPLObjectsDialog : public QDialog {
   
   Q_OBJECT
   
   public:
   XOrsaImportJPLObjectsDialog(std::vector<orsa::BodyWithEpoch>&, QWidget *parent=0);
   
   public:
   bool ok;
   
   private slots:
   void slot_JPL_file_browse();
   void slot_time_now();
   void ok_pressed();
   void cancel_pressed();
   //
   void set_file_path();
   
   private:
   QCheckBox *sun, *mercury, *venus, *mars, *jupiter, *saturn, *uranus, *neptune, *pluto;
   QRadioButton *earth, *earth_with_moon, *earth_moon_barycenter, *moon, *none;
   QLineEdit *file_le;
   
   XOrsaDate *date;
   
   std::vector<orsa::BodyWithEpoch> &list;
   };
*/

class XOrsaImportJPLObjectsWidgetDialog : public QDialog {
  
  Q_OBJECT
  
 public:
  XOrsaImportJPLObjectsWidgetDialog(std::vector<orsa::BodyWithEpoch>&, QWidget *parent=0);
  
 public:
  bool ok;
  
 private slots:
   // void slot_JPL_file_browse();
  void ok_pressed();
  void cancel_pressed();
  //
  // void set_file_path();
  
 private:
  // QLineEdit * file_le;
  XOrsaDate * date;
  XOrsaJPLPlanetsWidget * jpl_w;
  std::vector<orsa::BodyWithEpoch> & list;  
};

#endif // _XORSA_IMPORT_JPL_OBJECTS_H_
