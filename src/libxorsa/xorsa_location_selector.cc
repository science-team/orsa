/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_location_selector.h"

#include <orsa_secure_math.h>

#include <qlayout.h>
#include <qpushbutton.h> 
#include <qlistview.h>

using namespace std;
using namespace orsa;

class XOrsaLocationItem : public QListViewItem {
  
 public:
  XOrsaLocationItem(QListView *parent, QString label1, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8) { }
  
 public:
  int compare(QListViewItem * i, int col, bool ascending) const {
    
    // doubles
    if ((col == 1) || (col == 2)) {
      double d = (atof(key( col, ascending ).latin1()) - atof( i->key(col, ascending).latin1()));
      
      if (d < 0.0) return (-1);
      if (d > 0.0) return (+1);
      return (0);
    }
    
    // default
    return key( col, ascending ).compare( i->key(col, ascending));
  }
  
 public:
  Location location;
};


XOrsaLocationSelector::XOrsaLocationSelector(QWidget *parent, const bool modal) : QDialog(parent,0,modal) {
  
  if (isModal()) {
    setCaption("observer location selector");
  } else {
    setCaption("observer locations");
  }
  
  QVBoxLayout *vlay = new QVBoxLayout(this,4);
  
  listview = new QListView(this);
  //
  listview->setAllColumnsShowFocus(true);
  listview->setShowSortIndicator(true);
  listview->setSelectionMode(QListView::Single);
  listview->setItemMargin(3); // in pixels
  // listview->setResizeMode(QListView::LastColumn);
  //
  listview->addColumn("code");
  listview->addColumn("longitude");
  listview->addColumn("latitude");
  listview->addColumn("name");
  //
  // listview->setSorting(1,false);
  
  vlay->addWidget(listview);
  
  ////
  
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  
  if (isModal()) {
    hok->addStretch();
    //
    okpb = new QPushButton(this);
    okpb->setText("OK");
    hok->addWidget(okpb);
    connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
    //
    cancpb = new QPushButton(this);
    cancpb->setText("Cancel");
    hok->addWidget(cancpb);
    connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
    //
    hok->addStretch();
  } else {
    hok->addStretch();
    //
    /*  okpb = new QPushButton(this);
	okpb->setText("OK");
	hok->addWidget(okpb);
	connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
    */
    //
    cancpb = new QPushButton(this);
    cancpb->setText("Close");
    hok->addWidget(cancpb);
    connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
    // 
    hok->addStretch();
  }
  
  connect(listview,SIGNAL(selectionChanged()),this,SLOT(slot_enable_ok_button()));
  
  fill_listview();
  
  slot_enable_ok_button();
  
}

void XOrsaLocationSelector::fill_listview() {
  
  listview->clear();  
  
  /* 
     Config conf;
     OrsaConfigFile ocf(&conf);
     ocf.Read();
  */
  
  const string obscode_path = config->paths[OBSCODE]->GetValue().c_str();
  
  LocationFile lf;
  lf.SetFileName(obscode_path);
  lf.Read();
  
  // cerr << "locations read: " << lf.codes.size() << endl;
  
  if (lf.codes.size()) {
    
    QString s_code, s_longitude, s_latitude, s_name;
    
    list<string>::iterator itr;
    itr = lf.codes.begin();
    //
    while (itr != lf.codes.end()) {
      
      s_code.sprintf("%s",(*itr).c_str());
      
      if ( (lf.locations[(*itr)].lon == 0.0) && 
	   (lf.locations[(*itr)].pz  == 0.0) &&
	   (lf.locations[(*itr)].pxy == 0.0) ) {
      	s_longitude.sprintf("---");
	s_latitude.sprintf( "---");
      } else {    
	s_longitude.sprintf("%g",lf.locations[(*itr)].lon*(180/pi));
	s_latitude.sprintf( "%g",secure_atan2(lf.locations[(*itr)].pz,lf.locations[(*itr)].pxy)*(180/pi));
      }
      
      s_name.sprintf("%s",lf.locations[(*itr)].name.c_str());
      
      XOrsaLocationItem *li = new XOrsaLocationItem(listview, s_code, s_longitude, s_latitude, s_name);
      li->location = lf.locations[(*itr)];
      
      ++itr;
    }     
  }
}


void XOrsaLocationSelector::ok_pressed() {
  ok = true;
  XOrsaLocationItem *li;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      li = dynamic_cast <XOrsaLocationItem*> (it.current());
      if (li) {
	location_selected = li->location;
      }
      break;
    }
    ++it;
  }    
  done(0);
}


void XOrsaLocationSelector::cancel_pressed() {
  ok = false;  
  done(0);
}


void XOrsaLocationSelector::slot_enable_ok_button() {
  if (isModal()) {
    bool ok_enabled = false;
    QListViewItemIterator it = listview->firstChild();
    while ((it.current() != 0) && (ok_enabled == false)) {
      if (it.current()->isSelected()) ok_enabled = true;
      ++it;
    }
    okpb->setEnabled(ok_enabled);
  } 
}

// XOrsaLocationPushButton

XOrsaLocationPushButton::XOrsaLocationPushButton(QWidget *parent) : QPushButton(parent) {
  
  /* 
     Config conf;
     OrsaConfigFile ocf(&conf);
     ocf.Read();
  */
  const string obscode_path = config->paths[OBSCODE]->GetValue().c_str();
  
  LocationFile lf;
  lf.SetFileName(obscode_path);
  lf.Read();
  lf.Close();
  
  if (lf.codes.size()) {
    location = lf.locations[*lf.codes.begin()]; // first one
    update_label();
  } else {
    ORSA_ERROR("cannot find a valid location");
  }
  
  connect(this,SIGNAL(clicked()),this,SLOT(slot_change_location()));
}

XOrsaLocationPushButton::XOrsaLocationPushButton(Location &l, QWidget *parent) : QPushButton(parent), location(l) {
  update_label();
  connect(this,SIGNAL(clicked()),this,SLOT(slot_change_location()));
}

void XOrsaLocationPushButton::slot_change_location() {
  XOrsaLocationSelector ls(this,true);
  ls.show();
  ls.exec();
  if (ls.ok) {
    location = ls.location_selected;
    emit LocationChanged();
  }
  update_label();
}

void XOrsaLocationPushButton::update_label() {
  QString label;
  label.sprintf("observatory: %s",location.name.c_str());
  setText(label);
}
