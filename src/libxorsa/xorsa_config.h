/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_CONFIG_H_
#define _XORSA_CONFIG_H_


#include <list>
#include <map>

#include <qwidget.h>
#include <qtabdialog.h> 
#include <qscrollview.h> 

class QLineEdit;
class QPushButton;

#include <orsa_config.h>

enum XOrsaFileEntryMode {
  FE_OPEN_FILE,
  FE_SAVE_FILE
};

class XOrsaFileEntry : public QWidget {
   
  Q_OBJECT
  
 public:
  XOrsaFileEntry(const XOrsaFileEntryMode, QWidget *parent=0);
  
 public:
  void setText(const QString&);
  QString text() const;
  
 signals:
  void textChanged(const QString&);
  
 private slots:
  void browse();
  
 private:
  const XOrsaFileEntryMode mode;
  QLineEdit   * le;
  QPushButton * pb;
};

class XOrsaConfig : public QTabDialog {
  
  Q_OBJECT
  
 public:
  XOrsaConfig(const std::list<orsa::ConfigEnum>, QWidget *parent=0);
  
 private:
  QScrollView * paths_w;
  
 private:
  void draw_paths_w();
  void draw_paths_w_util(QWidget*, orsa::ConfigEnum);
  
 private slots:
  void save();
  
 private:
  void save_paths(orsa::ConfigEnum);
  
 private:
  std::map < orsa::ConfigEnum, XOrsaFileEntry * > map_paths;
  std::list< orsa::ConfigEnum> list_enum; 
};

#endif // _XORSA_CONFIG_H_
