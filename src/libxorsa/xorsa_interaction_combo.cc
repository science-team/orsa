/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_interaction_combo.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#ifdef HAVE_MPI
#include <mpi.h>
#endif // HAVE_MPI

#include <orsa_interaction.h>

using namespace orsa;

InteractionCombo::InteractionCombo(QWidget * parent) : QComboBox(false,parent) {
  
  insertItem("Newton");
  insertItem("Newton + Relativistic effects");
  insertItem("Gravitational TreeCode");
  insertItem("Galactic Potential (Allen)");
  insertItem("Galactic Potential (Allen) + Newton");
#ifdef HAVE_MPI
  insertItem("Newton (MPI)");
#endif  
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetInteraction(int)));
  
  // sync!
#ifdef HAVE_MPI
  int size;
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  if (size > 1) {
    setCurrentItem(count()-1);
    activated(count()-1);
  } else {
    setCurrentItem(0);
    activated(0);
  }
#else
  setCurrentItem(0);
  activated(0);
#endif
}

void InteractionCombo::SetInteraction(int i) {
  
  // look at the combobox for the right order
  switch (i) {
  case 0: t = NEWTON;                               break;
  case 1: t = RELATIVISTIC;                         break;
  case 2: t = GRAVITATIONALTREE;                    break;
  case 3: t = GALACTIC_POTENTIAL_ALLEN;             break;
  case 4: t = GALACTIC_POTENTIAL_ALLEN_PLUS_NEWTON; break;
#ifdef HAVE_MPI
  case 5: t = NEWTON_MPI;                           break;
#endif
  }
  
}

void InteractionCombo::SetInteraction(InteractionType t) {
  // look at the combobox for the right order
  switch (t) {
  case NEWTON:                               setCurrentItem(0); SetInteraction(0); break;
  case RELATIVISTIC:                         setCurrentItem(1); SetInteraction(1); break;
  case GRAVITATIONALTREE:                    setCurrentItem(2); SetInteraction(2); break;
  case GALACTIC_POTENTIAL_ALLEN:             setCurrentItem(3); SetInteraction(3); break;
  case GALACTIC_POTENTIAL_ALLEN_PLUS_NEWTON: setCurrentItem(4); SetInteraction(4); break;
  case ARMONICOSCILLATOR: /******************************************************/ break;
  case JPL_PLANETS_NEWTON: /*****************************************************/ break;
#ifdef HAVE_MPI
  case NEWTON_MPI:                           setCurrentItem(5); SetInteraction(5); break;
#endif
  }
  
}

InteractionType InteractionCombo::GetInteraction() {
  return t;
}
