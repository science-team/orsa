/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_objects_generator_keplerian_covariance_file.h"

#include <orsa_orbit_gsl.h>
#include <orsa_secure_math.h>

#include <qlayout.h>
#include <qpushbutton.h>  
#include <qlabel.h>
#include <qspinbox.h> 

using namespace std;

using namespace orsa;

XOrsaObjectsGeneratorKeplerianCovarianceFile::XOrsaObjectsGeneratorKeplerianCovarianceFile(vector<Asteroid> &gen, const vector<Asteroid> &par, QWidget *parent) : QDialog(parent,0,true), generated_asteroids(gen), parent_asteroids(par) {
  
  init_draw();
  
  QString caption;
  caption.sprintf("generate asteroids from covariance matrix for the %i asteroids selected",parent_asteroids.size());
  setCaption(caption);
}

void XOrsaObjectsGeneratorKeplerianCovarianceFile::init_draw() {
  
  QGridLayout *grid_lay = new QGridLayout(this,3,2,3,3);
  
  grid_lay->addWidget(new QLabel("asteroids generated for each orbit:",this),0,0);
  num_spin = new QSpinBox(1,10000,1,this);
  num_spin->setValue(32);
  grid_lay->addWidget(num_spin,0,1);
  
  grid_lay->addWidget(new QLabel("random seed:",this),1,0);
  seed_spin = new QSpinBox(1,1000000000,1,this);
  seed_spin->setValue(123456);
  grid_lay->addWidget(seed_spin,1,1);
  
  QWidget *bw = new QWidget(this);
  QHBoxLayout *hok = new QHBoxLayout(bw);
  //
  hok->addStretch();
  //
  okpb = new QPushButton(bw);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  cancpb = new QPushButton(bw);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
  //
  grid_lay->addMultiCellWidget(bw,2,2,0,1);
  
}

void XOrsaObjectsGeneratorKeplerianCovarianceFile::ok_pressed() {
  
  generated_asteroids.clear();
  
  unsigned int k,l;
  char fmt[1024];
  sprintf(fmt," [%%0%ii]",(int)(ceil(secure_log10(num_spin->value()))));
  // cerr << "fmt: " << fmt << endl;
  char buf[1024];
  vector<OrbitWithEpoch> local_gen_orb;
  Asteroid local_asteroid;
  for (k=0;k<parent_asteroids.size();++k) {
    // parent_asteroids[k].orb.GenerateUsingPrincipalAxisTransformation(local_gen_orb,num_spin->value(),seed_spin->value());  
    parent_asteroids[k].orb.GenerateUsingCholeskyDecomposition(local_gen_orb,num_spin->value(),seed_spin->value());  
    for (l=0;l<local_gen_orb.size();++l) {
      local_asteroid = parent_asteroids[k];
      // sprintf(buf," [%i]",l);
      sprintf(buf,fmt,l);
      local_asteroid.name.append(buf);
      local_asteroid.orb = local_gen_orb[l];
      local_asteroid.orb.epoch = parent_asteroids[k].orb.epoch;
      generated_asteroids.push_back(local_asteroid);
    }
  }
  
  done(0);
}

void XOrsaObjectsGeneratorKeplerianCovarianceFile::cancel_pressed() {
  done(0);
}
