/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_import_JPL_objects.h"

#include <qlayout.h>
#include <qcombobox.h>
#include <qcheckbox.h> 
#include <qradiobutton.h> 
#include <qvgroupbox.h> 
#include <qhgroupbox.h> 
#include <qlabel.h> 
#include <qhbuttongroup.h> 
#include <qvbuttongroup.h> 
#include <qpushbutton.h> 
#include <qlineedit.h>
#include <qgrid.h>
#include <qfiledialog.h>
#include <qhbox.h> 

#include <orsa_config.h>
#include <orsa_file.h>

#include <iostream>

using namespace orsa;
using namespace std;

// small utility
/* 
   void SetMRV(double & mass, Vector & r, Vector & v, JPLFile & jf, Date d, JPL_planets p) {
   mass = jf.GetMass(p);  
   jf.GetEph(d,p,r,v);
   }
*/

// XOrsaEarthCombo

XOrsaEarthCombo::XOrsaEarthCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("Earth");
  insertItem("Earth-Moon barycenter");
  insertItem("Earth and Moon");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetPlanet(int)));
  
  // sync!
  setCurrentItem(1);
  activated(1); 
}

void XOrsaEarthCombo::SetPlanet(int i) {
  // look at the combobox for the right order
  switch (i) {
  case 0: p = EARTH;                  break;
  case 1: p = EARTH_MOON_BARYCENTER;  break;
  case 2: p = EARTH_AND_MOON;         break;
  }
}

void XOrsaEarthCombo::SetPlanet(JPL_planets p) {
  // look at the combobox for the right order
  switch (p) {
  case EARTH:                 setCurrentItem(0); SetPlanet(0); break;
  case EARTH_MOON_BARYCENTER: setCurrentItem(1); SetPlanet(1); break;
  case EARTH_AND_MOON:        setCurrentItem(2); SetPlanet(2); break;
  default: /*************************************************/ break;
  }
}

JPL_planets XOrsaEarthCombo::GetPlanet() const {
  return p;
}

// XOrsaJPLPlanetsCombo

XOrsaJPLPlanetsCombo::XOrsaJPLPlanetsCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("Sun");
  insertItem("Mercury");
  insertItem("Venus");
  insertItem("Earth");
  insertItem("Moon");
  insertItem("Mars");
  insertItem("Jupiter");
  insertItem("Saturn"); 
  insertItem("Uranus"); 
  insertItem("Neptune"); 
  insertItem("Pluto");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetPlanet(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0); 
}

void XOrsaJPLPlanetsCombo::SetPlanet(int i) {
  // look at the combobox for the right order
  switch (i) {
  case 0:  p = SUN;     break;
  case 1:  p = MERCURY; break;
  case 2:  p = VENUS;   break;
  case 3:  p = EARTH;   break;
  case 4:  p = MOON;    break;
  case 5:  p = MARS;    break;
  case 6:  p = JUPITER; break;
  case 7:  p = SATURN;  break;
  case 8:  p = URANUS;  break;
  case 9:  p = NEPTUNE; break;
  case 10: p = PLUTO;   break;
  }
}

void XOrsaJPLPlanetsCombo::SetPlanet(JPL_planets p) {
  // look at the combobox for the right order
  switch (p) {
  case SUN:     setCurrentItem(0);  SetPlanet(0);  break;
  case MERCURY: setCurrentItem(1);  SetPlanet(1);  break;
  case VENUS:   setCurrentItem(2);  SetPlanet(2);  break;
  case EARTH:   setCurrentItem(3);  SetPlanet(3);  break;
  case MOON:    setCurrentItem(4);  SetPlanet(4);  break;
  case MARS:    setCurrentItem(5);  SetPlanet(5);  break;
  case JUPITER: setCurrentItem(6);  SetPlanet(6);  break;
  case SATURN:  setCurrentItem(7);  SetPlanet(7);  break;
  case URANUS:  setCurrentItem(8);  SetPlanet(8);  break;
  case NEPTUNE: setCurrentItem(9);  SetPlanet(9);  break;
  case PLUTO:   setCurrentItem(10); SetPlanet(10); break;
  default: /*************************************************/ break;
  }
}

JPL_planets XOrsaJPLPlanetsCombo::GetPlanet() const {
  return p;
}

// XOrsaJPLPlanetsTable

XOrsaJPLPlanetsTable::XOrsaJPLPlanetsTable(QWidget *parent) : QTable(9,2,parent) {
  
  setSelectionMode(QTable::NoSelection);
  setShowGrid(false);
  
  QHeader *header = horizontalHeader();
  header->setLabel( 0,"in",20);
  header->setLabel( 1,"options");
  // header->setMovingEnabled(TRUE);
  
  QHeader *names = verticalHeader();
  names->setLabel(0,"Mercury");
  names->setLabel(1,"Venus");
  names->setLabel(2,"Earth");
  names->setLabel(3,"Mars");
  names->setLabel(4,"Jupiter");
  names->setLabel(5,"Saturn");
  names->setLabel(6,"Uranus");
  names->setLabel(7,"Neptune");
  names->setLabel(8,"Pluto");
  
  unsigned int r=0;
  while (r<9) {
    setItem(r,0, new QCheckTableItem(this,0));
    setItem(r,1, new QTableItem(this,QTableItem::Never,0));
    ++r;
  }
  
  QStringList comboEntries;
  comboEntries << "E" << "E & M" << "E + M";
  QComboTableItem *item = new QComboTableItem(this, comboEntries, false);
  item->setCurrentItem(2);
  setItem(2,1,item);
  
}   

void XOrsaJPLPlanetsTable::AddSelectedPlanets(Frame&,bool) {
  
}

XOrsaJPLPlanetsWidget::XOrsaJPLPlanetsWidget(QWidget *parent) : QWidget(parent) {
  
  QVBoxLayout *vbox_lay = new QVBoxLayout(this,3);
  QGridLayout *grid_lay = new QGridLayout(vbox_lay,5,2,3);
  
  vbox_lay->addStretch(1);
  
  cb_mercury = new QCheckBox("Mercury",this); grid_lay->addWidget(cb_mercury,0,0,Qt::AlignLeft);
  cb_venus   = new QCheckBox("Venus",  this); grid_lay->addWidget(cb_venus,  0,1,Qt::AlignLeft);
  // the Earth will appear here
  cb_mars    = new QCheckBox("Mars",   this); grid_lay->addWidget(cb_mars,   2,0,Qt::AlignLeft);
  cb_jupiter = new QCheckBox("Jupiter",this); grid_lay->addWidget(cb_jupiter,2,1,Qt::AlignLeft);
  cb_saturn  = new QCheckBox("Saturn", this); grid_lay->addWidget(cb_saturn ,3,0,Qt::AlignLeft);  
  cb_uranus  = new QCheckBox("Uranus", this); grid_lay->addWidget(cb_uranus ,3,1,Qt::AlignLeft); 
  cb_neptune = new QCheckBox("Neptune",this); grid_lay->addWidget(cb_neptune,4,0,Qt::AlignLeft);  
  cb_pluto   = new QCheckBox("Pluto",  this); grid_lay->addWidget(cb_pluto  ,4,1,Qt::AlignLeft);
  
  // the Earth code
  QHBox *hbox = new QHBox(this); 
  hbox->setSpacing(3);
  cb_earth = new QCheckBox(hbox); 
  earth_combo = new XOrsaEarthCombo(hbox);
  QSizePolicy hsp = hbox->sizePolicy();
  hsp.setVerData(QSizePolicy::Fixed);
  hbox->setSizePolicy(hsp);
  grid_lay->addMultiCellWidget(hbox,1,1,0,1,Qt::AlignLeft);
  
  connect(cb_earth,SIGNAL(toggled(bool)),earth_combo,SLOT(setEnabled(bool)));
  cb_earth->setChecked(false);
  earth_combo->setEnabled(false);
}

void XOrsaJPLPlanetsWidget::AddSelectedPlanets(Frame &frame, const bool bool_sun) {
  
  // adds the selected objects to the frame;
  // the epoch is taken from the frame
  // const Date epoch = frame.GetDate();
  
  AddSelectedPlanets(frame.GetDate(),frame,bool_sun);
  
}

void XOrsaJPLPlanetsWidget::AddSelectedPlanets(const Date & epoch, Frame & frame, const bool bool_sun) {
  
  // date checks
  if (epoch < jpl_file->EphemStart()) { ORSA_ERROR("date requested is before ephem file start time!"); return; }
  if (epoch > jpl_file->EphemEnd())   { ORSA_ERROR("date requested is after ephem file end time!");    return; }
  
  if (bool_sun) { frame.push_back(jpl_cache->GetJPLBody(SUN,epoch)); }
  
  if (cb_mercury->isChecked()) { frame.push_back(jpl_cache->GetJPLBody(MERCURY,epoch)); }
  if (cb_venus->isChecked())   { frame.push_back(jpl_cache->GetJPLBody(VENUS,epoch)); }
  
  // now Earth and Moon
  if (cb_earth->isChecked()) { 
    switch(earth_combo->GetPlanet()) {
    case EARTH:
      frame.push_back(jpl_cache->GetJPLBody(EARTH,epoch));
      break;
    case EARTH_AND_MOON:
      frame.push_back(jpl_cache->GetJPLBody(EARTH,epoch));
      frame.push_back(jpl_cache->GetJPLBody(MOON,epoch));
      break;
    case EARTH_MOON_BARYCENTER:
      frame.push_back(jpl_cache->GetJPLBody(EARTH_MOON_BARYCENTER,epoch));
      break;
    default:
      break;
    }
  }
  
  if (cb_mars->isChecked())    { frame.push_back(jpl_cache->GetJPLBody(MARS,    epoch)); }
  if (cb_jupiter->isChecked()) { frame.push_back(jpl_cache->GetJPLBody(JUPITER, epoch)); }
  if (cb_saturn->isChecked())  { frame.push_back(jpl_cache->GetJPLBody(SATURN,  epoch)); }
  if (cb_uranus->isChecked())  { frame.push_back(jpl_cache->GetJPLBody(URANUS,  epoch)); }
  if (cb_neptune->isChecked()) { frame.push_back(jpl_cache->GetJPLBody(NEPTUNE, epoch)); }
  if (cb_pluto->isChecked())   { frame.push_back(jpl_cache->GetJPLBody(PLUTO,   epoch)); }
  
}

void XOrsaJPLPlanetsWidget::AddSelectedPlanets(const Date & epoch, vector<BodyWithEpoch> & frame, const bool bool_sun) {
  
  // date checks
  if (epoch < jpl_file->EphemStart()) { ORSA_ERROR("date requested is before ephem file start time!"); return; }
  if (epoch > jpl_file->EphemEnd())   { ORSA_ERROR("date requested is after ephem file end time!");    return; }
  
  if (bool_sun) { frame.push_back(jpl_cache->GetJPLBody(SUN,epoch)); }
  
  if (cb_mercury->isChecked()) { frame.push_back(jpl_cache->GetJPLBody(MERCURY,epoch)); }
  if (cb_venus->isChecked())   { frame.push_back(jpl_cache->GetJPLBody(VENUS,epoch)); }
  
  // now Earth and Moon
  if (cb_earth->isChecked()) { 
    switch(earth_combo->GetPlanet()) {
    case EARTH:
      frame.push_back(jpl_cache->GetJPLBody(EARTH,epoch));
      break;
    case EARTH_AND_MOON:
      frame.push_back(jpl_cache->GetJPLBody(EARTH,epoch));
      frame.push_back(jpl_cache->GetJPLBody(MOON,epoch));
      break;
    case EARTH_MOON_BARYCENTER:
      frame.push_back(jpl_cache->GetJPLBody(EARTH_MOON_BARYCENTER,epoch));
      break;
    default:
      break;
    }
  }
  
  if (cb_mars->isChecked())    { frame.push_back(jpl_cache->GetJPLBody(MARS,    epoch)); }
  if (cb_jupiter->isChecked()) { frame.push_back(jpl_cache->GetJPLBody(JUPITER, epoch)); }
  if (cb_saturn->isChecked())  { frame.push_back(jpl_cache->GetJPLBody(SATURN,  epoch)); }
  if (cb_uranus->isChecked())  { frame.push_back(jpl_cache->GetJPLBody(URANUS,  epoch)); }
  if (cb_neptune->isChecked()) { frame.push_back(jpl_cache->GetJPLBody(NEPTUNE, epoch)); }
  if (cb_pluto->isChecked())   { frame.push_back(jpl_cache->GetJPLBody(PLUTO,   epoch)); }
  
}

void XOrsaJPLPlanetsWidget::GetSelectedPlanets(vector<JPL_planets> &planets, const bool bool_sun) {
  
  planets.clear();
  
  // date checks
  // if (epoch.GetTime() <  jf.EphemStart().GetTime()) { cerr << "date requested is before ephem file start time!" << endl; return; }
  // if (epoch.GetTime() >  jf.EphemEnd().GetTime())   { cerr << "date requested is after ephem file end time!"    << endl; return; }
  
  if (bool_sun) { planets.push_back(SUN); }
  
  if (cb_mercury->isChecked()) { planets.push_back(MERCURY); }
  if (cb_venus->isChecked())   { planets.push_back(VENUS); }
  
  // now Earth and Moon
  if (cb_earth->isChecked()) { 
    switch(earth_combo->GetPlanet()) {
    case EARTH:
      planets.push_back(EARTH);
      break;
    case EARTH_AND_MOON:
      planets.push_back(EARTH);
      planets.push_back(MOON);
      break;
    case EARTH_MOON_BARYCENTER:
      planets.push_back(EARTH_MOON_BARYCENTER);
      break;
    default:
      break;
    }
  }
  
  if (cb_mars->isChecked())    { planets.push_back(MARS   ); }
  if (cb_jupiter->isChecked()) { planets.push_back(JUPITER); }
  if (cb_saturn->isChecked())  { planets.push_back(SATURN ); }
  if (cb_uranus->isChecked())  { planets.push_back(URANUS ); }
  if (cb_neptune->isChecked()) { planets.push_back(NEPTUNE); }
  if (cb_pluto->isChecked())   { planets.push_back(PLUTO  ); }
}

void XOrsaJPLPlanetsWidget::SetBodies(const vector<JPL_planets> &jf) {
  if (jf.size()) {
    ClearBodies();
    unsigned int k;
    for (k=0;k<jf.size();++k) {
      SetBody(jf[k]);
    }
  }
}

void XOrsaJPLPlanetsWidget::ClearBodies() {
  cb_mercury->setChecked(false);
  cb_venus->setChecked(false);
  cb_earth->setChecked(false);
  cb_mars->setChecked(false);
  cb_jupiter->setChecked(false);
  cb_saturn->setChecked(false);
  cb_neptune->setChecked(false);
  cb_uranus->setChecked(false);
  cb_pluto->setChecked(false);
}

void XOrsaJPLPlanetsWidget::SetBody(JPL_planets p) {
  
  switch(p) {
  case MERCURY:
    cb_mercury->setChecked(true);
    break;
  case VENUS:
    cb_venus->setChecked(true);
    break;
  case MARS:
    cb_mars->setChecked(true);
    break;
  case JUPITER:
    cb_jupiter->setChecked(true);
    break;
  case SATURN:
    cb_saturn->setChecked(true);
    break;
  case URANUS:
    cb_uranus->setChecked(true);
    break;
  case NEPTUNE:
  cb_neptune->setChecked(true);
    break;
  case PLUTO:   
    cb_pluto->setChecked(true);
    break;
  case EARTH:
    cb_earth->setChecked(true);
    earth_combo->SetPlanet(EARTH);
    break;
  case MOON:
    cb_earth->setChecked(true);
    earth_combo->SetPlanet(EARTH_AND_MOON);
    break;
  case EARTH_MOON_BARYCENTER:
    cb_earth->setChecked(true);
    earth_combo->SetPlanet(EARTH_MOON_BARYCENTER);
    break;
  case SUN:
    break;
  case NUTATIONS:
  case LIBRATIONS:
  case EARTH_AND_MOON:
  case SOLAR_SYSTEM_BARYCENTER:
  case NONE:
    break;
  }
  
}

////

XOrsaImportJPLObjectsWidgetDialog::XOrsaImportJPLObjectsWidgetDialog(vector<BodyWithEpoch> &list_in, QWidget *parent) : QDialog(parent,0,true), list(list_in) {
  
  setCaption("JPL import dialog");
  
  QVBoxLayout *vlay = new QVBoxLayout(this,3);
  
  
  /* 
     QHGroupBox *file_gb = new QHGroupBox("input file",this);
     new QLabel("JPL file:",file_gb);
     file_le = new QLineEdit(file_gb);
     set_file_path();
     QPushButton *pb_file_browse = new QPushButton("browse",file_gb);
     connect(pb_file_browse,SIGNAL(clicked()),this,SLOT(slot_JPL_file_browse()));
     vlay->addWidget(file_gb);
  */
  
  QVGroupBox *epoch_gb = new QVGroupBox("Epoch",this); 
  date = new XOrsaDate(epoch_gb);  
  vlay->addWidget(epoch_gb);
  
  jpl_w = new XOrsaJPLPlanetsWidget(this);
  vlay->addWidget(jpl_w);
  
  ////
  
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  
  hok->addStretch();
  //
  QPushButton *okpb = new QPushButton(this);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  QPushButton *cancpb = new QPushButton(this);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
}

/* 
   void XOrsaImportJPLObjectsWidgetDialog::slot_JPL_file_browse() {
   
   QString s =  QFileDialog::getOpenFileName(QString::null,
   QString::null,
   this,
   QString::null,
   "select file");
   if (!(s.isEmpty())) {
   file_le->setText(s);
   }
   }
*/

/* 
   void XOrsaImportJPLObjectsWidgetDialog::set_file_path() {
   QString path = config->paths[JPL_EPHEM_FILE]->GetValue().c_str();
   file_le->setText(path);
   }
*/

void XOrsaImportJPLObjectsWidgetDialog::cancel_pressed() {
  ok = false;
  done(0);
}


void XOrsaImportJPLObjectsWidgetDialog::ok_pressed() {
  
  const Date d = date->GetDate();
  
  jpl_w->AddSelectedPlanets(d,list,true);
  
  ok = true;
  done(0);
}
