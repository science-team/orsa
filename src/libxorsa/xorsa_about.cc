/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_about.h"

#include <orsa_version.h>

#include <orsa_icon.xpm>

#include <qapplication.h> 
#include <qvbox.h>
#include <qstring.h>
#include <qlabel.h>
#include <qlayout.h> 
#include <qpushbutton.h> 
#include <qsizepolicy.h> 

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#ifdef HAVE_FFTW
#include <fftw.h>
#endif // HAVE_FFTW

#ifdef HAVE_GSL
#include <gsl/gsl_version.h>
#endif // HAVE_GSL

#ifdef HAVE_LIBZ
#include <zlib.h>
#endif // HAVE_LIBZ

#ifdef HAVE_MPI
#include <mpi.h>
#endif // HAVE_MPI

#include "gl2ps.h"

XOrsaAbout::XOrsaAbout(QWidget *parent, const QString &program) : QWidget(parent,0,Qt::WType_TopLevel) {
  
  const QString qtversion(qVersion());
  
#ifdef HAVE_FFTW
  char  fv[12],dummy[7];
  sscanf(fftw_version,"%s %s",dummy,fv);
  // const QString fftwversion(fftw_version);
  const QString fftwversion(&fv[1]); // [1] to skip the leading V
#endif // HAVE_FFTW
  
#ifdef HAVE_MPI
  QString mpi_string;
  mpi_string.sprintf("%i.%i",MPI_VERSION,MPI_SUBVERSION);
#ifdef MPICH_VERSION
  mpi_string.append(" (MPICH ");
  mpi_string.append(MPICH_VERSION);
  mpi_string.append(")");
#endif
#ifdef LAM_MPI
  mpi_string.append(" (LAM/MPI)");
#endif
#endif // HAVE_MPI
  
  QString gl2ps_string;
  gl2ps_string.sprintf("%i.%i.%i",GL2PS_MAJOR_VERSION,GL2PS_MINOR_VERSION,GL2PS_PATCH_VERSION);
  
  QVBoxLayout *vlay = new QVBoxLayout(this,3);
  
  // QFrame *frame = new QFrame(this);
  // vlay->addWidget(frame);
  
  QHBoxLayout *hlay = new QHBoxLayout(vlay,3);
  // QHBoxLayout *hlay = new QHBoxLayout(frame,3);
  
  QLabel *icon_label = new QLabel(this);
  // QLabel *icon_label = new QLabel(frame);
  const QPixmap icon(orsa_icon_xpm);
  icon_label->setPixmap(icon);
  QVBoxLayout *icon_vlay = new QVBoxLayout(hlay,3);
  icon_vlay->addWidget(icon_label);
  icon_vlay->addStretch();
  
  QLabel *info = new QLabel(this);
  // QLabel *info = new QLabel(frame);
  info->setText("<h1>" + program + "</h1>"
		"<hr>"
		"ORSA - Orbit Reconstruction, Simulation and Analysis"
		"<br>"
		"Version " ORSA_VERSION " (" ORSA_RELEASE_DATE ")"
		"<br>"
		"<br>"
		"Copyright (C) 2002-2004 Pasquale Tricarico"
		"<br>"
		"<br>Compiled " ORSA_COMPILE_DATE " with: <br>"
		"<ul>"
#ifdef HAVE_MPI
		"<li> MPI " + mpi_string + "<br>"		
#endif // HAVE_MPI
#ifdef HAVE_FFTW
		"<li> FFTW " + fftwversion + "<br>"
#endif // HAVE_FFTW
		"<li> GL2PS " + gl2ps_string + "<br>"
#ifdef HAVE_GSL
		"<li> GSL " GSL_VERSION "<br>"
#endif // HAVE_GSL
		"<li> Qt " + qtversion + "<br>"
#ifdef HAVE_LIBZ
		"<li> zlib " + ZLIB_VERSION + "<br>"
#endif // HAVE_LIBZ
		"</ul>"
		"<br>"
		// "Website: <a href=\"http://orsa.sourceforge.net\">http://orsa.sourceforge.net</a>"
		"Website: <a href=\"http://orsa.sf.net\">http://orsa.sf.net</a>"
		"<br>"
		);
  
  hlay->addWidget(info);
  
  QPushButton *ok_pb = new QPushButton("OK",this);
  ok_pb->setMinimumWidth(120);
  
  QHBoxLayout *ok_pb_hlay = new QHBoxLayout(vlay,3);
  ok_pb_hlay->addStretch(); 
  ok_pb_hlay->addWidget(ok_pb);
  ok_pb_hlay->addStretch(); 
  
  connect(ok_pb,SIGNAL(clicked()),this,SLOT(close()));
  
  const QSize s = sizeHint();
  setMinimumSize(s);
  setMaximumSize(s);  
}
