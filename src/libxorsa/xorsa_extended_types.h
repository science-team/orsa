/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_EXTENDED_TYPES_H_
#define _XORSA_EXTENDED_TYPES_H_

#include <qobject.h> 

#include <orsa_coord.h>
#include <orsa_error.h>
#include <orsa_common.h>

class BoolObject : public QObject {
  
  Q_OBJECT
  
 public:
  BoolObject() : QObject() { 
    _b = false;
  }
  
  BoolObject(const bool b) : QObject() { 
    _b = b;
  }
  
 public:
  bool operator = (const bool b) {
    if (b != _b) {
      _b = b;
      emit changed();
    }
    return _b;
  }
  
  operator bool() const {
    return _b;
  }
  
 signals:
  void changed();
  
 private:
  bool _b;  
};

class IntObject : public QObject {
  
  Q_OBJECT
  
 public:
  IntObject() : QObject() { 
    _i = 0;
  }
    
  IntObject(const int i) : QObject() { 
    _i = i;
  }
  
 public:
  int operator = (const int i) {
    if (i != _i) {
      _i = i;
      emit changed();
    }
    return _i;
  }
  
  int operator = (const IntObject &int_obj) {
    if (int_obj != _i) {
      _i = int_obj;
      emit changed();
    }
    return _i;
  }
  
  operator int() const {
    return _i;
  }	
  
  // ++ and -- (prefix) operators 
  
 public:
  IntObject & operator ++ () {
    ++_i;
    emit changed();
    return *this;
  }
  
  IntObject & operator -- () {
    --_i;
    emit changed();
    return *this;
  }
  
 signals:
  void changed();
  
 private:
  int _i;
};

// periodic: from 0 to size-1;
// if equal to size-1, the ++ operator sets it to 0;
// if equal to 0, the -- operator sets it to size-1;
// useful to save a position inside a vector;
class SizeObject : public QObject {
  
  Q_OBJECT
  
 public:
  SizeObject() : QObject() { 
    internal_change = false;     
    _i = 0;
    _s = 1;
  }
  
 public:
  unsigned int operator = (const unsigned int i) {
    if (!internal_change) {
      internal_change = true;
      //
      if (i != _i) {
	_i = i % _s;
	emit changed();
      }
      //
      internal_change = false;
    }
    return _i;
  }
  
  operator unsigned int () const {
    return _i;
  }
  
  void SetSize(unsigned int s) {
    if (!internal_change) {
      internal_change = true;
      //
      if (s != _s) {
	_s = s;
	emit size_changed();
	_i = _i % _s;
      }
      //
      internal_change = false;
    }
  }
  
  unsigned int Size() const {
    return _s;
  }
  
  // ++ and -- (prefix) operators 
  
 public:
  SizeObject & operator ++ () {
    if (!internal_change) {
      internal_change = true;
      //
      _i = ++_i % _s;
      emit changed();
      //
      internal_change = false;
    }      
    return *this;
  }
  
  SizeObject & operator -- () {
    if (!internal_change) {
      internal_change = true;
      //
      _i = (_s + --_i) % _s;
      emit changed();
      //
      internal_change = false;
    }  
    return *this;
  }
  
 private:
  bool internal_change;
 signals:
  void changed();
  void size_changed();
 private:
  unsigned int _i;
 private:
  unsigned int _s;
};

class DoubleObject : public QObject {
  
  Q_OBJECT
  
 public:
  DoubleObject() : QObject() { 
    _d = 0.0;
  }
  
  DoubleObject(const double d) : QObject() { 
    _d = d;
  }
  
 public:
  double operator = (const double d) {
    if (d != _d) {
      _d = d;
      emit changed();
    }
    return _d;
  }
  
  operator double() const {
    return _d;
  }	
  
  DoubleObject & operator += (const DoubleObject & d) {
    if (d != 0.0) {
      _d += d;
      emit changed();
    }
    return *this;
  }
  
  DoubleObject & operator -= (const DoubleObject & d) {
    if (d != 0.0) {
      _d -= d;
      emit changed();
    }
    return *this;
  }
  
  DoubleObject & operator *= (const DoubleObject & d) {
    if (d != 1.0) {
      _d *= d;
      emit changed();
    }
    return *this;
  }
  
 signals:
  void changed();
  
 private:
  double _d;
};

class DoubleObjectWithLimits : public QObject {
  
  Q_OBJECT
  
 public:
  DoubleObjectWithLimits() : QObject() { 
    _d = _min = _max = 0.0;
  }
  
  DoubleObjectWithLimits(const double d) : QObject() { 
    _d = _min = _max = d;
  }
  
 public:
  double operator = (const double d) {
    if (d != _d) {
      _d = d;
      check_limits();
      emit changed();
    }
    return _d;
  }
  
  operator double() const {
    return _d;
  }
  
  void SetMin(double m) {
    if (m != _min) {
      _min = MIN(m,_max);
      _max = MAX(m,_max);
      check_limits();
      emit limits_changed();
    }
  }
  
  void SetMax(double M) {
    if (M != _max) {
      _min = MIN(M,_min);
      _max = MAX(M,_min);
      check_limits();
      emit limits_changed();
    }
  }
  
  void SetMinMax(double m, double M) {
    if ((m != _min) || (M != _max)) {
      _min = MIN(m,M);
      _max = MAX(m,M);
      check_limits();
      emit limits_changed();
    }
  }
  
  double min() const {
    return _min;
  }
  
  double max() const {
    return _max;
  }
  
  DoubleObjectWithLimits & operator += (const DoubleObjectWithLimits & d) {
    if (d != 0.0) {
      _d += d;
      check_limits();
      emit changed();
    }
    return *this;
  }
  
  DoubleObjectWithLimits & operator -= (const DoubleObjectWithLimits & d) {
    if (d != 0.0) {
      _d -= d;
      check_limits();
      emit changed();
    }
    return *this;
  }
  
  DoubleObjectWithLimits & operator *= (const DoubleObjectWithLimits & d) {
    if (d != 1.0) {
      _d *= d;
      check_limits();
      emit changed();
    }
    return *this;
  }
  
 signals:
  void changed();
  void limits_changed();
  
 private:
  double _d;
  
 private:
  double _min, _max;
  
 private:
  void check_limits() {
    if (_d < _min) {
      {
	ORSA_ERROR("DoubleObjectWithLimits::check_limits(): %g < %g",_d,_min);
      }
      _d = _min;
      emit changed();
    } 
    if (_d > _max) {
      {
	ORSA_ERROR("DoubleObjectWithLimits::check_limits(): %g > %g",_d,_max);
      }
      _d = _max;
      emit changed();
    }
  }
};


class DoubleObjectPeriodic : public QObject {
  
  Q_OBJECT
  
 public:
  DoubleObjectPeriodic() : QObject() { 
    _d = _min = _max = 0.0;
    locked = false;
  }
  
  DoubleObjectPeriodic(const double d) : QObject() { 
    _d = _min = _max = d;
    locked = false;
  }
  
 public:
  double operator = (const double d) {
    if (!locked && (d != _d)) {
      _d = d;
      check_limits();
      emit changed();
    }
    return _d;
  }
  
  operator double() const {
    return _d;
  }
  
  void SetMin(double m) {
    if (locked) {
      ORSA_WARNING("called DoubleObjectPeriodic::SetMin() for a locked variable...");
    }
    if (m != _min) {
      _min = MIN(m,_max);
      _max = MAX(m,_max);
      check_limits();
      emit limits_changed();
    }
  }
  
  void SetMax(double M) {
    if (locked) {
      ORSA_WARNING("called DoubleObjectPeriodic::SetMax() for a locked variable...");
    }
    if (M != _max) {
      _min = MIN(M,_min);
      _max = MAX(M,_min);
      check_limits();
      emit limits_changed();
    }
  }
  
  void SetMinMax(double m, double M) {
    if (locked) {
      ORSA_WARNING("called DoubleObjectPeriodic::SetMinMax() for a locked variable...");
    }
    if ((m != _min) || (M != _max)) {
      _min = MIN(m,M);
      _max = MAX(m,M);
      check_limits();
      emit limits_changed();
    }
  }
  
  double min() const {
    return _min;
  }
  
  double max() const {
    return _max;
  }
  
  void Lock(const double d) {
    if (locked && (d != _d)) {
      ORSA_WARNING("DoubleObjectPeriodic::Lock(): this variable was already locked to a different value...");
    }	
    _d = d;
    locked = true;
    emit changed();
  }
  
  void UnLock() {
    if (!locked) {
      ORSA_WARNING("DoubleObjectPeriodic::UnLock(): this variable was not locked...");
    }
    locked = false;
  }
  
  bool Locked() const {
    return locked;
  }
  
  DoubleObjectPeriodic & operator += (const DoubleObjectPeriodic & d) {
    if (!locked && (d != 0.0)) {
      _d += d;
      check_limits();
      emit changed();
    }
    return *this;
  }
  
  DoubleObjectPeriodic & operator -= (const DoubleObjectPeriodic & d) {
    if (!locked && (d != 0.0)) {
      _d -= d;
      check_limits();
      emit changed();
    }
    return *this;
  }
  
  DoubleObjectPeriodic & operator *= (const DoubleObjectPeriodic & d) {
    if (!locked && (d != 1.0)) {
      _d *= d;
      check_limits();
      emit changed();
    }
    return *this;
  }
  
 signals:
  void changed();
  void limits_changed();
  
 private:
  double _d;
  
 private:
  double _min, _max;
  
 private:
  bool locked;
  
 private:
  double periodic_mod(const double x, const double min, const double max) {
    using std::fmod;
    const double period = max - min;
    return (min+fmod(fmod(x-min,period)+period,period));
  }
  
 private:
  void check_limits() {
    if (locked) return;
    if (_d < _min) {
      _d = periodic_mod(_d,_min,_max);
      emit changed();
    } 
    if (_d > _max) {
      _d = periodic_mod(_d,_min,_max);
      emit changed();
    }
  }
};

class VectorObject : public QObject {
  
  Q_OBJECT
  
 public:
  VectorObject() : QObject() { 
    _v.Set(0,0,0);
  }

  VectorObject(const orsa::Vector &v) : QObject() { 
    _v = v;
  }
  
 public:
  orsa::Vector & operator = (const orsa::Vector &v) {
    if (v != _v) {
      _v = v;
      emit changed();
    }
    return _v;
  }
  
  operator orsa::Vector() const {
    return _v;
  }	
  
 signals:
  void changed();
  
 private:
  orsa::Vector _v;  
};

#endif // _XORSA_EXTENDED_TYPES_H_
