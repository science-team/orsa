/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_objects_generator_cartesian.h"

#include <qlayout.h>
#include <qpushbutton.h> 
#include <qvalidator.h> 
#include <qlabel.h> 
#include <qframe.h>
#include <qgroupbox.h> 
#include <qlineedit.h> 
#include <qspinbox.h> 

#include <gsl/gsl_rng.h> // random number generator

using namespace std;
using namespace orsa;

XOrsaObjectsGeneratorCartesian::XOrsaObjectsGeneratorCartesian(vector<BodyWithEpoch> &b, QWidget *parent) : QDialog(parent,0,true), bodies(b) {
  
  Date d;
  d.SetGregor(2000,1,1);
  epoch.SetDate(d);
  
  init_draw();
  
  setCaption("generate objects");
}

void XOrsaObjectsGeneratorCartesian::init_draw() {
  
  QVBoxLayout *vlay = new QVBoxLayout(this,4);
  
  // name and mass
  QGroupBox *name_mass_gb = new QGroupBox("name and mass",this);
  name_mass_gb->setColumns(1); // IMPORTANT: number of columns
  //
  QWidget *gw = new QWidget(name_mass_gb);
  QGridLayout *grid_lay = new QGridLayout(gw,2,3,3,3);
  
  grid_lay->addWidget(new QLabel("name",gw),0,0);
  le_name = new QLineEdit(gw);
  grid_lay->addMultiCellWidget(le_name,0,0,1,2);
  //
  switch (universe->GetUniverseType()) {
  case Real: { 
    name_mass_gb->setTitle("name");
    /* 
       new QLabel("mass range",name_mass_gb);
       new QLabel("0.0",name_mass_gb);
       new QLabel("0.0",name_mass_gb);
    */
    break;
  } 
  case Simulated: {
    
    grid_lay->addWidget(new QLabel("mass range",gw),1,0);
    le_mass_m = new QLineEdit(gw);
    grid_lay->addWidget(le_mass_m,1,1);
    le_mass_M = new QLineEdit(gw);
    grid_lay->addWidget(le_mass_M,1,2);
    grid_lay->addWidget(new QLabel("units",gw),2,0,Qt::AlignRight);
    masscb = new MassCombo(gw);
    masscb->SetUnit(units->GetMassBaseUnit());
    grid_lay->addMultiCellWidget(masscb,2,2,1,2);
    break;
  }
  }  
  //
  vlay->addWidget(name_mass_gb);
  
  // epoch
  if (universe->GetUniverseType() == Real) {
    QGroupBox *misc_gb = new QGroupBox(this);
    misc_gb->setColumns(2);
    
    new QLabel("epoch",misc_gb);
    odpb = new XOrsaDatePushButton(epoch,misc_gb);
    
    vlay->addWidget(misc_gb);
  }
  
  // position
  QGroupBox *position_gb = new QGroupBox("position",this);
  position_gb->setColumns(1); // IMPORTANT: number of columns
  //
  QWidget *pw = new QWidget(position_gb);
  QGridLayout *p_grid_lay = new QGridLayout(pw,4,3,3,3);
  //
  // QLabel *pxl = new QLabel(position_gb);
  // pxl->setText("X");
  p_grid_lay->addWidget(new QLabel("X",pw),0,0,Qt::AlignRight);
  le_px_m = new QLineEdit(pw);  
  p_grid_lay->addWidget(le_px_m,0,1);
  le_px_M = new QLineEdit(pw);  
  p_grid_lay->addWidget(le_px_M,0,2);
  //
  // QLabel *pyl = new QLabel(position_gb);
  // pyl->setText("Y");
  p_grid_lay->addWidget(new QLabel("Y",pw),1,0,Qt::AlignRight);
  le_py_m = new QLineEdit(pw);  
  p_grid_lay->addWidget(le_py_m,1,1);
  le_py_M = new QLineEdit(pw);  
  p_grid_lay->addWidget(le_py_M,1,2);
  //
  // QLabel *pzl = new QLabel(position_gb);
  // pzl->setText("Z");
  p_grid_lay->addWidget(new QLabel("Z",pw),2,0,Qt::AlignRight);
  le_pz_m = new QLineEdit(pw);  
  p_grid_lay->addWidget(le_pz_m,2,1);
  le_pz_M = new QLineEdit(pw);  
  p_grid_lay->addWidget(le_pz_M,2,2);
  //
  // QLabel *spacel = new QLabel(pw);
  // spacel->setText("units");
  p_grid_lay->addWidget(new QLabel("units",pw),3,0);
  pos_spacecb = new LengthCombo(pw);
  pos_spacecb->SetUnit(units->GetLengthBaseUnit());
  p_grid_lay->addMultiCellWidget(pos_spacecb,3,3,1,2);
  //
  vlay->addWidget(position_gb);
  
  // velocity
  QGroupBox *velocity_gb = new QGroupBox("velocity",this);
  velocity_gb->setColumns(1); // IMPORTANT: number of columns
  //
  QWidget *vw = new QWidget(velocity_gb);
  QGridLayout *v_grid_lay = new QGridLayout(vw,4,3,3,3);
  //
  // QLabel *vxl = new QLabel(velocity_gb);
  // vxl->setText("X");
  v_grid_lay->addWidget(new QLabel("X",vw),0,0,Qt::AlignRight);
  le_vx_m = new QLineEdit(vw);
  v_grid_lay->addWidget(le_vx_m,0,1);
  le_vx_M = new QLineEdit(vw);
  v_grid_lay->addWidget(le_vx_M,0,2);
  //
  // QLabel *vyl = new QLabel(velocity_gb);
  // vyl->setText("Y");
  v_grid_lay->addWidget(new QLabel("Y",vw),1,0,Qt::AlignRight);
  le_vy_m = new QLineEdit(vw);
  v_grid_lay->addWidget(le_vy_m,1,1);
  le_vy_M = new QLineEdit(vw);
  v_grid_lay->addWidget(le_vy_M,1,2);
  //
  // QLabel *vzl = new QLabel(velocity_gb);
  // vzl->setText("Z");
  v_grid_lay->addWidget(new QLabel("Z",vw),2,0,Qt::AlignRight);
  le_vz_m = new QLineEdit(vw);
  v_grid_lay->addWidget(le_vz_m,2,1);
  le_vz_M = new QLineEdit(vw);
  v_grid_lay->addWidget(le_vz_M,2,2);
  // 
  // new QLabel("units",velocity_gb);
  v_grid_lay->addWidget(new QLabel("units",vw),3,0);
  //
  QWidget *tw = new QWidget(vw);
  QHBoxLayout *vu_hbl = new QHBoxLayout(tw);
  //
  vel_spacecb = new LengthCombo(tw);
  vel_spacecb->SetUnit(units->GetLengthBaseUnit());
  vu_hbl->addWidget(vel_spacecb);
  //
  QLabel *sll = new QLabel(" / ",tw);
  vu_hbl->addWidget(sll);
  //
  vel_timecb = new TimeCombo(tw);
  vel_timecb->SetUnit(units->GetTimeBaseUnit());
  vu_hbl->addWidget(vel_timecb);
  //
  vu_hbl->addStretch(1);
  //
  v_grid_lay->addMultiCellWidget(tw,3,3,1,2);
  //
  vlay->addWidget(velocity_gb);
  
  QHBox *num_box = new QHBox(this);
  new QLabel("objects to generate",num_box);
  sb_num = new QSpinBox(1,10000,1,num_box); // you really have to generate more objects? edit here!
  vlay->addWidget(num_box);
  
  // validation
  QDoubleValidator *vd = new QDoubleValidator(this);
  //
  le_px_m->setValidator(vd);
  le_py_m->setValidator(vd);
  le_pz_m->setValidator(vd);
  le_px_M->setValidator(vd);
  le_py_M->setValidator(vd);
  le_pz_M->setValidator(vd);
  //
  le_vx_m->setValidator(vd);
  le_vy_m->setValidator(vd);
  le_vz_m->setValidator(vd);
  le_vx_M->setValidator(vd);
  le_vy_M->setValidator(vd);
  le_vz_M->setValidator(vd);
  // mass >= 0.0
  QDoubleValidator *vdz = new QDoubleValidator(this);
  vdz->setBottom(0.0);
  if (universe->GetUniverseType() == Simulated) {
    le_mass_m->setValidator(vdz);
    le_mass_M->setValidator(vdz);
  }
  
  // initial values
  if (universe->GetUniverseType() == Simulated) {
    le_mass_m->setText("0.0");
    le_mass_M->setText("0.0");
  }
  //
  le_px_m->setText("0.0");
  le_py_m->setText("0.0");
  le_pz_m->setText("0.0");
  le_px_M->setText("0.0");
  le_py_M->setText("0.0");
  le_pz_M->setText("0.0");
  //
  le_vx_m->setText("0.0");
  le_vy_m->setText("0.0");
  le_vz_m->setText("0.0");
  le_vx_M->setText("0.0");
  le_vy_M->setText("0.0");
  le_vz_M->setText("0.0");
  
  // push buttons
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  
  hok->addStretch();
  //
  okpb = new QPushButton(this);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  cancpb = new QPushButton(this);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
}

void XOrsaObjectsGeneratorCartesian::ok_pressed() {
  const int num = sb_num->value();
  
  // units
  length_unit lu_pos = pos_spacecb->GetUnit();
  length_unit lu_vel = vel_spacecb->GetUnit();
  time_unit   tu_vel = vel_timecb->GetUnit();
  
  // mass 
  double mass_m=0, mass_M=0;
  //
  switch (universe->GetUniverseType()) {
  case Real: { 
    mass_m = mass_M = 0.0;
    break;
  }
  case Simulated: {
    mass_unit mu = masscb->GetUnit();
    mass_m = FromUnits(le_mass_m->text().toDouble(),mu);
    mass_M = FromUnits(le_mass_M->text().toDouble(),mu);
    break;
  }
  }
  
  // position
  const double px_m = FromUnits(le_px_m->text().toDouble(),lu_pos);
  const double py_m = FromUnits(le_py_m->text().toDouble(),lu_pos);
  const double pz_m = FromUnits(le_pz_m->text().toDouble(),lu_pos);
  const double px_M = FromUnits(le_px_M->text().toDouble(),lu_pos);
  const double py_M = FromUnits(le_py_M->text().toDouble(),lu_pos);
  const double pz_M = FromUnits(le_pz_M->text().toDouble(),lu_pos);
  
  // velocity
  const double vx_m =  FromUnits(FromUnits(le_vx_m->text().toDouble(),lu_vel),tu_vel,-1);
  const double vy_m =  FromUnits(FromUnits(le_vy_m->text().toDouble(),lu_vel),tu_vel,-1);
  const double vz_m =  FromUnits(FromUnits(le_vz_m->text().toDouble(),lu_vel),tu_vel,-1);
  const double vx_M =  FromUnits(FromUnits(le_vx_M->text().toDouble(),lu_vel),tu_vel,-1);
  const double vy_M =  FromUnits(FromUnits(le_vy_M->text().toDouble(),lu_vel),tu_vel,-1);
  const double vz_M =  FromUnits(FromUnits(le_vz_M->text().toDouble(),lu_vel),tu_vel,-1);
  
  QString s_name,s_num;
  
  QString base_name = le_name->text().simplifyWhiteSpace().latin1();
  
  string b_name;
  double b_mass;
  Vector r,v;
  
  // random number generator
  const int random_seed = 124323; // should be user-defined...
  gsl_rng *rnd;
  rnd = gsl_rng_alloc(gsl_rng_gfsr4);
  gsl_rng_set(rnd,random_seed);
  
  int k;
  for (k=0;k<num;k++) {
    
    s_num.sprintf("%i",k);
    s_name = base_name + s_num;
    // b.name = s_name.latin1();
    b_name = s_name.latin1();
     
    // b.mass = mass_m+(mass_M-mass_m)*gsl_rng_uniform(rnd);
    b_mass = mass_m+(mass_M-mass_m)*gsl_rng_uniform(rnd);
    
    /* 
       b.position.Set(px_m+(px_M-px_m)*gsl_rng_uniform(rnd),
       py_m+(py_M-py_m)*gsl_rng_uniform(rnd),
       pz_m+(pz_M-pz_m)*gsl_rng_uniform(rnd));
       
       b.velocity.Set(vx_m+(vx_M-vx_m)*gsl_rng_uniform(rnd),
       vy_m+(vy_M-vy_m)*gsl_rng_uniform(rnd),
       vz_m+(vz_M-vz_m)*gsl_rng_uniform(rnd));
    */
    
    r.Set(px_m+(px_M-px_m)*gsl_rng_uniform(rnd),
	  py_m+(py_M-py_m)*gsl_rng_uniform(rnd),
	  pz_m+(pz_M-pz_m)*gsl_rng_uniform(rnd));
    
    v.Set(vx_m+(vx_M-vx_m)*gsl_rng_uniform(rnd),
	  vy_m+(vy_M-vy_m)*gsl_rng_uniform(rnd),
	  vz_m+(vz_M-vz_m)*gsl_rng_uniform(rnd));
    
    bodies.push_back(BodyWithEpoch(b_name,b_mass,r,v,epoch));
    // bodies[bodies.size()-1].epoch = epoch;
  }
  
  // free random number generator
  gsl_rng_free(rnd);
  
  done(0);  
}

void XOrsaObjectsGeneratorCartesian::cancel_pressed() {
  done(0);
}
