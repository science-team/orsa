/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_NEW_INTEGRATION_DIALOG_H_
#define _XORSA_NEW_INTEGRATION_DIALOG_H_

#include <qdialog.h> 
#include <qprogressbar.h>
#include <qlayout.h> 
#include <qlabel.h> 
#include <qapplication.h> 
#include <qthread.h>
#include <qmutex.h> 
#include <qpushbutton.h>

class QLineEdit;
class QTextEdit;
class QComboBox;

#include "xorsa_units_combo.h"
#include "xorsa_interaction_combo.h"
#include "xorsa_integrator_combo.h"
#include "xorsa_wrapper.h"
#include "xorsa_plot_area.h" // FineLabel()...
#include "xorsa_integrations_info.h"
#include "xorsa_all_objects_info.h"

#include <orsa_body.h>
#include <orsa_universe.h>

class XOrsaIntegrationProgress : public QWidget {
  
  Q_OBJECT
    
 public:
  XOrsaIntegrationProgress(XOrsaEvolution * evol, QWidget * parent=0) : QWidget(parent, 0, Qt::WType_TopLevel | Qt::WDestructiveClose) {
    
    QString caption;
    caption.sprintf("integration: %s",evol->orsa::Evolution::name.c_str());
    setCaption(caption);
    
    QVBoxLayout *vlay = new QVBoxLayout(this,4);
    
    if (orsa::universe->GetUniverseType() == orsa::Simulated) time_label = orsa::TimeLabel().c_str();
    
    QGroupBox *main_gb = new QGroupBox("info",this);
    main_gb->setColumns(2); // IMPORTANT: number of columns
    main_gb->setAlignment(Qt::AlignLeft);
    new QLabel("integrator:",main_gb);
    new QLabel(label(evol->GetIntegrator()->GetType()).c_str(),main_gb);
    new QLabel("start time:",main_gb);
    // QString sts;
    // sts.sprintf("%g %s",(*evol)[evol->size()-1].time,TimeLabel().c_str());
    QString start_time_string;
    // FineLabel(start_time_string,(*evol)[evol->size()-1].Time());
    FineDate(start_time_string,(*evol)[evol->size()-1]);
    // QString start_time_string_with_units = start_time_string + TimeLabel().c_str();
    // new QLabel(start_time_string_with_units,main_gb);
    new QLabel(start_time_string + " " + time_label,main_gb);
    new QLabel("stop time:",main_gb);
    stop_time_label = new QLabel(main_gb);
    new QLabel("current time:",main_gb);
    current_time_label = new QLabel(main_gb);
    new QLabel("current timestep:",main_gb);
    current_timestep_label = new QLabel(main_gb);
    
    vlay->addWidget(main_gb);
    
    QHBoxLayout *hlay = new QHBoxLayout(vlay,4);
    
    pb = new QProgressBar(1000,this);
    hlay->addWidget(pb);
    
    cancel = new QPushButton("cancel",this);
    hlay->addWidget(cancel);
    
    // connect(cancel,SIGNAL(clicked()),this,SLOT(slot_stop_integration()));
    
    stop_integration=false;
    stop_time_label_updated=false;
  };
  
 private slots:
  void slot_step_signal(double initial_time, double time_stop, double integrator_timestep, orsa::Frame &f, bool &continue_integration) {
    
    if (mutex.tryLock()) {
      
      qApp->lock();
      
      if (stop_integration) { 
	continue_integration=false;
	close();
      }
      
      if (!stop_time_label_updated) {
	 // QString sts;
	 // sts.sprintf("%g %s",time_stop,TimeLabel().c_str());
	QString stop_time_string;
	// FineLabel(stop_time_string,time_stop);
	FineDate(stop_time_string,time_stop);
	// QString stop_time_string_with_units = stop_time_string + TimeLabel().c_str();
	// stop_time_label->setText(sts);
	// stop_time_label->setText(stop_time_string_with_units);
	stop_time_label->setText(stop_time_string + " " + time_label);
	stop_time_label_updated=true;
      }
      
      // current_time_string.sprintf("%g %s",f.time,TimeLabel().c_str());
      // FineLabel(current_time_string,f.Time());
      FineDate(current_time_string,f);
      current_time_label->setText(current_time_string + " " + time_label);
      
      // current_timestep_string.sprintf("%g %s",integrator_timestep,TimeLabel().c_str());
      // FineLabel(current_timestep_string,integrator_timestep);
      orsa::time_unit current_timestep_tu = AutoTimeUnit(integrator_timestep);
      current_timestep_string.sprintf("%g %s",
				      FromUnits(integrator_timestep,current_timestep_tu,-1),
				      orsa::units->label(current_timestep_tu).c_str());
      
      current_timestep_label->setText(current_timestep_string);
      
      const double done = (f.Time()-initial_time)/(time_stop-initial_time);
      
      pb->setProgress((int)(done*1000));
      
      qApp->unlock();
      
      mutex.unlock();
    }
  }
  
  void slot_stop_integration() {
    stop_integration=true;
  }
  
 private:
  QPushButton  *cancel;
  
 private:
  QProgressBar *pb;
  QMutex mutex; 
  
 private:
  // QLabel *start_time_label;
  QLabel *stop_time_label;
  QLabel *current_time_label;
  QLabel *current_timestep_label;
  
 private:
  QString current_time_string;
  QString current_timestep_string;
  
  QString time_label;
  
 private:
  bool stop_integration;
  bool stop_time_label_updated;
};

////

class IntegrationThread : public QThread {
  
 public:
  IntegrationThread(XOrsaEvolution *evolution, const orsa::UniverseTypeAwareTime &time_stop_in) : QThread(), evol(evolution), time_stop(time_stop_in) {
      
  }
  
  void run() {
    evol->Integrate(time_stop);
  }
  
 private:
  XOrsaEvolution * const evol;
  orsa::UniverseTypeAwareTime time_stop;
};

////




////

class XOrsaNewIntegrationDialog : public QDialog {
  
  Q_OBJECT
  
 public:
  XOrsaNewIntegrationDialog(XOrsaEvolution *evol_in, QWidget * parent = 0);
  
 private slots:
  void ok_pressed();
  void cancel_pressed();
  void closeEvent(QCloseEvent*);
  
 private:
  QLineEdit *namele;
  
  // InteractionCombo *interactioncb;
  QCheckBox * rb_multipole;
  QCheckBox * rb_relativistic;
  QCheckBox * rb_fast_relativistic;
  QCheckBox * rb_force_JPL;
  
  IntegratorCombo  *integratorcb;
  
  QComboBox *machinecb;
  
  TimeCombo *timecb;
  
  QPushButton *okpb;
  QPushButton *cancpb; 
  
  XOrsaAllObjectsInfo *all_obj_info;
  
  QLineEdit *time_start_le;
  QLineEdit *time_stop_le;
  QLineEdit *time_incr_le; // integrator timestep
  QLineEdit *accuracy_le;  // integrator accuracy, when needed
  QLineEdit *sample_period_le; // evolution sample period
  
  XOrsaDatePushButton *start_time, *stop_time;
  
 private slots:
  void check_lines_integrator_enabled(int);
  
 private slots:
  void uncheck_rb_relativistic();
  void uncheck_rb_fast_relativistic();
  
 private:
  XOrsaEvolution * const evol;
  
 public:
  bool ok; // wheter the ok button has been pressed
};

#endif // _XORSA_NEW_INTEGRATION_DIALOG_H_
