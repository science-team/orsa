/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_OPENGL_H_
#define _XORSA_OPENGL_H_

// this header must go first, otherwise bcc55 will complain loudly
#include <qgl.h>

#include <string>
#include <map>

// GL2PS
#include "gl2ps.h"

#include <qmainwindow.h>
#include <qtabwidget.h> 
#include <qcombobox.h>
#include <qfont.h>
#include <qapplication.h>
#include <qtooltip.h> 
#include <qgroupbox.h>
#include <qdatetime.h> 
#include <qtimer.h>
#include <qslider.h>
#include <qtoolbutton.h> 

#include <orsa_universe.h>
#include <orsa_orbit.h>

#include "xorsa_units_combo.h"
#include "xorsa_location_selector.h"
#include "xorsa_objects_combo.h"
#include "xorsa_extended_types.h"

#include "support.h"

class QVBoxLayout;
class QPopupMenu;
class QPushButton;
class QLabel;
class QDockWindow;
class QVBox;
class QRadioButton;
class QCheckBox;

// icons
#include "play.xpm"
#include "lagrange.xpm"
#include "camera_position.xpm"
#include "rotation.xpm"

// get rid of windows cruft
#if defined(near)
#undef near
#endif
#if defined(far)
#undef far
#endif

QColor planet_color(const orsa::JPL_planets);

GLboolean invert(GLdouble src[16], GLdouble inverse[16]);

enum OpenGL_Labels_Mode { MASSLESS,
			  MASSIVE,
			  ALL };

// check gl2ps.h
enum OpenGL_Export_File { OGL_PS_FILE  = GL2PS_PS,
			  OGL_EPS_FILE = GL2PS_EPS,
			  OGL_PDF_FILE = GL2PS_PDF };

enum OpenGL_Projection { OGL_ORTHO,
			 OGL_PERSPECTIVE };

class XOrsaLabelsModeCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  XOrsaLabelsModeCombo(QWidget *parent=0);
  
 private slots:
  void SetLabelsMode(int);
  
 public slots:
  void SetLabelsMode(OpenGL_Labels_Mode);
  
 signals:
  void ModeChanged(OpenGL_Labels_Mode);
  
 public:  
  OpenGL_Labels_Mode GetLabelsMode();
  
 private:
  OpenGL_Labels_Mode m; 
};

class OrbitCache {
 public:
  OrbitCache() {
    set=false;
  }
  orsa::Orbit orbit;
  int ref_body_index;
  bool set;
};

// special ones
class ProjectionObject : public QObject {
  
  Q_OBJECT
  
 public:
  ProjectionObject() : QObject() { 
    _p = OGL_ORTHO;
  }
  
  ProjectionObject(const OpenGL_Projection p) : QObject() { 
    _p = p;
  }
  
 public:
  OpenGL_Projection operator = (const OpenGL_Projection p) {
    if (p != _p) {
      _p = p;
      emit changed();
    }
    return _p;
  }
  
  operator OpenGL_Projection() const {
    return _p;
  }	
  
 signals:
  void changed();
  
 private:
  OpenGL_Projection _p;  
};

class LabelsModeObject : public QObject {
  
  Q_OBJECT
  
 public:
  LabelsModeObject() : QObject() { 
    _m = MASSIVE;
  }
  
  LabelsModeObject(const OpenGL_Labels_Mode m) : QObject() { 
    _m = m;
  }
  
 public:
  OpenGL_Labels_Mode operator = (const OpenGL_Labels_Mode m) {
    if (m != _m) {
      _m = m;
      emit changed();
    }
    return _m;
  }
  
  operator OpenGL_Labels_Mode() const {
    return _m;
  }	
  
 signals:
  void changed();
  
 private:
  OpenGL_Labels_Mode _m;  
};


class FPS {
 public:
  FPS() : max_vec_size(64) {
    fps = 0.0;
    qtime.start();
    fps_running_average_index = 0;    
  }
  void sample() {
    const double sample_fps = 1000.0/qtime.elapsed();
    if (fps_running_average_vector.size() < max_vec_size) {
      fps_running_average_vector.push_back(sample_fps);
      fps = (fps*(fps_running_average_vector.size()-1)+sample_fps)/(fps_running_average_vector.size());
    } else {
      const double old_sample_fps = fps_running_average_vector[fps_running_average_index];
      fps_running_average_vector[fps_running_average_index] = sample_fps;
      fps += (sample_fps-old_sample_fps)/max_vec_size;
      fps_running_average_index = (fps_running_average_index+1)%(fps_running_average_vector.size());
    }
    qtime.restart();
  }
  
  operator double () const {
    return fps;
  }
  
  void reset() {
    fps = 0.0;
    fps_running_average_vector.clear();
    fps_running_average_index = 0;
    qtime.restart();
  }
 private:
  double  fps;
  QTime qtime;
  const unsigned int  max_vec_size;
  std::vector<double> fps_running_average_vector;
  unsigned int        fps_running_average_index;
};

// Tools

class XOrsaBoolToolButton : public QToolButton {
  
  Q_OBJECT
    
 public:
  XOrsaBoolToolButton(BoolObject *bool_obj, const QIconSet & iconSet, const QString & textLabel, QToolBar * parent, bool toggle_button=true) : QToolButton(iconSet,textLabel,QString::null,0,0,parent), obj(bool_obj) { 
    setToggleButton(toggle_button);
    internal_change = false;
    slot_object_changed();
    connect(obj,SIGNAL(changed()),this,SLOT(slot_object_changed()));
    connect(this,SIGNAL(toggled(bool)),this,SLOT(slot_button_toggled()));
  }
  
 private slots:
  void slot_object_changed() {
    if (!internal_change) {
      internal_change = true;
      //
      if (*obj) {
	setState(QButton::On);
      } else {
	setState(QButton::Off);
      }
      //
      internal_change = false;
    }
  }
  
  void slot_button_toggled() {
    if (!internal_change) {
      internal_change = true;
      //
      *obj = isOn();
      //
      internal_change = false;
    }
  }
  
 private:
  BoolObject * const obj;
 private: 
  bool internal_change;
};


class XOrsaSizeSliderTool : public QSlider {
  
  Q_OBJECT
    
 public:
  XOrsaSizeSliderTool(SizeObject * size_obj, QWidget * parent) : QSlider(parent), obj(size_obj) { 
        
    internal_change = false;
    //
    setMinimumWidth(50);
    // 
    setMinValue(0);
    setMaxValue(obj->Size()-1);
    setValue(0);
    //
    slot_object_changed();
    //
    connect(obj,SIGNAL(changed()),this,SLOT(slot_object_changed()));
    connect(obj,SIGNAL(size_changed()),this,SLOT(slot_object_size_changed()));
    //
    connect(this,SIGNAL(valueChanged(int)),this,SLOT(slot_slider_value_changed(int)));
    connect(this,SIGNAL(sliderPressed()),this,SLOT(slot_slider_pressed()));
    connect(this,SIGNAL(sliderReleased()),this,SLOT(slot_slider_released()));
  }
  
 private slots:
  void slot_object_changed() {
    if (!internal_change) {
      internal_change = true;
      //
      setValue(*obj);
      //
      internal_change = false;
    }
  }
  
  void slot_object_size_changed() {
    if (!internal_change) {
      internal_change = true;
      //
      setMaxValue(obj->Size()-1);
      //
      internal_change = false;
    }
  }
  
  void slot_slider_value_changed(int i) {
    if (!internal_change) {
      internal_change = true;
      //
      *obj = i;
      //
      internal_change = false;
    }
  }
  
 protected slots:
  virtual void slot_slider_pressed() {
    focusWidget()->clearFocus();
    setFocus();
  }
  
  virtual void slot_slider_released() {
    
  }
  
 private:
  SizeObject * const obj;
 private: 
  bool internal_change;
};



class XOrsaAnimationDelaySliderTool : public QSlider {
  
  Q_OBJECT
    
 public:
  XOrsaAnimationDelaySliderTool(IntObject * int_obj, QWidget * parent) : QSlider(parent), obj(int_obj) { 
        
    internal_change = false;
    //
    setMinimumWidth(50);
    // 
    // the slider values are in FPS
    setMinValue(1);
    setMaxValue(120);
    //
    setValue(25);
    //
    slot_object_changed();
    //
    connect(obj,SIGNAL(changed()),this,SLOT(slot_object_changed()));
    //
    connect(this,SIGNAL(valueChanged(int)),this,SLOT(slot_slider_value_changed(int)));
    connect(this,SIGNAL(sliderPressed()),this,SLOT(slot_slider_pressed()));
    connect(this,SIGNAL(sliderReleased()),this,SLOT(slot_slider_released()));
  }
  
 private:
  int slider_from_ms(const int ms) {
    return ((int)rint(1000.0/ms));
  }
  
  int ms_from_slider(const int slider) {
    return ((int)rint(1000.0/slider));
  }
  
 private slots:
  void slot_object_changed() {
    if (!internal_change) {
      internal_change = true;
      //
      setValue(slider_from_ms(*obj));
      //
      internal_change = false;
    }
  }
  
  void slot_slider_value_changed(int i) {
    if (!internal_change) {
      internal_change = true;
      //
      *obj = ms_from_slider(i);
      //
      internal_change = false;
    }
  }
  
 protected slots:
  virtual void slot_slider_pressed() {
    focusWidget()->clearFocus();
    setFocus();
  }
  
  virtual void slot_slider_released() {
    
  }
  
 private:
  IntObject * const obj;
 private: 
  bool internal_change;
};


class XOrsaDoubleObjectWithLimitsSliderTool : public QSlider {
  
  Q_OBJECT
    
 public:
  XOrsaDoubleObjectWithLimitsSliderTool(DoubleObjectWithLimits * double_obj, QWidget * parent) : QSlider(parent), obj(double_obj) { 
        
    internal_change = false;
    //
    setMinimumWidth(50);
    // 
    setMinValue(0);
    setMaxValue(1000);
    //
    slot_object_changed();
    //
    connect(obj,SIGNAL(changed()),this,SLOT(slot_object_changed()));
    connect(obj,SIGNAL(limits_changed()),this,SLOT(slot_object_changed()));
    //
    connect(this,SIGNAL(valueChanged(int)),this,SLOT(slot_slider_value_changed(int)));
    connect(this,SIGNAL(sliderPressed()),this,SLOT(slot_slider_pressed()));
    connect(this,SIGNAL(sliderReleased()),this,SLOT(slot_slider_released()));
  }
  
 private:
  double factor() const {
    /* 
       std::cerr << "factor: " << pow((obj->max()/obj->min()),1.0/(maxValue()-minValue())) << std::endl;
       std::cerr << "obj->max(): " << obj->max() << std::endl;
       std::cerr << "obj->min(): " << obj->min() << std::endl;
       std::cerr << "maxValue(): " << maxValue() << std::endl;
       std::cerr << "minValue(): " << minValue() << std::endl;
    */
    //
    if (obj->min()==0.0) {
      ORSA_WARNING("obj->min() == 0.0");
    }
    return std::pow((obj->max()/obj->min()),1.0/(maxValue()-minValue()));
  }
  
  double double_from_int(const int i) const {
    return (obj->min()*std::pow(factor(),i-minValue()));
  }
  
  int int_from_double(const double d) const {
    using std::log;
    return ((int)std::floor(log(d/obj->min())/log(factor()))+minValue());
  }
  
 private slots:
  void slot_object_changed() {
    if (!internal_change) {
      internal_change = true;
      //
      setValue(int_from_double(*obj));
      //
      internal_change = false;
    }
  }
  
  void slot_slider_value_changed(int i) {
    if (!internal_change) {
      internal_change = true;
      //
      *obj = double_from_int(i);
      //
      internal_change = false;
    }
  }
  
 protected slots:
  virtual void slot_slider_pressed() {
    focusWidget()->clearFocus();
    setFocus();
  }
  
  virtual void slot_slider_released() {
    
  }
  
 private:
  DoubleObjectWithLimits * const obj;
 private: 
  bool internal_change;
};



class XOrsaImprovedObjectsComboTool : public XOrsaImprovedObjectsCombo {
  
  Q_OBJECT
  
 public:
  XOrsaImprovedObjectsComboTool(IntObject * int_obj, QWidget * parent=0) : XOrsaImprovedObjectsCombo(parent), obj(int_obj) {
    init();
  }
  
  XOrsaImprovedObjectsComboTool(IntObject * int_obj, int special_objects_flags=0, QWidget * parent=0) : XOrsaImprovedObjectsCombo(special_objects_flags,parent), obj(int_obj) {
    init();
  }
					
  XOrsaImprovedObjectsComboTool(IntObject * int_obj, const std::vector<orsa::Body> * bodies, const bool only_massive=false, QWidget * parent=0) : XOrsaImprovedObjectsCombo(bodies,only_massive,parent), obj(int_obj) {
    init();
  }
  
  XOrsaImprovedObjectsComboTool(IntObject * int_obj, const std::vector<orsa::Body> * bodies, const bool only_massive=false, int special_objects_flags=0, QWidget * parent=0) : XOrsaImprovedObjectsCombo(bodies,only_massive,special_objects_flags,parent), obj(int_obj) {
    init();
  }
  
 private:
  void init() {
    
    internal_change = false;
    
    slot_object_changed();
    
    connect(obj,SIGNAL(changed()),this,SLOT(slot_object_changed()));
    //
    connect(this,SIGNAL(ObjectChanged(int)),this,SLOT(slot_combo_changed(int))); // here the ObjectChanged signal refers to the XOrsaImprovedObjectsCombo...
  }
  
 private slots:
  void slot_object_changed() {
    if (!internal_change) {
      internal_change = true;
      //
      SetObject(*obj);
      //
      internal_change = false;
    }
  }
  
  void slot_combo_changed(int i) {
    if (!internal_change) {
      internal_change = true;
      //
      *obj = i;
      //
      internal_change = false;
    }
  }
  
 private:
  IntObject * const obj;
 private: 
  bool internal_change;
};


// a rewrite of XOrsaOpenGL
class XOrsaOpenGLWidget : public QGLWidget {
  
  Q_OBJECT
    
 public:
  XOrsaOpenGLWidget(QWidget *parent=0, WFlags f=0);
  XOrsaOpenGLWidget(int w, int h, QWidget *parent=0, WFlags f=0);
  
 private:
  void init();
  void init_signals_slots();
  
 protected:
  void initializeGL();
  void resizeGL(int, int);
  void paintGL();
  
 private slots:
  void slot_call_resizeGL() { 
    resizeGL(width(),height());
    updateGL();
  }
  
 protected:
  virtual void draw() = 0;
  
  // utils
 public:
  void glTranslate(const orsa::Vector &v) const { glTranslated(v.x,v.y,v.z); }
  /* 
     orsa::Vector glTranslate(const orsa::Vector &v) const {  
     
     double m_pre[16];
     glGetDoublev(GL_MODELVIEW_MATRIX,m_pre);
     //
     double inverted_pre[16];
     invert(m_pre,inverted_pre);
     //
     glTranslated(v.x,v.y,v.z);
     //
     double m_post[16];
     glGetDoublev(GL_MODELVIEW_MATRIX,m_post);
     //
     double w[3];
     //
     w[0] = m_post[12]-m_pre[12];
     w[1] = m_post[13]-m_pre[13];
     w[2] = m_post[14]-m_pre[14];
     //
     orsa::Vector q;
     //
     q.x = w[0]*inverted_pre[0]+w[1]*inverted_pre[4]+w[2]*inverted_pre[8];
     q.y = w[0]*inverted_pre[1]+w[1]*inverted_pre[5]+w[2]*inverted_pre[9];
     q.z = w[0]*inverted_pre[2]+w[1]*inverted_pre[6]+w[2]*inverted_pre[10];
     //
     // note: v-q is the difference between the translation requested and 
     //       the real OpenGL translation: the two differ for many reasons,
     //       especially because OpenGL internally uses floats... :(
     //
     
     // return (v-q);
     return q;
     }
  */
  
  void glVertex(const orsa::Vector &v) const { glVertex3d(v.x,v.y,v.z); }
  
  // text, with gl2ps support
  void renderText(int x, int y, const QString &str, const QFont &fnt) {
    QGLWidget::renderText(x,y,"",fnt);
    const int fontsize=MAX(fnt.pixelSize(),fnt.pointSize());
    gl2psText(str,"Times-Roman",fontsize);
    QGLWidget::renderText(x,y,str,fnt);
  }
  
  void renderText(double x, double y, double z, const QString &str, const QFont &fnt) {
    QGLWidget::renderText(x,y,z,"",fnt);
    const int fontsize=MAX(fnt.pixelSize(),fnt.pointSize());
    gl2psText(str,"Times-Roman",fontsize);
    QGLWidget::renderText(x,y,z,str,fnt);  
  }
  
  void renderText(const orsa::Vector &v, const QString &str, const QFont &fnt) {
    renderText(v.x,v.y,v.z,str,fnt);
  }
  
  void renderText(int x, int y, const QString &str) {
    renderText(x,y,str,font());
  }
  
  void renderText(const orsa::Vector &v, const QString &str) {
    renderText(v,str,font());
  }
  
  //! length units when projection==OGL_ORTHO,
  //! degrees when projection==OGL_PERSPECTIVE.
  double PixelLength() const {
    double pixel_length = 0.0;
    switch(projection) {
    case OGL_ORTHO:
      pixel_length = 2.0*ortho_xy_scale/MIN(height(),width());
      break;
    case OGL_PERSPECTIVE:
      pixel_length = FOV/height(); // FOV is always along y (see man gluPerspective)
      break;
    }
    return pixel_length;
  }
  
 public:
  void export_file(const QString &, const OpenGL_Export_File);
 public slots:
  // handy export slots...
  void export_png();
  void export_ps();
  void export_pdf();
  
  // camera
 public:
  ProjectionObject projection;
  //
  DoubleObjectWithLimits distance;
  //
  DoubleObjectWithLimits FOV;
  DoubleObjectWithLimits near;
  DoubleObjectWithLimits far;
  //
  BoolObject near_and_far_limit_on_distance;
  //
  DoubleObjectWithLimits ortho_xy_scale;
  DoubleObjectWithLimits ortho_z_near_scale;
  DoubleObjectWithLimits ortho_z_far_scale;
  //
 private slots:
  void slot_near_changed();
  void slot_far_changed();
 private:
  bool bool_near_internal_change;
  bool bool_far_internal_change;
 private slots:
  void slot_near_limits_changed();
  void slot_far_limits_changed();
 private:
  bool bool_near_limits_internal_change;
  bool bool_far_limits_internal_change;
 private slots:
  void slot_near_and_far_limit_on_distance_changed();  
  
  // animation
 protected slots:
  virtual void animate();
 private slots:
  void slot_bool_animate_changed();
  void slot_animation_delay_changed();
 public:
  BoolObject bool_animate;
  IntObject  animation_delay_ms;
 protected:
  bool bool_already_animating;
 private:
  QTimer animation_timer;
 protected:
  void showEvent(QShowEvent*);
  void hideEvent(QHideEvent*);
  
  // center rotation
 public:
  DoubleObjectPeriodic center_X_rotation;
  DoubleObjectPeriodic center_Y_rotation;
  DoubleObjectPeriodic center_Z_rotation;
  //
  DoubleObjectWithLimits center_rotation_impulse;
  
  // eye rotation
 public:
  DoubleObjectPeriodic eye_X_rotation;
  DoubleObjectPeriodic eye_Y_rotation;
  DoubleObjectPeriodic eye_Z_rotation;
  //
  DoubleObjectWithLimits eye_rotation_impulse;
 private slots:
  void slot_update_eye_rotation_impulse();
  
 protected:
  void mousePressEvent(QMouseEvent*);
  void mouseReleaseEvent(QMouseEvent*);
  void mouseMoveEvent(QMouseEvent*);
  void wheelEvent(QWheelEvent*);
 private:
  QPoint old_mouse_position;
  
  // FPS
 private:
  FPS fps;
  int last_fps_frame_index;
 public:
  // get_fps: used by OSD, allows to keep FPS private...
  double get_fps() const {
    return fps; 
  }
};


class OSD;

class XOrsaOpenGLEvolutionWidget : public XOrsaOpenGLWidget {
  
  Q_OBJECT
  
 public:
  XOrsaOpenGLEvolutionWidget(QWidget *parent=0, WFlags f=0);
  XOrsaOpenGLEvolutionWidget(int w, int h, QWidget *parent=0, WFlags f=0);
  
 private:
  void init();
  
  // Evolution
 public:
  void SetEvolution(const orsa::Evolution*);
 private:
  const orsa::Evolution * evolution; 
  orsa::Frame evol_frame;
 public:
  SizeObject evol_counter;
 private:
  void update_sizes();
 signals:
  void evolution_changed();
  void frame_changed(int); 
  public slots:
 public:	
  unsigned int total_frames() const {
    if (evolution != 0) return evolution->size();
    return 0;
  }
 private:
  bool frame_changed_from_animation;
  
 private:
  void customEvent(QCustomEvent*);
  
 public:
  const orsa::Frame * bodies() const { 
    // should check more...
    if (evolution)
      return &(evolution[0][0]);
    else 
      return 0;
  };
  
 public slots:
  void export_movie();
  
  // scale
 private:
  DoubleObject range;
  unsigned int last_size_checked;
  void update_range();
 private slots:
  void range_changed() {
    distance.SetMax( 10.0*range);
    near.SetMax(     11.0*range);
    far.SetMax(      11.0*range);
    //
    ortho_xy_scale.SetMax(     11.0*range);
    ortho_z_near_scale.SetMax( 11.0*range);
    ortho_z_far_scale.SetMax(  11.0*range);
  }
  
 protected:
  void initializeGL();
  
 private:
  void draw();
  
 private:
  void check_and_call_list(const orsa::JPL_planets);
  
 private:
  GLint sun;
  GLint mercury;
  GLint venus;
  GLint earth; 
  GLint moon;
  GLint mars;
  GLint jupiter;
  GLint saturn;
  GLint uranus;
  GLint neptune;
  GLint pluto;
  
 private:
  GLuint     sun_texture;
  GLuint mercury_texture;
  GLuint   venus_texture;
  GLuint   earth_texture; 
  GLuint    moon_texture;
  GLuint    mars_texture;
  GLuint jupiter_texture;
  GLuint  saturn_texture;
  GLuint  uranus_texture;
  GLuint neptune_texture;
  GLuint   pluto_texture;

 private:
  GLint circle;
  GLint on_circle;
  GLint reference_system;
  GLint xy_grid;
  GLint sphere;
  //
  int max_circle_index;
  
 protected:
  void animate();
  
  // reference frame: center, eye and rotation
 public:
  IntObject  center_body;
  //
  IntObject  eye_body;
  BoolObject eye_on_body; // the eye position is locked on the eye_body;
  //
  IntObject  rotation_body;
  BoolObject rotate_with_rotation_body;
  
 private slots:
  void eye_body_changed() {
    if (eye_on_body) {
      rotation_body = eye_body;
      rotate_with_rotation_body = true;
      
      if ((eye_body >= 0) && (eye_body < (int)evol_frame.size())) {
	const double eye_body_radius = evol_frame[eye_body].radius();
	if (eye_body_radius > 0.0) {
	  // near.SetMin(1.05*eye_body_radius);
       	  near.SetMin(0.05*eye_body_radius);
       	  //
	  ortho_z_near_scale.SetMin(1.05*eye_body_radius);
	}
      }      
    }
  }
  
  void eye_on_body_changed() {
    // disconnect updateGL() from distance.changed() when
    // using eye_on_body, as the distance in this case is not
    // free. This should solve the problem of having paintGL()
    // called multiple times when eye_on_body is true.
    
    if (eye_on_body) {
      disconnect(&distance,SIGNAL(changed()),this,SLOT(updateGL()));
    } else {
      connect(&distance,SIGNAL(changed()),this,SLOT(updateGL()));
    } 
    
    // angles update
    if (eye_on_body) {
      center_X_rotation.Lock(270.0);
      center_Y_rotation.Lock(0.0);
      center_Z_rotation.Lock(90.0);
      //
      rotation_body = eye_body;
      rotate_with_rotation_body = true;
    } else {
      center_X_rotation.UnLock();
      center_Y_rotation.UnLock();
      center_Z_rotation.UnLock();
      //
      // rotate_with_rotation_body = false;
    }
  }
  
 private:
  orsa::Vector BodyPosition(const int) const;
  //
  orsa::Vector CenterBodyPosition() const;
  orsa::Vector EyeBodyPosition() const;
  orsa::Vector RotationBodyPosition() const;
  //
  orsa::Vector EyePosition() const { return eye_position; }
  //
  void  update_EyePosition();
  orsa::Vector eye_position;
  //
  std::string  BodyName(const int) const;
  //
  std::string  CenterBodyName() const;
  std::string  EyeBodyName() const;
  std::string  RotationBodyName() const;
  
 private slots:
  void evol_counter_changed() {
    evol_frame = (*evolution)[evol_counter];
  }
  
  void center_body_changed() {
    
    eye_X_rotation = eye_Y_rotation = eye_Z_rotation = 0.0;
    
    if (center_body >= 0) {
      const double center_body_radius = evol_frame[center_body].radius();
      if (center_body_radius > 0.0) {
	ortho_xy_scale.SetMin(     1.05*center_body_radius);
	//
	ortho_z_near_scale.SetMin( 1.05*center_body_radius);
	ortho_z_far_scale.SetMin(  1.05*center_body_radius); }
    }
  }
  
  void update_distance() {
    
    if (projection != OGL_PERSPECTIVE) return;
    
    if (center_body >= 0) {
      const double center_body_radius = evol_frame[center_body].radius();
      if (center_body_radius > 0.0) {
	distance.SetMin(1.10*center_body_radius);
      }
    }
    
    if ((eye_on_body) && (center_body != eye_body)) {
      distance = (EyeBodyPosition() - CenterBodyPosition()).Length();
    }    
  }
  
  void distance_changed() {
    
    if (near_and_far_limit_on_distance) {
      if ((center_body >= 0) && (center_body < (int)evol_frame.size())) {
	const double center_body_radius = evol_frame[center_body].radius();
	if (center_body_radius > 0.0) {
	  // this min should be already set, but...
	  distance.SetMin(1.10*center_body_radius);
	  //
	  near.SetMax(distance-1.05*center_body_radius);
	  far.SetMin( distance+1.05*center_body_radius);
	} else {
       	  near.SetMax(distance);
	  far.SetMin(distance);
	}
      } else {
	near.SetMax(distance);
	far.SetMin(distance);
      }
    }
  }
  
  // orbits
 public:
  BoolObject draw_orbits;
  IntObject orbit_reference_body_index;
  BoolObject bright_positive_Z;
 private:
  mutable std::vector < std::vector <int> > orbit_reference_body_index_vector;
  mutable std::vector < std::map < int , OrbitCache > > orbit_cache_vector;
  
  // MOID
 public:
  BoolObject draw_MOID;
  IntObject MOID_body_1;
  IntObject MOID_body_2;
  
  // Lagrange points
 public:
  BoolObject draw_Lagrange_points;
  IntObject Lagrange_body_1;
  IntObject Lagrange_body_2;
  
  // tracks
 public:
  BoolObject draw_tracks;
  BoolObject absolute_tracks;
  
  // misc
 public:
  BoolObject draw_reference_system;
  BoolObject draw_XY_grid;
  BoolObject draw_ra_dec_grid;
  BoolObject draw_Z_bars;
  BoolObject draw_scale_bar;
  BoolObject light_time_correction;
  
  // labels
 public:
  BoolObject       draw_labels;
  LabelsModeObject labels_mode;

 private:
  void internal_draw_bodies_and_labels();
  // void internal_draw_orbit_ellipse();
  void internal_draw_orbit_on_ellipse();
  void internal_draw_MOID();
  void internal_draw_Lagrange_points();
  //
  void internal_draw_OSD(); // On Screen Display
  friend class OSD;
  
};

enum OSD_ZONE {
  TOP_LEFT,
  TOP_CENTER,
  TOP_RIGHT,
  CENTER_LEFT,
  CENTER,
  CENTER_RIGHT,
  BOTTOM_LEFT,
  BOTTOM_CENTER,
  BOTTOM_RIGHT
};

class OSDZoneObject : public QObject {
  
  Q_OBJECT
  
 public:
  OSDZoneObject() : QObject() { 
    _z = TOP_LEFT;
  }
  
  OSDZoneObject(const OSD_ZONE z) : QObject() { 
    _z = z;
  }
  
 public:
  OSD_ZONE operator = (const OSD_ZONE z) {
    if (z != _z) {
      _z = z;
      emit changed();
    }
    return _z;
  }
  
  operator OSD_ZONE() const {
    return _z;
  }	
  
 signals:
  void changed();
  
 private:
  OSD_ZONE _z;  
};

class OSD : public QObject {
  
  Q_OBJECT
  
 public:
  OSD(XOrsaOpenGLEvolutionWidget * opengl_ew) : opengl(opengl_ew), fm(opengl->font()) {
    connect(&zone, SIGNAL(changed()), this, SLOT(zone_changed()));
    zone = TOP_LEFT;
    x = y = 0;
  }
  
 public:
  void draw();
  
 private slots:
  void zone_changed() {
    const int w  = opengl->width();
    const int h  = opengl->width();
    const int w2 = opengl->width()/2;
    const int h2 = opengl->width()/2;
    switch (zone) {
    case TOP_LEFT:      x = 0;  y = 0;  break;
    case TOP_CENTER:    x = w2; y = 0;  break;
    case TOP_RIGHT:     x = w;  y = 0;  break;
    case CENTER_LEFT:   x = 0;  y = h2; break;
    case CENTER:        x = w2; y = h2; break;
    case CENTER_RIGHT:  x = w;  y = h2; break;
    case BOTTOM_LEFT:   x = 0;  y = h;  break;
    case BOTTOM_CENTER: x = w2; y = h;  break;
    case BOTTOM_RIGHT:  x = w;  y = h;  break;
    }
  }
  
 private:
  void write(const QString & str) {
    const QRect str_rect = fm.boundingRect(str);
    //
    x = MAX(x,abs(str_rect.left())+fm.maxWidth()); // ..if close to left border
    x = MIN(x,opengl->width()-str_rect.right()-fm.maxWidth()); // ..if close to right border
    //
    y = MAX(y,fm.lineSpacing()); // ..if close to top border
    y = MIN(y,opengl->height()-fm.lineSpacing()); // ..if close to right border
    //
    opengl->renderText(x,y,str);
    //
    switch (zone) {
    case TOP_LEFT:
    case TOP_CENTER: 
    case TOP_RIGHT:
      y += fm.lineSpacing();
      break;
    case CENTER_LEFT:
    case CENTER:
    case CENTER_RIGHT:
      y += fm.lineSpacing();
      break;
    case BOTTOM_LEFT:
    case BOTTOM_CENTER:
    case BOTTOM_RIGHT:
      y -= 2 * fm.lineSpacing();
      break;
    }
  }
  
 public:
  OSDZoneObject zone;
  
 private:
  // cursor position...
  int x, y;
  
 private:
  XOrsaOpenGLEvolutionWidget * const opengl;
  
 private:
  const QFontMetrics fm;
};


class XOrsaOpenGLEvolutionTool : public QMainWindow {
  
  Q_OBJECT
    
 public:
  XOrsaOpenGLEvolutionTool(QWidget *parent=0);
  XOrsaOpenGLEvolutionTool(int w, int h, QWidget *parent=0);
  
  ~XOrsaOpenGLEvolutionTool();
  
 public:
  void SetEvolution(const orsa::Evolution*);
  
 private:
  void init_toolbars();
  
 private slots:
  void slot_set_orbit_reference_body_index(int);
  void slot_set_moid_body_1_index(int);
  void slot_set_moid_body_2_index(int);
  //
  void widgets_enabler();
  
 private:
  XOrsaBoolToolButton *plot_orbits_tb;
  // XOrsaAutoObjectsCombo *oc_orbits;
  XOrsaImprovedObjectsCombo *oc_orbits;
  XOrsaBoolToolButton *bright_positive_z_tb;
  //
  XOrsaBoolToolButton *moid_tb;
  // XOrsaObjectsCombo *oc_moid_1, *oc_moid_2;
  XOrsaImprovedObjectsCombo *oc_moid_1, *oc_moid_2;
  
 private:
  XOrsaOpenGLEvolutionWidget * opengl;
};







class TimeSlider : public XOrsaSizeSliderTool {
  
  Q_OBJECT
  
 public:
  TimeSlider(SizeObject * size_object, XOrsaOpenGLEvolutionWidget * opengl_ew, QWidget * parent=0) : XOrsaSizeSliderTool(size_object,parent), opengl(opengl_ew) {
    /* setWFlags(Qt::WRepaintNoErase);
       setBackgroundMode(Qt::NoBackground);
    */
  }
  
 protected slots:
  void slot_slider_pressed() {
    XOrsaSizeSliderTool::slot_slider_pressed();
    old_animation_state = opengl->bool_animate;
    opengl->bool_animate = false;
  }
  
  void slot_slider_released() {
    XOrsaSizeSliderTool::slot_slider_released();
    opengl->bool_animate = old_animation_state;
  }
  
 private:
  SizeObject * size_object;
  XOrsaOpenGLEvolutionWidget * const opengl;
 private:
  bool old_animation_state;
};











class XOrsaAnimationToolBar : public QToolBar {
 public:
  XOrsaAnimationToolBar(XOrsaOpenGLEvolutionWidget * opengl, QMainWindow * parent) : QToolBar(parent) {
    
    setLabel("Animation Tools");		  
    
    new XOrsaBoolToolButton(&opengl->bool_animate,QIconSet(play_xpm),"Start/Stop Animation",this); 
    
    TimeSlider * time_slider = new TimeSlider(&opengl->evol_counter,opengl,this);
    time_slider->setOrientation(Qt::Horizontal);
    QToolTip::add(time_slider,"Time Control");
    
    XOrsaAnimationDelaySliderTool * delay_slider = new XOrsaAnimationDelaySliderTool(&opengl->animation_delay_ms,this);
    delay_slider->setOrientation(Qt::Horizontal);
    QToolTip::add(delay_slider,"Animation Speed");
  }
};



class XOrsaLagrangePointsToolBar :  QToolBar {
  
  Q_OBJECT
    
 public:
  XOrsaLagrangePointsToolBar(XOrsaOpenGLEvolutionWidget * opengl_ew, QMainWindow * parent) : QToolBar(parent), opengl(opengl_ew) {
				  
    setLabel("Lagrange Points");		  
    
    enable_tb = new XOrsaBoolToolButton(&opengl->draw_Lagrange_points,QIconSet(lagrange_xpm),"Compute Lagrange Points Position",this); 
    connect(enable_tb,SIGNAL(toggled(bool)),this,SLOT(widgets_enabler()));
    
    combo_1 = new XOrsaImprovedObjectsComboTool(&opengl->Lagrange_body_1,0,this);
    QToolTip::add(combo_1,"Body 1");
    
    combo_2 = new XOrsaImprovedObjectsComboTool(&opengl->Lagrange_body_2,0,this);
    QToolTip::add(combo_2,"Body 2");
    
    connect(opengl,SIGNAL(evolution_changed()),this,SLOT(slot_evolution_changed()));
    
    widgets_enabler();
  } 
  
 private slots:
  void slot_evolution_changed() {
    const orsa::Frame * bodies = opengl->bodies();
    combo_1->Set(bodies,true);
    combo_2->Set(bodies,true);
  }
  
 private slots:  
  void widgets_enabler() {
    if (enable_tb->isOn()) {
      combo_1->setEnabled(true);
      combo_2->setEnabled(true);
    } else {
      combo_1->setEnabled(false);
      combo_2->setEnabled(false);
    }
  }
  
 private:
  XOrsaOpenGLEvolutionWidget * const opengl; 
  
 private:
  XOrsaBoolToolButton * enable_tb;
  
 private:
  XOrsaImprovedObjectsComboTool * combo_1;
  XOrsaImprovedObjectsComboTool * combo_2;
};

class XOrsaCameraToolBar : public QToolBar {
  
  Q_OBJECT
  
 public:
  XOrsaCameraToolBar(XOrsaOpenGLEvolutionWidget * opengl_ew, QMainWindow * parent) : QToolBar(parent), opengl(opengl_ew) {
			       
    setLabel("Camera Tools");
    
    center_cb = new XOrsaImprovedObjectsComboTool(&opengl->center_body, HEX_ORIGIN | HEX_CENTER_OF_MASS, this);
    QToolTip::add(center_cb,"Camera Center");
    
    connect(opengl,SIGNAL(evolution_changed()),this,SLOT(slot_evolution_changed()));
    
    distance_slider = new XOrsaDoubleObjectWithLimitsSliderTool(&opengl->distance,this);
    distance_slider->setOrientation(Qt::Horizontal);
    QToolTip::add(distance_slider,"Distance From Center");
    
    addSeparator();
    
    eye_tb = new XOrsaBoolToolButton(&opengl->eye_on_body,QIconSet(camera_position_xpm),"Enable/Disable Camera Position",this); 
    connect(eye_tb,SIGNAL(toggled(bool)),this,SLOT(widgets_enabler()));
    
    eye_cb = new XOrsaImprovedObjectsComboTool(&opengl->eye_body, HEX_ORIGIN | HEX_CENTER_OF_MASS,this);
    QToolTip::add(eye_cb,"Camera Position");
    
    addSeparator();
    
    rotation_tb = new XOrsaBoolToolButton(&opengl->rotate_with_rotation_body,QIconSet(rotation_xpm),"Enable/Disable Rotation",this); 
    connect(rotation_tb,SIGNAL(toggled(bool)),this,SLOT(widgets_enabler()));
    
    rotation_cb = new XOrsaImprovedObjectsComboTool(&opengl->rotation_body, HEX_ORIGIN | HEX_CENTER_OF_MASS, this);
    QToolTip::add(rotation_cb,"Rotation Body");
    
    addSeparator();
    
    projection_cb = new QComboBox(false,this);
    //
    projection_cb->insertItem("Orthographic");
    projection_cb->insertItem("Perspective");
    //
    connect(projection_cb,SIGNAL(activated(const QString &)),this,SLOT(slot_projection_changed()));
    //
    QToolTip::add(projection_cb,"Camera Projection");
    
    FOV_slider = new XOrsaDoubleObjectWithLimitsSliderTool(&opengl->FOV,this);
    FOV_slider->setOrientation(Qt::Horizontal);
    QToolTip::add(FOV_slider,"Field Of View");
    
    // or...
    
    ortho_xy_slider = new XOrsaDoubleObjectWithLimitsSliderTool(&opengl->ortho_xy_scale,this);
    ortho_xy_slider->setOrientation(Qt::Horizontal);
    QToolTip::add(ortho_xy_slider,"Scale");
    
    addSeparator();
    
    near_slider = new XOrsaDoubleObjectWithLimitsSliderTool(&opengl->near,this);
    near_slider->setOrientation(Qt::Horizontal);
    QToolTip::add(near_slider,"Near Clipping Plane Distance");
    
    far_slider = new XOrsaDoubleObjectWithLimitsSliderTool(&opengl->far,this);
    far_slider->setOrientation(Qt::Horizontal);
    QToolTip::add(far_slider,"Far Clipping Plane Distance");
    
    // or...
    
    ortho_z_near_slider = new XOrsaDoubleObjectWithLimitsSliderTool(&opengl->ortho_z_near_scale,this);
    ortho_z_near_slider->setOrientation(Qt::Horizontal);
    QToolTip::add(ortho_z_near_slider,"Depth Clipping Plane Near Distance");
    
    ortho_z_far_slider = new XOrsaDoubleObjectWithLimitsSliderTool(&opengl->ortho_z_far_scale,this);
    ortho_z_far_slider->setOrientation(Qt::Horizontal);
    QToolTip::add(ortho_z_far_slider,"Depth Clipping Plane Far Distance");
    
    slot_projection_changed();
  }
  
 private:
  OpenGL_Projection projection() const {
    OpenGL_Projection p;
    const QString projection_combo_text = projection_cb->currentText();
    if (projection_combo_text=="Orthographic") {
      p = OGL_ORTHO;
    } else if (projection_combo_text=="Perspective") {
      p = OGL_PERSPECTIVE;
    } else {
      ORSA_ERROR("XOrsaCameraToolBar::projection(): can't handle [%s] projection",projection_combo_text.latin1());
      //
      p = OGL_ORTHO; // return something...
    }
    return p;
  }
  
 private slots:
  void slot_projection_changed() {
    const OpenGL_Projection p = projection();
    opengl->projection = p;
    widgets_enabler();
  }
  
  void slot_evolution_changed() {
    const orsa::Frame * bodies = opengl->bodies();
    center_cb->Set(   bodies, false);
    eye_cb->Set(      bodies, false);
    rotation_cb->Set( bodies, false);
  }
  
 private slots:  
  void widgets_enabler() {
    
    eye_cb->setEnabled(eye_tb->isOn());
    
    rotation_cb->setEnabled(rotation_tb->isOn());
    
    switch (projection()) {
    case OGL_ORTHO:
      distance_slider->hide();
      //
      eye_tb->setOn(false);
      //
      eye_tb->hide();
      eye_cb->hide();
      //
      FOV_slider->hide();
      near_slider->hide();
      far_slider->hide();
      //
      ortho_xy_slider->show();
      ortho_z_near_slider->show();
      ortho_z_far_slider->show();
      break;
    case OGL_PERSPECTIVE:
      distance_slider->show();
      distance_slider->setEnabled(!eye_tb->isOn());
      //
      eye_tb->show();
      eye_cb->show();
      //
      FOV_slider->show();
      near_slider->show();
      far_slider->show();
      //
      ortho_xy_slider->hide();
      ortho_z_near_slider->hide();
      ortho_z_far_slider->hide();
      break;
    } 
  } 
  
 private:
  XOrsaOpenGLEvolutionWidget * const opengl; 
 private:
  XOrsaImprovedObjectsComboTool * center_cb;
 private:
  XOrsaBoolToolButton * eye_tb;
  XOrsaImprovedObjectsComboTool * eye_cb;
 private:
  XOrsaBoolToolButton * rotation_tb;
  XOrsaImprovedObjectsComboTool * rotation_cb;
 private:
  QComboBox * projection_cb;
 private:
  XOrsaDoubleObjectWithLimitsSliderTool * distance_slider;
  //
  XOrsaDoubleObjectWithLimitsSliderTool *  FOV_slider;
  XOrsaDoubleObjectWithLimitsSliderTool * near_slider;
  XOrsaDoubleObjectWithLimitsSliderTool *  far_slider;
  //
  XOrsaDoubleObjectWithLimitsSliderTool * ortho_xy_slider;
  XOrsaDoubleObjectWithLimitsSliderTool * ortho_z_near_slider;
  XOrsaDoubleObjectWithLimitsSliderTool * ortho_z_far_slider;
};

#endif // _XORSA_OPENGL_H_
