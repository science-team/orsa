/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_analysis.h"

#include <qlayout.h> 
#include <qlabel.h>
#include <qgroupbox.h>
#include <qpushbutton.h> 
#include <qstatusbar.h> 
#include <qtabwidget.h> 
#include <qlineedit.h> 
#include <qvalidator.h> 
#include <qpushbutton.h>
#include <qspinbox.h> 

#include <iostream>

using namespace std;
using namespace orsa;

XOrsaAnalysis::XOrsaAnalysis(SWIFTFile *file, QWidget *parent) : QWidget(parent,0,Qt::WType_TopLevel | Qt::WDestructiveClose), swift_file(file), mode(SWIFTFILE) {
  
  QString cap;
  cap.sprintf("analysis tool: %s",swift_file->GetFileName().c_str());
  setCaption(cap);
  
  curves = new vector<XOrsaPlotCurve>;
  
  InitCommonGraphics();
}

XOrsaAnalysis::XOrsaAnalysis(OrbitStream *os_in, QWidget *parent) : QWidget(parent,0,Qt::WType_TopLevel | Qt::WDestructiveClose), mode(ORBITSTREAM) {
  
  QString cap;
  cap.sprintf("analysis tool: %s",os_in->label.c_str());
  setCaption(cap);
  
  curves = new vector<XOrsaPlotCurve>;
  
  os = *(os_in);
  
  InitCommonGraphics();
}

XOrsaAnalysis::XOrsaAnalysis(const Evolution *evol_in, QWidget *parent) : QWidget(parent,0,Qt::WType_TopLevel | Qt::WDestructiveClose), evol(evol_in), mode(EVOLUTION) {
  
  QString cap;
  cap.sprintf("analysis tool: %s",evol->name.c_str());
  setCaption(cap);
  
  curves = new vector<XOrsaPlotCurve>;
  
  // else??
  if (evol->size() > 0) bodies = &(evol[0][0]);
  
  InitCommonGraphics();
}

void XOrsaAnalysis::InitCommonGraphics() {
  
  QVBoxLayout *vlay = new QVBoxLayout(this); // ,4
  
  QHBoxLayout *header_lay = new QHBoxLayout(vlay,4);
  
  control = new QWidget(this);
  //
  QVBoxLayout *control_lay = new QVBoxLayout(control,4); // 
  // control_lay->setResizeMode(QLayout::Fixed);
  // QVBoxLayout *control_lay = new QVBoxLayout(control_gb,4); // ,4
  //

  if (mode==EVOLUTION) {
    QHBoxLayout *objbox = new QHBoxLayout(control_lay);
    // objbox->addStretch();
    QLabel *objl = new QLabel(control);
    objl->setText("Body:");
    objbox->addWidget(objl);
    objects_combo = new XOrsaImprovedObjectsCombo(bodies,false,control);
    objbox->addWidget(objects_combo);
    // control_lay->addWidget(objects_combo);
    connect(objects_combo,SIGNAL(ObjectChanged(int)),this,SLOT(update_body(int)));
    
    QLabel *robjl = new QLabel(control);
    robjl->setText("Ref. Body:");
    objbox->addWidget(robjl);
    robjects_combo = new XOrsaImprovedObjectsCombo(bodies,true,control);
    objbox->addWidget(robjects_combo);
    connect(robjects_combo,SIGNAL(ObjectChanged(int)),this,SLOT(update_rbody(int)));
    objbox->addStretch();
  } else if (mode==SWIFTFILE) {
    QHBoxLayout *objbox = new QHBoxLayout(control_lay);
    QLabel *objl = new QLabel(control);
    objl->setText("SWIFT file object:");
    objbox->addWidget(objl);
    swift_spin = new QSpinBox(1,swift_file->AsteroidsInFile(),1,control);
    objbox->addWidget(swift_spin);
    objbox->addStretch();
  }
  
  // analysis parameters
  
  QHBoxLayout *gb_lay = new QHBoxLayout(control_lay);
  
  QGroupBox *param_gb = new QGroupBox("Parameters",control);
  param_gb->setColumns(2); 
  // control_lay->addWidget(param_gb);
  gb_lay->addWidget(param_gb);
  //
  QSizePolicy prsp = param_gb->sizePolicy();
  prsp.setHorData(QSizePolicy::Fixed);
  param_gb->setSizePolicy(prsp);
  
  // QWidget *signal_type_widget = new QWidget(param_gb);
  // QHBoxLayout *sth = new QHBoxLayout(signal_type_widget);
  // QLabel *stl = new QLabel(signal_type_widget);
  /* QLabel *stl = new QLabel(param_gb);
     stl->setText("signal type:");
     // sth->addWidget(stl);
     // stc = new SignalTypeCombo(signal_type_widget);
     stc = new SignalTypeCombo(param_gb);
     connect(stc,SIGNAL(activated(int)),this,SLOT(signal_type_changed(int)));
     // sth->addWidget(stc);
  */
  
  new QLabel("signal amplitude:",param_gb);
  sac = new SignalAmplitudeCombo(param_gb);
  
  new QLabel("signal phase:",param_gb);
  spc = new SignalPhaseCombo(param_gb);
  
  // QWidget *fft_algo_widget = new QWidget(param_gb);
  // QHBoxLayout *fah = new QHBoxLayout(fft_algo_widget);
  // QLabel *fal = new QLabel(fft_algo_widget);
  QLabel *fal = new QLabel(param_gb);
  fal->setText("algorithm:");
  // fah->addWidget(fal);
  // fac = new FFTAlgoCombo(fft_algo_widget);
  fac = new FFTAlgoCombo(param_gb);
  connect(fac,SIGNAL(activated(int)),this,SLOT(fft_algo_changed(int)));
  // fah->addWidget(fac);

  QLabel *nwl = new QLabel(param_gb);
  nwl->setText("number of peaks:");
  spin = new QSpinBox(1,32,1,param_gb);
  spin->setValue(4); // default (widget) number of peaks
  
  // QWidget *window_amplitude_widget = new QWidget(param_gb);
  // QHBoxLayout *wah = new QHBoxLayout(window_amplitude_widget);
  // QLabel *wal = new QLabel(window_amplitude_widget);
  QLabel *wal = new QLabel(param_gb);
  wal->setText("window amplitude:");
  // wah->addWidget(wal);
  // wale = new QLineEdit(window_amplitude_widget);
  wale = new QLineEdit(param_gb);
  QString line; 
  if (mode==EVOLUTION) line.sprintf("%g",(*evol)[evol->size()-1].Time());
  wale->setText(line);
  // wah->addWidget(wale);
  
  // QWidget *window_start_widget = new QWidget(param_gb);
  // QHBoxLayout *wstarth = new QHBoxLayout(window_start_widget);
  // QLabel *wstartl = new QLabel(window_start_widget);
  QLabel *wstartl = new QLabel(param_gb);
  wstartl->setText("window start:");
  // wstarth->addWidget(wstartl);
  // wstartle = new QLineEdit(window_start_widget);
  wstartle = new QLineEdit(param_gb);
  line.sprintf("%g",0.0);
  wstartle->setText(line);
  // wstarth->addWidget(wstartle);
  
  // QWidget *window_step_widget = new QWidget(param_gb);
  // QHBoxLayout *wsteph = new QHBoxLayout(window_step_widget);
  // QLabel *wstepl = new QLabel(window_step_widget);
  QLabel *wstepl = new QLabel(param_gb);
  wstepl->setText("window step:");
  // wsteph->addWidget(wstepl);
  // wsteple = new QLineEdit(window_step_widget);
  wsteple = new QLineEdit(param_gb);
  line.sprintf("%g",0.0);
  wsteple->setText(line);  
  // wsteph->addWidget(wsteple);
  
  QDoubleValidator *vd = new QDoubleValidator(this);
  wale->setValidator(vd);
  wstartle->setValidator(vd);
  wsteple->setValidator(vd);
  
  QSizePolicy csp = control->sizePolicy();
  // csp.setHorData(QSizePolicy::Fixed);
  // csp.setVerData(QSizePolicy::Fixed);
  csp.setVerData(QSizePolicy::Maximum);
  control->setSizePolicy(csp);
  
  //
  QPushButton *compute_pb = new QPushButton("Analyze",control);
  connect(compute_pb,SIGNAL(clicked()),this,SLOT(ComputeFFT()));
  control_lay->addWidget(compute_pb);
  //
  header_lay->addWidget(control);
  
  // peaks data
  
  QGroupBox *peaks_gb = new QGroupBox("Peaks",control);
  peaks_gb->setColumns(1);
  // header_lay->addWidget(peaks_gb);
  gb_lay->addWidget(peaks_gb);
  //
  QSizePolicy psp = peaks_gb->sizePolicy();
  // psp.setVerData(QSizePolicy::Fixed);
  // psp.setVerData(QSizePolicy::Maximum);
  psp.setHorData(QSizePolicy::Minimum);
  peaks_gb->setSizePolicy(psp);
  
  QWidget *windows_list = new QWidget(peaks_gb);
  QHBoxLayout *wl_lay = new QHBoxLayout(windows_list);
  QLabel *wll = new QLabel("window list:",windows_list);
  wl_lay->addWidget(wll);
  //
  wincombo = new QComboBox(windows_list);
  QSizePolicy wsp = wincombo->sizePolicy();
  wsp.setHorData(QSizePolicy::Expanding); // MinimumExpanding,Expanding
  wincombo->setSizePolicy(wsp);
  //
  wl_lay->addWidget(wincombo);
  wl_lay->addStretch();
  
  list_peaks = new QListView(peaks_gb);
  //
  list_peaks->setAllColumnsShowFocus(true);
  list_peaks->setShowSortIndicator(true);
  list_peaks->setSelectionMode(QListView::Extended);
  list_peaks->setItemMargin(3); // in pixels
  // list_peaks->setResizeMode(QListView::LastColumn);
  //
  QString frequency_label;
  frequency_label.sprintf("frequency [%s^-1]",TimeLabel().c_str());
  list_peaks->addColumn(frequency_label);
  list_peaks->addColumn("amplitude");
  list_peaks->addColumn("phase [DEG]");
  list_peaks->addColumn("notes");
  //
  list_peaks->setSorting(1,false);
  //
  /*  QSizePolicy lsp = list_peaks->sizePolicy();
      lsp.setVerData(QSizePolicy::Fixed);
      lsp.setHorData(QSizePolicy::Maximum);
      list_peaks->setSizePolicy(lsp);
  */
  //
  /* QSizePolicy lsp = control->sizePolicy();
     lsp.setVerData(QSizePolicy::Fixed);
     // lsp.setHorData(QSizePolicy::Maximum);
     control->setSizePolicy(lsp);
  */
  
  tab = new QTabWidget(this);
  
  QWidget *signal_tab_widget = new QWidget(tab);
  QVBoxLayout *signal_tab_lay = new QVBoxLayout(signal_tab_widget,2);
  //
  XOrsaExtendedPlotArea *signal_ext_area = new XOrsaExtendedPlotArea(200,150,signal_tab_widget);
  area_signal = signal_ext_area->area;
  signal_tab_lay->addWidget(signal_ext_area);
  tab->insertTab(signal_tab_widget,"signal");
  
  QWidget *spectrum_tab_widget = new QWidget(tab);
  QVBoxLayout *spectrum_tab_lay = new QVBoxLayout(spectrum_tab_widget,2);
  //
  XOrsaExtendedPlotArea *spectrum_ext_area = new XOrsaExtendedPlotArea(200,150,spectrum_tab_widget);
  area_spectrum = spectrum_ext_area->area;
  // area_spectrum->SetLogAxis(Y,true);
  area_spectrum->Y.SetLogScale(true);
  spectrum_tab_lay->addWidget(spectrum_ext_area);
  tab->insertTab(spectrum_tab_widget,"spectrum");
  
  QWidget *peaks_tab_widget = new QWidget(tab);
  QVBoxLayout *peaks_tab_lay = new QVBoxLayout(peaks_tab_widget,2);
  //
  XOrsaExtendedPlotArea *peaks_ext_area = new XOrsaExtendedPlotArea(200,150,peaks_tab_widget);
  area_peaks = peaks_ext_area->area;
  peaks_tab_lay->addWidget(peaks_ext_area);
  tab->insertTab(peaks_tab_widget,"peaks");
  
  QWidget *xy_tab_widget = new QWidget(tab);
  QVBoxLayout *xy_tab_lay = new QVBoxLayout(xy_tab_widget,2);
  //
  XOrsaExtendedPlotArea *xy_ext_area = new XOrsaExtendedPlotArea(200,150,xy_tab_widget);
  area_xy = xy_ext_area->area;
  area_xy->SetSameScale(true);
  xy_tab_lay->addWidget(xy_ext_area);
  tab->insertTab(xy_tab_widget,"complex signal");
  
  /* QSizePolicy gsp = tab->sizePolicy();
     gsp.setVerData(QSizePolicy::MinimumExpanding);
     // gsp.setHorData(QSizePolicy::MinimumExpanding);
     tab->setSizePolicy(gsp);
  */
  
  vlay->addWidget(tab);
  // control_lay->addWidget(tab);
  
  QStatusBar *status_bar = new QStatusBar(this);
  status_bar_label = new QLabel(status_bar);
  status_bar_label->setTextFormat(Qt::RichText);
  //
  status_bar_label->setFixedHeight(status_bar_label->fontMetrics().height());
  //
  status_bar->addWidget(status_bar_label,1);
  /* 
     QSizePolicy sp = status_bar->sizePolicy();
     sp.setVerData(QSizePolicy::Fixed);
     status_bar->setSizePolicy(sp);
  */
  //
  vlay->addWidget(status_bar);
  
  
  connect(tab,SIGNAL(currentChanged(QWidget*)),this,SLOT(SetArea(QWidget*)));
  
  connect(area_signal,  SIGNAL(mouse_moved(QMouseEvent*)),this,SLOT(status_bar_plot_coords(QMouseEvent*)));
  connect(area_spectrum,SIGNAL(mouse_moved(QMouseEvent*)),this,SLOT(status_bar_plot_coords(QMouseEvent*)));
  connect(area_peaks   ,SIGNAL(mouse_moved(QMouseEvent*)),this,SLOT(status_bar_plot_coords(QMouseEvent*)));
  connect(area_xy      ,SIGNAL(mouse_moved(QMouseEvent*)),this,SLOT(status_bar_plot_coords(QMouseEvent*)));
  
  area = area_signal;
}

void XOrsaAnalysis::SetBodiesIndex() {
  body_index  =  objects_combo->GetObject();
  rbody_index = robjects_combo->GetObject();
}

void XOrsaAnalysis::update_body(int) { // int b
  // if (b != body_index) {
  SetBodiesIndex();
  // ComputeOrbitalElements();
  // }
}

void XOrsaAnalysis::update_rbody(int) { // int b
  SetBodiesIndex();
  // ComputeOrbitalElements();
}

void XOrsaAnalysis::status_bar_plot_coords(QMouseEvent *me) {
  /* 
     QString x_s, y_s;
     FineLabel(x_s,area->x(me->pos()));
     FineLabel(y_s,area->y(me->pos()));
     status_bar_label->setText("coords: (" + x_s + ";" + y_s + ")");
  */
  
  QString x_s, y_s;
  switch (area->X.GetType()) {
  case AT_DATE: FineDate( x_s,area->x(me->pos()),false); break;
  default:   FineLabel(x_s,area->x(me->pos())); break;
  }
  switch (area->Y.GetType()) {
  case AT_DATE: FineDate( y_s,area->y(me->pos()),false); break;
  default:   FineLabel(y_s,area->y(me->pos())); break;
  }
  status_bar_label->setText("[" + x_s + ";" + y_s + "]");
}

void XOrsaAnalysis::SetArea(QWidget*) {
  
  int page = tab->currentPageIndex();
  
  if (page == 0) {
    area = area_signal;
  } else if (page == 1) {
    area = area_spectrum;
  } else if (page == 2) {
    area = area_peaks;
  } else if (page == 3) {
    area = area_xy;
  }
  
  if (this->mouseGrabber() != 0) {
    this->mouseGrabber()->releaseMouse();
  }
  area->setMouseTracking(true);
  
}

void XOrsaAnalysis::signal_type_changed(int) {
  
}

void XOrsaAnalysis::fft_algo_changed(int) {
  
}

void XOrsaAnalysis::ComputeOrbitalElements() {
 
  SetBodiesIndex();
  
  os.clear();
  
  if (body_index==rbody_index) {
    cerr << "warning: body_index == rbody_index..." << endl;
    return;
  }
  
  // naive..
  // os.timestep = (*evol)[1].Time() - (*evol)[0].Time();
  // cerr << "step [1]" << endl;
  os.timestep = (*evol)[evol->size()/5].Time() - (*evol)[(evol->size()/5)-1].Time();
  // cerr << "step [2]" << endl;
  Frame f;
  OrbitWithEpoch o;
  unsigned int k;
  // Vector dr, dv;
  // double mu;
  // cerr << " body index = " <<  body_index << endl;
  // cerr << "rbody index = " << rbody_index << endl;
  // const double mu = GetG()*((*evol)[0][body_index].mass()+(*evol)[0][rbody_index].mass());
  // cerr << "mu = " << mu << endl;
  // cerr << "step [3]" << endl;
  for (k=0;k<evol->size();++k) {
    f = (*evol)[k];
    // o.Compute(f,body_index,rbody_index);
    // print(f[ body_index]);
    // print(f[rbody_index]);
    // dr = f[body_index].position() - f[rbody_index].position();
    // dv = f[body_index].velocity() - f[rbody_index].velocity();
    // mu = GetG()*(f[body_index].mass()+f[rbody_index].mass());
    o.Compute(f[body_index],f[rbody_index],f);
    // cerr << "orbit computed: a = " << o.a << "  o.e = " << o.e << endl;
    os.push_back(o);
  }
  
}

void XOrsaAnalysis::ComputeFFT() {
  
  const double window_amplitude = wale->text().toDouble();
  const double window_start     = wstartle->text().toDouble();
  const double window_step      = wsteple->text().toDouble();
  
  switch (mode) {
  case SWIFTFILE: { 
    
    // bad style: if in SWIFTFILE mode, read the OrbitStream from file
    swift_file->os->asteroid_number     = swift_spin->value();
    
    // if (window_step == 0.0) {
    swift_file->os->wp.window_amplitude = window_amplitude;
    swift_file->os->wp.window_start     = window_start;
    swift_file->os->wp.window_step      = window_step;
    /* } else {
       swift_file->os->wp.window_amplitude = 1e100; // huge value, to read all the file
       swift_file->os->wp.window_start     = window_start;
       swift_file->os->wp.window_step      = window_step;
       }
    */
    
    swift_file->Read();
    
    os = (*swift_file->os);
    cerr << "SWIFTFILE os size: " << os.size() << " original size: " << swift_file->os->size()<< endl;
  }
    break;
  case EVOLUTION: {
    ComputeOrbitalElements();
  }
    break;
  case ORBITSTREAM: {
    
  }
    break;
  }	
  
  // double reconstruction_precision=0.04;
  
  OrbitStream       fft_os;
  FFTPowerSpectrum  power_spectrum;
  vector<Peak>      peaks;
  FFTDataStream     reco;
  
  FFT fft(fft_os,power_spectrum,peaks,reco);
  
  // fft.relative_amplitude = reconstruction_precision;
  
  fft.nfreq = spin->value();
  
  // FFTSearch    signal_type = stc->GetSignalType();
  FFTSearchAmplitude sa = sac->GetSignalAmplitude();
  FFTSearchPhase     sp = spc->GetSignalPhase(); 
  FFTAlgorithm algo = fac->GetFFTAlgo();
  
  // too much, should copy only the "header" (non-vector data)
  fft_os = os;
  
  vector< vector<Peak> > all_peaks;
  
  unsigned int k, window_counter=0;
  double local_time;
  do {
    
    local_time = window_start + window_step*window_counter;
    
    fft_os.clear();
    // if (use_ref) ref_fft_os.resize(0);
    for(k=0;k<os.size();k++) {
      if ( (os[k].epoch.Time() >= (local_time)) &&
	   (os[k].epoch.Time() <= (local_time+window_amplitude))) {
	
	fft_os.push_back(os[k]);
	// if (use_ref) ref_fft_os.push_back(ref_planet_os[k]);
	
	// check
	/* if (os[k].time != ref_planet_os[k].time) {
	   cerr << "ERROR: improper alignment. EXIT" << endl;
	   exit(0);
	   } */
	
      }
    }
    
    cerr << "fft_os.size(): " << fft_os.size() << endl;
    cerr << "    os.size(): " <<     os.size() << endl;
    cerr << "fft_os.timestep: " << fft_os.timestep << endl;
    cerr << "window start time: " << local_time << "   end time: " << (local_time+window_amplitude) << endl;
    
    if (fft_os.size() > 10) { // minimum size...
      // fft.Search(signal_type,algo);       
      fft.Search(sa,sp,algo);       
      all_peaks.push_back(peaks);
    }
    
    // next window
    window_counter++;
    
  } while ((fft_os.size() != 0) && (window_step != 0.0));
  
  if (all_peaks.size()>0) {
    
    // update wincombo
    { 
      wincombo->clear();
      unsigned int w;
      QString qs;
      for (w=0;w<all_peaks.size();w++) {
	qs.sprintf("%g - %g",
		   window_start+w*window_step,
		   window_start+w*window_step+window_amplitude);
	wincombo->insertItem(qs);
      }
    }
    
    double rad_deg_conv = 1.0;   
    if (sa==I) rad_deg_conv = 180/pi;
    
    { 
      curves->clear();
      unsigned int j;
      XOrsaPlotPoint point;
      XOrsaPlotCurve cur;
      
      // signal
      cur.clear();
      for (j=0;j<fft.data_stream.size();j++) {
     	point.x = fft.data_stream[j].time;
	point.y = fft.data_stream[j].amplitude*rad_deg_conv;
	cur.push_back(point);
      }
      //
      cur.index = body_index;
      cur.color = Qt::black;
      curves->push_back(cur);
      
      // reconstructed signal
      cur.clear();
      for (j=0;j<reco.size();j++) {
	point.x = reco[j].time;
	point.y = reco[j].amplitude*rad_deg_conv;
	// cerr << "reco: " << point.x << "  " << point.y << endl;
	cur.push_back(point);
      }
      //
      cur.index = body_index;
      cur.color = Qt::red;
      curves->push_back(cur);
      
      if (universe->GetUniverseType() == Real) {
	area_signal->X.SetType(AT_DATE);
      } else {
	area_signal->X.SetType(AT_TIME);
      }	
      area_signal->Y.SetType(AT_GENERIC);
      
      area_signal->SetData(curves);
    }

    {
      // spectrum
      
      curves->clear();
      unsigned int j;
      XOrsaPlotPoint point;
      XOrsaPlotCurve cur;
      for (j=0;j<power_spectrum.size();j++) {
	point.x = power_spectrum[j].frequency;
	point.y = power_spectrum[j].power*rad_deg_conv;
	cur.push_back(point);
      }
      
      cur.index = body_index;
      cur.color = Qt::black;
      curves->push_back(cur);
      
      area_spectrum->X.SetType(AT_GENERIC);
      area_spectrum->Y.SetType(AT_GENERIC);
      
      area_spectrum->SetData(curves);
    }
    
    if (all_peaks.size()>0) {
      
      // peaks
      
      cerr << " area peaks.. all peaks size: " << all_peaks.size() << endl;
      
      curves->clear();
      unsigned int pv,pc;
      XOrsaPlotPoint point;
      // XOrsaPlotCurve cur;
      
      for (pv=0;pv<all_peaks.size();pv++) {
	XOrsaPlotCurve cur;
	cur.clear();
	for (pc=0;pc<all_peaks[pv].size();pc++) {
	  point.x = window_start + window_step*pv;
	  point.y = all_peaks[pv][pc].frequency;
	  cur.push_back(point);
	}
	cur.index = pv;
	cur.color = Qt::black;
	curves->push_back(cur);
      }
      
        if (universe->GetUniverseType() == Real) {
	area_peaks->X.SetType(AT_DATE);
      } else {
	area_peaks->X.SetType(AT_TIME);
      }	
      area_peaks->Y.SetType(AT_GENERIC);
      
      area_peaks->SetData(curves);      
    }
    
    {
      // xy 
      
      curves->clear();
      unsigned int j;
      XOrsaPlotPoint point;
      XOrsaPlotCurve cur;
      
      // signal
      cur.clear();
      for (j=0;j<fft.data_stream.size();j++) {
	// point.x = fft.data_stream[j].time;
	// point.y = fft.data_stream[j].amplitude*rad_deg_conv;
	point.x = fft.data_stream[j].amplitude*cos(fft.data_stream[j].phase);
	point.y = fft.data_stream[j].amplitude*sin(fft.data_stream[j].phase);
	cur.push_back(point);
      }
      //
      cur.index = body_index;
      cur.color = Qt::black;
      curves->push_back(cur);
      
      // reconstructed signal
      cur.clear();
      for (j=0;j<reco.size();j++) {
	// point.x = reco[j].time;
	// point.y = reco[j].amplitude*rad_deg_conv;
	point.x = reco[j].amplitude*cos(fft.data_stream[j].phase);
	point.y = reco[j].amplitude*sin(fft.data_stream[j].phase);
	cur.push_back(point);
      }
      //
      cur.index = body_index;
      cur.color = Qt::red;
      curves->push_back(cur);
      
      area_xy->X.SetType(AT_GENERIC);
      area_xy->Y.SetType(AT_GENERIC);
      
      area_xy->SetData(curves);
    }
    
    {
      // peaks list
      list_peaks->clear();
      unsigned int j;
      QString sf,sa,sp,sn;
      for (j=0;j<peaks.size();j++) {
	// fprintf(file_peaks,"%e  %e\n",peaks[j].frequency,peaks[j].amplitude);
	sf.sprintf("%.14g",peaks[j].frequency);
	sa.sprintf("%.14g",peaks[j].amplitude*rad_deg_conv);
	sp.sprintf("%g",fmod(peaks[j].phase,twopi)*(180/pi));
	sn.sprintf("T = 1/f = %.14g %s",1.0/peaks[j].frequency,TimeLabel().c_str());
	
	new XOrsaPeaksListItem(list_peaks,sf,sa,sp,sn);
      }
    } 
  }
}

// SignalTypeCombo

SignalTypeCombo::SignalTypeCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("hk");
  insertItem("pq");
  insertItem("node");
  insertItem("anomaly");
  insertItem("anomaly-phase");
  insertItem("a-M");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetSignalType(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0); 
}


void SignalTypeCombo::SetSignalType(int i) {
 
  // look at the combobox for the right order
  switch (i) {
  case 0: t = HK;            break;
  case 1: t = PQ;            break;
  case 2: t = NODE;          break;
  case 3: t = ANOMALY;       break;
  case 4: t = ANOMALY_PHASE; break;
  case 5: t = A_M;           break;
  }
  
}

FFTSearch SignalTypeCombo::GetSignalType() {
  return t;
}


// SignalAmplitudeCombo 

SignalAmplitudeCombo::SignalAmplitudeCombo (QWidget *parent) : QComboBox(false,parent) {
  insertItem("a");
  insertItem("e");
  insertItem("i");
  insertItem("sin(i)");
  insertItem("tan(i/2)");
  insertItem("1.0");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetSignalAmplitude(int)));
  
  // sync!
  setCurrentItem(1);
  activated(1); 
}

void SignalAmplitudeCombo::SetSignalAmplitude(int i) {
   
  // look at the combobox for the right order
  switch (i) {
  case 0: sa = A;       break;
  case 1: sa = E;       break;
  case 2: sa = I;       break;
  case 3: sa = SIN_I;   break;
  case 4: sa = TAN_I_2; break;
  case 5: sa = ONE;     break;
  }
  
}

void SignalAmplitudeCombo::SetSignalAmplitude(FFTSearchAmplitude sa_in) {
  // look at the combobox for the right order
  switch (sa_in) {
  case A:       setCurrentItem(0); SetSignalAmplitude(0); break;
  case E:       setCurrentItem(1); SetSignalAmplitude(1); break;
  case I:       setCurrentItem(2); SetSignalAmplitude(2); break;
  case SIN_I:   setCurrentItem(3); SetSignalAmplitude(3); break;
  case TAN_I_2: setCurrentItem(3); SetSignalAmplitude(4); break;
  case ONE:     setCurrentItem(4); SetSignalAmplitude(5); break;
  }
}

FFTSearchAmplitude SignalAmplitudeCombo::GetSignalAmplitude() {
  return sa;
}

// SignalPhaseCombo 

SignalPhaseCombo::SignalPhaseCombo (QWidget *parent) : QComboBox(false,parent) {
  insertItem("omega node");
  insertItem("omega pericenter");
  insertItem("omega_tilde");
  insertItem("M");
  insertItem("lambda");
  insertItem("0.0");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetSignalPhase(int)));
  
  // sync!
  setCurrentItem(2);
  activated(2); 
}

void SignalPhaseCombo::SetSignalPhase(int i) {
   
  // look at the combobox for the right order
  switch (i) {
  case 0: sp = OMEGA_NODE;       break;
  case 1: sp = OMEGA_PERICENTER; break;
  case 2: sp = OMEGA_TILDE;      break;
  case 3: sp = MM;               break;
  case 4: sp = LAMBDA;           break;
  case 5: sp = ZERO;             break;
  }
  
}

void SignalPhaseCombo::SetSignalPhase(FFTSearchPhase sp_in) {
  // look at the combobox for the right order
  switch (sp_in) {
  case OMEGA_NODE:       setCurrentItem(0); SetSignalPhase(0); break;
  case OMEGA_PERICENTER: setCurrentItem(1); SetSignalPhase(1); break;
  case OMEGA_TILDE:      setCurrentItem(2); SetSignalPhase(2); break;
  case MM:               setCurrentItem(3); SetSignalPhase(3); break;
  case LAMBDA:           setCurrentItem(4); SetSignalPhase(4); break;
  case ZERO:             setCurrentItem(5); SetSignalPhase(5); break;
  }
}

FFTSearchPhase SignalPhaseCombo::GetSignalPhase() {
  return sp;
}

// FFTAlgoCombo

FFTAlgoCombo::FFTAlgoCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("FFT");
  insertItem("FFTB");
  insertItem("MFT"); 
  insertItem("FMFT 1");
  insertItem("FMFT 2");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetFFTAlgo(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0); 
}

void FFTAlgoCombo::SetFFTAlgo(int i) {
  
  // look at the combobox for the right order
  switch (i) {
  case 0: t = algo_FFT;   break;
  case 1: t = algo_FFTB;  break;
  case 2: t = algo_MFT;   break;
  case 3: t = algo_FMFT1; break;
  case 4: t = algo_FMFT2; break;
  }
  
}

FFTAlgorithm FFTAlgoCombo::GetFFTAlgo() {
  return t;
}

// XOrsaPeaksListItem

XOrsaPeaksListItem::XOrsaPeaksListItem(QListView *parent, QString label1, QString label2, QString label3, QString label4, QString label5, QString label6, QString label7, QString label8) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8) {
  
}

int XOrsaPeaksListItem::compare ( QListViewItem * i, int col, bool ascending ) const {
  // always doubles compare
  double d = (atof(key( col, ascending ).latin1()) - atof( i->key(col, ascending).latin1()));
  
  if (d < 0.0) return (-1);
  if (d > 0.0) return (+1);
  return (0);
}
