/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_plot_tool_II.h"

#include <orsa_secure_math.h>
#include <orsa_error.h>

#include <qlayout.h> 
#include <qstring.h>
#include <qsizepolicy.h>
#include <qgroupbox.h>
#include <qpushbutton.h> 
#include <qcheckbox.h> 
#include <qlabel.h>
#include <qstatusbar.h> 
#include <qtabwidget.h> 
#include <qmessagebox.h>

#include <iostream>

using namespace std;
using namespace orsa;

XOrsaPlotTool_II::XOrsaPlotTool_II(const Evolution *evol_in, QWidget *parent) : QWidget(parent,0,Qt::WType_TopLevel | Qt::WDestructiveClose) {
  
  evol = evol_in;
  
  curves = new vector<XOrsaPlotCurve>;
  
  // error: no data
  // if (evol->size() == 0) return;
  
  // else??
  if (evol->size() > 0) bodies = (evol[0][0]);
  
  // DEBUG
  /* cerr << "XOrsaPlotTool_II bodies: " << endl;
     unsigned int j;
     for (j=0;j<bodies->size();j++) print((*bodies)[j]);
  */
  
  QString cap;
  cap.sprintf("plotting tool: %s",evol->name.c_str());
  setCaption(cap);
  
  QVBoxLayout *vlay = new QVBoxLayout(this); // ,4
  
  /* control_gb = new QGroupBox("Plotting parameters",this);
     control_gb->setColumns(1); */
  control = new QWidget(this);
  QVBoxLayout *control_lay = new QVBoxLayout(control,4); // 
  // QVBoxLayout *control_lay = new QVBoxLayout(control_gb,4); // ,4
  //
  QHBoxLayout *objbox = new QHBoxLayout(control_lay);
  objbox->addStretch();
  QLabel *objl = new QLabel(control);
  objl->setText("Body:");
  objbox->addWidget(objl);
  objects_combo = new XOrsaImprovedObjectsCombo(&bodies,false,control);
  objbox->addWidget(objects_combo);
  // control_lay->addWidget(objects_combo);
  connect(objects_combo,SIGNAL(ObjectChanged(int)),this,SLOT(update_body(int)));
  //
  // QHBoxLayout *robjbox = new QHBoxLayout(control_lay);
  // objbox->addStretch();
  


  /* QHBoxLayout *robjbox = new QHBoxLayout(control_lay);
     robjbox->addStretch();
     QLabel *robjl = new QLabel(control);
     robjl->setText("Ref. Body:");
     robjbox->addWidget(robjl);
     robjects_combo = new XOrsaObjectsCombo(bodies,true,control);
     robjbox->addWidget(robjects_combo);
     connect(robjects_combo,SIGNAL(ObjectChanged(int)),this,SLOT(update_rbody(int)));
  */
  //


  vlay->addWidget(control);
  
  // PlotTab *tab = new PlotTab(this);
  tab = new QTabWidget(this);
  // QTabWidget *tab = new QTabWidget(this, "tabname", Qt::WRepaintNoErase | Qt::WStaticContents | Qt::WPaintClever | Qt::WPaintUnclipped);
  // tab->setBackgroundMode(Qt::NoBackground);
  // tab->setAutoMask(false);
  // tab->currentPage()->setWFlags(Qt::WRepaintNoErase);
  /* QSizePolicy spt = tab->sizePolicy();
     spt.setVerData(QSizePolicy::Fixed);
     tab->setSizePolicy(spt);
  */
  vlay->addWidget(tab);
  
  // QWidget *control2 = new QWidget(this);
  // QVBoxLayout *control2_lay = new QVBoxLayout(control2,4); // 
  
  /* QHBoxLayout *ptbox = new QHBoxLayout(control2_lay);
     ptbox->addStretch();
     QLabel *ptjl = new QLabel(control2);
     ptjl->setText("Plot type:");
     ptbox->addWidget(ptjl);
     type_combo = new XOrsaPlotTypeCombo(control2);
     ptbox->addWidget(type_combo);
     // control_lay->addWidget(type_combo);
     connect(type_combo,SIGNAL(TypeChanged(XOrsaPlotType)),this,SLOT(update_area(XOrsaPlotType)));
     //
  */
  
  // QHBoxLayout *cboxs = new QHBoxLayout(control2_lay);
  // cboxs->addStretch();
  //
  /* QCheckBox *connect_points = new QCheckBox(control2);
     connect_points->setText("connect points");
     cboxs->addWidget(connect_points);
     //
     QCheckBox *same_scale = new QCheckBox(control2);
     same_scale->setText("same scale");
     cboxs->addWidget(same_scale);
  */
  //
  /*  QSizePolicy csp = control2->sizePolicy();
      csp.setVerData(QSizePolicy::Fixed);
      control2->setSizePolicy(csp);
      vlay->addWidget(control2);
  */
  //
  
  QWidget *kepler_tab_widget = new QWidget(tab);
  QVBoxLayout *kepler_tab_lay = new QVBoxLayout(kepler_tab_widget,2);
  QHBoxLayout *khl = new QHBoxLayout(kepler_tab_lay);
  // khl->addStretch();
  //
  QLabel *ptjl = new QLabel(kepler_tab_widget);
  ptjl->setText("Plot type:");
  khl->addWidget(ptjl);
  kepler_type_combo = new XOrsaKeplerPlotTypeCombo(kepler_tab_widget);
  khl->addWidget(kepler_type_combo);
  connect(kepler_type_combo,SIGNAL(TypeChanged(XOrsaPlotType)),this,SLOT(update_area(XOrsaPlotType)));
  //
  khl->addStretch();
  //
  QLabel *robjl = new QLabel(kepler_tab_widget);
  robjl->setText("Ref. Body:");
  khl->addWidget(robjl);
  robjects_combo_kepler = new XOrsaImprovedObjectsCombo(&bodies,true,kepler_tab_widget);
  khl->addWidget(robjects_combo_kepler);
  connect(robjects_combo_kepler,SIGNAL(ObjectChanged(int)),this,SLOT(update_rbody(int)));
  //
  XOrsaExtendedPlotArea *ext_area = new XOrsaExtendedPlotArea(200,150,kepler_tab_widget);
  kepler_tab_lay->addWidget(ext_area);
  tab->insertTab(kepler_tab_widget,"Keplerian");
  
  
  area_kepler = ext_area->area;
  area_kepler->SetConnectPoints(false);
  area_kepler->SetSameScale(false);
  
  QWidget *twoD_tab_widget = new QWidget(tab);
  QVBoxLayout *twoD_tab_lay = new QVBoxLayout(twoD_tab_widget,2);
  QHBoxLayout *twohl = new QHBoxLayout(twoD_tab_lay);
  // twohl->addStretch();
  //
  QLabel *ptjl2 = new QLabel(twoD_tab_widget);
  ptjl2->setText("Plot type:");
  twohl->addWidget(ptjl2);
  twoD_type_combo = new XOrsa2DPlotTypeCombo(twoD_tab_widget);
  twohl->addWidget(twoD_type_combo);
  connect(twoD_type_combo,SIGNAL(TypeChanged(XOrsaPlotType)),this,SLOT(update_area(XOrsaPlotType)));
  //
  QCheckBox *ref_body_fixed = new QCheckBox(twoD_tab_widget);
  ref_body_fixed->setText("Ref. Body fixed");
  twohl->addWidget(ref_body_fixed);
  connect(ref_body_fixed,SIGNAL(toggled(bool)),this,SLOT(slot_ref_body_fixed(bool)));
  //
  use_direction = new QCheckBox(twoD_tab_widget);
  use_direction->setText("Direction Body:");
  twohl->addWidget(use_direction);
  connect(use_direction,SIGNAL(toggled(bool)),this,SLOT(slot_use_direction(bool)));
  direction_combo = new XOrsaImprovedObjectsCombo(&bodies,false,twoD_tab_widget);
  twohl->addWidget(direction_combo);
  connect(direction_combo,SIGNAL(ObjectChanged(int)),this,SLOT(update_dirbody(int)));
  connect(use_direction,SIGNAL(toggled(bool)),direction_combo,SLOT(setEnabled(bool)));
  use_direction->setDown(false); 
  direction_combo->setEnabled(false);
  //
  twohl->addStretch();
  //
  QLabel *robjl2 = new QLabel(twoD_tab_widget);
  robjl2->setText("Ref. Body:");
  twohl->addWidget(robjl2);
  robjects_combo_twoD = new XOrsaImprovedObjectsCombo(&bodies,true,twoD_tab_widget);
  twohl->addWidget(robjects_combo_twoD);
  connect(robjects_combo_twoD,SIGNAL(ObjectChanged(int)),this,SLOT(update_rbody(int)));
  //
  XOrsaExtendedPlotArea *ext_area_2 = new XOrsaExtendedPlotArea(200,150,twoD_tab_widget);
  twoD_tab_lay->addWidget(ext_area_2);
  tab->insertTab(twoD_tab_widget,"2D plots");
  
  area_twoD = ext_area_2->area;
  area_twoD->SetSameScale(true);
  area_twoD->SetConnectPoints(false);
  
  
  // tab->currentPage()->setBackgroundMode(Qt::NoBackground);
  // tab->addTab(area,"Plot title tab");
  // area->SetPlotType(type_combo->GetPlotType());
  // area->SetBody(objects_combo->GetObject());
  // area->SetRBody(robjects_combo->GetObject());
  area_kepler->update();
  area_twoD->update();
  //
  // vlay->addWidget(area);
  //
  QStatusBar *status_bar = new QStatusBar(this);
  status_bar_label = new QLabel(status_bar);
  status_bar_label->setTextFormat(Qt::RichText);
  // status_bar_label->setFixedHeight(status_bar_label->height());
  status_bar_label->setFixedHeight(status_bar_label->fontMetrics().height());
  //
  status_bar->addWidget(status_bar_label,1);
  //
  /* QSizePolicy sp = status_bar->sizePolicy();
     sp.setVerData(QSizePolicy::Fixed);
     // sp.setVerData(QSizePolicy::Minimum);
     // sp.setVerData(QSizePolicy::Maximum);
     status_bar->setSizePolicy(sp);
  */
  //
  // status_bar->setMaximumHeight(status_bar->height());
  //
  vlay->addWidget(status_bar);
  
  // area_kepler->setMouseTracking(true);
  connect(area_kepler,SIGNAL(mouse_moved(QMouseEvent*)),this,SLOT(status_bar_plot_coords(QMouseEvent*)));
  connect(area_twoD,  SIGNAL(mouse_moved(QMouseEvent*)),this,SLOT(status_bar_plot_coords(QMouseEvent*)));
  // connect(connect_points,SIGNAL(toggled(bool)),this,SLOT(area->SetConnectPoint(bool)));
  
  /* connect(connect_points,SIGNAL(toggled(bool)),this,SLOT(slot_connect_points(bool)));
     connect_points->setChecked(true);
     
     connect(same_scale,SIGNAL(toggled(bool)),area,SLOT(SetSameScale(bool)));
     connect_points->setChecked(true);
  */
  
  // XOrsaPlotArea *area_misc = new XOrsaPlotArea(tab);
  // tab->insertTab(area_misc,"Miscellaneous");
  

  connect(tab,SIGNAL(currentChanged(QWidget*)),this,SLOT(SetArea(QWidget*)));
  //
  area = area_kepler; kepler_type_combo->SetPlotType(A);
  area = area_twoD;     twoD_type_combo->SetPlotType(XY);
  
  // type_combo->SetPlotType(XY);
  // ref_body_is_fixed  = false;
  // use_direction_body = false;
  /* ref_body_fixed->setChecked(false);
     use_direction->setChecked(false);
     ref_body_fixed->setChecked(true);
     use_direction->setChecked(true);
     ref_body_fixed->setChecked(false);
     use_direction->setChecked(false);
  */
  //
  ref_body_is_fixed = use_direction_body = false;
  //
  slot_ref_body_fixed(false);
  slot_use_direction(false);
  //
  ref_body_fixed->setChecked(false);
  use_direction->setChecked(false);
  //
  body_index = rbody_index = 0;
  
  //
  area = area_kepler; kepler_type_combo->SetPlotType(A);
  area = area_twoD;     twoD_type_combo->SetPlotType(XY);
}


void XOrsaPlotTool_II::SetBodiesIndex() {
  
  body_index = objects_combo->GetObject();
  
  int tab_page = tab->currentPageIndex();
  
  if (tab_page == 0) {
    rbody_index = robjects_combo_kepler->GetObject();
  } else if (tab_page == 1) {
    rbody_index = robjects_combo_twoD->GetObject();
  }
  
}

void XOrsaPlotTool_II::SetPlotType() {
  int tab_page = tab->currentPageIndex();
  if (tab_page == 0) {
    plot_type = kepler_type_combo->GetPlotType();
  } else if (tab_page == 1) {
    plot_type = twoD_type_combo->GetPlotType();
  }
}

void XOrsaPlotTool_II::SetArea(QWidget*) {
  
  int page = tab->currentPageIndex();
  
  if (page == 0) {
    area = area_kepler;
  } else if (page == 1) {
    area = area_twoD;
  }
  
  if (this->mouseGrabber() != 0) {
    this->mouseGrabber()->releaseMouse();
  }
  area->setMouseTracking(true);
  
}

void XOrsaPlotTool_II::update_area(XOrsaPlotType t) {
  
  // cerr << "inside update_area..." << endl;
  
  SetBodiesIndex();
  // SetPlotType();
  plot_type = t;
  
  ComputeOrbitVector();
  FillPlotAreaData();
  
  area->SetData(curves);
  
  UpdateTitle();
  /* cerr << " update area: " << curves->size();
     if (curves->size() != 0) {
     unsigned int r;
     for (r=0;r<curves->size();r++) cerr << "  " << (*curves)[r].size();
     }
     cerr << endl;
  */
  
}


void XOrsaPlotTool_II::update_body(int b) {
  
  if (((unsigned int)b) != body_index) {
    
    SetBodiesIndex();
    // SetPlotType();
    
    XOrsaPlotArea *area_backup = area;
    
    area = area_kepler;
    rbody_index = robjects_combo_kepler->GetObject();
    update_area(kepler_type_combo->GetPlotType());
    //
    area = area_twoD;
    rbody_index = robjects_combo_twoD->GetObject();
    update_area(twoD_type_combo->GetPlotType());
    
    area = area_backup;
    
    /* ComputeOrbitVector();
       FillPlotAreaData();
       area->SetData(curves);
       UpdateTitle();
    */
  }
}


void XOrsaPlotTool_II::update_rbody(int) {
  
  /* the rbody index can be updated from different places,
   * and the test on its previous value is not useful here */
  // if  (b != rbody_index) {
  
  SetBodiesIndex();
  SetPlotType();
  
  ComputeOrbitVector();
  FillPlotAreaData();
  area->SetData(curves);
  UpdateTitle();
  
  // }
}


void XOrsaPlotTool_II::status_bar_plot_coords(QMouseEvent *me) {
  QString x_s, y_s;
  // FineLabel(x_s,area->x(me->pos()));
  // FineLabel(y_s,area->y(me->pos()));
  switch (area->X.GetType()) {
  case AT_DATE: FineDate( x_s,area->x(me->pos()),false); break;
  default:   FineLabel(x_s,area->x(me->pos())); break;
  }
  switch (area->Y.GetType()) {
  case AT_DATE: FineDate( y_s,area->y(me->pos()),false); break;
  default:   FineLabel(y_s,area->y(me->pos())); break;
  }
  // status_bar_label->setText("coords: (" + x_s + ";" + y_s + ")");
  status_bar_label->setText("[" + x_s + ";" + y_s + "]");
}


void XOrsaPlotTool_II::ComputeOrbitVector() {
  
  SetBodiesIndex();
  
  if ((*evol)[0].size() < 2) return;
  
  if (body_index == rbody_index) return;
  
  if ( ( body_index >= (*evol)[0].size()) ||
       (rbody_index >= (*evol)[0].size()) ) return;
  
  os.clear();
  
  // naive..
  os.timestep = (*evol)[1].Time() - (*evol)[0].Time();
  
  Frame f;
  Orbit o;
  
  // t_min = t_max = (*evol)[0].Time();
  
  // Body subject = (*bodies)[objects_combo->GetObject()];
  
  unsigned int k;
  
  for (k=0;k<evol->size();k++) {
    
    f = (*evol)[k];
    // o.Compute(f,body_index,rbody_index);
    o.Compute(f[body_index],f[rbody_index]);
    os.push_back(o);
  }
  
}


void XOrsaPlotTool_II::FillPlotAreaData() {
  
  /* 
  ORSA_ERROR("ref_body_is_fixed: %i",ref_body_is_fixed);
  ORSA_ERROR("use_direction_body: %i",use_direction_body);
  */
  
  curves->clear();
  
  XOrsaPlotPoint point;
  XOrsaPlotCurve cur;
  
  SetBodiesIndex();
  
  if (plot_type == DISTANCE) {
    
    if (universe->GetUniverseType() == Real) {
      area->X.SetType(AT_DATE);
    } else {
      area->X.SetType(AT_TIME);
    }	
    area->Y.SetType(AT_LENGTH);
    
    if ( ( body_index >= (*evol)[0].size()) ||
         (rbody_index >= (*evol)[0].size()) ) return;
    
    if (body_index == rbody_index) return;
    
    unsigned int t;
    
    cur.clear();
    for (t=0;t<evol->size();t++) {
      point.x = (*evol)[t].Time();
      point.y = ((*evol)[t][body_index].position() - (*evol)[t][rbody_index].position()).Length();
      cur.push_back(point);
    } 
    
    cur.index = body_index;
    cur.color = Qt::black;
    curves->push_back(cur);
    
  } else if ( (plot_type == A) ||
              (plot_type == E) ||
              (plot_type == I) ||
              (plot_type == NODE) ||
              (plot_type == PERI) || 
              (plot_type == MM) || 
              (plot_type == EE) || 
              (plot_type == PERIOD) ||
              (plot_type == PERI_DIST) || 
              (plot_type == APO_DIST) ) {
    
    if (universe->GetUniverseType() == Real) {
      area->X.SetType(AT_DATE);
    } else {
      area->X.SetType(AT_TIME);
    }
    //
    switch (plot_type) {
    case A:
    case PERI_DIST:
    case APO_DIST:
      area->Y.SetType(AT_LENGTH);
      break;
    case I:
    case NODE:
    case PERI:
    case MM:
    case EE:
      area->Y.SetType(AT_ANGLE);
      break;
    case PERIOD:
      area->Y.SetType(AT_TIME);
      break;
    default:
      area->Y.SetType(AT_GENERIC);
    }
    
    if ( ( body_index >= (*evol)[0].size()) ||
         (rbody_index >= (*evol)[0].size()) ) return;
    
    if (body_index == rbody_index) return;
    
    // check
    if (evol->size() != os.size()) std::cerr << "XOrsaPlotTool_II::FillPlotAreaData  WARNING: OrbitStream and Evolution have different a size!!   " << evol->size() << "  " << os.size() << std::endl;
    
    if (os.size() == 0) return;
    
    unsigned int k;
    
    cur.clear();
    cur.resize(os.size());
   
    for (k=0;k<os.size();k++) {
      cur[k].x = (*evol)[k].Time();
    }
    
    if (plot_type == A) for (k=0;k<os.size();k++) {
      cur[k].y = os[k].a;
    } else if (plot_type == E) for (k=0;k<os.size();k++) {
      cur[k].y = os[k].e;
    } else if (plot_type == I) for (k=0;k<os.size();k++) {
      cur[k].y = (180/pi)*os[k].i;
    } else if (plot_type == NODE) for (k=0;k<os.size();k++) {
      cur[k].y = (180/pi)*os[k].omega_node;
    } else if (plot_type == PERI) for (k=0;k<os.size();k++) {
      cur[k].y = (180/pi)*os[k].omega_pericenter;
    } else if (plot_type == MM) for (k=0;k<os.size();k++) {
      cur[k].y = (180/pi)*os[k].M;
    } else if (plot_type == EE) for (k=0;k<os.size();k++) {
      cur[k].y = (180/pi)*os[k].GetE();
    } else if (plot_type == PERIOD) for (k=0;k<os.size();k++) {
      cur[k].y = os[k].Period();
    } else if (plot_type == PERI_DIST) for (k=0;k<os.size();k++) {
      cur[k].y = os[k].a*fabs(1.0-os[k].e);
    } else if (plot_type == APO_DIST) for (k=0;k<os.size();k++) {
      cur[k].y = os[k].a*(1.0+os[k].e);
    }
    
    cur.index = 11;
    cur.color = Qt::black;
    curves->push_back(cur);
    
  } else if ( (plot_type == XY) ||
              (plot_type == XZ) ||
              (plot_type == YZ) ||
	      (plot_type == RZ) ) {
    
    area->X.SetType(AT_LENGTH);
    area->Y.SetType(AT_LENGTH);
    
    unsigned int t;
    unsigned int b;
    
    
    /* if (ref_body_is_fixed) {
       
       Vector rb_pos;
       
       if (plot_type == XY) for (b=0;b<(*evol)[0].size();b++) {
       cur.clear();
       for (t=0;t<evol->size();t++) {
       rb_pos  = (*evol)[t][rbody_index].position;
       point.x = (*evol)[t][b].position.x-rb_pos.x;
       point.y = (*evol)[t][b].position.y-rb_pos.y;
       cur.push_back(point);
       } 
       
       cur.index = b;
       cur.color = Qt::black;
       curves->push_back(cur);
       
       } else if (plot_type == XZ) for (b=0;b<(*evol)[0].size();b++) {
       cur.clear();
       for (t=0;t<evol->size();t++) {
       rb_pos  = (*evol)[t][rbody_index].position;
       point.x = (*evol)[t][b].position.x-rb_pos.x;
       point.y = (*evol)[t][b].position.z-rb_pos.z;
       cur.push_back(point);
       } 
       
       cur.index = b;
       cur.color = Qt::black;
       curves->push_back(cur);
       
       } else if (plot_type == YZ) for (b=0;b<(*evol)[0].size();b++) {
       cur.clear();
       for (t=0;t<evol->size();t++) {
       rb_pos  = (*evol)[t][rbody_index].position;
       point.x = (*evol)[t][b].position.y-rb_pos.y;
       point.y = (*evol)[t][b].position.z-rb_pos.z;
       cur.push_back(point);
       } 
       
       cur.index = b;
       cur.color = Qt::black;
       curves->push_back(cur);
       
       }
       
       } else { 
    */
    
    if (plot_type == XY) for (b=0;b<(*evol)[0].size();b++) {
      cur.clear();
      for (t=0;t<evol->size();t++) {
	point.x = (*evol)[t][b].position().x;
	point.y = (*evol)[t][b].position().y;
	cur.push_back(point);
      } 
      
      cur.index = b;
      cur.color = Qt::black;
      curves->push_back(cur);
      
    } else if (plot_type == XZ) for (b=0;b<(*evol)[0].size();b++) {
      cur.clear();
      for (t=0;t<evol->size();t++) {
	point.x = (*evol)[t][b].position().x;
	point.y = (*evol)[t][b].position().z;
	cur.push_back(point);
      } 
      
      cur.index = b;
      cur.color = Qt::black;
      curves->push_back(cur);
      
    } else if (plot_type == YZ) for (b=0;b<(*evol)[0].size();b++) {
      cur.clear();
      for (t=0;t<evol->size();t++) {
	point.x = (*evol)[t][b].position().y;
	point.y = (*evol)[t][b].position().z;
	cur.push_back(point);
      } 
      
      cur.index = b;
      cur.color = Qt::black;
      curves->push_back(cur);
      
    } else if (plot_type == RZ) for (b=0;b<(*evol)[0].size();b++) {
      cur.clear();
      for (t=0;t<evol->size();t++) {
	point.x = secure_sqrt(secure_pow((*evol)[t][b].position().x,2.0)+secure_pow((*evol)[t][b].position().y,2.0));
	point.y = (*evol)[t][b].position().z;
	cur.push_back(point);
      } 
      
      cur.index = b;
      cur.color = Qt::black;
      curves->push_back(cur);
      
    }
    
    if (ref_body_is_fixed) {
      
      bool found = false;
      XOrsaPlotCurve ref_curve;
      
      unsigned int kk=0;
      for (kk=0;kk<curves->size();kk++) {
	if (((*curves)[kk].index) == (int)rbody_index) {
	  ref_curve = (*curves)[kk];
	  found = true;
	  break;
	}
      }
      
      if (found) {
	
	unsigned int p,c;
	for (c=0;c<curves->size();c++) {
	  for (p=0;p<ref_curve.size();p++) {
	    (*curves)[c][p].x -= ref_curve[p].x;
	    (*curves)[c][p].y -= ref_curve[p].y;
	  }
	}
      }
      
      if (use_direction_body) {
	
	if (rbody_index == direction_combo->GetObject()) {
	  QMessageBox::warning( this, "Reference body is equal to direction Body!",
				"The reference body must be different than the direction body.",
				QMessageBox::Ok,QMessageBox::NoButton);
	  
	} else {
	  
	  XOrsaPlotCurve dir_curve;
	  
	  unsigned int j=0;
	  for (j=0;j<curves->size();j++) {
	    if (((*curves)[j].index) == direction_combo->GetObject()) {
	      dir_curve = (*curves)[j];
	      found = true;
	      break;
	    }
	  }
	  
	  if (found) {
	    
	    vector<double> angle_vec;
	    angle_vec.resize(dir_curve.size());
	    unsigned int p;
	    for (p=0;p<dir_curve.size();p++) {
	      angle_vec[p] = pi - secure_atan2(dir_curve[p].y,dir_curve[p].x);
	    }
	    
	    unsigned int c;
	    double angle,sin_angle,cos_angle;
	    double old_x,old_y;
	    for (c=0;c<curves->size();c++) {
	      for (p=0;p<dir_curve.size();p++) {
		
		angle = angle_vec[p];
		sin_angle = sin(angle);
		cos_angle = cos(angle);
		// cerr << "angle: " << angle << endl;
		old_x = (*curves)[c][p].x;
		old_y = (*curves)[c][p].y;
		(*curves)[c][p].x = old_x * cos_angle - old_y * sin_angle;
		(*curves)[c][p].y = old_y * cos_angle + old_x * sin_angle;
	      }
	    }
	  }
	}
      }      
    }
    
    unsigned int c; for (c=0;c<curves->size();c++) {
      if ((*curves)[c].index == (int) body_index) (*curves)[c].color = Qt::red;
      if ((*curves)[c].index == (int) rbody_index) (*curves)[c].color = Qt::green;
    }
    
  }
  
}


void XOrsaPlotTool_II::UpdateTitle() {
  
  SetBodiesIndex();
  
  QString  bs = bodies[ body_index].name().c_str();
  QString rbs = bodies[rbody_index].name().c_str();
  
  QString title;
  
  switch (plot_type) {
  case DISTANCE:  title = "Distance between " + bs + " and " + rbs;            break;
  case A:         title = bs + " semi-major axis with respect to " + rbs;      break;
  case E:         title = bs + " eccentricity with respect to " + rbs;         break;
  case I:         title = bs + " inclination with respect to " + rbs;          break; 
  case NODE:      title = bs + " longitude of ascending node with respect to " + rbs;       break;
  case PERI:      title = bs + " argument of pericenter with respect to " + rbs; break;
  case MM:        title = bs + " mean anomaly with respect to " + rbs;         break;
  case EE:        title = bs + " eccentric anomaly with respect to " + rbs;         break;
  case PERIOD:    title = bs + " revolution period with respect to " + rbs;    break;  
  case XY:        title = "X-Y plane plot"; break;
  case XZ:        title = "X-Z plane plot"; break;
  case YZ:        title = "Y-Z plane plot"; break;
  case RZ:        title = "R-Z plane plot"; break;
  case PERI_DIST: title = bs + " pericenter distance with respect to " + rbs; break;
  case APO_DIST:  title = bs + " apocenter distance with respect to " + rbs;  break;
  }
  
  area->SetTitle(title);
}

void XOrsaPlotTool_II::slot_ref_body_fixed(bool b) {
  ref_body_is_fixed = b; 
  use_direction->setEnabled(b);
  FillPlotAreaData();
  area->SetData(curves);
  UpdateTitle();
}

void XOrsaPlotTool_II::slot_use_direction(bool b) {
  use_direction_body = b; 
  SetPlotType();
  FillPlotAreaData();
  area->SetData(curves);
  UpdateTitle();
}

void XOrsaPlotTool_II::update_dirbody(int) {
  slot_use_direction(use_direction_body);
}
