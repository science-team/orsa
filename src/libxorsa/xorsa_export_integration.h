/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_EXPORT_INTEGRATION_H_
#define _XORSA_EXPORT_INTEGRATION_H_

#include <qdialog.h>
#include <qcombobox.h> 

#include <orsa_universe.h>

#include <xorsa_config.h>
#include <xorsa_objects_combo.h>

class QRadioButton;
class QPushButton;

enum CartesianMode {
  CM_XYZ,
  CM_TXYZ,
  CM_XYZT,
  CM_XY,
  CM_TXY,
  CM_XYT
};

class CartesianModeCombo : public QComboBox {
  
  Q_OBJECT
    
 public:
  CartesianModeCombo(QWidget *parent=0);
  
 private slots:
  void SetMode(int);
  
 public slots:
  void SetMode(CartesianMode);
  
 public:  
  CartesianMode GetMode() const;
  
 private:
  CartesianMode mode;
};

enum KeplerianMode {
  KM_AEI,
  KM_AEIT,
  KM_TAEI,
  KM_AEINPM,
  KM_AEINPMT,
  KM_TAEINPM
};

class KeplerianModeCombo : public QComboBox {
  
  Q_OBJECT
    
 public:
  KeplerianModeCombo(QWidget *parent=0);
  
 private slots:
  void SetMode(int);
  
 public slots:
  void SetMode(KeplerianMode);
  
 public:  
  KeplerianMode GetMode() const;
  
 private:
  KeplerianMode mode;
};

class XOrsaExportIntegration : public QDialog {
  
  Q_OBJECT
 
 public:
  XOrsaExportIntegration(const orsa::Evolution*, QWidget *parent=0);
  
 private slots:
  void ok_pressed();
  void cancel_pressed();
  void widgets_enabler();
  
 private:
  // XOrsaObjectsCombo *objects_combo;
  XOrsaImprovedObjectsCombo *objects_combo;
  // XOrsaObjectsCombo *ref_keplerian_objects_combo;
  XOrsaImprovedObjectsCombo *ref_keplerian_objects_combo;
  XOrsaFileEntry *file_entry;
  
 private:
  QRadioButton *cartesian_rb;
  QRadioButton *keplerian_rb;
  
 private:
  CartesianModeCombo *cartesian_mode_cb;
  KeplerianModeCombo *keplerian_mode_cb;
  
  /* 
     private:
     void print_cartesian(const Vector &v, const double time, const CartesianMode mode, FILE *file);
     void print_keplerian(const Orbit &o,  const double time, const KeplerianMode mode, FILE *file);
  */
  
 private:
  QPushButton *okpb, *cancpb;
  
 private:
  std::vector<orsa::Body> bodies;
  
 private:
  const orsa::Evolution *evol;
};

#endif // _XORSA_EXPORT_INTEGRATION_H_
