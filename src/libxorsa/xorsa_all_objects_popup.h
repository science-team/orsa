/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_ALL_OBJECTS_POPUP_H_
#define _XORSA_ALL_OBJECTS_POPUP_H_

#include <qpopupmenu.h> 

// #include "xorsa_all_objects_listview.h"
class XOrsaAllObjectsListView; // recursion problem when including the file above...

class XOrsaAllObjectsPopupMenu : public QPopupMenu {
  
  Q_OBJECT
  
 public:
  // XOrsaAllObjectsPopupMenu(QWidget *parent=0);
  XOrsaAllObjectsPopupMenu(XOrsaAllObjectsListView *listview);
  
 public:
  void SetOnItem(const bool);
  void SetHaveMassiveObjects(const bool);
  
 private:
  int id_new_cartesian, id_new_keplerian;
  int id_generate_cartesian, id_generate_keplerian;
  int id_import_JPL, id_import_astorb, id_import_TLE;
  int id_edit_menu, id_edit_cartesian, id_edit_keplerian;
  int id_copy, id_delete, id_select_all;
  
 private:
  XOrsaAllObjectsListView *listview;
  
};

#endif // _XORSA_ALL_OBJECTS_POPUP_H_
