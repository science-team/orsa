/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_OBJECTS_GENERATOR_KEPLERIAN_H_
#define _XORSA_OBJECTS_GENERATOR_KEPLERIAN_H_

#include <qdialog.h>

class QLineEdit;
class QLabel;
class QRadioButton;
class QButtonGroup;
class QSpinBox;

#include "xorsa_units_combo.h"
#include "xorsa_objects_combo.h"
#include "xorsa_date.h"

class XOrsaJPLPlanetsCombo;

#include <orsa_units.h>

class XOrsaObjectsGeneratorKeplerian : public QDialog {
  
  Q_OBJECT
    
 public:
  XOrsaObjectsGeneratorKeplerian(std::vector<orsa::BodyWithEpoch>&, QWidget *parent=0);
  
 private slots:
  void init_draw();
  void ok_pressed();
  void cancel_pressed();
  
 private:
  std::vector<orsa::BodyWithEpoch> &bodies;
  
 private:
  // XOrsaObjectsCombo *ref_body_combo;
  XOrsaImprovedObjectsCombo *ref_body_combo;
  XOrsaJPLPlanetsCombo *jpl_planets_combo;
  
 private:
  QLineEdit *le_name;
  QLineEdit *le_mass_m, *le_mass_M;
  QLineEdit *le_ka_m, *le_ke_m, *le_ki_m, *le_knode_m, *le_kperi_m, *le_km_m;
  QLineEdit *le_ka_M, *le_ke_M, *le_ki_M, *le_knode_M, *le_kperi_M, *le_km_M;
  
  QSpinBox  *sb_num;
  
  QPushButton *okpb;
  QPushButton *cancpb;
  
  XOrsaDatePushButton *epoch;
  
 private:
  bool internal_change;
  
  QButtonGroup *keplerian_elements_gb;
  
 private:
  std::vector<orsa::Body> bodies_for_combo;
  
 private:
  LengthCombo *spacecb;
  MassCombo   *masscb;
};

#endif // _XORSA_OBJECTS_GENERATOR_KEPLERIAN_H_
