/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_CLOSE_APPROACHES_H_
#define _XORSA_CLOSE_APPROACHES_H_

#include <qwidget.h>
#include <qlayout.h>
#include <qhbox.h> 
#include <qvbox.h> 
#include <qpushbutton.h> 

#include "xorsa_units_combo.h"
#include "xorsa_plot_area.h"
#include "xorsa_date.h"

#include <orsa_body.h>

class QLineEdit;
class QSpinBox;
class TimeCombo;
class LengthCombo;
class XOrsaJPLPlanetsWidget;
class QListView;

class XOrsaCloseApproachesDialog : public QWidget {
  
  Q_OBJECT
    
 public:
  XOrsaCloseApproachesDialog(QWidget *parent=0);
  
 public slots:
  void slot_import_asteroids();
  void slot_new_keplerian();
  void slot_compute();
  void slot_update_listview();
  
 private:
  std::vector<orsa::BodyWithEpoch> bodies;
  
 private:
  QLineEdit *le_sample_period;
  TimeCombo *tc_sample_period;
  
 private:
  QLineEdit *le_threshold;
  LengthCombo *lc_threshold;
  
 private:
  XOrsaJPLPlanetsWidget *jpl_planets_widget;
  
 private:
  QListView *listview;
  
 private:
  XOrsaDatePushButton *epoch_start, *epoch_stop;
};

#endif // _XORSA_CLOSE_APPROACHES_H_
