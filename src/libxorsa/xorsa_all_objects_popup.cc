/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_all_objects_popup.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#include "xorsa_all_objects_listview.h"

#include <orsa_universe.h>

using namespace orsa;

XOrsaAllObjectsPopupMenu::XOrsaAllObjectsPopupMenu(XOrsaAllObjectsListView *l) : QPopupMenu(l), listview(l) {
  
  QPopupMenu *new_pm = new QPopupMenu;
  //
  id_new_cartesian = new_pm->insertItem("cartesian",listview,SLOT(slot_new_cartesian()));
  id_new_keplerian = new_pm->insertItem("keplerian",listview,SLOT(slot_new_keplerian()));
  //
  insertItem("new",new_pm);
  
  QPopupMenu *generate_pm = new QPopupMenu;
  //
  id_generate_cartesian                 = generate_pm->insertItem("cartesian",listview,SLOT(slot_generate_cartesian()));
  id_generate_keplerian                 = generate_pm->insertItem("keplerian",listview,SLOT(slot_generate_keplerian()));
  //
  insertItem("generate",generate_pm);
  
#ifndef HAVE_GSL
  generate_pm->setEnabled(false);
#endif // HAVE_GSL
  
  QPopupMenu *import_pm = new QPopupMenu;
  //
  if (universe->GetUniverseType() == Simulated) {
    id_import_JPL = import_pm->insertItem("JPL",listview,SLOT(slot_import_JPL()));
    // enabled only if a JPL file is available from the config file
    {
      if (!(jpl_file->GoodFile())) {
	import_pm->setItemEnabled(id_import_JPL,false);
      }
    }
  }
  id_import_astorb = import_pm->insertItem("asteroids and comets",listview,SLOT(slot_import_astorb()));
  id_import_TLE    = import_pm->insertItem("artificial satellites from TLE file",listview,SLOT(slot_import_TLE()));
  //
  insertItem("import",import_pm);
  
  QPopupMenu *edit_pm = new QPopupMenu;
  //
  id_edit_cartesian = edit_pm->insertItem("cartesian",listview,SLOT(slot_edit_cartesian()));
  id_edit_keplerian = edit_pm->insertItem("keplerian",listview,SLOT(slot_edit_keplerian()));
  //
  id_edit_menu = insertItem("edit",edit_pm);
  
  id_copy       = insertItem("copy",listview,SLOT(slot_copy()));
  id_delete     = insertItem("delete",listview,SLOT(slot_delete()));
  id_select_all = insertItem("select all",listview,SLOT(slot_select_all()));
  
}

void XOrsaAllObjectsPopupMenu::SetOnItem(const bool on_item) {

  const bool visible = on_item;

  setItemVisible(id_edit_menu,  visible); 
  setItemVisible(id_copy,       visible);
  setItemVisible(id_delete,     visible);
  setItemVisible(id_select_all, visible);
  
}

void XOrsaAllObjectsPopupMenu::SetHaveMassiveObjects(const bool have_massive_objects) {
  
  const bool visible = have_massive_objects;
  
  setItemVisible(id_new_keplerian,                      visible);
  setItemVisible(id_generate_keplerian,                 visible);
  setItemVisible(id_edit_keplerian,                     visible);
}
