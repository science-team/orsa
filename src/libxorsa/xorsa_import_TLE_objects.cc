/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_import_TLE_objects.h"

#include <qlayout.h>
#include <qhbox.h> 

#include <orsa_file.h>

using namespace std;
using namespace orsa;

// XOrsaTLEFileTypeCombo

XOrsaTLEFileTypeCombo::XOrsaTLEFileTypeCombo(QWidget * parent) : QComboBox(false,parent) {
  
  insertItem(Label(TLE_NASA).c_str());
  insertItem(Label(TLE_GEO).c_str());
  insertItem(Label(TLE_GPS).c_str());
  insertItem(Label(TLE_ISS).c_str());
  insertItem(Label(TLE_KEPELE).c_str());
  insertItem(Label(TLE_VISUAL).c_str());
  insertItem(Label(TLE_WEATHER).c_str());
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetFileType(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0);
}

void XOrsaTLEFileTypeCombo::SetFileType(int i) {
  
  // look at the combobox for the right order
  switch (i) {
  case 0:  ft = TLE_NASA;    break;
  case 1:  ft = TLE_GEO;     break;
  case 2:  ft = TLE_GPS;     break;
  case 3:  ft = TLE_ISS;     break;
  case 4:  ft = TLE_KEPELE;  break;
  case 5:  ft = TLE_VISUAL;  break;
  case 6:  ft = TLE_WEATHER; break;
  }
}

void XOrsaTLEFileTypeCombo::SetFileType(ConfigEnum ft) {
  // look at the combobox for the right order
  switch (ft) {
  case TLE_NASA:     setCurrentItem(0);  SetFileType(0);  break;
  case TLE_GEO:      setCurrentItem(1);  SetFileType(1);  break;
  case TLE_GPS:      setCurrentItem(2);  SetFileType(2);  break;
  case TLE_ISS:      setCurrentItem(3);  SetFileType(3);  break;
  case TLE_KEPELE:   setCurrentItem(4);  SetFileType(4);  break;
  case TLE_VISUAL:   setCurrentItem(5);  SetFileType(5);  break;
  case TLE_WEATHER:  setCurrentItem(6);  SetFileType(6);  break;
  default: /********************************************/ break;
  }
}

ConfigEnum XOrsaTLEFileTypeCombo::GetFileType() {
  return (ft);
}

// XOrsaImportTLEObjectsDialog

XOrsaImportTLEObjectsDialog::XOrsaImportTLEObjectsDialog(std::vector<orsa::BodyWithEpoch> &b, QWidget *parent) : QDialog(parent,0,true), bodies(b) {
  
  QVBoxLayout * vlay = new QVBoxLayout(this);
  
  QHBox * file_box = new QHBox(this);
  file_box->setSpacing(3);
  //
  file_type_combo = new XOrsaTLEFileTypeCombo(file_box);
  fe = new XOrsaFileEntry(FE_OPEN_FILE,file_box);
  //
  connect(file_type_combo,SIGNAL(activated(int)),this,SLOT(update_file_entry()));
  //
  update_file_entry(); // int unused
  //
  vlay->addWidget(file_box);
  
  QWidget *bw = new QWidget(this);
  QHBoxLayout *hok = new QHBoxLayout(bw);
  //
  hok->addStretch();
  //
  okpb = new QPushButton(bw);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  cancpb = new QPushButton(bw);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
  //
  vlay->addWidget(bw);  
}

void XOrsaImportTLEObjectsDialog::update_file_entry() {
  /* 
     orsa::Config conf;
     OrsaConfigFile ocf(&conf);
     ocf.Read();
     ocf.Close();
  */
  QString path = config->paths[file_type_combo->GetFileType()]->GetValue().c_str();
  fe->setText(path);
}

void XOrsaImportTLEObjectsDialog::ok_pressed() {
  
  hide();
  
  XOrsaTLEFile tle;
  tle.SetFileName(fe->text().latin1());
  tle.Read();
  tle.Close();
  
  for (unsigned int k=0;k<tle.sat.size();++k) {
    bodies.push_back(tle.sat[k]);
  }
  
  ok = true;
  done(0);
}

void XOrsaImportTLEObjectsDialog::cancel_pressed() {
  ok=false;
  done(0);
}
