/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_OBJECTS_COMBO_H_
#define _XORSA_OBJECTS_COMBO_H_

#include <orsa_body.h>

#include <qcombobox.h> 

#include <map>

// note: negative values are returned by XOrsaImprovedObjectsCombo::GetObject()!!
enum SpecialHEXObject { HEX_NONE           = 0, // don't use this!
			HEX_ORIGIN         = (1<<0),
			HEX_CENTER_OF_MASS = (1<<1),
			HEX_FREE           = (1<<2),
			HEX_NOT_SET        = (1<<3),
			HEX_AUTO           = (1<<4)
};

class XOrsaImprovedObjectsCombo : public QComboBox {
  
  Q_OBJECT
    
 public:
  XOrsaImprovedObjectsCombo(QWidget *parent=0);
  XOrsaImprovedObjectsCombo(int special_objects_flags=0, QWidget *parent=0);
  XOrsaImprovedObjectsCombo(const std::vector<orsa::Body> *, const bool only_massive=false, QWidget *parent=0);
  XOrsaImprovedObjectsCombo(const std::vector<orsa::Body> *, const bool only_massive=false, int special_objects_flags=0, QWidget *parent=0);
  
 public:
  bool HEXFlag(const SpecialHEXObject hex) const {
    return (special_objects_flags & hex);
  }
  
 signals:
  void ObjectChanged(int);
  
 public:  
  int GetObject();
  orsa::Body GetBody();
  //
  void Set(const std::vector<orsa::Body> *, const bool only_massive=false);
  
 public slots:
  void SetObject(int);
  void SetObject(SpecialHEXObject);
  
 private:
  const int special_objects_flags;
  
 private:
  const std::vector<orsa::Body> * list;
  
 private: 
  std::map<int,int> map_list;
  
 private:
  std::map<int,int> map_special;
};

#endif // _XORSA_OBJECTS_COMBO_H_
