/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_export_integration.h"

#include <orsa_error.h>
#include <orsa_orbit.h>

#include <qlayout.h> 
#include <qvbuttongroup.h> 
#include <qradiobutton.h>
#include <qpushbutton.h> 
#include <qlabel.h> 
#include <qhgroupbox.h> 
#include <qmessagebox.h> 

using namespace orsa;

// CartesianModeCombo

CartesianModeCombo::CartesianModeCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("XYZ");
  insertItem("TXYZ");
  insertItem("XYZT");
  insertItem("XY");
  insertItem("TXY");
  insertItem("XYT");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetMode(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0); 
}

void CartesianModeCombo::SetMode(int i) {
  switch (i) {
  case 0: mode = CM_XYZ;  break;
  case 1: mode = CM_TXYZ; break;
  case 2: mode = CM_XYZT; break;
  case 3: mode = CM_XY;   break;
  case 4: mode = CM_TXY;  break;
  case 5: mode = CM_XYT;  break;
  }
}

void CartesianModeCombo::SetMode(CartesianMode m) {
  switch (m) {
  case CM_XYZ:  setCurrentItem(0); SetMode(0); break;
  case CM_TXYZ: setCurrentItem(1); SetMode(1); break;
  case CM_XYZT: setCurrentItem(2); SetMode(2); break;
  case CM_XY:   setCurrentItem(3); SetMode(3); break;
  case CM_TXY:  setCurrentItem(4); SetMode(4); break;
  case CM_XYT:  setCurrentItem(5); SetMode(5); break;
  }
}

CartesianMode CartesianModeCombo::GetMode() const {
  return mode;
}

// KeplerianModeCombo

KeplerianModeCombo::KeplerianModeCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("AEI");
  insertItem("AEIT");
  insertItem("TAEI");
  insertItem("AEINPM");
  insertItem("AEINPMT");
  insertItem("TAEINPM");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetMode(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0); 
}

void KeplerianModeCombo::SetMode(int i) {
  switch (i) {
  case 0: mode = KM_AEI;     break;
  case 1: mode = KM_AEIT;    break;
  case 2: mode = KM_TAEI;    break;
  case 3: mode = KM_AEINPM;  break;
  case 4: mode = KM_AEINPMT; break;
  case 5: mode = KM_TAEINPM; break; 
  }
}

void KeplerianModeCombo::SetMode(KeplerianMode m) {
  switch (m) {
  case KM_AEI:     setCurrentItem(0); SetMode(0); break;
  case KM_AEIT:    setCurrentItem(1); SetMode(1); break;
  case KM_TAEI:    setCurrentItem(2); SetMode(2); break;
  case KM_AEINPM:  setCurrentItem(3); SetMode(3); break;
  case KM_AEINPMT: setCurrentItem(4); SetMode(4); break;
  case KM_TAEINPM: setCurrentItem(5); SetMode(5); break;
  }
}

KeplerianMode KeplerianModeCombo::GetMode() const {
  return mode;
}

// XOrsaExportIntegration

XOrsaExportIntegration::XOrsaExportIntegration(const Evolution *e, QWidget *parent) : QDialog(parent,0,true), evol(e) {
  
  setCaption("Export Integration");
  
  QVBoxLayout *vlay = new QVBoxLayout(this,3);
  
  //
  
  QHGroupBox *object_group = new QHGroupBox(this);
  new QLabel("object:",object_group);
  // objects_combo = new XOrsaObjectsCombo(object_group);
  objects_combo = new XOrsaImprovedObjectsCombo(object_group);
  vlay->addWidget(object_group);
  //
  {
    bodies.clear();
    for (unsigned int k=0;k<(*evol)[0].size();++k) bodies.push_back((*evol)[0][k]);
    objects_combo->Set(&bodies);
  }
  
  //
  
  QHGroupBox *file_group = new QHGroupBox(this);
  new QLabel("file",file_group);
  file_entry = new XOrsaFileEntry(FE_SAVE_FILE,file_group); 
  connect(file_entry,SIGNAL(textChanged(const QString&)),this,SLOT(widgets_enabler()));
  vlay->addWidget(file_group);
  
  //
  
  QVButtonGroup *format_bg = new QVButtonGroup("file format",this);
  format_bg->setRadioButtonExclusive(true);
  format_bg->setColumns(4);
  
  cartesian_rb = new QRadioButton("cartesian",format_bg);
  cartesian_mode_cb = new CartesianModeCombo(format_bg);
  new QLabel(format_bg); // to keep sync
  new QLabel(format_bg); // to keep sync
  
  keplerian_rb = new QRadioButton("keplerian",format_bg);
  keplerian_mode_cb = new KeplerianModeCombo(format_bg);
  new QLabel("ref. body:",format_bg);
  ref_keplerian_objects_combo = new XOrsaImprovedObjectsCombo(format_bg);
  ref_keplerian_objects_combo->Set(&bodies,true);
  
  connect(cartesian_rb,SIGNAL(toggled(bool)),cartesian_mode_cb,SLOT(setEnabled(bool)));
  connect(keplerian_rb,SIGNAL(toggled(bool)),keplerian_mode_cb,SLOT(setEnabled(bool)));
  connect(keplerian_rb,SIGNAL(toggled(bool)),ref_keplerian_objects_combo,SLOT(setEnabled(bool)));
  
  cartesian_rb->setChecked(true);
  keplerian_rb->setChecked(true);
  cartesian_rb->setChecked(true);
  
  vlay->addWidget(format_bg);
  
  //
  
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  hok->addStretch();
  //
  okpb = new QPushButton(this);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  cancpb = new QPushButton(this);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
  
  //
  
  widgets_enabler();
  
  okpb->setAutoDefault(true);
}

static void print_cartesian(const Vector &v, const double time, const CartesianMode mode, FILE *file) {
  switch(mode) {
  case CM_XYZ:  fprintf(file,"%22.16f %22.16f %22.16f\n",         v.x,v.y,v.z);      break;
  case CM_TXYZ: fprintf(file,"%16.05f %22.16f %22.16f %22.16f\n", time,v.x,v.y,v.z); break;
  case CM_XYZT: fprintf(file,"%22.16f %22.16f %22.16f %16.05f\n", v.x,v.y,v.z,time); break;
  case CM_XY:   fprintf(file,"%22.16f %22.16f\n",                 v.x,v.y);          break;
  case CM_TXY:  fprintf(file,"%16.05f %22.16f %22.16f\n",         time,v.x,v.y);     break;
  case CM_XYT:  fprintf(file,"%22.16f %22.16f %16.05f\n",         v.x,v.y,time);     break;
  }
}

static void print_keplerian(const Orbit &o, const double time, const KeplerianMode mode, FILE *file) {
  switch(mode) {
  case KM_AEI:     fprintf(file,"%22.16f %22.16f %22.16f\n",                                 o.a,o.e,(180.0/pi)*o.i);                                                                           break;
  case KM_AEIT:    fprintf(file,"%22.16f %22.16f %22.16f %16.05f\n",                         o.a,o.e,(180.0/pi)*o.i,time);                                                                      break;
  case KM_TAEI:    fprintf(file,"%16.05f %22.16f %22.16f %22.16f\n",                         time,o.a,o.e,(180.0/pi)*o.i);                                                                      break;
  case KM_AEINPM:  fprintf(file,"%22.16f %22.16f %22.16f %22.16f %22.16f %22.16f\n",         o.a,o.e,(180.0/pi)*o.i,(180.0/pi)*o.omega_node,(180.0/pi)*o.omega_pericenter,(180.0/pi)*o.M);      break;
  case KM_AEINPMT: fprintf(file,"%22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %16.05f\n", o.a,o.e,(180.0/pi)*o.i,(180.0/pi)*o.omega_node,(180.0/pi)*o.omega_pericenter,(180.0/pi)*o.M,time); break;
  case KM_TAEINPM: fprintf(file,"%16.05f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f\n", time,o.a,o.e,(180.0/pi)*o.i,(180.0/pi)*o.omega_node,(180.0/pi)*o.omega_pericenter,(180.0/pi)*o.M); break;
  }
}

void XOrsaExportIntegration::ok_pressed() {  
  
  FILE *file = fopen(file_entry->text().latin1(),"w");
  if (file == 0) {
    QMessageBox::warning(this,"file problem",
			 "The file " + file_entry->text() + " cannot be opened in write mode.",
			 QMessageBox::Ok,QMessageBox::NoButton);
    return;
  }
  
  if (cartesian_rb->isChecked()) {
    const int body_index = objects_combo->GetObject();
    const CartesianMode mode = cartesian_mode_cb->GetMode();
    switch (universe->GetUniverseType()) {
    case Real: { 
      for (unsigned int k=0;k<evol->size();++k) {
	print_cartesian((*evol)[k][body_index].position(),(*evol)[k].GetDate().GetJulian(),mode,file);
      }
      break;
    }
    case Simulated: {
      for (unsigned int k=0;k<evol->size();++k) {
	print_cartesian((*evol)[k][body_index].position(),(*evol)[k].GetTime(),mode,file);
      }
      break;
    }
    }
  } else if (keplerian_rb->isChecked()) {
    const int body_index = objects_combo->GetObject();
    const int ref_body_index = ref_keplerian_objects_combo->GetObject();
    const KeplerianMode mode = keplerian_mode_cb->GetMode();
    Orbit orbit;
    switch (universe->GetUniverseType()) {
    case Real: { 
      for (unsigned int k=0;k<evol->size();++k) {
	orbit.Compute((*evol)[k][body_index],(*evol)[k][ref_body_index]);
	print_keplerian(orbit,(*evol)[k].GetDate().GetJulian(),mode,file);
      }
      break;
    }
    case Simulated: {
      for (unsigned int k=0;k<evol->size();++k) {
   	orbit.Compute((*evol)[k][body_index],(*evol)[k][ref_body_index]);
	print_keplerian(orbit,(*evol)[k].GetTime(),mode,file);
      }
      break;
    }
    }    
  } else {
    ORSA_LOGIC_ERROR("");
  }
  
  fclose(file);
  
  done(0);
}

void XOrsaExportIntegration::cancel_pressed() {
  done(0);
}

void XOrsaExportIntegration::widgets_enabler() {
  if (file_entry->text().isEmpty()) {
    okpb->setEnabled(false);
  } else {
    okpb->setEnabled(true);
  }
}
