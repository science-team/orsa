/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_ALL_OBJECTS_INFO_H_
#define _XORSA_ALL_OBJECTS_INFO_H_

#include <qgroupbox.h>
#include <qcombobox.h>

#include <orsa_body.h>

#include "xorsa_objects_combo.h"
#include "xorsa_import_JPL_objects.h"

#include <vector>

class QLabel;

#include "xorsa_all_objects_listview.h"

class XOrsaListViewMode : public QComboBox {
  
  Q_OBJECT
  
 public:
  XOrsaListViewMode(QWidget *parent=0);
  
  private slots:
    void SetMode(int);
  
  public slots:
    void SetMode(ObjectsListViewMode);
  
 signals:
  void ModeChanged();
  
 public:  
  inline ObjectsListViewMode GetMode() const {
    return mode;
  }	
  
 private:
  ObjectsListViewMode mode;
  
};

class XOrsaAllObjectsInfo : public QGroupBox {
  
  Q_OBJECT
    
 public:
  // XOrsaAllObjectsInfo(vector<BodyWithEpoch>&, vector<JPLBody>&, QWidget *parent=0);
  XOrsaAllObjectsInfo(std::vector<orsa::BodyWithEpoch>&, std::vector<orsa::JPL_planets>&, QWidget *parent=0);
  
 public slots:
  void update_info();
  void update_misc();
  
 private slots:	
  void widgets_enabler();
  
 public:
  // Frame StartFrame(Interaction*, Integrator*, const UniverseTypeAwareTime&);
  // void GetBodies(vector<BodyWithEpoch>&, vector<JPLBody>&);
  void GetBodies(std::vector<orsa::BodyWithEpoch>&, std::vector<orsa::JPL_planets>&);
  
 private:
  std::vector<orsa::BodyWithEpoch> & body;
  std::vector<orsa::JPL_planets>   & JPL_body;
  
 private:
  std::vector<orsa::Body> bodies_for_combo;
  
 private:
  XOrsaListViewMode * mode_combo;
  XOrsaImprovedObjectsCombo * obj_combo;
  XOrsaJPLPlanetsWidget * jpl_widget;
  
 private:
  XOrsaAllObjectsListView *listview;
  
 private:
  QLabel *info;
};

#endif // _XORSA_ALL_OBJECTS_INFO_H_
