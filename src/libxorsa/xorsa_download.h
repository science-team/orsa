/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_DOWNLOAD_H_
#define _XORSA_DOWNLOAD_H_

#include <string>
#include <vector>

#include <orsa_config.h>

#include <qwidget.h>
#include <qurloperator.h> 
#include <qurl.h> 

class QCheckBox;
class QLabel;
class QLineEdit;
class QGridLayout;
class QProgressBar;
class QPushButton;
class QTabWidget;
class QNetworkOperation;
class QHttp;
class QFtp;
class QFile;

enum XOrsaDownloadItemType {
  PLANET,
  ASTEROID,
  COMET,
  ARTIFICIAL_SATELLITE,
  TEXTURE,
  OTHER
};

XOrsaDownloadItemType Type(const orsa::ConfigEnum);

class XOrsaDownloadItem {
 public:
  XOrsaDownloadItem(const QString &url, orsa::ConfigEnum e, int s=0, bool d=false, std::string n="") : URL(url), config_enum(e), size(s), default_download(d), note(n) { }
  
 public:
  QUrl             URL;
  orsa::ConfigEnum config_enum;
  int              size; // used when the correct size cannot be obtained from the server
  bool             default_download;
  std::string      note;
};

class XOrsaDownloadEntry : public QObject, public XOrsaDownloadItem {
  
  Q_OBJECT
  
 public:
  XOrsaDownloadEntry(const XOrsaDownloadItem&, QWidget*);
  
 public:
  QLabel       *la;
  QLineEdit    *le;
  QProgressBar *bar;
  QPushButton  *pb;
  
  QFtp  *ftp;
  QHttp *http;
  QFile *file;
  
  public slots:
  void setProgress(int done, int total);
  void pb_clicked();  
  void post_download(bool);
  void download();
};


class XOrsaDownloadTabPage : public QWidget {
  
  Q_OBJECT
  
 public:
  XOrsaDownloadTabPage(QWidget *parent=0);
  
 public:	
  void InsertItem(const XOrsaDownloadItem&);
  
 private:
  QGridLayout *grid;
  
 public:
  std::vector<XOrsaDownloadEntry*> entries;
};


class XOrsaDownload : public QWidget {
  
  Q_OBJECT
  
 public:
  XOrsaDownload(std::vector<XOrsaDownloadItem>&, QWidget *parent=0);
  
 private:
  XOrsaDownloadTabPage * planets_w;
  XOrsaDownloadTabPage * asteroids_w;
  XOrsaDownloadTabPage * comets_w;
  XOrsaDownloadTabPage * art_sat_w;
  XOrsaDownloadTabPage * textures_w;
  XOrsaDownloadTabPage * others_w;
  
  private slots:
  void cancel_pressed();
  
 private:
  std::vector<XOrsaDownloadTabPage*> pages;
  
 private:
  QTabWidget * tab;
};

#endif // _XORSA_DOWNLOAD_H_
