/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_units_combo.h"

using namespace orsa;

// Time

TimeCombo::TimeCombo(QWidget * parent) : QComboBox(false,parent) {
  
  insertItem("second");
  insertItem("minute"); 
  insertItem("hour");
  insertItem("day");
  insertItem("year");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetUnit(int)));
  
  // sync!
  setCurrentItem(4);
  activated(4); 
  
}

void TimeCombo::SetUnit(int i) {
  
  // look at the combobox for the right order
  switch (i) {
  case 0: t = SECOND; break;
  case 1: t = MINUTE; break;
  case 2: t = HOUR;   break;
  case 3: t = DAY;    break;
  case 4: t = YEAR;   break;
  }
  
}

void TimeCombo::SetUnit(time_unit tu) {
  // look at the combobox for the right order
  switch (tu) {
  case SECOND: setCurrentItem(0); SetUnit(0); break;
  case MINUTE: setCurrentItem(1); SetUnit(1); break;
  case HOUR:   setCurrentItem(2); SetUnit(2); break;
  case DAY:    setCurrentItem(3); SetUnit(3); break;
  case YEAR:   setCurrentItem(4); SetUnit(4); break;
  }
}

time_unit TimeCombo::GetUnit() const {
  return t;
}


// Length

LengthCombo::LengthCombo(QWidget * parent) : QComboBox(false,parent) {
  
  insertItem("cm");
  insertItem("m");
  insertItem("km");
  insertItem("MR");
  insertItem("ER");
  insertItem("LD");
  insertItem("AU");
  insertItem("ly");
  insertItem("pc");
  insertItem("kpc");
  insertItem("Mpc");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetUnit(int)));
  
  // sync!
  setCurrentItem(6);
  activated(6);
  
}

void LengthCombo::SetUnit(int i) {
  // look at the combobox for the right order
  switch (i) {
  case 0:  l = CM;        break;
  case 1:  l =  M;        break;
  case 2:  l = KM;        break;
  case 3:  l = RMOON;     break;
  case 4:  l = REARTH;    break;
  case 5:  l = EARTHMOON; break;
  case 6:  l = AU;        break;
  case 7:  l = LY;        break;
  case 8:  l =  PARSEC;   break;
  case 9:  l = KPARSEC;   break;
  case 10: l = MPARSEC;   break;
  }  
}

void LengthCombo::SetUnit(length_unit lu) {
  // look at the combobox for the right order
  switch (lu) {
  case CM:        setCurrentItem(0);  SetUnit(0);  break;
  case M:         setCurrentItem(1);  SetUnit(1);  break;
  case KM:        setCurrentItem(2);  SetUnit(2);  break;
  case RMOON:     setCurrentItem(3);  SetUnit(3);  break;
  case REARTH:    setCurrentItem(4);  SetUnit(4);  break;
  case EARTHMOON: setCurrentItem(5);  SetUnit(5);  break;
  case AU:        setCurrentItem(6);  SetUnit(6);  break;
  case LY:        setCurrentItem(7);  SetUnit(7);  break;
  case  PARSEC:   setCurrentItem(8);  SetUnit(8);  break;
  case KPARSEC:   setCurrentItem(9);  SetUnit(9);  break;
  case MPARSEC:   setCurrentItem(10); SetUnit(10); break;
  }  
}

length_unit LengthCombo::GetUnit() const {
  return l;
}


// Mass

MassCombo::MassCombo(QWidget * parent) : QComboBox(false,parent) {
  
  insertItem("g");
  insertItem("kg");
  insertItem("Moon mass");
  insertItem("Earth mass");
  insertItem("Jupiter mass");
  insertItem("Sun mass");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetUnit(int)));
  
  // sync!
  setCurrentItem(5);
  activated(5);
}

void MassCombo::SetUnit(int i) {
  // look at the combobox for the right order
  switch (i) {
  case 0: m = GRAM;     break;
  case 1: m = KG;       break;
  case 2: m = MMOON;    break;
  case 3: m = MEARTH;   break;
  case 4: m = MJUPITER; break;
  case 5: m = MSUN;     break;
  }
}

void MassCombo::SetUnit(mass_unit mu) {
  // look at the combobox for the right order
  switch (mu) {
  case GRAM:     setCurrentItem(0); SetUnit(0); break;
  case KG:       setCurrentItem(1); SetUnit(1); break;
  case MMOON:    setCurrentItem(2); SetUnit(2); break;
  case MEARTH:   setCurrentItem(3); SetUnit(3); break;
  case MJUPITER: setCurrentItem(4); SetUnit(4); break;
  case MSUN:     setCurrentItem(5); SetUnit(5); break;
  }
}

mass_unit MassCombo::GetUnit() const {
  return m;
}

// TimeScale

TimeScaleCombo::TimeScaleCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("UTC");
  insertItem("UT1");
  insertItem("UT");
  insertItem("TAI");
  insertItem("TDT");
  insertItem("ET");
  insertItem("GPS");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetTimeScale(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0); 
  
}

void TimeScaleCombo::SetTimeScale(int i) {
  // look at the combobox for the right order
  switch (i) {
  case 0: ts = orsa::UTC; break;
  case 1: ts = orsa::UT1; break;
  case 2: ts = orsa::UT;  break;
  case 3: ts = orsa::TAI; break;
  case 4: ts = orsa::TDT; break;
  case 5: ts = orsa::ET;  break;
  case 6: ts = orsa::GPS; break;
  }
}

void TimeScaleCombo::SetTimeScale(TimeScale ts) {
  // look at the combobox for the right order
  switch (ts) {
  case orsa::UTC: setCurrentItem(0); SetTimeScale(0); break;
    // case orsa::UT1: setCurrentItem(1); SetTimeScale(1); break;
  case orsa::UT:  setCurrentItem(2); SetTimeScale(2); break;
  case orsa::TAI: setCurrentItem(3); SetTimeScale(3); break;
  case orsa::TDT: setCurrentItem(4); SetTimeScale(4); break;
    // case orsa::ET:  setCurrentItem(5); SetTimeScale(5); break;
  case orsa::GPS: setCurrentItem(6); SetTimeScale(6); break;
  }
}


TimeScale TimeScaleCombo::GetTimeScale() const {
  return ts;
}

