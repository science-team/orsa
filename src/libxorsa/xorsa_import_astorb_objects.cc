/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#include "xorsa_import_astorb_objects.h"
#include "xorsa_object_selector.h"
#include "xorsa_download.h"
#include "xorsa_config.h"

#ifdef HAVE_GSL
#include "xorsa_objects_generator_keplerian_covariance_file.h"
#endif // HAVE_GSL

#include <orsa_config.h>
#include <orsa_file.h>

#include <iostream>

using namespace std;

using namespace orsa;

#include <qlayout.h>
#include <qlineedit.h>
#include <qfiledialog.h>
#include <qhgroupbox.h> 
#include <qvgroupbox.h> 
#include <qlabel.h> 
#include <qpushbutton.h> 
#include <qgroupbox.h>
#include <qlistview.h>
#include <qpainter.h>
#include <qvbuttongroup.h> 
#include <qradiobutton.h> 
#include <qregexp.h> 
#include <qcombobox.h> 
#include <qvalidator.h> 
#include <qmessagebox.h> 

////
// XOrsaAstorbObjectsSelectPopupMenu
////

XOrsaAstorbObjectsSelectPopupMenu::XOrsaAstorbObjectsSelectPopupMenu(XOrsaImportAstorbObjectsAdvancedDialog *main_dialog, QWidget *parent) : QPopupMenu(parent), main(main_dialog) {
  insertItem("select",main,SLOT(slot_select()));
  insertItem("generate from covariance matrix",main,SLOT(slot_generate_from_covariance_matrix_from_file_listview()));
#ifndef HAVE_GSL
  setItemEnabled(idAt(1),false);
#endif
}

void XOrsaAstorbObjectsSelectPopupMenu::GenerateEnabled(bool b) {
  setItemEnabled(idAt(1),b);
}

////
// XOrsaAstorbObjectsRemovePopupMenu
////

XOrsaAstorbObjectsRemovePopupMenu::XOrsaAstorbObjectsRemovePopupMenu(XOrsaImportAstorbObjectsAdvancedDialog *main_dialog, QWidget *parent) : QPopupMenu(parent), main(main_dialog) {
  insertItem("remove",main,SLOT(slot_remove()));
  insertItem("generate from covariance matrix",main,SLOT(slot_generate_from_covariance_matrix_from_selected_listview()));
#ifndef HAVE_GSL
  setItemEnabled(idAt(1),false);
#endif
}

void XOrsaAstorbObjectsRemovePopupMenu::GenerateEnabled(bool b) {
  setItemEnabled(idAt(1),b);
}

///////////

XOrsaAsteroidFileTypeCombo::XOrsaAsteroidFileTypeCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem(Label(LOWELL_ASTORB).c_str());
  insertItem(Label(MPC_MPCORB).c_str());
  insertItem(Label(MPC_NEA).c_str());
  insertItem(Label(MPC_DAILY).c_str());
  insertItem(Label(MPC_DISTANT).c_str());
  insertItem(Label(MPC_PHA).c_str());
  insertItem(Label(MPC_UNUSUALS).c_str());
  insertItem(Label(MPC_COMET).c_str());
  insertItem(Label(ASTDYS_ALLNUM_CAT).c_str());
  insertItem(Label(ASTDYS_ALLNUM_CTC).c_str());
  insertItem(Label(ASTDYS_ALLNUM_CTM).c_str());
  insertItem(Label(ASTDYS_UFITOBS_CAT).c_str());
  insertItem(Label(ASTDYS_UFITOBS_CTC).c_str());
  insertItem(Label(ASTDYS_UFITOBS_CTM).c_str());
  insertItem(Label(NEODYS_CAT).c_str());
  insertItem(Label(NEODYS_CTC).c_str());
  insertItem(Label(JPL_DASTCOM_NUM).c_str());
  insertItem(Label(JPL_DASTCOM_UNNUM).c_str());
  insertItem(Label(JPL_DASTCOM_COMET).c_str());
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetFileType(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0);
}

void XOrsaAsteroidFileTypeCombo::SetFileType(int i) {
  
  // look at the combobox for the right order
  switch (i) {
  case 0:  ft = LOWELL_ASTORB;      break;
  case 1:  ft = MPC_MPCORB;         break;
  case 2:  ft = MPC_NEA;            break;
  case 3:  ft = MPC_DAILY;          break;
  case 4:  ft = MPC_DISTANT;        break;
  case 5:  ft = MPC_PHA;            break;
  case 6:  ft = MPC_UNUSUALS;       break;
  case 7:  ft = MPC_COMET;          break;
  case 8:  ft = ASTDYS_ALLNUM_CAT;  break;
  case 9:  ft = ASTDYS_ALLNUM_CTC;  break;
  case 10: ft = ASTDYS_ALLNUM_CTM;  break;
  case 11: ft = ASTDYS_UFITOBS_CAT; break;
  case 12: ft = ASTDYS_UFITOBS_CTC; break;
  case 13: ft = ASTDYS_UFITOBS_CTM; break;    
  case 14: ft = NEODYS_CAT;         break;
  case 15: ft = NEODYS_CTC;         break;
  case 16: ft = JPL_DASTCOM_NUM;    break;
  case 17: ft = JPL_DASTCOM_UNNUM;  break;
  case 18: ft = JPL_DASTCOM_COMET;  break;
  }
  
}

void XOrsaAsteroidFileTypeCombo::SetFileType(ConfigEnum ft) {
  // look at the combobox for the right order
  switch (ft) {
  case LOWELL_ASTORB:      setCurrentItem(0);  SetFileType(0);  break;
  case MPC_MPCORB:         setCurrentItem(1);  SetFileType(1);  break;
  case MPC_NEA:            setCurrentItem(2);  SetFileType(2);  break;
  case MPC_DAILY:          setCurrentItem(3);  SetFileType(3);  break;
  case MPC_DISTANT:        setCurrentItem(4);  SetFileType(4);  break;
  case MPC_PHA:            setCurrentItem(5);  SetFileType(5);  break;
  case MPC_UNUSUALS:       setCurrentItem(6);  SetFileType(6);  break;
  case MPC_COMET:          setCurrentItem(7);  SetFileType(7);  break;
  case ASTDYS_ALLNUM_CAT:  setCurrentItem(8);  SetFileType(8);  break;
  case ASTDYS_ALLNUM_CTC:  setCurrentItem(9);  SetFileType(9);  break;
  case ASTDYS_ALLNUM_CTM:  setCurrentItem(10); SetFileType(10); break;
  case ASTDYS_UFITOBS_CAT: setCurrentItem(11); SetFileType(11); break;
  case ASTDYS_UFITOBS_CTC: setCurrentItem(12); SetFileType(12); break;
  case ASTDYS_UFITOBS_CTM: setCurrentItem(13); SetFileType(13); break;
  case NEODYS_CAT:         setCurrentItem(14); SetFileType(14); break;
  case NEODYS_CTC:         setCurrentItem(15); SetFileType(15); break;
  case JPL_DASTCOM_NUM:    setCurrentItem(16); SetFileType(16); break;
  case JPL_DASTCOM_UNNUM:  setCurrentItem(17); SetFileType(17); break;
  case JPL_DASTCOM_COMET:  setCurrentItem(18); SetFileType(18); break;
  default: /**************************************************/ break;
  }
}

ConfigEnum XOrsaAsteroidFileTypeCombo::GetFileType() {
  return (ft);
}


// utils

static void add_item_to_listview(Asteroid asteroid, XOrsaAstorbObjectListView *listview) {
  
  QString s_number,s_name,s_a,s_e,s_i,s_note;
  
  if(asteroid.n != 0) {
    s_number.sprintf("  %7i ", asteroid.n);
  } else {
    s_number = "";
  }
  
  s_name = asteroid.name.c_str();
  s_a.sprintf("  %9.5f",asteroid.orb.a);
  s_e.sprintf("  %8.5f",asteroid.orb.e);
  s_i.sprintf("  %9.5f",asteroid.orb.i*(180/pi));
#ifdef HAVE_GSL
  if (asteroid.orb.have_covariance_matrix()) {
    s_note.sprintf("object with covariance matrix");
  } else {
    s_note = "";
  }
#else
  s_note = "";
#endif
  
  XOrsaAstorbObjectItem *aoi = new XOrsaAstorbObjectItem(listview,s_number,s_name,s_a,s_e,s_i,s_note);
  aoi->ast = asteroid;
  
}

////////////////
// XOrsaImportAstorbObjectsAdvancedDialog
//////////////

/* 
   XOrsaImportAstorbObjectsAdvancedDialog::XOrsaImportAstorbObjectsAdvancedDialog(vector<BodyWithEpoch> &list_in, QWidget *parent) : QDialog(parent,0,true), list(list_in) { 
   common_init();
   }
*/

XOrsaImportAstorbObjectsAdvancedDialog::XOrsaImportAstorbObjectsAdvancedDialog(vector<BodyWithEpoch> &list_in, QWidget *parent) : QDialog(parent, 0, true, Qt::WType_TopLevel | Qt::WDestructiveClose), list(list_in) { 
  common_init();
}

XOrsaImportAstorbObjectsAdvancedDialog::~XOrsaImportAstorbObjectsAdvancedDialog() {
  if (asteroid_database_file) delete asteroid_database_file;
}

void XOrsaImportAstorbObjectsAdvancedDialog::common_init() {
  
  asteroid_database_file = 0;
  
  read_thread = new ReadAstorbFileThread(this);
  
  int_validator = new QIntValidator(this);
  int_validator->setBottom(1); 
  //
  double_validator = new QDoubleValidator(this);
  double_validator->setBottom(0.0); 
  
  kepler_ref_body_active = false;
  
  setCaption("asteroids/comets import dialog");
  
  QVBoxLayout *vlay = new QVBoxLayout(this,4);
  
  /* 
     QHGroupBox *file_gb = new QHGroupBox("database file",this);
     file_type_combo = new XOrsaAsteroidFileTypeCombo(file_gb);
     file_le = new QLineEdit(file_gb);
     connect(file_type_combo,SIGNAL(activated(int)),this,SLOT(update_file_le(int)));
     // set_file_path();
     update_file_le(0); // int unused
     QPushButton *pb_file_browse = new QPushButton("browse",file_gb);
     // pb_file_browse->releaseKeyboard();
     connect(pb_file_browse,SIGNAL(clicked()),this,SLOT(slot_file_browse()));
     QPushButton *pb_read = new QPushButton("read",file_gb);
     // connect(pb_read,SIGNAL(clicked()),this,SLOT(update_listview()));
     connect(pb_read,SIGNAL(clicked()),this,SLOT(read_file()));
     vlay->addWidget(file_gb);
  */
  
  QHGroupBox *file_gb = new QHGroupBox("database file",this);
  file_type_combo = new XOrsaAsteroidFileTypeCombo(file_gb);
  // file_le = new QLineEdit(file_gb);
  file_entry = new XOrsaFileEntry(FE_OPEN_FILE,file_gb);
  connect(file_type_combo,SIGNAL(activated(int)),this,SLOT(update_file_entry()));
  update_file_entry(); // int unused
  // QPushButton *pb_file_browse = new QPushButton("browse",file_gb);
  // connect(pb_file_browse,SIGNAL(clicked()),this,SLOT(slot_file_browse()));
  QPushButton *pb_read = new QPushButton("read",file_gb);
  pb_read->setAutoDefault(false);
  connect(pb_read,SIGNAL(clicked()),this,SLOT(read_file()));
  vlay->addWidget(file_gb);
  
  ////

  if (universe->GetUniverseType() == Simulated) {
    
    QHBoxLayout *refbox = new QHBoxLayout(vlay);
    
    // QWidget *ref_body_widget = new QWidget(this);
    QLabel *refl = new QLabel(this);
    refl->setText("ref. body:");
    refbox->addWidget(refl);
    kepler_ref_body_label = new QLineEdit(this);
    kepler_ref_body_label->setReadOnly(true);
    kepler_ref_body_label->setText("none selected");
    refbox->addWidget(kepler_ref_body_label);
    xospb = new QPushButton("select",this);
    xospb->setAutoDefault(false);
    connect(xospb,SIGNAL(clicked()),this,SLOT(slot_object_selector()));
    refbox->addWidget(xospb);
  }
  
  ////
  
  listview_gb = new QVGroupBox("database file objects",this);
  
  // <<<<<<< xorsa_import_astorb_objects.cc
  file_info_label = new QLabel(listview_gb);
  
  // =======
  QVButtonGroup *displayed_bg = new QVButtonGroup("displayed objects",listview_gb);
  displayed_bg->setRadioButtonExclusive(true);
  displayed_bg->setColumns(2);
  //
  all_rb = new QRadioButton("all",displayed_bg);
  // QWidget *all_w = new QWidget(displayed_bg);
  new QLabel("(may take a few minutes to display)",displayed_bg);
  //
  all_numbered_rb = new QRadioButton("all numbered",displayed_bg);
  new QWidget(displayed_bg);
  // QWidget *all_numbered_w = new QWidget(displayed_bg);
  // new QLabel("(may take a few minutes to display)",displayed_bg);
  //
  select_rb = new QRadioButton("range:",displayed_bg);
  QWidget *range_w = new QWidget(displayed_bg);
  QHBoxLayout *range_w_hlay = new QHBoxLayout(range_w,4);
  range_combo = new QComboBox(false,range_w);
  range_combo->insertItem("number");
  range_combo->insertItem("a [AU]");
  range_combo->insertItem("e");
  range_combo->insertItem("i");
  //
  /* QFont greek_font; 
     greek_font.setFamily("Symbol");
     range_combo->setFont(greek_font);
  */
  // range_combo->insertItem("w+W+M");
  //
  connect(range_combo,SIGNAL(activated(int)),this,SLOT(range_combo_changed(int)));
  range_w_hlay->addWidget(range_combo);
  QLabel *range_start_l = new QLabel("from:",range_w);
  range_w_hlay->addWidget(range_start_l);
  range_start_le = new QLineEdit(range_w);
  range_w_hlay->addWidget(range_start_le);
  QLabel *range_stop_l = new QLabel("to:",range_w);
  range_w_hlay->addWidget(range_stop_l);
  range_stop_le  = new QLineEdit(range_w);
  range_w_hlay->addWidget(range_stop_le);
  //
  connect(select_rb,SIGNAL(toggled(bool)),range_w,SLOT(setEnabled(bool)));
  //
  range_combo->setCurrentItem(0);
  range_start_le->setValidator(int_validator);
  range_stop_le->setValidator(int_validator);
  //
  name_rb = new QRadioButton("name:",displayed_bg);
  QWidget *name_w = new QWidget(displayed_bg);
  QHBoxLayout *name_w_hlay = new QHBoxLayout(name_w);
  name_le = new QLineEdit(name_w);
  connect(name_le,SIGNAL(returnPressed()),this,SLOT(update_listview()));
  // connect(name_le,SIGNAL(returnPressed()),this,SLOT(update_listview()));
  name_w_hlay->addWidget(name_le);
  //
  connect(name_rb,SIGNAL(toggled(bool)),name_w,SLOT(setEnabled(bool)));
  //
  all_rb->setChecked(true);
  all_numbered_rb->setChecked(true);
  select_rb->setChecked(true);
  name_rb->setChecked(true);
  //
  all_numbered_rb->setChecked(true);
  
  QPushButton *update_listview_pb = new QPushButton("update list",listview_gb);
  update_listview_pb->setAutoDefault(true);
  connect(update_listview_pb,SIGNAL(clicked()),this,SLOT(update_listview()));
  
  listview_label = new QLabel(listview_gb);
  listview = new XOrsaAstorbObjectListView(listview_gb);

  connect(listview,SIGNAL(selectionChanged()),this,SLOT(update_listview_label()));
  
  // vlay->addWidget(listview);
  vlay->addWidget(listview_gb);
  
  //
  
  selected_listview_gb = new QVGroupBox("selected objects",this);
  
  selected_listview_label = new QLabel(selected_listview_gb);
  selected_listview = new XOrsaAstorbObjectListView(selected_listview_gb);
  
  QSizePolicy csp = selected_listview->sizePolicy();
  csp.setVerData(QSizePolicy::Fixed);
  selected_listview->setSizePolicy(csp);
  
  // vlay->addWidget(selected_listview);
  vlay->addWidget(selected_listview_gb);
  
  ////
  
  select_menu = new XOrsaAstorbObjectsSelectPopupMenu(this,this);
  connect(listview,SIGNAL(contextMenuRequested(QListViewItem *, const QPoint &, int)),this,SLOT(popup_select(QListViewItem *, const QPoint &, int)));
  connect(listview,SIGNAL(doubleClicked(QListViewItem *)),this,SLOT(select_item(QListViewItem *)));
  
  remove_menu = new XOrsaAstorbObjectsRemovePopupMenu(this,this);
  connect(selected_listview,SIGNAL(contextMenuRequested(QListViewItem *, const QPoint &, int)),this,SLOT(popup_remove(QListViewItem *, const QPoint &, int)));
  // connect(selected_listview,SIGNAL(doubleClicked(QListViewItem *)),this,SLOT(remove_item(QListViewItem *)));
  
  ////
  
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  
  hok->addStretch();
  //
  okpb = new QPushButton(this);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  okpb->setEnabled(false);
  //
  cancpb = new QPushButton(this);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
  
  ////
  
  // tab order for pb_file_browse, pb_read, xospb, update_listview_pb, okpb, cancpb
  // setTabOrder(update_listview_pb,pb_read);
  // update_listview_pb->setFocus();
  // focusWidget()->clearFocus();
  
  widgets_enabler();
  
}

/* 
   void XOrsaImportAstorbObjectsAdvancedDialog::slot_file_browse() {
   QString s =  QFileDialog::getOpenFileName(QString::null,
   QString::null,
   this,
   QString::null,
   "select file");
   if (!(s.isEmpty())) {
   file_entry->setText(s);
   } 
   widgets_enabler();
   }
*/

void XOrsaImportAstorbObjectsAdvancedDialog::slot_object_selector() {
  
  XOrsaObjectSelector *xos = new XOrsaObjectSelector(list,true,this);
  
  xos->show();
  xos->exec();
  
  if (xos->ok) {
    
    QString line;
    
    kepler_ref_body = xos->body_selected;
    line.sprintf("%s",kepler_ref_body.name().c_str());
    //
    kepler_ref_body_active = true;
    
    kepler_ref_body_label->setText(line);
    
    okpb->setEnabled(true);
  }
  
  widgets_enabler();
}

void XOrsaImportAstorbObjectsAdvancedDialog::wait_for_the_read_thread() {
  if (read_thread->running()) {
    asteroid_database_file->stop_read();
    read_thread->wait();
  }
}

void XOrsaImportAstorbObjectsAdvancedDialog::cancel_pressed() {
  
  wait_for_the_read_thread();
  
  listview->clear();
  selected_listview->clear();
  
  ok = false;
  done(0);
}

void XOrsaImportAstorbObjectsAdvancedDialog::closeEvent(QCloseEvent *e) {
  wait_for_the_read_thread();
  // should ask for a confirmation before...
  e->accept();
  ok = false;
  done(0);
}

void XOrsaImportAstorbObjectsAdvancedDialog::ok_pressed() {
  
  wait_for_the_read_thread();
  
  const UniverseType ut = universe->GetUniverseType();
  
  if ((ut==Real) || (ut==Simulated && kepler_ref_body_active)) {
    
    AsteroidDatabase asteroids_to_be_added;
    XOrsaAstorbObjectItem *ai;
    QListViewItemIterator it = selected_listview->firstChild();
    while (it.current() != 0) {
      ai = dynamic_cast <XOrsaAstorbObjectItem*> (it.current());
      if (ai) {
	asteroids_to_be_added.push_back(ai->ast);
      }
      it++;
    }
    
    if (asteroids_to_be_added.size() > 0) {
      
      if (ut==Real) {
	
	unsigned int j;
	Vector r,v; 
	Vector r_s, v_s;
	
	/* 
	   Config conf;
	   OrsaConfigFile ocf(&conf);
	   ocf.Read();
	*/
	const string jpl_path = config->paths[JPL_EPHEM_FILE]->GetValue().c_str();
	JPLFile jf(jpl_path);
	
	string name;
	char buffer[1024];
	AsteroidDatabase asteroids_excluded;
	
	for(j=0;j<asteroids_to_be_added.size();j++) {
	  
	  if ( (asteroids_to_be_added[j].orb.epoch.GetTime() < jf.EphemStart().GetTime()) ||
	       (asteroids_to_be_added[j].orb.epoch.GetTime() > jf.EphemEnd().GetTime()) ) {
	    
	    asteroids_excluded.push_back(asteroids_to_be_added[j]);
	    
	  } else {
	    
	    jf.GetEph(asteroids_to_be_added[j].orb.epoch,SUN,r_s,v_s);
	    
	    asteroids_to_be_added[j].orb.RelativePosVel(r,v);
	    
	    if (asteroids_to_be_added[j].n > 0) {
	      snprintf(buffer,1024,"(%05i) %s",asteroids_to_be_added[j].n,asteroids_to_be_added[j].name.c_str());
	      name = buffer;
	    } else {
	      name = asteroids_to_be_added[j].name;
	    }
	    
	    list.push_back(BodyWithEpoch(name,0.0,r+r_s,v+v_s,asteroids_to_be_added[j].orb.epoch));
	    
	  }
	  
	}
	
	if (asteroids_excluded.size()) {
	  
	  int y,m,d;
	  
	  QString eph_start;
	  jf.EphemStart().GetDate().GetGregor(y,m,d);
	  eph_start.sprintf("[%i/%i/%i]",y,m,d);
	  
	  QString eph_end;
	  jf.EphemEnd().GetDate().GetGregor(y,m,d);
	  eph_end.sprintf("[%i/%i/%i]",y,m,d);
	  
	  QString header = 
	    "Sorry, the following objects are excluded from the selection\n"
	    "because their epoch falls outside the JPL ephemeris validity\n"
	    "period " + eph_start + " -> " + eph_end + ":\n\n";
	  
	  QString obj_list;
	  unsigned int k=0;
	  QString s_epoch;
	  while (k<asteroids_excluded.size()) {
	    asteroids_excluded[k].orb.epoch.GetDate().GetGregor(y,m,d);
	    s_epoch.sprintf(" epoch: [%i/%i/%i]",y,m,d);
	    
	    obj_list.append(asteroids_excluded[k].name.c_str());
	    obj_list.append(s_epoch);
	    obj_list.append("\n");
	    ++k;
	  }
	  
	  QMessageBox::warning( this, "objects epoch outside JPL ephemeris period",
				header + obj_list,
				QMessageBox::Ok,QMessageBox::NoButton);
	}
	
      } else if ((ut==Simulated) && (kepler_ref_body_active)) {
	
	unsigned int j;
	Vector r,v; 
	char buffer[1024];
	string name;
	
	for(j=0;j<asteroids_to_be_added.size();j++) {
	  
	  asteroids_to_be_added[j].orb.RelativePosVel(r,v);
	  
	  if (asteroids_to_be_added[j].n > 0) {
	    snprintf(buffer,1024,"(%05i) %s",asteroids_to_be_added[j].n,asteroids_to_be_added[j].name.c_str());
	    name = buffer;
	  } else {
	    name = asteroids_to_be_added[j].name;
	  }
	  
	  list.push_back(BodyWithEpoch(name,0.0,r+kepler_ref_body.position(),v+kepler_ref_body.velocity(),asteroids_to_be_added[j].orb.epoch));
	  
	}
	
      }
    
    }
    
  }
  
  listview->clear();
  selected_listview->clear();
  
  ok = true;
  done(0);
}

void XOrsaImportAstorbObjectsAdvancedDialog::read_file() {
  
  if (!read_thread->running()) {
    
    if (asteroid_database_file) delete asteroid_database_file;
    
    // better position?
    switch(file_type_combo->GetFileType()) {
    case LOWELL_ASTORB:      asteroid_database_file = new XOrsaAsteroidDatabaseFile_AstorbFile;          break;
    case MPC_MPCORB:         asteroid_database_file = new XOrsaAsteroidDatabaseFile_MPCOrbFile;          break;
    case MPC_COMET:          asteroid_database_file = new XOrsaAsteroidDatabaseFile_MPCCometFile;        break;
    case MPC_NEA:            asteroid_database_file = new XOrsaAsteroidDatabaseFile_MPCOrbFile;          break;
    case MPC_DAILY:          asteroid_database_file = new XOrsaAsteroidDatabaseFile_MPCOrbFile;          break;
    case MPC_DISTANT:        asteroid_database_file = new XOrsaAsteroidDatabaseFile_MPCOrbFile;          break;
    case MPC_PHA:            asteroid_database_file = new XOrsaAsteroidDatabaseFile_MPCOrbFile;          break;
    case MPC_UNUSUALS:       asteroid_database_file = new XOrsaAsteroidDatabaseFile_MPCOrbFile;          break;
    case ASTDYS_ALLNUM_CAT:  asteroid_database_file = new XOrsaAsteroidDatabaseFile_NEODYSCAT;           break;
    case ASTDYS_ALLNUM_CTC:  asteroid_database_file = new XOrsaAsteroidDatabaseFile_AstDySMatrixFile;    break;
    case ASTDYS_ALLNUM_CTM:  asteroid_database_file = new XOrsaAsteroidDatabaseFile_AstDySMatrixFile;    break;
    case ASTDYS_UFITOBS_CAT: asteroid_database_file = new XOrsaAsteroidDatabaseFile_NEODYSCAT;           break;
    case ASTDYS_UFITOBS_CTC: asteroid_database_file = new XOrsaAsteroidDatabaseFile_AstDySMatrixFile;    break;
    case ASTDYS_UFITOBS_CTM: asteroid_database_file = new XOrsaAsteroidDatabaseFile_AstDySMatrixFile;    break;
    case NEODYS_CAT:         asteroid_database_file = new XOrsaAsteroidDatabaseFile_NEODYSCAT;           break;
    case NEODYS_CTC:         asteroid_database_file = new XOrsaAsteroidDatabaseFile_AstDySMatrixFile;    break;
    case JPL_DASTCOM_NUM:    asteroid_database_file = new XOrsaAsteroidDatabaseFile_JPLDastcomNumFile;   break;
    case JPL_DASTCOM_UNNUM:  asteroid_database_file = new XOrsaAsteroidDatabaseFile_JPLDastcomUnnumFile; break;
    case JPL_DASTCOM_COMET:  asteroid_database_file = new XOrsaAsteroidDatabaseFile_JPLDastcomCometFile; break;
    default: /****************************************************************************************/ break;
    }
    
    asteroid_database_file->SetFileName(file_entry->text().latin1());
    
    /* 
       XOrsaAstorbFileReadProgress *rp = new XOrsaAstorbFileReadProgress(this);
       connect(asteroid_database_file,SIGNAL(progress(int)),rp,SLOT(progress_slot(int)));
       connect(asteroid_database_file,SIGNAL(finished()),rp,SLOT(close()));
       rp->show();
    */
    
    connect(asteroid_database_file,SIGNAL(progress(int)),this,SLOT(progress_slot(int)));
    
    read_thread->asteroid_database_file = asteroid_database_file;
    read_thread->start();
  }
  
  widgets_enabler();
}

void XOrsaImportAstorbObjectsAdvancedDialog::progress_slot(int n) {
  update_file_labels(n);
}

void XOrsaImportAstorbObjectsAdvancedDialog::update_file_labels(int n) {
  
  if (n==0) n=asteroid_database_file->db->size();
  
  QString text;
  // text.sprintf("objects: %i",asteroid_database_file->db->size());
  text.sprintf("objects: %i",n);
  file_info_label->setText(text);
  
  // int_validator->setTop(asteroid_database_file->db->size());
  int_validator->setTop(n);
}

void XOrsaImportAstorbObjectsAdvancedDialog::update_listview() {
  
  if (!asteroid_database_file) return;
  
  // cerr << "inside update_listview()..." << endl;
  
  listview->clear();  
  
  asteroid_database_file->pause_read();
  
  if (asteroid_database_file->db->size()!=0) {
    
    bool selected;
    QRegExp regexp;
    int min_num=0, max_num=0;
    double min_range=0, max_range=0;
    
    if (all_rb->isChecked()) {
      
    } else if (all_numbered_rb->isChecked()) {
      
    } else if (select_rb->isChecked()) {
      
      if (range_combo->currentItem() == 0) {
	
	int start=range_start_le->text().toInt();
	int stop =range_stop_le->text().toInt();
	
	if (start<=stop) {
	  min_num=start;
	  max_num=stop;
	} else {
	  min_num=stop;
	  max_num=start;
	}
	
	if (min_num < 1) min_num=1;
	
      } else {
	
	double start=range_start_le->text().toDouble();
	double stop =range_stop_le->text().toDouble();
	
	if (start<=stop) {
	  min_range=start;
	  max_range=stop;
	} else {
	  min_range=stop;
	  max_range=start;
	}
	
      }
      
    } else if (name_rb->isChecked()) {
      
      regexp.setWildcard(true);
      regexp.setCaseSensitive(false);
      // regexp.setPattern(name_le->text().simplifyWhiteSpace());
      regexp.setPattern(name_le->text().stripWhiteSpace());
      
    }
    
    // while (itr != astorb_file.db->begin()) {
    unsigned int ast_k;
    // AstorbDataEntry current_ast;
    Asteroid current_ast;
    for (ast_k=0;ast_k<asteroid_database_file->db->size();++ast_k) {
      
      // cerr << " OK: inside while (itr != astorb_file.db->begin())..." << endl;
      
      // itr--;
      current_ast = (*(asteroid_database_file->db))[ast_k];
      selected=false;
      
      if (all_rb->isChecked()) {
	
	selected=true;
	
      } else if (all_numbered_rb->isChecked()) {
	
	// if ((*itr).n > 0) selected=true;
	// if ( (*(asteroid_database_file.db))[ast_k].n > 0) selected=true;
	if (current_ast.n > 0) selected=true;
	
      } else if (select_rb->isChecked()) {
	
	int    int_value=0;
	double double_value=0;
	
	/* 
	   switch (range_combo->currentItem()) {
	   case 0: int_value=(*itr).n; break; // number
	   case 1: double_value=(*itr).orb.a; break; // a
	   case 2: double_value=(*itr).orb.e; break; // e	  
	   case 3: double_value=(*itr).orb.i*(180/pi); break; // i	
	   }
	*/
	
	switch (range_combo->currentItem()) {
	case 0: int_value=current_ast.n; break; // number
	case 1: double_value=current_ast.orb.a; break; // a
	case 2: double_value=current_ast.orb.e; break; // e	  
	case 3: double_value=current_ast.orb.i*(180/pi); break; // i	
	}
	
	if (range_combo->currentItem() == 0) {
	  if ((int_value >= min_num) && (int_value <= max_num)) selected=true;
	} else {
	  if ((double_value >= min_range) && (double_value <= max_range)) selected=true;
	}
	
      } else if (name_rb->isChecked()) {
	// if (regexp.search((*itr).name.c_str()) != -1) selected=true;
     	if (regexp.exactMatch(current_ast.name.c_str())) selected=true;
      }
      
      if (selected) add_item_to_listview(current_ast,listview);
      
    }
    
    update_listview_label();
    update_selected_listview_label();
    
  }
  
  asteroid_database_file->continue_read();
  
}

void XOrsaImportAstorbObjectsAdvancedDialog::update_listview_label() {

  int selected=0;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      selected++;
    }
    it++;
  }
  
  QString label;
  label.sprintf("displayed objects: %i    highlighted: %i",listview->childCount(),selected);
  listview_label->setText(label);
}

void XOrsaImportAstorbObjectsAdvancedDialog::update_selected_listview_label() {
  QString label;
  label.sprintf("selected objects: %i",selected_listview->childCount());
  selected_listview_label->setText(label);  
}

void XOrsaImportAstorbObjectsAdvancedDialog::update_file_entry() {
  /* 
     orsa::Config conf;
     OrsaConfigFile ocf(&conf);
     ocf.Read();
  */
  QString path = config->paths[file_type_combo->GetFileType()]->GetValue().c_str();
  file_entry->setText(path);
}

void XOrsaImportAstorbObjectsAdvancedDialog::slot_select() {
  
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      select_item(it.current());
    }
    it++;
  }
  
  update_selected_listview_label();
  
  widgets_enabler();
}

void XOrsaImportAstorbObjectsAdvancedDialog::slot_remove() {
  
  vector<QListViewItem*> items_to_be_removed;
  QListViewItemIterator it = selected_listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      items_to_be_removed.push_back(it.current());
    }
    it++;
  }
  
  // remove from the end to the beginning, otherwise some bugs arise
  vector<QListViewItem*>::iterator itt = items_to_be_removed.end();
  while (itt != items_to_be_removed.begin()) {
    itt--;
    remove_item(*itt);
  } 
  
  update_selected_listview_label();
  
  widgets_enabler();
}

void XOrsaImportAstorbObjectsAdvancedDialog::slot_generate_from_covariance_matrix_from_file_listview() {
  private_generate_from_covariance_matrix(listview);
}

void XOrsaImportAstorbObjectsAdvancedDialog::slot_generate_from_covariance_matrix_from_selected_listview() {
  private_generate_from_covariance_matrix(selected_listview);
}

void XOrsaImportAstorbObjectsAdvancedDialog::private_generate_from_covariance_matrix(XOrsaAstorbObjectListView *local_listview) {
#ifdef HAVE_GSL
  vector<Asteroid> parent_asteroids;
  XOrsaAstorbObjectItem *ai_it;
  QListViewItemIterator it = local_listview->firstChild();
  while (it.current()) {
    if (it.current()->isSelected()) {
      ai_it = dynamic_cast <XOrsaAstorbObjectItem*> (it.current());
      if (ai_it) {
	parent_asteroids.push_back(ai_it->ast);
      }
    }
    it++;
  }
  
  vector<Asteroid> generated_asteroids;
  
  XOrsaObjectsGeneratorKeplerianCovarianceFile *gc = new XOrsaObjectsGeneratorKeplerianCovarianceFile(generated_asteroids,parent_asteroids);
  
  gc->show();
  gc->exec();
  
  unsigned int k;
  for (k=0;k<generated_asteroids.size();++k) {
    add_item_to_listview(generated_asteroids[k],selected_listview);
  }
  
  update_selected_listview_label();
  widgets_enabler();
#endif // HAVE_GSL
}

void XOrsaImportAstorbObjectsAdvancedDialog::select_item(QListViewItem *item) {
  
  XOrsaAstorbObjectItem *ai = dynamic_cast <XOrsaAstorbObjectItem*> (item);
  XOrsaAstorbObjectItem *ai_it;
  // check for duplicates!
  QListViewItemIterator it = selected_listview->firstChild();
  while (it.current() != 0) {
    ai_it = dynamic_cast <XOrsaAstorbObjectItem*> (it.current());
    if (ai_it) {
      if ( (ai_it->ast.n     == ai->ast.n) &&
	   (ai_it->ast.orb.a == ai->ast.orb.a) &&
	   (ai_it->ast.orb.e == ai->ast.orb.e) ) {
	return; // object already present in selected_listview
      }
    }
    it++;
  }
  
  /* 
     if (ai) {
     QString s_number,s_name,s_a,s_e,s_i,s_note;
     // AstorbDataEntry ast=ai->ast;
     Asteroid ast=ai->ast;
     
     if (ast.n != 0) {
     s_number.sprintf("  %7i ", ast.n);
     } else {
     s_number = "";
     }
     
     s_name = ast.name.c_str();
     s_a.sprintf("  %9.5f",ast.orb.a);
     s_e.sprintf("  %8.5f",ast.orb.e);
     s_i.sprintf("  %9.5f",ast.orb.i*(180/pi));
     #ifdef HAVE_GSL
     if (ast.orb.have_covariance_matrix()) {
     s_note.sprintf("object with covariance matrix");
     } else {
     s_note = "";
     }
     #else
     s_note = "";
     #endif
     
     XOrsaAstorbObjectItem *yyy = new XOrsaAstorbObjectItem(selected_listview,s_number,s_name,s_a,s_e,s_i,s_note);
     yyy->ast = ast;
     }
  */
  
  if (ai) add_item_to_listview(ai->ast,selected_listview);
  
  update_selected_listview_label();
  
  widgets_enabler();
}

void XOrsaImportAstorbObjectsAdvancedDialog::remove_item(QListViewItem *item) {
  delete item;
  
  update_selected_listview_label();
  
  widgets_enabler();
}

void XOrsaImportAstorbObjectsAdvancedDialog::popup_select(QListViewItem *item, const QPoint &point, int) {
  
#ifdef HAVE_GSL
  bool generate_enabled = false;
  //
  XOrsaAstorbObjectItem *ai_it;
  QListViewItemIterator it = listview->firstChild();
  while (it.current()) {
    if (it.current()->isSelected()) {
      ai_it = dynamic_cast <XOrsaAstorbObjectItem*> (it.current());
      if (ai_it) {
	if (ai_it->ast.orb.have_covariance_matrix()) {
	  generate_enabled = true;
	  break;
	}
      }
    }
    it++;
  }
  //
  select_menu->GenerateEnabled(generate_enabled);
#endif // HAVE_GSL  
  
  if (item) select_menu->popup(point);
  widgets_enabler();
}

void XOrsaImportAstorbObjectsAdvancedDialog::popup_remove(QListViewItem *item, const QPoint &point, int) {
  
#ifdef HAVE_GSL
  bool generate_enabled = false;
  //
  XOrsaAstorbObjectItem *ai_it;
  QListViewItemIterator it = selected_listview->firstChild();
  while (it.current()) {
    if (it.current()->isSelected()) {
      ai_it = dynamic_cast <XOrsaAstorbObjectItem*> (it.current());
      if (ai_it) {
	if (ai_it->ast.orb.have_covariance_matrix()) {
	  generate_enabled = true;
	  break;
	}
      }
    }	
    it++;
  }
  //
  remove_menu->GenerateEnabled(generate_enabled);
#endif // HAVE_GSL  
  
  if (item) remove_menu->popup(point);
  widgets_enabler();
}

void XOrsaImportAstorbObjectsAdvancedDialog::range_combo_changed(int index) {
  
  range_start_le->clear();
  range_stop_le->clear();
  
  if (index==0) {
    // int
    range_start_le->setValidator(int_validator);
    range_stop_le->setValidator(int_validator);
  } else {
    // double
    range_start_le->setValidator(double_validator);
    range_stop_le->setValidator(double_validator);
  }
}

void XOrsaImportAstorbObjectsAdvancedDialog::widgets_enabler() {
  
  const UniverseType ut = universe->GetUniverseType();
  
  if ( ((ut==Real) || (ut==Simulated && kepler_ref_body_active)) && 
       (selected_listview->childCount() > 0) ) {
    okpb->setEnabled(true);
  } else {
    okpb->setEnabled(false);
  }
  
  if (asteroid_database_file) {
    if (asteroid_database_file->db) {
      listview_gb->setEnabled(true);
      selected_listview_gb->setEnabled(true);
    }
  } else {
    listview_gb->setEnabled(false);
    selected_listview_gb->setEnabled(false);
  }
  
  /* 
     if (kepler_ref_body_active && (selected_listview->childCount() > 0)) {
     okpb->setEnabled(true);
     } else {
     okpb->setEnabled(false);
     }
  */
}
