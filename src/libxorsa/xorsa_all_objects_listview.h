/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_ALL_OBJECTS_LISTVIEW_H_
#define _XORSA_ALL_OBJECTS_LISTVIEW_H_

#include <qlistview.h>

#include <orsa_body.h>

#include "xorsa_all_objects_popup.h"

enum ObjectsListViewMode {Cartesian, Keplerian};

class XOrsaAllObjectsItem;

class XOrsaAllObjectsListView : public QListView {
  
  Q_OBJECT
  
 public:
  XOrsaAllObjectsListView(std::vector<orsa::BodyWithEpoch>&, QWidget *parent=0);
  
  public slots:
    inline void SetMode(const ObjectsListViewMode m) {
    mode = m;
    update_header();
    update_content();
  }
  
  /* 
     inline void SetHierarchical(bool b) {
     bool_hierarchical = b;
     if (bool_hierarchical) {
     setRootIsDecorated(true);
     } else {
     setRootIsDecorated(false);
     }
     update_content();
     }
  */
  
 public:
  inline ObjectsListViewMode GetMode() const {
    return mode;
  }
  
  public slots:
    inline void SetKeplerRefBodyIndex(int i) {
    kepler_ref_body_index = i;
    update_content();
  }
  
 public:	
  inline int GetKeplerRefBodyIndex() {
    return kepler_ref_body_index;
  }
  
 private slots:
  void popup(QListViewItem*, const QPoint&, int);
  
 private slots:
  void update_header();
  void update_content();
  void fill_item(XOrsaAllObjectsItem*);
  
 public slots:
  void slot_new_cartesian();
  void slot_new_keplerian();
  void slot_generate_cartesian();
  void slot_generate_keplerian();
  void slot_import_JPL();
  void slot_import_astorb();
  void slot_import_TLE();
  void slot_edit_cartesian();
  void slot_edit_keplerian();
  void slot_copy();
  void slot_delete();
  void slot_select_all();
  
 signals:
  void ObjectsChanged();
  
 private:
  XOrsaAllObjectsPopupMenu *menu;
  
 private:
  std::vector<orsa::BodyWithEpoch> &body;
  // vector<InitialCondition*> &body;
  
 private:
  ObjectsListViewMode mode;
  
 private:
  int kepler_ref_body_index;
  // bool bool_hierarchical;
};

class XOrsaAllObjectsItem : public QListViewItem {
  
  // DON'T use Q_OBJECT for this class
  // Q_OBJECT
  
 public:
  /* 
     inline XOrsaAllObjectsItem(std::vector<orsa::BodyWithEpoch>::iterator it, XOrsaAllObjectsListView *parent, QString label1 = QString::null, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8), b_it(it) { };
  */
  //
  inline XOrsaAllObjectsItem(orsa::BodyWithEpoch * ptr, XOrsaAllObjectsListView *parent, QString label1 = QString::null, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8), b_ptr(ptr), b_id(ptr->BodyId()) { };
     
 public:
  /* 
     inline XOrsaAllObjectsItem(std::vector<orsa::BodyWithEpoch>::iterator it, XOrsaAllObjectsItem *parent, QString label1 = QString::null, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8), b_it(it) { };
  */
  //
  inline XOrsaAllObjectsItem(orsa::BodyWithEpoch * ptr, XOrsaAllObjectsItem *parent, QString label1 = QString::null, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8), b_ptr(ptr), b_id(ptr->BodyId()) { };
    
 public:
  int compare(QListViewItem *i, int col, bool ascending) const;
  
 public:
  inline ObjectsListViewMode mode() const {
    QListView *listview = listView();
    XOrsaAllObjectsListView *all_listview = dynamic_cast <XOrsaAllObjectsListView*> (listview);
    if (all_listview) {
      return (all_listview->GetMode());
    }
    return ObjectsListViewMode();
  }
  
 public:
  const orsa::BodyWithEpoch * b_ptr;
  const unsigned int b_id;
};

#endif // _XORSA_ALL_OBJECTS_LISTVIEW_H_
