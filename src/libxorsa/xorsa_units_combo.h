/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_UNITS_COMBO_H_
#define _XORSA_UNITS_COMBO_H_

#include <orsa_units.h>

#include <qcombobox.h> 

class TimeCombo : public QComboBox {
  
  Q_OBJECT
    
 public:
  TimeCombo(QWidget *parent=0);
  
 private slots:
  void SetUnit(int);
  
 public slots:
  void SetUnit(orsa::time_unit);
  
 public:  
  orsa::time_unit GetUnit() const;
  
 private:
  orsa::time_unit t;
};


class LengthCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  LengthCombo(QWidget *parent=0);
  
 private slots:
  void SetUnit(int);
  
 public slots:
  void SetUnit(orsa::length_unit);
  
 public:  
  orsa::length_unit GetUnit() const;
  
 private:
  orsa::length_unit l;
};


class MassCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  MassCombo(QWidget *parent=0);
  
 private slots:
  void SetUnit(int);
  
 public slots:
  void SetUnit(orsa::mass_unit);
  
 public:  
  orsa::mass_unit GetUnit() const;
  
 private:
  orsa::mass_unit m;
};


class TimeScaleCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  TimeScaleCombo(QWidget *parent=0);
  
 private slots:
  void SetTimeScale(int);
  
 public slots:
  void SetTimeScale(orsa::TimeScale);
  
 public:  
  orsa::TimeScale GetTimeScale() const;
  
 private:
  orsa::TimeScale ts;
};


#endif // _XORSA_UNITS_COMBO_H_
