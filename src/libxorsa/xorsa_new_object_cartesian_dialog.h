/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_NEW_OBJECT_CARTESIAN_DIALOG_H_
#define _XORSA_NEW_OBJECT_CARTESIAN_DIALOG_H_

#include <qdialog.h>

#include <vector>

class QLineEdit;
class QLabel;

#include "xorsa_units_combo.h"
#include "xorsa_date.h"

#include <orsa_body.h>

class XOrsaNewObjectCartesianDialog : public QDialog {
  
  Q_OBJECT
  
 public:
  
  // new
  XOrsaNewObjectCartesianDialog(QWidget *parent=0);
  
  // edit
  XOrsaNewObjectCartesianDialog(orsa::BodyWithEpoch&, QWidget *parent=0);
  
 public:
  orsa::BodyWithEpoch GetBody();
  
 protected:
  void SetBody();
  
  private slots:
  void ok_pressed();
  void cancel_pressed();
  
 private:
  void init_draw();
  void init_values();
  
 private:
  QLineEdit *le_name;
  
  QLineEdit *le_px, *le_py, *le_pz;
  QLineEdit *le_vx, *le_vy, *le_vz;
  
  QLineEdit *le_mass;
  
  XOrsaDatePushButton *odpb;
  orsa::UniverseTypeAwareTime epoch; // needed!!
  
  QPushButton *okpb;
  QPushButton *cancpb;
  
 private:
  LengthCombo *pos_spacecb;
  LengthCombo *vel_spacecb;
  TimeCombo   *vel_timecb;
  MassCombo   *masscb;
  
 public:
  bool ok; // wheter the ok button has been pressed

 private:
  orsa::BodyWithEpoch b, ref_body;
};

#endif // _XORSA_NEW_OBJECT_CARTESIAN_DIALOG_H_
