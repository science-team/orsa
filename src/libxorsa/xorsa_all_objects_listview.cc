/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_all_objects_listview.h"

#include <qmessagebox.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#include <xorsa_new_object_cartesian_dialog.h>
#include <xorsa_new_object_keplerian_dialog.h>
#ifdef HAVE_GSL
#include <xorsa_objects_generator_cartesian.h>
#include <xorsa_objects_generator_keplerian.h>
#endif // HAVE_GSL
#include <xorsa_plot_area.h>
#include <xorsa_import_JPL_objects.h>
#include <xorsa_import_TLE_objects.h>
#include <xorsa_import_astorb_objects.h>

#include <orsa_orbit.h>
#include <orsa_frame.h>

using namespace std;
using namespace orsa;

int XOrsaAllObjectsItem::compare(QListViewItem *i, int col, bool ascending) const {
  
  switch(mode()) {
  case Cartesian: {
    
    // doubles
    if ((col == 1) || (col == 2) || (col == 3)) {
      const double d = (atof(key( col, ascending ).latin1()) - atof( i->key(col, ascending).latin1()));
      if (d < 0.0) return (-1);
      if (d > 0.0) return (+1);
      return (0);
    }
    
    // default
    return key(col,ascending).compare(i->key(col,ascending));
    
    break;
  }
  case Keplerian: {
    
    // doubles
    if ((col == 1) || (col == 3) || (col == 4) || (col == 5) || (col == 6) || (col == 7) || (col == 8) || (col == 9)) {
      
      const char *k = key(col,ascending).latin1();
      const char *item_k = i->key(col,ascending).latin1();
      
      // check for empty fields
      if (k == 0) {
	if (item_k == 0) {
	  return 0;
	} else {
	  return (-1);
	}
      } else if (item_k == 0) {
	return (+1);
      }
      
      const double d = atof(k)-atof(item_k);
      if (d < 0.0) return (-1);
      if (d > 0.0) return (+1);
      return (0);
    }
    
    // epoch->double
    if (col == 2) {
      XOrsaAllObjectsItem * aoit = dynamic_cast <XOrsaAllObjectsItem*> (i);
      if (aoit) {
	const double d = b_ptr->Epoch().GetTime() - aoit->b_ptr->Epoch().GetTime();
	if (d < 0.0) return (-1);
	if (d > 0.0) return (+1);
	return (0);
      } else {
	// default
	return key(col,ascending).compare(i->key(col,ascending));
      }
    }
    
    // default
    return key(col,ascending).compare(i->key(col,ascending));
    
    break;
  }
  }
  
  // default
  return key(col,ascending).compare(i->key(col,ascending));
  
}


XOrsaAllObjectsListView::XOrsaAllObjectsListView(vector<BodyWithEpoch> &b, QWidget *parent) : QListView(parent), body(b) {
  
  setAllColumnsShowFocus(true);
  setShowSortIndicator(true);
  setSelectionMode(QListView::Extended);
  setItemMargin(3);
  
  connect(this,SIGNAL(ObjectsChanged()),this,SLOT(update_content()));
  
  menu = new XOrsaAllObjectsPopupMenu(this);
  connect(this,SIGNAL(rightButtonPressed(QListViewItem*, const QPoint &, int)),this,SLOT(popup(QListViewItem*, const QPoint &, int)));
  
  SetMode(Cartesian);
  
  kepler_ref_body_index=0;
  // bool_hierarchical=false;
}

void XOrsaAllObjectsListView::update_header() {
  
  // check this numbers when changing columns
  const int cartesian_columns = 4;
  // const int keplerian_columns = 9;
  const int keplerian_columns = 10;
  
  QString name_label, mass_label, position_label, velocity_label, epoch_label, a_label, q_label, e_label, i_label, node_label, peri_label, M_label;
  name_label.sprintf("name");
  mass_label.sprintf("mass [%s]",MassLabel().c_str());
  position_label.sprintf("position [%s]",LengthLabel().c_str());
  velocity_label.sprintf("velocity [%s/%s]",LengthLabel().c_str(),TimeLabel().c_str());
  // epoch_label.sprintf("epoch");
  epoch_label.sprintf("epoch [%s]",TimeScaleLabel(universe->GetTimeScale()).c_str());
  a_label.sprintf("a [%s]",LengthLabel().c_str());
  q_label.sprintf("q [%s]",LengthLabel().c_str());
  e_label.sprintf("e");
  i_label.sprintf("i [deg]");
  // node_label.sprintf("node [deg]");
  QChar   *uc_Omega = new QChar(0x03A9);
  QString suc_Omega(uc_Omega,1);  
  node_label = suc_Omega;
  node_label.append(" [deg]");
  // peri_label.sprintf("pericenter [deg]");
  QChar   *uc_omega = new QChar(0x03C9);
  QString suc_omega(uc_omega,1);    
  peri_label = suc_omega;
  peri_label.append(" [deg]");
  M_label.sprintf("M [deg]");
  
  switch(mode) {
  case Cartesian: {
    
    while (columns() < cartesian_columns) addColumn("");
    while (columns() > cartesian_columns) removeColumn(columns()-1);
    
    setColumnText(0, name_label);      setColumnAlignment(0,Qt::AlignLeft);   setColumnWidth(0,fontMetrics().width(name_label)*3/2);      setColumnWidthMode(0,QListView::Maximum);
    setColumnText(1, mass_label);      setColumnAlignment(1,Qt::AlignRight);  setColumnWidth(1,fontMetrics().width(mass_label)*3/2);      setColumnWidthMode(1,QListView::Maximum);
    setColumnText(2, position_label);  setColumnAlignment(2,Qt::AlignRight);  setColumnWidth(2,fontMetrics().width(position_label)*3/2);  setColumnWidthMode(2,QListView::Maximum);
    setColumnText(3, velocity_label);  setColumnAlignment(3,Qt::AlignRight);  setColumnWidth(3,fontMetrics().width(velocity_label)*3/2);  setColumnWidthMode(3,QListView::Maximum);
    
    break;
  }
  case Keplerian: {
    
    while (columns() < keplerian_columns) addColumn("");
    while (columns() > keplerian_columns) removeColumn(columns()-1);
    
    setColumnText(0, name_label);   setColumnAlignment(0,Qt::AlignLeft);   setColumnWidth(0,fontMetrics().width(name_label)*3/2);   setColumnWidthMode(0,QListView::Maximum);
    setColumnText(1, mass_label);   setColumnAlignment(1,Qt::AlignRight);  setColumnWidth(1,fontMetrics().width(mass_label)*3/2);   setColumnWidthMode(1,QListView::Maximum);
    setColumnText(2, epoch_label);  setColumnAlignment(2,Qt::AlignRight);  setColumnWidth(2,fontMetrics().width(epoch_label)*3/2);  setColumnWidthMode(2,QListView::Maximum);
    setColumnText(3, a_label);      setColumnAlignment(3,Qt::AlignRight);  setColumnWidth(3,fontMetrics().width(a_label)*3/2);      setColumnWidthMode(3,QListView::Maximum);
    setColumnText(4, q_label);      setColumnAlignment(4,Qt::AlignRight);  setColumnWidth(4,fontMetrics().width(q_label)*3/2);      setColumnWidthMode(4,QListView::Maximum);
    setColumnText(5, e_label);      setColumnAlignment(5,Qt::AlignRight);  setColumnWidth(5,fontMetrics().width(e_label)*7/2);      setColumnWidthMode(5,QListView::Maximum);
    setColumnText(6, i_label);      setColumnAlignment(6,Qt::AlignRight);  setColumnWidth(6,fontMetrics().width(i_label)*3/2);      setColumnWidthMode(6,QListView::Maximum);
    setColumnText(7, node_label);   setColumnAlignment(7,Qt::AlignRight);  setColumnWidth(7,fontMetrics().width(node_label)*3/2);   setColumnWidthMode(7,QListView::Maximum);
    setColumnText(8, peri_label);   setColumnAlignment(8,Qt::AlignRight);  setColumnWidth(8,fontMetrics().width(peri_label)*3/2);   setColumnWidthMode(8,QListView::Maximum);
    setColumnText(9, M_label);      setColumnAlignment(9,Qt::AlignRight);  setColumnWidth(9,fontMetrics().width(M_label)*3/2);      setColumnWidthMode(9,QListView::Maximum);
    
    break;
  }
  }
  
}

void XOrsaAllObjectsListView::update_content() {
  
  clear();
  
  if (body.size() == 0) return;
  
  vector<BodyWithEpoch>::iterator itr = body.begin();
  while (itr != body.end()) {
    // fill_item(new XOrsaAllObjectsItem(itr,this));
    fill_item(new XOrsaAllObjectsItem(&(*itr),this));
    ++itr; 
    // qApp->processEvents(100);
  }
}

void XOrsaAllObjectsListView::fill_item(XOrsaAllObjectsItem * ii) {
  
  if (!ii) return;
  
  switch(mode) {
  case Cartesian: {
    
    QString m,p,v;
    
    const BodyWithEpoch * b = ii->b_ptr;
    
    /* 
       m.sprintf("%g",b.mass());
       p.sprintf("%.3g (%.3g;%.3g;%.3g)",Length(b.position()),b.position().x,b.position().y,b.position().z);
       v.sprintf("%.3g (%.3g;%.3g;%.3g)",Length(b.velocity()),b.velocity().x,b.velocity().y,b.velocity().z);
    */
    //
    m.sprintf("%g",b->mass());
    p.sprintf("%.3g (%.3g;%.3g;%.3g)",b->position().Length(),b->position().x,b->position().y,b->position().z);
    v.sprintf("%.3g (%.3g;%.3g;%.3g)",b->velocity().Length(),b->velocity().x,b->velocity().y,b->velocity().z);
    
    // ii->setText(0,b.name().c_str());
    ii->setText(0,b->name().c_str());
    ii->setText(1,m);
    ii->setText(2,p);
    ii->setText(3,v);
    
    break;
  }
  case Keplerian: {
    
    // const BodyWithEpoch b = (*ii->b_it);
    // BodyWithEpoch *b = &(*(ii->b_it));
    const BodyWithEpoch * b = ii->b_ptr;
    
    QString m,epoch;
    m.sprintf("%g",b->mass());
    switch (universe->GetUniverseType()) {
    case Real: { 
      FineDate(epoch,b->Epoch());
      break;
    }
    case Simulated: {
      epoch.sprintf("---");
      break;
    }
    }
    
    if (universe->GetUniverseType() == Simulated) {
      if (body[kepler_ref_body_index].BodyId() == b->BodyId()) {
	
	ii->setText(0,b->name().c_str());
	ii->setText(1,m);
	ii->setText(2,epoch);
	
	break;
      }
    }
    
    Orbit orbit;
    
    switch (universe->GetUniverseType()) {
    case Real: { 
      JPLBody orbit_ref_body(SUN,b->Epoch().GetDate());
      orbit.Compute(*b,orbit_ref_body);
      break;
    }
    case Simulated: {
      Body orbit_ref_body = body[kepler_ref_body_index];
      orbit.Compute(*b,orbit_ref_body);
      break;
    }
    }
    
    QString a,q,e,i,node,peri,M;
    a.sprintf("%g",orbit.a);
    q.sprintf("%g",orbit.a*fabs(1.0-orbit.e));
    e.sprintf("%g",orbit.e);
    i.sprintf("%g",orbit.i*(180/pi));
    node.sprintf("%g",orbit.omega_node*(180/pi));
    peri.sprintf("%g",orbit.omega_pericenter*(180/pi));
    M.sprintf("%g",orbit.M*(180/pi));
    
    ii->setText(0,b->name().c_str());
    ii->setText(1,m);
    ii->setText(2,epoch);
    ii->setText(3,a);
    ii->setText(4,q);
    ii->setText(5,e);
    ii->setText(6,i);
    ii->setText(7,node);
    ii->setText(8,peri);
    ii->setText(9,M);
    
    break;
  }
  }
  
}

void XOrsaAllObjectsListView::popup(QListViewItem *Item, const QPoint &point, int) {
  
  bool on_item;
  if (Item) {
    on_item = true;
  } else {
    on_item = false;
  }
  
  switch (universe->GetUniverseType()) {
  case Real: { 
    menu->SetHaveMassiveObjects(true);
    break;
  }
  case Simulated: {
    bool found_massive = false;
    vector<BodyWithEpoch>::iterator b_it = body.begin();
    while ((b_it != body.end()) && (found_massive == false)) {
      if ((*b_it).mass() > 0) {
	found_massive = true;
	break;
      }
      ++b_it;
    }
    menu->SetHaveMassiveObjects(found_massive);
    break;
  }
  }
  
  // menu->SetHaveMassiveObjects(found_massive);
  menu->SetOnItem(on_item);
  
  menu->popup(point);
  
  ////////////////////////////////////////////////////////////
  // NOTE: DON'T update here, otherwise the popup doesn't work.
  ////////////////////////////////////////////////////////////
  // update_content();
  
}

void XOrsaAllObjectsListView::slot_new_cartesian() {
  
  XOrsaNewObjectCartesianDialog *nod = new XOrsaNewObjectCartesianDialog(this);
  
  nod->show();
  nod->exec();
  
  if (nod->ok) {
    body.push_back(nod->GetBody());
    emit ObjectsChanged();
  }
  
}

void XOrsaAllObjectsListView::slot_new_keplerian() {
  
  XOrsaNewObjectKeplerianDialog *nod = new XOrsaNewObjectKeplerianDialog(body,this);
  
  nod->show();
  nod->exec();
  
  if (nod->ok) {
    body.push_back(nod->GetBody());
    emit ObjectsChanged();
  }
  
}

void XOrsaAllObjectsListView::slot_generate_cartesian() {
#ifdef HAVE_GSL
  XOrsaObjectsGeneratorCartesian gen(body,this);
  
  gen.show();
  gen.exec();
  
  emit ObjectsChanged();
#endif // HAVE_GSL
}

void XOrsaAllObjectsListView::slot_generate_keplerian() {
#ifdef HAVE_GSL
  XOrsaObjectsGeneratorKeplerian gen(body,this);
  
  gen.show();
  gen.exec();
  
  emit ObjectsChanged();
#endif // HAVE_GSL
}

void XOrsaAllObjectsListView::slot_edit_cartesian() {
  
  XOrsaAllObjectsItem * all_it = 0;
  QListViewItemIterator it = firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      all_it = dynamic_cast <XOrsaAllObjectsItem*> (it.current());
      if (all_it) {
	
	BodyWithEpoch tmp_body(*all_it->b_ptr);
	
	XOrsaNewObjectCartesianDialog * nod = new XOrsaNewObjectCartesianDialog(tmp_body,this);
	
	nod->show();
	nod->exec();
	
	if (nod->ok) {
	  
	  bool found = false;
	  vector<BodyWithEpoch>::iterator itr = body.begin();
	  while (itr != body.end()) {
	    if (&(*itr) == all_it->b_ptr) {
	      *itr = nod->GetBody();
	      // this is not needed, since all the XOrsaAllObjectsItem will be regenerated
	      // all_it->b_ptr = &(*itr);
	      found = true;
	      emit ObjectsChanged();
	      break;
	    }
	    ++itr;
	  }
	  //
	  if (!found) {
	    ORSA_ERROR("problems in XOrsaAllObjectsListView::slot_edit_cartesian()...");
	  }
	}
	
      }
    }
    ++it;
  }
}

void XOrsaAllObjectsListView::slot_edit_keplerian() {
  
  XOrsaAllObjectsItem * all_it = 0;
  QListViewItemIterator it = firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      all_it = dynamic_cast <XOrsaAllObjectsItem*> (it.current());
      if (all_it) {
	
	BodyWithEpoch tmp_body(*all_it->b_ptr);
	
	XOrsaNewObjectKeplerianDialog * nod = new XOrsaNewObjectKeplerianDialog(body,tmp_body,this);
	
	nod->show();
	nod->exec();
	
	if (nod->ok) {
	  
	  bool found = false;
	  vector<BodyWithEpoch>::iterator itr = body.begin();
	  while (itr != body.end()) {
	    if (&(*itr) == all_it->b_ptr) {
	      *itr = nod->GetBody();
	      // this is not needed, since all the XOrsaAllObjectsItem will be regenerated
	      // all_it->b_ptr = &(*itr);
	      found = true;
	      emit ObjectsChanged();
	      break;
	    }
	    ++itr;
	  }
	  //
	  if (!found) {
	    ORSA_ERROR("problems in XOrsaAllObjectsListView::slot_edit_keplerian()...");
	  }
	}
	
	/* 
	   XOrsaNewObjectKeplerianDialog * nod = new XOrsaNewObjectKeplerianDialog(body,(*all_it->b_ptr),this);
	   
	   nod->show();
	   nod->exec();
	   
	   if (nod->ok) {
	   (*all_it->b_ptr) = nod->GetBody();
	   emit ObjectsChanged();
	   }
	*/
      }
    }
    ++it;
  }
}

void XOrsaAllObjectsListView::slot_import_JPL() {
  
  XOrsaImportJPLObjectsWidgetDialog * imp = new XOrsaImportJPLObjectsWidgetDialog(body,this);
  
  imp->show();
  imp->exec();
  
  if (imp->ok) {
    emit ObjectsChanged();
  }
  
}

void XOrsaAllObjectsListView::slot_import_astorb() {
  
  XOrsaImportAstorbObjectsAdvancedDialog *imp = new XOrsaImportAstorbObjectsAdvancedDialog(body,this);
  imp->show();
  imp->exec();
  
  if (imp->ok) {
    emit ObjectsChanged();
  }
}

void XOrsaAllObjectsListView::slot_import_TLE() {
  
  XOrsaImportTLEObjectsDialog imp(body,this);
  imp.show();
  imp.exec();
  
  if (imp.ok) {
    emit ObjectsChanged();
  }
}

void XOrsaAllObjectsListView::slot_copy() {
  
  vector<BodyWithEpoch> bodies_to_be_copied;
  XOrsaAllObjectsItem *ii_it;
  QListViewItemIterator it = firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      ii_it = dynamic_cast <XOrsaAllObjectsItem*> (it.current());
      if (ii_it) {
	bodies_to_be_copied.push_back(*ii_it->b_ptr);
      }
    }
    ++it;
  }
  
  if (bodies_to_be_copied.size() > 0) {
    unsigned int j;
    for(j=0;j<bodies_to_be_copied.size();j++) {
      body.push_back(bodies_to_be_copied[j]);
    }
    emit ObjectsChanged();
  }
}

void XOrsaAllObjectsListView::slot_delete() {
  
  const int return_value =  QMessageBox::information(this,"delete","delete selected object(s)?",QMessageBox::Ok,QMessageBox::Cancel);
  
  if (return_value == QMessageBox::Cancel) {
    return;
  } else {
    
    // vector<QListViewItem*> items_to_be_removed;
    // vector<const orsa::BodyWithEpoch*> bodies_to_be_removed;
    // vector<const orsa::BodyWithEpoch*> body_id_to_be_removed;
    vector<unsigned int> body_id;
    QListViewItemIterator it = firstChild();
    while (it.current() != 0) {
      if (it.current()->isSelected()) {
	// items_to_be_removed.push_back(it.current());
	XOrsaAllObjectsItem * ii_it = dynamic_cast <XOrsaAllObjectsItem*> (it.current());
	if (ii_it) {
	  // bodies_to_be_removed.push_back(ii_it->b_ptr);
	  body_id.push_back(ii_it->b_id);
	}
      }
      ++it;
    }
    
    // if (items_to_be_removed.size() == 0) return;
    // if (bodies_to_be_removed.size() == 0) return;
    if (body_id.size() == 0) return;
    
    /* 
       vector<QListViewItem*>::iterator itt = items_to_be_removed.begin();
       while (itt != items_to_be_removed.end()) {
    */
    
    for (unsigned int k=0; k<body_id.size(); ++k) {
      const unsigned int b_id = body_id[k];
      bool found = false;
      vector<BodyWithEpoch>::iterator itr = body.begin();
      while (itr != body.end()) {
	if ((*itr).BodyId() == b_id) {
	  body.erase(itr);
	  found = true;
	  emit ObjectsChanged();
	  break;
	}
	++itr;
      }
      if (!found) {
	ORSA_ERROR("problems in XOrsaAllObjectsListView::slot_delete()...");
      }
    }
    
    // remove from the end to the beginning, otherwise some bugs can arise
    /* 
       vector<const orsa::BodyWithEpoch*>::iterator btt = bodies_to_be_removed.end();
       while (btt != bodies_to_be_removed.begin()) {
       --btt;
       
       bool found = false;
       vector<BodyWithEpoch>::iterator itr = body.begin();
       while (itr != body.end()) {
       if ((*itr).BodyId() == (*btt)->b_id) {
       body.erase(itr);
       found = true;
       emit ObjectsChanged();
       break;
       }
       ++itr;
       }
       if (!found) {
       ORSA_ERROR("problems in XOrsaAllObjectsListView::slot_delete()...");
       }
       }
    */
    
    /* 
       delete (*itt);
       }
    */
    
    // remove from the end to the beginning, otherwise some bugs can arise
    /* 
       vector<QListViewItem*>::iterator itt = items_to_be_removed.end();
       while (itt != items_to_be_removed.begin()) {
       --itt;
       ii_it = dynamic_cast <XOrsaAllObjectsItem*> (*itt);
       if (ii_it) {
       body.erase(ii_it->b_it);
       }
       delete (*itt);
       } 
    */
    
    emit ObjectsChanged();
  }
  
}

void XOrsaAllObjectsListView::slot_select_all() {
  QListViewItemIterator it = firstChild();
  while (it.current() != 0) {
    it.current()->setSelected(true);
    it.current()->repaint();
    ++it;
  }
}
