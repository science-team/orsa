/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifdef _WIN32
// BCC55 will bomb out if this is included later
#include <qgl.h>
#endif

#include "xorsa_integrations_info.h"

#include "xorsa_plot_tool_II.h"
#include "xorsa_opengl.h"
#include "xorsa_new_integration_dialog.h"
#include "xorsa_export_integration.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#ifdef HAVE_FFTW
#ifdef HAVE_GSL
#include "xorsa_analysis.h"
#endif // HAVE_GSL
#endif // HAVE_FFTW

#include <orsa_universe.h>

#include <qstring.h>
#include <qmessagebox.h>
#include <qlayout.h> 
#include <qcursor.h>

// icons
#include "ghost.xpm"
#include "ray.xpm"
#include "plot.xpm"
#include "red_blue.xpm"
#include "stop.xpm"

using namespace std;
using namespace orsa;

XOrsaIntegrationItem::XOrsaIntegrationItem(Evolution *e, QListView *parent, QString label1, QString label2, QString label3, QString label4, QString label5, QString label6, QString label7, QString label8) : QObject(parent), QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8), e_ptr(e) {
  //XOrsaIntegrationItem::XOrsaIntegrationItem(Evolution *e, XOrsaIntegrationsInfo *xoii, QString label1, QString label2, QString label3, QString label4, QString label5, QString label6, QString label7, QString label8) : QObject(xoii->listview), QListViewItem(xoii->listview, label1, label2, label3, label4, label5, label6, label7, label8), e_ptr(e) {
  timer_evolution_msec = 1000;
  connect(&timer_evolution,SIGNAL(timeout()),this,SLOT(print_item()));
  print_item();
  {
    const XOrsaEvolution * const oe = dynamic_cast <const XOrsaEvolution * const> (e_ptr);
    if (oe) {
      oe->add_event_receiver(integration_step_done_event_type,this);
      oe->add_event_receiver(integration_started_event_type,this);
      oe->add_event_receiver(integration_finished_event_type,this);
      oe->add_event_receiver(evolution_modified_event_type,this);
    } else {
      ORSA_ERROR("I don't know why...");
    }
  }
}

XOrsaIntegrationItem::~XOrsaIntegrationItem() {
  
  /* not needed anymore...
     {
     const XOrsaEvolution * const oe = dynamic_cast <const XOrsaEvolution * const> (e_ptr);
     if (oe) {
     oe->remove_event_receiver(integration_step_done_event_type,this);
     oe->remove_event_receiver(integration_started_event_type,this);
     oe->remove_event_receiver(integration_finished_event_type,this);
     oe->remove_event_receiver(evolution_modified_event_type,this);
     }
     }
  */
}

void XOrsaIntegrationItem::paintCell(QPainter *p, const QColorGroup &cg, int column, int width, int align) {
  if (e_ptr==0) return;
  if (e_ptr->integrating()) {
    QColorGroup local_cg(cg);
    if (isSelected()) {
      local_cg.setColor( QColorGroup::Highlight,darkYellow);
      local_cg.setColor( QColorGroup::HighlightedText,white);   
    } else {
      local_cg.setColor(QColorGroup::Base,yellow);
    }
    QListViewItem::paintCell(p,local_cg,column,width,align);
  } else {
    QListViewItem::paintCell(p,cg,column,width,align);
  }  
}

void XOrsaIntegrationItem::print_item() {
  
  if (e_ptr==0) return;
  
  QString s_obj, sitrt, sitt, s_t_start, s_t_stop, s_t_incr, s_sample_period, s_accuracy;
  
  switch (universe->GetUniverseType()) {
  case Real: { 
    s_obj.sprintf("%i (%i)",
		  e_ptr->start_bodies.size() + e_ptr->start_JPL_bodies.size(),
		  e_ptr->start_JPL_bodies.size());
    break;
  }
  case Simulated: {
    unsigned int j, nmass=0;
    for (j=0;j<e_ptr->start_bodies.size();++j) if (e_ptr->start_bodies[j].mass() != 0.0) ++nmass;
    s_obj.sprintf("%i (%i)",e_ptr->start_bodies.size(),nmass);
    break;
  }
  }
  
  // cerr << "-- TAG [3]" << endl;
  
  /* 
     const InteractionType itrt = e_ptr->GetInteraction()->GetType();
     sitrt = label(itrt).c_str();
  */
  //
  const InteractionType itrt = e_ptr->GetInteraction()->GetType();
  if (itrt == NEWTON) {
    const Newton * newton = dynamic_cast <const Newton *> (e_ptr->GetInteraction());
    if (newton) {
      sitrt = label(itrt).c_str();
      if (newton->IsIncludingMultipoleMoments()) {
	sitrt.append(" + Multipoles");
      }
      if (newton->IsIncludingRelativisticEffects()) {
	sitrt.append(" + Relativistic");
      }
      if (newton->IsIncludingFastRelativisticEffects()) {
	sitrt.append(" + Fast Relativistic");
      }
      if (newton->IsSkippingJPLPlanets()) {
    	sitrt.append(" + Forced JPL");
      }
    } else {
      sitrt = label(itrt).c_str();
    }
  } else {
    sitrt = label(itrt).c_str();
  }
  //
  // cerr << "-- TAG [4]" << endl;
  const IntegratorType itt = e_ptr->GetIntegrator()->GetType();
  sitt = label(itt).c_str();
  // cerr << "-- TAG [5]" << endl;
  FineDate(s_t_start,e_ptr[0][0]);
  FineDate(s_t_stop,e_ptr[0][e_ptr[0].size()-1]);
  //
  // time_unit sample_period_tu = AutoTimeUnit(e_ptr->sample_period);
  time_unit sample_period_tu = AutoTimeUnit(e_ptr->GetSamplePeriod().GetDouble());
  // s_sample_period.sprintf("%g %s",FromUnits(e_ptr->sample_period,sample_period_tu,-1),units->label(sample_period_tu).c_str());
  s_sample_period.sprintf("%g %s",FromUnits(e_ptr->GetSamplePeriod().GetDouble(),sample_period_tu,-1),units->label(sample_period_tu).c_str());
  // cerr << "-- TAG [6]" << endl;
  if (itt==BULIRSCHSTOER || itt==RA15) {
    s_t_incr.sprintf( "---");
    s_accuracy.sprintf( "%g",e_ptr->GetIntegrator()->accuracy);
  } else {
    s_t_incr.sprintf( "%g",e_ptr->GetIntegrator()->timestep.GetDouble());
    s_accuracy.sprintf("---");
  }
  // cerr << "-- TAG [7]" << endl;
  setText(0,e_ptr->name.c_str());
  setText(1,s_obj);
  setText(2,sitrt);
  setText(3,sitt);
  setText(4,s_t_incr);
  setText(5,s_accuracy);
  setText(6,s_t_start);
  setText(7,s_t_stop);
  setText(8,s_sample_period);
}

void XOrsaIntegrationItem::plot_tool() {
  XOrsaPlotTool_II * pt = new XOrsaPlotTool_II(e_ptr);
  connect(this,SIGNAL(closing_universe()),pt,SLOT(close()));
  pt->show();
}

void XOrsaIntegrationItem::opengl_tool() {
  XOrsaOpenGLEvolutionTool *oglt = new XOrsaOpenGLEvolutionTool();
  connect(this,SIGNAL(closing_universe()),oglt,SLOT(close()));
  QString cap;
  cap.sprintf("OpenGL viewer: %s",e_ptr->name.c_str());
  oglt->setCaption(cap);
  oglt->SetEvolution(e_ptr);
  oglt->show();
}

void XOrsaIntegrationItem::analyse_tool() {
#ifdef HAVE_FFTW
#ifdef HAVE_GSL
  XOrsaAnalysis *a = new XOrsaAnalysis(e_ptr);
  connect(this,SIGNAL(closing_universe()),a,SLOT(close()));
  a->show();
#endif // HAVE_GSL
#endif // HAVE_FFTW
}

void XOrsaIntegrationItem::export_tool() {
  XOrsaExportIntegration *ei = new XOrsaExportIntegration(e_ptr);
  ei->show();
  ei->exec();
  delete ei;
}

void XOrsaIntegrationItem::integration_copy() {
  
  XOrsaEvolution * evol = new XOrsaEvolution;
  
  evol->SetIntegrator(e_ptr->GetIntegrator());
  evol->SetInteraction(e_ptr->GetInteraction());
  evol->start_bodies     = e_ptr->start_bodies;
  evol->start_JPL_bodies = e_ptr->start_JPL_bodies;
  evol->SetSamplePeriod(e_ptr->GetSamplePeriod());
  evol->Evolution::name  = e_ptr->Evolution::name;
  
  XOrsaNewIntegrationDialog * id = new XOrsaNewIntegrationDialog(evol);
  
  id->show();
  id->exec();
  
  if (id->ok) {	  
    universe->push_back(evol);
    universe->modified = true;
    emit new_evolution();
  } else {
    delete evol;
  }
  
  delete id;
}

void XOrsaIntegrationItem::stop_integration() {
  e_ptr->stop_integration();
}

void XOrsaIntegrationItem::delete_evolution() {
  // before deleting e_ptr, remove the pointer from the universe
  // vector<Evolution*>::iterator it = universe->begin();
  Universe::iterator it = universe->begin();
  while (it != universe->end()) {
    if (*it == e_ptr) {
      universe->erase(it);
      break;
    }
    ++it;
  }
  
  delete e_ptr;
}

void XOrsaIntegrationItem::customEvent(QCustomEvent *e) {
  switch (e->type()) {
  case integration_step_done_event_type: 
    break;
  case integration_started_event_type:
    break;
  case integration_finished_event_type: 
    break;
  case evolution_modified_event_type: 
    if (!timer_evolution.isActive()) timer_evolution.start(timer_evolution_msec,true);
    break;
  case universe_modified_event_type:
    break;
  default:
    break;
  }
}

// 

XOrsaIntegrationsInfo::XOrsaIntegrationsInfo(QWidget *parent) : QWidget(parent) {
  
  QVBoxLayout *vlay = new QVBoxLayout(this,4);
  
  listview = new QListView(this);
  //
  listview->setAllColumnsShowFocus(true);
  listview->setShowSortIndicator(true);
  listview->setSelectionMode(QListView::Extended);
  listview->setItemMargin(3); // in pixels
  // listview->setResizeMode(QListView::LastColumn);
  //
  listview->addColumn("name");              listview->setColumnAlignment(0,Qt::AlignLeft);
  listview->addColumn("objects (massive)"); listview->setColumnAlignment(1,Qt::AlignRight);
  listview->addColumn("interaction");       listview->setColumnAlignment(2,Qt::AlignLeft);
  listview->addColumn("integrator");        listview->setColumnAlignment(3,Qt::AlignLeft);
  listview->addColumn("time increment");    listview->setColumnAlignment(4,Qt::AlignRight);
  listview->addColumn("accuracy");          listview->setColumnAlignment(5,Qt::AlignRight);
  listview->addColumn("time start");        listview->setColumnAlignment(6,Qt::AlignRight);
  listview->addColumn("time stop");         listview->setColumnAlignment(7,Qt::AlignRight);
  listview->addColumn("sample period");     listview->setColumnAlignment(8,Qt::AlignRight);
  //
  listview->setSorting(6,false);
  
  vlay->addWidget(listview);
  
  menu = new XOrsaIntegrationsPopupMenu(this,listview);
  
  connect(listview,SIGNAL(rightButtonPressed(QListViewItem *, const QPoint &, int)),this,SLOT(popup(QListViewItem *, const QPoint &, int)));
  connect(listview,SIGNAL(selectionChanged()),this,SIGNAL(selectionChanged()));
  
  XOrsaUniverse *ou = dynamic_cast <XOrsaUniverse*> (universe);
  if (ou) {
    ou->add_event_receiver(universe_modified_event_type,this);
  } else {
    ORSA_ERROR("I don't know why...");
  }
}

XOrsaIntegrationsInfo::~XOrsaIntegrationsInfo() {
  { 
    XOrsaUniverse *ou = dynamic_cast <XOrsaUniverse*> (universe);
    if (ou) {
      ou->remove_event_receiver(universe_modified_event_type,this);
    }
  }
}

bool XOrsaIntegrationsInfo::at_least_one_selected() const {
  bool ret_val=false;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      ret_val=true;
      break;
    }
    it++;
  }
  return ret_val;
}

bool XOrsaIntegrationsInfo::at_least_one_selected_is_integrating() const {
  bool ret_val=false;
  XOrsaIntegrationItem *ii_it;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      ii_it = dynamic_cast <XOrsaIntegrationItem*> (it.current());
      if (ii_it) {
	if (ii_it->integrating()) {
	  ret_val=true;
	  break;
	}
      }
    }
    it++;
  }
  return ret_val;
}

bool XOrsaIntegrationsInfo::at_least_one() const {
  bool ret_val=false;
  QListViewItemIterator it = listview->firstChild();
  if (it.current() != 0) {
    ret_val=true;
  }
  return ret_val;
}

/* 
   void XOrsaIntegrationsInfo::update_info() {
   if (universe==0) return;
   if (universe->size() == 0) return;
   
   XOrsaIntegrationItem *ii;
   QListViewItemIterator it = listview->firstChild();
   while (it.current() != 0) {
   ii = dynamic_cast <XOrsaIntegrationItem*> (it.current());
   if (ii) {
   print_item(ii);
   }
   it++;
   }
   }
*/

/* 
   void XOrsaIntegrationsInfo::update_info() {
   if (universe==0) return;
   if (universe->size() == 0) return;
   
   XOrsaIntegrationItem *ii;
   QListViewItemIterator it = listview->firstChild();
   while (it.current() != 0) {
   ii = dynamic_cast <XOrsaIntegrationItem*> (it.current());
   if (ii) {
   print_item(ii);
   }
   it++;
   }   
   }
*/

void XOrsaIntegrationsInfo::clear() {
  listview->clear();
}

void XOrsaIntegrationsInfo::universe_modified() {
  
  listview->clear();
  
  if (universe==0) return;
  if (universe->size() == 0) return;
  Universe::iterator itr = universe->begin();
  while ((itr != universe->end()) && (*itr != 0)) {
    XOrsaIntegrationItem *ii = new XOrsaIntegrationItem(*itr,listview);
    connect(this,SIGNAL(closing_universe()),ii,SIGNAL(closing_universe()));
    connect(ii,SIGNAL(new_evolution()),this,SLOT(universe_modified()));
    itr++;
  }
}

/* 
   void XOrsaIntegrationsInfo::print_item(XOrsaIntegrationItem *ii) {
   if (ii) {
   
   if ((*ii->e_it) == 0) return;
   
   Universe::const_iterator ce_it = ii->e_it;
   
   QString s_obj, sitrt, sitt, s_t_start, s_t_stop, s_t_incr, s_sample_period, s_accuracy;
   
   switch (universe->GetUniverseType()) {
   case Real: { 
   s_obj.sprintf("%i (%i)",
   (*ce_it)->start_bodies.size() + (*ce_it)->start_JPL_bodies.size(),
   (*ce_it)->start_JPL_bodies.size());
   break;
   }
   case Simulated: {
   unsigned int j, nmass=0;
   for (j=0;j<(*ce_it)->start_bodies.size();++j) if ((*ce_it)->start_bodies[j].mass() != 0.0) ++nmass;
   s_obj.sprintf("%i (%i)",(*ce_it)->start_bodies.size(),nmass);
   break;
   }
   }
   
   // cerr << "-- TAG [3]" << endl;
   
   const InteractionType itrt = (*ce_it)->interaction->type;
   sitrt = label(itrt).c_str();
   // cerr << "-- TAG [4]" << endl;
   const IntegratorType itt = (*ce_it)->integrator->GetType();
   sitt = label(itt).c_str();
   // cerr << "-- TAG [5]" << endl;
   FineDate(s_t_start,(*ce_it)[0][0]);
   FineDate(s_t_stop,(*ce_it)[0][(*ce_it)[0].size()-1]);
   //
   time_unit sample_period_tu = AutoTimeUnit((*ce_it)->sample_period);
   s_sample_period.sprintf("%g %s",FromUnits((*ce_it)->sample_period,sample_period_tu,-1),units->label(sample_period_tu).c_str());
   // cerr << "-- TAG [6]" << endl;
   if (itt==BULIRSCHSTOER || itt==RA15) {
   s_t_incr.sprintf( "---");
   s_accuracy.sprintf( "%g",(*ce_it)->integrator->accuracy);
   } else {
   s_t_incr.sprintf( "%g",(*ce_it)->integrator->timestep);
   s_accuracy.sprintf("---");
   }
   // cerr << "-- TAG [7]" << endl;
   ii->setText(0,(*ce_it)->name.c_str());
   ii->setText(1,s_obj);
   ii->setText(2,sitrt);
   ii->setText(3,sitt);
   ii->setText(4,s_t_incr);
   ii->setText(5,s_accuracy);
   ii->setText(6,s_t_start);
   ii->setText(7,s_t_stop);
   ii->setText(8,s_sample_period);
   }
   }
*/

// void XOrsaIntegrationsInfo::popup(QListViewItem *Item, const QPoint &point, int) {
void XOrsaIntegrationsInfo::popup(QListViewItem*, const QPoint &point, int) {
  
  // if (Item) menu->popup(point);
  menu->popup(point);
  
  ////////////////////////////////////////////////////////////
  // NOTE: DON'T update here, otherwise the popup doesn't work.
  ////////////////////////////////////////////////////////////
  // update_info();
  
}

/* 
   void XOrsaIntegrationsInfo::slot_plot() {
   XOrsaIntegrationItem *ii_it;
   QListViewItemIterator it = listview->firstChild();
   while (it.current() != 0) {
   if (it.current()->isSelected()) {
   ii_it = dynamic_cast <XOrsaIntegrationItem*> (it.current());
   if (ii_it) {
   XOrsaPlotTool_II *pt = new XOrsaPlotTool_II(*ii_it->e_it);
   connect(this,SIGNAL(closing_universe()),pt,SLOT(close()));
   pt->show();
   }
   }
   it++;
   }
   }
*/

void XOrsaIntegrationsInfo::slot_plot() {
  XOrsaIntegrationItem *ii_it;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      ii_it = dynamic_cast <XOrsaIntegrationItem*> (it.current());
      if (ii_it) {
	ii_it->plot_tool();
      }
    }
    it++;
  }
}

void XOrsaIntegrationsInfo::slot_new() {
  emit new_integration();
}

void XOrsaIntegrationsInfo::slot_opengl() {
  XOrsaIntegrationItem *ii_it;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      ii_it = dynamic_cast <XOrsaIntegrationItem*> (it.current());
      if (ii_it) {
	ii_it->opengl_tool();
      }
    }
    it++;
  }
}


void XOrsaIntegrationsInfo::slot_analyse() {
#ifdef HAVE_FFTW
#ifdef HAVE_GSL
  XOrsaIntegrationItem *ii_it;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      ii_it = dynamic_cast <XOrsaIntegrationItem*> (it.current());
      if (ii_it) {
	ii_it->analyse_tool();
      }
    }
    it++;
  }
#endif // HAVE_GSL
#endif // HAVE_FFTW
}


void XOrsaIntegrationsInfo::slot_delete() {
  
  int return_value =  QMessageBox::information(this,"Delete","Delete selected integration(s)?",QMessageBox::Ok,QMessageBox::Cancel);
  
  if (return_value == QMessageBox::Cancel) {
    return;
  } else {
    
    vector<QListViewItem*> items_to_be_removed;
    XOrsaIntegrationItem *ii_it;
    QListViewItemIterator it = listview->firstChild();
    while (it.current() != 0) {
      if (it.current()->isSelected()) {
	items_to_be_removed.push_back(it.current());
      }
      it++;
    }
    
    // remove from the end to the beginning, otherwise some bugs arise
    vector<QListViewItem*>::iterator itt = items_to_be_removed.end();
    while (itt != items_to_be_removed.begin()) {
      itt--;
      ii_it = dynamic_cast <XOrsaIntegrationItem*> (*itt);
      if (ii_it) {
	ii_it->delete_evolution();
      }
      delete (*itt);
    } 
    
    // update_info();
  }
}

void XOrsaIntegrationsInfo::slot_select_all() {
  listview->selectAll(true);
}

void XOrsaIntegrationsInfo::slot_integration_copy() {
  XOrsaIntegrationItem *ii_it;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      ii_it = dynamic_cast <XOrsaIntegrationItem*> (it.current());
      if (ii_it) {
	ii_it->integration_copy();
	universe_modified();
      }
    }
    it++;
  }
}

void XOrsaIntegrationsInfo::slot_stop_integration() {
  XOrsaIntegrationItem *ii_it;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      ii_it = dynamic_cast <XOrsaIntegrationItem*> (it.current());
      if (ii_it) {
	ii_it->stop_integration();
	emit stopped_integration();
      }
    }
    it++;
  }
}

void XOrsaIntegrationsInfo::slot_export() {
  XOrsaIntegrationItem *ii_it;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    if (it.current()->isSelected()) {
      ii_it = dynamic_cast <XOrsaIntegrationItem*> (it.current());
      if (ii_it) {
	ii_it->export_tool();
      }
    }
    it++;
  }
}

void XOrsaIntegrationsInfo::customEvent(QCustomEvent *e) {
  switch (e->type()) {
  case integration_step_done_event_type: 
    break;
  case integration_started_event_type:
    break;
  case integration_finished_event_type: 
    break;
  case evolution_modified_event_type: 
    break;
  case universe_modified_event_type:
    universe_modified();
    break;
  default:
    break;
  }
}

// XOrsaIntegrationsPopupMenu

XOrsaIntegrationsPopupMenu::XOrsaIntegrationsPopupMenu(XOrsaIntegrationsInfo *evolution_info_in, QWidget *parent) : QPopupMenu(parent) {
  
  connect(this,SIGNAL(aboutToShow()),this,SLOT(widgets_enabler()));
  
  evolution_info = evolution_info_in;
  
  id_new = insertItem(QIconSet(ray_xpm),"New",evolution_info,SLOT(slot_new()));
  
  id_stop_integration = insertItem(QIconSet(stop_xpm),"Stop",evolution_info,SLOT(slot_stop_integration()));
  
  id_3D = insertItem(QIconSet(red_blue_xpm),"3D Viewer",evolution_info,SLOT(slot_opengl()));
  if (!(QGLFormat::hasOpenGL())) {
    setItemEnabled(id_3D,false);
  }
  
  id_2D = insertItem(QIconSet(plot_xpm),"2D Plots",evolution_info,SLOT(slot_plot()));
  
  id_analyse = insertItem("Analyse",evolution_info,SLOT(slot_analyse()));
#ifndef HAVE_FFTW
  setItemEnabled(id_analyse,false);
#endif // HAVE_FFTW
#ifndef HAVE_GSL
  setItemEnabled(id_analyse,false);
#endif // HAVE_GSL
  
  insertSeparator();
  
  id_integration_copy = insertItem(QIconSet(ghost_xpm),"New integration with same objects",evolution_info,SLOT(slot_integration_copy()));
  
  insertSeparator();
  
  id_export = insertItem("Export",evolution_info,SLOT(slot_export()));
  
  insertSeparator();
  
  id_select_all = insertItem("Select All",evolution_info,SLOT(slot_select_all()));
  
  id_delete = insertItem("Delete",evolution_info,SLOT(slot_delete()));
}

void XOrsaIntegrationsPopupMenu::widgets_enabler() {
  bool var_bool=false;
  if (evolution_info->at_least_one_selected()) {
    var_bool=true;
  }
  
  setItemEnabled(id_stop_integration,evolution_info->at_least_one_selected_is_integrating());
    
  if (QGLFormat::hasOpenGL()) {
    setItemEnabled(id_3D,var_bool);
  }
  setItemEnabled(id_2D,var_bool);
  
#ifdef HAVE_FFTW
#ifdef HAVE_GSL
  setItemEnabled(id_analyse,var_bool);
#endif // HAVE_GSL
#endif // HAVE_FFTW
  
  setItemEnabled(id_integration_copy,var_bool);
  setItemEnabled(id_export,var_bool);
  setItemEnabled(id_select_all,(var_bool && evolution_info->at_least_one()));
  setItemEnabled(id_delete,var_bool);
}
