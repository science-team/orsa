/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_date.h"

#include <qlineedit.h> 
#include <qspinbox.h> 
#include <qlayout.h>
#include <qlabel.h> 
#include <qvalidator.h> 
#include <qhbox.h> 

#include <time.h>

#ifdef _WIN32
#include "support.h"
#endif

using namespace std;
using namespace orsa;

XOrsaDate::XOrsaDate(QWidget *parent) : QWidget(parent) {
  
  date.SetGregor(2000,1,1);
  
  init_draw();
  
  internal_change = true;
  update_JD_content();
  update_MJD_content();
  update_Date_content();  
  internal_change = false;
}


XOrsaDate::XOrsaDate(const Date &d, QWidget *parent) : QWidget(parent,0,Qt::WType_TopLevel) {
  
  date = d;
  
  init_draw();
  
  internal_change = true;
  update_JD_content();
  update_MJD_content();
  update_Date_content();
  internal_change = false;
}


void XOrsaDate::SetDate(const Date &d) {
  
  date = d;
  
  internal_change = true;
  update_JD_content();
  update_MJD_content();
  update_Date_content();
  internal_change = false;
}


void XOrsaDate::SetDate(const Date &d, const TimeScale ts) {
  
  date = d;
  ts_combo->SetTimeScale(ts);
  
  internal_change = true;
  update_JD_content();
  update_MJD_content();
  update_Date_content();
  internal_change = false;
}

void XOrsaDate::SetNow() {
  Date d;
  d.SetNow();
  SetDate(d);
}

void XOrsaDate::SetToday() {
  Date d;
  d.SetToday();
  SetDate(d);
}

Date XOrsaDate::GetDate() {
  return (date);  
}

void XOrsaDate::init_draw() {
  
  // setColumns(2);
  QGridLayout *grid_lay = new QGridLayout(this,6,4,3,3);
  // grid_lay->setAutoAdd(true); 
  
  // JD
  // QLabel    *label_JD = new QLabel(this);
  // label_JD->setText("JD");
  grid_lay->addWidget(new QLabel("JD",this),0,0);
  //
  line_JD = new QLineEdit(this);
  line_JD->setAlignment(Qt::AlignRight); 
  connect(line_JD,SIGNAL(textChanged(const QString &)),this,SLOT(update_from_JD()));
  grid_lay->addMultiCellWidget(line_JD,0,0,1,3);
  
  // MJD
  // QLabel    *label_MJD = new QLabel(this);
  // label_MJD->setText("MJD");
  grid_lay->addWidget(new QLabel("MJD",this),1,0);
  //
  line_MJD = new QLineEdit(this);
  line_MJD->setAlignment(Qt::AlignRight); 
  connect(line_MJD,SIGNAL(textChanged(const QString &)),this,SLOT(update_from_MJD()));
  grid_lay->addMultiCellWidget(line_MJD,1,1,1,3);
  
  // Date
  // QLabel    *label_DATE = new QLabel(this);
  // label_DATE->setText("date (Y/M/D)");
  grid_lay->addWidget(new QLabel("date (Y/M/D)",this),2,0);
  //
  // QWidget *w_date = new QWidget(this);
  // QHBoxLayout *lay_date = new QHBoxLayout(w_date);
  //
  spin_y = new QSpinBox(this);
  spin_y->setMinValue(-4713);
  spin_y->setMaxValue(100000);
  // lay_date->addWidget(spin_y);
  connect(spin_y,SIGNAL(valueChanged(int)),this,SLOT(update_from_date()));
  grid_lay->addWidget(spin_y,2,1);
  
  //
  spin_m = new QSpinBox(this);
  spin_m->setMinValue(1);
  spin_m->setMaxValue(12);
  // lay_date->addWidget(spin_m);
  connect(spin_m,SIGNAL(valueChanged(int)),this,SLOT(update_from_date()));
  grid_lay->addWidget(spin_m,2,2);
  
  //
  spin_d = new QSpinBox(this);
  spin_d->setMinValue(1);
  spin_d->setMaxValue(31);
  // lay_date->addWidget(spin_d);
  connect(spin_d,SIGNAL(valueChanged(int)),this,SLOT(update_from_date()));
  grid_lay->addWidget(spin_d,2,3);
  
  // time
  // QLabel    *label_time = new QLabel(this);
  // label_time->setText("time (H/M/S)");
  grid_lay->addWidget(new QLabel("time (H/M/S)",this),3,0);
  //
  // QWidget *w_UTC = new QWidget(this);
  // QHBoxLayout *lay_UTC = new QHBoxLayout(w_UTC);
  //
  spin_H = new QSpinBox(this);
  spin_H->setMaxValue(23);
  // lay_UTC->addWidget(spin_H);
  connect(spin_H,SIGNAL(valueChanged(int)),this,SLOT(update_from_date()));
  grid_lay->addWidget(spin_H,3,1);
  //
  spin_M = new QSpinBox(this);
  spin_M->setMaxValue(59);
  // lay_UTC->addWidget(spin_M);
  connect(spin_M,SIGNAL(valueChanged(int)),this,SLOT(update_from_date()));
  grid_lay->addWidget(spin_M,3,2);
  //
  // line_S = new QLineEdit(w_UTC);
  spin_S = new QSpinBox(this);
  spin_S->setMaxValue(59);
  // lay_UTC->addWidget(spin_S);
  // connect(line_S,SIGNAL(textChanged(const QString &)),this,SLOT(update_from_date()));
  connect(spin_S,SIGNAL(valueChanged(int)),this,SLOT(update_from_date()));
  grid_lay->addWidget(spin_S,3,3);
  
  // timescale
  // QLabel    *label_timescale = new QLabel(this);
  // label_timescale->setText("timescale");
  grid_lay->addWidget(new QLabel("timescale",this),4,0);
  //
  // QWidget       *w_ts = new QWidget(this);
  // QHBoxLayout *lay_ts = new QHBoxLayout(w_ts);
  //
  ts_combo = new TimeScaleCombo(this);
  ts_combo->SetTimeScale(universe->GetTimeScale());
  connect(ts_combo,SIGNAL(activated(int)),this,SLOT(TimeScaleChanged(int)));
  // lay_ts->addWidget(ts_combo);
  grid_lay->addMultiCellWidget(ts_combo,4,4,1,3);
  
  // now push button
  /* QPushButton *now_pb = new QPushButton("now",this);
     connect(now_pb,SIGNAL(clicked()),this,SLOT(SetNow()));
     grid_lay->addMultiCellWidget(now_pb,5,5,1,3);
  */
  
  // now and today pb's
  QHBox * buttons_hb = new QHBox(this);
  buttons_hb->setSpacing(3);
  //
  QPushButton * now_pb = new QPushButton("now",buttons_hb);
  connect(now_pb,SIGNAL(clicked()),this,SLOT(SetNow()));
  //
  QPushButton * today_pb = new QPushButton("today",buttons_hb);
  connect(today_pb,SIGNAL(clicked()),this,SLOT(SetToday()));
  //
  grid_lay->addMultiCellWidget(buttons_hb,5,5,1,3);
  
  // validation
  QDoubleValidator * valid = new QDoubleValidator(this);
  //
  line_JD->setValidator(valid);
  line_MJD->setValidator(valid);
  // line_S->setValidator(valid);
}


void XOrsaDate::update_from_JD() {
  if (!internal_change) {
    internal_change = true;
    date.SetJulian(line_JD->text().toDouble(),ts_combo->GetTimeScale());
    update_MJD_content();
    update_Date_content();
    internal_change = false;
  }
}


void XOrsaDate::update_from_MJD() {
  if (!internal_change) {
    internal_change = true;
    date.SetJulian(line_MJD->text().toDouble()+2400000.5,ts_combo->GetTimeScale());
    update_JD_content();
    update_Date_content();
    internal_change = false;
  }
}


void XOrsaDate::update_from_date() {
  if (!internal_change) {
    internal_change = true;
    int y,m,d;
    y = spin_y->value();
    m = spin_m->value();
    d = spin_d->value();
    int H,M,S;
    // double S;
    H = spin_H->value();
    M = spin_M->value();
    S = spin_S->value();
    
    // double frac_day = H/24.0 + M/(24.0*60.0) + S/(24.0*60.0*60.0);
    // date.SetGregor(y,m,d+frac_day,ts_combo->GetTimeScale());
    //
    date.SetGregor(y,m,d,H,M,S,ts_combo->GetTimeScale());
    
    update_JD_content();
    update_MJD_content();
    
    internal_change = false;
  }
}


void XOrsaDate::update_JD_content() {
  QString s;
  double jd = date.GetJulian(ts_combo->GetTimeScale());
  // s.sprintf("%.13g",jd);
  // s.sprintf("%.12g",jd);
  s.sprintf("%.5f",jd);
  line_JD->setText(s);
}


void XOrsaDate::update_MJD_content() {
  QString s;
  double jd = date.GetJulian(ts_combo->GetTimeScale());
  // s.sprintf("%.11g",jd-2400000.5);
  // s.sprintf("%.10g",jd-2400000.5);
  s.sprintf("%.5f",jd-2400000.5);
  line_MJD->setText(s);
}

void XOrsaDate::TimeScaleChanged(int) {
  internal_change = true;
  update_JD_content();
  update_MJD_content();
  update_Date_content();
  internal_change = false;
}

void XOrsaDate::update_Date_content() { 
  int y,m,d;
  date.GetGregor(y,m,d,ts_combo->GetTimeScale());
  spin_y->setValue(y);
  spin_m->setValue(m);
  spin_d->setValue(d); 
  
  double frac = date.GetDayFraction(ts_combo->GetTimeScale());
  
  int H,M,S;
  frac *= 24;
  
  H = (int)floor(frac);
  frac -= H;
  
  frac *= 60;
  
  M = (int)floor(frac);
  frac -= M;
  
  frac *= 60;
  
  // S = (int)floor(frac);
  // S = (int)rint(frac);
  S = (int)floor(frac);
  
  spin_H->setValue(H);
  spin_M->setValue(M);
  spin_S->setValue(S);
  
}

// XOrsaDateDialog

XOrsaDateDialog::XOrsaDateDialog(UniverseTypeAwareTime &t, QWidget *parent) : QDialog(parent,0,true), time(t) {
  
  QVBoxLayout *vlay = new QVBoxLayout(this,3);
  
  QVBox *vbox = new QVBox(this);
  
  vlay->addWidget(vbox);
  
  od = new XOrsaDate(vbox);
  od->SetDate(time.GetDate());
  
  QHBox *bottom_buttons_gb = new QHBox(vbox);
  
  ok_pb = new QPushButton("OK",bottom_buttons_gb);
  connect(ok_pb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  
  cancel_pb = new QPushButton("Cancel",bottom_buttons_gb);
  connect(cancel_pb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
}

void XOrsaDateDialog::ok_pressed() {
  time = od->GetDate();
  done(0);
}	

void XOrsaDateDialog::cancel_pressed() {
  done(0);
}

// XOrsaDatePushButton

XOrsaDatePushButton::XOrsaDatePushButton(QWidget *parent) : QPushButton(parent) {
  switch (universe->GetUniverseType()) {
  case Real: {
    Date d;
    // d.SetGregor(2000,1,1,orsa::UTC);
    d.SetGregor(2000,1,1); // the timescale is the default one
    SetDate(d);
    break;
  }
  case Simulated: 
    SetTime(0.0);
    break;
  }
  update_label();
  connect(this,SIGNAL(clicked()),this,SLOT(slot_change_time()));
}

XOrsaDatePushButton::XOrsaDatePushButton(UniverseTypeAwareTime &t, QWidget *parent) : QPushButton(parent), UniverseTypeAwareTime(t) {
  update_label();
  connect(this,SIGNAL(clicked()),this,SLOT(slot_change_time()));
}

void XOrsaDatePushButton::slot_change_time() {
  XOrsaDateDialog odd(*this);
  odd.show();
  odd.exec();
  update_label();
}

void XOrsaDatePushButton::update_label() {
  QString label;
  FineDate(label,*this);
  label.append(" ");
  label.append(TimeScaleLabel(universe->GetTimeScale()).c_str());
  setText(label);
  emit DateChanged();
}
