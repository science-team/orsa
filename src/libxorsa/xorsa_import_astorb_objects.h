/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_IMPORT_ASTORB_OBJECTS_H_
#define _XORSA_IMPORT_ASTORB_OBJECTS_H_

#include <qdialog.h>
#include <qlistview.h>
#include <qlabel.h>
#include <qpopupmenu.h> 
#include <qlayout.h>
#include <qprogressbar.h>
#include <qmutex.h>
#include <qapplication.h>
#include <qthread.h>
#include <qobject.h>
#include <qhbox.h> 

#include <orsa_body.h>
#include <orsa_file.h>

#include "xorsa_config.h"

#include <iostream>

#include <vector>

#include <qcombobox.h> 

class QLineEdit;
class QRadioButton;
class QComboBox;
class QDoubleValidator;
class QIntValidator;
class QVGroupBox;

class ReadAstorbFileThread;

class XOrsaAsteroidFileTypeCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  XOrsaAsteroidFileTypeCombo(QWidget *parent=0);
  
 private slots:
  void SetFileType(int);
  
 public slots:
  void SetFileType(orsa::ConfigEnum);
  
 public:  
  orsa::ConfigEnum GetFileType();
  
 private:
  orsa::ConfigEnum ft;
};

/* 
   class XOrsaAstorbFile : public QObject, public AstorbFile {
   
   Q_OBJECT
   
   public:
   XOrsaAstorbFile() : QObject(), AstorbFile() {
   old_progress     = 0;
   local_bool_stop  = false;
   local_bool_pause = false;
   };
   
   public:
   void read_progress(int n, bool &bool_pause, bool &bool_stop) {
   
   bool_pause = local_bool_pause;
   bool_stop  = local_bool_stop;
   
   if (n-old_progress >= 1000) {
   old_progress=n;
   if (mutex.tryLock()) {
   emit progress(n);
   mutex.unlock();
   }
   }
   }
   
   public slots:
   inline void pause_read() { 
   local_bool_pause = true;  
   };
   
   inline void continue_read() { 
   local_bool_pause = false; 
   };
   
   public:
   void read_finished() {
   emit finished();
   };
   
   public slots:
   void stop_read() { local_bool_stop = true; };
   
   signals:
   void progress(int);
   void finished();
   
   private:
   QMutex mutex;
   int old_progress;
   bool local_bool_stop, local_bool_pause;
   };
*/

class XOrsaAsteroidDatabaseFile : public QObject, public orsa::AsteroidDatabaseFile {
  // class XOrsaAsteroidDatabaseFile : public QObject {
  // class XOrsaAsteroidDatabaseFile : public QObject, public ReadFile {
  
  Q_OBJECT
  
 public:
  XOrsaAsteroidDatabaseFile() : QObject(), orsa::AsteroidDatabaseFile() {
    // XOrsaAsteroidDatabaseFile() : QObject() {
    // XOrsaAsteroidDatabaseFile() : QObject(), ReadFile() {
    // old_progress     = 0;
    local_bool_stop  = false;
    local_bool_pause = false;
  };
  
 public:
  void read_progress(int n, bool &bool_pause, bool &bool_stop) {
    
    bool_pause = local_bool_pause;
    bool_stop  = local_bool_stop;
    
    // if (n-old_progress >= 1000) {
    // old_progress=n;
    if (!(n%1000)) {
      if (mutex.tryLock()) {
	emit progress(n);
	mutex.unlock();
      }
    }
  }
  
  public slots:
  inline void pause_read() { 
    local_bool_pause = true;  
  };
  
  inline void continue_read() { 
    local_bool_pause = false; 
  };
  
 public:
  void read_finished() {
    emit finished();
  };
  
  /* 
     virtual void Read() {
     cerr << "fake XOrsaAsteroidDatabaseFile::Read() called..." << endl;
     }
  */
  
 public slots:
  void stop_read() { local_bool_stop = true; };
  
 signals:
  void progress(int);
  void finished();
  
 private:
  QMutex mutex;
  // int old_progress;
  bool local_bool_stop, local_bool_pause;
};

////

class XOrsaAsteroidDatabaseFile_AstorbFile : public orsa::AstorbFile, public XOrsaAsteroidDatabaseFile {
  
 public:
  orsa::AsteroidDatabase *db;
  
 public:
  XOrsaAsteroidDatabaseFile_AstorbFile() : orsa::AstorbFile(), XOrsaAsteroidDatabaseFile() {
    db = orsa::AstorbFile::db;
    XOrsaAsteroidDatabaseFile::db = db;
  }
  
  void Read() {
    orsa::AstorbFile::Read();
  }
  
  void SetFileName (std::string name_in) {
    orsa::AstorbFile::SetFileName(name_in);
  }
  
  void SetFileName (char* name_in) {
    orsa::AstorbFile::SetFileName(name_in);
  }
  
  std::string GetFileName () const {
    return orsa::AstorbFile::GetFileName();
  }
  
  void read_progress(int n, bool &bool_pause, bool &bool_stop) {
    XOrsaAsteroidDatabaseFile::read_progress(n,bool_pause,bool_stop);
  }
  
  void read_finished() {
    XOrsaAsteroidDatabaseFile::read_finished();
  }
  
};

class XOrsaAsteroidDatabaseFile_MPCOrbFile : public orsa::MPCOrbFile, public XOrsaAsteroidDatabaseFile {
  
 public:
  orsa::AsteroidDatabase *db;
  
 public:
  XOrsaAsteroidDatabaseFile_MPCOrbFile() : orsa::MPCOrbFile(), XOrsaAsteroidDatabaseFile() {
    db = orsa::MPCOrbFile::db;
    XOrsaAsteroidDatabaseFile::db = db;
  }
  
  void Read() {
    orsa::MPCOrbFile::Read();
  }
  
  void SetFileName (std::string name_in) {
    orsa::MPCOrbFile::SetFileName(name_in);
  }
  
  void SetFileName (char* name_in) {
    orsa::MPCOrbFile::SetFileName(name_in);
  }
  
  std::string GetFileName () const {
    return orsa::MPCOrbFile::GetFileName();
  }
  
  void read_progress(int n, bool &bool_pause, bool &bool_stop) {
    XOrsaAsteroidDatabaseFile::read_progress(n,bool_pause,bool_stop);
  }
  
  void read_finished() {
    XOrsaAsteroidDatabaseFile::read_finished();
  }
  
}; 

class XOrsaAsteroidDatabaseFile_MPCCometFile : public orsa::MPCCometFile, public XOrsaAsteroidDatabaseFile {
  
 public:
  orsa::AsteroidDatabase *db;
  
 public:
  XOrsaAsteroidDatabaseFile_MPCCometFile() : orsa::MPCCometFile(), XOrsaAsteroidDatabaseFile() {
    db = orsa::MPCCometFile::db;
    XOrsaAsteroidDatabaseFile::db = db;
  }
  
  void Read() {
    orsa::MPCCometFile::Read();
  }
  
  void SetFileName (std::string name_in) {
    orsa::MPCCometFile::SetFileName(name_in);
  }
  
  void SetFileName (char* name_in) {
    orsa::MPCCometFile::SetFileName(name_in);
  }
  
  std::string GetFileName () const {
    return orsa::MPCCometFile::GetFileName();
  }
  
  void read_progress(int n, bool &bool_pause, bool &bool_stop) {
    XOrsaAsteroidDatabaseFile::read_progress(n,bool_pause,bool_stop);
  }
  
  void read_finished() {
    XOrsaAsteroidDatabaseFile::read_finished();
  }
  
};

class XOrsaAsteroidDatabaseFile_NEODYSCAT : public orsa::NEODYSCAT, public XOrsaAsteroidDatabaseFile {
  
 public:
  orsa::AsteroidDatabase *db;
  
 public:
  XOrsaAsteroidDatabaseFile_NEODYSCAT() : orsa::NEODYSCAT(), XOrsaAsteroidDatabaseFile() {
    db = orsa::NEODYSCAT::db;
    XOrsaAsteroidDatabaseFile::db = db;
  }
  
  void Read() {
    orsa::NEODYSCAT::Read();
  }
  
  void SetFileName (std::string name_in) {
    orsa::NEODYSCAT::SetFileName(name_in);
  }
  
  void SetFileName (char* name_in) {
    orsa::NEODYSCAT::SetFileName(name_in);
  }
  
  std::string GetFileName () const {
    return orsa::NEODYSCAT::GetFileName();
  }
  
  void read_progress(int n, bool &bool_pause, bool &bool_stop) {
    XOrsaAsteroidDatabaseFile::read_progress(n,bool_pause,bool_stop);
  }
  
  void read_finished() {
    XOrsaAsteroidDatabaseFile::read_finished();
  }
  
};

class XOrsaAsteroidDatabaseFile_JPLDastcomNumFile : public orsa::JPLDastcomNumFile, public XOrsaAsteroidDatabaseFile {
  
 public:
  orsa::AsteroidDatabase *db;
  
 public:
  XOrsaAsteroidDatabaseFile_JPLDastcomNumFile() : orsa::JPLDastcomNumFile(), XOrsaAsteroidDatabaseFile() {
    db = orsa::JPLDastcomNumFile::db;
    XOrsaAsteroidDatabaseFile::db = db;
  }
  
  void Read() {
    orsa::JPLDastcomNumFile::Read();
  }
  
  void SetFileName (std::string name_in) {
    orsa::JPLDastcomNumFile::SetFileName(name_in);
  }
  
  void SetFileName (char* name_in) {
    orsa::JPLDastcomNumFile::SetFileName(name_in);
  }
  
  std::string GetFileName () const {
    return orsa::JPLDastcomNumFile::GetFileName();
  }
  
  void read_progress(int n, bool &bool_pause, bool &bool_stop) {
    XOrsaAsteroidDatabaseFile::read_progress(n,bool_pause,bool_stop);
  }
  
  void read_finished() {
    XOrsaAsteroidDatabaseFile::read_finished();
  }
  
};

class XOrsaAsteroidDatabaseFile_JPLDastcomUnnumFile : public orsa::JPLDastcomUnnumFile, public XOrsaAsteroidDatabaseFile {
  
 public:
  orsa::AsteroidDatabase *db;
  
 public:
  XOrsaAsteroidDatabaseFile_JPLDastcomUnnumFile() : orsa::JPLDastcomUnnumFile(), XOrsaAsteroidDatabaseFile() {
    db = orsa::JPLDastcomUnnumFile::db;
    XOrsaAsteroidDatabaseFile::db = db;
  }
  
  void Read() {
    orsa::JPLDastcomUnnumFile::Read();
  }
  
  void SetFileName (std::string name_in) {
    orsa::JPLDastcomUnnumFile::SetFileName(name_in);
  }
  
  void SetFileName (char* name_in) {
    orsa::JPLDastcomUnnumFile::SetFileName(name_in);
  }
  
  std::string GetFileName () const {
    return orsa::JPLDastcomUnnumFile::GetFileName();
  }
  
  void read_progress(int n, bool &bool_pause, bool &bool_stop) {
    XOrsaAsteroidDatabaseFile::read_progress(n,bool_pause,bool_stop);
  }
  
  void read_finished() {
    XOrsaAsteroidDatabaseFile::read_finished();
  }
  
};

class XOrsaAsteroidDatabaseFile_JPLDastcomCometFile : public orsa::JPLDastcomCometFile, public XOrsaAsteroidDatabaseFile {
  
 public:
  orsa::AsteroidDatabase *db;
  
 public:
  XOrsaAsteroidDatabaseFile_JPLDastcomCometFile() : orsa::JPLDastcomCometFile(), XOrsaAsteroidDatabaseFile() {
    db = orsa::JPLDastcomCometFile::db;
    XOrsaAsteroidDatabaseFile::db = db;
  }
  
  void Read() {
    orsa::JPLDastcomCometFile::Read();
  }
  
  void SetFileName (std::string name_in) {
    orsa::JPLDastcomCometFile::SetFileName(name_in);
  }
  
  void SetFileName (char* name_in) {
    orsa::JPLDastcomCometFile::SetFileName(name_in);
  }
  
  std::string GetFileName () const {
    return orsa::JPLDastcomCometFile::GetFileName();
  }
  
  void read_progress(int n, bool &bool_pause, bool &bool_stop) {
    XOrsaAsteroidDatabaseFile::read_progress(n,bool_pause,bool_stop);
  }
  
  void read_finished() {
    XOrsaAsteroidDatabaseFile::read_finished();
  }
  
};

class XOrsaAsteroidDatabaseFile_AstDySMatrixFile : public orsa::AstDySMatrixFile, public XOrsaAsteroidDatabaseFile {
  
 public:
  orsa::AsteroidDatabase *db;
  
 public:
  XOrsaAsteroidDatabaseFile_AstDySMatrixFile() : orsa::AstDySMatrixFile(), XOrsaAsteroidDatabaseFile() {
    db = orsa::AstDySMatrixFile::db;
    XOrsaAsteroidDatabaseFile::db = db;
  }
  
  void Read() {
    orsa::AstDySMatrixFile::Read();
  }
  
  void SetFileName (std::string name_in) {
    orsa::AstDySMatrixFile::SetFileName(name_in);
  }
  
  void SetFileName (char* name_in) {
    orsa::AstDySMatrixFile::SetFileName(name_in);
  }
  
  std::string GetFileName () const {
    return orsa::AstDySMatrixFile::GetFileName();
  }
  
  void read_progress(int n, bool &bool_pause, bool &bool_stop) {
    XOrsaAsteroidDatabaseFile::read_progress(n,bool_pause,bool_stop);
  }
  
  void read_finished() {
    XOrsaAsteroidDatabaseFile::read_finished();
  }
  
};

/*  
   // class XOrsaAsteroidDatabaseFile_MPCOrbFile : public XOrsaAsteroidDatabaseFile, public MPCOrbFile {
   class XOrsaAsteroidDatabaseFile_MPCOrbFile : public XOrsaAsteroidDatabaseFile {
   public:
   XOrsaAsteroidDatabaseFile_MPCOrbFile() {
   status = CLOSE;
   db = new AsteroidDatabase();
   }
   
   void Read() {
   MPCOrbFile af;
   af.db = db;
   af.SetFileName(GetFileName());
   af.Read();
   }
   };
   
   // class XOrsaAsteroidDatabaseFile_MPCCometFile : public XOrsaAsteroidDatabaseFile, public MPCCometFile {
   class XOrsaAsteroidDatabaseFile_MPCCometFile : public XOrsaAsteroidDatabaseFile {
   public:
   XOrsaAsteroidDatabaseFile_MPCCometFile() {
   status = CLOSE;
   db = new AsteroidDatabase();
   } 
   
   void Read() {
   MPCCometFile af;
   af.db = db;
   af.SetFileName(GetFileName());
   af.Read();
   }
   };
   
   // class XOrsaAsteroidDatabaseFile_NEODYSCAT : public XOrsaAsteroidDatabaseFile, public NEODYSCAT {
   class XOrsaAsteroidDatabaseFile_NEODYSCAT : public XOrsaAsteroidDatabaseFile {
   public:
   XOrsaAsteroidDatabaseFile_NEODYSCAT() {
   status = CLOSE;
   db = new AsteroidDatabase();
   } 
   
   void Read() {
   NEODYSCAT af;
   af.db = db;
   af.SetFileName(GetFileName());
   af.Read();
   }
   };
*/

////

class XOrsaAstorbFileReadProgress : public QWidget {
  
  Q_OBJECT
  
 public:
  XOrsaAstorbFileReadProgress(QWidget *parent=0) : QWidget(parent, 0, Qt::WType_TopLevel | Qt::WDestructiveClose) {
    
    QString caption;
    caption.sprintf("file read progress...");
    setCaption(caption);
    
    QVBoxLayout *vlay = new QVBoxLayout(this,4);
    
    status_label = new QLabel(this);
    vlay->addWidget(status_label);
    
    write_label(0);
    old_value=0;
  };
  
 private:
  void write_label(int value) {
    label.sprintf("objects read: %5i",value);
    status_label->setText(label);
  }
  
  public slots:
    void progress_slot(int value) {
    if (value-old_value >= 1000) {
      old_value = value;
      if (mutex.tryLock()) {
	write_label(value);
	mutex.unlock();
      }
    }
  }
  
 private:
  int old_value;
  QLabel *status_label;
  QString label;
  QMutex mutex; 
};


class XOrsaAstorbObjectItem : public QListViewItem {
  
  // DON'T use Q_OBJECT for this class
  // Q_OBJECT
  
 public:
  inline XOrsaAstorbObjectItem(QListView *parent, QString label1, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8) { };
  
 public:
  inline int XOrsaAstorbObjectItem::compare(QListViewItem *i, int col, bool ascending) const {

    using std::atof;
        
    XOrsaAstorbObjectItem *ii = dynamic_cast<XOrsaAstorbObjectItem*> (i);
    
    // integers
    // if (col == 0) return (atoi(key( col, ascending ).latin1()) - atoi( i->key(col, ascending).latin1()));
    
    // integers, with the zeros after all the numbered
    if (col == 0) {
      if ( (ast.n > 0) && (ii->ast.n > 0) ) return (ast.n - ii->ast.n);
      if ( (ast.n < 1) && (ii->ast.n < 1) ) return 0;
      if (    ast.n < 1) return (ii->ast.n);
      if (ii->ast.n < 1) return (   -ast.n);
    }
    
    // doubles
    if (col == 2 || col == 3 || col == 4) {
      const double d = (atof(key( col, ascending ).latin1()) - atof( i->key(col, ascending).latin1()));
      if (d < 0.0) return (-1);
      if (d > 0.0) return (+1);
      return (0);
    }
    
    // default
    return key( col, ascending ).compare( i->key(col, ascending));
  };
  
 public:
  // AstorbDataEntry ast;
  orsa::Asteroid ast;
};

class XOrsaAstorbObjectListView : public QListView {
  
  Q_OBJECT
  
 public:
  // XOrsaAstorbObjectListView(QWidget *parent=0) : QListView(parent) {
  XOrsaAstorbObjectListView(QWidget *parent=0) : QListView(parent, 0, Qt::WDestructiveClose) {
    
    setAllColumnsShowFocus(true);
    setShowSortIndicator(true);
    setSelectionMode(QListView::Extended);
    setItemMargin(3);
    
    QString a_label;
    a_label.sprintf("a [%s]",orsa::LengthLabel().c_str());
    
    addColumn("number");  setColumnAlignment(0,Qt::AlignRight);
    addColumn("name");    setColumnAlignment(1,Qt::AlignLeft);
    addColumn(a_label);   setColumnAlignment(2,Qt::AlignRight);
    addColumn("e");       setColumnAlignment(3,Qt::AlignRight);
    addColumn("i [DEG]"); setColumnAlignment(4,Qt::AlignRight);
    addColumn("notes");   setColumnAlignment(5,Qt::AlignLeft);
    
    // setResizeMode(QListView::LastColumn);
    
    setSorting(0,true);
    
    update_header();
  }
  
  public slots:
  
  void update_header() { 
    QString a_label;
    a_label.sprintf("a [%s]",orsa::LengthLabel().c_str());
    setColumnText(2,a_label);
  }
  
};

class XOrsaImportAstorbObjectsAdvancedDialog;

class XOrsaAstorbObjectsSelectPopupMenu : public QPopupMenu {
  
  Q_OBJECT
  
 public:
  XOrsaAstorbObjectsSelectPopupMenu(XOrsaImportAstorbObjectsAdvancedDialog *main_dialog, QWidget *parent=0);
  
 public:
  void GenerateEnabled(bool);
  
 public:
  XOrsaImportAstorbObjectsAdvancedDialog *main;
};

class XOrsaAstorbObjectsRemovePopupMenu : public QPopupMenu {
  
  Q_OBJECT
  
 public:
  XOrsaAstorbObjectsRemovePopupMenu(XOrsaImportAstorbObjectsAdvancedDialog *main_dialog, QWidget *parent=0);
  
 public:
  void GenerateEnabled(bool);
  
 public:
  XOrsaImportAstorbObjectsAdvancedDialog *main;
};

class XOrsaImportAstorbObjectsAdvancedDialog : public QDialog {
  
  Q_OBJECT
    
 public:
  XOrsaImportAstorbObjectsAdvancedDialog(std::vector<orsa::BodyWithEpoch>&, QWidget *parent=0);
  ~XOrsaImportAstorbObjectsAdvancedDialog();
  
 public:
  bool ok;
  
 private slots:
  // void slot_file_browse();
  void ok_pressed();
  void cancel_pressed();
  void closeEvent(QCloseEvent*);
  //
  void wait_for_the_read_thread();
  //
  void update_listview();
  void slot_object_selector();
  
  private slots:
  void popup_select(QListViewItem*, const QPoint&, int);
  void popup_remove(QListViewItem*, const QPoint&, int);
  //
  void select_item(QListViewItem*);
  void remove_item(QListViewItem*);
  //
  void read_file();
  void range_combo_changed(int);
  // 
  void update_listview_label();
  void update_file_entry();
  void update_selected_listview_label();
  //
  void widgets_enabler();
  //
  void progress_slot(int);
  
 public slots:
  void update_file_labels(int=0);
  
 private:
  void common_init();
    
 public slots:
  void slot_select();
  void slot_remove();
  void slot_generate_from_covariance_matrix_from_file_listview();
  void slot_generate_from_covariance_matrix_from_selected_listview();
  
 private:
  void private_generate_from_covariance_matrix(XOrsaAstorbObjectListView*);
  
 private:
  // QLineEdit *file_le;
  XOrsaFileEntry *file_entry;
  
  std::vector<orsa::BodyWithEpoch> &list;
  
 private:
  XOrsaAstorbObjectListView *listview, *selected_listview;
  
 private:
  QVGroupBox *listview_gb;
  QVGroupBox *selected_listview_gb;
  QLineEdit *kepler_ref_body_label;
  QLabel *file_info_label;
  QPushButton *xospb;
  XOrsaAsteroidFileTypeCombo *file_type_combo;
  XOrsaAstorbObjectsSelectPopupMenu *select_menu;
  XOrsaAstorbObjectsRemovePopupMenu *remove_menu;
  QLabel *listview_label, *selected_listview_label;
  QComboBox *range_combo;
  QLineEdit *range_start_le, *range_stop_le;
  QIntValidator *int_validator;
  QDoubleValidator *double_validator;
  QPushButton *okpb;
  QPushButton *cancpb;
  
 private:
  orsa::BodyWithEpoch kepler_ref_body;
  bool                kepler_ref_body_active;
  
 private:
  QLineEdit *name_le;
  QRadioButton *all_rb, *all_numbered_rb, *select_rb, *name_rb;
  
 private:
  // AstorbDatabase ast_db;
  // XOrsaAstorbFile astorb_file;
  XOrsaAsteroidDatabaseFile *asteroid_database_file;
  ReadAstorbFileThread *read_thread;
};

class ReadAstorbFileThread : public QThread {
  
 public:
  // ReadAstorbFileThread(XOrsaAstorbFile &astorb_file_in, XOrsaImportAstorbObjectsAdvancedDialog *w_in) : QThread(), astorb_file(astorb_file_in), w(w_in) { }
  // ReadAstorbFileThread(XOrsaAsteroidDatabaseFile *asteroid_database_file_in, XOrsaImportAstorbObjectsAdvancedDialog *w_in) : QThread(), asteroid_database_file(asteroid_database_file_in), w(w_in) { }
  ReadAstorbFileThread(XOrsaImportAstorbObjectsAdvancedDialog *w_in) : QThread(), w(w_in) { }
  
  void run() {
    // cerr << "ReadAstorbFileThread... filename: " << asteroid_database_file->GetFileName() << endl;
    asteroid_database_file->Read();
    asteroid_database_file->Close();
    w->update_file_labels();
  }
  
 public:
  // XOrsaAstorbFile &astorb_file;
  XOrsaAsteroidDatabaseFile *asteroid_database_file;
 private:
  XOrsaImportAstorbObjectsAdvancedDialog *w;
};

#endif // _XORSA_IMPORT_ASTORB_OBJECTS_H_
