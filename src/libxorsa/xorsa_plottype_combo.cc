/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_plottype_combo.h"

/* 
   XOrsaPlotTypeCombo::XOrsaPlotTypeCombo(QWidget *parent) : QComboBox(false,parent) {
   
   insertItem("distance");
   insertItem("semi-major axis");
   insertItem("eccentricity");
   insertItem("inclination");
   insertItem("node longitude");
   insertItem("pericenter longitude");
   insertItem("mean anomaly");
   insertItem("revolution period");
   insertItem("XY");
   insertItem("XZ");
   insertItem("YZ");
   
   connect(this,SIGNAL(activated(int)),this,SLOT(SetPlotType(int)));
   
   // sync!
   setCurrentItem(8);
   activated(8);
   }
   
   void XOrsaPlotTypeCombo::SetPlotType(int i) {
   
   // XOrsaPlotType old_t = t;
   
   // look at the combobox for the right order
   switch (i) {
   case 0: t = DISTANCE; break;
   case 1: t = A; break;
   case 2: t = E; break;
   case 3: t = I; break;
   case 4: t = NODE; break;
   case 5: t = PERI; break;
   case 6: t = MM; break;
   case 7: t = PERIOD; break;
   case 8: t = XY; break;
   case 9: t = XZ; break;
   case 10: t = YZ; break;
   }
   
   // if (t != old_t) emit TypeChanged(t);
   emit TypeChanged(t);
   
   }
   
   void XOrsaPlotTypeCombo::SetPlotType(XOrsaPlotType t_in) {
   
   // if (t_in != t) {
   
   t = t_in; 
   
   // look at the combobox for the right order
   switch (t) {
   case DISTANCE: activated(0); break;
   case A:        activated(1); break;
   case E:        activated(2); break;
   case I:        activated(3); break;
   case NODE:     activated(4); break;
   case PERI:     activated(5); break;
   case MM:       activated(6); break;
   case PERIOD:   activated(7); break;
   case XY:       activated(8); break;
   case XZ:       activated(9); break;
   case YZ:       activated(10); break;
   }
   
   emit TypeChanged(t);
   
   // }
   
   }
   
   XOrsaPlotType XOrsaPlotTypeCombo::GetPlotType() {
   return t;
   }
*/

//
// Kepler
//

XOrsaKeplerPlotTypeCombo::XOrsaKeplerPlotTypeCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("semi-major axis");
  insertItem("eccentricity");
  insertItem("inclination");
  insertItem("longitude of ascending node"); // insertItem("node longitude");
  insertItem("argument of pericenter"); // insertItem("pericenter longitude");
  insertItem("mean anomaly");
  insertItem("eccentric anomaly");
  insertItem("revolution period");
  insertItem("distance");
  insertItem("pericenter distance");
  insertItem("apocenter distance");
  // insertItem("XY");
  // insertItem("XZ");
  // insertItem("YZ");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetPlotType(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0);
}

void XOrsaKeplerPlotTypeCombo::SetPlotType(int i) {
  
  // XOrsaPlotType old_t = t;
  
  // look at the combobox for the right order
  switch (i) {
  case 0:  t = A; break;
  case 1:  t = E; break;
  case 2:  t = I; break;
  case 3:  t = NODE; break;
  case 4:  t = PERI; break;
  case 5:  t = MM; break;
  case 6:  t = EE; break;
  case 7:  t = PERIOD; break;
  case 8:  t = DISTANCE; break;
  case 9:  t = PERI_DIST; break;
  case 10: t = APO_DIST; break;
  }
  
  // if (t != old_t) emit TypeChanged(t);
  emit TypeChanged(t);
  
}

void XOrsaKeplerPlotTypeCombo::SetPlotType(XOrsaPlotType t_in) {
  
  // if (t_in != t) {
  
  t = t_in; 
  
  // look at the combobox for the right order
  switch (t) {
  case A:         activated(0);  break;
  case E:         activated(1);  break;
  case I:         activated(2);  break;
  case NODE:      activated(3);  break;
  case PERI:      activated(4);  break;
  case MM:        activated(5);  break;
  case EE:        activated(6);  break;
  case PERIOD:    activated(7);  break;
  case DISTANCE:  activated(8);  break;
  case PERI_DIST: activated(9);  break;
  case APO_DIST:  activated(10); break;
  default: /*******************/ break;
  }
  
  emit TypeChanged(t);
  
  // }
  
}

XOrsaPlotType XOrsaKeplerPlotTypeCombo::GetPlotType() {
  return t;
}


//
// 2D
//

XOrsa2DPlotTypeCombo::XOrsa2DPlotTypeCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("XY");
  insertItem("XZ");
  insertItem("YZ");
  insertItem("RZ");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetPlotType(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0);
}

void XOrsa2DPlotTypeCombo::SetPlotType(int i) {
  
  // look at the combobox for the right order
  switch (i) {
  case 0: t = XY; break;
  case 1: t = XZ; break;
  case 2: t = YZ; break;
  case 3: t = RZ; break;
  }
  
  emit TypeChanged(t);
  
}

void XOrsa2DPlotTypeCombo::SetPlotType(XOrsaPlotType t_in) {
  
  t = t_in; 
  
  // look at the combobox for the right order
  switch (t) {
  case XY:       activated(0); break;
  case XZ:       activated(1); break;
  case YZ:       activated(2); break;
  case RZ:       activated(3); break;
  default: /*****************/ break;
  }
  
  emit TypeChanged(t);
  
}

XOrsaPlotType XOrsa2DPlotTypeCombo::GetPlotType() {
  return t;
}


