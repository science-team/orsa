/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_new_universe_dialog.h"

#include "xorsa_wrapper.h"

#include <orsa_file.h>

#include <qlayout.h>
#include <qgroupbox.h> 
#include <qlabel.h> 
#include <qlineedit.h> 
#include <qpushbutton.h> 
#include <qtextedit.h> 
#include <qmessagebox.h> 

#include <string.h>

using namespace orsa;

XOrsaNewUniverseDialog::XOrsaNewUniverseDialog(bool read_only_in, QWidget *parent) : QDialog(parent,0,true), read_only(read_only_in) {
  
  if (read_only) {
    setCaption("Universe Info");
  } else {
    setCaption("New Universe Dialog");
  }
  
  QVBoxLayout *vlay = new QVBoxLayout(this,4);
  
  QGroupBox *description_gb = new QGroupBox("description",this);
  description_gb->setColumns(2);
  description_gb->setAlignment(Qt::AlignLeft);
  QLabel *namel = new QLabel(description_gb);
  namel->setText("name:");
  le_name = new QLineEdit(description_gb);
  le_name->setText(universe->name.c_str());
  
  QWidget *dw = new QWidget(description_gb);
  QVBoxLayout *dw_lay = new QVBoxLayout(dw);
  QLabel *descriptionl = new QLabel(dw);
  dw_lay->addWidget(descriptionl);
  dw_lay->addStretch();
  descriptionl->setText("description:");
  te_description = new QTextEdit(description_gb);
  te_description->setText(universe->description.c_str());
  vlay->addWidget(description_gb);
  
  ////
  
  // UNITS
  units_gb = new QGroupBox("default units",this);
  units_gb->setColumns(2);
  //
  QLabel *spacel = new QLabel(units_gb);
  spacel->setText("space:");
  spacecb = new LengthCombo(units_gb);
  spacecb->SetUnit(units->GetLengthBaseUnit());
  //
  QLabel *masslabel = new QLabel(units_gb);
  masslabel->setText("mass:");
  masscb = new MassCombo(units_gb);
  masscb->SetUnit(units->GetMassBaseUnit());
  //
  QLabel *timel = new QLabel(units_gb);
  timel->setText("time:");
  timecb = new TimeCombo(units_gb);
  timecb->SetUnit(units->GetTimeBaseUnit());
  //
  vlay->addWidget(units_gb);
  
  //// 
  
  // universe type and reference system
  utrs_gb = new QGroupBox("universe type and reference system",this);
  utrs_gb->setColumns(2);
  //
  new QLabel("universe type:",utrs_gb);
  utcb = new UniverseTypeCombo(utrs_gb);
  //
  new QLabel("reference system:",utrs_gb);
  recb = new ReferenceSystemCombo(utrs_gb);
  //
  new QLabel("time-scale:",utrs_gb);
  tscb = new TimeScaleCombo(utrs_gb);
  //
  vlay->addWidget(utrs_gb);
  
  utcb->SetUniverseType(universe->GetUniverseType());
  recb->SetReferenceSystem(universe->GetReferenceSystem());
  tscb->SetTimeScale(universe->GetTimeScale());
  
  connect(utcb,SIGNAL(activated(int)),this,SLOT(widgets_enabler()));
  
  ////
  
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  
  hok->addStretch();
  //
  okpb = new QPushButton(this);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  cancpb = new QPushButton(this);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();

  widgets_enabler();
}


void XOrsaNewUniverseDialog::widgets_enabler() {
  
  if (utcb->GetUniverseType() == Real) {
    recb->setEnabled(true);
    tscb->setEnabled(true);
  } else {
    recb->setEnabled(false);
    tscb->setEnabled(false);
  }
  
  if (read_only) {
    units_gb->setEnabled(false);
    utrs_gb->setEnabled(false);    
  } else {
    units_gb->setEnabled(true);
    utrs_gb->setEnabled(true);    
  }
  
}

void XOrsaNewUniverseDialog::ok_pressed() {
  
  if (read_only) {
    
    universe->name = le_name->text().latin1();
    universe->description = te_description->text().latin1();
    
  } else {
    
    if (utcb->GetUniverseType() == Real) {
      
      // have the JPL ephem file?
      if ( strlen(config->paths[JPL_EPHEM_FILE]->GetValue().c_str()) < 5) {
	QMessageBox::warning( this, "No JPL_EPHEM_FILE file defined!",
			      "Sorry, but for the Real Universe you need a valid JPL ephemeris file.\n"
			      "You can download this file using the 'update' tool in the 'Tools' menu.\n"
			      "See the ORSA website for more information: http://orsa.sourceforge.net\n",
			      QMessageBox::Ok,QMessageBox::NoButton);
	return;
      }
    }
    
    emit closing_universe();
    
    if (universe) delete universe;
    // new Universe(spacecb->GetUnit(), masscb->GetUnit(), timecb->GetUnit(), utcb->GetUniverseType(), recb->GetReferenceSystem(), tscb->GetTimeScale());
    new XOrsaUniverse(spacecb->GetUnit(), masscb->GetUnit(), timecb->GetUnit(), utcb->GetUniverseType(), recb->GetReferenceSystem(), tscb->GetTimeScale());
    
    universe->name = le_name->text().latin1();
    universe->description = te_description->text().latin1();
  }  
  
  ok = true;
  done(0);
}

void XOrsaNewUniverseDialog::cancel_pressed() {
  ok = false;
  done(0);
}

////

UniverseTypeCombo::UniverseTypeCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("real");
  insertItem("simulated");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetUniverseType(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0);
}

void UniverseTypeCombo::SetUniverseType(int i) {
  
  // look at the combobox for the right order
  switch (i) {
  case 0: ut = Real;      break;
  case 1: ut = Simulated; break;
  }

}

void UniverseTypeCombo::SetUniverseType(UniverseType ut) {
  // look at the combobox for the right order
  switch (ut) {
  case Real:      setCurrentItem(0); SetUniverseType(0); break;
  case Simulated: setCurrentItem(1); SetUniverseType(1); break;
  }
}

UniverseType UniverseTypeCombo::GetUniverseType() {
  return (ut);
}


////

ReferenceSystemCombo::ReferenceSystemCombo(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("equatorial");
  insertItem("ecliptic");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetReferenceSystem(int)));
  
  // sync!
  setCurrentItem(1);
  activated(1);
}

void ReferenceSystemCombo::SetReferenceSystem(int i) {
  
  // look at the combobox for the right order
  switch (i) {
  case 0: rs = EQUATORIAL; break;
  case 1: rs = ECLIPTIC;   break;
  }

}

void ReferenceSystemCombo::SetReferenceSystem(ReferenceSystem rs) {
  // look at the combobox for the right order
  switch (rs) {
  case EQUATORIAL: setCurrentItem(0); SetReferenceSystem(0); break;
  case ECLIPTIC:   setCurrentItem(1); SetReferenceSystem(1); break;
  }  
}

ReferenceSystem ReferenceSystemCombo::GetReferenceSystem() {
  return (rs);
}

