/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_all_objects_info.h"

#include <qlabel.h>
#include <qlayout.h>
#include <qcheckbox.h>

using namespace std;
using namespace orsa;

// XOrsaAllObjectsInfo::XOrsaAllObjectsInfo(vector<BodyWithEpoch> &b, vector<JPLBody> &jb, QWidget *parent) : QGroupBox(parent), body(b), JPL_body(jb) {
XOrsaAllObjectsInfo::XOrsaAllObjectsInfo(vector<BodyWithEpoch> &b, vector<JPL_planets> &jb, QWidget *parent) : QGroupBox(parent), body(b), JPL_body(jb) {
  
  setColumns(1);
  setInsideSpacing(0);
  
  QWidget *top_hb = new QWidget(this);
  QHBoxLayout *top_lay = new QHBoxLayout(top_hb,3); // ,3
  
  switch (universe->GetUniverseType()) {
  case Real: { 
    
    QWidget     *obj_w  = new QWidget(this);
    QHBoxLayout *obj_lay = new QHBoxLayout(obj_w,3); // ,3
    
    QGroupBox *w_gb = new QGroupBox("planets",obj_w);
    w_gb->setColumns(1);
    obj_lay->addWidget(w_gb); 
    jpl_widget = new XOrsaJPLPlanetsWidget(w_gb);
    jpl_widget->SetBodies(JPL_body);
    
    QGroupBox *l_gb = new QGroupBox("objects",obj_w);
    l_gb->setColumns(1);
    obj_lay->addWidget(l_gb); 
    listview = new XOrsaAllObjectsListView(body,l_gb);
    info = new QLabel(l_gb);
    
    listview->SetMode(Keplerian);
    
    break;
  }
  case Simulated: { 
    
    setTitle("objects");
    
    top_lay->addWidget(new QLabel("mode",top_hb));
    mode_combo = new XOrsaListViewMode(top_hb);
    top_lay->addWidget(mode_combo);
    top_lay->addWidget(new QLabel("ref. body",top_hb));
    // obj_combo = new XOrsaObjectsCombo(&bodies_for_combo,true,top_hb);
    obj_combo = new XOrsaImprovedObjectsCombo(&bodies_for_combo,true,top_hb);
    top_lay->addWidget(obj_combo); 
    top_lay->addStretch(1);
    //
    mode_combo->SetMode(Cartesian);
    obj_combo->setEnabled(false);
    
    listview = new XOrsaAllObjectsListView(body,this);
    info = new QLabel(this);
    
    connect(obj_combo,SIGNAL(ObjectChanged(int)),listview,SLOT(SetKeplerRefBodyIndex(int)));
    connect(mode_combo,SIGNAL(ModeChanged()),this,SLOT(widgets_enabler()));
    
    break;
  }
  }
  
  connect(listview,SIGNAL(ObjectsChanged()),this,SLOT(update_info()));
  connect(listview,SIGNAL(ObjectsChanged()),this,SLOT(update_misc()));
  
  update_info();
  update_misc();
  
}

void XOrsaAllObjectsInfo::update_info() {
  
  unsigned int j, nmass=0;
  for (j=0;j<body.size();j++) if(body[j].mass() > 0) nmass++;
  
  QString s;
  
  switch (universe->GetUniverseType()) {
  case Simulated: s.sprintf("objects: %i   massive: %i",body.size(),nmass); break;
  case Real:      s.sprintf("objects: %i",body.size());                     break;
  }
  
  info->setText(s);  
}

void XOrsaAllObjectsInfo::update_misc() {
  
  if (universe->GetUniverseType() == Simulated) {
    bodies_for_combo.resize(body.size());
    unsigned int k=0;
    while (k < body.size()) {
      bodies_for_combo[k] = body[k];
      ++k;
    }
    obj_combo->Set(&bodies_for_combo,true);
  }
}

void XOrsaAllObjectsInfo::widgets_enabler() {
  
  if (universe->GetUniverseType() == Simulated) {
    
    switch(mode_combo->GetMode()) {
    case Cartesian: obj_combo->setEnabled(false); break;
    case Keplerian: obj_combo->setEnabled(true);  break;
    }
    
    listview->SetKeplerRefBodyIndex(obj_combo->GetObject());
    listview->SetMode(mode_combo->GetMode());
  }
}

// void XOrsaAllObjectsInfo::GetBodies(vector<BodyWithEpoch> &f, vector<JPLBody> &jf) {
void XOrsaAllObjectsInfo::GetBodies(vector<BodyWithEpoch> &f, vector<JPL_planets> &jf) {
  f = body;
  if (universe->GetUniverseType() == Real) {
    // Date dummy_date; dummy_date.SetGregor(2000,1,1);
    // jpl_widget->GetSelectedPlanets(dummy_date,jf);
    jpl_widget->GetSelectedPlanets(jf);
  }
}

/*****/

XOrsaListViewMode::XOrsaListViewMode(QWidget *parent) : QComboBox(false,parent) {
  
  insertItem("cartesian");
  insertItem("keplerian");
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetMode(int)));
  
  // sync!
  setCurrentItem(0);
  activated(0); 
  
}

void XOrsaListViewMode::SetMode(int i) {
  
  // look at the combobox for the right order
  switch (i) {
  case 0: mode = Cartesian; break;
  case 1: mode = Keplerian; break;
  }
  
  emit ModeChanged();
  
}

void XOrsaListViewMode::SetMode(ObjectsListViewMode m) {
  
  // look at the combobox for the right order
  switch (m) {
  case Cartesian: setCurrentItem(0); SetMode(0); break;
  case Keplerian: setCurrentItem(1); SetMode(1); break;
  }
  
}
