/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_DATE_H_
#define _XORSA_DATE_H_

#include <qdialog.h>
#include <qlayout.h>
#include <qhbox.h> 
#include <qvbox.h> 
#include <qpushbutton.h> 

#include "xorsa_units_combo.h"
#include "xorsa_plot_area.h"

class QLineEdit;
class QSpinBox;

#include <orsa_units.h>

class XOrsaDate : public QWidget {
  
  Q_OBJECT
  
 public:
  XOrsaDate(QWidget *parent=0);
  XOrsaDate(const orsa::Date&, QWidget *parent=0);
  
 public:
  void SetDate(const orsa::Date&);
  void SetDate(const orsa::Date&, const orsa::TimeScale);
  orsa::Date GetDate();
  
 public slots:
  void SetNow();
  void SetToday();
  
 private:
  void init_draw();
  void update_JD_content();
  void update_MJD_content();
  void update_Date_content();
  
 private slots:
  void TimeScaleChanged(int);
  
  protected slots:
  void update_from_JD();
  void update_from_MJD();
  void update_from_date();  
  
 private:
  QLineEdit * line_JD, * line_MJD;
  QSpinBox  * spin_y, * spin_m, * spin_d;
  QSpinBox  * spin_H, * spin_M, * spin_S;
  TimeScaleCombo * ts_combo;
  
 private:
  orsa::Date date;
  bool internal_change;
};

class XOrsaDateDialog : public QDialog {
  
  Q_OBJECT
  
 public:
  XOrsaDateDialog(orsa::UniverseTypeAwareTime &t, QWidget *parent=0);
  
 private slots:
  void ok_pressed();
  void cancel_pressed();
  
 private:
  orsa::UniverseTypeAwareTime & time;
  XOrsaDate * od;
  QPushButton * ok_pb, * cancel_pb;
};

class XOrsaDatePushButton : public QPushButton, public orsa::UniverseTypeAwareTime {
  
  Q_OBJECT
  
 public:
  XOrsaDatePushButton(QWidget *parent=0);
  XOrsaDatePushButton(orsa::UniverseTypeAwareTime &t, QWidget *parent=0);
  
 public:
  void SetTime(const double x)       { orsa::UniverseTypeAwareTime::SetTime(x); update_label(); }
  void SetTime(const orsa::Date & d) { orsa::UniverseTypeAwareTime::SetTime(d); update_label(); }
  void SetDate(const orsa::Date & d) { orsa::UniverseTypeAwareTime::SetDate(d); update_label(); }
  void SetTime(const orsa::UniverseTypeAwareTime & t) { orsa::UniverseTypeAwareTime::SetTime(t); update_label(); }
  
 signals:
  void DateChanged();
  
 private slots:
  void slot_change_time(); 
  
 private slots:
  void update_label(); 
};

#endif // _XORSA_DATE_H_
