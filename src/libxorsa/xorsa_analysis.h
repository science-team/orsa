/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_ANALYSIS_H_
#define _XORSA_ANALYSIS_H_

#include <orsa_universe.h>
#include <orsa_body.h>
#include <orsa_orbit.h>
#include <orsa_fft.h>
#include <orsa_file.h>

#include <qdialog.h>
#include <qlistview.h>

#include "xorsa_plot_area.h"
#include "xorsa_objects_combo.h"

class QTabWidget;
class QStatusBar;
class QSpinBox;
class QLabel;

class SignalTypeCombo : public QComboBox {
  
  Q_OBJECT
    
 public:
  SignalTypeCombo(QWidget * parent = 0);
  
 private slots:
  void SetSignalType(int);
  
 public:  
  orsa::FFTSearch GetSignalType();
  
 private:
  orsa::FFTSearch t;
};


class SignalAmplitudeCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  SignalAmplitudeCombo(QWidget *parent=0);
  
 private slots:
  void SetSignalAmplitude(int);
  
 public slots:
  void SetSignalAmplitude(orsa::FFTSearchAmplitude);
  
 public:  
  orsa::FFTSearchAmplitude GetSignalAmplitude();
  
 private:
  orsa::FFTSearchAmplitude sa;
};


class SignalPhaseCombo : public QComboBox {
  
  Q_OBJECT
    
 public:
  SignalPhaseCombo(QWidget * parent = 0);
  
 private slots:
  void SetSignalPhase(int);
  
 public slots:
  void SetSignalPhase(orsa::FFTSearchPhase);
  
 public:  
  orsa::FFTSearchPhase GetSignalPhase();
  
 private:
  orsa::FFTSearchPhase sp;
};


class FFTAlgoCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  FFTAlgoCombo(QWidget * parent = 0);
  
 private slots:
  void SetFFTAlgo(int);
  
 public:
  orsa::FFTAlgorithm GetFFTAlgo();
  
 private:
  orsa::FFTAlgorithm t;
};


class XOrsaPeaksListItem : public QListViewItem {
  
  // DON'T use Q_OBJECT for this class
  // Q_OBJECT
  
 public:
  XOrsaPeaksListItem(QListView *parent, QString label1, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null);
  
 public:
  int XOrsaPeaksListItem::compare(QListViewItem * i, int col, bool ascending) const;    
  
};

enum XOrsaAnalysisMode {EVOLUTION,ORBITSTREAM,SWIFTFILE};

class XOrsaAnalysis : public QWidget {
  
  Q_OBJECT

 public:
  XOrsaAnalysis(const orsa::Evolution*,   QWidget *parent=0);
  XOrsaAnalysis(orsa::OrbitStream*, QWidget *parent=0);
  XOrsaAnalysis(orsa::SWIFTFile*,   QWidget *parent=0);
  
 private: 
  int  body_index;
  int rbody_index;
  
 private:
  QWidget *control;

 private slots:
  void update_body(int);
  void update_rbody(int);
  void status_bar_plot_coords(QMouseEvent*);
  void SetBodiesIndex();
  // void update_area(XOrsaPlotType type);
  void SetArea(QWidget*);
  void signal_type_changed(int);
  void fft_algo_changed(int);
  void ComputeOrbitalElements();
  void ComputeFFT();
  void InitCommonGraphics();
  
 private:  
  orsa::OrbitStream os;

 private: 
  const std::vector<orsa::Body> *bodies;
  
  std::vector<XOrsaPlotCurve> *curves;
  QTabWidget *tab;
  XOrsaImprovedObjectsCombo *objects_combo, *robjects_combo;
  XOrsaPlotArea *area, *area_signal, *area_spectrum, *area_peaks, *area_xy;
  QLabel     *status_bar_label;
  SignalAmplitudeCombo *sac;
  SignalPhaseCombo *spc;
  FFTAlgoCombo *fac;
  QListView *list_peaks;
  QLineEdit *wale, *wstartle, *wsteple;
  QSpinBox  *spin;
  QComboBox *wincombo;
  
  // SWIFT MODE
  QSpinBox  *swift_spin;
  orsa::SWIFTFile *swift_file;
  
 private:
  const orsa::Evolution * evol;
  
 private:
  const XOrsaAnalysisMode mode;
};

#endif // _XORSA_OBJECT_SELECTOR_H_
