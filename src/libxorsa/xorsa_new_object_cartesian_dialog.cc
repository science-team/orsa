/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_new_object_cartesian_dialog.h"

#include <orsa_coord.h>
#include <orsa_universe.h>

#include <qlayout.h>
#include <qpushbutton.h> 
#include <qvalidator.h> 
#include <qlabel.h> 
#include <qgroupbox.h> 
#include <qlineedit.h> 
#include <qhbox.h> 

using namespace orsa;

XOrsaNewObjectCartesianDialog::XOrsaNewObjectCartesianDialog(QWidget *parent) : QDialog(parent,0,true) {
  
  char name[1024];
  snprintf(name,1024,"object name");
  b = BodyWithEpoch(name,0.0);
  //
  Date d;
  d.SetGregor(2000,1,1);
  epoch.SetDate(d);
  
  init_draw();
  init_values();
  
  setCaption("new object dialog");
}

XOrsaNewObjectCartesianDialog::XOrsaNewObjectCartesianDialog(BodyWithEpoch &b_in, QWidget *parent) : QDialog(parent,0,true) {
  
  b        = b_in;
  ref_body = b_in;
  //
  epoch = b.Epoch();
  
  init_draw();
  init_values();
  
  setCaption("object editor");
}

void XOrsaNewObjectCartesianDialog::init_values() {
  
  QString line;
  
  // units
  length_unit lu_pos = pos_spacecb->GetUnit();
  length_unit lu_vel = vel_spacecb->GetUnit();
  time_unit   tu_vel = vel_timecb->GetUnit();
  // mass_unit   mu     = masscb->GetUnit();
  
  line = b.name().c_str();                           le_name->setText(line);
  if (universe->GetUniverseType() == Simulated) {  
    mass_unit   mu     = masscb->GetUnit();
    line.sprintf("%.12g",FromUnits(b.mass(),mu,-1));   le_mass->setText(line);
  }
  //
  line.sprintf("%.12g",FromUnits(b.position().x,lu_pos,-1)); le_px->setText(line);
  line.sprintf("%.12g",FromUnits(b.position().y,lu_pos,-1)); le_py->setText(line);
  line.sprintf("%.12g",FromUnits(b.position().z,lu_pos,-1)); le_pz->setText(line);
  //
  line.sprintf("%.12g",FromUnits(FromUnits(b.velocity().x,lu_vel,-1),tu_vel)); le_vx->setText(line);
  line.sprintf("%.12g",FromUnits(FromUnits(b.velocity().y,lu_vel,-1),tu_vel)); le_vy->setText(line);
  line.sprintf("%.12g",FromUnits(FromUnits(b.velocity().z,lu_vel,-1),tu_vel)); le_vz->setText(line);
  
}

void XOrsaNewObjectCartesianDialog::init_draw() {
  
  QVBoxLayout *vlay = new QVBoxLayout(this,4);
  
  // name and mass
  QGroupBox *name_mass_gb = new QGroupBox("name and mass",this);
  name_mass_gb->setColumns(2); // IMPORTANT: number of columns
  //
  new QLabel("name",name_mass_gb);
  le_name = new QLineEdit(name_mass_gb);
  //
  switch (universe->GetUniverseType()) {
  case Real: { 
    new QLabel("mass",name_mass_gb);
    new QLabel("0.0",name_mass_gb);
    break;
  } 
  case Simulated: {
    new QLabel("mass",name_mass_gb);
    le_mass = new QLineEdit(name_mass_gb);
    new QLabel("units",name_mass_gb);
    masscb = new MassCombo(name_mass_gb);
    masscb->SetUnit(units->GetMassBaseUnit());
    break;
  }
  }  
  //
  vlay->addWidget(name_mass_gb);
  
  // epoch
  if (universe->GetUniverseType() == Real) {
    QGroupBox *misc_gb = new QGroupBox(this);
    misc_gb->setColumns(2);
    
    new QLabel("epoch",misc_gb);
    odpb = new XOrsaDatePushButton(epoch,misc_gb);
    
    vlay->addWidget(misc_gb);
  }
  
  // position
  QGroupBox *position_gb = new QGroupBox("position",this);
  position_gb->setColumns(2); // IMPORTANT: number of columns
  //
  QLabel *pxl = new QLabel(position_gb);
  pxl->setText("X");
  le_px = new QLineEdit(position_gb);  
  //
  QLabel *pyl = new QLabel(position_gb);
  pyl->setText("Y");
  le_py = new QLineEdit(position_gb);  
  //
  QLabel *pzl = new QLabel(position_gb);
  pzl->setText("Z");
  le_pz = new QLineEdit(position_gb);  
  //
  QLabel *spacel = new QLabel(position_gb);
  spacel->setText("units");
  pos_spacecb = new LengthCombo(position_gb);
  pos_spacecb->SetUnit(units->GetLengthBaseUnit());
  //
  vlay->addWidget(position_gb);
  
  // velocity
  QGroupBox *velocity_gb = new QGroupBox("velocity",this);
  velocity_gb->setColumns(2); // IMPORTANT: number of columns
  //
  QLabel *vxl = new QLabel(velocity_gb);
  vxl->setText("X");
  le_vx = new QLineEdit(velocity_gb);
  //
  QLabel *vyl = new QLabel(velocity_gb);
  vyl->setText("Y");
  le_vy = new QLineEdit(velocity_gb);
  //
  QLabel *vzl = new QLabel(velocity_gb);
  vzl->setText("Z");
  le_vz = new QLineEdit(velocity_gb);
  // 
  new QLabel("units",velocity_gb);
  //
  QWidget *tw = new QWidget(velocity_gb);
  QHBoxLayout *vu_hbl = new QHBoxLayout(tw);
  //
  vel_spacecb = new LengthCombo(tw);
  vel_spacecb->SetUnit(units->GetLengthBaseUnit());
  vu_hbl->addWidget(vel_spacecb);
  //
  QLabel *sll = new QLabel(" / ",tw);
  vu_hbl->addWidget(sll);
  //
  vel_timecb = new TimeCombo(tw);
  vel_timecb->SetUnit(units->GetTimeBaseUnit());
  vu_hbl->addWidget(vel_timecb);
  //
  vu_hbl->addStretch(1);
  //
  vlay->addWidget(velocity_gb);
  
  // validation
  QDoubleValidator *vd = new QDoubleValidator(this);
  //
  le_px->setValidator(vd);
  le_py->setValidator(vd);
  le_pz->setValidator(vd);
  //
  le_vx->setValidator(vd);
  le_vy->setValidator(vd);
  le_vz->setValidator(vd);
  // mass >= 0.0
  QDoubleValidator *vdz = new QDoubleValidator(this);
  vdz->setBottom(0.0);
  if (universe->GetUniverseType() == Simulated) {
    le_mass->setValidator(vdz);
  }
  
  // push buttons
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  
  hok->addStretch();
  //
  okpb = new QPushButton(this);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  cancpb = new QPushButton(this);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
  
}

void XOrsaNewObjectCartesianDialog::SetBody() {
  
  // units
  length_unit lu_pos = pos_spacecb->GetUnit();
  length_unit lu_vel = vel_spacecb->GetUnit();
  time_unit   tu_vel = vel_timecb->GetUnit();
  // mass_unit   mu     = masscb->GetUnit();
  
  switch (universe->GetUniverseType()) {
  case Real: { 
    b = BodyWithEpoch(le_name->text().simplifyWhiteSpace().latin1(),0.0);
    break;
  }
  case Simulated: {
    mass_unit mu = masscb->GetUnit();
    b = BodyWithEpoch(le_name->text().simplifyWhiteSpace().latin1(),FromUnits(le_mass->text().toDouble(),mu));
    break;
  }
  }
  
  b.SetEpoch(epoch);
  
  // position
  //
  double px,py,pz;
  //
  px = le_px->text().toDouble();
  py = le_py->text().toDouble();
  pz = le_pz->text().toDouble();
  //
  px = FromUnits(px,lu_pos);
  py = FromUnits(py,lu_pos);
  pz = FromUnits(pz,lu_pos);
  //
  b.SetPosition(px,py,pz);
  
  // velocity
  //
  double vx,vy,vz;
  //
  vx = le_vx->text().toDouble();
  vy = le_vy->text().toDouble();
  vz = le_vz->text().toDouble();
  //
  vx = FromUnits(vx,lu_vel);
  vy = FromUnits(vy,lu_vel);
  vz = FromUnits(vz,lu_vel);
  //
  vx = FromUnits(vx,tu_vel,-1);
  vy = FromUnits(vy,tu_vel,-1);
  vz = FromUnits(vz,tu_vel,-1);
  //
  b.SetVelocity(vx,vy,vz);
  
}


BodyWithEpoch XOrsaNewObjectCartesianDialog::GetBody() {
  return b;
}


void XOrsaNewObjectCartesianDialog::ok_pressed() {
  SetBody();
  ok = true;
  done(0);  
}


void XOrsaNewObjectCartesianDialog::cancel_pressed() {
  ok = false;
  b  = ref_body;
  done(0);
}

