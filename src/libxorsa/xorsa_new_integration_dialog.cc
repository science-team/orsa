/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_new_integration_dialog.h"

#include "xorsa_plot_area.h"
#include "xorsa_date.h"

#include <orsa_universe.h>
#include <sdncal.h>

#include <iostream>

#include <qlayout.h>
#include <qgroupbox.h> 
#include <qlabel.h> 
#include <qlineedit.h> 
#include <qtextedit.h> 
#include <qcombobox.h> 
#include <qpushbutton.h> 
#include <qvalidator.h> 
#include <qhgroupbox.h> 
#include <qmessagebox.h>
#include <qcheckbox.h>
#include <qbuttongroup.h> 
#include <qtooltip.h> 

using namespace std;
using namespace orsa;

XOrsaNewIntegrationDialog::XOrsaNewIntegrationDialog(XOrsaEvolution *evol_in, QWidget *parent) : QDialog(parent,0,true), evol(evol_in) {
  
  setCaption("New Integration Dialog");
  
  QVBoxLayout *vlay = new QVBoxLayout(this,3);
  
  QGroupBox *description_gb = new QGroupBox("Description",this);
  description_gb->setColumns(2); // IMPORTANT: number of columns
  // description_gb->setAlignment(Qt::AlignLeft);
  QLabel *namel = new QLabel(description_gb);
  namel->setText("Name:");
  namele = new QLineEdit(description_gb);
  namele->setText("integration name");
  
  vlay->addWidget(description_gb);
  
  // QButtonGroup * phys_gb = new QButtonGroup("Physical parameters", this);
  QButtonGroup * phys_gb = new QButtonGroup("Gravitational Interaction Options", this);
  // phys_gb->setColumns(2); // IMPORTANT: number of columns
  phys_gb->setColumns(5); // IMPORTANT: number of columns
  // phys_gb->setAlignment(Qt::AlignLeft);
  phys_gb->setExclusive(false);
  
  // QLabel *interactionl = new QLabel(phys_gb);
  // interactionl->setText("Interaction:");
  // interactioncb = new InteractionCombo(phys_gb);
  
  // new NewtonOptions(phys_gb);
  rb_multipole         = new QCheckBox("Multipole Expansion",phys_gb);
  rb_relativistic      = new QCheckBox("Relativistic Corrections",phys_gb);
  rb_fast_relativistic = new QCheckBox("Relativistic Corrections (fast version)",phys_gb);
  rb_force_JPL         = new QCheckBox("Force JPL Ephemeris Data",phys_gb);
  //
  connect(rb_relativistic,SIGNAL(clicked()),this,SLOT(uncheck_rb_fast_relativistic()));
  connect(rb_fast_relativistic,SIGNAL(clicked()),this,SLOT(uncheck_rb_relativistic()));
  //
  QToolTip::add(rb_multipole,        "Include Multipole Expansion corrections for oblate objects");
  QToolTip::add(rb_relativistic,     "Include Relativistic Corrections to the Gravitational interaction");
  QToolTip::add(rb_fast_relativistic,"Include Relativistic Corrections to the Gravitational interaction (this version uses a faster, approximated equation)");
  QToolTip::add(rb_force_JPL,        "Position and Velocity of Planets and Moon are always taken from the JPL Epheeris files");
  //
  if (universe->GetUniverseType() != Real) {
    rb_force_JPL->setEnabled(false);
  }
  
  vlay->addWidget(phys_gb);
  
  ////
  
  all_obj_info = new XOrsaAllObjectsInfo(evol->start_bodies, evol->start_JPL_bodies, this);
  vlay->addWidget(all_obj_info);
  
  ////
  
  QDoubleValidator * timevalid = new QDoubleValidator(this);
  
  ////
  
  QGroupBox * integrator_gb = new QGroupBox("Integrator",this);
  integrator_gb->setColumns(6); // IMPORTANT: number of columns
  
  QLabel * algol = new QLabel(integrator_gb);
  algol->setText("algorithm:");
  
  integratorcb = new IntegratorCombo(integrator_gb);
  connect(integratorcb,SIGNAL(activated(int)),this,SLOT(check_lines_integrator_enabled(int)));
  connect(integratorcb,SIGNAL(IntegratorChanged(int)),this,SLOT(check_lines_integrator_enabled(int)));
  
  QLabel *accuracyl = new QLabel(integrator_gb);
  accuracyl->setText("accuracy:");
  accuracy_le = new QLineEdit(integrator_gb);
  accuracy_le->setValidator(timevalid);
  accuracy_le->setText("1.0e-8");
  
  QLabel *incrtimel = new QLabel(integrator_gb);
  incrtimel->setText("time increment:");
  time_incr_le = new QLineEdit(integrator_gb);
  time_incr_le->setValidator(timevalid);
  time_incr_le->setText("0.01");
  
  integratorcb->SetIntegrator(RA15);
  
  vlay->addWidget(integrator_gb);
  
  ////
  
  QHGroupBox *times_gb = new QHGroupBox("Integration parameters",this);
  
  switch (universe->GetUniverseType()) {
  case Real: { 
    
    QLabel *starttimel = new QLabel(times_gb);
    starttimel->setText("time start:");
    // real_time_start_pb = new QPushButton(times_gb);
    // connect(real_time_start_pb,SIGNAL(clicked()),this,SLOT(set_start_time()));
    start_time = new XOrsaDatePushButton(times_gb);
    
    QLabel *stoptimel = new QLabel(times_gb);
    stoptimel->setText("time stop:");
    // real_time_stop_pb = new QPushButton(times_gb);
    // connect(real_time_stop_pb,SIGNAL(clicked()),this,SLOT(set_stop_time()));
    stop_time = new XOrsaDatePushButton(times_gb);
    
    Date d;
    d.SetGregor(2000,1,1);
    // start_time = d;
    start_time->SetDate(d);
    d.SetGregor(2100,1,1);
    // stop_time  = d;
    stop_time->SetDate(d);
    
    // update_time_labels();
    
    break;
  }
  case Simulated: {
    
    QLabel *starttimel = new QLabel(times_gb);
    starttimel->setText("time start:");
    time_start_le = new QLineEdit(times_gb);
    time_start_le->setValidator(timevalid);
    time_start_le->setText("0.0");
    
    QLabel *stoptimel = new QLabel(times_gb);
    stoptimel->setText("time stop:");
    time_stop_le = new QLineEdit(times_gb);
    time_stop_le->setValidator(timevalid);
    time_stop_le->setText("1.0");
    break;
  }
  }
  
  QLabel *sample_periodl = new QLabel(times_gb);
  sample_periodl->setText("sample period:");
  sample_period_le = new QLineEdit(times_gb);
  sample_period_le->setValidator(timevalid);
  sample_period_le->setText("0.01");
  
  timecb = new TimeCombo(times_gb);
  timecb->SetUnit(units->GetTimeBaseUnit());
  
  vlay->addWidget(times_gb);
  
  //------//
  
  // update from evol, if possible
  
  if (evol) {
    namele->setText(evol->orsa::Evolution::name.c_str());
    if (evol->GetInteraction()) {
      rb_force_JPL->setChecked(evol->GetInteraction()->IsSkippingJPLPlanets());
      const Newton * newton = dynamic_cast <const Newton *> (evol->GetInteraction());
      if (newton) {
	rb_multipole->setChecked(newton->IsIncludingMultipoleMoments());
	rb_relativistic->setChecked(newton->IsIncludingRelativisticEffects());
	rb_fast_relativistic->setChecked(newton->IsIncludingFastRelativisticEffects());
      }
    }
    if (evol->GetIntegrator()) { 
      integratorcb->SetIntegrator(evol->GetIntegrator()->GetType());
      {
	QString sp;
	//
	if (accuracy_le->isEnabled()) {
	  sp.sprintf("%g",evol->GetIntegratorAccuracy());
	  accuracy_le->setText(sp);
	}
	//
	if (time_incr_le->isEnabled()) {
	  sp.sprintf("%g",evol->GetIntegratorTimeStep().GetDouble());
	  time_incr_le->setText(sp);
	}
      }
    }
    // this doesn't work because evol is empty...
    /* if (evol->size()) {
       (*start_time).SetTime((*evol)[0]);
       (*start_time).SetTime((*evol)[evol->size()-1]);
       }
    */
    {
      QString sp;
      sp.sprintf("%g",FromUnits(evol->GetSamplePeriod().GetDouble(),timecb->GetUnit(),-1));
      sample_period_le->setText(sp);
    }
  }
  
  //------//
  
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  
  hok->addStretch();
  //
  okpb = new QPushButton(this);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  cancpb = new QPushButton(this);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
  
}

void XOrsaNewIntegrationDialog::ok_pressed() {
  
  // not here!
  // hide();
  
  /* 
     QMessageBox::information(this,
     "integration starting...",
     "The data for the new integration\nis almost ready, wait please.",
     QMessageBox::Ok);
  */
  
  if (rb_force_JPL->isChecked()) {
    
    if ( ((*start_time) < jpl_file->EphemStart()) || 
	 ((*start_time) > jpl_file->EphemEnd())   || 
	 ((*stop_time)  < jpl_file->EphemStart()) ||
	 ((*stop_time)  > jpl_file->EphemEnd()) ) {
      
      QString warning_message;
      warning_message.sprintf("The integration start and stop time lie outside\n"
			      "the JPL Ephemeris file limit, while the check box\n"
			      "[Force JPL Ephemeris Data] is checked.");
      
      QMessageBox::warning(this,
			   "JPL Ephemeris file problem",
			   warning_message,
			   QMessageBox::Ok,
			   QMessageBox::NoButton);
      
      return;
    }
  }
  
  // Interaction * itr = 0;
  // make_new_interaction(&itr,interactioncb->GetInteraction());
  // make_new_interaction(&itr,NEWTON);
  Newton * itr = new Newton;
  //
  itr->IncludeMultipoleMoments(rb_multipole->isChecked());
  itr->IncludeRelativisticEffects(rb_relativistic->isChecked());
  itr->IncludeFastRelativisticEffects(rb_fast_relativistic->isChecked());
  itr->SkipJPLPlanets(rb_force_JPL->isChecked());
  
  Integrator * itg = 0;
  make_new_integrator(&itg,integratorcb->GetIntegrator());
  
  if ( (itr->depends_on_velocity()) && 
       (!itg->can_handle_velocity_dependant_interactions()) ) {
    
    QString warning_message;
    warning_message.sprintf("The interaction [%s]\n"
			    "cannot be handled by\n"
			    "the integrator [%s].\n"
			    "Please select another integrator.",
			    label(itr->GetType()).c_str(),label(itg->GetType()).c_str());
    
    QMessageBox::warning(this,
			 "interaction/integrator problem",
			 warning_message,
			 QMessageBox::Ok,
			 QMessageBox::NoButton);
    
    // cleanups
    delete itr;
    delete itg;    
    
    return;
  }
  
  hide();
  
  if (accuracy_le->isEnabled()) {
    itg->accuracy = accuracy_le->text().toDouble();
  }
  
  if (time_incr_le->isEnabled()) {
    itg->timestep = time_incr_le->text().toDouble();
  } else {
    // something better?
    itg->timestep = FromUnits(1,DAY);
  }
  
  evol->orsa::Evolution::name = namele->text().latin1();
  evol->SetSamplePeriod(FromUnits(sample_period_le->text().toDouble(),timecb->GetUnit()));
  evol->SetInteraction(itr);
  evol->SetIntegrator(itg);
  
  all_obj_info->GetBodies(evol->start_bodies, evol->start_JPL_bodies);
  Frame frame_start = StartFrame(evol->start_bodies, evol->start_JPL_bodies, itr, itg, *start_time);
  
  evol->push_back(frame_start);
  
  if (frame_start.size()) {
    IntegrationThread * it = 0;
    switch (universe->GetUniverseType()) {
    case Real: { 
      it = new IntegrationThread(evol,*stop_time);
      break;
    }
    case Simulated: {
      it = new IntegrationThread(evol,time_stop_le->text().toDouble());
      break;
    }
    }
    //
    // it->start();
    it->start(QThread::LowPriority); // the priority should be defined bu the user...
  }
  
  ok = true;
  done(0);
}

void XOrsaNewIntegrationDialog::cancel_pressed() {
  ok = false;
  done(0);
}

void XOrsaNewIntegrationDialog::closeEvent(QCloseEvent *e) {
  // should ask for a confirmation before...
  e->accept();
  ok = false;
  done(0);
}

void XOrsaNewIntegrationDialog::check_lines_integrator_enabled(int) {
  IntegratorType it = integratorcb->GetIntegrator();
  if (it==BULIRSCHSTOER || it==RA15) {
    accuracy_le->setEnabled(true);
    time_incr_le->setEnabled(false);
  } else {
    accuracy_le->setEnabled(false);
    time_incr_le->setEnabled(true);
  }
}

void XOrsaNewIntegrationDialog::uncheck_rb_relativistic() {
  rb_relativistic->setChecked(false);
}

void XOrsaNewIntegrationDialog::uncheck_rb_fast_relativistic() {
  rb_fast_relativistic->setChecked(false);
}
