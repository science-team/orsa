/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_integrator_combo.h"

using namespace orsa;

IntegratorCombo::IntegratorCombo(QWidget * parent) : QComboBox(false,parent) {
  
  insertItem(label(STOER).c_str());
  // insertItem(label(BULIRSCHSTOER).c_str());
  insertItem(label(RUNGEKUTTA).c_str());
  insertItem(label(DISSIPATIVERUNGEKUTTA).c_str());
  insertItem(label(RA15).c_str());
  insertItem(label(LEAPFROG).c_str());
  
  connect(this,SIGNAL(activated(int)),this,SLOT(SetIntegrator(int)));
  
  // sync!
  setCurrentItem(1);
  activated(1);
}

void IntegratorCombo::SetIntegrator(int i) {
  
  // look at the combobox for the right order
  switch (i) {
  case 0: t = STOER;                 break;
    // case 1: t = BULIRSCHSTOER;         break;
  case 1: t = RUNGEKUTTA;            break;
  case 2: t = DISSIPATIVERUNGEKUTTA; break;
  case 3: t = RA15;                  break;
  case 4: t = LEAPFROG;              break;
  }
  
}


void IntegratorCombo::SetIntegrator(IntegratorType it) {
  
  t = it;
  
  // look at the combobox for the right order
  switch (it) {
  case STOER:                 setCurrentItem(0); IntegratorChanged(0); break;
    // case BULIRSCHSTOER:         setCurrentItem(1); IntegratorChanged(1); break;
  case BULIRSCHSTOER: /**********************************************/ break;
  case RUNGEKUTTA:            setCurrentItem(1); IntegratorChanged(1); break;
  case DISSIPATIVERUNGEKUTTA: setCurrentItem(2); IntegratorChanged(2); break;
  case RA15:                  setCurrentItem(3); IntegratorChanged(3); break;
  case LEAPFROG:              setCurrentItem(4); IntegratorChanged(4); break;
  }  
}


IntegratorType IntegratorCombo::GetIntegrator() {
  return t;
}
