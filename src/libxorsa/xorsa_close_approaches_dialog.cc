/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_close_approaches_dialog.h"

#include <orsa_orbit_gsl.h>

#include "xorsa_date.h"
#include "xorsa_import_JPL_objects.h"
#include "xorsa_import_astorb_objects.h"
#include "xorsa_new_object_keplerian_dialog.h"

#include <qlabel.h>
#include <qlineedit.h> 
#include <qvalidator.h> 
#include <qgroupbox.h> 
#include <qlistview.h> 
#include <qcursor.h>

using namespace std;
using namespace orsa;

class XOrsaCloseApproachItem : public QListViewItem {
  
 public:
  XOrsaCloseApproachItem(QListView *parent, QString label1, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8) { }
  
  XOrsaCloseApproachItem(QListViewItem *parent, QString label1, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8) { }
  
 public:
  int compare(QListViewItem * i, int col, bool ascending) const {
    
    // epoch->double
    if (col == 1) {
      XOrsaCloseApproachItem * cait = dynamic_cast <XOrsaCloseApproachItem*> (i);
      if (cait) {
	const double d = close_approach.epoch.GetTime() - cait->close_approach.epoch.GetTime();
	if (d < 0.0) return (-1);
	if (d > 0.0) return (+1);
	return (0);
      }
    }
    
    // doubles
    if ((col == 2) || (col == 3)) {
      double d = (atof(key( col, ascending ).latin1()) - atof( i->key(col, ascending).latin1()));
      
      if (d < 0.0) return (-1);
      if (d > 0.0) return (+1);
      return (0);
    }
    
    // default
    return key( col, ascending ).compare( i->key(col, ascending));
  }
  
 public:
  BodyWithEpoch body;
  CloseApproach close_approach;
};


XOrsaCloseApproachesDialog::XOrsaCloseApproachesDialog(QWidget *parent) : QWidget(parent) {
  
  setCaption("close approaches");
  
  QDoubleValidator *positive_double_valid = new QDoubleValidator(this);
  positive_double_valid->setBottom(0.0);
  
  QGridLayout *grid_lay = new QGridLayout(this,7,2,3,3);
  
  // CATALOG OBJECTS
  QPushButton *pb_objects = new QPushButton("import objects from catalogs",this);
  connect(pb_objects,SIGNAL(clicked()),this,SLOT(slot_import_asteroids()));
  grid_lay->addWidget(pb_objects,0,0);
  
  // INSERT NEW OBJECT
  QPushButton *pb_insert = new QPushButton("insert new object (keplerian)",this);
  connect(pb_insert,SIGNAL(clicked()),this,SLOT(slot_new_keplerian()));
  grid_lay->addWidget(pb_insert,1,0);
  
  // DATES
  QWidget *w_dates = new QWidget(this);
  //
  QHBoxLayout *hlay_dates = new QHBoxLayout(w_dates,3,3);
  hlay_dates->setAutoAdd(true);
  //
  new QLabel("from:",w_dates);
  epoch_start = new XOrsaDatePushButton(w_dates);
  new QLabel("to:",w_dates);
  epoch_stop = new XOrsaDatePushButton(w_dates);
  //
  // set dates
  { 
    Date d;
    d.SetGregor(1990,1,1); epoch_start->SetDate(d);
    d.SetGregor(2050,1,1); epoch_stop->SetDate(d);
  }
  //
  grid_lay->addWidget(w_dates,2,0);
  
  // SAMPLE PERIOD
  QWidget *w_sample_period = new QWidget(this);
  //
  QHBoxLayout *hlay_sample_period = new QHBoxLayout(w_sample_period,3,3);
  hlay_sample_period->setAutoAdd(true);
  //
  new QLabel("sample period:",w_sample_period);
  le_sample_period = new QLineEdit("5.0",w_sample_period);
  le_sample_period->setAlignment(Qt::AlignRight);
  le_sample_period->setValidator(positive_double_valid);
  tc_sample_period = new TimeCombo(w_sample_period);
  tc_sample_period->SetUnit(DAY);
  //
  grid_lay->addWidget(w_sample_period,3,0);
  
  // THRESHOLD
  QWidget *w_threshold = new QWidget(this);
  //
  QHBoxLayout *hlay_threshold = new QHBoxLayout(w_threshold,3,3);
  hlay_threshold->setAutoAdd(true);
  //
  new QLabel("threshold:",w_threshold);
  le_threshold = new QLineEdit("0.2",w_threshold);
  le_threshold->setAlignment(Qt::AlignRight);
  le_threshold->setValidator(positive_double_valid);
  lc_threshold = new LengthCombo(w_threshold);
  lc_threshold->SetUnit(AU);
  //
  grid_lay->addWidget(w_threshold,4,0);
  
  // COMPUTE
  QPushButton *pb_compute = new QPushButton("compute",this);
  connect(pb_compute,SIGNAL(clicked()),this,SLOT(slot_compute()));
  grid_lay->addWidget(pb_compute,5,0);
  
  // PLANETS
  QGroupBox *gb_planets = new QGroupBox("planets",this);
  gb_planets->setColumns(1);
  jpl_planets_widget = new XOrsaJPLPlanetsWidget(gb_planets);
  grid_lay->addMultiCellWidget(gb_planets,0,5,1,1);
  
  listview = new QListView(this);
  listview->setAllColumnsShowFocus(true);
  listview->setShowSortIndicator(true);
  listview->setSorting(1,true);
  listview->setSelectionMode(QListView::Extended);
  listview->setRootIsDecorated(true);
  listview->setItemMargin(3);
  //
  listview->addColumn("name");
  //
  QString s_date;
  s_date.sprintf("date [%s]",TimeScaleLabel(universe->GetTimeScale()).c_str());
  listview->addColumn(s_date);
  //
  listview->addColumn("distance [AU]");
  listview->addColumn("relative velocity [km/s]");
  //
  grid_lay->addMultiCellWidget(listview,6,6,0,1);
  
}

void XOrsaCloseApproachesDialog::slot_import_asteroids() {
  
  // remove previous bodies
  bodies.clear(); 
  
  XOrsaImportAstorbObjectsAdvancedDialog *imp = new XOrsaImportAstorbObjectsAdvancedDialog(bodies,this);
  imp->show();
  imp->exec();
  
  if (imp->ok) {
    slot_update_listview();
  }
  
}

void XOrsaCloseApproachesDialog::slot_new_keplerian() {
  
  XOrsaNewObjectKeplerianDialog *nod = new XOrsaNewObjectKeplerianDialog(bodies,this);
  
  nod->show();
  nod->exec();
  
  if (nod->ok) {
    bodies.push_back(nod->GetBody());
    slot_update_listview();
  }
  
}

void XOrsaCloseApproachesDialog::slot_update_listview() {
  
  listview->clear();
  
  QString qs_name, qs_date, qs_distance, qs_relative_velocity;
  unsigned int k=0;
  while (k<bodies.size()) {
    
    qs_name = bodies[k].name().c_str();
    qs_date = "";
    qs_distance = "";
    qs_relative_velocity = "";
    
    XOrsaCloseApproachItem *cai = new XOrsaCloseApproachItem(listview,qs_name,qs_date,qs_distance,qs_relative_velocity);
    cai->body = bodies[k];
    
    ++k;
  }
}

void XOrsaCloseApproachesDialog::slot_compute() {
  
  QCursor cursor(Qt::WaitCursor);
  setCursor(cursor);
  
  const double distance_threshold = FromUnits(le_threshold->text().toDouble(),lc_threshold->GetUnit());
  const double sample_period      = FromUnits(le_sample_period->text().toDouble(),tc_sample_period->GetUnit());
  
  Frame f;
  vector<CloseApproach> clapp;
  XOrsaCloseApproachItem *cai;
  unsigned int k;
  QString qs_name, qs_date, qs_distance, qs_relative_velocity;
  QListViewItemIterator it = listview->firstChild();
  while (it.current() != 0) {
    cai = dynamic_cast <XOrsaCloseApproachItem*> (it.current());
    if (cai) {
      if (cai->depth() == 0) {
	// remove old close approaches
	{
	  QListViewItem *child = cai->firstChild();
	  while (child) {
	    delete child;
	    child = cai->firstChild();
	  }
	}
	f.clear();
	f.SetTime(cai->body.Epoch().GetTime());
	jpl_planets_widget->AddSelectedPlanets(cai->body.Epoch(),f);
	f.push_back(cai->body);
	clapp.clear();
	{
#warning "FIXME: CODE NEEDED HERE!!"
	  ORSA_ERROR("FIXME: code update needed here!");
	  // ComputeCloseApproaches(f,f.size()-1,*epoch_start,*epoch_stop,clapp,distance_threshold,sample_period);
	}
	if (clapp.size()) {
	  k=0;
	  while (k<clapp.size()) {
	    qs_name = clapp[k].name.c_str();
	    FineDate(qs_date,clapp[k].epoch);
	    qs_distance.sprintf("%g",FromUnits(clapp[k].distance,AU,-1));
	    qs_relative_velocity.sprintf("%g",FromUnits(FromUnits(clapp[k].relative_velocity,KM,-1),SECOND));
	    XOrsaCloseApproachItem *cai_new = new XOrsaCloseApproachItem(cai,qs_name,qs_date,qs_distance,qs_relative_velocity);
	    cai_new->body=cai->body;
	    cai_new->close_approach=clapp[k]; 
	    ++k;
	  }
	  listview->triggerUpdate();
	}
      }
    }
    ++it;
  }
  
  unsetCursor();
}
