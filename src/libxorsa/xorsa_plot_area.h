/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_PLOT_AREA_H_
#define _XORSA_PLOT_AREA_H_

#include <vector>

#include <qobject.h> 
#include <qwidget.h>

#include <cmath>

#include <orsa_universe.h>
#include <orsa_secure_math.h>
#include <orsa_units.h>

class QPoint;
class QPixmap;
class QPaintDevice;
class QColor;
class QString;
class QVBoxLayout;
class QCheckBox;
class QPrinter;
class QPushButton;

inline void FineLabel(QString &label, const double &x) {
  using std::pow;
  using std::floor;
  using std::fabs;
  label.sprintf("%g",x);
  if (label.contains('e',false)>0) {
    label.sprintf("%gx10<sup>%i</sup>",x/pow(10.0,floor(orsa::secure_log10(fabs(x)))),(int)(floor(orsa::secure_log10(fabs(x)))));
    // label.sprintf("%gx10^%i",x/pow(10.0,floor(secure_log10(fabs(x)))),(int)(floor(secure_log10(fabs(x)))));
  }
}

extern char *MonthNameShort[13]; // sdncal.h

void FineDate(QString & label, const orsa::UniverseTypeAwareTime & t, bool=true);
void FineDate_HMS(QString & label, const orsa::UniverseTypeAwareTime & t);

orsa::length_unit AutoLengthUnit(const double x);
orsa::time_unit   AutoTimeUnit(const double x); 
orsa::mass_unit   AutoMassUnit(const double x); 

enum XOrsaPlotAxisOrientation {HORIZONTAL,VERTICAL};

class XOrsaPlotPoint {
 public:
  double x;
  double y;
};

bool operator == (const XOrsaPlotPoint &a, const XOrsaPlotPoint &b);

class XOrsaPlotCurve : public std::vector<XOrsaPlotPoint> {
 public:
  XOrsaPlotCurve() : index(0), color(Qt::black) { };
 public:
  int index;
  QColor       color;
};

enum XOrsaPlotAxisType { AT_GENERIC,
			 AT_ANGLE,
			 AT_TIME,
			 AT_DATE,
			 AT_LENGTH };

enum XOrsaPlotAxisDateStepUnit { DS_YEAR,
				 DS_MONTH,
				 DS_DAY,
				 DS_FRAC_DAY };

class XOrsaPlotAxisLabel {
 public:
  QString label;
  double position;
};

class XOrsaPlotAxis : public QObject {
  
  Q_OBJECT
  
 public:
  XOrsaPlotAxis(XOrsaPlotAxisOrientation o) : QObject(), orientation(o) {
    init_variables();
  };
  
 signals:
  void RangeChanged();
  void PixelLengthChanged();
  void LogScaleChanged(bool);
  void TicksChanged();
  void TypeChanged();
  
 public:
  inline void SetMinMax(const double x, const double y) {
    if (x==y) return;
    if (x<y) {
      min=x;
      max=y;
    } else {
      min=y;
      max=x;
    }
    range=max-min;
    emit RangeChanged();
    // check if log scale is still valid
    if (min<=0) SetLogScale(false);
  };
  
  inline void SetRange(const double x, const double y) {
    SetMinMax(x,y);
  }
  
  inline void SetPixelLength(const double pl, bool emit_signal=true) {
    pixel_length = pl;
    if (emit_signal) emit PixelLengthChanged();
  };
  
  inline void SetLogScale(bool b) {
    if (b==false) {
      is_log_scale=b;
      emit LogScaleChanged(b);
    } else { // b==true
      if ((min>0) && (max>0) && (type!=AT_DATE)) {
	is_log_scale=b;
	emit LogScaleChanged(b);
      }
    }
  };
  
  inline void SetMajorTicks(int n) {
    num_major_ticks = n;
    emit TicksChanged();
  };
  
  inline void SetMinorTicks(int n) {
    num_minor_ticks = n;
    emit TicksChanged();
  };
  
  inline double GetMin() const { return min; };
  inline double GetMax() const { return max; };
  
  inline double GetRange() const { return range; };
  
  inline double GetPixelLength() const { return pixel_length; };
  
  inline bool   IsLogScale() const { return is_log_scale; };
  
  inline XOrsaPlotAxisOrientation GetOrientation() const { return orientation; };
  
  int GetMajorTicks() const { return num_major_ticks; };
  int GetMinorTicks() const { return num_minor_ticks; };
    
  inline void SetType(const XOrsaPlotAxisType t) {
    if (t != type) {
      type = t;
      if (t==AT_DATE) SetLogScale(false);
      emit TypeChanged();
    }
  };
  
  inline XOrsaPlotAxisType GetType() const {
    return type;
  };
  
 private:
  void init_variables() {
    min=0.0;
    max=1.0;
    range=max-min;
    pixel_length=1.0;
    is_log_scale=false; 
    num_major_ticks=11;
    num_minor_ticks=1;
    type = AT_GENERIC;
  };
  
 public:
  double center, step;
  XOrsaPlotAxisDateStepUnit date_step;
  std::vector<XOrsaPlotAxisLabel> labels;
  
 private:
  double min,max,range;
  double pixel_length;
  bool   is_log_scale;
  const  XOrsaPlotAxisOrientation orientation;
  int    num_major_ticks,num_minor_ticks;
  XOrsaPlotAxisType type;
};

enum BORDER {TOP,BOTTOM,LEFT,RIGHT};

class XOrsaPlotArea : public QWidget {
  
  Q_OBJECT
  
 public:
  XOrsaPlotArea(QWidget *parent=0);
  XOrsaPlotArea(int w, int h, QWidget *parent=0);
  
  ~XOrsaPlotArea();
  
 public:
  double x(const QPoint);
  double y(const QPoint);
  double x(const int);
  double y(const int);
  QPoint p(const XOrsaPlotPoint);
  QPoint p(const double x, const double y);
  bool   isRegularQPoint(const QPoint, const int, const int);
  bool   isRegularQPoint(const int x, const int y, const int, const int);
  bool   isInsideBorder(const QPoint, const int, const int);
  bool   isInsideBorder(const int x, const int y, const int, const int);
  
  public slots:
  void SetConnectPoints(bool);
  void SetSameScale(bool);
  // void SetLogAxis(XOrsaPlotAxis&,bool); // obsolete
  void SetAxisRange(XOrsaPlotAxis&,const double, const double);
  void SetBothAxisRange(const double, const double, const double, const double);
  void SetData(std::vector<XOrsaPlotCurve> *curves_in, bool autoscale=true);
  inline void SetStack(bool b) { bool_stack = b; };
  //
  inline void SetFixXRange(bool b) { bool_fix_x_range = b; };
  inline void SetFixYRange(bool b) { bool_fix_y_range = b; };
  //
  void SetTitle(QString);
  //
  void PrintArea();
  //
  void SaveData();

 signals:
  void mouse_moved(QMouseEvent*);
  void ConnectChanged(bool);
  void SameScaleChanged(bool);
  // void LogAxisChanged(XOrsaPlotAxis&,bool);
  void BorderChanged(BORDER);
  
 private:
  bool bool_connect_points;
  bool same_scale_x_y;
  // bool log_x_axis;
  // bool log_y_axis;
  bool bool_stack;  
  bool bool_fix_x_range, bool_fix_y_range;
  
  bool zooming;
  
 private:
  void Init();
  void Init_signals_slots();
  
  private slots:
  void paintEvent(QPaintEvent*); 
  void resizeEvent(QResizeEvent*);
  // void SetDefaultRanges();
  void ComputeLimits();
  void ComputeOriginPosition(QPaintDevice*);
  void DrawArea(QPainter*, QPaintEvent* =0);
  void DrawAxis(QPainter*, QPaintEvent* =0);
  void DrawTicksAndLabels(const XOrsaPlotAxis&, QPainter*);
  bool TicksAndLabelsComputations(XOrsaPlotAxis&, QPainter*);
  bool TicksAndLabelsComputations_DATE(XOrsaPlotAxis&, QPainter*);
  void paintPixmap();
  void mouseMoveEvent(QMouseEvent*);
  void mouseReleaseEvent(QMouseEvent*);
  void mousePressEvent(QMouseEvent*);
  void WriteLabel(QString&,const double&, const XOrsaPlotAxis&);
  //
  void SetBorder(BORDER,int);
  //
  void slot_axis_range_changed();
  void slot_pixel_length_changed();
  void slot_log_scale_changed();
  void slot_ticks_changed();
  void slot_type_changed();
  void slot_borders_changed(BORDER);
  
 private:
  void update_font_size(QPainter*);
  
 private:
  inline void pixmap_needs_update() { bool_pixmap_needs_update=true; };
  
 private:
  QPoint orig;
  XOrsaPlotPoint orig_pp;
  // int    border;
  int top_border,bottom_border,left_border,right_border;
  // double x_min, x_max, y_min, y_max;
  // double x_range, y_range;
  // double x_pixel_length, y_pixel_length;
 public:
  XOrsaPlotAxis X,Y;
  
 private:
  QPixmap  *pixmap, *tmp_pixmap;
  bool bool_pixmap_needs_update;
  QPrinter *printer;
  double printer_font_scale;
  std::vector<XOrsaPlotCurve> *curves;
  
  // qprinter or the XOrsaPlotArea QWidget (via QPixmap...);
  QPaintDevice *active_paint_device;
  
  QPoint zoom_start, zoom_stop, mouse_pos;

  QString title;
};


class XOrsaExtendedPlotArea : public QWidget {
  
  Q_OBJECT
  
 public:
  XOrsaExtendedPlotArea(QWidget *parent=0);
  XOrsaExtendedPlotArea(int w, int h, QWidget *parent=0);
  
  ~XOrsaExtendedPlotArea();
  
  protected slots:
    void syncLogCheckBox(bool);
  void TryLogX(bool);
  void TryLogY(bool);
  
 public:
  XOrsaPlotArea *area;
  
 private:
  void Init(QVBoxLayout *);
 
 private:
  QCheckBox *log_x, *log_y;
  QCheckBox *stack;
  QCheckBox *cb_x_range, *cb_y_range;
  // QPushButton *print_pb;
  // QPushButton *save_data_pb;
};

#endif // _XORSA_PLOT_AREA_H_
