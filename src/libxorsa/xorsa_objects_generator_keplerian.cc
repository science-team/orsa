/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_objects_generator_keplerian.h"
#include "xorsa_import_JPL_objects.h"

#include <qlayout.h>
#include <qpushbutton.h> 
#include <qvalidator.h> 
#include <qlabel.h> 
#include <qframe.h>
#include <qbuttongroup.h> 
#include <qgroupbox.h> 
#include <qlineedit.h> 
#include <qspinbox.h> 

#include <orsa_orbit.h>

#include <gsl/gsl_rng.h> // random number generator

using namespace std;
using namespace orsa;

XOrsaObjectsGeneratorKeplerian::XOrsaObjectsGeneratorKeplerian(vector<BodyWithEpoch> &b, QWidget *parent) : QDialog(parent,0,true), bodies(b) {
  
  bodies_for_combo.resize(bodies.size());
  unsigned int k=0;
  while (k < bodies.size()) {
    bodies_for_combo[k] = bodies[k];
    ++k;
  }
  
  init_draw();
  
  setCaption("generate objects");
}

void XOrsaObjectsGeneratorKeplerian::init_draw() {
  
  QVBoxLayout *vlay = new QVBoxLayout(this,4);
  
  // name and mass
  QGroupBox *name_mass_gb = new QGroupBox("name and mass",this);
  name_mass_gb->setColumns(1); // IMPORTANT: number of columns
  //
  QWidget *gw = new QWidget(name_mass_gb);
  QGridLayout *grid_lay = new QGridLayout(gw,2,3,3,3);
  
  grid_lay->addWidget(new QLabel("name",gw),0,0);
  le_name = new QLineEdit(gw);
  grid_lay->addMultiCellWidget(le_name,0,0,1,2);
  //
  switch (universe->GetUniverseType()) {
  case Real: { 
    name_mass_gb->setTitle("name");
    break;
  } 
  case Simulated: {
    
    grid_lay->addWidget(new QLabel("mass range",gw),1,0);
    le_mass_m = new QLineEdit(gw);
    grid_lay->addWidget(le_mass_m,1,1);
    le_mass_M = new QLineEdit(gw);
    grid_lay->addWidget(le_mass_M,1,2);
    grid_lay->addWidget(new QLabel("units",gw),2,0,Qt::AlignRight);
    masscb = new MassCombo(gw);
    masscb->SetUnit(units->GetMassBaseUnit());
    grid_lay->addMultiCellWidget(masscb,2,2,1,2);
    break;
  }
  }  
  //
  vlay->addWidget(name_mass_gb);
  
  // keplerian
  QGroupBox *keplerian_gb = new QGroupBox("keplerian",this);
  keplerian_gb->setColumns(1);
  
  QGroupBox *misc_gb = new QGroupBox(keplerian_gb);
  misc_gb->setColumns(2);
  
  switch (universe->GetUniverseType()) {
  case Real: { 
    new QLabel("ref. body",misc_gb);
    // new QLabel("Sun",misc_gb);
    jpl_planets_combo = new XOrsaJPLPlanetsCombo(misc_gb);
    break;
  }
  case Simulated: {
    new QLabel("ref. body",misc_gb);
    ref_body_combo = new XOrsaImprovedObjectsCombo(&bodies_for_combo,true,misc_gb);
    break;
  }
  }
  
  if (universe->GetUniverseType() == Real) {
    new QLabel("epoch",misc_gb);
    epoch = new XOrsaDatePushButton(misc_gb);
  }
  
  keplerian_elements_gb = new QButtonGroup(keplerian_gb);
  keplerian_elements_gb->setRadioButtonExclusive(true);
  keplerian_elements_gb->setColumns(3);
  // a
  new QLabel("a",keplerian_elements_gb);
  // QHBox *auehb = new QHBox(keplerian_elements_gb);
  // auehb->setSpacing(3);
  // le_ka_m = new QLineEdit(auehb);
  le_ka_m = new QLineEdit(keplerian_elements_gb);
  QHBox *auehb = new QHBox(keplerian_elements_gb);
  auehb->setSpacing(3);
  le_ka_M = new QLineEdit(auehb);
  spacecb = new LengthCombo(auehb);
  spacecb->SetUnit(units->GetLengthBaseUnit());
  // e
  new QLabel("e",keplerian_elements_gb);
  le_ke_m = new QLineEdit(keplerian_elements_gb);  
  le_ke_M = new QLineEdit(keplerian_elements_gb);  
  // i
  new QLabel("i",keplerian_elements_gb);
  le_ki_m = new QLineEdit(keplerian_elements_gb);  
  le_ki_M = new QLineEdit(keplerian_elements_gb);  
  // node
  QChar   *uc_Omega = new QChar(0x03A9);
  QString suc_Omega(uc_Omega,1);    
  new QLabel(suc_Omega,keplerian_elements_gb);
  le_knode_m = new QLineEdit(keplerian_elements_gb); 
  le_knode_M = new QLineEdit(keplerian_elements_gb); 
  // peri
  QChar   *uc_omega = new QChar(0x03C9);
  QString suc_omega(uc_omega,1);    
  new QLabel(suc_omega,keplerian_elements_gb);
  le_kperi_m = new QLineEdit(keplerian_elements_gb);
  le_kperi_M = new QLineEdit(keplerian_elements_gb);
  // M
  new QLabel("M",keplerian_elements_gb);
  le_km_m = new QLineEdit(keplerian_elements_gb);  
  le_km_M = new QLineEdit(keplerian_elements_gb);  
  //
  vlay->addWidget(keplerian_gb);
  
  // validation
  QDoubleValidator *vd = new QDoubleValidator(this);
  QDoubleValidator *vdz = new QDoubleValidator(this);
  vdz->setBottom(0.0);
  // le_mass->setValidator(vdz);
  if (universe->GetUniverseType() == Simulated) {
    le_mass_m->setValidator(vdz);
    le_mass_M->setValidator(vdz);
  }
  
  // keplers
  le_ka_m->setValidator(vdz);
  le_ke_m->setValidator(vdz);
  le_ki_m->setValidator(vdz);
  //
  le_knode_m->setValidator(vd);
  le_kperi_m->setValidator(vd);
  le_km_m->setValidator(vd);
  
  le_ka_M->setValidator(vdz);
  le_ke_M->setValidator(vdz);
  le_ki_M->setValidator(vdz);
  //
  le_knode_M->setValidator(vd);
  le_kperi_M->setValidator(vd);
  le_km_M->setValidator(vd);
  
  le_ka_m->setText("0");
  le_ke_m->setText("0");
  le_ki_m->setText("0");
  le_knode_m->setText("0");
  le_kperi_m->setText("0");
  le_km_m->setText("0");
   
  le_ka_M->setText("0");
  le_ke_M->setText("0");
  le_ki_M->setText("0");
  le_knode_M->setText("0");
  le_kperi_M->setText("0");
  le_km_M->setText("0");
  
  QHBox *num_box = new QHBox(this);
  new QLabel("objects to generate",num_box);
  sb_num = new QSpinBox(1,10000,1,num_box); // you really have to generate more objects? edit here!
  vlay->addWidget(num_box);
  
  // push buttons
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  
  hok->addStretch();
  //
  okpb = new QPushButton(this);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  cancpb = new QPushButton(this);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
}

void XOrsaObjectsGeneratorKeplerian::ok_pressed() {
  
  // units
  length_unit lu = spacecb->GetUnit();
  
  const double a_m                = FromUnits(le_ka_m->text().toDouble(),lu);
  const double e_m                = le_ke_m->text().toDouble();
  const double i_m                = (pi/180)*le_ki_m->text().toDouble();
  const double omega_node_m       = (pi/180)*le_knode_m->text().toDouble();
  const double omega_pericenter_m = (pi/180)*le_kperi_m->text().toDouble();
  const double M_m                = (pi/180)*le_km_m->text().toDouble();
  //
  const double a_M                = FromUnits(le_ka_M->text().toDouble(),lu);
  const double e_M                = le_ke_M->text().toDouble();
  const double i_M                = (pi/180)*le_ki_M->text().toDouble();
  const double omega_node_M       = (pi/180)*le_knode_M->text().toDouble();
  const double omega_pericenter_M = (pi/180)*le_kperi_M->text().toDouble();
  const double M_M                = (pi/180)*le_km_M->text().toDouble();
  
  double mass_m=0, mass_M=0;
  
  switch (universe->GetUniverseType()) {
  case Real: { 
    mass_m = mass_M = 0.0;
    break;
  }
  case Simulated: {
    mass_unit mu = masscb->GetUnit();
    mass_m = FromUnits(le_mass_m->text().toDouble(),mu);
    mass_M = FromUnits(le_mass_M->text().toDouble(),mu);
    break;
  }
  }
  
  QString s_name,s_num;
  
  QString base_name = le_name->text().simplifyWhiteSpace().latin1();
  string b_name;
  double b_mass;
  Vector r,v;
  
  // random number generator
  const int random_seed = 124323; // should be user-defined...
  gsl_rng *rnd;
  rnd = gsl_rng_alloc(gsl_rng_gfsr4);
  gsl_rng_set(rnd,random_seed);
  
  JPLBody *jb=0;
  if (universe->GetUniverseType() == Real) {
    jb = new JPLBody(jpl_planets_combo->GetPlanet(),*epoch);
  }
  
  Orbit orbit;
  
  switch (universe->GetUniverseType()) {
  case Real: { 
    // orbit.mu = GetG()*(b.mass() + jb->mass());
    orbit.mu = GetG()*jb->mass();
    break;
  }
  case Simulated: {
    // orbit.mu can be different for each object...
    break;
  }
  }
  
  int k;
  for (k=0;k<sb_num->value();k++) {
    
    s_num.sprintf("%i",k);
    s_name = base_name + s_num;
    b_name = s_name.latin1();
    
    b_mass = mass_m+(mass_M-mass_m)*gsl_rng_uniform(rnd);
    
    orbit.a                = a_m+(a_M-a_m)*gsl_rng_uniform(rnd);
    orbit.e                = e_m+(e_M-e_m)*gsl_rng_uniform(rnd);
    orbit.i                = i_m+(i_M-i_m)*gsl_rng_uniform(rnd);
    orbit.omega_node       = omega_node_m+(omega_node_M-omega_node_m)*gsl_rng_uniform(rnd);
    orbit.omega_pericenter = omega_pericenter_m+(omega_pericenter_M-omega_pericenter_m)*gsl_rng_uniform(rnd);
    orbit.M                = M_m+(M_M-M_m)*gsl_rng_uniform(rnd);
    
    switch (universe->GetUniverseType()) {
    case Real: {
      // orbit.mu = GetG()*(b.mass() + jb->mass());
      orbit.RelativePosVel(r,v);
      r += jb->position();
      v += jb->velocity();
      //
      // bodies.push_back(BodyWithEpoch(b_name,b_mass,r,v,*epoch));
      bodies.push_back(BodyWithEpoch(b_name,0.0,r,v,*epoch));
      break;
    } 
    case Simulated: {
      // kepler_ref_body = ref_body_combo->GetBody();
      orbit.mu = GetG()*(b_mass + ref_body_combo->GetBody().mass());
      orbit.RelativePosVel(r,v);
      r += ref_body_combo->GetBody().position();
      v += ref_body_combo->GetBody().velocity();
      //
      bodies.push_back(BodyWithEpoch(b_name,b_mass,r,v,0.0));
      break;
    }
    }
    
    // bodies.push_back(BodyWithEpoch(b_name,b_mass,r,v,*epoch));
  }
  
  if (universe->GetUniverseType() == Real) {
    if (jb) delete jb;
  }
  
  // free random number generator
  gsl_rng_free(rnd);
  
  done(0);
}

void XOrsaObjectsGeneratorKeplerian::cancel_pressed() {
  done(0);
}
