/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_object_selector.h"

#include <qlayout.h>
#include <qpushbutton.h> 
#include <qlistview.h>

using namespace std;
using namespace orsa;

class XOrsaObjectItem : public QListViewItem {
  
  // DON'T use Q_OBJECT for this class
  // Q_OBJECT
  
 public:
  XOrsaObjectItem(QListView *parent, QString label1, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null);
  
 public:
  int XOrsaObjectItem::compare(QListViewItem * i, int col, bool ascending) const;    
  
};

XOrsaObjectItem::XOrsaObjectItem(QListView *parent, QString label1, QString label2, QString label3, QString label4, QString label5, QString label6, QString label7, QString label8) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8) {
  
}

int XOrsaObjectItem::compare ( QListViewItem * i, int col, bool ascending ) const {
  
  // integers
  // if (col == 0) return (atoi(key( col, ascending ).latin1()) - atoi( i->key(col, ascending).latin1()));
  
  // doubles
  // if (col == 2 || col == 3 || col == 4) {
  if (col == 1) {
    double d = (atof(key( col, ascending ).latin1()) - atof( i->key(col, ascending).latin1()));
    
    if (d < 0.0) return (-1);
    if (d > 0.0) return (+1);
    return (0);
  }
  
  // test!
  if (col == 2 || col == 3) {
    double r, ri;
    char c;
    //
    sscanf(   key(col, ascending).latin1(),"%c=%lf",&c,&r);
    sscanf(i->key(col, ascending).latin1(),"%c=%lf",&c,&ri);
    //
    if (r < ri) return (-1);
    if (r > ri) return (+1);
    return 0;
  }
  
  
  // no sort
  /*  if (col == 2 || col == 3) {
      return 0;
      }
  */
  
  // default
  return key( col, ascending ).compare( i->key(col, ascending));
}


XOrsaObjectSelector::XOrsaObjectSelector(vector<BodyWithEpoch> &list_in, bool only_massive, QWidget *parent) : QDialog(parent,0,true), list(list_in), only_massive_objects(only_massive) {
  common_init();
}

void XOrsaObjectSelector::common_init() {
  
  if (only_massive_objects) {
    setCaption("massive object selector");
  } else {
    setCaption("object selector");
  }
  
  QVBoxLayout *vlay = new QVBoxLayout(this,4);
  
  listview = new QListView(this);
  //
  listview->setAllColumnsShowFocus(true);
  listview->setShowSortIndicator(true);
  listview->setSelectionMode(QListView::Single);
  listview->setItemMargin(3); // in pixels
  // listview->setResizeMode(QListView::LastColumn);
  //
  listview->addColumn("name");
  listview->addColumn("mass");
  listview->addColumn("position");
  listview->addColumn("velocity");
  //
  listview->setSorting(1,false);
  
  vlay->addWidget(listview);
  
  ////
  
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  
  hok->addStretch();
  //
  okpb = new QPushButton(this);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  cancpb = new QPushButton(this);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
  
  connect(listview,SIGNAL(selectionChanged()),this,SLOT(slot_enable_ok_button()));
  
  fill_listview();
  
  slot_enable_ok_button();
  
}


void XOrsaObjectSelector::fill_listview() {
  
  listview->clear();  
  
  if (list.size() > 0) {
    
    QString m,p,v;
    
    std::vector<BodyWithEpoch>::iterator itr;
    itr = list.begin();
    //
    while (itr != list.end()) {
      
      if (only_massive_objects && ((*itr).mass() == 0)) {
	itr++;
	continue;
      }
      
      m.sprintf("%.3e",(*itr).mass());
      p.sprintf("r=%.3e (%.1f;%.1f;%.1f)",(*itr).position().Length(),(*itr).position().x,(*itr).position().y,(*itr).position().z);
      v.sprintf("v=%.3e (%.1f;%.1f;%.1f)",(*itr).velocity().Length(),(*itr).velocity().x,(*itr).velocity().y,(*itr).velocity().z);
      
      body_map_it[new XOrsaObjectItem(listview,(*itr).name().c_str(),m,p,v)] = itr;
      
      itr++;
    }     
  }
}


void XOrsaObjectSelector::ok_pressed() {
  ok = true;
  if (list.size() != 0) {
    QListViewItemIterator it = listview->firstChild();
    while (it.current() != 0) {
      if (it.current()->isSelected()) {
	body_selected = ((*body_map_it[it.current()]));
      }
      it++;
    }
  }    
  done(0);
}


void XOrsaObjectSelector::cancel_pressed() {
  ok = false;  
  done(0);
}


void XOrsaObjectSelector::slot_enable_ok_button() {
  bool ok_enabled = false;
  if (list.size() != 0) {
    QListViewItemIterator it = listview->firstChild();
    while ((it.current() != 0) && (ok_enabled == false)) {
      if (it.current()->isSelected()) ok_enabled = true;
      it++;
    }
  }    
  okpb->setEnabled(ok_enabled);
}
