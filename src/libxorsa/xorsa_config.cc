/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_config.h"

#include <orsa_file.h>

#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qgrid.h>
#include <qvbox.h>
#include <qfiledialog.h>

using namespace std;

using namespace orsa;

// XOrsaFileEntry

XOrsaFileEntry::XOrsaFileEntry(const XOrsaFileEntryMode m, QWidget *parent) : QWidget(parent), mode(m) {
  
  QHBoxLayout *hlay = new QHBoxLayout(this);
  hlay->setSpacing(3);
  hlay->setAutoAdd(true);
  
  le = new QLineEdit(this);
  le->setMinimumWidth(60);
  
  pb = new QPushButton("browse",this);
  
  pb->setAutoDefault(false);
  
  connect(pb,SIGNAL(clicked()),this,SLOT(browse()));
  connect(le,SIGNAL(textChanged(const QString&)),this,SIGNAL(textChanged(const QString&)));
}

void XOrsaFileEntry::browse() {
  
  QString s;
  switch(mode) {
  case FE_OPEN_FILE:
    s =  QFileDialog::getOpenFileName(le->text(),
				      QString::null,
				      this,
				      QString::null,
				      "select file");
    break;
  case FE_SAVE_FILE:
    s =  QFileDialog::getSaveFileName(le->text(),
				      QString::null,
				      this,
				      QString::null,
				      "select file");
    break;
  }
  
  if (!s.isEmpty()) {
    le->setText(s);
  }
}

void XOrsaFileEntry::setText(const QString &text) {
  le->setText(text);
}

QString XOrsaFileEntry::text() const {
  return (le->text());
}

// XOrsaConfig

XOrsaConfig::XOrsaConfig(const list<ConfigEnum> le, QWidget * parent) : QTabDialog(parent,0,true), list_enum(le) {
  
  setCancelButton();
  
  connect(this,SIGNAL(applyButtonPressed()),this,SLOT(save()));
  
  config->read_from_file();
  
  paths_w = new QScrollView();
  paths_w->setResizePolicy(QScrollView::AutoOneFit);
  //
  draw_paths_w();
  addTab(paths_w,"Paths");
  
  resize(700,500);
}

void XOrsaConfig::save() {
  
  list<ConfigEnum>::const_iterator it = list_enum.begin();
  while (it != list_enum.end()) {
    save_paths((*it));
    ++it;
  }
  
  config->write_to_file();
}

void XOrsaConfig::save_paths(ConfigEnum ci) {
  ConfigItem<string> * co = config->paths[ci];
  co->SetValue(map_paths[ci]->text().latin1());
}

void XOrsaConfig::draw_paths_w() {
  
  QGrid * grid = new QGrid(2,Qt::Horizontal,paths_w->viewport());
  paths_w->addChild(grid);
  grid->setMargin(3); 
  grid->setSpacing(3); 
  
  list<ConfigEnum>::const_iterator it = list_enum.begin();
  while (it != list_enum.end()) {
    draw_paths_w_util(grid,(*it));
    ++it;
  }
}

void XOrsaConfig::draw_paths_w_util(QWidget *parent_grid, ConfigEnum ci) {
  
  ConfigItem<string> * co = config->paths[ci];
  
  new QLabel(Label(ci).c_str(),parent_grid);
  XOrsaFileEntry * fe = new XOrsaFileEntry(FE_OPEN_FILE,parent_grid);
  fe->setText(co->GetValue().c_str());
  
  map_paths[ci] = fe;
}
