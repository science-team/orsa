/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_NEW_UNIVERSE_DIALOG_H_
#define _XORSA_NEW_UNIVERSE_DIALOG_H_

#include <qdialog.h>

#include "xorsa_units_combo.h"

class QLineEdit;
class QLabel;
class QTextEdit;
class QPushButton;
class QGroupBox;

#include <orsa_universe.h>

class UniverseTypeCombo;
class ReferenceSystemCombo;

class XOrsaNewUniverseDialog : public QDialog {
  
  Q_OBJECT
    
 public:
  //! read_only = false means that when OK is presses a new universe is created and all the data is erased
  //! read_only = true means that only the name and the description of the universe can be changed
  // XOrsaNewUniverseDialog(Universe*, bool read_only_in=false, QWidget *parent=0);
  XOrsaNewUniverseDialog(bool read_only_in=false, QWidget *parent=0);
  
 private slots:
  void ok_pressed();
  void cancel_pressed();
  void widgets_enabler();
  
 private:
  QLineEdit *le_name;
  QTextEdit *te_description;
  QPushButton *okpb;
  QPushButton *cancpb; 
  
 public:
  bool ok; // wheter the ok button has been pressed
  
 private:
  TimeCombo   *timecb;
  LengthCombo *spacecb;
  MassCombo   *masscb;
  //
  UniverseTypeCombo *utcb;
  ReferenceSystemCombo *recb;
  TimeScaleCombo *tscb;
  
  QGroupBox *units_gb;
  QGroupBox *utrs_gb;   // universe type and reference system
  
 signals:
  void closing_universe();
  
 private:
  // Universe *local_universe;
  const bool read_only;
};

class UniverseTypeCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  UniverseTypeCombo(QWidget *parent=0);
  
 private slots:
  void SetUniverseType(int);
  
 public slots:
  void SetUniverseType(orsa::UniverseType);
  
 public:  
  orsa::UniverseType GetUniverseType();
  
 private:
  orsa::UniverseType ut;
};

class ReferenceSystemCombo : public QComboBox {
  
  Q_OBJECT
  
 public:
  ReferenceSystemCombo(QWidget *parent=0);
  
 private slots:
  void SetReferenceSystem(int);
  
 public slots:
  void SetReferenceSystem(orsa::ReferenceSystem);
  
 public:  
  orsa::ReferenceSystem GetReferenceSystem();
  
 private:
  orsa::ReferenceSystem rs;
};

#endif // _XORSA_NEW_UNIVERSE_DIALOG_H_
