/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_UNITS_CONVERTER_H_
#define _XORSA_UNITS_CONVERTER_H_

#include <xorsa_units_combo.h>

#include <qwidget.h>

class QLineEdit;

class LengthConverter : public QWidget {

  Q_OBJECT

 public:
  LengthConverter(QWidget * parent = 0);
  
 private:
  QLineEdit    *line_from,  *line_to;
  LengthCombo *combo_from, *combo_to;
  
 private slots:
  void update();
};

class MassConverter : public QWidget {

  Q_OBJECT

 public:
  MassConverter(QWidget * parent = 0);
  
 private:
  QLineEdit    *line_from,  *line_to;
  MassCombo *combo_from, *combo_to;
  
 private slots:
  void update();
};

class TimeConverter : public QWidget {

  Q_OBJECT

 public:
  TimeConverter(QWidget * parent = 0);
  
 private:
  QLineEdit    *line_from,  *line_to;
  TimeCombo *combo_from, *combo_to;
  
 private slots:
  void update();
};

class PhysicalConstantsConverter : public QWidget {
  
  Q_OBJECT
  
 public:
  PhysicalConstantsConverter(QWidget * parent = 0);
  
 private:
  LengthCombo * length_combo;
  MassCombo   * mass_combo;
  TimeCombo   * time_combo;
  //
  QLineEdit * line_G;
  QLineEdit * line_c;
  
 private slots:
  void update();
};


class UnitsConverter : public QWidget {
  
  Q_OBJECT
    
 public:
  UnitsConverter(QWidget *parent=0);
};

#endif // _XORSA_UNITS_CONVERTER_H_
