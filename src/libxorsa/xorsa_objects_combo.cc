/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_objects_combo.h"

using namespace std;
using namespace orsa;

XOrsaImprovedObjectsCombo::XOrsaImprovedObjectsCombo(const int objects_flags, QWidget *parent) : QComboBox(false,parent), special_objects_flags(objects_flags), list(0) {
  connect(this,SIGNAL(activated(int)),this,SLOT(SetObject(int)));
}

XOrsaImprovedObjectsCombo::XOrsaImprovedObjectsCombo(QWidget *parent) : QComboBox(false,parent), special_objects_flags(0), list(0) {
  connect(this,SIGNAL(activated(int)),this,SLOT(SetObject(int)));
}

XOrsaImprovedObjectsCombo::XOrsaImprovedObjectsCombo(const vector<Body> *list_in, const bool only_massive, QWidget *parent) : QComboBox(false,parent), special_objects_flags(0), list(0) {
  connect(this,SIGNAL(activated(int)),this,SLOT(SetObject(int)));
  Set(list_in,only_massive);
}

XOrsaImprovedObjectsCombo::XOrsaImprovedObjectsCombo(const vector<Body> *list_in, const bool only_massive, const int objects_flags, QWidget *parent) : QComboBox(false,parent), special_objects_flags(objects_flags), list(0) {
  connect(this,SIGNAL(activated(int)),this,SLOT(SetObject(int)));
  Set(list_in,only_massive);
}

void XOrsaImprovedObjectsCombo::Set(const vector<Body> *list_in, const bool only_massive) {
  
  if (list_in==0) return;
  
  list = list_in;
  
  clear();
  //
  map_list.clear();
  map_special.clear();
  
  if (list->size() > 0) {
    
    for (unsigned int l=0;l<list->size();l++) {
      if ( (only_massive && ((*list)[l].mass() != 0)) ||
	   (!only_massive) ) {
	insertItem((*list)[l].name().c_str());
	map_list[count()-1] = l;
      }
    }
  }
  
  if (special_objects_flags & HEX_ORIGIN) {
    insertItem("origin");
    const int minus_hex = -HEX_ORIGIN;
    map_list[count()-1] = minus_hex;
    map_special[minus_hex] = count()-1;
  }
  
  if (special_objects_flags & HEX_CENTER_OF_MASS) {
    insertItem("center of mass");
    const int minus_hex = -HEX_CENTER_OF_MASS;
    map_list[count()-1] = minus_hex;
    map_special[minus_hex] = count()-1;
  }
  
  if (special_objects_flags & HEX_FREE) {
    insertItem("free");
    const int minus_hex = -HEX_FREE;
    map_list[count()-1] = minus_hex;
    map_special[minus_hex] = count()-1;
  }
  
  if (special_objects_flags & HEX_NOT_SET) {
    insertItem("not set");
    const int minus_hex = -HEX_NOT_SET;
    map_list[count()-1] = minus_hex;
    map_special[minus_hex] = count()-1;
  }
  
  if (special_objects_flags & HEX_AUTO) {
    insertItem("auto");
    const int minus_hex = -HEX_AUTO;
    map_list[count()-1] = minus_hex;
    map_special[minus_hex] = count()-1;
  }
}

int XOrsaImprovedObjectsCombo::GetObject() {
  return (map_list[currentItem()]);
}

Body XOrsaImprovedObjectsCombo::GetBody() {
  if (GetObject() >= 0) {
    return (*list)[GetObject()];
  } else {
    ORSA_ERROR("XOrsaImprovedObjectsCombo::GetBody(): Error: no body defined, negative value (one of the special values...)");
    return Body();
  }
}

void XOrsaImprovedObjectsCombo::SetObject(int i) {
  setCurrentItem(i);
  emit ObjectChanged(map_list[i]);
}

void XOrsaImprovedObjectsCombo::SetObject(SpecialHEXObject hex) {
  const int minus_hex = -hex;
  SetObject(map_special[minus_hex]);
}

