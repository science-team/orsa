/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa_new_object_keplerian_dialog.h"
#include "xorsa_object_selector.h"
#include "xorsa_import_JPL_objects.h"

#include <orsa_error.h>

#include <qlayout.h>
#include <qpushbutton.h> 
#include <qvalidator.h> 
#include <qlabel.h> 
#include <qgroupbox.h> 
#include <qlineedit.h> 
#include <qhbox.h>
#include <qbuttongroup.h> 
#include <qradiobutton.h> 

using namespace std;
using namespace orsa;

XOrsaNewObjectKeplerianDialog::XOrsaNewObjectKeplerianDialog(vector<BodyWithEpoch> &list_in, QWidget *parent) : QDialog(parent,0,true), list(list_in), edit_mode(false) {
  
  char name[1024];
  snprintf(name,1024,"object name");
  b = BodyWithEpoch(name,0.0);
  
  bodies_for_combo.resize(list.size());
  unsigned int k=0;
  while (k < list.size()) {
    bodies_for_combo[k] = list[k];
    ++k;
  }
  
  internal_change=true;
  init_draw();
  init_values();
  internal_change=false;
  
  update_P();
  
  setCaption("new object dialog");
}

XOrsaNewObjectKeplerianDialog::XOrsaNewObjectKeplerianDialog(vector<BodyWithEpoch> &list_in, BodyWithEpoch &b_in, QWidget *parent) : QDialog(parent,0,true), list(list_in), edit_mode(true) {
  
  b        = b_in;
  ref_body = b_in;
  
  bodies_for_combo.resize(list.size());
  unsigned int k=0;
  while (k < list.size()) {
    bodies_for_combo[k] = list[k];
    ++k;
  }
  
  {
    internal_change=true;
    init_draw();  
    //
    if (universe->GetUniverseType() == Real) {
      epoch->SetTime(b_in.Epoch().Time());
      P_epoch->SetTime(b_in.Epoch().Time());
    }
    //
    Orbit orbit;
    compute_orbit_from_body(orbit);
    init_values();
    internal_change=false;
  }
  
  update_P();
  
  setCaption("object editor");
}

void XOrsaNewObjectKeplerianDialog::init_values() {
  
  QString line;
  
  line = b.name().c_str();             
  le_name->setText(line);
  if (universe->GetUniverseType() == Simulated) {  
    mass_unit mu = masscb->GetUnit();
    line.sprintf("%.12g",FromUnits(b.mass(),mu,-1)); le_mass->setText(line);
  }
  
  if (edit_mode) {
    Orbit orbit;
    compute_orbit_from_body(orbit);
    fill_kepler_fields(orbit);
  }
}

void XOrsaNewObjectKeplerianDialog::fill_kepler_fields(const Orbit &orbit) {
  
  QString line;
  
  // units
  length_unit lu = spacecb->GetUnit();
  
  line.sprintf("%.12g",FromUnits(orbit.a,lu,-1));         le_ka->setText(line);
  line.sprintf("%.12g",orbit.e);                          le_ke->setText(line);
  line.sprintf("%.12g",orbit.i*(180/pi));                 le_ki->setText(line);
  line.sprintf("%.12g",orbit.omega_node*(180/pi));        le_knode->setText(line);
  line.sprintf("%.12g",orbit.omega_pericenter*(180/pi));  le_kperi->setText(line);
  line.sprintf("%.12g",orbit.M*(180/pi));                 le_km->setText(line);
}

void XOrsaNewObjectKeplerianDialog::init_draw() {
  
  QVBoxLayout *vlay = new QVBoxLayout(this,4);
  
  // name and mass
  QGroupBox *name_mass_gb = new QGroupBox("name and mass",this);
  name_mass_gb->setColumns(2);
  //
  new QLabel("name",name_mass_gb);
  le_name = new QLineEdit(name_mass_gb);
  //
  switch (universe->GetUniverseType()) {
  case Real: { 
    new QLabel("mass",name_mass_gb);
    new QLabel("0.0",name_mass_gb);
    break;
  } 
  case Simulated: {
    new QLabel("mass",name_mass_gb);
    le_mass = new QLineEdit(name_mass_gb);
    new QLabel("units",name_mass_gb);
    masscb = new MassCombo(name_mass_gb);
    masscb->SetUnit(units->GetMassBaseUnit());
    break;
  }
  }  
  //
  vlay->addWidget(name_mass_gb);
  
  // keplerian
  QGroupBox *keplerian_gb = new QGroupBox("keplerian",this);
  keplerian_gb->setColumns(1);
  
  QGroupBox *misc_gb = new QGroupBox(keplerian_gb);
  misc_gb->setColumns(2);
  
  switch (universe->GetUniverseType()) {
  case Real: { 
    new QLabel("ref. body",misc_gb);
    jpl_planets_combo = new XOrsaJPLPlanetsCombo(misc_gb);
    connect(jpl_planets_combo,SIGNAL(activated(int)),this,SLOT(ref_body_changed()));
    break;
  }
  case Simulated: {
    new QLabel("ref. body",misc_gb);
    ref_body_combo = new XOrsaImprovedObjectsCombo(&bodies_for_combo,true,misc_gb);
    connect(ref_body_combo,SIGNAL(ObjectChanged(int)),this,SLOT(ref_body_changed()));
    break;
  }
  }
  
  if (universe->GetUniverseType() == Real) {
    new QLabel("epoch",misc_gb);
    epoch = new XOrsaDatePushButton(misc_gb);
    connect(epoch,SIGNAL(DateChanged()),this,SLOT(update_P()));
  }
  
  QButtonGroup *action_gb = new QButtonGroup("edit mode",keplerian_gb);
  action_gb->setRadioButtonExclusive(true);
  action_gb->setColumns(2);
  //
  rb_action_modify  = new QRadioButton("modify",action_gb);
  rb_action_convert = new QRadioButton("convert",action_gb);
  //
  connect(rb_action_modify,SIGNAL(clicked()),this,SLOT(action_changed()));
  connect(rb_action_convert,SIGNAL(clicked()),this,SLOT(action_changed()));
  //
  rb_action_modify->setChecked(true);
  
  keplerian_elements_gb = new QButtonGroup(keplerian_gb);
  keplerian_elements_gb->setRadioButtonExclusive(true);
  keplerian_elements_gb->setColumns(2);
  // a
  new QLabel("a",keplerian_elements_gb);
  QHBox *auehb = new QHBox(keplerian_elements_gb);
  auehb->setSpacing(3);
  le_ka = new QLineEdit(auehb);
  spacecb = new LengthCombo(auehb);
  spacecb->SetUnit(units->GetLengthBaseUnit());
  connect(spacecb,SIGNAL(activated(int)),this,SLOT(ref_body_changed()));
  // e
  new QLabel("e",keplerian_elements_gb);
  le_ke = new QLineEdit(keplerian_elements_gb);  
  // i
  new QLabel("i",keplerian_elements_gb);
  le_ki = new QLineEdit(keplerian_elements_gb);  
  // node
  QChar   *uc_Omega = new QChar(0x03A9);
  QString suc_Omega(uc_Omega,1);    
  new QLabel(suc_Omega,keplerian_elements_gb);
  le_knode = new QLineEdit(keplerian_elements_gb); 
  // peri
  QChar   *uc_omega = new QChar(0x03C9);
  QString suc_omega(uc_omega,1);    
  new QLabel(suc_omega,keplerian_elements_gb);
  le_kperi = new QLineEdit(keplerian_elements_gb);
  // M
  switch (universe->GetUniverseType()) {
  case Real: {
    
    M_rb = new QRadioButton("M",keplerian_elements_gb);
    le_km = new QLineEdit(keplerian_elements_gb);  
    connect(M_rb,SIGNAL(toggled(bool)),le_km,SLOT(setEnabled(bool)));
    connect(le_ka,SIGNAL(textChanged(const QString&)),this,SLOT(update_P()));
    connect(le_km,SIGNAL(textChanged(const QString&)),this,SLOT(update_P()));
    connect(spacecb,SIGNAL(activated(int)),this,SLOT(update_P()));
    
    P_rb = new QRadioButton("P",keplerian_elements_gb);
    P_epoch = new XOrsaDatePushButton(keplerian_elements_gb);
    connect(P_rb,SIGNAL(toggled(bool)),P_epoch,SLOT(setEnabled(bool)));
    connect(P_epoch,SIGNAL(DateChanged()),this,SLOT(update_M_from_P()));
    
    M_rb->setChecked(true);
    P_rb->setChecked(true);
    M_rb->setChecked(true);
    
    break;
  } 
  case Simulated: {
    new QLabel("M",keplerian_elements_gb);
    le_km = new QLineEdit(keplerian_elements_gb);  
    break;
  }
  }
  //
  vlay->addWidget(keplerian_gb);
  
  // validation
  QDoubleValidator *vd = new QDoubleValidator(this);
  QDoubleValidator *vdz = new QDoubleValidator(this);
  vdz->setBottom(0.0);
  if (universe->GetUniverseType() == Simulated) {
    le_mass->setValidator(vdz);
  }
  
  // keplers
  le_ka->setValidator(vdz);
  le_ke->setValidator(vdz);
  le_ki->setValidator(vdz);
  //
  le_knode->setValidator(vd);
  le_kperi->setValidator(vd);
  le_km->setValidator(vd);
  
  le_ka->setText("0.0");
  le_ke->setText("0.0");
  le_ki->setText("0.0");
  le_knode->setText("0.0");
  le_kperi->setText("0.0");
  le_km->setText("0.0");
  
  // push buttons
  QHBoxLayout *hok = new QHBoxLayout(vlay);
  
  hok->addStretch();
  //
  okpb = new QPushButton(this);
  okpb->setText("OK");
  hok->addWidget(okpb);
  connect(okpb,SIGNAL(clicked()),this,SLOT(ok_pressed()));
  //
  cancpb = new QPushButton(this);
  cancpb->setText("Cancel");
  hok->addWidget(cancpb);
  connect(cancpb,SIGNAL(clicked()),this,SLOT(cancel_pressed()));
  //
  hok->addStretch();
  
}

BodyWithEpoch XOrsaNewObjectKeplerianDialog::GetBody() {
  return b;
}

void XOrsaNewObjectKeplerianDialog::ok_pressed() {
  read_orbit_from_interface_and_update_body();
  ok = true;
  done(0);  
}

void XOrsaNewObjectKeplerianDialog::cancel_pressed() {
  ok = false;
  b  = ref_body;
  done(0);
}

void XOrsaNewObjectKeplerianDialog::update_P() {
  if (internal_change) return;
  switch (universe->GetUniverseType()) {
  case Real: {
    if (P_rb->isOn()) {
      update_M_from_P();
    } else {
      Orbit orbit;
      read_orbit_from_interface(orbit);
      orbit.M = fmod(10*twopi+fmod(orbit.M,twopi),twopi);
      if (orbit.M < pi) {
	P_epoch->SetTime(epoch->GetTime()-(orbit.M/twopi)*orbit.Period());
      } else {
	P_epoch->SetTime(epoch->GetTime()+((twopi-orbit.M)/twopi)*orbit.Period());
      }
    }
    break;
  }
  case Simulated:
    break;
  }
}

void XOrsaNewObjectKeplerianDialog::update_M_from_P() {
  if (internal_change) return;
  switch (universe->GetUniverseType()) {
  case Real: { 
    if (M_rb->isOn()) return;
    Orbit orbit;
    read_orbit_from_interface(orbit);
    QString line;
    orbit.M = ((epoch->Time() - P_epoch->Time())/orbit.Period())*twopi;  
    orbit.M = fmod(10*twopi+fmod(orbit.M,twopi),twopi);
    line.sprintf("%.12g",orbit.M*(180/pi));
    le_km->setText(line);
    break;
  }
  case Simulated:
    break;
  }
}

void XOrsaNewObjectKeplerianDialog::compute_orbit_from_body(Orbit & orbit) {  
  switch (universe->GetUniverseType()) {
  case Real: {
    JPLBody jb(jpl_planets_combo->GetPlanet(),*epoch);
    orbit.Compute(b,jb);
    break;
  } 
  case Simulated: {
    orbit.Compute(b,ref_body_combo->GetBody());
    break;
  }
  }
}

/* 
   void XOrsaNewObjectKeplerianDialog::kepler_object_changed(int) {  
   Orbit orbit;
   compute_orbit_from_body(orbit);
   fill_kepler_fields(orbit);
   }
*/

void XOrsaNewObjectKeplerianDialog::ref_body_changed() {  
  if (rb_action_modify->isOn()) {
    // nothing
  } else if (rb_action_convert->isOn()) {
    Orbit orbit;
    compute_orbit_from_body(orbit);
    fill_kepler_fields(orbit);
  } else {
    ORSA_LOGIC_ERROR("GUI");
    return;
  }
}

void XOrsaNewObjectKeplerianDialog::action_changed() {  
  
  bool enable;
  
  if (rb_action_modify->isOn()) {
    enable=true;
  } else if (rb_action_convert->isOn()) {
    read_orbit_from_interface_and_update_body();
    enable=false;
  } else {
    ORSA_LOGIC_ERROR("GUI");
    return;
  }
  
  le_ka->setEnabled(enable);
  le_ke->setEnabled(enable);
  le_ki->setEnabled(enable);
  le_knode->setEnabled(enable);
  le_kperi->setEnabled(enable);
  le_km->setEnabled(enable);
  
  switch (universe->GetUniverseType()) {
  case Real: { 
    M_rb->setEnabled(enable);
    P_rb->setEnabled(enable);
    //
    P_epoch->setEnabled(enable);
    break;
  } 
  case Simulated: {
    break;
  }
  }
}

void XOrsaNewObjectKeplerianDialog::read_orbit_from_interface(Orbit & orbit) {
  
  // units
  length_unit lu = spacecb->GetUnit();
  
  switch (universe->GetUniverseType()) {
  case Real: {
    JPLBody jb(jpl_planets_combo->GetPlanet(),*epoch);
    // orbit.mu = GetG()*(b.mass() + jb.mass());
    orbit.mu = GetG()*jb.mass();
    break;
  }
  case Simulated: {
    // orbit.mu = GetG()*(b.mass() + ref_body_combo->GetBody().mass());
    orbit.mu = GetG()*(FromUnits(le_mass->text().toDouble(),masscb->GetUnit()) + ref_body_combo->GetBody().mass());
    break;
  }
  }
  
  orbit.a                = FromUnits(le_ka->text().toDouble(),lu);
  orbit.e                = le_ke->text().toDouble();
  orbit.i                = (pi/180)*le_ki->text().toDouble();
  orbit.omega_node       = (pi/180)*le_knode->text().toDouble();
  orbit.omega_pericenter = (pi/180)*le_kperi->text().toDouble();
  //
  switch (universe->GetUniverseType()) {
  case Real: {
    if (P_rb->isOn()) {
      orbit.M = ((epoch->Time() - P_epoch->Time())/orbit.Period())*twopi;  
      orbit.M = fmod(10*twopi+fmod(orbit.M,twopi),twopi);
    } else if (M_rb->isOn()) {
      orbit.M = (pi/180)*le_km->text().toDouble();
    } else {
      ORSA_LOGIC_WARNING("a case is not handled correctly inside XOrsaNewObjectKeplerianDialog::compute_orbit_from_interface().");
    }
    break;
  } 
  case Simulated: {
    orbit.M = (pi/180)*le_km->text().toDouble();
    break;
  }
  }
  
}

void XOrsaNewObjectKeplerianDialog::read_orbit_from_interface_and_update_body() {
  
  Orbit orbit;
  
  read_orbit_from_interface(orbit);
  
  Vector orbit_position, orbit_velocity;
  
  orbit.RelativePosVel(orbit_position,orbit_velocity);
  
  switch (universe->GetUniverseType()) {
  case Real: {  
    JPLBody jb(jpl_planets_combo->GetPlanet(),*epoch);
    b = BodyWithEpoch(le_name->text().simplifyWhiteSpace().latin1(),0.0);
    b.SetPosition(jb.position() + orbit_position);
    b.SetVelocity(jb.velocity() + orbit_velocity);
    b.SetEpoch(*epoch);
    break;
  } 
  case Simulated: {
    b = BodyWithEpoch(le_name->text().simplifyWhiteSpace().latin1(),FromUnits(le_mass->text().toDouble(),masscb->GetUnit()));
    b.SetPosition(ref_body_combo->GetBody().position() + orbit_position);
    b.SetVelocity(ref_body_combo->GetBody().velocity() + orbit_velocity);
    break;
  }
  }
}

