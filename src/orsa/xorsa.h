/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _XORSA_H_
#define _XORSA_H_

#include <qdialog.h>
#include <qmainwindow.h>
#include <qlineedit.h> 
#include <qpushbutton.h>
#include <qstring.h> 
#include <qcombobox.h>
#include <qlistview.h>
#include <qlabel.h> 
#include <qobject.h> 
#include <qthread.h>
#include <qprogressbar.h> 
#include <qlayout.h>
#include <qapplication.h> 
#include <qmutex.h> 
#include <qtoolbutton.h> 
#include <qgroupbox.h> 

#include <orsa_universe.h>
#include <orsa_config.h>
#include <orsa_file.h>

#include <xorsa_integrations_info.h>
#include <xorsa_wrapper.h>
#include <xorsa_about.h>
#include <xorsa_download.h>
#include <xorsa_plot_area.h> // FineLabel()...

#include <iostream>

class ObjectItem : public QListViewItem {
  
  // Q_OBJECT
  
 public:
  ObjectItem(QListView *parent, QString label1, QString label2 = QString::null, QString label3 = QString::null, QString label4 = QString::null, QString label5 = QString::null, QString label6 = QString::null, QString label7 = QString::null, QString label8 = QString::null);
  
 public:
  int ObjectItem::compare(QListViewItem * i, int col, bool ascending) const;    
  
};

////

class ReadXOrsaFileThread : public QThread {
  
 public:
  ReadXOrsaFileThread(QString &ofn_in, XOrsaIntegrationsInfo *xoii_in) : QThread(), ofn(ofn_in), xoii(xoii_in) { };
  
  void run() {
    qApp->lock(); 
    xoii->clear();
    // xoii->stop_auto_updates();
    qApp->unlock(); 
    XOrsaFile of;
    of.SetFileName(ofn.latin1());
    of.Open(orsa::OPEN_R);
    of.Read();
    of.Close();
    qApp->lock(); 
    xoii->universe_modified();
    // xoii->start_auto_updates();
    qApp->unlock(); 
    orsa::universe->modified = false;
  }
  
 private:
  QString ofn;
  XOrsaIntegrationsInfo *xoii;
};

////

class XOrsa : public QMainWindow {
  
  Q_OBJECT
  
 public:
  XOrsa();
  
 private slots:
  void slot_options_preferences();
  void slot_file_new();
  void slot_file_open();
  bool slot_file_save();
  void slot_universe_info();
  
  void slot_download();
  void slot_units_converter();
  void slot_time_tool();
  void slot_debug_widget();
  void slot_location();
  void slot_close_approaches();
  
  void new_integration_dialog();
  void stop_integration();
  void about();
  void closeEvent(QCloseEvent*); 
  
 public slots:
  void widgets_enabler();
  
 signals:
  void closing_universe();
  
 public:  
  QString program_name;
  
 private:
  void make_download_window();
  XOrsaDownload * download;
  
 private:
  XOrsaAbout * abt;
  QMenuBar *menu;
  //
  QPopupMenu *file_pm;
  QPopupMenu *options_pm;
  QPopupMenu *universe_pm;
  QPopupMenu *import_pm;
  QPopupMenu *integration_pm;
  QPopupMenu *tools_pm;
  QPopupMenu *help_pm;
  
  QToolButton *universe_info_tb, *file_save_tb, *new_integration_tb, *stop_integration_tb, *plot_3D_tb, *plot_2D_tb, *integration_copy_tb;
  
 private:
  XOrsaDebugWidget * debug_widget;
  
  int id_file_pm,id_options_pm,id_universe_pm,id_objects_pm,id_import_pm,id_integration_pm,id_tools_pm,id_help_pm;
  int id_universe_info,id_file_save,id_clapp;
  
 private:
  XOrsaIntegrationsInfo *integrations_info;
  
 private:
  bool user_universe_active;
};

#endif // _XORSA_H_
