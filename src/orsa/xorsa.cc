/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <orsa_universe.h>
#include <orsa_file.h>

#include <vector>
#include <iostream>

#include <qapplication.h>
#include <qpainter.h>
#include <qframe.h>
#include <qpopupmenu.h> 
#include <qmenubar.h>
#include <qmessagebox.h>
#include <qpushbutton.h>
#include <qcombobox.h>
#include <qlayout.h> 
#include <qlabel.h> 
#include <qlineedit.h> 
#include <qfiledialog.h> 
#include <qstring.h>
#include <qlistview.h>
#include <qvalidator.h> 
#include <qstatusbar.h>
#include <qscrollview.h> 
#include <qcheckbox.h> 
#include <qnetwork.h>
#include <qtabwidget.h>
#include <qtextedit.h> 
#include <qvbox.h> 
#include <qtoolbutton.h> 

#include <xorsa_about.h>
#include <xorsa_config.h>
#include <xorsa_import_JPL_objects.h>
#include <xorsa_import_astorb_objects.h>
#include <xorsa_new_integration_dialog.h>
#include <xorsa_new_universe_dialog.h>
#include <xorsa_integrations_info.h>
#include <xorsa_download.h>
#include <xorsa_location_selector.h>
#include <xorsa_units_converter.h>

// icons
#include "fileopen.xpm"
#include "filesave.xpm"
#include "explosion.xpm"
#include "information.xpm"
#include "calc.xpm"
#include "clock.xpm"
#include "bug.xpm"
#include "eyes.xpm"
#include "ghost.xpm"
#include "ray.xpm"
#include "plot.xpm"
#include "red_blue.xpm"
#include "stop.xpm"

#ifdef HAVE_GSL	
#include <xorsa_close_approaches_dialog.h>
#endif

#include "xorsa.h"

using namespace orsa;

using namespace std;

ObjectItem::ObjectItem(QListView *parent, QString label1, QString label2, QString label3, QString label4, QString label5, QString label6, QString label7, QString label8) : QListViewItem(parent, label1, label2, label3, label4, label5, label6, label7, label8) {
  
}

int ObjectItem::compare ( QListViewItem * i, int col, bool ascending ) const {
  
  // integers
  if (col == 0) return (atoi(key( col, ascending ).latin1()) - atoi( i->key(col, ascending).latin1()));
  
  // doubles
  if (col == 2 || col == 3 || col == 4) {
    
    double d = (atof(key( col, ascending ).latin1()) - atof( i->key(col, ascending).latin1()));
    
    if (d < 0.0) return (-1);
    if (d > 0.0) return (+1);
    return (0);
  }

  // default
  return key( col, ascending ).compare( i->key(col, ascending));
}

////

XOrsa::XOrsa() : QMainWindow(0,"xorsa",WDestructiveClose) {
  
  debug_widget = new XOrsaDebugWidget();
  XOrsaDebug::construct(debug_widget);
  
#ifdef _WIN32
  program_name = "winorsa";
#elif defined(__APPLE__)
  program_name = "macorsa";
#else
  program_name = "xorsa";
#endif
  
  qInitNetworkProtocols();
  
  new XOrsaUniverse();
  
  {
    /* 
       Config configuration;
       OrsaConfigFile config_file(&configuration);
       config_file.Read();
       config_file.Close();
    */
    if (config->paths[JPL_EPHEM_FILE]->GetValue() != "") {
      new XOrsaUniverse(AU,MSUN,YEAR,Real);
    }
  } 
  //
  user_universe_active = false;
  // user_universe_active = true;
  
  ////
  
  integrations_info = new XOrsaIntegrationsInfo(this);
  connect(this,SIGNAL(closing_universe()),integrations_info,SIGNAL(closing_universe()));
  connect(integrations_info,SIGNAL(new_integration()),this,SLOT(new_integration_dialog()));
  connect(integrations_info,SIGNAL(selectionChanged()),this,SLOT(widgets_enabler()));
  connect(integrations_info,SIGNAL(stopped_integration()),this,SLOT(widgets_enabler()));
  setCentralWidget(integrations_info);
  
  menu = menuBar();
  
  file_pm = new QPopupMenu(menu);
  // file_pm->insertItem("&Open",this,SLOT(slot_file_open()));
  file_pm->insertItem(QIconSet(fileopen),"&Open",this,SLOT(slot_file_open()));
  // file_pm->insertItem("&Save",this,SLOT(slot_file_save()));
  id_file_save = file_pm->insertItem(QIconSet(filesave),"&Save",this,SLOT(slot_file_save()));
  file_pm->insertItem("&Quit",qApp,SLOT(closeAllWindows()));
  id_file_pm = menu->insertItem("&File",file_pm);
  
  universe_pm = new QPopupMenu(menu);
  universe_pm->insertItem(QIconSet(explosion),"&New", this,SLOT(slot_file_new()));
  id_universe_info = universe_pm->insertItem(QIconSet(information),"&Info",this,SLOT(slot_universe_info()));
  id_universe_pm = menu->insertItem("&Universe",universe_pm);
  
  integration_pm = new XOrsaIntegrationsPopupMenu(integrations_info,menu); 
  id_integration_pm = menu->insertItem("&Integration",integration_pm);
  
  tools_pm = new QPopupMenu(menu);
  id_tools_pm = menu->insertItem("&Tools",tools_pm);
  tools_pm->insertItem("&Update databases",this,SLOT(slot_download()));
  tools_pm->insertItem(QIconSet(calc),"&Units converter",this,SLOT(slot_units_converter()));
  tools_pm->insertItem(QIconSet(clock_xpm),"&Time",this,SLOT(slot_time_tool()));
  tools_pm->insertItem(QIconSet(eyes_xpm),"&Observer locations",this,SLOT(slot_location()));
  id_clapp = tools_pm->insertItem("&Close approaches",this,SLOT(slot_close_approaches()));
  tools_pm->insertItem(QIconSet(bug_xpm),"&Debug window",this,SLOT(slot_debug_widget()));
  
#ifndef HAVE_GSL
  tools_pm->setItemEnabled(id_clapp,false);
#else
  tools_pm->setItemEnabled(id_clapp,true);
#endif // HAVE_GSL
  
  options_pm = new QPopupMenu(menu);
  options_pm->insertItem("&Preferences",this,SLOT(slot_options_preferences()));
  id_options_pm = menu->insertItem("&Options",options_pm);
  
  menu->insertSeparator();
  help_pm = new QPopupMenu(menu);
  help_pm->insertItem( "&About",this,SLOT(about()));
  id_help_pm = menu->insertItem("&Help",help_pm);
  
  // docking
  {
    // file
    QToolBar * fileTools = new QToolBar(this);
    fileTools->setLabel("File Operations");
    //
    new QToolButton(QIconSet(fileopen),"Open File",QString::null,this,SLOT(slot_file_open()),fileTools);
    file_save_tb = new QToolButton(QIconSet(filesave),"Save File",QString::null,this,SLOT(slot_file_save()),fileTools);
  }
  
  {
    // universe
    QToolBar * universeTools = new QToolBar(this);
    universeTools->setLabel("Universe Operations");
    //
    new QToolButton(QIconSet(explosion),"New Universe",QString::null,this,SLOT(slot_file_new()),universeTools);
    universe_info_tb = new QToolButton(QIconSet(information),"Universe Info",QString::null,this,SLOT(slot_universe_info()),universeTools);
  }
  
  {
    // integration
    QToolBar * integrationTools = new QToolBar(this);
    integrationTools->setLabel("Integration Operations");
    //
    new_integration_tb = new QToolButton(QIconSet(ray_xpm),"New Integration",QString::null,this,SLOT(new_integration_dialog()),integrationTools);
    stop_integration_tb = new QToolButton(QIconSet(stop_xpm),"Stop Integration",QString::null,this,SLOT(stop_integration()),integrationTools);
    plot_3D_tb = new QToolButton(QIconSet(red_blue_xpm),"3D Viewer",QString::null,integrations_info,SLOT(slot_opengl()),integrationTools);
    plot_2D_tb = new QToolButton(QIconSet(plot_xpm),"2D Plots",QString::null,integrations_info,SLOT(slot_plot()),integrationTools);
    integration_copy_tb = new QToolButton(QIconSet(ghost_xpm),"New integration with same objects",QString::null,integrations_info,SLOT(slot_integration_copy()),integrationTools);
  }
  
  {
    // tools
    QToolBar * genericTools = new QToolBar(this);
    genericTools->setLabel("Tools");
    //
    new QToolButton(QIconSet(calc),"Units Converter",QString::null,this,SLOT(slot_units_converter()),genericTools);
    new QToolButton(QIconSet(clock_xpm),"Time Tool",QString::null,this,SLOT(slot_time_tool()),genericTools);
    new QToolButton(QIconSet(eyes_xpm),"Observer Locations",QString::null,this,SLOT(slot_location()),genericTools);
    new QToolButton(QIconSet(bug_xpm),"Debug Window",QString::null,this,SLOT(slot_debug_widget()),genericTools);
  }
  
  abt = new XOrsaAbout(this,program_name);
  
  make_download_window();
  
  resize(400,180);
  
  statusBar()->message( "Ready", 2000 );
  
  // timer: bad and potentially dangerous: used just to
  //        fix the problem of widget_enabler() not called
  //        after a orsafile read through a new thread...
  //        --- To fix soon.
  /* 
     QTimer *timer = new QTimer(this);
     connect(timer,SIGNAL(timeout()),this,SLOT(widgets_enabler()));
     timer->start(1000);
  */
  
  widgets_enabler();
}


void XOrsa::new_integration_dialog() {
  
  XOrsaEvolution *evol = new XOrsaEvolution();
  XOrsaNewIntegrationDialog *id = new XOrsaNewIntegrationDialog(evol,this);
  
  id->show();
  id->exec();
  
  if (id->ok) {
    /* 
       XOrsaIntegrationProgress *ip = new XOrsaIntegrationProgress(evol,this);
       ip->show(); 
    */
    universe->push_back(evol);
    universe->modified = true;
  } else {
    delete evol;
  }
  
  delete id;
  integrations_info->universe_modified();
}

void XOrsa::stop_integration() {
  integrations_info->slot_stop_integration();
}

void XOrsa::about() {
  abt->hide();
  abt->show();
}

void XOrsa::slot_file_new() {
  
  // if (orsa->user_universe_active && universe->modified) {
  if (user_universe_active && universe->modified) {
    
    switch( QMessageBox::warning( this, program_name,
				  "Save changes to \"" + QString(universe->name.c_str()) + "\"?",
				  "Save",
				  "Don't save",
				  "Cancel",0,2) 
	    ) {
    case 0: // save 
      if (!slot_file_save()) return;
      break;
    case 1: // don't save
      break;
    case 2: // cancel
      return;
      break;
    }    
  }
  
  // integrations_info->stop_auto_updates();
  
  // XOrsaNewUniverseDialog *ud = new XOrsaNewUniverseDialog(universe,false,this);
  XOrsaNewUniverseDialog *ud = new XOrsaNewUniverseDialog(false,this);
  
  connect(ud,SIGNAL(closing_universe()),this,SIGNAL(closing_universe()));
  
  ud->show();
  ud->exec();
  
  if (ud->ok) {
    
    // don't call integrations_info->list_modified(universe), just clear the listview...
    // integrations_info->list_modified(universe);
    
    integrations_info->listview->clear();    
    user_universe_active = true;
  }
  
  // integrations_info->start_auto_updates();
  
  widgets_enabler();
}


void XOrsa::slot_universe_info() {
  
  // XOrsaNewUniverseDialog *ud = new XOrsaNewUniverseDialog(universe,true,this);
  XOrsaNewUniverseDialog *ud = new XOrsaNewUniverseDialog(true,this);
  
  ud->show();
  ud->exec();
  
  if (ud->ok) {
    universe->modified = true;
  }  
}

void XOrsa::slot_options_preferences() {
  list<ConfigEnum> le;
  //
  le.push_back(JPL_EPHEM_FILE);
  le.push_back(JPL_DASTCOM_NUM);
  le.push_back(JPL_DASTCOM_UNNUM);
  le.push_back(JPL_DASTCOM_COMET);
  le.push_back(LOWELL_ASTORB);
  le.push_back(MPC_MPCORB);
  le.push_back(MPC_COMET);
  le.push_back(MPC_NEA);
  le.push_back(MPC_DAILY);
  le.push_back(MPC_DISTANT);
  le.push_back(MPC_PHA);
  le.push_back(MPC_UNUSUALS);
  le.push_back(ASTDYS_ALLNUM_CAT);
  le.push_back(ASTDYS_ALLNUM_CTC);
  le.push_back(ASTDYS_ALLNUM_CTM);
  le.push_back(ASTDYS_UFITOBS_CAT);
  le.push_back(ASTDYS_UFITOBS_CTC);
  le.push_back(ASTDYS_UFITOBS_CTM);
  le.push_back(NEODYS_CAT);
  le.push_back(NEODYS_CTC);
  le.push_back(OBSCODE);
  // TLE
  le.push_back(TLE_NASA);
  le.push_back(TLE_GEO);
  le.push_back(TLE_GPS);
  le.push_back(TLE_ISS);
  le.push_back(TLE_KEPELE);
  le.push_back(TLE_VISUAL);
  le.push_back(TLE_WEATHER);
  // textures
  le.push_back(TEXTURE_SUN);
  le.push_back(TEXTURE_MERCURY);
  le.push_back(TEXTURE_VENUS);
  le.push_back(TEXTURE_EARTH);
  le.push_back(TEXTURE_MOON);
  le.push_back(TEXTURE_MARS);
  le.push_back(TEXTURE_JUPITER);
  le.push_back(TEXTURE_SATURN);
  le.push_back(TEXTURE_URANUS);
  le.push_back(TEXTURE_NEPTUNE);
  le.push_back(TEXTURE_PLUTO);
  //
  XOrsaConfig * oc = new XOrsaConfig(le);
  oc->show();
}

void XOrsa::slot_file_open() {
  
  // if (orsa->user_universe_active && universe->modified) {
  if (user_universe_active && universe->modified) {
    
    switch( QMessageBox::warning( this, program_name,
				  "Save changes to \"" + QString(universe->name.c_str()) + "\" before opening a new file?",
				  "Save",
				  "Don't save",
				  "Cancel",0,2) 
	    ) {
    case 0: // save 
      if (!slot_file_save()) return;
      break;
    case 1: // don't save
      break;
    case 2: // cancel
      return;
      break;
    }    
  }
  
  QString s = QFileDialog::getOpenFileName(QString::null,QString::null,this,0,"Choose a file to open");
  if (!(s.isEmpty())) {
    
    if (!XOrsaFile::GoodFile(s.latin1())) {
      ORSA_ERROR("Bad file type, \"%s\" is not a good ORSA file.", s.latin1());
      QMessageBox::warning( this, "Bad file type!",
			    "The file " + s + " is not a good ORSA file.",
			    QMessageBox::Ok,QMessageBox::NoButton);
      return;
    }
    
    emit closing_universe();
    
    // ReadXOrsaFileThread *roft = new ReadXOrsaFileThread(s,universe,integrations_info);
    ReadXOrsaFileThread *roft = new ReadXOrsaFileThread(s,integrations_info);
    roft->start();
    // orsa->user_universe_active = true;
    user_universe_active = true;
  } 
  
  widgets_enabler();
}

bool XOrsa::slot_file_save() {
  bool saved = false;
  QString s = QFileDialog::getSaveFileName(QString::null,QString::null,this,0,"Save Universe as:");
  if (!(s.isEmpty())) {
    XOrsaFile of;
    of.SetFileName(s.latin1());
    of.Write();
    of.Close();
    universe->modified = false;
    saved = true;
  }
  
  widgets_enabler();
  
  return saved; 
}

void XOrsa::closeEvent(QCloseEvent *ce) {
  
  // if ( (!orsa->user_universe_active) ||
  //       ( orsa->user_universe_active && (!universe->modified)) ) {
  if ( (!user_universe_active) ||
       ( user_universe_active && (!universe->modified)) ) {
    
    ce->accept();
    return;
  }
  
  switch( QMessageBox::warning( this, program_name,
				"Save changes to \"" + QString(universe->name.c_str()) + "\"?",
				"Save",
				"Don't save",
				"Cancel",0,2) 
	  ) {
  case 0: // save 
    if (!slot_file_save()) {
      ce->ignore();
    } else {
      ce->accept();
    }
    break;
  case 1: // don't save
    ce->accept();
    break;
  case 2: // cancel
  default: // just for sanity
    ce->ignore();
    break;
  }    
  
}

void XOrsa::slot_units_converter() {
  UnitsConverter *uc = new UnitsConverter();
  uc->show();
}

void XOrsa::slot_time_tool() {
  XOrsaDate *od = new XOrsaDate();
  od->show();
}

void XOrsa::slot_debug_widget() {
  debug_widget->hide();
  debug_widget->show();
}

void XOrsa::slot_location() {
  XOrsaLocationSelector *ls = new XOrsaLocationSelector(this,false);
  ls->show();
}

void XOrsa::slot_close_approaches() {
#ifdef HAVE_GSL
  XOrsaCloseApproachesDialog *ca = new XOrsaCloseApproachesDialog();
  connect(this,SIGNAL(closing_universe()),ca,SLOT(close()));
  ca->show();
#endif
}

void XOrsa::make_download_window() {
  
  vector<XOrsaDownloadItem> items;
  
  items.push_back(XOrsaDownloadItem("ftp://ftp.lowell.edu/pub/elgb/astorb.dat.gz",
				    LOWELL_ASTORB));
  
  items.push_back(XOrsaDownloadItem("http://newton.dm.unipi.it/neodys/neodys.cat",
				    NEODYS_CAT));
  
  items.push_back(XOrsaDownloadItem("http://newton.dm.unipi.it/neodys/neodys.ctc",
				    NEODYS_CTC));
  
  items.push_back(XOrsaDownloadItem("http://hamilton.dm.unipi.it/astdys/catalogs/allnum.cat",
				    ASTDYS_ALLNUM_CAT));
 
  items.push_back(XOrsaDownloadItem("http://hamilton.dm.unipi.it/astdys/catalogs/allnum.ctc.gz",
				    ASTDYS_ALLNUM_CTC));
  
  items.push_back(XOrsaDownloadItem("http://hamilton.dm.unipi.it/astdys/catalogs/allnum.ctm.gz",
				    ASTDYS_ALLNUM_CTM));
  
  items.push_back(XOrsaDownloadItem("http://hamilton.dm.unipi.it/astdys/catalogs/ufitobs.cat",
				    ASTDYS_UFITOBS_CAT));
  
  items.push_back(XOrsaDownloadItem("http://hamilton.dm.unipi.it/astdys/catalogs/ufitobs.ctc.gz",
				    ASTDYS_UFITOBS_CTC));
  
  items.push_back(XOrsaDownloadItem("http://hamilton.dm.unipi.it/astdys/catalogs/ufitobs.ctm.gz",
				    ASTDYS_UFITOBS_CTM));
  
  /* 
     items.push_back(XOrsaDownloadItem("ftp://cfa-ftp.harvard.edu/pub/MPCORB/MPCORB.ZIP",
     MPC_MPCORB));
     
     items.push_back(XOrsaDownloadItem("ftp://cfa-ftp.harvard.edu/pub/MPCORB/COMET.DAT",
     MPC_COMET));
     
     items.push_back(XOrsaDownloadItem("ftp://cfa-ftp.harvard.edu/pub/MPCORB/NEA.DAT",
     MPC_NEA));
     
     items.push_back(XOrsaDownloadItem("ftp://cfa-ftp.harvard.edu/pub/MPCORB/DAILY.DAT",
     MPC_DAILY));
     
     items.push_back(XOrsaDownloadItem("ftp://cfa-ftp.harvard.edu/pub/MPCORB/DistantObjects.DAT",
     MPC_DISTANT));
     
     items.push_back(XOrsaDownloadItem("ftp://cfa-ftp.harvard.edu/pub/MPCORB/PHA.DAT",
     MPC_PHA));
     
     items.push_back(XOrsaDownloadItem("ftp://cfa-ftp.harvard.edu/pub/MPCORB/Unusuals.DAT",
     MPC_UNUSUALS));
  */
  
  items.push_back(XOrsaDownloadItem("http://www.astro.cz/mpcorb/MPCORB.ZIP",
				    MPC_MPCORB));
  
  items.push_back(XOrsaDownloadItem("http://www.astro.cz/mpcorb/NEA.DAT",
				    MPC_NEA));
  
  items.push_back(XOrsaDownloadItem("http://www.astro.cz/mpcorb/DAILY.DAT",
				    MPC_DAILY));
  
  items.push_back(XOrsaDownloadItem("http://www.astro.cz/mpcorb/DistantObjects.DAT",
				    MPC_DISTANT));
    
  items.push_back(XOrsaDownloadItem("http://www.astro.cz/mpcorb/PHA.DAT",
				    MPC_PHA));
  
  items.push_back(XOrsaDownloadItem("http://www.astro.cz/mpcorb/Unusuals.DAT",
				    MPC_UNUSUALS));
  
  items.push_back(XOrsaDownloadItem("http://www.astro.cz/mpcorb/COMET.DAT",
				    MPC_COMET));
  
  /* 
     items.push_back(XOrsaDownloadItem("ftp://ssd.jpl.nasa.gov/pub/eph/export/unix/unxp2000.405",
     JPL_EPHEM_FILE,
     4666512));
  */
  
  items.push_back(XOrsaDownloadItem("ftp://ssd.jpl.nasa.gov/pub/eph/export/unix/unxp1800.406",
				    JPL_EPHEM_FILE,
				    9988160));
  
  items.push_back(XOrsaDownloadItem("http://ssd.jpl.nasa.gov/data/ELEMENTS.NUMBR.gz",
				    JPL_DASTCOM_NUM));   
  
  items.push_back(XOrsaDownloadItem("http://ssd.jpl.nasa.gov/data/ELEMENTS.UNNUM.gz",
				    JPL_DASTCOM_UNNUM));   
  
  items.push_back(XOrsaDownloadItem("http://ssd.jpl.nasa.gov/data/ELEMENTS.COMET",
				    JPL_DASTCOM_COMET));   
  
  items.push_back(XOrsaDownloadItem("http://cfa-www.harvard.edu/iau/lists/ObsCodes.html",
				    OBSCODE));   
  
  // TLE
  items.push_back(XOrsaDownloadItem("http://www.amsat.org/amsat/ftp/keps/current/nasa.all",
				    TLE_NASA));   
  
  items.push_back(XOrsaDownloadItem("ftp://alphalma.cnrs-mrs.fr/pub/astro/geo.tle",
				    TLE_GEO));   
  
  items.push_back(XOrsaDownloadItem("http://www.celestrak.com/NORAD/elements/gps-ops.txt",
				    TLE_GPS));   
  
  items.push_back(XOrsaDownloadItem("ftp://alphalma.cnrs-mrs.fr/pub/astro/iss.tle",
				    TLE_ISS));   
  
  items.push_back(XOrsaDownloadItem("http://www.orbitessera.com/data/orbital/kepele.txt",
				    TLE_KEPELE));   
  
  items.push_back(XOrsaDownloadItem("ftp://alphalma.cnrs-mrs.fr/pub/astro/visual.tle",
				    TLE_VISUAL));   
  
  items.push_back(XOrsaDownloadItem("ftp://alphalma.cnrs-mrs.fr/pub/astro/weather.tle",
				    TLE_WEATHER));   
  
  // textures
  items.push_back(XOrsaDownloadItem("",
				    TEXTURE_SUN));
  
  items.push_back(XOrsaDownloadItem("http://orsa.sourceforge.net/textures/mercury-1k.png",
				    TEXTURE_MERCURY));
  
  items.push_back(XOrsaDownloadItem("http://orsa.sourceforge.net/textures/venus-1k.png",
				    TEXTURE_VENUS));
  
  items.push_back(XOrsaDownloadItem("http://orsa.sourceforge.net/textures/earth-1k.png",
				    TEXTURE_EARTH));
  
  items.push_back(XOrsaDownloadItem("http://orsa.sourceforge.net/textures/moon-1k.png",
				    TEXTURE_MOON));
  
  items.push_back(XOrsaDownloadItem("http://orsa.sourceforge.net/textures/mars-1k.png",
				    TEXTURE_MARS));
  
  items.push_back(XOrsaDownloadItem("http://orsa.sourceforge.net/textures/jupiter-1k.png",
				    TEXTURE_JUPITER));
  
  items.push_back(XOrsaDownloadItem("http://orsa.sourceforge.net/textures/saturn-1k.png",
				    TEXTURE_SATURN));
  
  items.push_back(XOrsaDownloadItem("http://orsa.sourceforge.net/textures/uranus-1k.png",
				    TEXTURE_URANUS));
  
  items.push_back(XOrsaDownloadItem("http://orsa.sourceforge.net/textures/neptune-1k.png",
				    TEXTURE_NEPTUNE));
  
  items.push_back(XOrsaDownloadItem("",TEXTURE_PLUTO));
  
  download = new XOrsaDownload(items);
}

void XOrsa::slot_download() {
  download->hide();
  download->show();
}

void XOrsa::widgets_enabler() {
  if (!user_universe_active) {
    file_pm->setItemEnabled(id_file_save,false);
    file_save_tb->setEnabled(false);
    new_integration_tb->setEnabled(false);
    stop_integration_tb->setEnabled(false);
    plot_3D_tb->setEnabled(false);
    plot_2D_tb->setEnabled(false);
    integration_copy_tb->setEnabled(false);
    universe_pm->setItemEnabled(id_universe_info,false);
    universe_info_tb->setEnabled(false);
    integration_pm->setItemEnabled(integration_pm->idAt(0),false);
    tools_pm->setItemEnabled(id_clapp,false);
  } else {
    file_pm->setItemEnabled(id_file_save,true);
    file_save_tb->setEnabled(true);
    new_integration_tb->setEnabled(true);
    stop_integration_tb->setEnabled(integrations_info->at_least_one_selected_is_integrating());
    plot_3D_tb->setEnabled(integrations_info->at_least_one_selected());
    plot_2D_tb->setEnabled(integrations_info->at_least_one_selected());
    integration_copy_tb->setEnabled(integrations_info->at_least_one_selected());
    universe_pm->setItemEnabled(id_universe_info,true);
    universe_info_tb->setEnabled(true);
    integration_pm->setItemEnabled(integration_pm->idAt(0),true);
#ifdef HAVE_GSL
    if (universe->GetUniverseType()==Real) {
      tools_pm->setItemEnabled(id_clapp,true);
    } else {
      tools_pm->setItemEnabled(id_clapp,false);
    }	
#endif
  }
}
