/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "xorsa.h"

#include <orsa_version.h>

#include <qapplication.h>

#include <iostream>

#include <orsa_icon.xpm>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#ifdef HAVE_MPI
#include <mpi.h>
#endif

using namespace std;

using namespace orsa;

// tests: avoids session management problems...
class BetterApplication : public QApplication {
public:
  BetterApplication(int argc, char **argv) : QApplication(argc,argv) { }
public:
  inline void commitData(QSessionManager&) { }
  inline void saveState(QSessionManager&) { }
};

int main_no_MPI(int argc, char **argv) {
  
  BetterApplication a(argc,argv);
  
  XOrsa *mw = new XOrsa();
  
  mw->setCaption(mw->program_name + " " + ORSA_VERSION);
  mw->show();
  a.setMainWidget(mw);
  a.connect(&a,SIGNAL(lastWindowClosed()),&a,SLOT(quit()));
  
  const QPixmap icon(orsa_icon_xpm);
  mw->setIcon(icon);
  
  return a.exec();
}

#ifdef HAVE_MPI
int main_MPI(int argc, char **argv) {
  int rank, size,retval;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  if (rank == 0) {
    retval = main_no_MPI(argc,argv);
  } else {
    Newton_MPI *itr = new Newton_MPI();  
    Frame f;
    vector<Vector> a;
    while (1) {
      itr->Acceleration(f,a);
    }
    retval = 0;
  }
  MPI_Finalize();
  return retval;
}
#endif // HAVE_MPI

int main(int argc, char **argv) {
#ifndef HAVE_MPI
  return main_no_MPI(argc,argv);
#else
  return main_MPI(argc,argv);
#endif
}
