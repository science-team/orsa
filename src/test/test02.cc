
#include <orsa_universe.h>
#include <orsa_orbit.h>

#include <cstdio>

#include <orsa_file.h>

#include <iostream>

using namespace std;
using namespace orsa;

void print(Vector v) {
  cout << v.x << "  " << v.y << "  " << v.z << endl;
}

void p(Vector v) { print(v); }

void p(double f) { cout << f << endl; }

int main() {
  
  // Universe U(AU,MSUN,YEAR,ECLIPTIC,Real);
  // new Universe(AU,MSUN,YEAR,ECLIPTIC,Simulated);
  // Universe U(AU,MEARTH,YEAR,ECLIPTIC,Simulated);
  new Universe(AU,MSUN,YEAR,Simulated);
  
  // cout << "G    = " << GetG() << endl;
  // cout << "MSun = " << GetMSun() << endl;
  
  // tests
  /* cerr << "BFNew   = " << BFNew   << endl;
     cerr << "BFChild = " << BFChild << endl;
     cerr << "BFJPL   = " << BFJPL   << endl;
  */
  
  Newton itr;
  // GravitationalTree *itr = new GravitationalTree;
  
  // BulirschStoer *itg = new BulirschStoer; itg->accuracy = 1.0e-6;
  // itg->
  // Stoer *itg = new Stoer(itr,16);
  // Stoer *itg = new Stoer(16);
  Radau15 itg; itg.accuracy = 1.0e-8;
  // Leapfrog *itg = new Leapfrog;
  // DissipativeRungeKutta *itg = new DissipativeRungeKutta();
  // RungeKutta *itg = new RungeKutta; 
  // itg->timestep = 0.01;
  itg.timestep = 0.01;
  Evolution * evol = new Evolution;
  evol->SetIntegrator(&itg);
  evol->SetInteraction(&itr);
  evol->SetSamplePeriod(0.1);
  
  // U.push_back(evol);
  
  Frame f;
  
  Body b1("alpha",FromUnits(1.0,MSUN));
  b1.SetPosition(0,0,0);
  b1.SetVelocity(0,0,0);
  
  f.push_back(b1);
  
  Body b2("gamma",FromUnits(0.0001,MSUN));
  b2.SetPosition(1,0,0);
  b2.SetVelocity(0,twopi,0);
  
  f.push_back(b2);
  
  f.SetTime(0.0);
  
  evol->push_back(f);
  
  evol->Integrate(3.0);
  
  // cout << "Frames: " << U[0].size() << endl;
  
  unsigned int k;
  
  Orbit ob;
  
  for (k=0;k<evol->size();k++) {
    
    // cout << "time: " << (*evol)[k].time << endl;
    
    /* cout << "Body 1: " 
       << "  x: " << U[0][k][1].position.x
       << "  y: " << U[0][k][1].position.y
       << "  z: " << U[0][k][1].position.z <<	 endl;
    */
    
    // ob.Compute(U[0][0][k],1,0);
    ob.Compute((*evol)[k][1],(*evol)[k][0]);
    
    // cout << U[0][k].time << "  " << ob.a << "  " << ob.e << "  " << ob.i << endl;
    
    printf("%7.4f   %10.6f   %10.6f   %10.6f\n",(*evol)[k].Time(),ob.a,ob.e,ob.i); 
    
  }
  
  // another test
  { 
    Frame f;
    unsigned int r, b;
    for (r=0;r<10;++r) {
      
      f.clear();
      
      f.SetTime(0.0);
      
      for (b=0;b<10+30*r;++b) {
	Body body(1.0);
	body.SetPosition(100.0*b,-100.0*b,0.0+b);
	body.SetVelocity(r+b,r,b);
	f.push_back(body);
      }
      
      // print(f);
      
      cerr << "bodies: " << f.size() << endl;
      
      evol->clear();
      evol->push_back(f);
      evol->Integrate(1.0);
      
      // cerr << "evol size: " << evol->size() << endl;
      
      // print((*evol)[evol->size()-1]);
      
    }
  }
  
  return 0;
}
