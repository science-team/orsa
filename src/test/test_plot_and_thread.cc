#include "test_plot_and_thread.h"

int main(int argc, char **argv) {
  
  QApplication a(argc,argv);
  
  MainWidget *mw = new MainWidget();
  
  mw->show();
  
  a.setMainWidget(mw);
  
  a.connect(&a,SIGNAL(lastWindowClosed()),&a,SLOT(quit()));
  
  return (a.exec());
}
