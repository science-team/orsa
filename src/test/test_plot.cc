#include <qapplication.h>

#include <xorsa_plot_area.h>

#include <orsa_universe.h>
#include <orsa_integrator.h>

using namespace orsa;
using namespace std;

int main(int argc, char **argv) {
  
  QApplication a(argc,argv);
  
  XOrsaExtendedPlotArea *expa = new XOrsaExtendedPlotArea(500,300);
  XOrsaPlotArea *xpa = expa->area;
  
  expa->show();
  a.connect(&a,SIGNAL(lastWindowClosed()),&a,SLOT(quit()));
  
  vector<XOrsaPlotCurve> curves;
  XOrsaPlotCurve cur;
  XOrsaPlotPoint point;
  int x;
  
  cur.clear();
  for (x=0;x<100000;x++) {
    point.x = x / 10000.0;
    point.y = 3 + 5 * sin(x / 10000.0);
    cur.push_back(point);
  } 
  cur.index = 11;
  cur.color = Qt::black;
  curves.push_back(cur);
  
  
  cur.clear();
  for (x=0;x<100;x++) {
    point.x = x - 1;
    point.y = 2 - x / 20.0;
    cur.push_back(point);
  } 
  cur.index = 12;
  cur.color = Qt::magenta;
  curves.push_back(cur);
  
  
  /* double radius = 1.0e-7;
     cur.clear();
     int j;
     for (j=0;j<100;j++) {
     point.x = -3 + radius * cos(j*2.410);
     point.y = 2 + radius * sin(j*2.410);
     // printf("point: %.20g %.20g\n",point.x,point.y);
     cur.push_back(point);
     } 
     cur.index = 12;
     cur.color = Qt::red;
     curves.push_back(cur);
  */
  
  /* cur.clear();
     for (x=0;x<10000;x++) {
     point.x = 0.03 + x / 100000.0;
     point.y = 3.8 + 1.0e-12 * sin(x / 100.0);
     cur.push_back(point);
     } 
     cur.index = 12;
     cur.color = Qt::magenta;
     curves.push_back(cur);
  */
  
  xpa->SetData(&curves);
  
  xpa->SetConnectPoints(true);
  xpa->SetSameScale(false);
  
  return (a.exec());
}
