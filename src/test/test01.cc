
#include <orsa_universe.h>
#include <orsa_file.h>
#include <orsa_config.h>
#include <orsa_body.h>
#include <orsa_fft.h>

#include <iostream>

using namespace orsa;
using namespace std;

void print(Vector v) {
  cout << v.x << "  " << v.y << "  " << v.z << endl;
}

void p(Vector v) { print(v); }

void p(double f) { cout << f << endl; }

int main() {
  

  new Universe(AU,MSUN,YEAR);
  
  Body bd;

  Vector a,b,c;
  
  a.Set(0,10,0);
  
  print(a);
  
  b.Set(3,6,7);
  
  print(b);
  
  p((a+b));
  
  p(a);
  p(b);
  p(a*b);

  c.Set(pi,twopi,pisq);
  
  p(c);

  Vector d = ExternalProduct(a,b);
  
  print(d);
  
  cout << "(1) " << units->GetG() << endl;
  cout << "(2) " << orsa::GetG() << endl;
  
  cout << "G*MSun = " << GetG()*GetMSun() << endl;
  
  printf("G*MSun = %24.18f\n",GetG()*GetMSun());
  printf("  MSun = %24.18f\n",GetMSun());
  
  cout << "==================" << endl;
  
  vector<VectorWithParameter> samples;
  
  VectorWithParameter vx;
  
  vx.par = 0;
  vx.Set(-5,0,77);
  samples.push_back(vx);
  
  vx.par = 1;
  vx.Set(-4,2,73);
  samples.push_back(vx);
  
  vx.par = 2;
  vx.Set(-3,4,72);
  samples.push_back(vx);
  
  vx.par = 3;
  vx.Set(-2,6,77);
  samples.push_back(vx);
  
  cout << "size: " << samples.size() << endl;
  
  Vector vi, verr;
  
  Interpolate(samples,1.9,vi,verr);
  
  p(vi);
  p(verr);
  
  //////////////////////////
  
  // BulirschStoer *integ;
  
  // RungeKutta *integ;
  
  // Evolution ev(integ);
  
  cout << "==================" << endl;

  Orbit       o,o2;
  
  /* 
     Config *configuration = new Config();
     
     OrsaConfigFile ocf(configuration);
     ocf.Read();
     cerr << configuration->paths[ASTORB_DAT]->label << ": " << configuration->paths[ASTORB_DAT]->GetValue() << endl;
     cerr << configuration->paths[MPCORB_DAT]->label << ": " << configuration->paths[MPCORB_DAT]->GetValue() << endl;
     cerr << configuration->paths[JPLEPH]->label     << ": " << configuration->paths[JPLEPH]->GetValue()     << endl;
     cerr << configuration->paths[OBSCODE]->label    << ": " << configuration->paths[OBSCODE]->GetValue()    << endl;
  */
  
  cout << "==================" << endl;
  
  {
    
    Vector per,par,pir;
    
    
    per.Set(1,2,3);
    //
    print(per);
    print(par);
    print(pir);
    
    par = per*7.0;
    //
    print(per);
    print(par);
    print(pir);
    
    pir = per/3.0;
    //
    print(per);
    print(par);
    print(pir);
    
  }
  
  cout << "==================" << endl;
  
  /* 
     int wc=0;
     while (1) {
     cerr << ++wc << endl;
     JPLFile *jf;
     jf = new JPLFile("/home/tricaric/JPL/unxp1975.403");
     // delete jf;
     }
  */
  
  /* 
     {
     JPLBody *jb;
     Date d;
     d.SetGregor(2004,1,1);
     d.SetTimeScale(UT);
     while (1) {
     jb = new JPLBody(MOON,d);
     delete jb;
     }
     }
  */
  
  cout << "==================" << endl;
  
  cout << "secure_pow(NAN,-2) = " << secure_pow(NAN,-2) << endl;
  
  return 0;
}
