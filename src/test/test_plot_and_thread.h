#ifndef __TEST_PLOT_AND_THREAD_H__
#define __TEST_PLOT_AND_THREAD_H__

#include <qthread.h>
#include <qobject.h>
#include <qscrollview.h> 
#include <qvbox.h> 
#include <qlayout.h>
#include <qlabel.h> 
#include <qapplication.h>

#include <xorsa_plot_area.h>

class PlotThread : public QObject, public QThread {
  
  Q_OBJECT

 public:
  
  PlotThread(XOrsaPlotArea *xp1_in, XOrsaPlotArea *xp2_in, const unsigned long msecs_in=2000) : QObject(), QThread(), xp1(xp1_in), xp2(xp2_in), msecs(msecs_in) {
    mem1=mem2=0;
  };
  
  void run() {
    int loop_counter=0;
    while (1) {
      loop_counter++;
      FillCurve1();
      FillCurve2();
      qApp->lock();
      if (loop_counter==1) {
	xp1->SetData(&c1,true);
	xp2->SetData(&c2,true);
      } else {
	xp1->SetData(&c1,false);
	xp2->SetData(&c2,false);
      }
      qApp->unlock();
      msleep (msecs); 
    }
  };
  
  void FillCurve1() {
   
    c1.clear();
    
    cur.clear();
    for (j=0;j<10000;j++) {
      point.x = j / 10000.0;
      point.y = 3 + 5 * sin(j / 100.0 + mem1*530);
      cur.push_back(point);
    } 
    cur.index = 1;
    cur.color = Qt::black;
    
    c1.push_back(cur);
    mem1 += 23;
  };
  
  void FillCurve2() {
    
    c2.clear();
    
    cur.clear();
    for (j=0;j<10000;j++) {
      point.x = j - 1 ;
      // point.y = 2 - j * mem2 / 20.0 +j*(j-4500);
      // point.y = 2 - j * mem2 / 20.0 + j*(j-4500) + 1e7*exp(-pow(j-mem2,2)/(1e6));
      point.y = 2 - j * mem2 / 20.0 + j*(j-4500) + 1e7*exp(-pow(10000*sin((j-mem2)/1300.0),2)/(1e6));
      cur.push_back(point);
    } 
    cur.index = 1;
    cur.color = Qt::red;
    
    c2.push_back(cur);
    mem2 += 142;
    // mem2  = mem2%10000;
  };
  
 private:
  XOrsaPlotArea *xp1, *xp2;
  std::vector<XOrsaPlotCurve> c1, c2;
  const unsigned long msecs;
  //
  XOrsaPlotCurve cur;
  XOrsaPlotPoint point;
  int j;
  int mem1,mem2;
};


class MainWidget : public QScrollView {
  
  Q_OBJECT

 public:
  MainWidget(QWidget *parent=0) : QScrollView(parent) {
  
    setResizePolicy(QScrollView::AutoOneFit);
    
    QWidget *big_box = new QWidget(viewport());
    addChild(big_box);
    
    QVBoxLayout *vlay = new QVBoxLayout(big_box,4);
    
    XOrsaPlotArea *xpa = new XOrsaPlotArea(160,100,big_box); 
    vlay->addWidget(xpa);
    
    XOrsaPlotArea *xpb = new XOrsaPlotArea(160,100,big_box); 
    vlay->addWidget(xpb);
    
    // PlotThread *pt = new PlotThread(xpa,xpb,250);
    PlotThread *pt = new PlotThread(xpa,xpb,40);
    pt->start();
    
  };
  
};

#endif // __TEST_PLOT_AND_THREAD_H__
