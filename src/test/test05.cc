#include <orsa_universe.h>
#include <orsa_file.h>

using namespace orsa;

#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

int main(int argc, char **argv) {
  
  Universe z();
  
  // Angle tests...
  /* Angle a;
     double d,p,s,h,m;
     a.SetHMS(2,56,21.29);
     cout << a.GetRad() << endl;
     a.GetHMS(h,m,s);
     cout << h << " " << m << " " << s << endl;
     a.SetDPS(-12,2,3);
     cout << a.GetRad() << endl;
     a.SetHMS(12,0,0);
     cout << a.GetRad() << endl;
     a.SetDPS(-12,2,3);
     cout << a.GetRad() << endl;
     a.SetRad(-0.921431);
     a.GetDPS(d,p,s);
     cout << d << " " << p << " " << s << endl;
     a.SetDPS(-52,47,38.7867);
     cout << a.GetRad() << endl;
     exit(0);
  */
  
  char path[1024] = {0};
  
  if (argc == 2) {
    sprintf(path,argv[1]); 
  } else {
    cerr << "Usage: " << argv[0] << " <input file>" << endl;
    exit(0);
  }
  
  // vector<Observation> *obs = new vector<Observation>;
  // MPCObsFile *mpc_obs_file = new MPCObsFile(obs);
  MPCObsFile mpc_obs_file;
  
  mpc_obs_file.SetFileName(path);
  
  cerr << "trying file " << mpc_obs_file.GetFileName() << endl;
  
  mpc_obs_file.Open();
  
  mpc_obs_file.Read();
  
  cout << "observations: " << mpc_obs_file.obs.size() << endl;
  
  return 0;
}
