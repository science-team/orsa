#include <orsa_universe.h>
#include <orsa_file.h>

using namespace orsa;

#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

int main(int argc, char **argv) {
  
  Universe z(AU,MSUN,DAY);
  
  cout << "testing MPCORB.DAT reading capabilities..." << endl;
  
  // FILE *f;
  
  char path[1024];
  // sprintf(path,"/home/lino/c++/astorb.dat");
  // sprintf(path,"/home/tricaric/tmp/astorb.dat");
  if (argc == 2) {
    sprintf(path,argv[1]); 
  } else {
    cerr << "Usage: " << argv[0] << " <input file>" << endl;
    exit(0);
  }
  
  // if ( (f = fopen("/home/lino/c++/astorb.dat","r")) != 0 ) {
  // if ( (f = fopen("/home/tricaric/tmp/astorb.dat","r")) != 0 ) {
  
  // if ( (f = fopen(path,"r")) != 0 ) {
  
  // AstorbFile af(&adb,f);
  
  // MPCOrbFile af(&adb);
  
  MPCOrbFile af;
  AsteroidDatabase &adb = *(af.db);
  
  af.SetFileName(path);
  
  af.Open();
  
  af.Read();
  
  af.Close();
  
  cout << "Database entries: " << adb.size() << endl;
  
  unsigned int k;
  
  /* for(k=0;k<adb.size();k++) {  
     if( (adb[k].orb.a > FromUnits(4.8,AU)) && 
     (adb[k].orb.a < FromUnits(5.5,AU)) ) { 
     // cout << adb[k].n  << "  " << adb[k].name << "  " << adb[k].orb.a << endl;
     // printf("%9i %9i\n",k+1,adb[k].n);
     printf("%9i  %f  %e\n",k+1,adb[k].orb.a,adb[k].orb.T);
     }
     }
  */
  
  if (adb.size() > 20) {
    for(k=0;k<20;k++) {  
      cout << adb[k].name << "  time: " << adb[k].orb.epoch.Time() <<  endl;
    } 
  }
  
  return 0;
}
