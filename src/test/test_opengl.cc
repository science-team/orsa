#include <qapplication.h>

#include "config.h"

#include <iostream>

#ifndef HAVE_GL
// dummy main
int main(int argc, char **argv) {
  cerr << "Sorry, OpenGL is not available." << endl;
  return (-1);
}
#else // HAVE_GL

#include <orsa_universe.h>

#include <xorsa_opengl.h>

using namespace std;
using namespace orsa;

class GLW : public XOrsaOpenGLWidget {
 protected:
  void draw() {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE);
    glTranslate(Vector(-1,-1,2));
    glColor4d(1,0,0,0.6);   
    { GLUquadricObj *qobj = gluNewQuadric();
      gluQuadricDrawStyle(qobj,GLU_SILHOUETTE);
      gluSphere(qobj,1.0,72,36);
      gluDeleteQuadric(qobj);
    }
    
    glTranslate(Vector(0,0,3));
    glColor4d(1,1,0,0.45);   
    { GLUquadricObj *qobj = gluNewQuadric();
      gluQuadricDrawStyle(qobj,GLU_SILHOUETTE);
      gluSphere(qobj,0.7,72,36);
      gluDeleteQuadric(qobj);
    }
    glDisable(GL_BLEND);
  }
};

int main(int argc, char **argv) {   
  
  new orsa::Universe();
  
  QApplication a(argc,argv);
  
  if ( !QGLFormat::hasOpenGL() ) {
    qWarning( "This system has no OpenGL support. Exiting." );
    return -1;
  }
  
  GLW * oglw = new GLW;
  
  oglw->projection = OGL_PERSPECTIVE;
  oglw->bool_animate = false;
  //
  oglw->center_rotation_impulse.SetMinMax(0.01,100.0);
  oglw->center_rotation_impulse = 0.1;
  //
  oglw->eye_rotation_impulse.SetMinMax(1.0e-8,100.0); // small minimum limit, useful with big zooms...
  oglw->eye_rotation_impulse    = 0.1;
  //
  oglw->FOV.SetMinMax(1.0/3600.0,150.0);
  oglw->FOV = 50.0;
  //
  oglw->near.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU));
  oglw->near = FromUnits(0.1,AU);
  //
  oglw->far.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU)); 
  oglw->far = FromUnits(20.0,AU);
  //
  oglw->ortho_xy_scale.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU)); 
  oglw->ortho_xy_scale = FromUnits(3.0,AU);
  //
  oglw->ortho_z_near_scale.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU)); 
  oglw->ortho_z_near_scale  = FromUnits(10.0,AU);
  //
  oglw->ortho_z_far_scale.SetMinMax(FromUnits(0.1,AU),FromUnits(20,AU)); 
  oglw->ortho_z_far_scale  = FromUnits(10.0,AU);
  
  oglw->show(); 
  
  a.connect(&a,SIGNAL(lastWindowClosed()),&a,SLOT(quit()));
  return (a.exec());
}

#endif // HAVE_GL
