#include <orsa_universe.h>
#include <orsa_file.h>

using namespace orsa;

#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

int main(int argc, char **argv) {
  
  // Universe z(AUSUNY);
  // Universe z(MKS);
  Universe z(AU,MSUN,DAY);
  
  cout << "G:    " << GetG()    << endl;
  cout << "MSun: " << GetMSun() << endl;
  
  cout << "testing astorb.dat reading capabilities..." << endl;
  
  // FILE *f;
  
  char path[1024];
  // sprintf(path,"/home/lino/c++/astorb.dat");
  // sprintf(path,"/home/tricaric/tmp/astorb.dat");
  if (argc == 2) {
    sprintf(path,argv[1]); 
  } else {
    cerr << "Usage: " << argv[0] << " <input file>" << endl;
    exit(0);
  }
  
  // if ( (f = fopen("/home/lino/c++/astorb.dat","r")) != 0 ) {
  // if ( (f = fopen("/home/tricaric/tmp/astorb.dat","r")) != 0 ) {
  
  // if ( (f = fopen(path,"r")) != 0 ) {
    
  // AstorbFile af(&adb,f);

  // AstorbFile af(&adb);
  AstorbFile af;
  AsteroidDatabase &adb = *(af.db);
  
  af.SetFileName(path);
  
  af.Open();
  
  af.Read();
  
  af.Close();
  
  cout << "Database entries: " << adb.size() << endl;
  
  unsigned int k;
  
  for(k=0;k<adb.size();k++) {  
    if( (adb[k].orb.a > FromUnits(4.8,AU)) && 
	(adb[k].orb.a < FromUnits(5.5,AU)) ) { 
      // cout << adb[k].n  << "  " << adb[k].name << "  " << adb[k].orb.a << endl;
      // printf("%9i %9i\n",k+1,adb[k].n);
      printf("%9i  %f  %e\n",k+1,adb[k].orb.a,adb[k].orb.Period());
    }
  }
  
  return 0;
}
