#include <qapplication.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qstring.h>
#include <qfont.h>
#include <qpushbutton.h>
#include <qlayout.h>

int main(int argc, char **argv) {
  
  QApplication a(argc,argv);
  
  a.connect(&a,SIGNAL(lastWindowClosed()),&a,SLOT(quit()));
  QWidget *mw = new QWidget;
  a.setMainWidget(mw);
  QVBoxLayout *lay = new QVBoxLayout(mw);

  QFont   font; 
  // font.setStyleHint(QFont::Times);
  // font.setFamily("Symbol");
  font.setFamily("Symbol");
  // font.setPointSize(25);
  // a.setFont(font);
  
  QString string;
  // string.sprintf("abcdefghijklmnopqrstuvwxyz");
  string.sprintf("l\nw\nW\na");
  
  QPushButton *pb = new QPushButton(string,mw);
  pb->setFont(font);
  lay->addWidget(pb);
  
  string.sprintf(" w + W + M = l ");
  QPushButton *pb2 = new QPushButton(string,mw);
  pb2->setFont(font);
  lay->addWidget(pb2);
  
  QLabel *label = new QLabel(mw);
  label->setFont(font);
  label->setText("Carpe diem");
  QHBoxLayout *hlay = new QHBoxLayout(lay);
  hlay->addWidget(label);
  
  QLabel *label2 = new QLabel(mw);
  label2->setFont(font);
  label2->setText("...   G<sub>mn</sub>");
  hlay->addWidget(label2);
  hlay->addStretch();
  
  QLabel *r = new QLabel(mw);
  r->setFont(font);
  r->setText("F<sub>mn</sub> = d");
  lay->addWidget(r);
  
  QChar   *uc_omega = new QChar(0x03C9);
  QString suc_omega(uc_omega,1);    
  QLabel *luc_omega = new QLabel(suc_omega,mw);
  lay->addWidget(luc_omega);
  
  QChar uc_mu(0x03BC);
  QChar uc_nu(0x03BD);
  QString s_F_mn("F<sub></sub>");
  s_F_mn.insert(6,uc_mu);
  s_F_mn.insert(7,uc_nu);
  QLabel *uc_F = new QLabel(s_F_mn,mw);
  lay->addWidget(uc_F);
  
  
  mw->show();
  
  return (a.exec());
}
