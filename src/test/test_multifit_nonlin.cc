#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_blas.h>

#include <vector>

using namespace std;

#include <orsa_common.h>
// #include <xorsa_plot_area.h>

// #include <qapplication.h>

#include <iostream>

// **** UPDATE FUN, N_PAR and DF_i !!! ****
//
// #define FUN   par[0]*DF_0+par[1]*DF_1+par[2]*DF_2+par[3]*DF_3+par[4]*DF_4+par[5]*DF_5+par[6]*DF_6+par[7]*DF_7+par[8]*DF_8+par[9]*DF_9+par[10]*DF_10+par[11]*DF_11+par[12]*DF_12+par[13]*DF_13+par[14]*DF_14+par[15]*DF_15
// #define FUN   par[0]*DF_0+par[1]*DF_1+par[2]*DF_2+par[3]*DF_3+par[4]*DF_4+par[5]*DF_5
#define FUN   par[0]*DF_0+par[1]*DF_1+par[2]*DF_2+par[3]*DF_3+par[4]*DF_4+par[5]*DF_5+par[6]*DF_6+par[7]*DF_7+par[8]*DF_8
#define N_PAR 9
// set the unused DF to 1
// enclose each DF into parenthesis
// here: x=e_p, y=D, inc_arg=sin(i/2)
//
#define DF_0   1
#define DF_1   x*x
#define DF_2   y*y
#define DF_3   z*z
#define DF_4   x*x*y*y
#define DF_5   y*y*y*y
#define DF_6   x*x*z*z
#define DF_7   y*y*z*z
#define DF_8   z*z*z*z
#define DF_9   1
#define DF_10  1
#define DF_11  1
#define DF_12  1
#define DF_13  1
#define DF_14  1
#define DF_15  1
//
/* 
   #define DF_0   1
   #define DF_1   x*x
   #define DF_2   z*z
   #define DF_3   x*x*z*z
   #define DF_4   z*z*z*z
   #define DF_5   x
   #define DF_6   z
   #define DF_7   x*z
   #define DF_8   1
   #define DF_9   1
   #define DF_10  1
   #define DF_11  1
   #define DF_12  1
   #define DF_13  1
   #define DF_14  1
   #define DF_15  1
*/
//
/* 
   #define DF_0   1
   #define DF_1   x*x
   #define DF_2   y*y
   #define DF_3   z*z
   #define DF_4   x*x*y*y
   #define DF_5   y*y*y*y
   #define DF_6   x*x*z*z
   #define DF_7   y*y*z*z
   #define DF_8   z*z*z*z
   #define DF_9   x
   #define DF_10  y
   #define DF_11  z
   #define DF_12  1
   #define DF_13  1
   #define DF_14  1
   #define DF_15  1
*/
//
/* // here: x=e_p^2, y=D, inc_arg=sin(i/2)^2
   #define DF_0   (1+inc_arg)
   #define DF_1   (1+inc_arg)*y
   #define DF_2   (1+inc_arg)*y*y
   #define DF_3   (1+inc_arg)*x
   #define DF_4   (1+inc_arg)*x*y
   #define DF_5   (1+inc_arg)*x*y*y
   #define DF_6   1
   #define DF_7   1
   #define DF_8   1
   #define DF_9   1
   #define DF_10  1
   #define DF_11  1
   #define DF_12  1
   #define DF_13  1
   #define DF_14  1
   #define DF_15  1
*/
//
/* #define DF_0   1
   #define DF_1   y
   #define DF_2   y*y
   #define DF_3   x
   #define DF_4   x*y
   #define DF_5   x*y*y
   #define DF_6   inc_arg
   #define DF_7   inc_arg*y
   #define DF_8   inc_arg*y*y
   #define DF_9   inc_arg*x
   #define DF_10  inc_arg*x*y
   #define DF_11  inc_arg*x*y*y
   #define DF_12  1
   #define DF_13  1
   #define DF_14  1
   #define DF_15  1
*/
//
#define LOCAL_PAR double par[N_PAR]={0};

// !! Stringification !! (Do not touch!)
#define STR(X) #X
#define STR_OK(X) STR(X)
#define FUN_STR STR_OK(FUN)

struct data_point {
  double x,y,z,f,sigma;
};

// globally defined
double inc_arg; // (sin(i/2))^2
double inc_arg_2, inc_arg_3, inc_arg_4;

int function_sum_f (const gsl_vector *parameters, void *data_points, gsl_vector *function) {
  
  LOCAL_PAR;
  
  int kk; for (kk=0;kk<N_PAR;kk++) {
    par[kk] = gsl_vector_get(parameters,kk);
  }
  
  // vector<data_point> v = ((struct data *)data_points)->v;
  vector<data_point> &v(*((vector<data_point>*)data_points));
  
  size_t i;
  // size_t n = ((struct data *)data_points)->v.size();
  size_t n = v.size();
  double f;
  double x,y,z;
  
  for (i = 0; i < n; i++) {
    
    x = v[i].x;
    y = v[i].y;
    z = v[i].z;
    
    f = FUN;
    
    gsl_vector_set (function, i, (f-v[i].f)/v[i].sigma);
    
  }
  
  return GSL_SUCCESS;
}


int function_sum_df (const gsl_vector *parameters, void *data_points, gsl_matrix *J) {
  
  LOCAL_PAR;
  
  int kk; for (kk=0;kk<N_PAR;kk++) {
    par[kk] = gsl_vector_get(parameters,kk);
  }
  
  // vector<data_point> v = ((struct data *)data_points)->v;
  vector<data_point> &v(*((vector<data_point>*)data_points));
  
  size_t i;
  // size_t n = ((struct data *)data_points)->v.size();
  size_t n = v.size();
  double x,y,z;
  double sigma;
  
  for (i = 0; i < n; i++) {
    
    x = v[i].x;
    y = v[i].y;
    z = v[i].z;
    
    sigma = v[i].sigma;
    
    if (N_PAR>0)  gsl_matrix_set (J, i, 0,  DF_0/ sigma);
    if (N_PAR>1)  gsl_matrix_set (J, i, 1,  DF_1/ sigma);
    if (N_PAR>2)  gsl_matrix_set (J, i, 2,  DF_2/ sigma);
    if (N_PAR>3)  gsl_matrix_set (J, i, 3,  DF_3/ sigma);
    if (N_PAR>4)  gsl_matrix_set (J, i, 4,  DF_4/ sigma);
    if (N_PAR>5)  gsl_matrix_set (J, i, 5,  DF_5/ sigma);
    if (N_PAR>6)  gsl_matrix_set (J, i, 6,  DF_6/ sigma);
    if (N_PAR>7)  gsl_matrix_set (J, i, 7,  DF_7/ sigma);
    if (N_PAR>8)  gsl_matrix_set (J, i, 8,  DF_8/ sigma);
    if (N_PAR>9)  gsl_matrix_set (J, i, 9,  DF_9/ sigma);
    if (N_PAR>10) gsl_matrix_set (J, i, 10, DF_10/sigma);
    if (N_PAR>11) gsl_matrix_set (J, i, 11, DF_11/sigma);
    if (N_PAR>12) gsl_matrix_set (J, i, 12, DF_12/sigma);
    if (N_PAR>13) gsl_matrix_set (J, i, 13, DF_13/sigma);
    if (N_PAR>14) gsl_matrix_set (J, i, 14, DF_14/sigma);
    if (N_PAR>15) gsl_matrix_set (J, i, 15, DF_15/sigma);
    
  }
  
  return GSL_SUCCESS;
}

int function_sum_fdf (const gsl_vector *parameters, void *data_points, gsl_vector *function, gsl_matrix *J) {
  
  function_sum_f  (parameters, data_points, function);
  function_sum_df (parameters, data_points, J);
  
  return GSL_SUCCESS;
}

void print_state (size_t iter, gsl_multifit_fdfsolver * s) {
  
  printf ("iter: %3u  ",iter);
  int kk; for (kk=0;kk<N_PAR;kk++) {
    printf("par[%i]= %.4g  ",kk,gsl_vector_get(s->x,kk));
  }
  printf("|f(x)| = %g\n",gsl_blas_dnrm2(s->f));
  
}


int main(int argc, char **argv) {
  
  setlinebuf(stdout);
  setlinebuf(stderr);
  
  const gsl_multifit_fdfsolver_type *T;
  gsl_multifit_fdfsolver *s;
  
  // 
  // size_t i, iter = 0;
  
  // const size_t n = N;
  // const size_t p = 3;
  
  const size_t n_parameters = N_PAR;
  
  gsl_matrix *covar = gsl_matrix_alloc (n_parameters, n_parameters);
  
  // double y[N], sigma[N];
  
  // struct data d = { n, y, sigma};
  
  vector<data_point> data_points_vector;
  // double inclination_DEG=0;
  
  gsl_multifit_function_fdf f;
  
  // const gsl_rng_type * type;
  // gsl_rng * r;
  
  // gsl_rng_env_setup();
  
  // type = gsl_rng_default;
  // r = gsl_rng_alloc (type);
  
  
  /* This is the data to be fitted */
  data_points_vector.resize(0);
  
  if (argc==2) {
    // use file
    
    FILE *file = fopen(argv[1],"r");
    if (file==0) {
      cerr << "Can't open input file " << argv[1] << endl;
      exit(1);
    }
    
    // inclination_DEG = atof(argv[2]);
    // cerr << "inclination (DEG): " << inclination_DEG << endl;
    
    /* inc_arg   = sin(inclination_DEG*(orsa::pi/180));
       inc_arg_2 = tan(2.0*inclination_DEG*(orsa::pi/180));
       inc_arg_3 = sin((30.5-inclination_DEG)*(orsa::pi/180));
    */
    
    double x,y,z,T,g;
    double dummy;
    int    ok;
    
    double sigma_data = 1.0e-2;
    data_point pt;
    
    do {
      
      /*
	ok = fscanf(file,"%lf %lf %lf %lf %lf %lf %lf %lf",
	&dummy,&y,&T,&z,&x,&dummy,&dummy,&dummy);
      */
      
      ok = fscanf(file,"%lf %lf %lf %lf %lf %lf %lf %lf",
		  &dummy,&T,&y,&z,&x,&g,&dummy,&dummy);
      
      // D,e,i,f...
      /* ok = fscanf(file,"%lf %lf %lf %lf %lf %lf",
	 &z,&x,&y,&g,&dummy,&dummy);
      */
      
      /* 
	 ok = fscanf(file,"%lf %lf %lf %lf",
	 &z,&x,&y,&g);
      */
      
      T *= (360*60*60);
      // g  = 1/g*(360*60*60);
      // g *=(360*60*60);
      
      // Venus  : 0.00195972
      // Jupiter: 0.27835
      // Saturn : 0.280507
      // Uranus : 0.21964
      // Neptune: 0.37379
      
      /* 
	 x /= 0.15;
	 y /= 0.6;
	 z  = ((z/2.0)*(orsa::pi/180.0)*0.2783)/0.15; // from Erdi theory
      */
      
      /* 
	 x /= 0.15;
	 y /= 0.6;
	 z  = ((z/2.0)*(orsa::pi/180.0)*0.37379)/0.15; // from Erdi theory
      */
      
      /* 
	 x /= 0.15;
	 y /= 0.6;
	 z  = ((z/2.0)*(orsa::pi/180.0)*0.21964)/0.15; // from Erdi theory
      */
      
      x /= 0.15;
      y /= 0.6; // funziona se viene passato direttamente il seno
      // y  = sin(y*orsa::pi/180)/0.6; // leggi gradi, usa seno...
      z  = ((z/2.0)*(orsa::pi/180.0)*0.00195972)/0.15; // from Erdi theory
      
      if (ok != EOF) {
	
	pt.x = x;   // x=e_p
	pt.y = y;   // y=sin_ip
	pt.z = z;   // z=D
	
	pt.f = T;
	// pt.f = g;
	
	cerr << "pt.f = " << pt.f << endl;
	
	// pt.f *= 1e5;
	
	pt.sigma = sigma_data;
     	
	data_points_vector.push_back(pt);
      }
      
    } while (ok != EOF);
    
  } else {
    
    // demo data
    
    LOCAL_PAR;
    
    // inc_arg   = 0.2;
    // inc_arg_2 = 0.1;
    
    // random number generator
    const int random_seed = 124323;
    gsl_rng *rnd;
    rnd = gsl_rng_alloc(gsl_rng_gfsr4);
    gsl_rng_set(rnd,random_seed);
    { 
      double x,y,z,f;
      if (N_PAR>0) par[0] = 21;
      if (N_PAR>1) par[1] = -0.41;
      if (N_PAR>2) par[2] = 171;
      if (N_PAR>3) par[3] = -0.000041;
      if (N_PAR>4) par[4] = 321;
      if (N_PAR>5) par[5] = .41;
      if (N_PAR>6) par[6] = 1271;
      if (N_PAR>7) par[7] = -0.03;
      if (N_PAR>8) par[8] = 15.0001;
      if (N_PAR>9) par[9] = -15.0001;
      
      double sigma = 1.0;
      data_point pt;
      size_t i;
      for (i = 0; i < 1000; i++) {
	x = gsl_rng_uniform(rnd);
	y = gsl_rng_uniform(rnd);
	z = gsl_rng_uniform(rnd);
	
      	f = FUN;
	
	// cerr << "z: " << z << endl;
	
       	pt.x=x;
	pt.y=y;
	pt.z=z;
	pt.f=f;
	pt.sigma=sigma;
	
	data_points_vector.push_back(pt);
      }
    }
  }
  
  // cerr << "inc_arg: " << inc_arg << endl;
  
  double parameters_initial_values[n_parameters] = {0};
  
  if (N_PAR>0) parameters_initial_values[0] = 1.0;
  if (N_PAR>1) parameters_initial_values[1] = 1.0;
  if (N_PAR>2) parameters_initial_values[2] = 1.0;
  
  /* 
     if (N_PAR>0) parameters_initial_values[0] = 146.29;
     if (N_PAR>1) parameters_initial_values[1] =  -2.7313;
     if (N_PAR>2) parameters_initial_values[2] = -22.85;
     if (N_PAR>3) parameters_initial_values[3] =  -0.021251;
     if (N_PAR>4) parameters_initial_values[4] =   7.7054;
     if (N_PAR>5) parameters_initial_values[5] =   1.5939;
     if (N_PAR>6) parameters_initial_values[6] =  10.172;
     if (N_PAR>7) parameters_initial_values[7] =   1.235;
     if (N_PAR>8) parameters_initial_values[8] =  -4.1845;
  */
  
  /* 
     if (N_PAR>0) parameters_initial_values[0] = 360.07;
     if (N_PAR>1) parameters_initial_values[1] = -11.628;
     if (N_PAR>2) parameters_initial_values[2] = -127.34;
     if (N_PAR>3) parameters_initial_values[3] = 91.718;
     if (N_PAR>4) parameters_initial_values[4] = 1.9484;
     if (N_PAR>5) parameters_initial_values[5] = 8.3105;
     if (N_PAR>6) parameters_initial_values[6] = -40.444;
     if (N_PAR>7) parameters_initial_values[7] = -136.03;
     if (N_PAR>8) parameters_initial_values[8] = -37.235;
  */
  
  gsl_vector_view x = gsl_vector_view_array (parameters_initial_values,n_parameters);
  
  f.f   = &function_sum_f;
  f.df  = &function_sum_df;
  f.fdf = &function_sum_fdf;
  f.n   = data_points_vector.size();
  f.p   = n_parameters;
  f.params = &data_points_vector;
  
  T = gsl_multifit_fdfsolver_lmsder;
  s = gsl_multifit_fdfsolver_alloc (T, f.n, n_parameters);
  gsl_multifit_fdfsolver_set (s, &f, &x.vector);
  
  
  size_t iter=0;
  int status;
  print_state (iter, s);
  
  do {
    iter++;
    status = gsl_multifit_fdfsolver_iterate (s);
    
    // printf ("status = %s\n", gsl_strerror (status));
    
    print_state (iter, s);
    
    if (status)
      break;
    
    // status = gsl_multifit_test_delta (s->dx, s->x,1e-4, 1e-4);
    status = gsl_multifit_test_delta (s->dx,s->x,0.0,1.0e-6);
    
  } while (status == GSL_CONTINUE && iter < 500);
  
  gsl_multifit_covar (s->J, 0.0, covar);
  
#define FIT(i) gsl_vector_get(s->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))
  
  printf("\n Function: %s\n\n",FUN_STR);
  int kk; for (kk=0;kk<N_PAR;kk++) {
    printf("par[%i] = %10.5g +/- %10.5g\n",kk,FIT(kk),ERR(kk));
  }
  /* printf("a = %10.5g +/- %10.5g\n",FIT(0),ERR(0));
     printf("b = %10.5g +/- %10.5g\n",FIT(1),ERR(1));
     printf("c = %10.5g +/- %10.5g\n",FIT(2),ERR(2));
     printf("d = %10.5g +/- %10.5g\n",FIT(3),ERR(3));
     printf("e = %10.5g +/- %10.5g\n",FIT(4),ERR(4)); */
  // printf("f = %10.5g +/- %10.5g\n",FIT(5),ERR(5));
  // printf("g = %10.5g +/- %10.5g\n",FIT(6),ERR(6));
  
  printf ("\n status = %s\n\n", gsl_strerror(status));
  
  {
    unsigned int r;
    double x,y,z,f,fun;
    LOCAL_PAR;
    int kk; for (kk=0;kk<N_PAR;kk++) {
      par[kk] = FIT(kk);
      // printf("par[%i] = %10.5g +/- %10.5g\n",kk,FIT(kk),ERR(kk));
    }
    char filename[100];
    sprintf(filename,"fit.dat");
    cerr << "writing file " << filename << endl;
    FILE *fit = fopen(filename,"w");
    for(r=0;r<data_points_vector.size();r++) {
      x = data_points_vector[r].x;
      y = data_points_vector[r].y;
      z = data_points_vector[r].z;
      f = data_points_vector[r].f;
      fun = FUN;
      fprintf(fit,"%g  %g  %g  %g  %g\n",x,y,z,f,fun);
    }
    fclose(fit);
  }
  
  return 0;
}


