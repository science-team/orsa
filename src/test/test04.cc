
#include <orsa_universe.h>
#include <orsa_file.h>
#include <orsa_units.h>

using namespace orsa;

#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

int main(int argc, char **argv) {
   
  if (argc != 2) {
    cerr << "Usage: " << argv[0] << " <JPL ephem file>" << endl;
    exit(0);
  }
  
  // new Universe(AU,MSUN,YEAR,ECLIPTIC,Simulated);
  new Universe(AU,MSUN,YEAR,Simulated,ECLIPTIC);
  
  JPLFile jf(argv[1]);
  
  Vector r,v;
  
  Date date;
  
  date.SetGregor(2002,5,2);
  
  printf("julian date: %f\n",date.GetJulian());
  
  jf.GetEph(date,EARTH,r,v);
  
  printf("r: %20.12f  x: %20.12f y: %20.12f z: %20.12f\n",r.Length(),r.x,r.y,r.z);
  printf("v: %20.12f  x: %20.12f y: %20.12f z: %20.12f\n",v.Length(),v.x,v.y,v.z);
  
  cerr << "GetTag[AU] = " << jf.GetTag("AU") << endl;
  
  cerr << "GetMSun_MKS() = " << jf.GetMSun_MKS() << endl;
  
  cerr << "GetMass(VENUS) = " << jf.GetMass(VENUS) << endl;
  
  // stress
  
  while (1) {
    Date d;
    JPLBody *j = new JPLBody(EARTH,d);
    delete j;
  }
  
  return 0;
}
