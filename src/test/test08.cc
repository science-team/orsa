#include <orsa_units.h>
#include <orsa_universe.h>
#include <iostream>
#include <cstdio>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>

using namespace std;
using namespace orsa;

void print_again(const Date &date, const TimeScale ts) {
  
  const double julian = date.GetJulian(ts);
  
  int y,m,d;
  date.GetGregor(y,m,d,ts);
  
  double frac = date.GetDayFraction(ts);
  
  int H,M,S;
  frac *= 24;
  H = (int)floor(frac);
  frac -= H;
  frac *= 60;
  M = (int)floor(frac);
  frac -= M;
  frac *= 60;
  S = (int)floor(frac);
  
  printf(" (%s) \t%i/%02i/%02i  %02i:%02i:%06.3f  JD: %f  MJD: %f\n",
	 TimeScaleLabel(ts).c_str(),
	 y,m,d,H,M,frac,
	 julian,julian-2400000.5);
  
}

int main() {
  
  new Universe(AU,MSUN,YEAR,Real,ECLIPTIC,TAI);
  
  struct timeval  tv;
  struct timezone tz;
  gettimeofday(&tv,&tz);
  
  printf(" gettimeofday........: %i.%06i\n",(int)tv.tv_sec,(int)tv.tv_usec);
  
  const time_t tt_now = tv.tv_sec;
  
  struct tm *tm_struct = gmtime(&tt_now);
  
  printf(" gmtime..............: %i/%02i/%02i %02i:%02i:%02i\n",
	 1900+tm_struct->tm_year,
	 1+tm_struct->tm_mon,
	 tm_struct->tm_mday,
	 tm_struct->tm_hour,
	 tm_struct->tm_min,
	 tm_struct->tm_sec);
  
  cout << " ctime().............: " << ctime(&tt_now);
  cout << " asctime(localtime()): " << asctime(localtime(&tt_now));
  cout << " asctime(gmtime())...: " << asctime(gmtime(&tt_now));
  cout << endl;
  
  // Date date(UTC);
  Date date;
  date.SetGregor(1900+tm_struct->tm_year,
		 1+tm_struct->tm_mon,
		 tm_struct->tm_mday+((tm_struct->tm_sec+(tv.tv_usec*1.0e-6))/86400.0 +
				     tm_struct->tm_min/1440.0 + 
				     tm_struct->tm_hour/24.0),
		 UTC);
  
  double J_UTC,J_UT,J_TAI,J_TDT,J_GPS;
  
  /* date.ConvertToTimeScale(UTC);  J_UTC = date.GetJulian();  print_again(date);
     date.ConvertToTimeScale(UT);   J_UT  = date.GetJulian();  print_again(date);
     date.ConvertToTimeScale(TAI);  J_TAI = date.GetJulian();  print_again(date);
     date.ConvertToTimeScale(TDT);  J_TDT = date.GetJulian();  print_again(date);
     date.ConvertToTimeScale(GPS);  J_GPS = date.GetJulian();  print_again(date);
  */
  //
  J_UTC = date.GetJulian(UTC);  print_again(date,UTC);
  J_UT  = date.GetJulian(UT);   print_again(date,UT);
  J_TAI = date.GetJulian(TAI);  print_again(date,TAI);
  J_TDT = date.GetJulian(TDT);  print_again(date,TDT);
  J_GPS = date.GetJulian(GPS);  print_again(date,GPS);
  
  cout << endl;
  
  printf(" (UTC) - (UT)  = %06.3f [s]\n",            (J_UTC-J_UT )*86400);
  printf(" (TAI) - (UTC) = %06.3f [s] (tabulated)\n",(J_TAI-J_UTC)*86400);
  printf(" (TDT) - (UTC) = %06.3f [s]\n",            (J_TDT-J_UTC)*86400);
  printf(" (TAI) - (UT)  = %06.3f [s]\n",            (J_TAI-J_UT )*86400);
  printf(" (TDT) - (UT)  = %06.3f [s] (tabulated)\n",(J_TDT-J_UT )*86400);
  printf(" (TDT) - (TAI) = %06.3f [s] (fixed)\n",    (J_TDT-J_TAI)*86400);
  //
  printf(" (GPS) - (UTC) = %06.3f [s]\n",            (J_GPS-J_UTC)*86400);
  printf(" (TAI) - (GPS) = %06.3f [s]\n",            (J_TAI-J_GPS)*86400);
  printf(" (TDT) - (GPS) = %06.3f [s] (fixed)\n",    (J_TDT-J_GPS)*86400);
  printf(" (GPS) - (UT)  = %06.3f [s]\n",            (J_GPS-J_UT )*86400);
  
  cout << endl;
  
  cout << "NB: (UT) = (UT1) and (TDT) = (TT) = (ET)." << endl;
  
  return 0;
}
