/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <iostream>
#include <complex>

#include <fftw.h>
#include <gsl/gsl_heapsort.h>

#include "orsa_fft.h"
#include "orsa_common.h"

using namespace std;

namespace orsa {
  
  double  norm( const fftw_complex z) {
    return sqrt( z.re * z.re + z.im * z.im );
  }
  
  double  norm_sq( const fftw_complex z) {
    return ( z.re * z.re + z.im * z.im );
  }
  
  
  // genuine phi, without windowing
  //! Discrete Fourier Transform
  fftw_complex phi(double omega, fftw_complex in[], const int size) {
    
    fftw_complex result;
    result.re = 0;
    result.im = 0;
    
    double arg,c,s;
    int k;
    
    for(k=0;k<size;k++) {  
      arg = twopi*k*omega;
      c = cos(arg);
      s = sin(arg);
      result.re += in[k].re * c + in[k].im * s;
      result.im -= in[k].re * s - in[k].im * c;            
    }
    
    double scale = (double)size;
    result.re /= scale;
    result.im /= scale;
    
    // OUT HERE!
    return result; 
  }
  
  
  // phi with Hanning windowing
  //! Discrete Fourier Transform with Hanning windowing
  fftw_complex phi_Hanning(double omega, fftw_complex in[], const int size) {
    
    fftw_complex result;
    result.re = 0;
    result.im = 0;
    
    double arg,c,s;
    int k;
    double window_factor;
    
    for(k=0;k<size;k++) {  
      arg = twopi*k*omega;
      c = cos(arg);
      s = sin(arg);
      window_factor = (1-cos(k*twopi/size));
      result.re += window_factor * (in[k].re * c + in[k].im * s);
      result.im -= window_factor * (in[k].re * s - in[k].im * c);
    }
    
    double scale = (double)size;
    result.re /= scale;
    result.im /= scale;
    
    // OUT HERE!
    return result; 
  }
  
  
  //! amplitude for spectrum, without windowing
  double phi_amp(double omega, fftw_complex in[], const int size) {
    return sqrt( norm_sq(phi( omega,&in[0],size)) + 
		 norm_sq(phi(-omega,&in[0],size)) );
  }
  
  
  //! amplitude for spectrum, with Hanning windowing
  double phi_Hanning_amp(double omega, fftw_complex in[], const int size) {
    return sqrt( norm_sq(phi_Hanning( omega,&in[0],size)) + 
		 norm_sq(phi_Hanning(-omega,&in[0],size)) );
  }
  
  
  double phi_gsl (double x, void * params) {
    
    struct gsl_d1_minimization_parameters * p = (gsl_d1_minimization_parameters *) params;
    
    // return  (-phi_Hanning_amp(x, p->pointer_points_sequence, p->size));
    return  (-phi_amp(x, p->pointer_points_sequence, p->size));
    
  }
  
  double phi_gsl_two (double x, void * params) {
    
    struct gsl_d1_minimization_parameters * p = (gsl_d1_minimization_parameters *) params;
    
    // return  (-phi_Hanning_amp(x, p->pointer_points_sequence, p->size));
    return  (-norm(phi(x, p->pointer_points_sequence, p->size)));
  }
  
  double phi_Hanning_gsl (double x, void * params) {
    
    struct gsl_d1_minimization_parameters * p = (gsl_d1_minimization_parameters *) params;
    
    return  (-phi_Hanning_amp(x, p->pointer_points_sequence, p->size));
    // return  (-phi_amp(x, p->pointer_points_sequence, p->size));
    
  }
  
  
  typedef struct binamp {
    int     bin;
    double  amp;
  };
  
  
  //! sort binamp struct from the bigger to the smaller...
  int compare_binamp(const binamp *a, const binamp *b) {
    
    if (( (*a).amp - (*b).amp) < 0)
      return  1;
    if (( (*a).amp - (*b).amp) > 0)
      return -1;
    
    return 0;
  }
  
  
  double psd_max_again(const fftw_complex *transformed_signal, const int size) {
    
    vector<double> psd_plus((size-1)/2), psd_minus((size-1)/2);
    double Nyquist=0;
    
    int k;
    
    const double DC = norm(transformed_signal[0])/size;
    for (k=0;k<(size-1)/2;k++)
      psd_plus[k]  = norm(transformed_signal[k+1])/size;
    for (k=0;k<(size-1)/2;k++)
      psd_minus[k] = norm(transformed_signal[size-(k+1)])/size;
    if ( (size%2) == 0) // size is even
      Nyquist = norm(transformed_signal[size/2])/size;  // Nyquist freq.
    
    // cerr << "...some test data: " << DC << "  " << psd_plus[0]  << "  " << psd_plus[1]  << "  " << psd_plus[2]  << "  " << psd_plus[3]  << "  " << psd_plus[4] << endl; 
    
    // DEBUG: DUMP DATA TO FILE
    if (0) {
      static int filenum=0;
      char filename[20];
      sprintf(filename,"dump_psd_%02i.dat",filenum);
      cerr << "data dump on file " << filename << endl;
      FILE *fp = fopen(filename,"w");
      if (fp!=0) {
	int l;
	for (l=psd_minus.size()-1;l>=0;l--) fprintf(fp,"%i  %g\n",-(l+1),psd_minus[l]);
	fprintf(fp,"%i  %g\n",0,DC);
	for (l=0;l<(int)psd_plus.size(); l++) fprintf(fp,"%i  %g\n", (l+1),psd_plus[l]);
	if ( (size%2) == 0) fprintf(fp,"%i  %g\n",size/2,Nyquist);
      }
      fclose(fp);
      filenum++;
    }
    
    // psd.resize(size);
    //
    /* psd[0] = norm(transformed_signal[0])/size;
       for (k=1;k<(size-1)/2;k++)
       psd[k] = norm(transformed_signal[k])/size;
       for (k=(size/2)+1;k<size;k++)
       psd[k] = norm(transformed_signal[size-k])/size;
       if ( (size%2) == 0) // size is even
       psd[size/2] = norm(transformed_signal[size/2])/size;  // Nyquist freq.
    */
    
    double maxpow=DC;
    int bin=0;
    
    for (k=1;k<(size-3)/2;k++) {
      if ( (psd_plus[k] > psd_plus[k-1]) && 
	   (psd_plus[k] > psd_plus[k+1]) &&
	   (psd_plus[k] > maxpow) ) {
	maxpow = psd_plus[k];
	bin    = k+1;
      }
    }
    
    if ( (psd_plus[0] > DC) && 
	 (psd_plus[0] > psd_plus[1]) &&
	 (psd_plus[0] > maxpow) ) {
      maxpow = psd_plus[0];
      bin    = 1;
    }
    
    for (k=1;k<(size-3)/2;k++) {
      if ( (psd_minus[k] > psd_minus[k-1]) && 
	   (psd_minus[k] > psd_minus[k+1]) &&
	   (psd_minus[k] > maxpow) ) {
	maxpow = psd_minus[k];
	bin    = -(k+1);
      }
    }
    
    if ( (psd_minus[0] > DC) && 
	 (psd_minus[0] > psd_minus[1]) &&
	 (psd_minus[0] > maxpow) ) {
      maxpow = psd_minus[0];
      bin    = -1;
    }
    
    if ( (DC > psd_plus[0]) && 
	 (DC > psd_minus[0]) &&
	 (DC > maxpow) ) {
      maxpow = DC;
      bin    = 0;
    }
    
    // TODO: Nyquist...
    
    // cerr << " psd_max_again: returning " << ((double)bin/(double)size) << "   amp: " << maxpow << "        bin: " << bin << endl;
    
    // DEBUG: DUMP DATA TO FILE
    if (0) {
      int filenum=0;
      char filename[20];
      cerr << "DC:   " << DC << endl;
      cerr << "size: " << size << endl;
      sprintf(filename,"dump_psd_%02i.dat",filenum);
      cerr << "data dump on file " << filename << endl;
      FILE *fp = fopen(filename,"w");
      if (fp!=0) {
	int l;
	for (l=psd_minus.size()-1;l>=0;l--) fprintf(fp,"%i  %g\n",-(l+1),psd_minus[l]);
	fprintf(fp,"%i  %g\n",0,DC);
	for (l=0;l<(int)psd_plus.size(); l++) fprintf(fp,"%i  %g\n", (l+1),psd_plus[l]);
	if ( (size%2) == 0) fprintf(fp,"%i  %g\n",size/2,Nyquist);
      }
      fclose(fp);
      filenum++;
    }
    
    if (maxpow>0) return ((double)bin/(double)size);
    /* else { 
       cerr << "\n        *\n        **\n        ***\n        ****\n*************\n**************  WARNING!!! peak don't found... returning (-1)!!\n*************\n        ****\n        ***\n        **\n        *\n" << endl;
       return (-1);
       }
    */
    
    return ((double)bin/(double)size);
  }
  
  void psd_max_again_many(const fftw_complex *transformed_signal, const int size, vector<double> &candidate, const unsigned int nfreq) {
    
    vector<double> psd_plus((size-1)/2), psd_minus((size-1)/2);
    double Nyquist=0;
    
    int k;
    
    const double DC = norm(transformed_signal[0])/size;
    for (k=0;k<(size-1)/2;k++)
      psd_plus[k]  = norm(transformed_signal[k+1])/size;
    for (k=0;k<(size-1)/2;k++)
      psd_minus[k] = norm(transformed_signal[size-(k+1)])/size;
    if ( (size%2) == 0) // size is even
      Nyquist = norm(transformed_signal[size/2])/size;  // Nyquist freq.
    
    vector<binamp> vec_binamp;
    binamp ba;
    
    for (k=1;k<(size-3)/2;k++) {
      if ( (psd_plus[k] > psd_plus[k-1]) && 
	   (psd_plus[k] > psd_plus[k+1]) ) {
	// candidate.push_back((double)(k+1)/(double)size);
	ba.bin = k+1;
	ba.amp = psd_plus[k];
	vec_binamp.push_back(ba);
      }
    }
    
    if ( (psd_plus[0] > DC) && 
	 (psd_plus[0] > psd_plus[1]) ) {
      // candidate.push_back(1.0/(double)size);
      ba.bin = 1;
      ba.amp = psd_plus[0];
      vec_binamp.push_back(ba); 
    }
    
    for (k=1;k<(size-3)/2;k++) {
      if ( (psd_minus[k] > psd_minus[k-1]) && 
	   (psd_minus[k] > psd_minus[k+1]) ) {
	// candidate.push_back((double)-(k+1)/(double)size);
    	ba.bin = -(k+1);
	ba.amp = psd_minus[k];
	vec_binamp.push_back(ba);  
      }
    }
    
    if ( (psd_minus[0] > DC) && 
	 (psd_minus[0] > psd_minus[1]) ) {
      // candidate.push_back(-1.0/(double)size);
      ba.bin = -1;
      ba.amp = psd_minus[0];
      vec_binamp.push_back(ba);
    }
    
    if ( (DC > psd_plus[0]) && 
	 (DC > psd_minus[0]) ) {
      // candidate.push_back(0.0);
      ba.bin = 0;
      ba.amp = DC;
      vec_binamp.push_back(ba);
    }
    
    
    unsigned int bsize = vec_binamp.size();
    binamp *ptr_binamp;
    ptr_binamp = (binamp *) malloc(bsize*sizeof(binamp));
    { 
      unsigned int k;
      for (k=0;k<bsize;k++) {
	ptr_binamp[k].bin = vec_binamp[k].bin;
	ptr_binamp[k].amp = vec_binamp[k].amp;
      }
    }
    gsl_heapsort(ptr_binamp,bsize,sizeof(binamp),(gsl_comparison_fn_t)compare_binamp);
    
    candidate.clear();
    
    {
      unsigned int k;
      for (k=0;k<nfreq;k++) {
	candidate.push_back((double)ptr_binamp[k].bin/(double)size);
      } 
    }
    
    free(ptr_binamp);
  }
  
  
  // rough estimate of the PSD max freq (the same as bracket...)
  double psd_max(const fftw_complex *transformed_signal, const int size) {
    
    vector<double> psd;
    
    int k;
    
    psd.resize(size/2+1);
    psd[0] = norm(transformed_signal[0])/size;
    for (k=1;k<(size+1)/2;k++)
      psd[k] = sqrt(norm_sq(transformed_signal[k])+norm_sq(transformed_signal[size-k]))/size;
    if ( (size%2) == 0) // size is even
      psd[size/2] = norm(transformed_signal[size/2])/size;  // Nyquist freq.
    
    /* cerr << " === DUMP PSD ===" << endl;
       for (k=0;psd.size();++k) {
       printf("PSD[%05i]=%g\n",k,psd[k]);
       }
    */
    
    double maxamp=0;
    int bin=0;
    unsigned int l;
    //
    double found=false;
    if ( (psd[0] > psd[1]) &&
	 (psd[0] > maxamp) ) {
      maxamp = psd[0];
      bin    = 0;
      found  = true;
    }
    //
    for (l=1;l<(psd.size()-1);l++) {
      if ( ( (psd[l] > psd[l-1]) && (psd[l] > psd[l+1]) ) &&
	   (psd[l] > maxamp) ) {
	maxamp = psd[l];
	bin    = l;
	found  = true;
      }
    }
    
    /* for (l=1;l<psd.size();l++) {
       if (psd[l] > maxamp) {
       maxamp = psd[l];
       bin    = l;
       }
       }
    */ 
    
    // DEBUG: DUMP DATA TO FILE
    if (0) {
      int filenum=0;
      char filename[20];
      sprintf(filename,"dump_psd_%02i.dat",filenum);
      cerr << "data dump on file " << filename << endl;
      FILE *fp = fopen(filename,"w");
      if (fp!=0) {
	for (l=0;l<psd.size();l++) {
	  fprintf(fp,"%i  %g\n",l,psd[l]);
	}
      }
      fclose(fp);
      filenum++;
    }
    
    // cerr << " psd_max: returning " << ((double)bin/(double)size) << "   amp: " << maxamp << "        bin: " << bin << "  found: " << found << endl;
    
    if (found) return ((double)bin/(double)size);
    else { 
      cerr << "\n        *\n        **\n        ***\n        ****\n*************\n**************  WARNING!!! peak don't found... returning (-1)!!\n*************\n        ****\n        ***\n        **\n        *\n" << endl;
      return (-1);
    }
  }
  
  
  void apply_window(fftw_complex *signal_win, fftw_complex *signal, int size) {
    
    int j;
    double win_coeff,arg;
    
    for(j=0;j<size;j++) {
      
      arg = twopi*j/size;
      win_coeff = (1. - cos(arg));
      
      signal_win[j].re = signal[j].re * win_coeff;
      signal_win[j].im = signal[j].im * win_coeff;
      
    }
    
  }
  
  
  void amph(double *amp, double *phase, fftw_complex *phiR, fftw_complex *phiL, double freq, fftw_complex *in, int size) {
    
    *phiR = phi(freq, in,size);
    *phiL = phi(-freq,in,size);
    
    *amp   = norm(*phiR);
    *phase = secure_atan2(phiR->im,phiR->re);
    
    // cerr << "amph() DUMP:   amp = " << *amp << "  phase = " << *phase << endl;
    
  }
  
  double accurate_peak(double left, double center, double right, fftw_complex *in, int size) {
    
    // check values 
    // if ( (center<0) || (center>0.5) ) return center; // ERROR...
    if ( (center<(-0.5)) || (center>0.5) ) { 
      cerr << "warning!! Peak out of range!" << endl;
      return center; // ERROR...
    }
    
    // int    c_iter=0, l_iter=0, r_iter=0, max_iter = 100;
    int    max_iter = 100;
    double center_amp, left_amp, right_amp;
    
    left_amp   = norm(phi(left,  in,size));
    right_amp  = norm(phi(right, in,size));
    center_amp = norm(phi(center,in,size));
    
    // TODO: check for gsl peak search conditions!!!
    
    // gsl stuff
    gsl_d1_minimization_parameters par;
    gsl_function F;
    gsl_min_fminimizer *s;
    const gsl_min_fminimizer_type *T;
    //
    T = gsl_min_fminimizer_goldensection;  
    s = gsl_min_fminimizer_alloc(T);
    //
    par.size = size;
    par.pointer_points_sequence = &in[0];
    //
    F.function = &phi_gsl_two;
    F.params   = &par;
    //
    gsl_min_fminimizer_set(s,&F,center,left,right);
    
    double epsrel = 1.0e-10; // for the d=1 minimization
    double epsabs = 0.0;     // for the d=1 minimization
    
    int iter = 0;
    int status;
    do { 
      iter++;
      
      status = gsl_min_fminimizer_iterate (s);
      //
      center = gsl_min_fminimizer_minimum (s);
      left   = gsl_min_fminimizer_x_lower (s);
      right  = gsl_min_fminimizer_x_upper (s);
      // delta     = range_stop - range_start;
      // minimize!
      status = gsl_min_test_interval(left,right,epsrel,epsabs);
      
      // post-minimization...
      /* printf("%5d [%f, %f] %.7f %.7e  bin:%i\n", 
	 iter,range_start,range_stop,test_peak.frequency,range_stop-range_start,bin);
      */
    } while ((status == GSL_CONTINUE) && (iter < max_iter));
    
    return (center);
  }

  
  FFT::FFT(OrbitStream &o, FFTPowerSpectrum &psd, vector<Peak> &pks, FFTDataStream &rds) {
    
    os = &o;
    //
    fps = &psd;
    fps->resize(0);
    //
    reconstructed_data_stream = &rds;
    
    peaks = &pks;
    peaks->resize(0);
    
    default_peak_reset_frequency = 1.0e-100;
    default_peak_reset_amplitude = 1.0e-4;
    default_peak_reset_phase     = 0.0;
    
    HiResSpectrum = false;
    
    relative_amplitude = 0.05;
    //
    // freq_start = 0.0;
    // freq_stop  = 100.0;
    
    // gsl stuff
    T = gsl_min_fminimizer_goldensection;  
    s = gsl_min_fminimizer_alloc (T);
    
    // defaults
    // search_type = HK;
    // window_type = NONE;
    // algorithm_type = FourierTransformPeaks;
    
    nfreq = 4;
    
  }
  
  
  void FFT::FillDataStream(FFTSearch s) {
    
    // cerr << "Filling data stream..." << endl;
    
    if (s == HK) {
      
      // FFTDataStream  &fds  = *data_stream;
      OrbitStream    &ostr = *os;
      
      data_stream.timestep = ostr.timestep;
      // cerr << "data_stream timestep: " << data_stream.timestep << endl;
      
      // please, handle with care the reserve() method!
      // first time allocation...
      // this reserve call seems to be buggy, with segfaults...
      // if (data_stream.size() == 0) data_stream.reserve(ostr.size());
      // clean vector...
      data_stream.resize(0);
      
      FFTDataStructure tmp_ds;
      
      // cerr << "size to fill: " << ostr.size() << endl;
      
      unsigned int k;      
      for (k=0;k<ostr.size();k++) {
        
	// cerr << "filling: " << k << endl;
	
        tmp_ds.time      = ostr[k].epoch.Time();
	tmp_ds.amplitude = ostr[k].e;
	tmp_ds.phase     = ostr[k].omega_node + ostr[k].omega_pericenter;
	
        data_stream.push_back(tmp_ds);
      }
      
      // was s == HK
      
    } else if (s == D) {
      
      OrbitStream    &ostr = *os;
      data_stream.timestep = ostr.timestep;
      data_stream.resize(0);
      
      FFTDataStructure tmp_ds;
      
      unsigned int k;      
      for (k=0;k<ostr.size();k++) {
        
	// cerr << "filling: " << k << endl;
	
        tmp_ds.time      = ostr[k].epoch.Time();
	tmp_ds.amplitude = ostr[k].libration_angle;
	tmp_ds.phase     = 0.0;
	
        data_stream.push_back(tmp_ds);
      }
      
    } else if (s == PQ) {
      
      OrbitStream    &ostr = *os;
      data_stream.timestep = ostr.timestep;
      data_stream.resize(0);
      
      FFTDataStructure tmp_ds;

      unsigned int k;      
      for (k=0;k<ostr.size();k++) {
        
	// cerr << "filling: " << k << endl;
	
        tmp_ds.time      = ostr[k].epoch.Time();
	tmp_ds.amplitude = sin(ostr[k].i);
	tmp_ds.phase     = ostr[k].omega_node;
	
        data_stream.push_back(tmp_ds);
      }

    } else if (s == NODE) {
      
      OrbitStream    &ostr = *os;
      data_stream.timestep = ostr.timestep;
      data_stream.resize(0);
      
      FFTDataStructure tmp_ds;
      
      unsigned int k;      
      for (k=0;k<ostr.size();k++) {
        
	// cerr << "filling: " << k << endl;
	
        tmp_ds.time      = ostr[k].epoch.Time();
	tmp_ds.amplitude = ostr[k].omega_node;
	tmp_ds.phase     = 0.0;
	
        data_stream.push_back(tmp_ds);
      }
    } else if (s == ANOMALY) {
      
      OrbitStream    &ostr = *os;
      data_stream.timestep = ostr.timestep;
      data_stream.resize(0);
      
      FFTDataStructure tmp_ds;
      
      unsigned int k;      
      for (k=0;k<ostr.size();k++) {
        
	// cerr << "filling: " << k << endl;
	
        tmp_ds.time      = ostr[k].epoch.Time();
	tmp_ds.amplitude = ostr[k].M;
	tmp_ds.phase     = 0.0;
	
        data_stream.push_back(tmp_ds);
      }
    } else if (s == ANOMALY_PHASE) {
      
      OrbitStream    &ostr = *os;
      data_stream.timestep = ostr.timestep;
      data_stream.resize(0);
      
      FFTDataStructure tmp_ds;
      
      unsigned int k;      
      for (k=0;k<ostr.size();k++) {
        
	// cerr << "filling: " << k << endl;
	
        tmp_ds.time      = ostr[k].epoch.Time();
	tmp_ds.amplitude = 1.0; // dummy value
	tmp_ds.phase     = ostr[k].M;
	
        data_stream.push_back(tmp_ds);
      }
    } else if (s == A_M) {
      
      OrbitStream    &ostr = *os;
      data_stream.timestep = ostr.timestep;
      data_stream.resize(0);
      
      FFTDataStructure tmp_ds;
      
      unsigned int k;      
      for (k=0;k<ostr.size();k++) {
        
	// cerr << "filling: " << k << endl;
	
        tmp_ds.time      = ostr[k].epoch.Time();
	tmp_ds.amplitude = ostr[k].a; // dummy value
	tmp_ds.phase     = ostr[k].M;
	
        data_stream.push_back(tmp_ds);
      }
    }
    /* 
       } else if (s == TESTING) {
       
       OrbitStream    &ostr = *os;
       data_stream.timestep = ostr.timestep;
       data_stream.resize(0);
       
       FFTDataStructure tmp_ds;
       
       unsigned int k;      
       for (k=0;k<ostr.size();k++) {
       
       // cerr << "filling: " << k << endl;
       
       tmp_ds.time      = ostr[k].epoch.Time();
       // tmp_ds.amplitude = ostr[k].a; // externally modified to be ostr[k].a = \sqrt{\frac{a-mean(a_J)}{a_J}}
       tmp_ds.amplitude = ostr[k].a; // externally modified to be ostr[k].a = $\sqrt{\frac{a}{a_J}}$
       tmp_ds.phase     = ostr[k].libration_angle;;
       
       data_stream.push_back(tmp_ds);
       }
       }
    */
    
  }
  
  void FFT::FillDataStream(FFTSearchAmplitude sa, FFTSearchPhase sp) {
    OrbitStream &ostr = *os;
    data_stream.clear();
    data_stream.timestep = ostr.timestep;
    FFTDataStructure tmp_ds;
    const unsigned int size = ostr.size();
    data_stream.resize(size);
    unsigned int k;
    
    switch (sa) {
    case A:       for(k=0;k<size;k++) { data_stream[k].time = ostr[k].epoch.Time(); data_stream[k].amplitude = ostr[k].a;        } break;
    case E:       for(k=0;k<size;k++) { data_stream[k].time = ostr[k].epoch.Time(); data_stream[k].amplitude = ostr[k].e;        } break;
    case I:       for(k=0;k<size;k++) { data_stream[k].time = ostr[k].epoch.Time(); data_stream[k].amplitude = ostr[k].i;        } break;
    case SIN_I:   for(k=0;k<size;k++) { data_stream[k].time = ostr[k].epoch.Time(); data_stream[k].amplitude = sin(ostr[k].i);   } break;
    case TAN_I_2: for(k=0;k<size;k++) { data_stream[k].time = ostr[k].epoch.Time(); data_stream[k].amplitude = tan(ostr[k].i/2); } break;
    case ONE:     for(k=0;k<size;k++) { data_stream[k].time = ostr[k].epoch.Time(); data_stream[k].amplitude = 1.0;              } break;
    }
    
    switch (sp) {
    case OMEGA_NODE:       for(k=0;k<size;k++) { data_stream[k].phase = ostr[k].omega_node;                                        } break;
    case OMEGA_PERICENTER: for(k=0;k<size;k++) { data_stream[k].phase = ostr[k].omega_pericenter;                                  } break;
    case OMEGA_TILDE:      for(k=0;k<size;k++) { data_stream[k].phase = ostr[k].omega_node + ostr[k].omega_pericenter;             } break;
    case MM:               for(k=0;k<size;k++) { data_stream[k].phase = ostr[k].M;                                                 } break;
    case LAMBDA:           for(k=0;k<size;k++) { data_stream[k].phase = ostr[k].omega_node + ostr[k].omega_pericenter + ostr[k].M; } break;
    case ZERO:             for(k=0;k<size;k++) { data_stream[k].phase = 0.0;                                                       } break;
    }
  }
  
  void FFT::Search(FFTSearch se, FFTAlgorithm algo) {
    
    FillDataStream(se);
    
    if (algo==algo_FFT) {
      Search_FFT();
    } else if (algo==algo_FFTB) {
      Search_FFTB();
    } else if (algo==algo_MFT) {
      Search_MFT();
    } else if (algo==algo_FMFT1) {
      Search_FMFT1();
    } else if (algo==algo_FMFT2) {
      Search_FMFT2();
    }
    
    ComputeCommonPowerSpectrum();
    ComputeCommonReconstructedSignal();
    
  }
  
  void FFT::Search(FFTSearchAmplitude sa, FFTSearchPhase sp, FFTAlgorithm algo) {
    
    FillDataStream(sa,sp);
    
    if (algo==algo_FFT) {
      Search_FFT();
    } else if (algo==algo_FFTB) {
      Search_FFTB();
    } else if (algo==algo_MFT) {
      Search_MFT();
    } else if (algo==algo_FMFT1) {
      Search_FMFT1();
    } else if (algo==algo_FMFT2) {
      Search_FMFT2();
    }
    
    ComputeCommonPowerSpectrum();
    ComputeCommonReconstructedSignal();
    
  }

  void FFT::ComputeCommonPowerSpectrum() {
    
    // cerr << "===>> Computing Power Spectrum..." << endl;
    
    unsigned int j,k;
    unsigned int size = data_stream.size();
    // cerr << "data_stream.size(): " << data_stream.size() << endl;
    
    // fftw_complex in[size], work[size], out[size];
    fftw_complex *in, *work, *out;
    in   = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    work = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    out  = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    //
    for (j=0;j<size;j++) {
      in[j].re = data_stream[j].amplitude * cos(data_stream[j].phase);
      in[j].im = data_stream[j].amplitude * sin(data_stream[j].phase);
    }
    //
    apply_window(work,in,size);
    //
    fftw_plan plan;
    plan = fftw_create_plan(size, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_one(plan, work, out);
    // fftw_one(plan, in, out);
    fftw_destroy_plan(plan);
    //
    vector<double> psd_plus((size-1)/2), psd_minus((size-1)/2);
    double DC, Nyquist=0;
    //
    DC = norm(out[0])/size;
    for (k=0;k<(size-1)/2;k++)
      psd_plus[k]  = norm(out[k+1])/size;
    for (k=0;k<(size-1)/2;k++)
      psd_minus[k] = norm(out[size-(k+1)])/size;
    if ( (size%2) == 0) // size is even
      Nyquist = norm(out[size/2])/size;  // Nyquist freq.
    
    if (HiResSpectrum) {
      const int resample = 5; // 1
      const double timestep = data_stream.timestep;
      FFTPowerSpectrum &FFTps = *fps; 
      FFTPowerSpectrumBaseElement tps;
      FFTps.clear();
      const double freq_start = -7.0e-5; // physical units. i.e. [y^{-1}]
      const double freq_stop  =  0.0e-5; // physical units
      const double freq_incr  =  1.0/((double)size*resample)/timestep;
      double freq=freq_start;
      while (freq <= freq_stop) {
	tps.frequency = freq;
	tps.power     = phi_Hanning_amp(freq*timestep,in,size);
	FFTps.push_back(tps);
	freq += freq_incr;
      }
    } else {
      int l;
      const double timestep = data_stream.timestep;
      FFTPowerSpectrum &FFTps = *fps; 
      FFTPowerSpectrumBaseElement tps;
      FFTps.clear();
      for (l=psd_minus.size()-1;l>=0;l--) {
	// fprintf(fp,"%i  %g\n",-(l+1),psd_minus[l]);
	tps.frequency = ((double)-(l+1)/(double)size)/timestep;
	tps.power     = psd_minus[l];
	FFTps.push_back(tps);
      }
      // fprintf(fp,"%i  %g\n",0,DC);
      tps.frequency = 0;
      tps.power     = DC;
      FFTps.push_back(tps);
      //
      {
	unsigned int l;
	for (l=0;l<psd_plus.size(); l++) {
	  // fprintf(fp,"%i  %g\n", (l+1),psd_plus[l]);
	  tps.frequency = ((double)(l+1)/(double)size)/timestep;
	  tps.power     = psd_plus[l];
	  FFTps.push_back(tps);
	}
      }
      if ( (size%2) == 0) {
	// fprintf(fp,"%i  %g\n",size/2,Nyquist);
	tps.frequency = ((double)(size/2)/(double)size)/timestep;
	tps.power     = Nyquist;
	FFTps.push_back(tps);
      }
    }
    
    free(in); free(work); free(out);
  }
  
  void FFT::ComputeCommonReconstructedSignal() {
    
    unsigned int size = data_stream.size();
    reconstructed_data_stream->resize(size);
    unsigned int j,pk;
    fftw_complex z;
    double arg, arg_j,time_zero=data_stream[0].time;
    for (j=0;j<size;j++) {
      (*reconstructed_data_stream)[j].time = data_stream[j].time;
      // cerr << "reco time: " << (*reconstructed_data_stream)[j].time << endl;
      arg_j = twopi*(data_stream[j].time-time_zero); 
      z.re = z.im = 0;
      for (pk=0;pk<peaks->size();pk++) {
	arg = arg_j*(*peaks)[pk].frequency+(*peaks)[pk].phase;
	z.re += (*peaks)[pk].amplitude*cos(arg);
	z.im += (*peaks)[pk].amplitude*sin(arg);
      }
      (*reconstructed_data_stream)[j].amplitude = norm(z);
      (*reconstructed_data_stream)[j].phase     = secure_atan2(z.im,z.re);
    }
    // cerr << "reco times: " <<  (*reconstructed_data_stream)[0].time << " to   " << (*reconstructed_data_stream)[size-1].time << endl;
  }
  
  void FFT::Search_FFT() {
    
    // FillDataStream(se);
    
    unsigned int size = data_stream.size();
    
    vector<Peak> &pks = *peaks;
    
    fftw_plan plan;
    
    // fftw_complex in[size], out[size];
    fftw_complex *in, *out;
    in   = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    out  = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    
    const double timestep = data_stream.timestep;
    
    unsigned int j,k;
    
    for (j=0;j<size;j++) {
      in[j].re = data_stream[j].amplitude * cos(data_stream[j].phase);
      in[j].im = data_stream[j].amplitude * sin(data_stream[j].phase);
    }
    
    plan = fftw_create_plan(size, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_one(plan, in, out);
    fftw_destroy_plan(plan);
    
    psd.resize(size/2+1);
    psd[0] = norm(out[0])/size;
    for (k = 1; k < (size+1)/2; ++k)
      psd[k] = sqrt(norm_sq(out[k])+norm_sq(out[size-k]))/size;
    if (size % 2 == 0) // size is even
      psd[size/2] = norm(out[size/2])/size;  // Nyquist freq.
    
    
    if (HiResSpectrum) {
      
      int resample = 5;
      unsigned int l;
      FFTPowerSpectrum &FFTps = *fps; 
      FFTPowerSpectrumBaseElement tps;
      FFTps.resize(0);
      for (l=0;l<psd.size()*resample;l++) {
	tps.frequency = ((double)l/(double)(size*resample))/timestep;
	
	// Hanning or flat (NONE) windowing...
	// tps.power = phi_amp(((double)l/(double)(size*resample)),in,size);
	tps.power = phi_Hanning_amp(((double)l/(double)(size*resample)),in,size);
	
 	FFTps.push_back(tps);
      }
      
    } else {
      
      unsigned int l;
      FFTPowerSpectrum &FFTps = *fps; 
      FFTPowerSpectrumBaseElement tps;
      FFTps.resize(0);
      for (l=0;l<psd.size();l++) {
	tps.frequency = ((double)l/(double)size)/timestep;
	tps.power     = psd[l];
	
	FFTps.push_back(tps);
      }
      
    }
    
    // DEBUG
    /* for (l=0;l<psd.size();l++) {
       cerr << (*fps)[l].frequency << "  " << (*fps)[l].power << endl;
       }
    */
    
    // peaks reset
    pks.resize(0);
    /* pks.resize(number_of_peaks);
       for (k=0;k<number_of_peaks;k++)
       pks[k].Set(default_peak_reset_frequency,
       default_peak_reset_amplitude,
       default_peak_reset_phase); 
    */
    
    // cerr << "Pass (4)" << endl;
    
    bool genuine_peak;
    double range_start=0.0,range_stop=0.5;
    double phi_tmp;
    unsigned int ip;
    Peak test_peak;
    unsigned int bin = 0;
    
    // cerr << "sizeof(binamp): " << sizeof(binamp) << endl;
    unsigned int bsize = psd.size();
    binamp *ptr_binamp = (binamp *) malloc(bsize*sizeof(binamp));
    for (k=0;k<bsize;k++) {
      ptr_binamp[k].bin = k;
      ptr_binamp[k].amp = psd[k];
    }
    gsl_heapsort(ptr_binamp,bsize,sizeof(binamp),(gsl_comparison_fn_t)compare_binamp);
    
    candidate_bin.resize(0);
    // include bin zero! (DC)
    double maxamp, relamp;
    maxamp = ptr_binamp[0].amp;
    k=0;
    do {
      relamp = ptr_binamp[k].amp / maxamp;
      if ((ptr_binamp[k].bin==0) || ptr_binamp[k].bin==(int)bsize-1) {
	candidate_bin.push_back(ptr_binamp[k].bin);
      } else {
	if ((psd[ptr_binamp[k].bin-1] < ptr_binamp[k].amp) &&
	    (psd[ptr_binamp[k].bin+1] < ptr_binamp[k].amp)) {
	  candidate_bin.push_back(ptr_binamp[k].bin);
	}
      }
      k++;
    } while ((k<bsize) && (candidate_bin.size() != nfreq));
    // } while (relamp >= relative_amplitude);
    //
    free(ptr_binamp);
    
    
    // cerr << "number of candidate peaks: " << candidate_bin.size() << endl;
    
    unsigned int candidate_bin_counter=0;
    for (candidate_bin_counter=0;candidate_bin_counter<candidate_bin.size();candidate_bin_counter++) {
      
      bin = candidate_bin[candidate_bin_counter];
      
      genuine_peak = true;
      test_peak.frequency = (double)bin/(double)size;
      // if (w == NONE) {
      test_peak.amplitude = phi_amp(test_peak.frequency,&in[0],size); 
      /* } else if (w == HANNING) {
	 test_peak.amplitude = phi_Hanning_amp(test_peak.frequency,&in[0],size); 
	 } else {
	 cerr << "Invalid Windowing --> EXIT" << endl;
	 exit(0);
	 }
      */
      
      if (bin == 0) test_peak.amplitude /= sqrt(2.0);
      
      ip = 0;
      
      if (genuine_peak == true) {
	
	if (bin != 0) {
	  ip = 0;
	  do {  
	    ip++;
	    range_start = test_peak.frequency - 0.1*(double)ip/(double)size;
	    // phi_tmp = phi_amp(range_start,&in[0],size);
	    // if (w == NONE) {
	      phi_tmp = phi_amp(range_start,&in[0],size); 
	      /* } else if (w == HANNING) {
		 phi_tmp = phi_Hanning_amp(range_start,&in[0],size); 
		 } else {
		 cerr << "Invalid Windowing --> EXIT" << endl;
		 exit(0);
		 }
	      */
	  } while ( (range_start >= 0) && 
		    (ip < 100) && 
		    (phi_tmp >= test_peak.amplitude) );
	  // cerr << "range_start iterations: " << ip << endl;
	  if ( (ip >= 100) || 
	       (range_start < 0) ) {
	    genuine_peak = false;  
	  } else {
	    ip = 0;
	    do {  
	      ip++;
	      range_stop = test_peak.frequency + 0.1*(double)ip/(double)size;
	      // phi_tmp = phi_amp(range_stop,&in[0],size);
	      // if (w == NONE) {
		phi_tmp = phi_amp(range_stop,&in[0],size); 
		/* } else if (w == HANNING) {
		   phi_tmp = phi_Hanning_amp(range_stop,&in[0],size); 
		   } else {
		   cerr << "Invalid Windowing --> EXIT" << endl;
		   exit(0);
		   }
		*/
		
	    } while ( (range_stop <= 0.5) && 
		      (ip < 100) && 
		      (phi_tmp >= test_peak.amplitude) );
	    // cerr << "range_stop  iterations: " << ip << endl;
	    if ( (ip >= 100) || 
		 (range_stop > 0.5) ) {
	      genuine_peak = false;  
	    }
	  }  
	}
      }
      
      // get the right normalizzation
      if (bin == 0) test_peak.amplitude /= sqrt(2.0);
      
      // look for maximums
      if (genuine_peak) {
	
	if (bin != 0) {
	  
	  // gsl stuff
	  par.size = size;
	  par.pointer_points_sequence = &in[0];
	  //
	  
	  // F.function = &phi_gsl;
	  // if (w == NONE) {
	  F.function = &phi_gsl;
	  /* } else if (w == HANNING) {
	     F.function = &phi_Hanning_gsl;
	     } else {
	     cerr << "Invalid Windowing --> EXIT" << endl;
	     exit(0);
	     }
	  */
	  
	  F.params   = &par;
	  
	  gsl_min_fminimizer_set(s,&F,test_peak.frequency,range_start,range_stop);
	  
	  double epsrel = 1.0e-9; // for the d=1 minimization
	  double epsabs = 0.0;    // for the d=1 minimization
	  
	  unsigned int iter = 0;
	  unsigned int max_iter = 100;
	  int status;
	  do { 
	    iter++;
	    
	    status = gsl_min_fminimizer_iterate (s);
	    //
	    test_peak.frequency = gsl_min_fminimizer_minimum (s);
	    range_start         = gsl_min_fminimizer_x_lower (s);
	    range_stop          = gsl_min_fminimizer_x_upper (s);
	    // delta     = range_stop - range_start;
	    // minimize!
	    status = gsl_min_test_interval (range_start, range_stop, epsrel, epsabs);
	    
	    // post-minimization...
	    /* printf("%5d [%f, %f] %.7f %.7e  bin:%i\n", 
	       iter,range_start,range_stop,test_peak.frequency,range_stop-range_start,bin);
	    */
	  } while ((status == GSL_CONTINUE) && (iter < max_iter));
	  
	}
	
	// test_peak.amplitude = phi_amp(test_peak.frequency,&in[0],size); 
	// if (w == NONE) {
	test_peak.amplitude = phi_amp(test_peak.frequency,&in[0],size); 
	/* } else if (w == HANNING) {
	   test_peak.amplitude = phi_Hanning_amp(test_peak.frequency,&in[0],size); 
	   } else {
	   cerr << "Invalid Windowing --> EXIT" << endl;
	   exit(0);
	   }
	*/
	
	if (bin == 0) test_peak.amplitude /= sqrt(2.0);
	
	// check if this peak is very close 
	// to the existing one already in the list
	bool new_peak = true;
	unsigned int  wp;
        if (pks.size() != 0) {
	  for (wp=0;wp<pks.size();wp++) {
	    if ( test_peak.amplitude > (0.9999) * pks[wp].amplitude &&
		 test_peak.amplitude < (1.0001) * pks[wp].amplitude )
	      new_peak = false;
	    
	    if ( test_peak.frequency > (0.9999) * pks[wp].frequency &&
		 test_peak.frequency < (1.0001) * pks[wp].frequency )
	      new_peak = false; 
	  }
	}
	
	// quicker, but not sorted...
	if (new_peak) {
	  test_peak.phiR = phi(+test_peak.frequency,&in[0],size);
	  test_peak.phiL = phi(-test_peak.frequency,&in[0],size);
	  test_peak.phase = secure_atan2(test_peak.phiR.im,test_peak.phiR.re);
	  pks.push_back(test_peak);
	  // cerr << "Added peak!" << endl;
	}
	
      }
      
    }
    
    // before than return
    // rescale freqs.
    for (j=0;j<pks.size();j++) {
      pks[j].frequency /= timestep;
    }
    
    // not here, probably in the destructor...
    // gsl_min_fminimizer_free(s); 
    
    free(in); free(out);
  }
  
  void FFT::Search_FFTB() {
     
    const unsigned int size = data_stream.size();
    const double timestep = data_stream.timestep;
    
    fftw_complex *in, *work, *out;
    in   = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    work = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    out  = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    
    unsigned int j;
    for (j=0;j<size;j++) {
      in[j].re = data_stream[j].amplitude * cos(data_stream[j].phase);
      in[j].im = data_stream[j].amplitude * sin(data_stream[j].phase);
    }
    
    apply_window(work,in,size);
    
    // FFT
    fftw_plan plan;
    plan = fftw_create_plan(size, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_one(plan, work, out);
    fftw_destroy_plan(plan);
    
    vector<double> candidate_freq;
    psd_max_again_many(out,size,candidate_freq,nfreq);
    /* unsigned int l;
       for (l=0;l<candidate_freq.size();l++) {
       cerr << "candidate freq: " << candidate_freq[l]/timestep << endl;
       }
    */
    
    double f,A,psi;
    fftw_complex phiR,phiL;
    double centerf,leftf,rightf; 
   
    unsigned int l;
    Peak tp;
    peaks->resize(candidate_freq.size());
    for (l=0;l<candidate_freq.size();l++) {
      
      centerf = candidate_freq[l];
      leftf   = centerf-1.0/size; 
      rightf  = centerf+1.0/size; 
      
      f = accurate_peak(leftf,centerf,rightf,work,size);
      /* COMPUTE AMPLITUDE AND PHASE */
      amph(&A,&psi,&phiR,&phiL,f,work,size);

      tp.frequency =   f/timestep;
      tp.amplitude =   A;
      tp.phase     = psi;   
      // 
      tp.phiR = phiR;
      tp.phiL = phiL;
      
      (*peaks)[l] = tp;
      
    }
    
    free(in); free(work); free(out);
    
  }
  
  void FFT::Search_MFT() {
    Search_FMFT_main();
  }
  
  
  // Eq. (34)
  double dQ(double y) {
    
    // y==0
    if (fabs(y) < 1.0e-12) return (0.0);
    
    // y==PI
    if (fabs(y-pi) < 1.0e-12) return (-3.0/(4*pi));
    
    // y==-PI
    if (fabs(y+pi) < 1.0e-12) return ( 3.0/(4*pi));
    
    double ysq=y*y;
    return (pisq/(y*(pisq-ysq))*(cos(y)+(sin(y)/y)*((3*ysq-pisq)/(pisq-ysq))));
    
  }
  
  void FFT::Search_FMFT1() {
    
    Search_FMFT_main();
    
    // save the peaks found
    const vector<Peak> peaks_MFT(*peaks);
    const double timestep = data_stream.timestep;
    
    unsigned int peaks_size = peaks_MFT.size();
    unsigned int size = data_stream.size();
    
    vector<double> epsilon(peaks_size);
    
    // EQ. (35)
    double ddQ_zero = 2.0/pisq - 1.0/3.0;
    
    double eps; // correction
    double y_sj;
    
    unsigned int j,s;
    for (j=0;j<peaks_size-1;j++) {
      
      eps=0.0;
      for (s=j+1;s<peaks_size;s++) {
	y_sj = (peaks_MFT[s].frequency - peaks_MFT[j].frequency)*timestep*(size/2.0);
	// y_sj = (peaks_MFT[s].frequency - peaks_MFT[j].frequency)*timestep;
	// cerr << "y_sj: " << y_sj << endl;
	eps += peaks_MFT[s].amplitude * dQ(twopi*y_sj) * cos(twopi*y_sj+peaks_MFT[s].phase-peaks_MFT[j].phase);
      }
      eps /= peaks_MFT[j].amplitude*ddQ_zero*timestep*(size/2.0);
      // eps /= peaks_MFT[j].amplitude*ddQ_zero*(size/2.0);
      // eps /= peaks_MFT[j].amplitude*ddQ_zero*timestep;
      //
      epsilon[j] = eps;
      printf("epsilon[%i]: %.20g\n",j,eps);
    }
    
    // correction for amplitde and phase
    vector<double> f(nfreq), A(nfreq), psi(nfreq);
    {
      double Q[nfreq][nfreq], alpha[nfreq][nfreq];
      double B[nfreq];
      
      Q[0][0]     = 1;
      alpha[0][0] = 1;
      
      double fac,xsum,ysum;
      f[0]   = ( peaks_MFT[0].frequency - epsilon[0] ) * timestep;
      psi[0] = peaks_MFT[0].phase;
      A[0]   = peaks_MFT[0].amplitude;
      
      unsigned int k,m;
      for(m=1;m<nfreq;m++) {
	
	f[m]   = ( peaks_MFT[m].frequency - epsilon[m] ) * timestep;
	psi[m] = peaks_MFT[m].phase;
	A[m]   = peaks_MFT[m].amplitude;
	
	/* EQUATION (3) in Sidlichovsky and Nesvorny (1997) */
	Q[m][m] = 1;
	for(j=0;j<m;j++) {
	  fac = (f[m]-f[j])*size/2.;
	  fac *= twopi;
	  // cerr << " ...TEST NEAR PI: " << fmod(fac,pi) << "   " << fac << endl;
	  // fix, if fac==0
	  if (fabs(fac) < 1.0e-12) { // 0
	    Q[m][j] = 1;
	  } else if (fabs(fabs(fac)-pi)< 1.0e-12) { // +/- PI
	    Q[m][j] = 0.5; // 1/2
	  } else {
	    Q[m][j] = sin(fac)/fac * pisq / (pisq - fac*fac);
	  }
	  
	  Q[j][m] = Q[m][j];
	}
	
	/* EQUATION (17) */
	for(k=0;k<m;k++){
	  B[k] = 0;
	  for(j=0;j<=k;j++)
	    B[k] += -alpha[k][j]*Q[m][j];
	}
	
	/* EQUATION (18) */
	alpha[m][m] = 1;
	for(j=0;j<m;j++)
	  alpha[m][m] -= B[j]*B[j];
	alpha[m][m] = 1. / sqrt(alpha[m][m]);
	
	/* EQUATION (19) */
	for(k=0;k<m;k++) {
	  alpha[m][k] = 0;
	  
	  for(j=k;j<m;j++)
	    alpha[m][k] += B[j]*alpha[j][k];
	  
	  alpha[m][k] = alpha[m][m]*alpha[m][k];
	}
	
      }
      
      /* EQUATION (26) */
      for(k=0;k<nfreq;k++) {
	xsum=0; ysum=0;
	for(j=k;j<nfreq;j++) {
	  fac = twopi*((f[j]-f[k])*size/2.) + psi[j];
	  xsum += alpha[j][j]*alpha[j][k]*A[j]*cos(fac);
	  ysum += alpha[j][j]*alpha[j][k]*A[j]*sin(fac);
	}
	A[k]   = sqrt(xsum*xsum+ysum*ysum);
	psi[k] = secure_atan2(ysum,xsum);
      }
    }
    
    unsigned int k;
    for (k=0;k<peaks_MFT.size();k++) {
      (*peaks)[k].frequency =   f[k]/timestep;
      (*peaks)[k].amplitude =   A[k];
      (*peaks)[k].phase     = psi[k];
    }
  }
  
  void FFT::Search_FMFT2() {
    
    Search_FMFT_main();
    
    // save the peaks found
    vector<Peak> peaks_MFT(*peaks);
    
    /* GENERATE THE QUASIPERIODIC FUNCTION COMPUTED BY MFT */
    unsigned int size = data_stream.size();
    FFTDataStream backup_data_stream(data_stream);
    // reconstructed_data_stream->resize(size);
    unsigned int j,pk;
    fftw_complex z;
    double arg, arg_j;
    for (j=0;j<size;j++) {
      // (*reconstructed_data_stream)[j].time = data_stream[j].time;
      arg_j = twopi*data_stream[j].time; 
      z.re = z.im = 0;
      for (pk=0;pk<peaks->size();pk++) {
	arg = arg_j*(*peaks)[pk].frequency+(*peaks)[pk].phase;
	z.re += (*peaks)[pk].amplitude*cos(arg);
	z.im += (*peaks)[pk].amplitude*sin(arg);
      }
      // (*reconstructed_data_stream)[j].amplitude = norm(z);
      // (*reconstructed_data_stream)[j].phase     = secure_atan2(z.im,z.re);
      data_stream[j].amplitude = norm(z);
      data_stream[j].phase     = secure_atan2(z.im,z.re);
    }
    
    // cerr << "Second step..." << endl;
    Search_FMFT_main();
    vector<Peak> peaks_FMFT1(*peaks);
    
    unsigned int k;
    for (k=0;k<peaks_MFT.size();k++) {
      (*peaks)[k].frequency = peaks_MFT[k].frequency + (peaks_MFT[k].frequency - peaks_FMFT1[k].frequency);
      (*peaks)[k].amplitude = peaks_MFT[k].amplitude + (peaks_MFT[k].amplitude - peaks_FMFT1[k].amplitude);
      (*peaks)[k].phase     = peaks_MFT[k].phase     + (peaks_MFT[k].phase     - peaks_FMFT1[k].phase    );
    }
    
    data_stream = backup_data_stream;
  }
  
  void FFT::Search_FMFT_main() {
    
    // nfreq = 12;
    
    bool nearfreqflag;
    
    const double timestep = data_stream.timestep;
    
    const int size = data_stream.size();
    // cerr << "FFT::Search_FMFT_main  size = " << size << endl;
    
    // fftw_complex in[size], work[size], out[size];    
    fftw_complex *in, *work, *out;
    in   = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    work = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    out  = (fftw_complex*)malloc(size*sizeof(fftw_complex));
    
    { 
      int j;
      for (j=0;j<size;j++) {
	in[j].re = data_stream[j].amplitude * cos(data_stream[j].phase);
	in[j].im = data_stream[j].amplitude * sin(data_stream[j].phase);
      }
    }
    
    if (0) { 
      // DUMP ON FILE
      // int filenum=0;
      char filename[20];
      sprintf(filename,"dump_signal_2n.dat");
      cerr << "data dump on file " << filename << endl;
      FILE *fp = fopen(filename,"w");
      int r=1; while (pow(2.0,r)<=size) {r++;} r--;
      int fsize = (int)rint(pow(2.0,r));
      cerr << " --> 2^r = " << fsize << endl;
      if (fp!=0) {
	int l;
	for (l=0;l<fsize;l++) {
	  fprintf(fp,"%i   %g   %g\n",l,in[l].re,in[l].im);
	}
      }
      fclose(fp);
    }
    
    apply_window(work,in,size);
    
    // FFT
    fftw_plan plan;
    plan = fftw_create_plan(size, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_one(plan, work, out);
    fftw_destroy_plan(plan);
    
    double centerf = psd_max_again(out,size);
    double leftf   = centerf-1.0/size; 
    double rightf  = centerf+1.0/size; 
    
    vector<double> f(nfreq), A(nfreq), psi(nfreq);
    vector<fftw_complex> phiR(nfreq), phiL(nfreq);
    
    f[0] = accurate_peak(leftf,centerf,rightf,work,size);
    // f[0] = golden(phi_amp,leftf,centerf,rightf,work,size);
    
    /* COMPUTE AMPLITUDE AND PHASE */
    amph(&A[0],&psi[0],&phiR[0],&phiL[0],f[0],work,size);
    
    /* SUBSTRACT THE FIRST HARMONIC FROM THE SIGNAL */ 
    {
      int j;
      for (j=0;j<size;j++) {
	in[j].re -= A[0]*cos(twopi*f[0]*j+psi[0]);
	in[j].im -= A[0]*sin(twopi*f[0]*j+psi[0]);
      }
    }
    
    /* HERE STARTS THE MAIN LOOP  *************************************/ 
    
    double Q[nfreq][nfreq], alpha[nfreq][nfreq];
    double B[nfreq];
    
    Q[0][0]     = 1;
    alpha[0][0] = 1;
    
    double fac,xsum,ysum;
    unsigned int m,k,j;
    for(m=1;m<nfreq;m++) {
      
      // cerr << "test in: " << in[0].re << "  " << in[0].im << "  " << in[1].re << "  " << in[1].im << "  " << endl;
      
      apply_window(work,in,size);
      
      // cerr << "test work: " << work[0].re << "  " << work[0].im << "  " << work[1].re << "  " << work[1].im << "  " << endl;
      
      // FFT
      fftw_plan plan;
      plan = fftw_create_plan(size, FFTW_FORWARD, FFTW_ESTIMATE);
      fftw_one(plan, work, out);
      fftw_destroy_plan(plan);
      
      // cerr << "test out: " << out[0].re << "  " << out[0].im << "  " << out[1].re << "  " << out[1].im << "  " << endl;
      
      centerf = psd_max_again(out,size);
      leftf   = centerf-1.0/size; 
      rightf  = centerf+1.0/size; 
      
      f[m] = accurate_peak(leftf,centerf,rightf,work,size);
      // f[m] = golden(phi_amp,leftf,centerf,rightf,work,size);
      // if (f[m]==-1) break;
      
      /* cerr << " Peaks found up to now..." << endl;
	 for(k=0;k<=m;k++) {
	 printf("f[%i]: %.20g\n",k,f[k]/timestep);
	 }
      */
      
      /* CHECK WHETHER THE NEW FREQUENCY IS NOT TOO CLOSE TO ANY PREVIOUSLY
	 DETERMINED ONE */
      nearfreqflag=false;
      unsigned int fq;
      for (fq=0;fq<m;fq++) if( fabs(f[m]-f[fq]) <= 1.0e-12/size) nearfreqflag=true;
      
      while (nearfreqflag) {
	f[m] = accurate_peak(leftf,centerf,rightf,work,size);
	amph(&A[m],&psi[m],&phiR[m],&phiL[m],f[m],work,size);
	printf(" ----- REMOVING DUPLICATED PEAK: : %.20g  %.20g  %.20g\n",f[m]/timestep,A[m],psi[m]);
	{
	  int j;
	  for (j=0;j<size;j++) {
	    in[j].re -= A[m]*cos(twopi*f[m]*j+psi[m]);
	    in[j].im -= A[m]*sin(twopi*f[m]*j+psi[m]);
	  }
	}
	apply_window(work,in,size);
	// FFT
	fftw_plan plan;
	plan = fftw_create_plan(size, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_one(plan, work, out);
	fftw_destroy_plan(plan);
	centerf = psd_max_again(out,size);
	leftf   = centerf-1.0/size; 
        rightf  = centerf+1.0/size; 
	f[m] = accurate_peak(leftf,centerf,rightf,work,size);
	nearfreqflag=false;
	unsigned int fq;
	for (fq=0;fq<m;fq++) if( fabs(f[m]-f[fq]) <= 1.0e-12/size) nearfreqflag=true;
      }
      
      /* COMPUTE AMPLITUDE AND PHASE */
      amph(&A[m],&psi[m],&phiR[m],&phiL[m],f[m],work,size);
      
      /* cerr << " Peaks found up to now..." << endl;
	 for(k=0;k<=m;k++) {
	 printf("f[%i]: %.20g  %.20g  %.20g\n",k,f[k]/timestep,A[k],psi[k]);
	 }
      */
      
      /* EQUATION (3) in Sidlichovsky and Nesvorny (1997) */
      Q[m][m] = 1;
      for(j=0;j<m;j++) {
	fac = (f[m]-f[j])*size/2.;
	fac *= twopi;
	// cerr << " ...TEST NEAR PI: " << fmod(fac,pi) << "   " << fac << endl;
	// fix, if fac==0
	if (fabs(fac) < 1.0e-12) { // 0
	  Q[m][j] = 1;
	} else if (fabs(fabs(fac)-pi)< 1.0e-12) { // +/- PI
	  Q[m][j] = 0.5; // 1/2
	} else {
	  Q[m][j] = sin(fac)/fac * pisq / (pisq - fac*fac);
	}
	
	Q[j][m] = Q[m][j];
      }
      
      // Q DUMP
      /* printf("Q dump...   m=%i\n",m);
	 for (k=0;k<=m;k++) {
	 for (j=0;j<=k;j++) {
	 printf("Q[%i][%i]: %.20g\n",k,j,Q[k][j]);
	 }
	 }
      */
      
      /* EQUATION (17) */
      for(k=0;k<m;k++){
	B[k] = 0;
	for(j=0;j<=k;j++)
	  B[k] += -alpha[k][j]*Q[m][j];
      }
      
      // B DUMP
      /* printf("B dump...   m=%i\n",m);
	 for (k=0;k<m;k++) {
	 printf("B[%i]: %.20g\n",k,B[k]);
	 }
      */
      
      /* EQUATION (18) */
      alpha[m][m] = 1.0;
      for(j=0;j<m;j++)
	alpha[m][m] -= (B[j]*B[j]);
      if (alpha[m][m]<0) {
	cerr << "WARNING!!!! Negative sqrt()!!!! " << alpha[m][m] << endl;
      }
      // cerr << "sqrt of: " << alpha[m][m] << endl;
      alpha[m][m] = 1. / sqrt(alpha[m][m]);
      
      /* EQUATION (19) */
      for(k=0;k<m;k++) {
	alpha[m][k] = 0;
	
	for(j=k;j<m;j++)
	  alpha[m][k] += B[j]*alpha[j][k];
	
	alpha[m][k] = alpha[m][m]*alpha[m][k];
      }
      
      // alpha DUMP
      /* printf("alpha dump...   m=%i\n",m);
	 for (k=0;k<=m;k++) {
	 for (j=0;j<=k;j++) {
	 printf("alpha[%i][%i]: %.20g\n",k,j,alpha[k][j]);
	 }
	 }
      */
      
      /* EQUATION (22) */
      {
	int i;
	for(i=0;i<size;i++) {
	  xsum=0; ysum=0;
	  for(j=0;j<=m;j++) {
	    fac = twopi*(f[j]*i + (f[m]-f[j])*size/2.) + psi[m];
	    xsum += alpha[m][j]*cos(fac);
	    ysum += alpha[m][j]*sin(fac);
	  }
	  in[i].re -= alpha[m][m]*A[m]*xsum;
	  in[i].im -= alpha[m][m]*A[m]*ysum;
	}
      }
    }
    
    /* cerr << " Peaks found up to now..." << endl;
       for(k=0;k<nfreq;k++) {
       printf("f[%i]: %.20g  %.20g  %.20g\n",k,f[k]/timestep,A[k],psi[k]);
       }
    */
    
    /* EQUATION (26) */
    for(k=0;k<nfreq;k++) {
      xsum=0; ysum=0;
      for(j=k;j<nfreq;j++) {
	fac = twopi*((f[j]-f[k])*size/2.) + psi[j];
	xsum += alpha[j][j]*alpha[j][k]*A[j]*cos(fac);
	ysum += alpha[j][j]*alpha[j][k]*A[j]*sin(fac);
      }
      A[k]   = sqrt(xsum*xsum+ysum*ysum);
      psi[k] = secure_atan2(ysum,xsum);
    }
    
    /* 
       cerr << " Peaks at last..." << endl;
       for(k=0;k<nfreq;k++) {
       printf("k=%i   f: %g   A: %g   psi: %g\n",k,f[k]/timestep,A[k],psi[k]);
       }
    */
    
    peaks->resize(nfreq);
    Peak tp;
    // cerr << "timestep: " << timestep << endl;
    for(k=0;k<nfreq;k++) {
      
      tp.frequency =   f[k]/timestep;
      tp.amplitude =   A[k];
      tp.phase     = psi[k];   
      // 
      tp.phiR = phiR[k];
      tp.phiL = phiL[k];
      
      (*peaks)[k] = tp;
    }
    
    free(in); free(work); free(out);
    
  }
  
} // namespace orsa
  
