/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_universe.h"
#include "orsa_file.h"
#include "orsa_error.h"

#include <iostream>
#include <string>
#include <limits>
#include <algorithm>

using namespace std;

namespace orsa {
  
  // global units
  Units * units = 0;
  
  // active universe
  Universe * universe = 0;
  
  // active configuration
  Config * config = 0;
  
  // global JPL file
  JPLFile * jpl_file = 0;
  
  // JPL cache global class
  JPLCache * jpl_cache = 0;
  
  // world visible LocationFile
  LocationFile * location_file = 0;
  
  Universe::Universe() : std::vector<Evolution*>(), type(Simulated), sys(ECLIPTIC), timescale(ET) {
    common_init(AU,MSUN,YEAR);
  }
  
  Universe::Universe(length_unit lu, mass_unit mu, time_unit tu, UniverseType ut, ReferenceSystem rs, TimeScale ts) : std::vector<Evolution*>(), type(ut), sys(rs), timescale(ts) {
    common_init(lu,mu,tu);
  }
  
  void Universe::common_init(const length_unit lu, const mass_unit mu, const time_unit tu) {
    if (universe) delete universe; 
    universe = 0;
    
    if (!orsa_paths) orsa_paths = new OrsaPaths;
    Debug::construct();
    //
    if (!config) {
      config = new Config;
    }
    config->read_from_file();
    //
    if (!units) {
      units = new Units;
    }
    //
    units->SetSystem(tu,lu,mu);
    //
    if (!jpl_file) {
      jpl_file = new JPLFile(config->paths[JPL_EPHEM_FILE]->GetValue().c_str());
    }
    //
    if (!jpl_cache) {
      jpl_cache = new JPLCache;
    }
    //
    if (!location_file) {
      location_file = new LocationFile; 
      location_file->SetFileName(config->paths[OBSCODE]->GetValue().c_str());
      location_file->Open();
      location_file->Read();
      location_file->Close();
    }
    //
    modified = true;
    default_Date_timescale = timescale;
    universe = this;
  }
  
  Universe::~Universe() {
    /* 
       int k;
       k = size();
       while (k>0) {
       --k;
       if ((*this)[k]) (*this)[k]->clear();
       }
    */
    // NOTE: keep these two loops separate!
    int k = size();
    while (k>0) {
      --k;
      delete (*this)[k];
      (*this)[k] = 0;
    }
    
    universe = 0;
  }
  
  // declared in orsa_units.h -- to put elsewhere in future
  double GetG()        { return (orsa::units->GetG()); }
  double GetG_MKS()    { return (orsa::units->GetG_MKS()); }
  double GetMSun()     { return (orsa::units->GetMSun()); }
  double GetC()        { return (orsa::units->GetC()); }
  
  /* 
     double FromUnits(double x, time_unit   u, double power) { return orsa::units->FromUnits(x,u,power); };
     double FromUnits(double x, length_unit u, double power) { return orsa::units->FromUnits(x,u,power); };
     double FromUnits(double x, mass_unit   u, double power) { return orsa::units->FromUnits(x,u,power); };
  */
  
  double FromUnits(const double x, const time_unit   u, const int power) { return orsa::units->FromUnits(x,u,power); };
  double FromUnits(const double x, const length_unit u, const int power) { return orsa::units->FromUnits(x,u,power); };
  double FromUnits(const double x, const mass_unit   u, const int power) { return orsa::units->FromUnits(x,u,power); };
  
  string TimeLabel()   { return orsa::units->TimeLabel();   };
  string LengthLabel() { return orsa::units->LengthLabel(); };
  string MassLabel()   { return orsa::units->MassLabel();   };
  
  unsigned int Evolution::used_evolution_id = 0;
  
  Evolution::Evolution() : std::vector<Frame>(), id(used_evolution_id++) {
    /* 
       if (universe->GetUniverseType() == Real) {
       sample_period = FromUnits(1,DAY);
       } else {
       sample_period = FromUnits(0.01,YEAR);
       }
    */
    sample_period = FromUnits(0.01,YEAR);
    integrator    = 0;
    interaction   = 0;
    max_unsaved_substeps_active = false;
    _integrating=false;
  }
  
  //! This copy constructor copies the parameters, not the frames!
  Evolution::Evolution(const Evolution & e) : std::vector<Frame>(), id(used_evolution_id++) {
    sample_period = e.sample_period;
    integrator    = e.integrator->clone();
    interaction   = e.interaction->clone();
    //
    max_unsaved_substeps_active = false;
    _integrating=false;
  }
  
  Evolution::~Evolution() {
    delete integrator;
    integrator = 0;
    delete interaction;
    interaction = 0;
  }
  
  void Evolution::SetIntegrator(const IntegratorType type) {
    make_new_integrator(&integrator,type);
  }
  
  void Evolution::SetIntegrator(const Integrator * itg) {
    delete integrator;
    integrator = itg->clone();
  }
  
  void Evolution::SetIntegratorTimeStep(const UniverseTypeAwareTimeStep ts) {
    integrator->timestep = ts;
  }
  
  const UniverseTypeAwareTimeStep & Evolution::GetIntegratorTimeStep() const {
    return integrator->timestep;
  }
  
  void Evolution::SetIntegratorAccuracy(const double a) {
    integrator->accuracy = a;
  }
  
  double Evolution::GetIntegratorAccuracy() const {
    return integrator->accuracy;
  }
  
  void Evolution::SetInteraction(const InteractionType type) {
    make_new_interaction(&interaction,type);
  }
  
  void Evolution::SetInteraction(const Interaction * itr) {
    delete interaction;
    interaction = itr->clone();
  }
  
  void Evolution::SetSamplePeriod(const UniverseTypeAwareTimeStep & sp) {
    sample_period = sp;
  }
  
  void Evolution::Integrate(const UniverseTypeAwareTime & time_stop, const bool save_last_anyway) {
    
    if (integrator==0) {
      ORSA_WARNING("Integrator not initialized");
      return;
    }
    
    if (interaction==0) {
      ORSA_WARNING("Interaction not initialized!");
      return;
    }
    
    integration_started();
    
    // FIXME: if the timestep is not set?
    // if (sample_period == 0) sample_period = integrator->timestep;
    if (sample_period.IsZero()) sample_period = integrator->timestep;
    sample_period = sample_period.absolute();
    
    if (size() == 0) {
      ORSA_ERROR("no starting frame in integration.");
      return;
    }
    
    Frame f_start = (*this)[size()-1];
    Frame f_stop  = f_start;
    
    if (f_start == time_stop) {
      // no integration needed...
      return;
    }
    
    const double time_start = f_start.Time();
    
    bool save_frame = false;
    
    bool continue_integration = true;
    
    // double sign;
    int sign;
    
    // if ((time_stop.Time()-f_stop.Time()) > 0) {
    if ((time_stop - f_stop) > 0) {
      // integrator->timestep = fabs(integrator->timestep);
      integrator->timestep = integrator->timestep.absolute();
      sign = +1;
    } else {
      // integrator->timestep = -fabs(integrator->timestep);
      integrator->timestep = -integrator->timestep.absolute();
      sign = -1;
    }
    
    // important init value!
    UniverseTypeAwareTimeStep last_genuine_timestep;
    if (integrator->timestep.absolute() > 0.0) {
      last_genuine_timestep = integrator->timestep;
    } else {
      last_genuine_timestep = time_stop - f_start;
    }
    
    unsigned int unsaved_substeps = 0;
    unsigned int total_substeps = 0;
    
    while ( (continue_integration) && (((time_stop.Time()-f_stop.Time())/(time_stop.Time()-time_start)) > 0) ) {
      
      f_start = f_stop;
      
      /* 
	 fprintf(stderr,
	 "==========================         \n"
	 "f_start.Time()           : %20.12e \n"
	 "integrator->timestep     : %20.12e \n"
	 "(*this)[size()-1].Time() : %20.12e \n"
	 "sample_period            : %20.12e \n"
	 "frames                   : %zi     \n"
	 "==========================         \n",
	 f_start.Time(),integrator->timestep,(*this)[size()-1].Time(),sample_period,size()
	 );
      */
      
      if (0) { 
	ORSA_DEBUG(
		"\n"
		"==========================         \n"
		"f_start.Time()           : %20.12e \n"
		"time_stop.Time()         : %20.12e \n"
		"integrator->timestep     : %20.12e \n"
		"(*this)[size()-1].Time() : %20.12e \n"
		"sample_period            : %20.12e \n"
		"frames                   : %zi     \n"
		"==========================         \n",
		f_start.Time(),time_stop.Time(),integrator->timestep.GetDouble(),
                (*this)[size()-1].Time(),sample_period.GetDouble(),size());
	//
	if (universe->GetUniverseType() == Real) {
	  int y,m,d;
	  double frac;
	  // 
	  f_start.GetDate().GetGregor(y,m,d);
	  frac = f_start.GetDate().GetDayFraction();
	  ORSA_ERROR(
		  "f_start date:    %i/%02i/%013.10f",
		  y,m,d+frac);
	  //
	  time_stop.GetDate().GetGregor(y,m,d);
	  frac = time_stop.GetDate().GetDayFraction();
	  ORSA_ERROR(
		  "time_stop date:  %i/%02i/%013.10f",
		  y,m,d+frac);
	}
      }
      
      if (integrator->timestep.IsZero()) integrator->timestep = last_genuine_timestep;
      
      // if ( fabs(f_start.Time() + integrator->timestep - (*this)[size()-1].Time()) > sample_period)  {
      // if ( (f_start + integrator->timestep - (*this)[size()-1]).absolute() > sample_period) {
      if ( (f_start - (*this)[size()-1] + integrator->timestep).absolute() > sample_period) {
	if (integrator->timestep > 0.0) last_genuine_timestep = integrator->timestep;
	// integrator->timestep = (*this)[size()-1].Time() + sign*sample_period - f_start.Time();
    	integrator->timestep = (*this)[size()-1] - f_start + sign*sample_period;
	/* 
	   fprintf(stderr,
	   "timestep changed: from %.20e to %.20e      val: %.20e sp: %.20e\n",
	   last_genuine_timestep.GetDouble(),
	   integrator->timestep.GetDouble(),
	   (f_start + integrator->timestep - (*this)[size()-1]).absolute().GetDouble(),
	   sample_period.GetDouble());
	*/
      }
      
      // cerr << "timestep (tmp):    " << integrator->timestep.GetDouble() << endl;
      
      if (integrator->timestep > 0) {
	// cerr << "timestep > 0 !!!" << endl;
	// if ( (f_start.Time()+integrator->timestep) > (time_stop.Time()) ) {
	if ( (f_start+integrator->timestep) > time_stop) {
	  // integrator->timestep = time_stop.Time() - f_start.Time();
	  if (integrator->timestep > 0.0) last_genuine_timestep = integrator->timestep;
	  integrator->timestep = time_stop - f_start;
	}
      } else if (integrator->timestep < 0) {
	// cerr << "timestep < 0 !!!" << endl;
	// if ( (f_start.Time()+integrator->timestep) < (time_stop.Time()) ) {
	if ( (f_start+integrator->timestep) < time_stop) {
	  // integrator->timestep = time_stop.Time() - f_start.Time();
	  if (integrator->timestep > 0.0) last_genuine_timestep = integrator->timestep;
	  integrator->timestep = time_stop - f_start;
	}
      } else {
	ORSA_WARNING("Timestep equal to zero!");
	// timestep == 0...
      }
      
      // cerr << "timestep (tmp-II): " << integrator->timestep.GetDouble() << endl;
      
      /* 
	 if (f_start == time_stop) {
	 return;
	 }
      */
      
      // debug
      /* 
	 if (integrator->timestep.absolute().GetDouble() < FromUnits(1.0,SECOND)) {
	 ORSA_ERROR("timestep smaller than 1.0 seconds!");
	 // sleep(1.0);
	 }
      */
      
      // step
      if (integrator->timestep.IsZero()) {
	ORSA_ERROR("zero timestep, no integrator->Step() call...");
	f_stop = f_start;
      } else {
	integrator->Step(f_start,f_stop,interaction);
	if (f_start.Time() != f_stop.Time()) {
	  ++total_substeps;
	}
      }
      
      if (0) {
      	if (universe->GetUniverseType() == Real) {
	  int y,m,d;
	  double frac;
	  // 
	  f_stop.GetDate().GetGregor(y,m,d);
	  frac = f_stop.GetDate().GetDayFraction();
	  ORSA_ERROR(
		  "f_stop date:     %i/%02i/%013.10f",
		  y,m,d+frac);
	}
      }
      
      /* 
	 {
	 // debug
	 fprintf(stderr,"..after step: %g\n",((f_stop - (*this)[size()-1]).absolute() - sample_period).absolute().GetDouble());
	 fprintf(stderr,"....timestep: %g\n",integrator->timestep.GetDouble());
	 fprintf(stderr,"....time....: %g\n",f_stop.Time());
	 }
      */
      
      /* 
	 { 
	 // debug
	 fprintf(stderr,
	 "save frame test: %.20e\n"
	 "sign*sample_period: %.20e      (*this)[size()-1].GetTime(): %.9f\n",
	 // ((f_stop-(*this)[size()-1]).absolute()-sample_period).absolute().GetDouble());
	 ( (*this)[size()-1] - f_stop + sign*sample_period).GetDouble(),
	 sign*sample_period.GetDouble(),
	 (*this)[size()-1].GetTime());
	 }
      */
      
      // save frame
      switch (universe->GetUniverseType()) {
      case Real:
	if ( ( (*this)[size()-1] - f_stop + sign*sample_period).IsZero() ) { 
	  save_frame = true;
	}
	break;
      case Simulated:
	if ( fabs( fabs(f_stop.Time() - (*this)[size()-1].Time()) - sample_period.GetDouble()) < (fabs(f_stop.Time())+fabs((*this)[size()-1].Time())+fabs(sample_period.GetDouble()))*total_substeps*std::numeric_limits<double>::epsilon()) {
	  save_frame = true;
	}
	break;
      }
      
      // end of integration
      /* if ( fabs(f_stop.Time() - time_stop.Time()) < fabs(time_stop.Time())*std::numeric_limits<double>::epsilon()) {
	 continue_integration=false;
	 if (save_last_anyway) save_frame=true;
	 }
      */
      //
      // cerr << "end of integration checks..." << endl;
      // end of integration
      switch (universe->GetUniverseType()) {
      case Real:
	// if (f_stop == time_stop) {
	// if (f_stop.GetDate() == time_stop.GetDate()) {
	if ((f_stop - time_stop).IsZero()) { // IMPORTANT! This one works properly, while "(f_stop == time_stop)" or "(f_stop.GetDate() == time_stop.GetDate())" don't... WHY??
	  // cerr << "(f_stop == time_stop)" << endl;
	  continue_integration = false;
	  if (save_last_anyway) save_frame = true;
	} else {
	  /* 
	     cerr << "(f_stop != time_stop)" << endl;
	     //
	     const Date fs_date = f_stop.GetDate();
	     const Date ts_date = time_stop.GetDate();
	     int y,m,d;
	     double frac;
	     //
	     fs_date.GetGregor(y,m,d);
	     frac = fs_date.GetDayFraction();
	     ORSA_ERROR(
	     "     fs_date:    %i/%02i/%013.10f",
	     y,m,d+frac);
	     //
	     ts_date.GetGregor(y,m,d);
	     frac = ts_date.GetDayFraction();
	     ORSA_ERROR(
	     "     ts_date:    %i/%02i/%013.10f",
	     y,m,d+frac);
	     //
	     ORSA_ERROR(
	     "   ts-fs: %013.10f seconds",
	     FromUnits(ts_date.GetTime()-fs_date.GetTime(),SECOND,-1));
	     //
	     UniverseTypeAwareTime fsw = fs_date;
	     UniverseTypeAwareTime tsw = ts_date;
	     ORSA_ERROR(
	     "2- ts-fs: %013.10f seconds",
	     FromUnits((tsw-fsw).GetDouble(),SECOND,-1));
	  */
	}
	break;
      case Simulated:
	if ( fabs(f_stop.Time() - time_stop.Time()) < fabs(time_stop.Time())*total_substeps*std::numeric_limits<double>::epsilon()) {
	  continue_integration = false;
	  if (save_last_anyway) save_frame = true;
	}
	break;
      }
      
      if (save_frame) {
      	step_done(time_start,time_stop,last_genuine_timestep,f_stop,continue_integration);
	if (interaction->IsSkippingJPLPlanets()) {
	  f_stop.ForceJPLEphemerisData();
	}
	push_back(f_stop);
	save_frame = false;
	integrator->timestep = last_genuine_timestep;
	unsaved_substeps = 0;
      } else {
	++unsaved_substeps;
	if (max_unsaved_substeps_active) {
	  if (unsaved_substeps > max_unsaved_substeps) {
	    continue_integration=false;
	    ORSA_WARNING("max unsaved substeps hit!!! stopping integration!");
	  }
	}
      }
      
      // test
      /* 
	 if (integrator->timestep.IsZero()) {
	 continue_integration=false;
	 }
      */
    }
    
    integration_finished();
  }
  
  void Evolution::Integrate(const Frame & f, const UniverseTypeAwareTime & utat_start, const UniverseTypeAwareTime& utat_stop) {
    
    // clear Evolution
    clear();
    
    UniverseTypeAwareTime t_start = utat_start;
    UniverseTypeAwareTime t_stop  = utat_stop;
    
    if (utat_start > utat_stop) {
      t_start = utat_stop;
      t_stop  = utat_start;
    }
    
    const UniverseTypeAwareTimeStep saved_sample_period = sample_period;
    const UniverseTypeAwareTimeStep huge_sample_period  = FromUnits(1e3,YEAR);
    
    Frame running_frame;
    
    if (t_start < f) {
      if (t_stop < f) {
	push_back(f);
	sample_period = huge_sample_period;
	Integrate(t_stop,true);
	running_frame = (*this)[size()-1];
      	clear();
	push_back(running_frame);
	sample_period = saved_sample_period;
	Integrate(t_start);
      } else {
	push_back(f);
	Integrate(t_start);
	// swap
	(*this)[0] = (*this)[size()-1];
	(*this)[size()-1] = f;
	//
	Integrate(t_stop);
      }
    } else {
      push_back(f);
      sample_period = huge_sample_period;
      Integrate(t_start,true);
      running_frame = (*this)[size()-1];
      clear();
      push_back(running_frame);
      sample_period = saved_sample_period;
      Integrate(t_stop);
    }
    
    std::sort(begin(),end());
  }
  
  // Frame StartFrame(const vector<BodyWithEpoch> &f, vector<JPLBody> &jf, Interaction *itr, Integrator *itg, const UniverseTypeAwareTime &t) {
  Frame StartFrame(const vector<BodyWithEpoch> & f, vector<JPL_planets> & jf, const Interaction * itr, const Integrator * itg, const UniverseTypeAwareTime & t) {
  // Frame StartFrame(const vector<BodyWithEpoch> & f, vector<JPL_planets> & jf, InteractionType interaction_type, IntegratorType integrator_type, const UniverseTypeAwareTime & t) {
    
    // const double original_timestep = itg->timestep;
    const UniverseTypeAwareTimeStep original_timestep = itg->timestep;
    
    Frame frame_start;
    
    switch (universe->GetUniverseType()) {
    case Real: { 
      frame_start.SetDate(t.GetDate());
      Frame          running_frame;
      Evolution      running_evolution;
      // running_evolution.interaction = itr->clone();
      // running_evolution.integrator  = itg->clone();
      running_evolution.SetIntegrator(itg);
      running_evolution.SetInteraction(itr);
      
      // put the JPL bodies into the frame_start
      /* 
	 if (jf.size()) {
	 Date ddf = t.GetDate();
	 unsigned int k;
	 for (k=0;k<jf.size();++k) {
	 jf[k].SetEpoch(ddf);
	 Body b(jf[k].name(),
	 jf[k].mass(),
	 jf[k].radius(),
	 jf[k].position(),
	 jf[k].velocity());
	 frame_start.push_back(b);
	 }
	 }
      */
      //
      if (jf.size()) {
	unsigned int k;
	for (k=0;k<jf.size();++k) {
	  frame_start.push_back(JPLBody(jf[k],t.GetDate()));
	}
      }
      
      if (f.size()) {
	
	Frame last_frame;
	unsigned int base_bodies, l;
	
	unsigned int j=0;
      	while (j < f.size()) {
	  
	  running_frame.clear();
	  // running_frame.SetDate(f[j].epoch.GetDate());
	  running_frame.SetDate(f[j].GetEpoch().GetDate());
	  
	  running_frame.push_back(f[j]);
	  ++j;
	  
	  // while ((j<f.size()) && (f[j].epoch.GetDate() == running_frame.GetDate())) {
	  while ((j<f.size()) && (f[j].Epoch().GetDate() == running_frame.GetDate())) {
	    running_frame.push_back(f[j]);
	    ++j;
	  }
	  
	  base_bodies = running_frame.size();
	  
	  // JPL 
	  /* 
	     Date ddt = running_frame.GetDate();
	     unsigned int k;
	     for (k=0;k<jf.size();++k) {
	     jf[k].SetEpoch(ddt);
	     Body b(jf[k].name(),
	     jf[k].mass(),
	     jf[k].radius(),
	     jf[k].position(),
	     jf[k].velocity());
	     running_frame.push_back(b);
	     }
	  */
	  {
	    unsigned int k;
	    for (k=0;k<jf.size();++k) {
	      running_frame.push_back(JPLBody(jf[k],running_frame.GetDate()));
	    }
	  }
	  
	  running_evolution.clear();
	  running_evolution.push_back(running_frame);
	  // running_evolution.sample_period = fabs(running_frame.GetTime() - frame_start.GetTime());
	  running_evolution.SetSamplePeriod((running_frame - frame_start).absolute());
	  running_evolution.SetIntegratorTimeStep(original_timestep);
	  running_evolution.Integrate(t);
	  
	  last_frame = running_evolution[running_evolution.size()-1];
	  if (fabs(last_frame.Time()-frame_start.Time()) > FromUnits(1.0e-3,SECOND)) {
	    /* 
	       fprintf(stderr,"!!! last_frame.Time() != frame_start.Time() --->> %30.20f != %30.20f\n",last_frame.Time(),frame_start.Time());
	       cerr << "|dT| = " << FromUnits(fabs(last_frame.Time()-frame_start.Time()),SECOND,-1) << " seconds" << endl;
	       print(last_frame);
	    */
	    ORSA_WARNING(
		    "last_frame.Time() != frame_start.Time() --->> %30.20f != %30.20f\n"
		    "|dT| = %g seconds\n",
		    last_frame.Time(),frame_start.Time(),FromUnits(fabs(last_frame.Time()-frame_start.Time()),SECOND,-1));
	    ORSA_WARNING("  objects in frame: ");
	    unsigned int k;
	    for (k=0;k<last_frame.size();++k) {
	      ORSA_WARNING("    %s", last_frame[k].name().c_str());
	    }
	  }
	  
	  for (l=0;l<base_bodies;++l) {
	    frame_start.push_back(last_frame[l]);
	  }
	}
      }
      break;
    }
    case Simulated: {
      frame_start.SetTime(0.0);
      unsigned int k;  
      for (k=0;k<f.size();++k) {
	frame_start.push_back(f[k]);
      }
      break;
    }
    }
    
    return frame_start;
  }
  
} // namespace orsa
