/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_config.h"
#include "orsa_file.h"

using namespace std;

namespace orsa {
  
  string Label(const ConfigEnum e) {
    string label;
    switch(e) {
    case JPL_EPHEM_FILE:     label="JPL ephemeris";                               break;
    case JPL_DASTCOM_NUM:    label="JPL asteroids database (NUM)";                break;
    case JPL_DASTCOM_UNNUM:  label="JPL asteroids database (UNNUM)";              break;
    case JPL_DASTCOM_COMET:  label="JPL comets database";                         break;
    case LOWELL_ASTORB:      label="Lowell asteroids database";                   break;
    case MPC_MPCORB:         label="MPC asteroids database";                      break;
    case MPC_COMET:          label="MPC comets database";                         break;
    case MPC_NEA:            label="MPC asteroids database (NEA)";                break;
    case MPC_DAILY:          label="MPC asteroids database (DAILY)";              break;
    case MPC_DISTANT:        label="MPC asteroids database (DISTANT)";            break;     
    case MPC_PHA:            label="MPC asteroids database (PHA)";                break;
    case MPC_UNUSUALS:       label="MPC asteroids database (UNUSUALS)";           break;     
    case ASTDYS_ALLNUM_CAT:  label="AstDyS asteroids database (CAT)";             break;
    case ASTDYS_ALLNUM_CTC:  label="AstDyS asteroids database (CTC)";             break;
    case ASTDYS_ALLNUM_CTM:  label="AstDyS asteroids database (CTM)";             break;
    case ASTDYS_UFITOBS_CAT: label="AstDyS unnumbered asteroids database (CAT)";  break;
    case ASTDYS_UFITOBS_CTC: label="AstDyS unnumbered asteroids database (CTC)";  break;
    case ASTDYS_UFITOBS_CTM: label="AstDyS unnumbered asteroids database (CTM)";  break;
    case NEODYS_CAT:         label="NEODyS asteroids database (CAT)";             break;
    case NEODYS_CTC:         label="NEODyS asteroids database (CTC)";             break;
    case OBSCODE:            label="Observatory codes";                           break;
      // TLE
    case TLE_NASA:    label="TLE (NASA)";    break;
    case TLE_GEO:     label="TLE (GEO)";     break;
    case TLE_GPS:     label="TLE (GPS)";     break;
    case TLE_ISS:     label="TLE (ISS)";     break;
    case TLE_KEPELE:  label="TLE (KEPELE)";  break;
    case TLE_VISUAL:  label="TLE (VISUAL)";  break;
    case TLE_WEATHER: label="TLE (WEATHER)"; break;   
      // textures
    case TEXTURE_SUN :    label="Sun's texture"; break;   
    case TEXTURE_MERCURY: label="Mercury's texture"; break;   
    case TEXTURE_VENUS:   label="Venus's texture"; break;   
    case TEXTURE_EARTH:   label="Earth's texture"; break;   
    case TEXTURE_MOON:    label="Moon's texture";  break;   
    case TEXTURE_MARS:    label="Mars's texture"; break;   
    case TEXTURE_JUPITER: label="Jupiter's texture"; break;   
    case TEXTURE_SATURN:  label="Saturn's texture"; break;   
    case TEXTURE_URANUS:  label="Uranus's texture"; break;   
    case TEXTURE_NEPTUNE: label="Neptune's texture"; break;   
    case TEXTURE_PLUTO:   label="Pluto's texture"; break;   
      //
    case NO_CONFIG_ENUM:  label="This shuld not be used!"; break;
    }
    return label;
  }
  
  Config::Config() {
    paths[JPL_EPHEM_FILE] =     new ConfigItem<string>("JPL_EPHEM_FILE");
    paths[JPL_DASTCOM_NUM] =    new ConfigItem<string>("JPL_DASTCOM_NUM");
    paths[JPL_DASTCOM_UNNUM] =  new ConfigItem<string>("JPL_DASTCOM_UNNUM");
    paths[JPL_DASTCOM_COMET] =  new ConfigItem<string>("JPL_DASTCOM_COMET");
    paths[LOWELL_ASTORB] =      new ConfigItem<string>("LOWELL_ASTORB");
    paths[MPC_MPCORB] =         new ConfigItem<string>("MPC_MPCORB");
    paths[MPC_COMET] =          new ConfigItem<string>("MPC_COMET");
    paths[MPC_NEA] =            new ConfigItem<string>("MPC_NEA");
    paths[MPC_DAILY] =          new ConfigItem<string>("MPC_DAILY");
    paths[MPC_DISTANT] =        new ConfigItem<string>("MPC_DISTANT");
    paths[MPC_PHA] =            new ConfigItem<string>("MPC_PHA");
    paths[MPC_UNUSUALS] =       new ConfigItem<string>("MPC_UNUSUALS");
    paths[ASTDYS_ALLNUM_CAT] =  new ConfigItem<string>("ASTDYS_ALLNUM_CAT");
    paths[ASTDYS_ALLNUM_CTC] =  new ConfigItem<string>("ASTDYS_ALLNUM_CTC");
    paths[ASTDYS_ALLNUM_CTM] =  new ConfigItem<string>("ASTDYS_ALLNUM_CTM");
    paths[ASTDYS_UFITOBS_CAT] = new ConfigItem<string>("ASTDYS_UFITOBS_CAT");
    paths[ASTDYS_UFITOBS_CTC] = new ConfigItem<string>("ASTDYS_UFITOBS_CTC");
    paths[ASTDYS_UFITOBS_CTM] = new ConfigItem<string>("ASTDYS_UFITOBS_CTM");
    paths[NEODYS_CAT] =         new ConfigItem<string>("NEODYS_CAT");
    paths[NEODYS_CTC] =         new ConfigItem<string>("NEODYS_CTC");
    paths[OBSCODE] =            new ConfigItem<string>("OBSCODE");
    // TLE
    paths[TLE_NASA] =    new ConfigItem<string>("TLE_NASA");
    paths[TLE_GEO] =     new ConfigItem<string>("TLE_GEO");
    paths[TLE_GPS] =     new ConfigItem<string>("TLE_GPS");
    paths[TLE_ISS] =     new ConfigItem<string>("TLE_ISS");
    paths[TLE_KEPELE] =  new ConfigItem<string>("TLE_KEPELE");
    paths[TLE_VISUAL] =  new ConfigItem<string>("TLE_VISUAL");
    paths[TLE_WEATHER] = new ConfigItem<string>("TLE_WEATHER");
    // textures
    paths[TEXTURE_SUN]     = new ConfigItem<string>("TEXTURE_SUN");
    paths[TEXTURE_MERCURY] = new ConfigItem<string>("TEXTURE_MERCURY");
    paths[TEXTURE_VENUS]   = new ConfigItem<string>("TEXTURE_VENUS");
    paths[TEXTURE_EARTH]   = new ConfigItem<string>("TEXTURE_EARTH");
    paths[TEXTURE_MOON]    = new ConfigItem<string>("TEXTURE_MOON");
    paths[TEXTURE_MARS]    = new ConfigItem<string>("TEXTURE_MARS");
    paths[TEXTURE_JUPITER] = new ConfigItem<string>("TEXTURE_JUPITER");
    paths[TEXTURE_SATURN]  = new ConfigItem<string>("TEXTURE_SATURN");
    paths[TEXTURE_URANUS]  = new ConfigItem<string>("TEXTURE_URANUS");
    paths[TEXTURE_NEPTUNE] = new ConfigItem<string>("TEXTURE_NEPTUNE");
    paths[TEXTURE_PLUTO]   = new ConfigItem<string>("TEXTURE_PLUTO");
    
    // DON'T READ HERE!!!
    // read_from_file();
  }
  
  void Config::read_from_file() {
    OrsaConfigFile ocf;
    ocf.Read();
    ocf.Close();
  }
  
  void Config::write_to_file() {
    OrsaConfigFile ocf;
    ocf.Write();
    ocf.Close();
  }
  
} // namespace orsa
