/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2003 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef _ORSA_VERSION_H_
#define _ORSA_VERSION_H_

#define ORSA_VERSION       "0.7.0"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef RELEASE_DATE
#define ORSA_RELEASE_DATE  RELEASE_DATE
#endif

#ifdef CONFIG_DATE
#define ORSA_CONFIG_DATE   CONFIG_DATE
#endif

#define ORSA_COMPILE_DATE  __DATE__

#endif // _ORSA_VERSION_H_
