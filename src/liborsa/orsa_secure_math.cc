/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_secure_math.h"
#include "orsa_error.h"
#include "support.h"

#include <cmath>

#include <iostream>

using namespace std;

namespace orsa {
  
  // avoids domain errors when x<0 and non-integer y
  double secure_pow(double x, double y) {
    
    if (x<0.0) {
      if (rint(y)!=y) {
	ORSA_DOMAIN_ERROR("secure_pow(%g,%g) is undefined!",x,y);
	return 1.0; // better value?
      } else {
	return pow(x,y);
      }
    } else {
      return pow(x,y);
    }
  }
  
  // avoids domain errors when x<=0
  double secure_log(double x) {
    if (x>0) {
      return log(x);
    } else {
      ORSA_DOMAIN_ERROR("secure_log(%g) is undefined!",x);
      return 1.0; // better value?
    }
  }
  
  // avoids domain errors when x<=0
  double secure_log10(double x) {
    if (x>0) {
      return log10(x);
    } else {
      ORSA_DOMAIN_ERROR("secure_log10(%g) is undefined!",x);
      return 1.0; // better value?
    }
  }
  
  // avoids domain errors when x=y=0
  double secure_atan2(double x, double y) {
    if (x==0.0) {
      if (y==0.0) {
	// domain error
	ORSA_DOMAIN_ERROR("secure_atan2(%g,%g) is undefined!",x,y);
	return 1.0; // better value?
      } else {
	return atan2(x,y);
      }
    } else {
      return atan2(x,y);
    }
  }
  
  // avoids domain errors when x is not in [-1,1]
  double secure_asin(double x) {
    if ((x>1.0) || (x<-1.0)) {
      // domain error
      ORSA_DOMAIN_ERROR("secure_asin(%g) is undefined!",x);
      return 1.0; // better value?
    } else {
      return asin(x);
    }
  }
  
  // avoids domain errors when x is not in [-1,1]
  double secure_acos(double x) {
    if ((x>1.0) || (x<-1.0)) {
      // domain error
      ORSA_DOMAIN_ERROR("secure_acos(%g) is undefined!",x);
      return 1.0; // better value?
    } else {
      return acos(x);
    }
  }
  
  // avoids domain errors when x<0
  double secure_sqrt(double x) {
    if (x<0) {
      // domain error
      ORSA_DOMAIN_ERROR("secure_sqrt(%g) is undefined!",x);
      return sqrt(fabs(x)); // better value?
    } else {
      return sqrt(x);
    }
  }
  
} // namespace orsa
