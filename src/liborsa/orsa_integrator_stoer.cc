/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <iostream>

#include "orsa_integrator.h"
#include "orsa_common.h"

namespace orsa {
  
  Stoer::Stoer() : MultistepIntegrator() {
    type = STOER;
    m = 8;
  }
  
  Stoer::Stoer(int m_) : MultistepIntegrator() {
    type = STOER;
    m = m_;
  }
  
  Stoer::Stoer(const Stoer & i) : MultistepIntegrator() {
    type     = i.type;
    timestep = i.timestep;
    accuracy = i.accuracy;
    m        = i.m;
  }
  
  Stoer::~Stoer() {
    
  };
  
  Integrator * Stoer::clone() const {
    return new Stoer(*this);
  }
  
  void Stoer::Step(const Frame &frame_in, Frame &frame_out, Interaction *interaction) {
    
    const unsigned int n = frame_in.size();
    double h, h2, hh; // local_x;
    
    unsigned int i, k;
    
    // h  = h_tot / m ;
    h  = timestep.GetDouble() / m ;
    h2 = h * 0.5   ;
    hh = h * h     ;
    
    std::vector<Vector> temp(n),delta(n);
    
    frame_out = frame_in;
    
    // initial values
    interaction->Acceleration(frame_out,temp);
    
    for (i=0;i<frame_out.size();++i) {
      delta[i] +=  h * ( frame_out[i].velocity() + h2 * temp[i] );  
      frame_out[i].AddToPosition(delta[i]); 
    }
    
    for ( k=1; k <= (m-1) ; ++k) {
      if (interaction->IsSkippingJPLPlanets()) {
	frame_out.SetTime(frame_in + h*k);
	frame_out.ForceJPLEphemerisData();
      }
      for (i=0;i<frame_out.size();++i) {     
        interaction->Acceleration(frame_out,temp);
        delta[i] += hh * temp[i];
        frame_out[i].AddToPosition(delta[i]);
      }
    }
    
    for (i=0;i<frame_out.size();++i) 
      frame_out[i].SetVelocity(delta[i]/h + h2 * temp[i]);   
    
    // frame_out.time += timestep;
    // frame_out.SetTime(frame_in.Time() + timestep);
    // frame_out.AddTimeStep(timestep);
    // frame_out += timestep;
    frame_out.SetTime(frame_in + timestep);
  }
  
} // namespace orsa
