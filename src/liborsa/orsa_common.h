/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _ORSA_COMMON_H_
#define _ORSA_COMMON_H_

// #include "orsa_units.h"

#ifdef PI
#undef PI
#endif
#define PI  3.1415926535897932384626433832795029L  /* long double constant, thanks to the trailing L */

#define MAX(a, b)      ((b) < (a) ? (a) : (b))
#define MIN(a, b)      ((a) < (b) ? (a) : (b))
#define ABS(a)         ((a) >= 0  ? (a) : -(a))

namespace orsa {

  #if defined(__BORLANDC__)
  #if (__BORLANDC__ <= 0x0564)
  #pragma option -w-8080
  // otherwise we get bogus warnings for constants below
  #endif
  #endif
  const double halfpi = PI/2;
  const double pi     = PI;
  const double twopi  = PI+PI;
  const double pisq   = PI*PI;
  
  // world visible units // defined in orsa_universe.cc
  // extern Units units;
  
} // namespace orsa

#endif // _ORSA_COMMON_H_
