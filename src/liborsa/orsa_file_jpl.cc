/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_file.h"
#include "orsa_error.h"
#include "orsa_secure_math.h"

#include <cstdio>

#include "sdncal.h"
#include "jpleph.h"
#include "jpl_int.h"

#include <iostream>

using namespace std;

namespace orsa {
  

  // UPDATE (Apr 2004): the results are the same, no matter what default object is selected
  // const JPL_planets JPLFile::default_ephem_center = SOLAR_SYSTEM_BARYCENTER;
  // const JPL_planets JPLFile::default_ephem_center = SUN;
  //
  // CORRECT: keep the EARTH as default center
  // const JPL_planets JPLFile::default_ephem_center = EARTH;
  //
  const JPL_planets JPLFile::default_ephem_center = SOLAR_SYSTEM_BARYCENTER;
  
  // JPL
  JPLFile::JPLFile(string name) : calc_velocity(true) {
    
    int N=0; // number of elements in nam[][6] and val[]
    const int max_N = 256;
    char nam[max_N][6];
    double val[max_N];
    
    // jpl_database = (jpl_eph_data *) jpl_init_ephemeris(name.c_str(),nam,val);
    jpl_database = (void *) jpl_init_ephemeris(name.c_str(),NULL,NULL);
    
    if (jpl_database != 0) {
      N = ((jpl_eph_data*)jpl_database)->ncon;
      if (N > max_N) {
	ORSA_ERROR("assumed max_N=%i is smaller than N=%i. Please recompile with a bigger max_N.",max_N,N);
       	exit(0);
      }
      jpl_close_ephemeris((jpl_eph_data *)jpl_database);
      
      // jpl_database = (void *) jpl_init_ephemeris(name.c_str(),nam,val);
      jpl_database = (void *) jpl_init_ephemeris(name.c_str(),nam,val);
    }
    
    if (jpl_database==0) {
      ORSA_ERROR("Can't open JPL ephemeris file [%s]",name.c_str());
      // exit is too much, should handle better this problem
      // exit(0);
      return;
    }
    
    // jpl_eph_data *jpldb = (jpl_eph_data *) jpl_database;
    // ephem_start.SetTime(FromUnits(jpldb->ephem_start,DAY));
    // ephem_end.SetTime(  FromUnits(jpldb->ephem_end,  DAY));
    
    bool_ephem_start_computed = bool_ephem_end_computed = false;  
    
    map_tag = new map<std::string,double>;
    
    // fill map_tag, DON'T REMOVE
    {
      string tag;
      char ctag[7];
      ctag[6] = 0;
      // cerr << "N: " << N << endl;
      for (int l = 0; l < N; l ++) {
	memcpy(ctag, nam[l], 6);
	tag = ctag;
	remove_leading_trailing_spaces(tag);
	(*map_tag)[tag] = val[l];
#if 0
	printf(" [l=%03i][%s] = %20.12e\n",l,tag.c_str(),val[l]);
	printf(" map_tag[%s] = %20.12e\n",tag.c_str(),(*map_tag)[tag]);
	printf(" map_tag[%s] = %20.12e\n",tag.c_str(),GetTag(tag));
#endif
      } 
    } 
  }
  
  JPLFile::~JPLFile() {
    if (jpl_database != 0) jpl_close_ephemeris((jpl_eph_data *)jpl_database);
    if (map_tag) delete map_tag;
  }
  
  const UniverseTypeAwareTime & JPLFile::EphemStart() {
    if (!bool_ephem_start_computed) ComputeEphemStart();
    return ephem_start;
  }
  
  const UniverseTypeAwareTime & JPLFile::EphemEnd() {
    if (!bool_ephem_end_computed) ComputeEphemEnd();
    return ephem_end;
  }
  
  void JPLFile::ComputeEphemStart() {
    jpl_eph_data *jpldb = (jpl_eph_data *) jpl_database;
    ephem_start.SetTime(FromUnits(jpldb->ephem_start,DAY));
    bool_ephem_start_computed = true;
  }
  
  void JPLFile::ComputeEphemEnd() {
    jpl_eph_data *jpldb = (jpl_eph_data *) jpl_database;
    ephem_end.SetTime(FromUnits(jpldb->ephem_end,DAY));
    bool_ephem_end_computed = true;
  }
  
  double JPLFile::GetAU_MKS() {
    return (GetTag("AU")*1.0e3);
  }
  
  double JPLFile::GetMSun_MKS() {
    //return (GetTag("GMS")*secure_pow(GetAU_MKS(),3.0)*secure_pow(24*3600.0,-2.0)/GetG_MKS());
    const double au_mks = GetAU_MKS();
    const double day_to_second = 24*3600.0;
    return (GetTag("GMS")*(au_mks*au_mks*au_mks)/(day_to_second*day_to_second)/GetG_MKS());
  }
  
  double JPLFile::GetMJupiter_MKS() {
    // return (GetTag("GM5")*secure_pow(GetAU_MKS(),3.0)*secure_pow(24*3600.0,-2.0)/GetG_MKS());
    const double au_mks = GetAU_MKS();
    const double day_to_second = 24*3600.0;
    return (GetTag("GM5")*(au_mks*au_mks*au_mks)/(day_to_second*day_to_second)/GetG_MKS());
  }
  
  double JPLFile::GetMEarth_MKS() {
    // const double EMRAT = val[7]; // Earth/Moon mass ratio
    const double EMRAT = GetTag("EMRAT");
    // return ((val[10]*EMRAT/(1+EMRAT))*pow(AU_MKS,3)*pow(24*3600.0,-2)/G_MKS);
    // return ((GetTag("GMB")*EMRAT/(1+EMRAT))*pow(AU_MKS,3.0)*pow(24*3600.0,-2.0)/G_MKS);
    // return ((GetTag("GMB")*EMRAT/(1+EMRAT))*secure_pow(GetAU_MKS(),3.0)*secure_pow(24*3600.0,-2.0)/GetG_MKS());
    const double au_mks = GetAU_MKS();
    const double day_to_second = 24*3600.0;
    return ((GetTag("GMB")*EMRAT/(1+EMRAT))*(au_mks*au_mks*au_mks)/(day_to_second*day_to_second)/GetG_MKS());
  }
  
  double JPLFile::GetMMoon_MKS() {
    // const double EMRAT = val[7]; // Earth/Moon mass ratio
    const double EMRAT = GetTag("EMRAT");
    // return ((val[10]/(1+EMRAT))*pow(AU_MKS,3)*pow(24*3600.0,-2)/G_MKS);
    // return ((GetTag("GMB")/(1+EMRAT))*pow(AU_MKS,3.0)*pow(24*3600.0,-2.0)/G_MKS);
    // return ((GetTag("GMB")/(1+EMRAT))*secure_pow(GetAU_MKS(),3.0)*secure_pow(24*3600.0,-2.0)/GetG_MKS());
    const double au_mks = GetAU_MKS();
    const double day_to_second = 24*3600.0;
    return ((GetTag("GMB")/(1+EMRAT))*(au_mks*au_mks*au_mks)/(day_to_second*day_to_second)/GetG_MKS());
  }
  
  double JPLFile::GetC_MKS() {
    return (GetTag("CLIGHT")*1.0e3);
  }
  
  double JPLFile::GetREarth_MKS() {
    return (GetTag("RE")*1.0e3);
  }
  
  double JPLFile::GetRMoon_MKS() {
    return (GetTag("AM")*1.0e3);
  }
  
  double JPLFile::GetMass(const JPL_planets planet) {
    
    // const double file_G = FromUnits( FromUnits( FromUnits(val[17],AU,3),MSUN,-1),DAY,-2);
    // cerr << "JPLFile::GetMass G=" << file_G << endl;
    
    // const double EMRAT = val[7]; // Earth/Moon mass ratio
    const double EMRAT = GetTag("EMRAT");
    
    double GM=0;
    
    switch(planet) {
    case MERCURY:
      GM = GetTag("GM1");
      break;
    case VENUS:
      GM = GetTag("GM2");
      break;
    case MARS:
      GM = GetTag("GM4");
      break;
    case JUPITER:
      GM = GetTag("GM5");
      break;
    case SATURN:
      GM = GetTag("GM6");
      break;
    case URANUS:
      GM = GetTag("GM7");
      break;
    case NEPTUNE: 
      GM = GetTag("GM8");
      break;
    case PLUTO:   
      GM = GetTag("GM9");
      break;
    case EARTH:
      GM = GetTag("GMB")*EMRAT/(1+EMRAT);
      break;
    case MOON:
      GM = GetTag("GMB")/(1+EMRAT);
      break;
    case EARTH_MOON_BARYCENTER:
      GM = GetTag("GMB");
      break;
    case SUN:
      GM = GetTag("GMS");
      break;
    default:
      GM = 0;
      break;
    }
    //
    // take the right units
    GM = FromUnits(FromUnits(GM,AU,3),DAY,-2);
    
    // return (GM/file_G);
    return (GM/GetG());
  }
  
  double JPLFile::GetTag(string tag) {
    remove_leading_trailing_spaces(tag);
    return (*map_tag)[tag];
  }
  
  //
  
  void JPLFile::GetEph(const UniverseTypeAwareTime & date, JPL_planets target, JPL_planets center, Vector & position, Vector & velocity) {
    double  xv[6];   
    
    jpl_eph_data * jpldb = (jpl_eph_data *) jpl_database;
    
    if ( (date < EphemStart()) ||
	 (date > EphemEnd()) ) {
      ORSA_WARNING("requested time out of the jpl database range");
      return;
    }
    
    jpl_pleph(jpldb,date.GetDate().GetJulian(ET),target,center,xv,calc_velocity ? 1 : 0);
    
    if ((target==NUTATIONS) || 
	(target==LIBRATIONS)) {
      // no units correction needed, are radians
      position.Set(xv[0],xv[1],xv[2]);
      velocity.Set(xv[3],xv[4],xv[5]);
      return;
    }
    
    // position (from AU)
    xv[0] = FromUnits(xv[0],AU);
    xv[1] = FromUnits(xv[1],AU);
    xv[2] = FromUnits(xv[2],AU);
    //
    position.Set(xv[0],xv[1],xv[2]);
    
    if (calc_velocity) {
      // velocity (from AU/DAY)
      xv[3] = FromUnits(xv[3],AU);
      xv[4] = FromUnits(xv[4],AU);
      xv[5] = FromUnits(xv[5],AU);
      //
      xv[3] = FromUnits(xv[3],DAY,-1);
      xv[4] = FromUnits(xv[4],DAY,-1);
      xv[5] = FromUnits(xv[5],DAY,-1);
      //
      velocity.Set(xv[3],xv[4],xv[5]);
    }
    
    // THIS IS THE ONLY CORRECT ROTATION:
    // from mean equatorial J2000 (ICRF) to mean ecliptic J2000
    if (universe->GetReferenceSystem() == ECLIPTIC) {
      Angle obl = obleq_J2000();
      position.rotate(0.0,-obl.GetRad(),0.0);
      velocity.rotate(0.0,-obl.GetRad(),0.0);
    }
  }
  
  // small utility
  /* 
     static void SetMRV(double &mass, Vector &r, Vector &v, JPLFile &jf, Date d, JPL_planets p) {
     mass = jf.GetMass(p);  
     jf.GetEph(d,p,r,v);
     }
  */
  
  /* 
     static void SetMRV(double &mass, Vector &r, Vector &v, JPLFile * jf, const UniverseTypeAwareTime & t, JPL_planets p) {
     mass = jf->GetMass(p);  
     jf->GetEph(t,p,r,v);
     }
  */
  
  //! The Sun is automatically included
  /* 
     void SetupSolarSystem(Frame & frame, const list<JPL_planets> & l, const UniverseTypeAwareTime & t) {
     
     // fprintf(stderr,"sss: ls=%i t=%f\n",l.size(),t.GetTime());
     
     const Date epoch(t.GetDate());
     
     frame.clear();
     frame.SetDate(epoch);
     
     const string jpl_path = config->paths[JPL_EPHEM_FILE]->GetValue().c_str();
     JPLFile jf(jpl_path);
     
     string name;
     double mass;
     Vector r,v;
     
     // date checks
     
     if (t.GetTime() < jf.EphemStart().GetTime()) { 
     ORSA_WARNING("epoch requested is before ephem file start time! (%g < %g)",t.GetTime(),jf.EphemStart().GetTime());
     return; 
     }
     //
     if (t.GetTime() > jf.EphemEnd().GetTime()) {
     ORSA_WARNING("epoch requested is after ephem file end time! (%g > %g)",t.GetTime(),jf.EphemStart().GetTime());
     return;
     }
     
     // auto include the Sun
     name="Sun"; SetMRV(mass,r,v,jf,epoch,SUN); frame.push_back(Body(name,mass,r,v));
     
     JPL_planets pl;
     list<JPL_planets>::const_iterator it = l.begin();
     while (it != l.end()) {
     pl = *it;
     if (pl==SUN) {
     ++it;
     continue; // sun already included
     } else if (pl==EARTH_AND_MOON) {
     pl = EARTH; name = JPL_planet_name(pl); SetMRV(mass,r,v,jf,epoch,pl);  frame.push_back(Body(name,mass,r,v));
     pl = MOON;  name = JPL_planet_name(pl); SetMRV(mass,r,v,jf,epoch,pl);  frame.push_back(Body(name,mass,r,v));
     } else {
     name = JPL_planet_name(pl); SetMRV(mass,r,v,jf,epoch,pl); frame.push_back(Body(name,mass,r,v));
     }
     ++it;
     }    
     }
  */
  
  //! The Sun is automatically included
  /* 
     void SetupSolarSystem(Frame & frame, const list<JPL_planets> & l, const UniverseTypeAwareTime & t) {
     
     frame.clear();
     frame.SetTime(t);
     
     string name;
     double mass;
     Vector r,v;
     
     // date checks
     if (t < jpl_file->EphemStart()) { 
     ORSA_WARNING("epoch requested is before ephem file start time! (%g < %g)",t.GetTime(),jpl_file->EphemStart().GetTime());
     return; 
     }
     //
     if (t > jpl_file->EphemEnd()) {
     ORSA_WARNING("epoch requested is after ephem file end time! (%g > %g)",t.GetTime(),jpl_file->EphemStart().GetTime());
     return;
     }
     
     // auto include the Sun
     name="Sun"; SetMRV(mass,r,v,jpl_file,t,SUN); frame.push_back(Body(name,mass,r,v));
     
     JPL_planets pl;
     list<JPL_planets>::const_iterator it = l.begin();
     while (it != l.end()) {
     pl = *it;
     if (pl==SUN) {
     ++it;
     continue; // sun already included
     } else if (pl==EARTH_AND_MOON) {
     pl = EARTH; name = JPL_planet_name(pl); SetMRV(mass,r,v,jpl_file,t,pl);  frame.push_back(Body(name,mass,r,v));
     pl = MOON;  name = JPL_planet_name(pl); SetMRV(mass,r,v,jpl_file,t,pl);  frame.push_back(Body(name,mass,r,v));
     } else {
     name = JPL_planet_name(pl); SetMRV(mass,r,v,jpl_file,t,pl); frame.push_back(Body(name,mass,r,v));
     }
     ++it;
     }    
     }
  */
  //! The Sun is automatically included
  void SetupSolarSystem(Frame & frame, const list<JPL_planets> & l, const UniverseTypeAwareTime & t) {
    
    frame.clear();
    frame.SetTime(t);
    
    /* 
       string name;
       double mass;
       Vector r,v;
    */
    
    // date checks
    if (t < jpl_file->EphemStart()) { 
      ORSA_WARNING("epoch requested is before ephem file start time! (%g < %g)",t.GetTime(),jpl_file->EphemStart().GetTime());
      return; 
    }
    //
    if (t > jpl_file->EphemEnd()) {
      ORSA_WARNING("epoch requested is after ephem file end time! (%g > %g)",t.GetTime(),jpl_file->EphemStart().GetTime());
      return;
    }
    
    // auto include the Sun
    // name="Sun"; SetMRV(mass,r,v,jpl_file,t,SUN); frame.push_back(Body(name,mass,r,v));
    frame.push_back(jpl_cache->GetJPLBody(SUN,t));
    
    JPL_planets pl;
    list<JPL_planets>::const_iterator it = l.begin();
    while (it != l.end()) {
      pl = *it;
      if (pl==SUN) {
	++it;
	continue; // sun already included
      } else if (pl==EARTH_AND_MOON) {
	// pl = EARTH; name = JPL_planet_name(pl); SetMRV(mass,r,v,jpl_file,t,pl);  frame.push_back(Body(name,mass,r,v));
	frame.push_back(jpl_cache->GetJPLBody(EARTH,t));
	// pl = MOON;  name = JPL_planet_name(pl); SetMRV(mass,r,v,jpl_file,t,pl);  frame.push_back(Body(name,mass,r,v));
	frame.push_back(jpl_cache->GetJPLBody(MOON,t));
      } else {
	// name = JPL_planet_name(pl); SetMRV(mass,r,v,jpl_file,t,pl); frame.push_back(Body(name,mass,r,v));
	frame.push_back(jpl_cache->GetJPLBody(pl,t));
      }
      ++it;
    }    
  }
  
  ////
  
  /* 
     bool is_genuine_planet(const JPL_planets p) {
     bool is_genuine;
     switch(p) {
     case SOLAR_SYSTEM_BARYCENTER:
     case EARTH_MOON_BARYCENTER:
     case NUTATIONS:
     case LIBRATIONS:
     case EARTH_AND_MOON:
     is_genuine = false;
     break;
     default:
     is_genuine = true;
     break;
     }
     return (is_genuine);
     }
  */
  
  string JPL_planet_name(const JPL_planets p) {
    
    string name;
    
    switch(p) {
    case SUN:      name = "Sun";      break;
    case MERCURY:  name = "Mercury";  break;
    case VENUS:    name = "Venus";    break;
    case EARTH:    name = "Earth";    break;
    case MARS:     name = "Mars";     break;
    case JUPITER:  name = "Jupiter";  break;
    case SATURN:   name = "Saturn";   break;
    case URANUS:   name = "Uranus";   break;
    case NEPTUNE:  name = "Neptune";  break;
    case PLUTO:    name = "Pluto";    break;
      //
    case MOON:                   name = "Moon";                   break;
    case EARTH_MOON_BARYCENTER:  name = "Earth-Moon barycenter";  break;
      //
    default: break;
    }
    
    return (name);
  }
  
  // data from http://ssd.jpl.nasa.gov/phys_props_planets.html (Dec 6 2003)
  // Sun data: http://www.solarviews.com/eng/sun.htm
  double radius(const JPL_planets p) { 
    double r=0;
    switch(p) {
    case SUN:     r = FromUnits(695000.  ,KM); break;
    case MERCURY: r = FromUnits(  2440.  ,KM); break;
    case VENUS:   r = FromUnits(  6051.84,KM); break;
    case EARTH:   r = FromUnits(  6371.01,KM); break;
    case MARS:    r = FromUnits(  3389.92,KM); break;
    case JUPITER: r = FromUnits( 69911.  ,KM); break;
    case SATURN:  r = FromUnits( 58232.  ,KM); break;
    case URANUS:  r = FromUnits( 25362.  ,KM); break;
    case NEPTUNE: r = FromUnits( 24624.  ,KM); break;
    case PLUTO:   r = FromUnits(  1151.  ,KM); break;
      //
    case MOON:    r = FromUnits(  1738.  ,KM); break;
      //
    default: break;
    }
    return (r);
  }
  
  // JPLCache 
  
  JPLCache::JPLCache() {
    // jf = new JPLFile(config->paths[JPL_EPHEM_FILE]->GetValue().c_str());
  }
  
  JPLCache::~JPLCache() {
    // if (jf) delete jf;
  }
  
  const JPLBody & JPLCache::GetJPLBody(const JPL_planets p, const UniverseTypeAwareTime & t) {
    data_map_type & data = big_map[p];
    data_map_type::const_iterator it = data.find(t);
    if (it != data.end()) {
      // ORSA_ERROR("JPLCache::GetJPLBody(...) ==> Found something in cache...");
      return ((*it).second);
    } else {
      // ORSA_ERROR("JPLCache::GetJPLBody(...) ==> Adding object to cache...");
      // data[t] = JPLBody(p,t);
      // return data[t];
      return (data[t] = JPLBody(p,t));
    }
  }
  
  void JPLCache::Clear() {
    big_map.clear();
  }
  
} // namespace orsa
