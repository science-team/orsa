/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_interaction.h"
#include "orsa_secure_math.h"
#include "orsa_universe.h"

#include <iostream>
#include <list>
#include <stack>
#include <map>

using namespace std;

namespace orsa {
  
  class TreeNode {
  public:
    TreeNode() {
      reset();
    }
    
    void reset() {
      bool_node_mass_computed = false;
      bool_node_quadrupole_computed = false;
      bool_node_center_of_mass_computed = false;
      // parent = 0;
      depth  = 0;
    }
    
    bool inside_domain(const Vector p) const;
    
    bool is_leaf() const {
      return (child.empty() && (!b.empty()));
    };
    
    double node_mass() const;
    
    double * node_quadrupole() const;
    
    Vector node_center_of_mass() const;
    
    void BuildMesh(const bool root=false);
    
  public:
    void print() const;
    
  public:
    list<Body> b;
    
  public:	
    list<TreeNode> child;
    // TreeNode *parent;
    // cubic domains... *** o is included, o+l is included ***
    Vector o; // cube origin (minimum x,y,z)
    double l; // cube side
    unsigned int depth;
    //
  private:
    mutable double _node_mass;
    mutable bool bool_node_mass_computed;
    //
    mutable double _node_quadrupole[3][3];
    mutable bool bool_node_quadrupole_computed;
    //
    mutable Vector _node_center_of_mass;
    mutable bool bool_node_center_of_mass_computed;
  };
  
  double delta_function(const unsigned int i, const unsigned int j) {
    if (i==j) return 1.0;
    return 0.0;
  }
  
  Vector ComputeAcceleration(const list<Body>::const_iterator body_it, const list<TreeNode>::const_iterator node_domain_it, const bool compute_quadrupole=true) {
    
    Vector a;
    
    if (node_domain_it->node_mass()==0.0) return a;
    
    Vector d = node_domain_it->node_center_of_mass() - body_it->position();
    
    // monopole
    
    const double l2 = d.LengthSquared();
    
    if (d.IsZero()) {
      cerr << "*** Warning: two objects in the same position! (" << l2 << ")" << endl;
      // continue;
      return a;
    }
    
    a += d * secure_pow(l2,-1.5) * node_domain_it->node_mass();
    
    if (!compute_quadrupole) {
      return a;
    }
    
    // quadrupole
    
    double x[3];
    
    x[0] = d.x;
    x[1] = d.y;
    x[2] = d.z;
    
    double coefficient = 0.0;
    unsigned int i,j;
    double c_node_quadrupole[3][3];
    memcpy((void*)c_node_quadrupole, (const void*)node_domain_it->node_quadrupole(), 3*3*sizeof(double)); // works?
    for (i=0;i<3;++i) {
      for (j=0;j<3;++j) {
	coefficient += c_node_quadrupole[i][j]*x[i]*x[j];
      }
    }
    
    a += d * secure_pow(l2,-3.0) * coefficient;
    
    return a;
  }
  
  bool TreeNode::inside_domain(const Vector p) const {
    
    // printf("inside: (%g,%g,%g)      origin: (%g,%g,%g) l: %g\n",p.x,p.y,p.z,o.x,o.y,o.z,l);
    
    if (p.x < o.x) return false;
    if (p.y < o.y) return false;
    if (p.z < o.z) return false;
    //
    if (p.x > o.x+l) return false;
    if (p.y > o.y+l) return false;
    if (p.z > o.z+l) return false;
    //
    return true;
  }
  
  double TreeNode::node_mass() const { 
    if (bool_node_mass_computed) return _node_mass;
    _node_mass = 0.0;
    {
      list<TreeNode>::const_iterator c_it=child.begin();
      while (c_it!=child.end()) {
	_node_mass += c_it->node_mass();
	++c_it;
      }
    }
    {
      list<Body>::const_iterator b_it=b.begin();
      while (b_it!=b.end()) {
	_node_mass += b_it->mass();
	++b_it;
      }
    }
    bool_node_mass_computed = true;
    return _node_mass;
  }
  
  double * TreeNode::node_quadrupole() const { 
    if (bool_node_quadrupole_computed) return (&_node_quadrupole[0][0]);
    unsigned int i,j;
    for (i=0;i<3;++i) {
      for (j=0;j<3;++j) {
	_node_quadrupole[i][j] = 0.0;
      }
    }
    double x[3];
    double l_sq;
    double c_node_quadrupole[3][3];
    Vector vec;
    {
      list<TreeNode>::const_iterator c_it=child.begin();
      while (c_it!=child.end()) {
	vec = c_it->node_center_of_mass() - node_center_of_mass();
	//
	x[0] = vec.x;
	x[1] = vec.y;
	x[2] = vec.z;
	//
	l_sq = vec.LengthSquared();
	//
	memcpy((void*)c_node_quadrupole, (const void*)c_it->node_quadrupole(), 3*3*sizeof(double)); // works?
  	//
	for (i=0;i<3;++i) {
	  for (j=0;j<3;++j) {
	    _node_quadrupole[i][j] += c_it->node_mass()*(3.0*x[i]*x[j]-l_sq*delta_function(i,j)) + c_node_quadrupole[i][j];
	  }
	}
	++c_it;
      }
    }
    {
      list<Body>::const_iterator b_it=b.begin();
      while (b_it!=b.end()) {
	vec = b_it->position() - node_center_of_mass();
	//
	x[0] = vec.x;
	x[1] = vec.y;
	x[2] = vec.z;
	//
	l_sq = vec.LengthSquared();
	//
	for (i=0;i<3;++i) {
	  for (j=0;j<3;++j) {
	    _node_quadrupole[i][j] += b_it->mass()*(3.0*x[i]*x[j]-l_sq*delta_function(i,j));
	  }
	}
	++b_it;
      }
    }
    bool_node_mass_computed = true;
    return (&_node_quadrupole[0][0]);
  }
  
  Vector TreeNode::node_center_of_mass() const { 
    if (bool_node_center_of_mass_computed) return _node_center_of_mass;
    Vector vec_sum;
    double mass_sum=0;
    {
      list<TreeNode>::const_iterator c_it=child.begin();
      while (c_it!=child.end()) {
      	vec_sum  += c_it->node_mass()*c_it->node_center_of_mass();
	mass_sum += c_it->node_mass();
	++c_it;
      }
    }
    {
      list<Body>::const_iterator b_it=b.begin();
      while (b_it!=b.end()) {
	vec_sum  += b_it->mass()*b_it->position();
	mass_sum += b_it->mass();
	++b_it;
      }
    }
    _node_center_of_mass = vec_sum/mass_sum;
    bool_node_center_of_mass_computed = true;
    return _node_center_of_mass;
  }
  
  void TreeNode::BuildMesh(const bool root) {
    
    // zero bodies
    if (b.begin() == b.end()) return;
  
    // one body
    if (++b.begin() == b.end()) return;
    
    // compute domain for root
    if (root) { 
      depth = 0;
      
      Vector p; // (maximum x,y,z)
      o = p = b.begin()->position();
      Vector r;
      list<Body>::iterator b_it = b.begin();
      ++b_it;
      unsigned int total_bodies=1;
      while (b_it != b.end()) {
	r = b_it->position();
	
	if (r.x<o.x) o.x = r.x;
	if (r.y<o.y) o.y = r.y;
	if (r.z<o.z) o.z = r.z;
	
	if (r.x>p.x) p.x = r.x;
	if (r.y>p.y) p.y = r.y;
	if (r.z>p.z) p.z = r.z;
	
	++total_bodies;
	++b_it;
      }
      
      // printf("total bodies: %i\n",total_bodies);
      
      l = (p.x-o.x);
      if ((p.y-o.y)>l) l = (p.y-o.y);
      if ((p.z-o.z)>l) l = (p.z-o.z);
    }
    
    // a slightly bigger root domain (NEEDED!)...
    //  if (root) { // only for root?
    {
      const double d=0.01*l;
      //
      o.x -= d;
      o.y -= d;
      o.z -= d;
      //
      l += 2.0*d;
    }
    
    // check
    {
      list<Body>::iterator b_it = b.begin();
      while (b_it != b.end()) {
	if (inside_domain(b_it->position())) {
	  
	} else {
	  printf("WARNING! One body outside domain...\n");
	}
	++b_it;
      }
    }
    
    // clear child list
    child.clear();
    
    // build eight nodes
    {
      TreeNode n;
      n.l = l/2.0;
      n.depth = depth+1;
      // n.parent = this;
      //
      n.o.Set(o.x    ,o.y    ,o.z    ); child.push_back(n);
      n.o.Set(o.x    ,o.y    ,o.z+n.l); child.push_back(n);
      n.o.Set(o.x    ,o.y+n.l,o.z    ); child.push_back(n);
      n.o.Set(o.x    ,o.y+n.l,o.z+n.l); child.push_back(n);
      n.o.Set(o.x+n.l,o.y    ,o.z    ); child.push_back(n);
      n.o.Set(o.x+n.l,o.y    ,o.z+n.l); child.push_back(n);
      n.o.Set(o.x+n.l,o.y+n.l,o.z    ); child.push_back(n);
      n.o.Set(o.x+n.l,o.y+n.l,o.z+n.l); child.push_back(n);
    }
    
    // fill new nodes with bodies...
    {
      list<TreeNode>::iterator c_it;
      list<Body>::iterator b_it = b.begin();
      while (b_it != b.end()) {
	/* 
	   printf("body position: (%g,%g,%g)\n",
	   b_it->position().x,
	   b_it->position().y,
	   b_it->position().z);
	*/
	
	c_it = child.begin();
	while (c_it != child.end()) {
	  if (c_it->inside_domain(b_it->position())) {
	    // printf("--- inside domain!!! ---\n");
	    c_it->b.push_back(*b_it);
	    
	    b.erase(b_it);
	    b_it = b.begin(); 
	    if (b_it == b.end()) break;
	    
	    c_it = child.begin();
	    --c_it;
	  }	  
	  ++c_it;
	}
	++b_it;
      }
    }
    
    // remove empty childs
    {
      list<TreeNode>::iterator c_it = child.begin();
      while (c_it != child.end()) {
	if (c_it->b.empty()) {
	  c_it = child.erase(c_it);
	} else {
	  ++c_it;
	}
      } 
    }
    
    // refine mesh
    {
      list<TreeNode>::iterator c_it = child.begin();
      while (c_it != child.end()) {
	c_it->BuildMesh();
	++c_it;
      }
    }
  }
  
  void TreeNode::print() const {
    unsigned int bodies=0;
    list<Body>::const_iterator b_it = b.begin();
    while (b_it != b.end()) {
      ++bodies;
      ++b_it;
    }
    
    unsigned int childs=child.size(); // Note: this *may* be O(N)
    
    printf("node --- depth: %i   childs: %i   mass: %g   cube side: %g   origin: (%g,%g,%g)   bodies: %i\n",depth,childs,node_mass(),l,o.x,o.y,o.z,bodies);
    
    list<TreeNode>::const_iterator it=child.begin();
    while (it!=child.end()) {
      it->print();
      ++it;
    }
  }
  
  GravitationalTree::GravitationalTree() : Interaction() {
    g = GetG();
    theta = 0.7;
  }
  
  GravitationalTree::GravitationalTree(const GravitationalTree &) : Interaction() {
    g = GetG();
    theta = 0.7;
  }
  
  Interaction * GravitationalTree::clone() const {
    return new GravitationalTree(*this);
  }
  
  void GravitationalTree::Acceleration(const Frame &f, vector<Vector> &a) {
    
    if (f.size() < 2) return;
    
    a.resize(f.size());
    
    {
      unsigned int k=0;
      while (k<a.size()) {    
	a[k].Set(0.0,0.0,0.0);
	++k;
      }
    }
    
    map <unsigned int, unsigned int> frame_map;
    {
      unsigned int k=0;
      while (k<f.size()) {
	frame_map[f[k].BodyId()] = k;
	++k;
      }
    }
    
    TreeNode root_node;
    
    unsigned int k=0;
    while (k<f.size()) {
      root_node.b.push_back(f[k]);
      ++k;
    }
    
    root_node.BuildMesh(true);
    
    // root_node.print();
    
    list<TreeNode>::const_iterator node_body_it, node_domain_it;
    list<Body>::const_iterator body_it;
    
    stack<list<TreeNode>::const_iterator> stk_body, stk_domain;
    
    double angle;
    
    unsigned int num_direct=0, num_domain=0;
    
    node_body_it = root_node.child.begin();
    while (node_body_it != root_node.child.end()) {
      
      if (node_body_it->is_leaf()) {
	
	body_it = node_body_it->b.begin();
        while (body_it != node_body_it->b.end()) {
	  
	  node_domain_it = root_node.child.begin();
	  while (node_domain_it != root_node.child.end()) {
	    
	    angle = (node_domain_it->l)/(node_domain_it->node_center_of_mass()-body_it->position()).Length();
	    
	    if (angle < theta) {
	      
	      ++num_domain;
	      // cerr << "num_domain: " << num_domain << "  num_direct: " << num_direct << "  ratio: " << (1.0*num_domain)/(1.0*num_direct) << endl;
	      
	      a[frame_map[body_it->BodyId()]] += ComputeAcceleration(body_it,node_domain_it);
	      
	      ++node_domain_it;
	      
	    } else if (node_domain_it->is_leaf()) {
	      
	      if (body_it->BodyId() != node_domain_it->b.begin()->BodyId()) {
		a[frame_map[body_it->BodyId()]] += ComputeAcceleration(body_it,node_domain_it);
		++num_direct;
		// cerr << "num_domain: " << num_domain << "  num_direct: " << num_direct << "  ratio: " << (1.0*num_domain)/(1.0*num_direct) << endl;
	      }
	      
	      ++node_domain_it;
	      
	    } else {
	      
	      stk_domain.push(node_domain_it);
	      node_domain_it = node_domain_it->child.begin();
	      
	    }
	    
	    while (stk_domain.size()) {
	      if (node_domain_it == stk_domain.top()->child.end()) {
		node_domain_it = stk_domain.top();
		++node_domain_it;
		stk_domain.pop();
	      } else {
		break;
	      }
	    }
	    
	  }
	  
	  ++body_it;
	}
	
	++node_body_it;
	
      } else { // not leaf
	
	stk_body.push(node_body_it);
	node_body_it = node_body_it->child.begin();
	
      }
      
      while (stk_body.size()) {
	if (node_body_it == stk_body.top()->child.end()) {
	  node_body_it = stk_body.top();
	  ++node_body_it;
	  stk_body.pop();
	} else {
	  break;
	}
      }
      
    }
    
    {
      unsigned int k=0;
      while (k<a.size()) {    
	a[k] *= g;
	++k;
      }
    }
    
  }
  
  double GravitationalTree::PotentialEnergy(const Frame&) {
    // to be done...
    return 0.0;
  }
  
} // namespace orsa
