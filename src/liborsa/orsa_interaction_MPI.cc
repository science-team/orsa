/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_interaction.h"
#include "orsa_secure_math.h"
#include "orsa_universe.h"

#include <cmath>
#include <iostream>

#include <mpi.h>

using namespace std;

namespace orsa {
  
  // Newton_MPI
  
  Newton_MPI::Newton_MPI() : Interaction() {
    av = av_local = bv = 0;
  }
  
  Newton_MPI::Newton_MPI(const Newton_MPI &) : Interaction() {
    av = av_local = bv = 0;
  }
  
  Newton_MPI::~Newton_MPI() {
    free(bv);
    free(av);
    free(av_local);
  }
  
  Interaction * Newton_MPI::clone() const {
    return new Newton_MPI(*this);
  }
  
  void Newton_MPI::Acceleration(const Frame &fr, vector<Vector> &a) {
    
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank );
    MPI_Comm_size(MPI_COMM_WORLD, &size );
    
    // fprintf(stderr,"inside Newton_MPI::Acceleration() ---> rank = %i\n",rank);
    
    unsigned int i,j;
    
    Vector d;
    
    double ls;
    
    // sync
    MPI_Barrier(MPI_COMM_WORLD); 
    
    unsigned int frame_size=0;
    if (rank==0) {
      frame_size = fr.size();
    }
    //
    MPI_Bcast(&frame_size, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    if (rank==0) {
      g = GetG();
    }
    //
    MPI_Bcast(&g, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    // cerr << "rank: " << rank << "  frame size: " << frame_size << endl;
    
    if (frame_size < 2) return;
    
    a.resize(frame_size);
    for (i=0;i<frame_size;++i)
      a[i].Set(0.0,0.0,0.0);
    
    // debug
    /* {
       cerr << "a reset:" << endl;
       unsigned int k=0;
       while (k<frame_size) {
       fprintf(stderr,"a[%i].Length() = %g\n",k,a[k].Length());
       ++k;
       }
       }
    */
    
    // 4 = mass (1) + position (3)
    bv = (double *)realloc(bv,4*frame_size*sizeof(double)); 
    
    if (rank==0) {
      unsigned int k=0;
      while (k<frame_size) {
	bv[4*k]   = fr[k].mass();
	//
	bv[4*k+1] = fr[k].position().x;
	bv[4*k+2] = fr[k].position().y;
	bv[4*k+3] = fr[k].position().z;
	//
	++k;
      }
    }
    
    // debug
    /* {
       cerr << "initialized bv:" << endl;
       unsigned int k=0;
       while (k<4*frame_size) {
       fprintf(stderr,"bv[%i] = %g\n",k,bv[k]);
       ++k;
       }
       }
    */
    
    // cerr << "before Bcast..." << endl;
    
    MPI_Bcast(bv, 4*frame_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    // cerr << "after Bcast..." << endl;
    
    // debug
    /* {
       cerr << "bv after Bcast:" << endl;
       unsigned int k=0;
       while (k<4*frame_size) {
       fprintf(stderr,"bv[%i] = %g\n",k,bv[k]);
       ++k;
       }
       }
    */
    
    // print(ff);
    
    for (i=1+rank;i<frame_size;i+=size) {
      
      for (j=0;j<i;++j) {
	
	// fprintf(stderr,"rank: %i   computing %i <--> %i\n",rank,i,j); 
	
	// if ((ff[i].mass()==0) && (ff[j].mass()==0)) continue;
	if ((bv[4*i]==0.0) && (bv[4*j]==0.0)) continue;
	
      	// d = ff[i].DistanceVector(ff[j]);
	d.Set(bv[4*j+1]-bv[4*i+1],
	      bv[4*j+2]-bv[4*i+2],
	      bv[4*j+3]-bv[4*i+3]);
	
	// fprintf(stderr,"rank: %i   computing %i <--> %i  d.Length(): %g\n",rank,i,j,d.Length()); 
	
	ls = d.LengthSquared();
	
	// fprintf(stderr,"rank: %i   computing %i <--> %i  ls: %g\n",rank,i,j,ls); 
	
	if (d.IsZero()) {
	  cerr << "*** Warning: two objects in the same position!" << endl;
          continue;
        }
	
	d *= secure_pow(ls,-1.5);
        
	// fprintf(stderr,"rank: %i   computing %i <--> %i  modified-d.Length(): %g\n",rank,i,j,d.Length()); 
	
	// fprintf(stderr,"rank: %i   computing %i <--> %i  pre-change a[%i].Length(): %g   a[%i].Length(): %g\n",rank,i,j,i,a[i].Length(),j,a[j].Length()); 
	
	// a[i] += d * ff[j].mass();
	// a[j] -= d * ff[i].mass();
      	// a[i] += d * bv[4*j];
	// a[j] -= d * bv[4*i];
	a[i] = a[i] + d * bv[4*j];
	a[j] = a[j] - d * bv[4*i];
	
	// fprintf(stderr,"rank: %i   computing %i <--> %i  end of cycle a[%i].Length(): %g   a[%i].Length(): %g\n",rank,i,j,i,a[i].Length(),j,a[j].Length()); 
      }  
    } 
    
    // cerr << "rank " << rank << "    out of the loop..." << endl;
    
    // double *av = (double *)malloc(3*frame_size*sizeof(double));
    av = (double *)realloc(av,3*frame_size*sizeof(double));
    
    if (rank == 0) {
      unsigned int k=0;
      while (k<3*frame_size) {
	av[k] = 0.0;
	++k;
      }
    }
    
    // double *av_local = (double *)malloc(3*frame_size*sizeof(double));
    av_local = (double *)realloc(av_local,3*frame_size*sizeof(double));
    
    {
      unsigned int k=0;
      while (k<frame_size) {
	av_local[3*k]   = a[k].x;
	av_local[3*k+1] = a[k].y;
	av_local[3*k+2] = a[k].z;
	++k;
      }
    }
    
    // debug   
    /* {
       unsigned int k=0;
       while (k<3*frame_size) {
       fprintf(stderr,"rank: %i  av_local[%i]: %g\n",rank,k,av_local[k]);
       ++k;
       }
       }
    */
    
    MPI_Reduce(av_local, av, 3*frame_size, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    
    if (rank == 0) {
      unsigned int k=0;
      while (k<frame_size) {
	a[k].x = av[3*k];
	a[k].y = av[3*k+1];
	a[k].z = av[3*k+2];	
	//
	a[k] *= g;
	//
	// fprintf(stderr,"acc[%i].Length() = %g\n",k,a[k].Length());
    	//
	++k;
      }
    }
  }
  
  double Newton_MPI::PotentialEnergy(const Frame &f) {
    Newton newton;
    return (newton.PotentialEnergy(f));
  }
  
} // namespace orsa
