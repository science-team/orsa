/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _ORSA_CONFIG_H_
#define _ORSA_CONFIG_H_

#include <string>
#include <map>

namespace orsa {
  
  class OrsaConfigFile;
  
  template <class T> class ConfigItem {
  public:
    ConfigItem(const std::string t) : tag(t) { };
    ConfigItem(const T v, const std::string t) : value(v), tag(t) { };
    
  public:
    T    GetValue() const    { return value; }
    void SetValue(const T v) { value = v; }
    
  private:
    T value;
    
  public:
    const std::string tag;
  };
  
  enum ConfigEnum {
    JPL_EPHEM_FILE,
    JPL_DASTCOM_NUM,
    JPL_DASTCOM_UNNUM,
    JPL_DASTCOM_COMET,
    LOWELL_ASTORB,
    MPC_MPCORB,
    MPC_COMET,
    MPC_NEA,
    MPC_DAILY,
    MPC_DISTANT,
    MPC_PHA,
    MPC_UNUSUALS,
    ASTDYS_ALLNUM_CAT,
    ASTDYS_ALLNUM_CTC,
    ASTDYS_ALLNUM_CTM,
    ASTDYS_UFITOBS_CAT,
    ASTDYS_UFITOBS_CTC,
    ASTDYS_UFITOBS_CTM,
    NEODYS_CAT,
    NEODYS_CTC,
    OBSCODE,
    // TLE files
    TLE_NASA,
    TLE_GEO,
    TLE_GPS,
    TLE_ISS,
    TLE_KEPELE,
    TLE_VISUAL,
    TLE_WEATHER,
    // textures
    TEXTURE_SUN,
    TEXTURE_MERCURY,
    TEXTURE_VENUS,
    TEXTURE_EARTH,
    TEXTURE_MOON,
    TEXTURE_MARS,
    TEXTURE_JUPITER,
    TEXTURE_SATURN,
    TEXTURE_URANUS,
    TEXTURE_NEPTUNE,
    TEXTURE_PLUTO,
    // 
    NO_CONFIG_ENUM
  };
  
  std::string Label(const ConfigEnum);
  
  class Config {
  public:
    Config();
    
  public:
    std::map < ConfigEnum, ConfigItem<std::string>* > paths;
    
  public:
    static void read_from_file();
    static void write_to_file();
  };
  
  //! The active configuration
  extern Config * config;
  
} // namespace orsa

#endif // _ORSA_CONFIG_H_
