/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_file.h"

#include <iostream>

using namespace std;

namespace orsa {
  
  // Mercury5IntegrationFile
  
  Mercury5IntegrationFile::Mercury5IntegrationFile(OrbitStream &osin) {
    os = &osin;
    cols = C10;
    status = CLOSE;
  }
  
  Mercury5IntegrationFile::Mercury5IntegrationFile(OrbitStream &osin, M5COLS c_in) {
    os   = &osin;
    cols = c_in; 
    status = CLOSE;
  }
  
  void Mercury5IntegrationFile::Read() {
    
    // if (file == 0) Open();
    if (status == CLOSE) Open();
    
    if (status != OPEN_R){ 
      cerr << "problems encountered when opening file.\n" << endl;
    }
    
    os->resize(0);
    os->timestep = 0.0;
    OrbitWithEpoch fo;
    REWIND_FILE(file); 
    
    // read the first 4 lines containing the header
    int l;
    char line[1024],label[1024];
    for (l=0;l<4;l++) {
      GETS_FILE(line,1024,file);
      if (l==1) { // second line
	sscanf(line,"%s",label);
	os->label = label;
	// cerr << "LABEL: [" << os->label << "]" << endl;
      }
    }
    
    double a,e,i,omega_per,omega_nod,M;
    double time,time_old=0,timestep;
    // start reading
    
    if (cols == C7) {
      while (GETS_FILE(line,1024,file) != 0) {
	
	/* while ( (fscanf(file,"%lf %lf %lf %lf %lf %lf %lf",
	   &time,&a,&e,&i,&omega_per,&omega_nod,&M)) != EOF ) {
	*/
	
	sscanf(line,"%lf %lf %lf %lf %lf %lf %lf",
	       &time,&a,&e,&i,&omega_per,&omega_nod,&M);
	
	timestep  = time - time_old;
	time_old  = time;
	if (os->size() == 2) { 
	  os->timestep = FromUnits(timestep,DAY); // read in days, save in the current time units
	  cerr << "timestep set to: " << os->timestep << endl;
	}
	
	fo.epoch.SetTime(FromUnits(time,DAY)); // read in days, save in the current time units
	
	fo.a                = FromUnits(a,AU);
	fo.e                = e;
	fo.i                = (pi/180.0)*i;
	fo.omega_node       = (pi/180.0)*omega_nod;
	fo.omega_pericenter = (pi/180.0)*omega_per;
	fo.M                = (pi/180.0)*M;
	
	os->push_back(fo);
      }
    }
    
    if (cols == C10) {
      double dummy;
      while (GETS_FILE(line,1024,file) != 0) {
	/* while ( (fscanf(file,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
	   &time,&a,&e,&i,&omega_per,&omega_nod,&M,&dummy,&dummy,&dummy))!= EOF ) {
	*/

	sscanf(line,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
	       &time,&a,&e,&i,&omega_per,&omega_nod,&M,&dummy,&dummy,&dummy);
	
	timestep  = time - time_old;
	time_old  = time;
	if (os->size() == 2) { 
	  os->timestep = FromUnits(timestep,DAY); // read in days, save in the current time units
	  cerr << "timestep set to: " << os->timestep << endl;
	}
	
	fo.epoch.SetTime(FromUnits(time,DAY)); // read in days, save in the current time units
	
	fo.a                = FromUnits(a,AU);
	fo.e                = e;
	fo.i                = (pi/180.0)*i;
	fo.omega_node       = (pi/180.0)*omega_nod;
	fo.omega_pericenter = (pi/180.0)*omega_per;
	fo.M                = (pi/180.0)*M;
	
	os->push_back(fo);
      }
    }  
  }
  
} // namespace orsa
