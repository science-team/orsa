/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_coord.h"

#include <cmath>
#include <iostream>

using namespace std;

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

namespace orsa {
  
  // rotation
  // ANGLES IN RADIANS !!!
  /* 
     Vector& Vector::rotate (const double omega_per, const double i, const double omega_nod) {      
     
     double local_x, local_y, local_z;
     
     // from Mathematica..
     local_x = cos(omega_nod)*(x*cos(omega_per) - y*sin(omega_per)) + sin(omega_nod)*(z*sin(i) - cos(i)*(y*cos(omega_per) + x*sin(omega_per)));
     local_y = -(z*cos(omega_nod)*sin(i)) + cos(i)*cos(omega_nod)*(y*cos(omega_per) + x*sin(omega_per)) + sin(omega_nod) * (x*cos(omega_per) - y*sin(omega_per));
     local_z = z*cos(i) + sin(i)*(y*cos(omega_per) + x*sin(omega_per));
     
     x = local_x; y = local_y; z = local_z;
     
     return *this;
     }
  */
  
  Vector & Vector::rotate (const double omega_per, const double i, const double omega_nod) {      
    
#ifdef HAVE_SINCOS
    double s,c;
    //
    sincos(i,&s,&c);
    const double s_i = s;
    const double c_i = c;
    //
    sincos(omega_nod,&s,&c);
    const double s_on = s;
    const double c_on = c;
    //
    sincos(omega_per,&s,&c);
    const double s_op = s;
    const double c_op = c;
#else // HAVE_SINCOS
    const double s_i = sin(i);
    const double c_i = cos(i);
    //
    const double s_on = sin(omega_nod);
    const double c_on = cos(omega_nod);
    //
    const double s_op = sin(omega_per);
    const double c_op = cos(omega_per);
#endif // HAVE_SINCOS
    
    // from Mathematica..
    /* 
       const double new_x = cos(omega_nod)*(x*cos(omega_per) - y*sin(omega_per)) + sin(omega_nod)*(z*sin(i) - cos(i)*(y*cos(omega_per) + x*sin(omega_per)));
       const double new_y = -(z*cos(omega_nod)*sin(i)) + cos(i)*cos(omega_nod)*(y*cos(omega_per) + x*sin(omega_per)) + sin(omega_nod) * (x*cos(omega_per) - y*sin(omega_per));
       const double new_z = z*cos(i) + sin(i)*(y*cos(omega_per) + x*sin(omega_per));
    */
    
    const double new_x = c_on*(x*c_op - y*s_op) + s_on*(z*s_i - c_i*(y*c_op + x*s_op));
    const double new_y = -(z*c_on*s_i) + c_i*c_on*(y*c_op + x*s_op) + s_on*(x*c_op - y*s_op);
    const double new_z = z*c_i + s_i*(y*c_op + x*s_op);
    
    x = new_x; 
    y = new_y;
    z = new_z;
    
    return * this;
  }
  
  void Interpolate(const vector<VectorWithParameter> vx_in, const double x, Vector &v_out, Vector &err_v_out) {
    
    // cerr << "Interpolate() call..." << endl;
    
    unsigned int i,j,m;
    
    unsigned int i_closest;
    
    double      diff,denom;
    double      ho,hp;
    double      tmp_double;

    unsigned int n_points = vx_in.size();
    
    /* cerr << " -----> n_points: " << n_points << endl;
       for (j=0;j<n_points;j++)
       std::cerr << "vx_in[" << j << "].par = " <<  vx_in[j].par << "\n";
    */
    
    if (n_points < 2) {
      cerr << "too few points..." << endl;
      for (j=0;j<n_points;j++) {
        v_out = vx_in[0];
        err_v_out.Set(0,0,0);
      }
      return;
    }
    
    Vector * c = new Vector[n_points];
    Vector * d = new Vector[n_points];
    
    Vector w;
    
    diff = fabs(x-vx_in[0].par);
    i_closest = 0;
    
    for (j=0;j<n_points;j++) { 
      
      tmp_double = fabs(x-vx_in[j].par);
      if (tmp_double <  diff) {
        diff = tmp_double;
        i_closest = j;
      }
      
      c[j] = vx_in[j];
      d[j] = vx_in[j]; 
    }
    
    v_out     = vx_in[i_closest];
    err_v_out = vx_in[i_closest];
    
    // cerr << "local..  v: " << Length(v_out) << "  i_closest: " << i_closest << endl;
    
    i_closest--;
    
    for (m=1;m<=(n_points-2);m++) {
      for (i=0;i<(n_points-m);i++) {
        ho = vx_in[i].par   - x;
        hp = vx_in[i+m].par - x;

        denom = ho - hp;

        if (denom == 0.0) {
          cerr << "interpolate() --> Error: divide by zero\n";
          cerr << "i: " << i << "   m: " << m << endl;
          for (j=0;j<n_points;j++)
            cerr << "vx_in[" << j << "].par = " <<  vx_in[j].par << "\n";
          exit(0);
        }
        
        w = c[i+1] - d[i];

        d[i] = hp*w/denom;
        c[i] = ho*w/denom;

        // cerr << " ++++++++++  d[" << i << "]: " << Length(d[i]) << endl;
        // cerr << " ++++++++++  c[" << i << "]: " << Length(c[i]) << endl;
      }
      
      if ( (2*i_closest) < (n_points-m) ) {
        err_v_out = c[i_closest+1]; 
      } else {
        err_v_out = d[i_closest];
        i_closest--;
      }
      
      v_out += err_v_out;    
    }
    
    delete [] c;
    delete [] d;
  }
  
} // namespace orsa
