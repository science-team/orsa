/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_units.h"

#include "sdncal.h"

#include "orsa_common.h"
#include "orsa_universe.h"
#include "orsa_file.h"

#include <vector>
#include <string>
#include <iostream>

#include <time.h>

#include "support.h"

using namespace std;

namespace orsa {
  
  const double        G_MKS = 6.67259e-11;
  const double     MSUN_MKS = 1.9889e30;
  const double MJUPITER_MKS = 1.8989e27;
  const double   MEARTH_MKS = 5.9742e24;
  const double    MMOON_MKS = 7.3483e22;
  const double       AU_MKS = 1.49597870660e11;
  const double        c_MKS = 299792458.0;
  const double  R_EARTH_MKS = 6378140.0;
  const double   R_MOON_MKS = 1737400.0;
  
  double Units::GetG_MKS() const { return G_MKS; }
  
  Units::Units() {
    init_base();
    Time.Set(SECOND);
    Length.Set(M);
    Mass.Set(KG);
    TryToSetUnitsFromJPLFile();
    Recompute();
  };
  
  Units::Units(time_unit tu, length_unit lu, mass_unit mu) {
    init_base();
    Time.Set(tu);
    Length.Set(lu);
    Mass.Set(mu);
    TryToSetUnitsFromJPLFile();
    Recompute();
  }
  
  void Units::init_base() {
    G_base        =        G_MKS;
    MSun_base     =     MSUN_MKS;
    MJupiter_base = MJUPITER_MKS;
    MEarth_base   =   MEARTH_MKS;
    MMoon_base    =    MMOON_MKS;
    AU_base       =       AU_MKS;
    c_base        =        c_MKS;
    r_earth_base  =  R_EARTH_MKS;
    r_moon_base   =   R_MOON_MKS;
  }
  
  void Units::SetSystem(time_unit tu, length_unit lu, mass_unit mu) {
    Time.Set(tu);
    Length.Set(lu);
    Mass.Set(mu);
    Recompute();
  }
  
  void Units::TryToSetUnitsFromJPLFile() {
    
    /* 
       Config conf;
       OrsaConfigFile ocf(&conf);
       ocf.Read();
       ocf.Close();
    */
    if (config->paths[JPL_EPHEM_FILE]->GetValue() != "") {
      
      // cerr << "using JPL constants for units conversions..." << endl;
      
      JPLFile jf(config->paths[JPL_EPHEM_FILE]->GetValue().c_str());
      
      AU_base       = jf.GetAU_MKS();
      MSun_base     = jf.GetMSun_MKS();
      MJupiter_base = jf.GetMJupiter_MKS();
      MEarth_base   = jf.GetMEarth_MKS();
      MMoon_base    = jf.GetMMoon_MKS();
      c_base        = jf.GetC_MKS();
      r_earth_base  = jf.GetREarth_MKS();
      r_moon_base   = jf.GetRMoon_MKS();
      
    }
    
    Recompute();
  }
  
  double Units::GetTimeScale(const time_unit tu) const {
    if (tu == YEAR)   return (31557600.0);
    if (tu == DAY)    return (86400.0);
    if (tu == HOUR)   return (3600.0);
    if (tu == MINUTE) return (60.0);
    if (tu == SECOND) return (1.0);
    
    // here should throw an error...
    return 1.0;
  }
  
  string Units::label(const time_unit tu) const {
    if (tu == YEAR)   return "y";
    if (tu == DAY)    return "d";
    if (tu == HOUR)   return "h";
    if (tu == MINUTE) return "m";
    if (tu == SECOND) return "s";
    return "";
  }
  
  double Units::GetLengthScale(const length_unit lu) const {
    
    double ls=1.0;
    
    switch(lu) {
    case   MPARSEC: ls = (parsec_base*1.0e6); break;
    case   KPARSEC: ls = (parsec_base*1.0e3); break;
    case    PARSEC: ls = (parsec_base);       break;
    case        LY: ls = (c_base*31557600.0); break;
    case        AU: ls = (AU_base);           break;
    case EARTHMOON: ls = (3.844e8);           break;
    case    REARTH: ls = (r_earth_base);      break;
    case     RMOON: ls = (r_moon_base);       break;
    case        KM: ls = (1000.0);            break;
    case         M: ls = (1.0);               break;
    case        CM: ls = (0.01);              break;
    }
    
    return (ls);
  }
  
  string Units::label(const length_unit lu) const {
    if (lu == MPARSEC)   return "Mpc";
    if (lu == KPARSEC)   return "kpc";
    if (lu == PARSEC)    return "pc";
    if (lu == LY)        return "ly";
    if (lu == AU)        return "AU";
    if (lu == EARTHMOON) return "LD"; // earth-moon mean distance
    if (lu == REARTH)    return "ER";
    if (lu == RMOON)     return "MR";
    if (lu == KM)        return "km";
    if (lu ==  M)        return "m";
    if (lu == CM)        return "cm";
    return "";
  }
  
  double Units::GetMassScale(const mass_unit mu) const {
    
    double ms=1.0;
    
    switch(mu) {
    case     MSUN: ms = (MSun_base);     break;
    case MJUPITER: ms = (MJupiter_base); break;
    case   MEARTH: ms = (MEarth_base);   break;
    case    MMOON: ms = (MMoon_base);    break;
    case       KG: ms = (1.0);           break;
    case     GRAM: ms = (0.001);         break;
    }
    
    return (ms);
  }
      
  string Units::label(const mass_unit mu) const {
    if (mu == MSUN)     return "Sun mass";
    if (mu == MJUPITER) return "Jupiter mass";
    if (mu == MEARTH)   return "Earth mass";
    if (mu == MMOON)    return "Moon mass";
    if (mu == KG)       return "kg";
    if (mu == GRAM)     return "g";
    return "";
  }
  
  double Units::GetTimeScale()   const { return Units::GetTimeScale(     Time.GetBaseUnit()); } 
  double Units::GetLengthScale() const { return Units::GetLengthScale( Length.GetBaseUnit()); } 
  double Units::GetMassScale()   const { return Units::GetMassScale(     Mass.GetBaseUnit()); } 
  
  void Units::Recompute() {
    G = G_base*secure_pow(GetTimeScale(),2.0)*secure_pow(GetLengthScale(),-3.0)*secure_pow(GetMassScale(),1.0); 
    MSun = MSun_base/GetMassScale();
    c = c_base*GetTimeScale()/GetLengthScale();
    parsec_base = AU_base/(2*sin((pi/180)/3600.0/2));
  }
  
  // Date related tables
  
  // const TimeScale default_Date_timescale = TDT; // ET
  TimeScale default_Date_timescale = TT; // ET, updated by the Universe costructor
  
  struct TAI_minus_UTC_element {
    int day,month,year;
    int TAI_minus_UTC;
  };
  
  inline bool operator == (const TAI_minus_UTC_element &x, const TAI_minus_UTC_element &y) {
    if (x.day   != y.day)   return false;
    if (x.month != y.month) return false;
    if (x.year  != y.year)  return false;
    if (x.TAI_minus_UTC != y.TAI_minus_UTC) return false;
    return true;
  }
  
  inline bool operator != (const TAI_minus_UTC_element &x, const TAI_minus_UTC_element &y) {
    return (!(x==y));
  }
  
  /*
   *  The TAI_minus_UTC_table should be updated periodically, 
   * for instance by consulting:
   * 
   * http://hpiers.obspm.fr/webiers/general/communic/publi/PUBLI.html
   * 
   * The present version is updated through Bulletin C17 (Jan 28, 1999)
   * Day, month, year, TAI-UTC (s)
   */
  
  const TAI_minus_UTC_element TAI_minus_UTC_table_final_element = {0,0,0,0};
  const TAI_minus_UTC_element TAI_minus_UTC_table[] = {
    {1,1,1972,10},
    {1,7,1972,11},
    {1,1,1973,12},
    {1,1,1974,13},
    {1,1,1975,14},
    {1,1,1976,15},
    {1,1,1977,16},
    {1,1,1978,17},
    {1,1,1979,18},
    {1,1,1980,19},
    {1,7,1981,20},
    {1,7,1982,21},
    {1,7,1983,22},
    {1,7,1985,23},
    {1,1,1988,24},
    {1,1,1990,25},
    {1,1,1991,26},
    {1,7,1992,27},
    {1,7,1993,28},
    {1,7,1994,29},
    {1,1,1996,30},
    {1,7,1997,31},
    {1,1,1999,32}, // latest leap second, updated Jan 2003
    {0,0,0,0} // TAI_minus_UTC_table_final_element
  };
  
  struct ET_minus_UT_element {
    int day,month,year;
    double ET_minus_UT;
  };
  
  inline bool operator == (const ET_minus_UT_element &x, const ET_minus_UT_element &y) {
    if (x.day   != y.day)   return false;
    if (x.month != y.month) return false;
    if (x.year  != y.year)  return false;
    if (x.ET_minus_UT != y.ET_minus_UT) return false;
    return true;
  }
  
  inline bool operator != (const ET_minus_UT_element &x, const ET_minus_UT_element &y) {
    return (!(x==y));
  }
  
  /* 
   * Values ET - UT at 0h UT of the date (specified as day, month, year)
   * from The Astronomical Almanac 1999, pages K8-9
   */
  
  const ET_minus_UT_element ET_minus_UT_table_final_element = {0,0,0,0};
  const ET_minus_UT_element ET_minus_UT_table[] = {
    {1,1,1800,13.7},
    {1,1,1801,13.4},
    {1,1,1802,13.1},
    {1,1,1803,12.9},
    {1,1,1804,12.7},
    {1,1,1805,12.6},
    {1,1,1806,12.5},
    {1,1,1816,12.5},
    {1,1,1817,12.4},
    {1,1,1818,12.3},
    {1,1,1819,12.2},
    {1,1,1820,12.0},
    {1,1,1821,11.7},
    {1,1,1822,11.4},
    {1,1,1823,11.1},
    {1,1,1824,10.6},
    {1,1,1825,10.2},
    {1,1,1826, 9.6},
    {1,1,1827, 9.1},
    {1,1,1828, 8.6},
    {1,1,1829, 8.0},
    {1,1,1830, 7.5},
    {1,1,1831, 7.0},
    {1,1,1832, 6.6},
    {1,1,1833, 6.3},
    {1,1,1834, 6.0},
    {1,1,1835, 5.8},
    {1,1,1836, 5.7},
    {1,1,1837, 5.6},
    {1,1,1838, 5.6},
    {1,1,1839, 5.6},
    {1,1,1840, 5.7},
    {1,1,1841, 5.8},
    {1,1,1842, 5.9},
    {1,1,1843, 6.1},
    {1,1,1844, 6.2},
    {1,1,1845, 6.3},
    {1,1,1846, 6.5},
    {1,1,1847, 6.6},
    {1,1,1848, 6.8},
    {1,1,1849, 6.9},
    {1,1,1850, 7.1},
    {1,1,1851, 7.2},
    {1,1,1852, 7.3},
    {1,1,1853, 7.4},
    {1,1,1854, 7.5},
    {1,1,1855, 7.6},
    {1,1,1856, 7.7},
    {1,1,1857, 7.7},
    {1,1,1858, 7.8},
    {1,1,1859, 7.8},
    {1,1,1860, 7.88},
    {1,1,1861, 7.82},
    {1,1,1862, 7.54},
    {1,1,1863, 6.97},
    {1,1,1864, 6.40},
    {1,1,1865, 6.02},
    {1,1,1866, 5.41},
    {1,1,1867, 4.10},
    {1,1,1868, 2.92},
    {1,1,1869, 1.82},
    {1,1,1870, 1.61},
    {1,1,1871, 0.10},
    {1,1,1872,-1.02},
    {1,1,1873,-1.28},
    {1,1,1874,-2.69},
    {1,1,1875,-3.24},
    {1,1,1876,-3.64},
    {1,1,1877,-4.54},
    {1,1,1878,-4.71},
    {1,1,1879,-5.11},
    {1,1,1880,-5.40},
    {1,1,1881,-5.42},
    {1,1,1882,-5.20},
    {1,1,1883,-5.46},
    {1,1,1884,-5.46},
    {1,1,1885,-5.79},
    {1,1,1886,-5.63},
    {1,1,1887,-5.64},
    {1,1,1888,-5.80},
    {1,1,1889,-5.66},
    {1,1,1890,-5.87},
    {1,1,1891,-6.01},
    {1,1,1892,-6.19},
    {1,1,1893,-6.64},
    {1,1,1894,-6.44},
    {1,1,1895,-6.47},
    {1,1,1896,-6.09},
    {1,1,1897,-5.76},
    {1,1,1898,-4.66},
    {1,1,1899,-3.74},
    {1,1,1900,-2.72},
    {1,1,1901,-1.54},
    {1,1,1902,-0.02},
    {1,1,1903, 1.24},
    {1,1,1904, 2.64},
    {1,1,1905, 3.86},
    {1,1,1906, 5.37},
    {1,1,1907, 6.14},
    {1,1,1908, 7.75},
    {1,1,1909, 9.13},
    {1,1,1910,10.46},
    {1,1,1911,11.53},
    {1,1,1912,13.36},
    {1,1,1913,14.65},
    {1,1,1914,16.01},
    {1,1,1915,17.20},
    {1,1,1916,18.24},
    {1,1,1917,19.06},
    {1,1,1918,20.25},
    {1,1,1919,20.95},
    {1,1,1920,21.16},
    {1,1,1921,22.25},
    {1,1,1922,22.41},
    {1,1,1923,23.03},
    {1,1,1924,23.49},
    {1,1,1925,23.62},
    {1,1,1926,23.86},
    {1,1,1927,24.49},
    {1,1,1928,24.34},
    {1,1,1929,24.08},
    {1,1,1930,24.02},
    {1,1,1931,24.00},
    {1,1,1932,23.87},
    {1,1,1933,23.95},
    {1,1,1934,23.86},
    {1,1,1935,23.93},
    {1,1,1936,23.73},
    {1,1,1937,23.92},
    {1,1,1938,23.96},
    {1,1,1939,24.02},
    {1,1,1940,24.33},
    {1,1,1941,24.83},
    {1,1,1942,25.30},
    {1,1,1943,25.70},
    {1,1,1944,26.24},
    {1,1,1945,26.77},
    {1,1,1946,27.28},
    {1,1,1947,27.78},
    {1,1,1948,28.25},
    {1,1,1949,28.71},
    {1,1,1950,29.15},
    {1,1,1951,29.57},
    {1,1,1952,29.97},
    {1,1,1953,30.36},
    {1,1,1954,30.72},
    {1,1,1955,31.07},
    {1,1,1956,31.35},
    {1,1,1957,31.68},
    {1,1,1958,32.18},
    {1,1,1959,32.68},
    {1,1,1960,33.15},
    {1,1,1961,33.59},
    {1,1,1962,34.00},
    {1,1,1963,34.47},
    {1,1,1964,35.03},
    {1,1,1965,35.73},
    {1,1,1966,36.54},
    {1,1,1967,37.43},
    {1,1,1968,38.29},
    {1,1,1969,39.20},
    {1,1,1970,40.18},
    {1,1,1971,41.17},
    {1,1,1972,42.23},
    {1,1,1973,43.37},
    {1,1,1974,44.49},
    {1,1,1975,45.48},
    {1,1,1976,46.46},
    {1,1,1977,47.52},
    {1,1,1978,48.53},
    {1,1,1979,49.59},
    {1,1,1980,50.54},
    {1,1,1981,51.38},
    {1,1,1982,52.17},
    {1,1,1983,52.96},
    {1,1,1984,53.79},
    {1,1,1985,54.34},
    {1,1,1986,54.87},
    {1,1,1987,55.32},
    {1,1,1988,55.82},
    {1,1,1989,56.30},
    {1,1,1990,56.86},
    {1,1,1991,57.57},
    {1,1,1992,58.31},
    {1,1,1993,59.12},
    {1,1,1994,59.98},
    {1,1,1995,60.78},
    {1,1,1996,61.63},
    {1,1,1997,62.29},
    {1,1,1998,62.97},
    {1,1,1999,63.46},
    {1,1,2000,63.83},
    {1,1,2001,64.09},
    {1,1,2002,64.30},
    {1,1,2003,64.47},
    {0,0,0,0} // ET_minus_UT_table_final_element
  };
  
  // Date
  
  double Date::delta_seconds(int y, int m, int d, const TimeScale from, const TimeScale to) {
    
    double delta_seconds=0.0;
    
    if (to==from) return delta_seconds;
    
    int j = 0;
    
    // convert from 'from' to TDT
    switch (from) {
      
    case TDT: 
      delta_seconds=0.0;
      break;
      
    case TAI:
      delta_seconds=32.184;
      break;
      
    case UTC:
      delta_seconds = 32.184;
      if (y>=TAI_minus_UTC_table[0].year) {
	j=0;
    	TAI_minus_UTC_element candidate = TAI_minus_UTC_table[0];
	while (TAI_minus_UTC_table[j] != TAI_minus_UTC_table_final_element) {
	  
	  if (TAI_minus_UTC_table[j].year   <=y) {
	    if (TAI_minus_UTC_table[j].month<=m) {
	      if (TAI_minus_UTC_table[j].day<=d) {
		if ( ( (TAI_minus_UTC_table[j].year >  candidate.year) ) ||
		     ( (TAI_minus_UTC_table[j].year == candidate.year) && (TAI_minus_UTC_table[j].month >  candidate.month) ) ||
		     ( (TAI_minus_UTC_table[j].year == candidate.year) && (TAI_minus_UTC_table[j].month == candidate.month) && (TAI_minus_UTC_table[j].day > candidate.day) ) ) { 
		  candidate = TAI_minus_UTC_table[j];
		}		
	      }
	    }
	  }
	  
	  j++;
	}
	
	delta_seconds += candidate.TAI_minus_UTC;
      }
      break;
      
    case UT1:
      delta_seconds = 0.0;
      if (y>=ET_minus_UT_table[0].year) {
	j=0;
	ET_minus_UT_element candidate=ET_minus_UT_table[0];
	while (ET_minus_UT_table[j] != ET_minus_UT_table_final_element) {
	  
	  if (ET_minus_UT_table[j].year   <=y) {
	    if (ET_minus_UT_table[j].month<=m) {
	      if (ET_minus_UT_table[j].day<=d) {
		
		if ( ( (ET_minus_UT_table[j].year >  candidate.year) ) ||
		     ( (ET_minus_UT_table[j].year == candidate.year) && (ET_minus_UT_table[j].month >  candidate.month) ) ||
		     ( (ET_minus_UT_table[j].year == candidate.year) && (ET_minus_UT_table[j].month == candidate.month) && (ET_minus_UT_table[j].day > candidate.day) ) ) {
		  candidate = ET_minus_UT_table[j];
		}
	      }
	    }
	  }
	  
	  j++;
	}
	delta_seconds += candidate.ET_minus_UT;
      }
      break;
      
    case GPS:
      delta_seconds = 32.184 + 19.0;
      break;
    }
    
    // convert from TDT to 'to'
    switch (to) {
      
    case TDT:
      break;
      
    case TAI: 
      delta_seconds -= 32.184; 
      break;
      
    case UTC: 
      delta_seconds -= 32.184;
      if (y>=TAI_minus_UTC_table[0].year) {
	j=0;
      	TAI_minus_UTC_element candidate = TAI_minus_UTC_table[0];
	while (TAI_minus_UTC_table[j] != TAI_minus_UTC_table_final_element) {
	  
	  if (TAI_minus_UTC_table[j].year   <=y) {
	    if (TAI_minus_UTC_table[j].month<=m) {
	      if (TAI_minus_UTC_table[j].day<=d) {
		if ( ( (TAI_minus_UTC_table[j].year >  candidate.year) ) ||
		     ( (TAI_minus_UTC_table[j].year == candidate.year) && (TAI_minus_UTC_table[j].month >  candidate.month) ) ||
		     ( (TAI_minus_UTC_table[j].year == candidate.year) && (TAI_minus_UTC_table[j].month == candidate.month) && (TAI_minus_UTC_table[j].day > candidate.day) ) ) { 
		  candidate = TAI_minus_UTC_table[j];
		}
	      }
	    }
	  }
	  
	  j++;
	}
	
	delta_seconds -= candidate.TAI_minus_UTC;
      }  
      break;
      
    case UT1:
      if (y>=ET_minus_UT_table[0].year) {
	j=0;
      	ET_minus_UT_element candidate=ET_minus_UT_table[0];
	while (ET_minus_UT_table[j] != ET_minus_UT_table_final_element) {
	  
	  if (ET_minus_UT_table[j].year   <=y) {
	    if (ET_minus_UT_table[j].month<=m) {
	      if (ET_minus_UT_table[j].day<=d) {
		if ( ( (ET_minus_UT_table[j].year >  candidate.year) ) ||
		     ( (ET_minus_UT_table[j].year == candidate.year) && (ET_minus_UT_table[j].month >  candidate.month) ) ||
		     ( (ET_minus_UT_table[j].year == candidate.year) && (ET_minus_UT_table[j].month == candidate.month) && (ET_minus_UT_table[j].day > candidate.day) ) ) {
		  candidate = ET_minus_UT_table[j];
		}
	      }
	    }
	  }
	  
	  j++;
	}
	
	delta_seconds -= candidate.ET_minus_UT;
      }
      break;
      
    case GPS: 
      delta_seconds -= (32.184 + 19.0);
      break;
    }
    
    // cerr << "delta_seconds: from " << TimeScaleLabel(from) << " to " << TimeScaleLabel(to) << " = " << delta_seconds << endl;
    
    return (delta_seconds);
  }
  
  Date::Date() : sdn(0), df(0) { }
  
  Date::Date(const UniverseTypeAwareTime & t) : sdn(t.GetDate().sdn), df(t.GetDate().df) { }
  
  Date::Date(const Date & d) : sdn(d.sdn), df(d.df) { }
  
  //! fractionary day, relative to 00:00  
  void Date::SetGregor(int  y, int  m, double d, TimeScale ts) {
    
    int id = (int)(floor(d));
    d -= id;
    //
    d *= 24;
    int H = (int)(floor(d));
    d -= H;
    //
    d *= 60;
    int M = (int)(floor(d));
    d -= M;
    //
    d *= 60;
    int S = (int)(floor(d));
    d -= S;
    //
    d *= 1000;
    int ms = (int)(floor(d));
    
    SetGregor(y,m,id,H,M,S,ms,ts);
  }
  
  void Date::SetGregor(int y, int m, int d, int H, int M, int S, int ms, TimeScale ts) {
    
    ms += (int)(1000.0 * delta_seconds(y,m,d,ts));
    
    // checks...
    while (ms >= 1000) {
      S  += 1;
      ms -= 1000;
    }
    while (S >= 60) {
      M += 1;
      S -= 60;
    }
    while (M >= 60) {
      H += 1;
      M -= 60;
    }
    while (H >= 24) {
      d += 1;
      H -= 24;
    }
    
    // more...
    while (ms < 0) {
      S  -= 1;
      ms += 60;
    }
    while (S < 0) {
      M -= 1;
      S += 60;
    }
    while (M < 0) {
      H -= 1;
      M += 60;
    }
    while (H < 0) {
      d -= 1;
      H += 24;
    }
    
    sdn = GregorianToSdn(y,m,d);
    df  = ( ( (H * 60 + M) * 60 + S) * 1000 + ms) * (TimeStep::max_day_fraction() / 86400000);
  }
  
  /* 
     void Date::GetGregor(int &y, int &m, int &d, TimeScale ts) const {
     SdnToGregorian((long int)floor(_ref_time+_delta_time), &y, &m, &d);
     const double tmp1 = _ref_time   - floor(_ref_time);
     const double tmp2 = _delta_time - floor(_delta_time);
     const double local_frac = (tmp1+tmp2) - floor(tmp1+tmp2) - (1.0/86400.0)*delta_seconds(y,m,d,ts); 
     if (local_frac < 0.0) {
     SdnToGregorian((long int)floor(_ref_time+_delta_time)-1, &y, &m, &d);
     } else if (local_frac > 1.0) { 
     SdnToGregorian((long int)floor(_ref_time+_delta_time)+1, &y, &m, &d);
     }
     }
  */
  
  void Date::GetGregor(int &y, int &m, int &d, TimeScale ts) const {
    SdnToGregorian(sdn, &y, &m, &d);
    const double ds = delta_seconds(y,m,d,ts);
    const int delta_df = - (int)(ds*(TimeStep::max_day_fraction() / 86400)); // negative!
    if (delta_df < 0) {
      if (abs(delta_df) > df) {
	SdnToGregorian(sdn-1, &y, &m, &d);
      }
    } else {
      const unsigned int local_df = df + delta_df;
      if (local_df >= TimeStep::max_day_fraction()) {
	SdnToGregorian(sdn+1, &y, &m, &d);
      }
    }
  }
  
  double Date::GetDayFraction(TimeScale ts) const {
    return (GetDayFraction_unsigned_int(ts)*1.0)/(TimeStep::max_day_fraction());
  }
  
  unsigned int Date::GetDayFraction_unsigned_int(TimeScale ts) const {
    int y,m,d;
    SdnToGregorian(sdn, &y, &m, &d);
    const double ds = delta_seconds(y,m,d,ts);
    const int delta_df = - (int)(ds*(TimeStep::max_day_fraction() / 86400)); // negative!
    unsigned int local_df = 0;
    if (delta_df < 0) {
      if (abs(delta_df) > df) {
	local_df = TimeStep::max_day_fraction() + df - abs(delta_df);
      } else {
	local_df = df + delta_df;
      }
    } else {
      local_df = df + delta_df;
    }
    local_df %= TimeStep::max_day_fraction();
    return (local_df);
  }
  
  double Date::Time() const {
    return (FromUnits(GetJulian(),DAY));
  }
  
  double Date::GetTime() const {
    return (FromUnits(GetJulian(),DAY));
  }
  
  void Date::SetTime(const double t) {
    SetJulian(FromUnits(t,DAY,-1));
  }
  
  Date & Date::operator += (const UniverseTypeAwareTimeStep & ts) {
    
    sdn += ts.sign() * ts.days();
    
    if (ts.sign() == -1) {
      if (ts.day_fraction() > df) {
	--sdn;
	df = TimeStep::max_day_fraction() + df - ts.day_fraction();
      } else {
	df -= ts.day_fraction();
      }
    } else {
      df += ts.day_fraction();
    }
    
    while (df >= TimeStep::max_day_fraction()) {
      ++sdn;
      df -= TimeStep::max_day_fraction();
    }
    
    return * this;
  }
  
  Date & Date::operator -= (const UniverseTypeAwareTimeStep & ts) {
    * this += - ts;
    return * this;
  }
  
  bool Date::operator == (const Date & d) const {
    if (sdn != d.sdn) return false;
    if (df != d.df) return false;
    return true;
  }
  
  bool Date::operator != (const Date & d) const {
    return (!((*this)==d));
  }
  
  bool Date::operator < (const Date & d) const {
    return (GetTimeStep() < d.GetTimeStep());
  }
  
  bool Date::operator > (const Date & d) const {
    return (GetTimeStep() > d.GetTimeStep());
  }
  
  void Date::SetJulian(double jd, TimeScale ts) {
    sdn = (unsigned int)(floor(jd));
    double frac = jd - floor(jd) + 0.5;
    if (frac >= 1.0) {
      ++sdn;
      frac = fmod(frac,1.0);
    }
    //
    {
      int y,m,d;
      SdnToGregorian(sdn, &y, &m, &d);
      jd += (1.0/86400.0)*delta_seconds(y,m,d,ts);
    }
    sdn = (unsigned int)(floor(jd));
    frac = jd - floor(jd) + 0.5;
    if (frac >= 1.0) {
      ++sdn;
      frac = fmod(frac,1.0);
    }
    
    df = (unsigned int)rint(frac * TimeStep::max_day_fraction());
  }
  
  void Date::GetJulian(double & jd, TimeScale ts) const {
    int y,m,d;
    SdnToGregorian(sdn, &y, &m, &d);
    jd = sdn + ((df*1.0)/ TimeStep::max_day_fraction()) - 0.5 - (1.0/86400.0)*delta_seconds(y,m,d,ts);
  }
  
  double Date::GetJulian(TimeScale ts) const {
    double jd;
    GetJulian(jd,ts);
    return jd;
  }
  
  void Date::SetNow() {
    const time_t tt_now = time(0);
    struct tm *tm_struct = gmtime(&tt_now);
    // this is accurate down to a second, microseconds are not used
    // if they were, the term is ...tm_sec+(tv.tv_usec*1.0e-6))...
    // SetGregor(1900+tm_struct->tm_year,1+tm_struct->tm_mon,tm_struct->tm_mday+((tm_struct->tm_sec)/86400.0+tm_struct->tm_min/1440.0+tm_struct->tm_hour/24.0),UTC);
    SetGregor(1900+tm_struct->tm_year,
	      1+tm_struct->tm_mon,
	      tm_struct->tm_mday,
	      tm_struct->tm_hour,
	      tm_struct->tm_min,
	      tm_struct->tm_sec,
	      0, // ms
	      UTC);
  }
  
  void Date::SetToday() {
    SetNow();
    df = 0; // reset day fraction
  }
  
  void Date::SetJ2000() {
    SetJulian(2451545.0,TT); /* IAU definition: [2000 Jan 1d 12h TDT]
				http://en.wikipedia.org/wiki/Terrestrial_Time 
				http://en.wikipedia.org/wiki/Month 
				http://aa.usno.navy.mil/software/novas/novas_c/novasc_info.html
				http://nanotitan.com/software/api/suite/diamond/nT/quantity/constant/TIME_INSTANT.html#J2000
			     */
  }
  
  string TimeScaleLabel(TimeScale ts) {
    if (ts==UTC) return "UTC";
    if (ts==UT)  return "UT";
    if (ts==UT1) return "UT1";
    if (ts==TAI) return "TAI";
    if (ts==TDT) return "TDT";
    if (ts==ET)  return "ET";
    if (ts==GPS) return "GPS";     
    return "";
  }
  
  // TimeStep
  
  TimeStep::TimeStep() : _days(0), _day_fraction(0), _sign(+1) { 
    internal_check();
  }
  
  TimeStep::TimeStep(const unsigned int days, const unsigned int day_fraction, const int sign) : _days(days), _day_fraction(day_fraction), _sign(sign) {
    if (_sign == 0) {
      ORSA_ERROR("Hmmm, sign equal to zero...");
    }	else {
      _sign = _sign / abs(_sign);
    }
    internal_check();
  }
  
  TimeStep::TimeStep(const double t) {
    if (t < 0) {
      _sign = -1;
    } else {
      _sign = +1;
    }	
    
    const double t_in_days = FromUnits(fabs(t),DAY,-1);
    _days         = (unsigned int)(floor(t_in_days));
    _day_fraction = (unsigned int)rint((t_in_days - _days)*max_day_fraction());
    
    internal_check();
  }
  
  TimeStep::TimeStep(const TimeStep & ts) : _days(ts._days), _day_fraction(ts._day_fraction), _sign(ts._sign) {
    internal_check();
  }
  
  void TimeStep::internal_check() {
    if (IsZero()) _sign = +1;
  }
  
  void TimeStep::AddDays(const unsigned int d, const int sign) {
    if (sign == _sign) {
      _days += d;
    } else {
      if (d > _days) {
	_sign = -_sign;
	_days = d - _days - 1;
       	_day_fraction = max_day_fraction() - _day_fraction;
	if (_day_fraction >= max_day_fraction()) {
	  ++_days;
	  _day_fraction -= max_day_fraction();
	}
      } else {
	_days -= d;
      }
    }
    internal_check();
  }
  
  void TimeStep::AddDayFractions(const unsigned int df, const int sign) {
    if (sign == _sign) {
      _day_fraction += df;
      if (_day_fraction >= max_day_fraction()) {
	++_days;
	_day_fraction -= max_day_fraction();
      }
    } else {
      if (_day_fraction >= df) {
	_day_fraction -= df;
      } else {
	if (_days > 0) {
	  --_days;
	  _day_fraction += max_day_fraction();
	  _day_fraction -= df;
	} else {
	  _sign = -_sign;
	  _day_fraction = df - _day_fraction;
	}
      }
    }
    internal_check();
  }
  
  /* 
     TimeStep & TimeStep::operator += (const TimeStep & t) { 
     
     if (_sign != t._sign) {
     if ( (t._days > _days) || 
     ( (t._days == _days) && 
     (_day_fraction >= t._day_fraction) ) ) {
     _sign = -_sign;
     _days = t._days - _days;
     if (_day_fraction >= t._day_fraction) {
     _day_fraction -= t._day_fraction;
     } else {
     --_days;
     _day_fraction += max_day_fraction();
     _day_fraction -= t._day_fraction;
     }
     } else {
     _days  -= t._days;
     
     // _day_fraction -= t._day_fraction;
     if (_day_fraction >= t._day_fraction) {
     _day_fraction -= t._day_fraction;
     } else {
     --_days;
     _day_fraction += max_day_fraction();
     _day_fraction -= t._day_fraction;
     }
     
     }
     } else {
     _days         += t._days;
     _day_fraction += t._day_fraction;
     }
     
     while (_day_fraction >= max_day_fraction()) {
     ++_days;
     _day_fraction -= max_day_fraction();
     }
     
     return * this;
     }
  */
  
  /* 
     TimeStep & TimeStep::operator += (const TimeStep & ts) { 
     if (_sign == ts._sign) {
     _days         += ts._days;
     _day_fraction += ts._day_fraction;
     while (_day_fraction >= max_day_fraction()) {
     ++_days;
     _day_fraction -= max_day_fraction();
     }
     } else {
     * this -= - ts;
     }
     return * this;
     }
  */
  
  TimeStep & TimeStep::operator += (const TimeStep & ts) { 
    AddDays(ts._days,ts._sign);
    AddDayFractions(ts._day_fraction,ts._sign);
    return * this;
  }
  
  TimeStep & TimeStep::operator -= (const TimeStep & ts) { 
    AddDays(ts._days,-ts._sign);
    AddDayFractions(ts._day_fraction,-ts._sign);
    return * this;
  }
  
  /* 
     TimeStep & TimeStep::operator -= (const TimeStep & ts) { 
     if (_sign == ts._sign) {
     if (ts._days > _days) {
     _sign = -_sign;
     _days = ts._days - _days;
     if (_day_fraction >= ts._day_fraction) {
     _day_fraction -= ts._day_fraction;
     } else {
     --_days;
     _day_fraction += max_day_fraction();
     _day_fraction -= ts._day_fraction;
     }
     } else if ( (ts._days == _days) && 
     (ts._day_fraction > _day_fraction) ) {
     _sign = -_sign;
     _days = 0;
     _day_fraction = ts._day_fraction - _day_fraction;
     } else {
     _days -= ts._days;
     // _day_fraction -= ts._day_fraction;
     if (_day_fraction >= ts._day_fraction) {
     _day_fraction -= ts._day_fraction;
     } else {
     --_days;
     _day_fraction += max_day_fraction();
     _day_fraction -= ts._day_fraction;
     }
     }
     } else {
     * this += - ts;
     }      
     return * this;
     }
  */
  
  TimeStep TimeStep::operator + (const TimeStep & ts) const {
    TimeStep _ts(*this);
    _ts += ts;
    return _ts;
  }
  
  TimeStep TimeStep::operator - (const TimeStep & ts) const {
    TimeStep _ts(*this);
    _ts -= ts;
    return _ts;
  }
  
  /* 
     TimeStep & TimeStep::operator *= (const int p) {
     
     if (p == 0) {
     _days = _day_fraction = 0;
     _sign = +1;
     return * this;
     }
     
     if (p < 0) _sign = -_sign;
     
     _days *= abs(p);  
     
     const unsigned int original_day_fraction = _day_fraction;
     unsigned int count = abs(p)-1;
     while (count != 0) {
     _day_fraction += original_day_fraction;
     while (_day_fraction >= max_day_fraction()) {
     ++_days;
     _day_fraction -= max_day_fraction();
     }
     --count;
     }
     
     return * this;
     }
  */
  
  TimeStep & TimeStep::operator *= (const int p) {
    const double t = GetDouble()*p;
    // same code in constructor from double
    if (t < 0) {
      _sign = -1;
    } else {
      _sign = +1;
    }	
    //    
    const double t_in_days = FromUnits(fabs(t),DAY,-1);
    _days         = (unsigned int)(floor(t_in_days));
    _day_fraction = (unsigned int)rint((t_in_days - _days)*max_day_fraction());
    //
    internal_check();
    //
    return * this;
  }	
  
  TimeStep & TimeStep::operator *= (const double x) {
    const double t = GetDouble()*x;
    // same code in constructor from double
    if (t < 0) {
      _sign = -1;
    } else {
      _sign = +1;
    }	
    //    
    const double t_in_days = FromUnits(fabs(t),DAY,-1);
    _days         = (unsigned int)(floor(t_in_days));
    _day_fraction = (unsigned int)rint((t_in_days - _days)*max_day_fraction());
    //
    internal_check();
    //
    return * this;
  }
  
  /* 
     TimeStep & TimeStep::operator *= (const double x) {
    
     const unsigned int original_days = _days;      
     
     const unsigned int original_day_fraction = _day_fraction;      
     
     std::cerr << "original_days = " << original_days << "  original_day_fraction = " << original_day_fraction << std::endl;
     
     if (x == 0.0) {
     _days = _day_fraction = 0;
     _sign = +1;
     return * this;
     }
     
     if (x < 0.0) _sign = -_sign;
     
     _days = (unsigned int)(fabs(x)*_days);
     
     _day_fraction = (unsigned int)rint((fabs(x)-floor(fabs(x)))*original_day_fraction);
     
     if (original_days > 0) {
     const unsigned int frac = (unsigned int)rint((fabs(x)-floor(fabs(x)))*max_day_fraction());
     for (unsigned int k=0;k<original_days;++k) {
     _day_fraction += frac;
     while (_day_fraction >= max_day_fraction()) {
     ++_days;
     _day_fraction -= max_day_fraction();
     }
     }
     }
     
     if (((unsigned int)floor(fabs(x))) > 0) {
     // unsigned int count = (unsigned int)floor(fabs(x))-1;
     unsigned int count = (unsigned int)floor(fabs(x));
     while (count != 0) {
     _day_fraction += original_day_fraction;
     while (_day_fraction >= max_day_fraction()) {
     ++_days;
     _day_fraction -= max_day_fraction();
     }
     --count;
     }
     }
     
     std::cerr << "out:   x = " << x << "  _days = " << _days << "  _day_fraction = " << _day_fraction << std::endl;
     
     return * this;
     }
  */
  
  TimeStep TimeStep::operator + () const { return TimeStep(*this); }
  
  TimeStep TimeStep::operator - () const { return TimeStep(_days,_day_fraction,-_sign); }
  
  bool TimeStep::operator < (const TimeStep & ts) const {
    
    if (*this == ts) return false; // they are equal!
    
    if (_sign == ts._sign) {
      if (_sign == -1) {
	if (_days > ts._days) return true;
	if ( (_days == ts._days) &&
	     (_day_fraction > ts._day_fraction) ) return true;
      } else {
	if (_days < ts._days) return true;
	if ( (_days == ts._days) &&
	     (_day_fraction < ts._day_fraction) ) return true;
      }
    } else {
      if (_sign == -1) {
	return true;
      }
    }
    return false;
  }
  
  bool TimeStep::operator > (const TimeStep & ts) const {
    if (*this == ts) return false; // they are equal!
    return (!((*this) < ts));
  }
  
  double TimeStep::GetDouble() const {
    return (FromUnits(_sign*(_days+(_day_fraction*1.0)/max_day_fraction()),DAY));
  }
  
  bool TimeStep::operator == (const TimeStep & ts) const {
    
    // if (ts.IsZero() && IsZero()) return true; // this check is 'probably' not needed since we use the internal_check() call every time we change the object
    if (ts._days != _days) return false;
    if (ts._day_fraction != _day_fraction) return false;
    if (ts._sign != _sign) return false;
    return true;
  }
  
  bool TimeStep::operator != (const TimeStep & ts) const {
    return (!((*this) == ts));
  }
  
  TimeStep TimeStep::absolute() const { return TimeStep(_days,_day_fraction,+1); }
  
  bool TimeStep::IsZero() const { return ((_days == 0) && (_day_fraction == 0)); }
  
  // UniverseTypeAwareTime
  
  UniverseTypeAwareTime::UniverseTypeAwareTime() { }	
  
  UniverseTypeAwareTime::UniverseTypeAwareTime(const double t) {
    SetTime(t);
  }
  
  UniverseTypeAwareTime::UniverseTypeAwareTime(const Date & d) {
    SetTime(d);
  }
  
  UniverseTypeAwareTime::UniverseTypeAwareTime(const UniverseTypeAwareTime & t) {
    date = t.date;
    time = t.time;
  }
  
  double UniverseTypeAwareTime::GetTime() const {
    double _t = 0.0;
    switch (universe->GetUniverseType()) {
    case Real:
      _t = date.GetTime();
      break;
    case Simulated:
      _t = time;
      break;
    }
    return _t;
  }
  
  double UniverseTypeAwareTime::Time() const {
    return (GetTime());
  }
  
  Date UniverseTypeAwareTime::GetDate() const {
    return (date);
  }
  
  void  UniverseTypeAwareTime::SetTime(const double t) {
    date.SetJulian(FromUnits(t,DAY,-1));
    time = t;
  }
  
  void UniverseTypeAwareTime::SetTime(const Date & d) {
    SetDate(d);
  }
  
  void  UniverseTypeAwareTime::SetDate(const Date & d) {
    date = d;
    time = d.GetTime();
  }
  
  void UniverseTypeAwareTime::SetTime(const UniverseTypeAwareTime & t) {
    date = t.date;
    time = t.time;
  }
  
  /* 
     UniverseTypeAwareTimeStep UniverseTypeAwareTime::operator + (const UniverseTypeAwareTime & t) const {
     switch (universe->GetUniverseType()) {
     case Real:
     {
     UniverseTypeAwareTimeStep _ts(date.GetTimeStep());
     _ts += t.GetDate().GetTimeStep();
     return _ts;
     }
     break;
     case Simulated:
     {
     UniverseTypeAwareTimeStep _ts(time);
     _ts += t.Time();
     return _ts;
     }
     break;
     }
     return UniverseTypeAwareTimeStep();
     }
  */
  
  UniverseTypeAwareTimeStep UniverseTypeAwareTime::operator - (const UniverseTypeAwareTime & t) const {
    switch (universe->GetUniverseType()) {
    case Real:
      {
	UniverseTypeAwareTimeStep _ts(date.GetTimeStep());
	_ts -= t.GetDate().GetTimeStep();
	return _ts;
      }
      break;
    case Simulated:
      {
	UniverseTypeAwareTimeStep _ts(time);
	_ts -= t.Time();
	return _ts;
      }
      break;
    }
    return UniverseTypeAwareTimeStep();
  }
  
  /* 
     UniverseTypeAwareTimeStep UniverseTypeAwareTime::operator + (const UniverseTypeAwareTimeStep & ts) const {
     switch (universe->GetUniverseType()) {
     case Real:
     {
     UniverseTypeAwareTimeStep _ts(date.GetTimeStep());
     _ts += ts;
     return _ts;
     }
     break;
     case Simulated:
     {
     UniverseTypeAwareTimeStep _ts(time);
     _ts += ts;
     return _ts;
     }
     break;
     }
     return UniverseTypeAwareTimeStep();
     }
  */
  
  /* 
     UniverseTypeAwareTimeStep UniverseTypeAwareTime::operator - (const UniverseTypeAwareTimeStep & ts) const {
     switch (universe->GetUniverseType()) {
     case Real:
     {
     UniverseTypeAwareTimeStep _ts(date.GetTimeStep());
     _ts -= ts;
     return _ts;
     }
     break;
     case Simulated:
     {
     UniverseTypeAwareTimeStep _ts(time);
     _ts -= ts;
     return _ts;
     }
     break;
     }
     return UniverseTypeAwareTimeStep();
     }
  */
  
  UniverseTypeAwareTime UniverseTypeAwareTime::operator + (const UniverseTypeAwareTimeStep & ts) const {
    switch (universe->GetUniverseType()) {
    case Real:
      {
	UniverseTypeAwareTime _t(date);
	_t += ts;
	return _t;
      }
      break;
    case Simulated:
      {
	UniverseTypeAwareTime _t(time);
	_t += ts;
	return _t;
      }
      break;
    }
    return UniverseTypeAwareTime();
  }
  
  UniverseTypeAwareTime UniverseTypeAwareTime::operator - (const UniverseTypeAwareTimeStep & ts) const {
    switch (universe->GetUniverseType()) {
    case Real:
      {
	UniverseTypeAwareTime _t(date);
	_t -= ts;
	return _t;
      }
      break;
    case Simulated:
      {
	UniverseTypeAwareTime _t(time);
	_t -= ts;
	return _t;
      }
      break;
    }
    return UniverseTypeAwareTime();
  }
  
  UniverseTypeAwareTime & UniverseTypeAwareTime::operator += (const UniverseTypeAwareTimeStep & ts_in) {
    switch (universe->GetUniverseType()) {
    case Real:
      date += ts_in;
      break;
    case Simulated:
      time += ts_in.GetDouble();
      break;
    }
    return * this;
  }
  
  UniverseTypeAwareTime & UniverseTypeAwareTime::operator -= (const UniverseTypeAwareTimeStep & ts_in) {
    switch (universe->GetUniverseType()) {
    case Real:
      date -= ts_in;
      break;
    case Simulated:
      time -= ts_in.GetDouble();
      break;
    }
    return * this;
  }
  
  bool UniverseTypeAwareTime::operator == (const UniverseTypeAwareTime & t) const {
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      // if (date.GetTimeStep() == t.GetDate().GetTimeStep()) _z = true;
      if (date == t.GetDate()) _z = true;
      break;
    case Simulated:
      if (time == t.time) _z = true;
      break;
    }
    return _z;
  }
  
  bool UniverseTypeAwareTime::operator != (const UniverseTypeAwareTime & t) const {
    return (!((*this) == t));
  }
  
  bool UniverseTypeAwareTime::operator < (const UniverseTypeAwareTime & t) const {
    if (*this == t) return false;
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      if (date.GetTimeStep() < t.GetDate().GetTimeStep()) _z = true;
      break;
    case Simulated:
      if (time < t.time) _z = true;
      break;
    }
    return _z;
  }
  
  bool UniverseTypeAwareTime::operator > (const UniverseTypeAwareTime & t) const {
    if (*this == t) return false;
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      if (date.GetTimeStep() > t.GetDate().GetTimeStep()) _z = true;
      break;
    case Simulated:
      if (time > t.time) _z = true;
      break;
    }
    return _z;
  }
  
  bool UniverseTypeAwareTime::operator <= (const UniverseTypeAwareTime & t) const {
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      if (date.GetTimeStep() <= t.GetDate().GetTimeStep()) _z = true;
      break;
    case Simulated:
      if (time <= t.time) _z = true;
      break;
    }
    return _z;
  }
  
  bool UniverseTypeAwareTime::operator >= (const UniverseTypeAwareTime & t) const {
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      if (date.GetTimeStep() >= t.GetDate().GetTimeStep()) _z = true;
      break;
    case Simulated:
      if (time >= t.time) _z = true;
      break;
    }
    return _z;
  }
  
  // out...
  
  UniverseTypeAwareTimeStep operator * (const double x, const UniverseTypeAwareTimeStep & ts) {
    UniverseTypeAwareTimeStep _ts(ts);
    _ts *= x;
    return _ts;
  }
  
  UniverseTypeAwareTimeStep operator * (const UniverseTypeAwareTimeStep & ts, const double x) {
    UniverseTypeAwareTimeStep _ts(ts);
    _ts *= x;
    return _ts; 
  }
  
  UniverseTypeAwareTimeStep operator * (const int i, const UniverseTypeAwareTimeStep & ts) {
    UniverseTypeAwareTimeStep _ts(ts);
    _ts *= i;
    return _ts; 
  }
  
  UniverseTypeAwareTimeStep operator * (const UniverseTypeAwareTimeStep & ts, const int i) {
    UniverseTypeAwareTimeStep _ts(ts);
    _ts *= i;
    return _ts; 
  }
  
  // UniverseTypeAwareTimeStep 

  UniverseTypeAwareTimeStep::UniverseTypeAwareTimeStep() : ts(0,0,1), dts(0.0) {
    
  }
  
  UniverseTypeAwareTimeStep::UniverseTypeAwareTimeStep(const double t) : ts(t), dts(t) {
    
  }
  
  UniverseTypeAwareTimeStep::UniverseTypeAwareTimeStep(const TimeStep & ts_in) : ts(ts_in), dts(ts_in.GetDouble()) {
    
  }
  
  UniverseTypeAwareTimeStep::UniverseTypeAwareTimeStep(const int days, const unsigned int day_fraction, const int sign) : ts(days,day_fraction,sign), dts(ts.GetDouble()) {
    
  }
  
  UniverseTypeAwareTimeStep::UniverseTypeAwareTimeStep(const UniverseTypeAwareTimeStep & ts_in) : ts(ts_in.ts), dts(ts_in.dts) {
    
  }
  
  UniverseTypeAwareTimeStep &  UniverseTypeAwareTimeStep::operator += (const UniverseTypeAwareTimeStep & ts_in) {
    ts  += ts_in.ts;
    dts += ts_in.dts;
    return * this;
  }
  
  UniverseTypeAwareTimeStep &  UniverseTypeAwareTimeStep::operator -= (const UniverseTypeAwareTimeStep & ts_in) {
    ts  -= ts_in.ts;
    dts -= ts_in.dts;
    return * this;
  }
  
  UniverseTypeAwareTimeStep & UniverseTypeAwareTimeStep::operator *= (const int p) {
    ts  *= p;
    dts *= p;
    return * this;
  }
  
  UniverseTypeAwareTimeStep & UniverseTypeAwareTimeStep::operator *= (const double x) {
    ts  *= x;
    dts *= x;
    return * this;
  }	
  
  double UniverseTypeAwareTimeStep::GetDouble() const {
    double _x = 0.0;
    switch (universe->GetUniverseType()) {
    case Real:
      _x = ts.GetDouble();
      break;
    case Simulated:
      _x = dts;
      break;
    }
    return _x;
  }
  
  bool UniverseTypeAwareTimeStep::operator < (const double x) const {
    return (GetDouble() < x);
  }
  
  bool UniverseTypeAwareTimeStep::operator > (const double x) const {
    return (GetDouble() > x);
  }
  
  bool UniverseTypeAwareTimeStep::operator < (const UniverseTypeAwareTime & t) const {
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      if (ts < t.GetDate().GetTimeStep()) _z = true;
      break;
    case Simulated:
      if (dts < t.GetTime()) _z = true;
      break;
    }
    return _z;
  }
  
  bool UniverseTypeAwareTimeStep::operator > (const UniverseTypeAwareTime & t) const {
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      if (ts > t.GetDate().GetTimeStep()) _z = true;
      break;
    case Simulated:
      if (dts > t.GetTime()) _z = true;
      break;
    }
    return _z;
  }
  
  UniverseTypeAwareTimeStep UniverseTypeAwareTimeStep::operator + () const {
    return * this;
  }
  
  UniverseTypeAwareTimeStep UniverseTypeAwareTimeStep::operator - () const {
    UniverseTypeAwareTimeStep _ts;
    _ts.ts  = - ts;
    _ts.dts = - dts;
    return _ts;
  }
  
  UniverseTypeAwareTimeStep UniverseTypeAwareTimeStep::operator + (const UniverseTypeAwareTimeStep & ts_in) const {
    UniverseTypeAwareTimeStep _ts(*this);
    _ts.ts  += ts_in.ts;
    _ts.dts += ts_in.dts;
    return _ts;
  }
  
  UniverseTypeAwareTimeStep UniverseTypeAwareTimeStep::operator - (const UniverseTypeAwareTimeStep & ts_in) const {
    UniverseTypeAwareTimeStep _ts(*this);
    _ts.ts  -= ts_in.ts;
    _ts.dts -= ts_in.dts;
    return _ts;
  }
  
  UniverseTypeAwareTimeStep UniverseTypeAwareTimeStep::operator + (const UniverseTypeAwareTime & t) const {
    UniverseTypeAwareTimeStep _ts(*this);
    switch (universe->GetUniverseType()) {
    case Real:
      _ts += t.GetDate().GetTimeStep();
      break;
    case Simulated:
      _ts.dts += t.GetTime();
      break;
    }
    return _ts;
  }
  
  UniverseTypeAwareTimeStep UniverseTypeAwareTimeStep::operator - (const UniverseTypeAwareTime & t) const {
    UniverseTypeAwareTimeStep _ts(*this);
    switch (universe->GetUniverseType()) {
    case Real:
      _ts.ts -= t.GetDate().GetTimeStep();
      break;
    case Simulated:
      _ts.dts -= t.GetTime();
      break;
    }
    return _ts;
  }
  
  UniverseTypeAwareTimeStep UniverseTypeAwareTimeStep::absolute() const {
    UniverseTypeAwareTimeStep _ts(*this);
    switch (universe->GetUniverseType()) {
    case Real:
      _ts.ts = ts.absolute();
      break;
    case Simulated:
      _ts.dts = fabs(dts);
      break;
    }
    return _ts;
  }
  
  bool UniverseTypeAwareTimeStep::IsZero() const {
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      _z = ts.IsZero();
      break;
    case Simulated:
      _z = (dts == 0.0);
      break;
    }
    return _z;
  }
  
  bool UniverseTypeAwareTimeStep::operator < (const UniverseTypeAwareTimeStep & ts_in) const {
    if (*this == ts_in) return false;
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      if (ts < ts_in.ts) _z = true;
      break;
    case Simulated:
      if (dts < ts_in.dts) _z = true;
      break;
    }
    return _z;
  }
  
  bool UniverseTypeAwareTimeStep::operator > (const UniverseTypeAwareTimeStep & ts_in) const {
    if (*this == ts_in) return false;
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      if (ts > ts_in.ts) _z = true;
      break;
    case Simulated:
      if (dts > ts_in.dts) _z = true;
      break;
    }
    return _z; 
  }
  
  bool UniverseTypeAwareTimeStep::operator == (const UniverseTypeAwareTimeStep & ts_in) const {
    bool _z = false;
    switch (universe->GetUniverseType()) {
    case Real:
      if (ts == ts_in.ts) _z = true;
      break;
    case Simulated:
      if (dts == ts_in.dts) _z = true;
      break;
    }
    return _z;
  }
  
  bool UniverseTypeAwareTimeStep::operator != (const UniverseTypeAwareTimeStep & ts_in) const {
    return (!((*this) == ts_in));
  }
  
  // Angle
  
  void Angle::SetRad(double a) {
    radians = a;
  }
  
  void Angle::GetRad(double & a) const {
    a = radians;
  }
  
  double Angle::GetRad() const {
    return radians;
  }
  
  void Angle::SetDPS(double  d, double  p, double  s) {
    if (d >= 0) 
      radians = (pi/180)*(d+p/60.0+s/3600.0);
    else 
      radians = (pi/180)*(d-p/60.0-s/3600.0);
  }
  
  void Angle::GetDPS(double &d, double &p, double &s) const {
    double frac, fdeg = (180/pi)*radians;
    if (fdeg < 0.0) {
      d    = -floor(-fdeg);
      frac = d - fdeg;
    } else {
      d    = floor(fdeg);
      frac = fdeg - d;
    }
    //
    p = floor(frac*60.0);  
    s = frac*3600.0 - p*60.0;
  } 
  
  void Angle::SetHMS(double  h, double  m, double  s) {
    if (h >= 0) 
      radians = 15*(pi/180)*(h+m/60.0+s/3600.0);
    else
      radians = 15*(pi/180)*(h-m/60.0-s/3600.0);
  }
  
  void Angle::GetHMS(double &h, double &m, double &s) const {
    double frac, fh = (180/pi)*radians/15.0;
    if (fh < 0.0) {
      h    = -floor(-fh);
      frac = h - fh;
    } else {
      h    = floor(fh);
      frac = fh - h;
    }
    //
    m = floor(frac*60.0);  
    s = frac*3600.0 - m*60.0; 
  }    
  
  // reference systems
  
  Angle obleq(const Date &date) {
    // T in centuries from J2000 
    Date d = date;
    // d.ConvertToTimeScale(UT); // UT or UTC ??
    // const double T = (d.GetJulian() - 2451545.0)/36525.0;
    // DOUBLE-CHECK this "UT"!!!
    const double T = (d.GetJulian(UT) - 2451545.0)/36525.0;
    Angle a;
    // updated Feb 2004
    a.SetDPS(23,26,21.448+((0.001813*T-0.00059)*T-46.8150)*T);
    return a;
  }
  
  // short term correction
  /* 
     Angle delta_obleq(const Date &date) {
     Date d = date;
     d.ConvertToTimeScale(UT); // UT or UTC ??
     // days from 2451545.0
     const double days = (d.GetJulian() - 2451545.0);
     Angle a;
     // updated Feb 2004
     const double delta_grad = ( 0.0026*cos((pi/180.0)*(125.0-0.05295*days)) +
     0.0002*cos((pi/180.0)*(200.9+1.97129*days)) );
     a.SetDPS(delta_grad,0,0);
     return a;
     }
  */
  
  // short term longitude correction
  /* 
     Angle delta_longitude(const Date &date) {
     Date d = date;
     d.ConvertToTimeScale(UT); // UT or UTC ??
     // days from 2451545.0
     const double days = (d.GetJulian() - 2451545.0);
     Angle a;
     // updated Feb 2004
     const double delta_grad = ( -0.0048*sin((pi/180.0)*(125.0-0.05295*days))
     -0.0004*sin((pi/180.0)*(200.9+1.97129*days)) );
     a.SetDPS(delta_grad,0,0);
     return a;
     }
  */
  
  Angle gmst(const Date &date) {
    // tested using xephem, very good agreement!
    // T in centuries from JD 2451545.0 UT1=UT
    Date d = date;
    // d.ConvertToTimeScale(UT); // UT or UTC ??
    // const double T = (d.GetJulian() - 2451545.0)/36525.0;
    const double T = (d.GetJulian(UT) - 2451545.0)/36525.0;
    Angle a;
    a.SetHMS(d.GetDayFraction()*24+6,41,50.54841+((-0.0000062*T+0.093104)*T+8640184.812866)*T);
    return a;
  }
  
  // J2000 versions
  Angle obleq_J2000() {
    Date d; d.SetJ2000();
    return obleq(d);
  }
  
  /* 
     Angle delta_obleq_J2000() {
     Date d; d.SetJ2000();
     return delta_obleq(d);
     }
     
     Angle delta_longitude_J2000() {
     Date d; d.SetJ2000();
     return delta_longitude(d);
     }
  */
  
  //
  
  /* 
     void EquatorialToDate_To_EquatorialJ2000(Vector &v, const Date &date) {
     // to be checked!
     }
  */
  
  //
  
  /* 
     void EclipticToEquatorial(Vector &v, const Date &date) {
     v.rotate(0,0,delta_longitude(date).GetRad());
     v.rotate(0,obleq(date).GetRad()+delta_obleq(date).GetRad(),0);  
     }
  */
  //
  /* 
     void EclipticToEquatorial(Vector &v, const Date &date) {
     v.rotate(0,0,delta_longitude(date).GetRad());
     v.rotate(0,obleq(date).GetRad()+delta_obleq(date).GetRad(),0);  
     }
     
     void EquatorialToEcliptic(Vector &v, const Date &date) {
     v.rotate(0,-obleq(date).GetRad()-delta_obleq(date).GetRad(),0);  
     v.rotate(0,0,-delta_longitude(date).GetRad());
     }
     
     void EclipticToEquatorial_J2000(Vector &v) {
     v.rotate(0,0,delta_longitude_J2000().GetRad());
     v.rotate(0,obleq_J2000().GetRad()+delta_obleq_J2000().GetRad(),0);  
     }
     
     void EquatorialToEcliptic_J2000(Vector &v) {
     v.rotate(0,-obleq_J2000().GetRad()-delta_obleq_J2000().GetRad(),0);  
     v.rotate(0,0,-delta_longitude_J2000().GetRad());
     }
  */
  
  void EclipticToEquatorial(Vector &v, const Date &date) {
    v.rotate(0,obleq(date).GetRad(),0);  
  }
  
  void EquatorialToEcliptic(Vector &v, const Date &date) {
    v.rotate(0,-obleq(date).GetRad(),0);  
  }
  
  void EclipticToEquatorial_J2000(Vector &v) {
    v.rotate(0,obleq_J2000().GetRad(),0);  
  }
  
  void EquatorialToEcliptic_J2000(Vector &v) {
    v.rotate(0,-obleq_J2000().GetRad(),0);  
  }
  
  // data from Table II, same paper of alpha_delta_meridian(...);
  static void alpha_delta_meridian_moon(const Date & date,
					Angle & alpha_zero, Angle & delta_zero, Angle & W) {
    Date tmp_date; 
    tmp_date.SetJ2000();
    const Date date_J2000(tmp_date);
    
    const double d = (date.GetJulian()-date_J2000.GetJulian());
    const double T = d / 36525.0;
    
    Angle E1, E2, E3, E4, E5, E6, E7, E8, E9, E10, E11, E12, E13;
    
    E1.SetDPS( 125.045 -  0.0529921*d,0,0);
    E2.SetDPS( 250.089 -  0.1059842*d,0,0);
    E3.SetDPS( 260.008 + 13.0120009*d,0,0);
    E4.SetDPS( 176.625 + 13.3407154*d,0,0);
    E5.SetDPS( 357.529 +  0.9856003*d,0,0);
    E6.SetDPS( 311.589 + 26.4057084*d,0,0);
    E7.SetDPS( 134.963 + 13.0649930*d,0,0);
    E8.SetDPS( 276.617 +  0.3287146*d,0,0);
    E9.SetDPS(  34.226 +  1.7484877*d,0,0);
    E10.SetDPS( 15.134 -  0.1589763*d,0,0);
    E11.SetDPS(119.743 +  0.0036096*d,0,0);
    E12.SetDPS(239.961 +  0.1643573*d,0,0);
    E13.SetDPS( 25.053 + 12.9590088*d,0,0);
    
    alpha_zero.SetDPS(269.9949 
		      +	0.0031*T
		      - 3.8787*sin(E1)
		      - 0.1204*sin(E2)
		      + 0.0700*sin(E3)
		      - 0.0172*sin(E4)
		      + 0.0072*sin(E6)
		      - 0.0052*sin(E10)
		      + 0.0043*sin(E13),0,0);
    
    delta_zero.SetDPS(66.5392
		      + 0.0130*T
		      + 1.5419*cos(E1)
		      + 0.0239*cos(E2)
		      - 0.0278*cos(E3)
		      + 0.0068*cos(E4)
		      - 0.0029*cos(E6)
		      + 0.0009*cos(E7)
		      + 0.0008*cos(E10)
		      - 0.0009*cos(E13),0,0);
    
    W.SetDPS(38.3213
	     + 13.17635815*d
	     -  1.4e-12*d*d
	     +  3.5610*sin(E1)
	     +  0.1208*sin(E2)
	     -  0.0642*sin(E3)
	     +  0.0158*sin(E4)
	     +  0.0252*sin(E5)
	     -  0.0066*sin(E6)
	     -  0.0047*sin(E7)
	     -  0.0046*sin(E8)
	     +  0.0028*sin(E9)
	     +  0.0052*sin(E10)
	     +  0.0040*sin(E11)
	     +  0.0019*sin(E12)
	     -  0.0044*sin(E13),0,0);
  }
  
  void alpha_delta_meridian(const JPL_planets p, const Date & date,
			    Angle & alpha_zero, Angle & delta_zero, Angle & W) {
    
    Date tmp_date; 
    tmp_date.SetJ2000();
    const Date date_J2000(tmp_date);
    
    // CHECK TIMESCALES!!
    const double d = (date.GetJulian()-date_J2000.GetJulian());
    const double T = d / 36525.0;
    
    switch (p) {
    case SUN:   
      alpha_zero.SetDPS(286.13,0,0);
      delta_zero.SetDPS(63.87,0,0);
      W.SetDPS(84.10+14.1844000*d,0,0);
      break;
    case MERCURY: 
      alpha_zero.SetDPS(281.01-0.033*T,0,0);
      delta_zero.SetDPS(61.45-0.005*T,0,0);
      W.SetDPS(329.548+6.1385025*d,0,0);
      break;
    case VENUS:  
      alpha_zero.SetDPS(272.76,0,0);
      delta_zero.SetDPS(67.16,0,0);
      W.SetDPS(160.20-1.4813688*d,0,0);
      break;
    case EARTH:
      alpha_zero.SetDPS(0.0-0.641*T,0,0);
      delta_zero.SetDPS(90.0-0.557*T,0,0);
      W.SetDPS(190.147+360.9856235*d,0,0);
      break;
    case MOON:
      alpha_delta_meridian_moon(date,alpha_zero,delta_zero,W);
      break;
    case MARS:   
      alpha_zero.SetDPS(317.68143-0.1061*T,0,0);
      delta_zero.SetDPS(52.88650-0.0609*T,0,0);
      W.SetDPS(176.630+350.89198226*d,0,0);
      break;
    case JUPITER: 
      alpha_zero.SetDPS(268.05-0.009*T,0,0);
      delta_zero.SetDPS(64.49+0.003*T,0,0);
      W.SetDPS(284.95+870.5366420*d,0,0);
      break;
    case SATURN:  
      alpha_zero.SetDPS(40.589-0.036*T,0,0);
      delta_zero.SetDPS(83.537-0.004*T,0,0);
      W.SetDPS(38.90+810.7939024*d,0,0);
      break;
    case URANUS: 
      alpha_zero.SetDPS(257.311,0,0);
      delta_zero.SetDPS(-15.175,0,0);
      W.SetDPS(203.81-501.1600928*d,0,0);
      break;
    case NEPTUNE:
      {
	Angle N;
	N.SetDPS(357.85+52.316*T,0,0);
	//
	alpha_zero.SetDPS(299.36+0.70*sin(N),0,0);
	delta_zero.SetDPS(43.46-0.51*cos(N),0,0);
	W.SetDPS(253.18+536.3128492*d-0.48*sin(N),0,0);
      }
      break;
    case PLUTO: 
      alpha_zero.SetDPS(313.02,0,0);
      delta_zero.SetDPS(9.09,0,0);
      W.SetDPS(236.77-56.3623195*d,0,0);
      break;
      //
    default: 
      alpha_zero.SetRad(0.0);
      delta_zero.SetRad(0.0);
      W.SetRad(0.0);
      break;
    }
  }
  
} // namespace orsa

