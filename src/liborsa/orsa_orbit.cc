/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_orbit.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H
#include "support.h"

#include "orsa_units.h"
#include "orsa_common.h"
#include "orsa_universe.h"
#include "orsa_file.h"
#include "orsa_frame.h"
#include "orsa_secure_math.h"

#ifndef _WIN32
#include <sys/time.h>
#endif
#include <time.h>

#include <cstring>
#include <limits>
#include <algorithm>

using namespace std;

namespace orsa {
  
  double Orbit::GetE() const {
    
    if (e >= 1.0) {
      ORSA_WARNING("orsa::Orbit::GetE() called with eccentricity = %g; returning M.",e);
      //
      return M;
    }

    double E = 0.0;
    if (e < 0.8) {
      
#ifdef HAVE_SINCOS
      double s,c;
      //
      ::sincos(M,&s,&c);
      const double sm = s;
      const double cm = c;
#else // HAVE_SINCOS
      const double sm = sin(M);
      const double cm = cos(M);
#endif // HAVE_SINCOS
      
      // begin with a guess accurate to order e^3 
      double x = M + e*sm*( 1.0 + e*( cm + e*( 1.0 -1.5*sm*sm)));
      
      double sx,cx;
      E = x;
      double old_E;
      double es,ec,f,fp,fpp,fppp,dx;
      
      unsigned int count = 0;
      const unsigned int max_count = 128;
      do {
	
#ifdef HAVE_SINCOS
	::sincos(x,&sx,&cx);
#else // HAVE_SINCOS
	sx = sin(x);
	cx = cos(x);
#endif // HAVE_SINCOS
	
	es = e*sx;
	ec = e*cx;
	f = x - es  - M;
	fp = 1.0 - ec; 
	fpp = es;
	fppp = ec; 
	dx = -f/fp;
	dx = -f/(fp + dx*fpp/2.0);
	dx = -f/(fp + dx*fpp/2.0 + dx*dx*fppp/6.0);
	//
	old_E = E;
	E = x + dx;
	++count;
	// update x, ready for the next iteration
	x = E;
	
      } while ((fabs(E-old_E) > 10*(fabs(E)+fabs(M))*std::numeric_limits<double>::epsilon()) && (count < max_count));
      
      if (count >= max_count) {
	ORSA_ERROR("Orbit::GetE(): max count reached, e = %g    E = %g   fabs(E-old_E) = %g   10*(fabs(E)+fabs(M))*std::numeric_limits<double>::epsilon() = %g",e,E,fabs(E-old_E),10*(fabs(E)+fabs(M))*std::numeric_limits<double>::epsilon());
      }
           
    } else {
      
      double m = fmod(10*twopi+fmod(M,twopi),twopi);
      bool iflag = false;
      if (m > pi) {
	m = twopi - m;
	iflag = true;
      }
      
      // Make a first guess that works well for e near 1.  
      double x = secure_pow(6.0*m,1.0/3.0) - m;
      E = x;
      double old_E;
      double sa,ca,esa,eca,f,fp,dx;
       
      unsigned int count = 0;
      const unsigned int max_count = 128;
      do {
	
#ifdef HAVE_SINCOS
	::sincos(x+m,&sa,&ca);
#else // HAVE_SINCOS
	sa = sin(x+m);
	ca = cos(x+m);
#endif // HAVE_SINCOS
	
	esa = e*sa;
	eca = e*ca;
	f = x - esa;
	fp = 1.0 - eca;
	dx = -f/fp;
	dx = -f/(fp + 0.5*dx*esa);
	dx = -f/(fp + 0.5*dx*(esa+1.0/3.0*eca*dx));
	x += dx;
	//
	old_E = E;
	E = x + m;
	++count;
	
      } while ((fabs(E-old_E) > 10*(fabs(E)+fabs(M)+twopi)*std::numeric_limits<double>::epsilon()) && (count < max_count));
      
      if (iflag) {
	E = twopi - E;
	old_E = twopi - old_E;
      }
      
      if (count >= max_count) {
	ORSA_WARNING("Orbit::GetE(): max count reached, e = %g    E = %g   fabs(E-old_E) = %g   10*(fabs(E)+fabs(M))*std::numeric_limits<double>::epsilon() = %g",e,E,fabs(E-old_E),10*(fabs(E)+fabs(M))*std::numeric_limits<double>::epsilon());
      }
    }

    if (E == 0.0) {
      ORSA_LOGIC_ERROR("E==0.0 in orsa::Orbit::GetE(); this should never happen.");
    }
    return E;
  }
  
  
  void Orbit::RelativePosVel(Vector &relative_position, Vector &relative_velocity) const {
    
    /////////////////////////////////////////////////////
    // This alghoritm is taken from the swift package, 
    // ORBEL_EL2XV.F file written by M. Duncan.
    /////////////////////////////////////////////////////
    
    // Generate rotation matrices (on p. 42 of Fitzpatrick)
    
    // double sp,cp,so,co,si,ci;
    
#ifdef HAVE_SINCOS
    double s,c;
    
    ::sincos(omega_pericenter,&s,&c);
    const double sp = s;
    const double cp = c;
    
    ::sincos(omega_node,&s,&c);
    const double so = s;
    const double co = c;
    
    ::sincos(i,&s,&c);
    const double si = s;
    const double ci = c;
#else // HAVE_SINCOS
    const double sp = sin(omega_pericenter);
    const double cp = cos(omega_pericenter);
    
    const double so = sin(omega_node);
    const double co = cos(omega_node);
    
    const double si = sin(i);
    const double ci = cos(i);    
#endif // HAVE_SINCOS
    
    const Vector d1(cp*co - sp*so*ci,
                    cp*so + sp*co*ci,
                    sp*si);
    
    const Vector d2(-sp*co - cp*so*ci,
                    -sp*so + cp*co*ci,
                    cp*si);
    
    // Get the other quantities depending on orbit type
    
    // double  cape,scap,ccap,sqe,sqgma,tmp;
    double  cape,tmp;
    double  xfac1,xfac2,vfac1,vfac2;
    
    double  capf,shcap,chcap;
    double  zpara;
    
    if (e < 1.0) {
      
      /* 
	 int count = 0;
	 cape = M;
	 do {
	 tmp  = cape;
	 cape = e * sin(cape) + M;
	 ++count;
	 } while ( (fabs((cape-tmp)/cape) > 1.0e-15) && (count < 100) );
      */
      
      cape = GetE();
      
      // cerr << "tmp: " << tmp << "  cape: " << cape << "  fabs(cape - tmp)" << fabs(cape - tmp) << endl;
      
#ifdef HAVE_SINCOS
      ::sincos(cape,&s,&c);
      const double scap = s;
      const double ccap = c;
#else // HAVE_SINCOS    
      const double scap = sin(cape);
      const double ccap = cos(cape);
#endif // HAVE_SINCOS
      const double sqe = secure_sqrt(1.0 - e*e);
      const double sqgma = secure_sqrt(mu*a);
      
      xfac1 = a*(ccap - e);
      xfac2 = a*sqe*scap;
      // ri = 1/r
      const double ri = 1.0/(a*(1.0 - e*ccap));
      vfac1 = -ri * sqgma * scap;
      vfac2 =  ri * sqgma * ccap * sqe;
      
    } else if (e > 1.0) {
      
      double x,shx,chx,esh,ech;
      double f,fp,fpp,fppp,dx;
      
      // use the 'right' value for M -- NEEDED!
      double local_M = M;
      if (fabs(local_M-twopi) < fabs(local_M)) {
	local_M -= twopi;
      }
      
      // begin with a guess proposed by Danby	
      if (local_M < 0.0) {
	tmp = -2.0*local_M/e + 1.8;
	x = -secure_log(tmp);
      } else {
	tmp = +2.0*local_M/e + 1.8;
	x =  secure_log(tmp);
      }
      
      capf = x;
      
      int count = 0;
      do {
       	x = capf;
	shx = sinh(x);
	chx = cosh(x);
	esh = e*shx;
	ech = e*chx;
	f = esh-x-local_M;
	fp = ech - 1.0; 
	fpp = esh; 
	fppp = ech; 
	dx = -f/fp;
	dx = -f/(fp + dx*fpp/2.0);
	dx = -f/(fp + dx*fpp/2.0 + dx*dx*fppp/6.0);
	capf = x + dx;
     	++count;
      } while ((fabs(dx) > 1.0e-14) && (count < 100));
      
      shcap = sinh(capf);
      chcap = cosh(capf);
      
      const double sqe = secure_sqrt(e*e - 1.0);
      const double sqgma = secure_sqrt(mu*a);
      xfac1 = a*(e - chcap);
      xfac2 = a*sqe*shcap;
      const double ri = 1.0/(a*(e*chcap - 1.0));
      vfac1 = -ri * sqgma * shcap;
      vfac2 =  ri * sqgma * chcap * sqe;
      
    } else { // e = 1.0 within roundoff errors
      
      double q = M;
      if (q < 1.0e-3) {
        zpara = q*(1.0 - (q*q/3.0)*(1.0-q*q));
      } else {
        double x = 0.5*(3.0*q+secure_sqrt(9.0*(q*q)+4.0));
	// double tmp = secure_pow(x,(1.0/3.0));
        double tmp = cbrt(x);
        zpara = tmp - 1.0/tmp;
      }
      
      const double sqgma = secure_sqrt(2.0*mu*a);
      xfac1 = a*(1.0 - zpara*zpara);
      xfac2 = 2.0*a*zpara;
      const double ri = 1.0/(a*(1.0 + zpara*zpara));
      vfac1 = -ri * sqgma * zpara;
      vfac2 =  ri * sqgma;
      
    }
    
    relative_position = d1*xfac1 + d2*xfac2;
    relative_velocity = d1*vfac1 + d2*vfac2;
    
  }
  
  void Orbit::Compute(const Body &b, const Body &ref_b) {
    
    const Vector dr = b.position() - ref_b.position();
    const Vector dv = b.velocity() - ref_b.velocity();
    
    const double mu = GetG()*(b.mass()+ref_b.mass());
    
    Compute(dr,dv,mu);
  }
  
  void Orbit::Compute(const Vector &relative_position, const Vector &relative_velocity, const double mu_in) {
    
    /////////////////////////////////////////////////////
    // This alghoritm is taken from the swift package, 
    // ORBEL_XV2EL.F file written by M. Duncan.
    /////////////////////////////////////////////////////
    
    mu = mu_in;
    
    const double tiny = 1.0e-100; // about 4.0e-15
    
    // internals
    double  face,cape,capf,tmpf,cw,sw,w,u;
    int ialpha = 0;
    
    // angular momentum
    Vector h = ExternalProduct(relative_position,relative_velocity);
    
    double h2 = h.LengthSquared();
    double hh = h.Length();
    
    // inclination
    i = secure_acos(h.z/hh);
    
    // Compute longitude of ascending node omega_node and the argument of latitude u
    // double fac = secure_sqrt(secure_pow(h.x,2)+secure_pow(h.y,2))/h2;
    double fac = secure_sqrt(h.x*h.x+h.y*h.y)/h2;
    
    if (fac < tiny) {
      omega_node = 0.0;
      u = secure_atan2(relative_position.y, relative_position.x);
      if ( fabs(i-pi) < 10.0 * tiny ) u = -u;
    } else {
      omega_node = secure_atan2(h.x,-h.y);
      u = secure_atan2(relative_position.z/sin(i), relative_position.x*cos(omega_node)+relative_position.y*sin(omega_node));
    }
    
    if (omega_node < 0.0) omega_node += twopi;
    if (u < 0.0) u += twopi;
    
    //  Compute the radius r and velocity squared v2, and the dot
    //  product rdotv, the energy per unit mass energy 
    double r  = relative_position.Length();
    double v2 = relative_velocity.LengthSquared();
    
    double vdotr  = relative_position*relative_velocity;
    
    double energy = v2/2.0 - mu/r;
    
    // Determine type of conic section and label it via ialpha
    if (fabs(energy*r/mu) < tiny) {
      ialpha = 0;
    } else {
      if (energy < 0)  ialpha = -1;
      if (energy > 0)  ialpha = +1;
    }
    
    // Depending on the conic type, determine the remaining elements 
    
    // ellipse 
    if (ialpha == -1) {
      
      a   = -mu/(2.0*energy); 
      
      fac = 1.0 - h2/(mu*a); 
      
      if (fac > tiny) {
	e = secure_sqrt(fac);
	face = (a-r)/(a*e);
	
	if (face > 1.0) {
	  cape = 0.0;
	} else {
	  if (face > -1.0)
	    cape = secure_acos(face);
	  else
	    cape = pi;
	}
	
	if (vdotr < 0.0) cape = twopi - cape;
	cw = (cos(cape)-e)/(1.0-e*cos(cape));                  
	sw = secure_sqrt(1.0-e*e)*sin(cape)/(1.0-e*cos(cape));  
	w = secure_atan2(sw,cw);
	if (w < 0.0) w += twopi;
      } else {
	e = 0.0;
	w = u;
	cape = u;
      }
      
      M = cape - e*sin(cape);
      omega_pericenter = u - w;
      if (omega_pericenter < 0) omega_pericenter += twopi;
      omega_pericenter = fmod(omega_pericenter,twopi);
      
    }
    
    // hyperbola
    if (ialpha == 1) {
      a   = mu/(2.0*energy); 
      fac = h2/(mu*a); 
      
      if (fac > tiny) {
	e = secure_sqrt(1.0+fac);
	tmpf = (a+r)/(a*e);
	if (tmpf < 1.0) tmpf = 1.0;
	
	capf = secure_log(tmpf+secure_sqrt(tmpf*tmpf-1.0));
	
	if (vdotr < 0.0) capf = -capf;
	
	cw = (e-cosh(capf))/(e*cosh(capf)-1.0); 
	sw = secure_sqrt(e*e-1.0)*sinh(capf)/(e*cosh(capf)-1.0);
	w = secure_atan2(sw,cw);
	if (w < 0.0) w += twopi;
      } else {
	// we only get here if a hyperbola is essentially a parabola 
	// so we calculate e and w accordingly to avoid singularities
	e = 1.0;
	tmpf = h2/(2.0*mu); 
	w = secure_acos(2.0*tmpf/r - 1.0);
	if (vdotr < 0.0) w = twopi - w;
	tmpf = (a+r)/(a*e);
	capf = secure_log(tmpf+secure_sqrt(tmpf*tmpf-1.0));
      }
      
      M = e * sinh(capf) - capf; 
      omega_pericenter = u - w;
      if (omega_pericenter < 0) omega_pericenter += twopi;
      omega_pericenter = fmod(omega_pericenter,twopi);
    }

    // parabola
    //  NOTE - in this case we use "a" to mean pericentric distance
    if (ialpha == 0) {
      a = 0.5*h2/mu;
      e = 1.0;
      w = secure_acos(2.0*a/r -1.0);
      if (vdotr < 0.0) w = twopi - w;
      tmpf = tan(0.5*w);
      
      M = tmpf*(1.0+tmpf*tmpf/3.0);
      omega_pericenter = u - w;
      if (omega_pericenter < 0) omega_pericenter += twopi;
      omega_pericenter = fmod(omega_pericenter,twopi);
    }
    
  }
  
  /* //! The RMS of the residuals in arcsec units
     double RMS_residuals(vector<Observation> &obs, OrbitWithEpoch &orbit) {
     
     Frame frame;
     
     list<JPL_planets> l;
     l.push_back(SUN);
     l.push_back(MERCURY);
     l.push_back(VENUS);
     l.push_back(EARTH_AND_MOON);
     l.push_back(MARS);
     l.push_back(JUPITER);
     l.push_back(SATURN);
     l.push_back(URANUS);
     l.push_back(NEPTUNE);
     
     SetupSolarSystem(frame,l,orbit.epoch);
     
     Config conf;
     OrsaConfigFile ocf(&conf);
     ocf.Read();
     ocf.Close();
     // vector<Location> loc;
     // LocationFile lf(&loc);
     LocationFile lf;
     lf.SetFileName(conf.paths[OBSCODE]->GetValue().c_str());
     lf.Read();
     lf.Close();
     
     {
     Body b("object",0.0);
     // RelativePosVel(b.position,b.velocity);
     Vector r,v;
     orbit.RelativePosVel(r,v);
     // cerr << "b.position: " << b.position.x << "  " << b.position.y << "  " << b.position.z << endl;
     // add Sun postion and velocity
     r += frame[0].position();
     v += frame[0].velocity();
     // cerr << "b.position: " << b.position.x << "  " << b.position.y << "  " << b.position.z << endl;
     b.SetPosition(r);
     b.SetVelocity(v);
     frame.push_back(b);
     }
     
     const Frame orbit_epoch_frame = frame;
     
     // cerr << "orbit_epoch_frame" << endl;
     // print(orbit_epoch_frame);
     
     UniverseTypeAwareTime obs_utat;
     Radau15 itg; 
     itg.accuracy = 1.0e-10;
     itg.timestep = FromUnits(1,HOUR);
     //
     Newton newton;
     // evol_base.integrator = itg;
     // evol_base.sample_period = FromUnits(3,HOUR);
     // evol_base.SetMaxUnsavedSubSteps(1000);
     Frame  last_frame;
     Vector relative_position;
     Sky    sky;
     double sum_delta_arcsec_squared=0.0, tmp_delta_arcsec;
     
     unsigned int k=0;
     while (k<obs.size()) {
     
     // Evolution *evol = new Evolution(evol_base);
     // Evolution *evol = new Evolution;
     Evolution evol;
     // push back only when debugging..
     // universe->push_back(evol);
     // evol->interaction = new Newton();
     evol.interaction = &newton;
     evol.integrator  = &itg;
     evol.push_back(orbit_epoch_frame);
     evol.SetMaxUnsavedSubSteps(100000);
     obs_utat.SetDate(obs[k].date); 
     // cerr << "integration period: " << FromUnits(orbit_epoch_frame.Time() - obs_utat.GetTime(),DAY,-1) << " days" << endl;
     evol.sample_period = orbit_epoch_frame.Time() - obs_utat.Time(); // important, otherwise the final frame is not saved
     evol.Integrate(obs_utat.GetTime());
     last_frame = evol[evol.size()-1];
     
     Vector geo = last_frame[3].position(); // earth center position
     
     // cerr << "last_frame[3].name() = " << last_frame[3].name() << endl;
     
     geo += lf.ObsPos(obs[k].obscode,obs[k].date);
     
     if (0) {
     unsigned int k=0;
     while (k<last_frame.size()) {
     cerr << "name: " << last_frame[k].name() << "    position: " << last_frame[k].position().x << "  " << last_frame[k].position().y << "  " << last_frame[k].position().z << endl;
     ++k;
     }
     }
     
     // relative_position = last_frame[last_frame.size()-1].position - last_frame[3].position; // object - earth
     relative_position = last_frame[last_frame.size()-1].position() - geo; // object - (earth + observer)
     
     // cerr << "relative_position: " << relative_position.x << "  " << relative_position.y << "  " << relative_position.z << "  " << endl;
     
     sky.Compute(relative_position,obs_utat);
     tmp_delta_arcsec = sky.delta_arcsec(obs[k]);
     // cerr << "obs. " << k << " delta_arcsec: " << tmp_delta_arcsec << endl;
     
     // cerr << "OBS:  R.A. = " << obs[k].ra.GetRad() << "   DEC. = " << obs[k].dec.GetRad() << endl;
     // cerr << "OBJ:  R.A. = " << sky.ra().GetRad()  << "   DEC. = " << sky.dec().GetRad()  << endl;
     
     // cerr << obs[k].ra.GetRad() << "  " << obs[k].dec.GetRad() << "  " << sky.ra().GetRad() << "  " << sky.dec().GetRad() << endl;
     
     sum_delta_arcsec_squared += secure_pow(tmp_delta_arcsec,2);
     
     // cerr << "RMS: adding " << tmp_delta_arcsec << endl;
     
     ++k;
     }
     
     return secure_sqrt(sum_delta_arcsec_squared/obs.size()); 
     }
  */
  
  //
  
  //! The RMS of the residuals in arcsec units
  double RMS_residuals(const vector<Observation> &obs, const OrbitWithEpoch &orbit) {
    
    double sum_delta_arcsec_squared=0.0;
    unsigned int k=0;
    Sky sky;
    OptimizedOrbitPositions opt(orbit);
    while (k<obs.size()) {
      // sky = PropagatedSky(orbit,obs[k].date,obs[k].obscode);
      sky = opt.PropagatedSky_J2000(obs[k].date,obs[k].obscode);
      // sum_delta_arcsec_squared += secure_pow(PropagatedSky(orbit,obs[k].date,obs[k].obscode).delta_arcsec(obs[k]),2);
      // sum_delta_arcsec_squared += secure_pow(sky.delta_arcsec(obs[k]),2);
      const double delta_arcsec = sky.delta_arcsec(obs[k]);
      sum_delta_arcsec_squared += delta_arcsec*delta_arcsec;
      //
      /* if (k>0) {
	 fprintf(stderr,"T-%i-%i: %g days\n",k,k-1,obs[k].date.GetJulian()-obs[k-1].date.GetJulian());
	 }
      */
      
      // debug
      /*
	printf("%16.12f %16.12f\n",obs[k].ra.GetRad(),obs[k].dec.GetRad());
	printf("%16.12f %16.12f\n",sky.ra().GetRad(),sky.dec().GetRad());
      */
      //
      
      ++k;
    }
    
    return (secure_sqrt(sum_delta_arcsec_squared/obs.size())); 
  }
  
  double residual(const Observation & obs, const OrbitWithEpoch & orbit) {
    OptimizedOrbitPositions opt(orbit);
    Sky sky = opt.PropagatedSky_J2000(obs.date,obs.obscode);
    return fabs(sky.delta_arcsec(obs));
  }
  
  //
  
  // Sky PropagatedSky(const OrbitWithEpoch &orbit, const UniverseTypeAwareTime &final_time, const std::string &obscode, bool integrate) {
  // Sky PropagatedSky_J2000(const OrbitWithEpoch &orbit, const UniverseTypeAwareTime &final_time, const std::string &obscode, bool integrate) {
  Sky PropagatedSky_J2000(const OrbitWithEpoch & orbit, const UniverseTypeAwareTime & final_time, const std::string & obscode, const bool integrate, const bool light_time_corrections) {
    
    if (!integrate) {
      
      list<JPL_planets> l;
      //
      l.push_back(SUN);
      l.push_back(EARTH);
      
      Frame frame;
      
      SetupSolarSystem(frame,l,final_time);
      
      /* 
	 LocationFile lf;
	 lf.SetFileName(config->paths[OBSCODE]->GetValue().c_str());
	 lf.Read();
	 lf.Close();
      */
      
      Vector geo = frame[1].position();
      // geo += lf.ObsPos(obscode,final_time.GetDate());
      geo += location_file->ObsPos(obscode,final_time.GetDate());
      
      Vector r,v;
      orbit.RelativePosVel(r,v,final_time);
      //
      r += frame[0].position();
      v += frame[0].velocity();
      
      const Vector relative_position = r - geo;
      
      Sky sky;
      
      if (light_time_corrections) {
	sky.Compute_J2000(relative_position-v*relative_position.Length()/GetC());
      } else {
	sky.Compute_J2000(relative_position);
      }
      
      return sky;
    }
    
    Frame frame;
    
    list<JPL_planets> l;
    //
    l.push_back(SUN);
    l.push_back(MERCURY);
    l.push_back(VENUS);
    l.push_back(EARTH_AND_MOON);   
    l.push_back(MARS);
    l.push_back(JUPITER);
    l.push_back(SATURN);
    l.push_back(URANUS);
    l.push_back(NEPTUNE);  
    
    SetupSolarSystem(frame,l,orbit.epoch);
    
    /* 
       Config conf;
       OrsaConfigFile ocf(&conf);
       ocf.Read();
       ocf.Close();
    */
    
    /* 
       LocationFile lf;
       lf.SetFileName(config->paths[OBSCODE]->GetValue().c_str());
       lf.Read();
       lf.Close();
    */
    
    {
      Body b("object",0.0);
      // RelativePosVel(b.position,b.velocity);
      Vector r,v;
      orbit.RelativePosVel(r,v);
      //
      // cerr << "PropagatedSky ---> RelativePosVel ---> r: " << Length(r) << endl;
      //
      // cerr << "b.position: " << b.position.x << "  " << b.position.y << "  " << b.position.z << endl;
      // add Sun postion and velocity
      r += frame[0].position();
      v += frame[0].velocity();
      // cerr << "b.position: " << b.position.x << "  " << b.position.y << "  " << b.position.z << endl;
      b.SetPosition(r);
      b.SetVelocity(v);
      frame.push_back(b);
      
      // debug
      // cerr << "object r: " << Length(r) << endl; 
    }
    
    const Frame orbit_epoch_frame = frame;
    
    UniverseTypeAwareTime obs_utat;
    
    Radau15 itg; 
    // itg.accuracy = 1.0e-12;
    itg.accuracy = 1.0e-12;
    itg.timestep = FromUnits(1.0,DAY);
    //
    Newton newton;
    
    // Frame  last_frame;
    // Vector relative_position;
    // Vector v;
    
    Evolution evol;
    
    // evol.interaction = &newton;
    evol.SetInteraction(&newton);
    // evol.interaction = &itr;
    // evol.integrator  = &itg;
    evol.SetIntegrator(&itg);
    evol.push_back(orbit_epoch_frame);
    evol.SetMaxUnsavedSubSteps(100000);
    obs_utat.SetDate(final_time.GetDate()); 
    // evol.sample_period = orbit_epoch_frame.Time() - obs_utat.Time(); // important, otherwise the final frame is not saved
    // evol.sample_period = (orbit_epoch_frame.Time() - obs_utat.Time())*(1.0-1.0e-8); // important, otherwise the final frame is not saved
    // evol.sample_period = (orbit_epoch_frame.Time() - obs_utat.Time());
    evol.SetSamplePeriod(orbit_epoch_frame - obs_utat);
    evol.Integrate(obs_utat.GetTime(),true);
    Frame last_frame = evol[evol.size()-1];
    
    // cerr << "sample_period: " << evol.sample_period << endl;
    // fprintf(stderr,"final time JD: %20.8f   last frame JD: %20.8f\n",final_time.GetDate().GetJulian(),last_frame.GetDate().GetJulian());
    
    // cerr << "..is Sun? -----> " << frame[0].name() << endl;
    // cerr << "..is Earth?  --> " << frame[3].name() << endl;
    //
    Vector geo = last_frame[3].position()-last_frame[0].position(); // earth center position
    // Vector geo = frame[3].position; // earth center position
    
    // IMPORTANT!!!
    geo += location_file->ObsPos(obscode,final_time.GetDate());
    
    // cerr << "obs. pos. length: " << Length(lf.ObsPos(obscode,final_time.GetDate())) << "  " << LengthLabel() << endl;
    // cerr << "obs. pos. length: " << FromUnits(Length(lf.ObsPos(obscode,final_time.GetDate())),REARTH,-1) << "  " << units.label(REARTH) << endl;
    
    // cerr << "Length(geo): " << Length(geo) << endl; 
    
    const Vector relative_position = last_frame[last_frame.size()-1].position() - last_frame[0].position() - geo; // object - (earth + observer)
    const Vector v                 = last_frame[last_frame.size()-1].velocity();
    
    // cerr << "relative_position: " << relative_position.x << "  " << relative_position.y << "  " << relative_position.z << "  " << endl;
    
    // cerr << "relative distance: " << Length(relative_position) << "  " << LengthLabel() << endl;
    
    Sky sky;
    // sky.Compute(relative_position,obs_utat);
    // sky.Compute(relative_position,last_frame);
    // sky.Compute_J2000(relative_position);
    //      
    if (light_time_corrections) {
      sky.Compute_J2000(relative_position-v*relative_position.Length()/GetC());
    } else {
      sky.Compute_J2000(relative_position);
    }
    
    // fprintf(stderr,"propagated sky:   ra = %16.12f    dec = %16.12f   orbit JD: %14.5f   final time JD: %14.5f\n",sky.ra().GetRad(),sky.dec().GetRad(),orbit.epoch.GetDate().GetJulian(),final_time.GetDate().GetJulian());
    
    // debug test
    /* 
       {
       
       // random number generator
       // const int random_seed = 124323;
       struct timeval  tv;
       struct timezone tz;
       gettimeofday(&tv,&tz); 
       const int random_seed = tv.tv_usec;
       gsl_rng *rnd;
       rnd = gsl_rng_alloc(gsl_rng_gfsr4);
       gsl_rng_set(rnd,random_seed);
       
       Vector geo,rand;
       int oc;
       Sky sky;
       // double h,m,s,d,p;
       for (oc=1;oc<400;++oc) {
       // geo = last_frame[3].position + lf.ObsPos(oc,final_time.GetDate());
       rand.Set(gsl_rng_uniform(rnd)*2-1,
       gsl_rng_uniform(rnd)*2-1,
       gsl_rng_uniform(rnd)*2-1);
       rand /= Length(rand);
       rand *= FromUnits(1.0,REARTH);
       geo = last_frame[3].position+rand;
       relative_position = last_frame[last_frame.size()-1].position - geo;
       sky.Compute(relative_position,obs_utat);
       // sky.ra().GetHMS(h,m,s);
       // printf("R.A.:   %2f %2f %10.4f    ",h,m,s);
       // sky.dec().GetDPS(d,p,s);
       // printf("dec.:   %2f %2f %10.4f\n",d,p,s);
       printf("%20.12f  %20.12f\n",sky.ra().GetRad(),sky.dec().GetRad());
       }
       
       gsl_rng_free(rnd);      
       }
    */
    
    return sky;
  }
  
  //
  
  OptimizedOrbitPositions::OptimizedOrbitPositions(const OrbitWithEpoch &orbit) : _orbit(orbit) {
    
    l.push_back(SUN);
    l.push_back(MERCURY);
    l.push_back(VENUS);
    l.push_back(EARTH_AND_MOON);
    l.push_back(MARS);
    l.push_back(JUPITER);
    l.push_back(SATURN);
    l.push_back(URANUS);
    l.push_back(NEPTUNE);   
    
    frames.clear();
  }
  
  OrbitWithEpoch OptimizedOrbitPositions::PropagatedOrbit(const UniverseTypeAwareTime & final_time, const bool integrate) {
    
    std::sort(frames.begin(),frames.end());
    
    /* 
       for (unsigned int k=0;k<frames.size();++k) {
       ORSA_ERROR("OptimizedOrbitPositions::PropagatedOrbit(...)  ==> frame[%i] time: %f   dt: %f",k,frames[k].GetTime(),(frames[k]-final_time).absolute().GetDouble());
       }
    */
    
    if (!integrate) {
      OrbitWithEpoch oe(_orbit);
      oe.M += twopi*(final_time.Time() - _orbit.epoch.Time())/(_orbit.Period());
      oe.M  = fmod(10*twopi+fmod(oe.M,twopi),twopi);
      oe.epoch = final_time;
      return (oe);
    }
    
    Frame start_frame;
    
    if (frames.size() > 0) {
      
      if (final_time <= frames[0]) {
	start_frame = frames[0];
      	// ORSA_ERROR("OptimizedOrbitPositions::PropagatedOrbit(...)  ==> starting from a saved frame...");
      } else if (final_time >= frames[frames.size()-1]) {
	start_frame = frames[frames.size()-1];	
       	// ORSA_ERROR("OptimizedOrbitPositions::PropagatedOrbit(...)  ==> starting from a saved frame...");
      } else {
	unsigned int k=1;
	while (k < frames.size()) {
	  
	  if ( (final_time >= frames[k-1]) && (final_time <= frames[k]) ) {
	    if ((final_time-frames[k-1]).absolute() < (frames[k]-final_time).absolute()) {
	      start_frame = frames[k-1];
	      // ORSA_ERROR("OptimizedOrbitPositions::PropagatedOrbit(...)  ==> starting from a saved frame...");
	      break;
	    } else {
	      start_frame = frames[k];
	      // ORSA_ERROR("OptimizedOrbitPositions::PropagatedOrbit(...)  ==> starting from a saved frame...");
	      break;
	    }
	  }
	  
	  ++k;
	}
      }
      
    } else {
      
      // ORSA_ERROR("OptimizedOrbitPositions::PropagatedOrbit(...)  ==> first call...");
      
      SetupSolarSystem(start_frame,l,_orbit.epoch);
      // start_frame.SetDate(_orbit.epoch);
      
      const JPLBody Sun(SUN,_orbit.epoch);
      
      Body b("object");
      Vector r, v;
      _orbit.RelativePosVel(r,v);
      r += Sun.position();
      v += Sun.velocity();
      // cerr << "b.position: " << b.position.x << "  " << b.position.y << "  " << b.position.z << endl;
      b.SetPosition(r);
      b.SetVelocity(v);
      
      start_frame.push_back(b);
      
      frames.push_back(start_frame);
    }
    
    Radau15 itg; 
    itg.accuracy = 1.0e-12;
    itg.timestep = FromUnits(1.0,DAY);
    
    Evolution evol;
    evol.SetInteraction(NEWTON);
    evol.SetIntegrator(&itg);
    evol.push_back(start_frame);
    evol.SetMaxUnsavedSubSteps(100000);
    // obs_utat.SetDate(final_time.GetDate()); 
    // evol.sample_period = orbit_epoch_frame.Time() - obs_utat.Time(); // important, otherwise the final frame is not saved
    // evol.sample_period = (orbit_epoch_frame.Time() - obs_utat.Time())*(1.0-1.0e-8); // important, otherwise the final frame is not saved
    // evol.sample_period = (start_frame.Time() - obs_utat.Time())*(1.0-1.0e-8); // important, otherwise the final frame is not saved
    // evol.sample_period = (start_frame.Time() - obs_utat.Time());
    // evol.sample_period = start_frame - final_time;
    evol.SetSamplePeriod(final_time - start_frame);
    evol.Integrate(final_time,true);
    
    Frame last_frame = evol[evol.size()-1];
    
    // don't add the frame if the size of evol is 1,
    // because this means that the integration was not needed,
    // and the 'frames' already contains it.
    if (evol.size() > 1) frames.push_back(last_frame);
    
    const Vector relative_position = last_frame[last_frame.size()-1].position() - last_frame[0].position();
    const Vector relative_velocity = last_frame[last_frame.size()-1].velocity() - last_frame[0].velocity();
    
    OrbitWithEpoch oe;
    oe.Compute(relative_position,relative_velocity,GetG()*GetMSun(),final_time);
    
    return (oe);
  }
  
  Sky OptimizedOrbitPositions::PropagatedSky_J2000(const UniverseTypeAwareTime &final_time, const string &obscode, const bool integrate, const bool light_time_corrections) {
    
    OrbitWithEpoch oe = PropagatedOrbit(final_time,integrate);
    
    // Frame frame;
    // SetupSolarSystem(frame,l,final_time);
    
    /* 
       Config conf;
       OrsaConfigFile ocf(&conf);
       ocf.Read();
       ocf.Close();
    */
    
    /* 
       LocationFile lf;
       lf.SetFileName(config->paths[OBSCODE]->GetValue().c_str());
       lf.Read();
       lf.Close();
    */
    
    JPLBody Sun(SUN,final_time.GetDate());
    JPLBody Earth(EARTH,final_time.GetDate());
    
    // Vector geo = frame[3].position()-frame[0].position();
    Vector geo = Earth.position();
    // geo += lf.ObsPos(obscode,final_time.GetDate());
    geo += location_file->ObsPos(obscode,final_time.GetDate());
    
    Vector r,v;
    oe.RelativePosVel(r,v,final_time);
    r += Sun.position();
    v += Sun.velocity();
    
    Vector relative_position = r - geo;
    
    Sky sky;
    // sky.Compute(relative_position,final_time);
    // sky.Compute_J2000(relative_position);
    // 
    if (light_time_corrections) {
      sky.Compute_J2000(relative_position-v*relative_position.Length()/GetC());
    } else {
      sky.Compute_J2000(relative_position);
    }
    
    return sky;
  }
  
  /* 
     Sky OptimizedOrbitPositions::PropagatedSky(const UniverseTypeAwareTime &final_time, const string &obscode, bool integrate) {
     
     sort(frames.begin(),frames.end());
     
     if (!integrate) {
     
     Vector r,v;
     _orbit.RelativePosVel(r,v,final_time);
     
     Frame frame;
     
     SetupSolarSystem(frame,l,final_time);
     
     Config conf;
     OrsaConfigFile ocf(&conf);
     ocf.Read();
     ocf.Close();
     
     LocationFile lf;
     lf.SetFileName(conf.paths[OBSCODE]->GetValue().c_str());
     lf.Read();
     lf.Close();
     
     Vector geo = frame[3].position()-frame[0].position();
     geo += lf.ObsPos(obscode,final_time.GetDate());
     
     Vector relative_position = r - geo;
     
     Sky sky;
     sky.Compute(relative_position,final_time);
     
     return sky;
     }
     
     Frame start_frame;
     
     if (frames.size()>1) {
     
     if (final_time.GetTime() < frames[0].GetTime()) {
     start_frame = frames[0];
     // cerr << "starting from a saved frame..." << endl;
     } else if (final_time.GetTime() > frames[frames.size()-1].GetTime()) {
     start_frame = frames[frames.size()-1];	
     // cerr << "starting from a saved frame..." << endl;
     } else {
     unsigned int k=1;
     while (k<frames.size()) {
     if ( (final_time.GetTime() > frames[k-1].GetTime()) &&
     (final_time.GetTime() < frames[k].GetTime()) ) {
     if ((final_time.GetTime()-frames[k-1].GetTime()) < (frames[k].GetTime()-final_time.GetTime())) {
     start_frame = frames[k-1];
     // cerr << "starting from a saved frame..." << endl;
     break;
     } else {
     start_frame = frames[k];
     // cerr << "starting from a saved frame..." << endl;
     break;
     }
     }
     ++k;
     }
     }    
     
     } else {
     
     // cerr << "don't using a saved frame..." << endl;
     
     SetupSolarSystem(start_frame,l,_orbit.epoch);
     
     Body b("object",0.0);
     Vector r,v;
     _orbit.RelativePosVel(r,v);
     r += start_frame[0].position();
     v += start_frame[0].velocity();
     // cerr << "b.position: " << b.position.x << "  " << b.position.y << "  " << b.position.z << endl;
     b.SetPosition(r);
     b.SetVelocity(v);
     start_frame.push_back(b);
     
     frames.push_back(start_frame);
     }
     
     UniverseTypeAwareTime obs_utat;
     
     Radau15 itg; 
     itg.accuracy = 1.0e-12;
     itg.timestep = FromUnits(1,MINUTE);
     //
     Newton newton;
     
     Frame  last_frame;
     Vector relative_position;
     
     Evolution evol;
     
     evol.interaction = &newton;
     // evol.interaction = &itr;
     evol.integrator  = &itg;
     evol.push_back(start_frame);
     evol.SetMaxUnsavedSubSteps(100000);
     obs_utat.SetDate(final_time.GetDate()); 
     // evol.sample_period = orbit_epoch_frame.Time() - obs_utat.Time(); // important, otherwise the final frame is not saved
     // evol.sample_period = (orbit_epoch_frame.Time() - obs_utat.Time())*(1.0-1.0e-8); // important, otherwise the final frame is not saved
     evol.sample_period = (start_frame.Time() - obs_utat.Time())*(1.0-1.0e-8); // important, otherwise the final frame is not saved
     evol.Integrate(obs_utat.GetTime());
     last_frame = evol[evol.size()-1];
     
     frames.push_back(last_frame);
     
     Vector geo = last_frame[3].position()-last_frame[0].position(); // earth center position
     // Vector geo = frame[3].position; // earth center position
     
     Config conf;
     OrsaConfigFile ocf(&conf);
     ocf.Read();
     ocf.Close();
     
     LocationFile lf;
     lf.SetFileName(conf.paths[OBSCODE]->GetValue().c_str());
     lf.Read();
     lf.Close();
     
     // IMPORTANT!!!
     geo += lf.ObsPos(obscode,final_time.GetDate());
     
     relative_position = last_frame[last_frame.size()-1].position() - last_frame[0].position() - geo; // object - (earth + observer)
     
     Sky sky;
     // sky.Compute(relative_position,obs_utat);
     sky.Compute(relative_position,last_frame);
     
     return sky;
     }
  */
  
  // 
  
  void OptimizedOrbitPositions::PropagatedPosVel(const UniverseTypeAwareTime &final_time, Vector &position, Vector &velocity, bool integrate) {
    OrbitWithEpoch oe = PropagatedOrbit(final_time,integrate);
    oe.RelativePosVel(position,velocity,final_time);
  }
  
  //
  
  /* 
     // OrbitWithCovarianceMatrix
     
     OrbitWithCovarianceMatrix::OrbitWithCovarianceMatrix(Orbit &nominal_orbit, double **covariance_matrix, CovarianceMatrixElements base) : OrbitWithEpoch(nominal_orbit), cov_base(base) {
     memcpy((void*)covm, (const void*)covariance_matrix, 6*6*sizeof(double)); // works?
     }
  
     void OrbitWithCovarianceMatrix::generate(vector<Orbit> &list, const int num) {
     
     list.clear();
     
     if (1) {
     // correlation matrix
     int i,k;
     for (i=0;i<6;++i) {
     for (k=0;k<6;++k) {
     // fprintf(stderr,"correlation[%i][%i]= %g\n",i,k,covm[i][k]/secure_sqrt(covm[i][i]*covm[k][k]));
     fprintf(stderr,"correlation[%i][%i]= %20.14f\n",i,k,covm[i][k]/secure_sqrt(covm[i][i]*covm[k][k]));
     }
     }
     }
     
     const size_t n=6;
     
     double meanv[n],parm[n*(n+3)/2+1],x[n],work[n][n];
     
     double *meanv_p = &meanv[0];
     double *covm_p  =  &covm[0][0];
     double *parm_p  =  &parm[0];
     double *x_p     =     &x[0];
     double *work_p  =  &work[0][0];
     
     switch (cov_base) {
     case Osculating: {
     meanv[0] = a;
     meanv[1] = e;
     meanv[2] = i;
     meanv[3] = omega_node;
     meanv[4] = omega_pericenter;
     meanv[5] = M;
     break;
     }
     case Equinoctal: {
     meanv[0] = a;
     meanv[1] = e*sin(omega_node+omega_pericenter);
     meanv[2] = e*cos(omega_node+omega_pericenter);
     meanv[3] = tan(i/2)*sin(omega_node);
     meanv[4] = tan(i/2)*cos(omega_node);
     meanv[5] = fmod(omega_node+omega_pericenter+M,twopi);
     break;
     }
     }
     
     // init
     setgmn(meanv_p,covm_p,n,parm_p);
     
     Orbit gen_orb = *this;
     int generated=0;
     
    switch (cov_base) {
    case Osculating: {
    while (1) {
    genmn(parm_p,x_p,work_p);
    ++generated;
    
    gen_orb.a                 = x[0];
    gen_orb.e                 = x[1];
    gen_orb.i                 = x[2];
    gen_orb.omega_node        = x[3];
    gen_orb.omega_pericenter  = x[4];
    gen_orb.M                 = x[5];
    
    list.push_back(gen_orb);
    
    if (generated==num) break;
    }
    break;
    }
    case Equinoctal: {
    double omega_tilde;
    while (1) {
    genmn(parm_p,x_p,work_p);
    ++generated;
    
    omega_tilde               = secure_atan2(x[1],x[2]);
    
    gen_orb.a                 = x[0];
    gen_orb.e                 = secure_sqrt(x[1]*x[1]+x[2]*x[2]);
    gen_orb.i                 = 2*atan(secure_sqrt(x[3]*x[3]+x[4]*x[4]));
    gen_orb.omega_node        = fmod(10*twopi+secure_atan2(x[3],x[4]),twopi); // works?
    gen_orb.omega_pericenter  = fmod(10*twopi+omega_tilde-omega_node,twopi);
    gen_orb.M                 = fmod(10*twopi+x[5]-omega_tilde,twopi);
    
    list.push_back(gen_orb);
    
    if (generated==num) break;
    }
    break;
    }
    }
    }
  */
  
  // Sky
  
  /* 
     void Sky::Compute(const Vector &relative_position, const UniverseTypeAwareTime& epoch) {
     // NOTE: relative_position = object - observer
     
     // ra.SetRad(fmod(secure_atan2(relative_position.y,relative_position.x)+twopi,twopi));
     // dec.SetRad(pi/2 - secure_acos(relative_position.z/Length(relative_position)));
     
     switch (universe->GetReferenceSystem()) {
     case ECLIPTIC: {
     Vector r = relative_position;
     Angle obl = obleq(epoch);
     r.rotate(0,obl.GetRad(),0);
     _ra.SetRad(fmod(secure_atan2(r.y,r.x)+twopi,twopi));
     _dec.SetRad(pi/2 - secure_acos(r.z/r.Length()));
     break;
     }
     case EQUATORIAL: {
     _ra.SetRad(fmod(secure_atan2(relative_position.y,relative_position.x)+twopi,twopi));
     _dec.SetRad(pi/2 - secure_acos(relative_position.z/relative_position.Length()));
     break;
     }
     }
     }
  */
  
  void Sky::Compute_J2000(const Vector &relative_position) {
    // NOTE: relative_position = object - observer
    switch (universe->GetReferenceSystem()) {
    case ECLIPTIC: {
      Vector r = relative_position;
      // Angle obl = obleq_J2000();
      // r.rotate(0,obl.GetRad(),0);
      EclipticToEquatorial_J2000(r);
      _ra.SetRad(fmod(secure_atan2(r.y,r.x)+twopi,twopi));
      _dec.SetRad(pi/2 - secure_acos(r.z/r.Length()));
      break;
    }
    case EQUATORIAL: {
      _ra.SetRad(fmod(secure_atan2(relative_position.y,relative_position.x)+twopi,twopi));
      _dec.SetRad(pi/2 - secure_acos(relative_position.z/relative_position.Length()));
      break;
    }
    }
  }
  
  static Vector unit_vector(const Angle & ra, const Angle & dec) {
    double sin_ra, cos_ra;
    sincos(ra,sin_ra,cos_ra);
    //
    double sin_dec, cos_dec;
    sincos(dec,sin_dec,cos_dec);
    //
    return Vector(cos_dec*sin_ra,
		  cos_dec*cos_ra,
		  sin_dec);
  }
  
  double Sky::delta_arcsec(const Observation & obs) const {
    return ((180/pi)*3600.0*acos(unit_vector(obs.ra,obs.dec)*unit_vector(_ra,_dec)));
  }
  
  /* 
     double Sky::delta_arcsec(const Observation & obs) const {
     
     // return (180/pi)*3600*(secure_sqrt(secure_pow(((obs.ra.GetRad()-_ra.GetRad())*cos(obs.dec.GetRad())),2)+secure_pow((obs.dec.GetRad()-_dec.GetRad()),2)));
     
     // fprintf(stderr,"SKY ra: %16.10f    dec: %16.10f\n",ra().GetRad(),dec().GetRad());
     // fprintf(stderr,"OBS ra: %16.10f    dec: %16.10f\n",obs.ra.GetRad(),obs.dec.GetRad());
     
     double d_ra = fabs(obs.ra.GetRad()-_ra.GetRad());
     if (fabs(obs.ra.GetRad()-_ra.GetRad()+twopi) < d_ra) d_ra = fabs(obs.ra.GetRad()-_ra.GetRad()+twopi);
     if (fabs(obs.ra.GetRad()-_ra.GetRad()-twopi) < d_ra) d_ra = fabs(obs.ra.GetRad()-_ra.GetRad()-twopi);
     
     double d_dec = fabs(obs.dec.GetRad()-_dec.GetRad());
     
     // const double delta = (180/pi)*3600.0*secure_sqrt(secure_pow(d_ra*cos(obs.dec.GetRad()),2)+secure_pow(d_dec,2));
     const double delta = (180/pi)*3600.0*secure_sqrt((d_ra*cos(obs.dec.GetRad()))*(d_ra*cos(obs.dec.GetRad()))+d_dec*d_dec);
     
     // cerr << "delta_arcsec: " << delta << endl;
     
     // return (180/pi)*3600.0*secure_sqrt(secure_pow(d_ra*cos(obs.dec.GetRad()),2)+secure_pow(d_dec,2));
     return delta;
     }
  */
  
} // namespace orsa
