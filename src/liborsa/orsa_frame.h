/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _ORSA_FRAME_H_
#define _ORSA_FRAME_H_

#include <vector>
#include <list>

#include "orsa_body.h"
#include "orsa_common.h"

namespace orsa {
  
  class Frame : public std::vector<Body>, public UniverseTypeAwareTime {
  public:
    Frame();
    Frame(const UniverseTypeAwareTime &);
    Frame(const Frame &);
    virtual ~Frame() { }
    
  public:
    virtual unsigned int size() const { return std::vector<Body>::size(); }
    
    // used to sort frames for increasing time 
    bool operator < (const Frame &) const;
    
  public:	
    void ForceJPLEphemerisData();
    
  public:
    Vector CenterOfMass() const;
    Vector CenterOfMassVelocity() const;
    Vector Barycenter() const;
    Vector BarycenterVelocity() const;
    Vector RelativisticBarycenter() const;
    Vector RelativisticBarycenterVelocity() const;
    Vector AngularMomentum(const Vector&) const;
    Vector BarycentricAngularMomentum() const;
  };
  
  void print(const Frame&);
  
  double KineticEnergy(const std::vector<Body>&);
  
  Vector CenterOfMass(const std::vector<Body>&);
  Vector CenterOfMassVelocity(const std::vector<Body>&);
  Vector Barycenter(const std::vector<Body>&);
  Vector BarycenterVelocity(const std::vector<Body>&);
  Vector RelativisticBarycenter(const std::vector<Body>&);
  Vector RelativisticBarycenterVelocity(const std::vector<Body>&);
  Vector AngularMomentum(const std::vector<Body>&, const Vector&);
  Vector BarycentricAngularMomentum(const std::vector<Body>&);
  
} // namespace orsa

#endif // _ORSA_FRAME_H_
