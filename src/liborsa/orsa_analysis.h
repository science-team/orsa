/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _ORSA_ANALYSIS_H_
#define _ORSA_ANALYSIS_H_

#include <string>

#include "orsa_orbit.h"

namespace orsa {
  
  class WindowParameters {
  public:
    WindowParameters();
    
  public:
    double window_amplitude;
    double window_start;
    double window_step;
  };
  
  class OrbitStream : public std::vector<OrbitWithEpoch> {
  public:
    int               asteroid_number;  
    double            timestep;
    WindowParameters  wp;    
    std::string       label; // used in output file names
  };
  
  class Analysis { 
  public:
    virtual ~Analysis() {};    
  };
  
  // derived classes
  
  // Lyapunov
  class Lyapunov : public Analysis {};
  
  // Mean Motion Resonance
  class MeanMotionResonance : public Analysis {};
  
  // Poincare surface of section
  class PoincareSurfaceOfSection : public Analysis {};
  
} // namespace orsa

#endif // _ORSA_ANALYSIS_H_

