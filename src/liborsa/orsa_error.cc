/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_error.h"

#include <cstdio>
#include <cassert>

namespace orsa {

  using namespace std;

  void Debug::construct()
  {
    if (!m_instance) m_instance = new Debug;
  }
  Debug::Debug() :
    doTrace(false),
    doDefaultOutput(false)
  { }
  Debug::~Debug()
  {
    m_instance = 0;
  }
  
  Debug * Debug::obj() {
    assert(m_instance != 0);
    return m_instance;
  }
  void Debug::setDefaultOutput(bool d) {
    obj()->doDefaultOutput = d;
  }
  void Debug::set(const char *msg, const char *file, const int line) {
    fprintf(stderr, "ORSA[%s:%i] %s ", file, line, msg);
    doTrace = true;
  }
  void Debug::trace(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    if (doTrace) vtrace(fmt, ap);
    doTrace = false;
  }
  void Debug::vtrace(const char *fmt, va_list ap) {
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
  }
  
  Debug * Debug::m_instance = 0;
  
} // namespace orsa

