/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _ORSA_ERROR_H_
#define _ORSA_ERROR_H_

#include <cstdarg>

namespace orsa {
  
  class Debug {
  public:
    static void construct();
    virtual ~Debug();
    virtual void set(const char *msg, const char *file, const int line);
    void trace(const char *fmt, ...); // do not make this one virtual
    static void setDefaultOutput(bool);
    static Debug * obj();
  protected:
    Debug();
    virtual void vtrace(const char *fmt, std::va_list list);   
    static Debug * m_instance;
    bool doTrace;
    bool doDefaultOutput;
  };

} // namespace orsa

// Note: do not change those to variadic macros. Believe it or not, some
// compilers just won't dig it. Besides, the code below *does* work properly
// when used like
// if (condition) ORSA_DEBUG("blublah");
#define ORSA_DEBUG          orsa::Debug::obj()->set("Debug:",__FILE__,__LINE__); orsa::Debug::obj()->trace
#define ORSA_ERROR          orsa::Debug::obj()->set("Error:",__FILE__,__LINE__); orsa::Debug::obj()->trace
#define ORSA_WARNING        orsa::Debug::obj()->set("Warning:",__FILE__,__LINE__); orsa::Debug::obj()->trace
#define ORSA_DOMAIN_ERROR   orsa::Debug::obj()->set("Domain Error:",__FILE__,__LINE__); orsa::Debug::obj()->trace
#define ORSA_LOGIC_ERROR    orsa::Debug::obj()->set("Logic Error (this shouldn't happen):",__FILE__,__LINE__); orsa::Debug::obj()->trace
#define ORSA_LOGIC_WARNING  orsa::Debug::obj()->set("Logic Warning (possibly unimplemented case):",__FILE__,__LINE__); orsa::Debug::obj()->trace
  
#endif // _ORSA_ERROR_H_
