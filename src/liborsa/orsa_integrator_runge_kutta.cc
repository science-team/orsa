/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <iostream>

#include "orsa_integrator.h"
#include "orsa_common.h"

using namespace std;

namespace orsa {
  
  RungeKutta::RungeKutta() : FixedTimestepIntegrator() {
    type = RUNGEKUTTA;
  }
  
  RungeKutta::RungeKutta(const RungeKutta & i) : FixedTimestepIntegrator() {
    type     = i.type;
    timestep = i.timestep;
    accuracy = i.accuracy;
    // m        = i.m;
  }
  
  RungeKutta::~RungeKutta() {
    
  }
  
  Integrator * RungeKutta::clone() const {
    return new RungeKutta(*this);
  }
  
  void RungeKutta::Step(const Frame & frame_in, Frame & frame_out, Interaction * interaction) {
    
    // NON-DISSIPATIVE (velocity indipendent) version
    
    const unsigned int n =  frame_in.size();
    
    const double h = timestep.GetDouble();
    
    vector<Vector> acc(n);
    
    vector<Vector> b1(n), b2(n), b3(n), b4(n);
    
    unsigned int j;
    
    interaction->Acceleration(frame_in,acc);
    //
    b1 = acc;
    
    Frame tmp_frame = frame_in;
    for (j=0;j<tmp_frame.size();++j) {
      tmp_frame[j].AddToPosition(frame_in[j].velocity() * h / 2.0);
    }
    //
    if (interaction->IsSkippingJPLPlanets()) {
      tmp_frame.SetTime(frame_in + h / 2.0);
      tmp_frame.ForceJPLEphemerisData();
    }
    //
    interaction->Acceleration(tmp_frame,acc);
    //
    b2 = acc;
    
    tmp_frame = frame_in;
    for (j=0;j<tmp_frame.size();++j) {
      tmp_frame[j].AddToPosition(frame_in[j].velocity() * h / 2.0 + b1[j] * h * h / 4.0);
    }
    //
    if (interaction->IsSkippingJPLPlanets()) {
      tmp_frame.SetTime(frame_in + h / 2.0);
      tmp_frame.ForceJPLEphemerisData();
    }
    //
    interaction->Acceleration(tmp_frame,acc);
    //
    b3 = acc;
    
    tmp_frame = frame_in;
    for (j=0;j<tmp_frame.size();++j) {
      tmp_frame[j].AddToPosition(frame_in[j].velocity() * h + b2[j] * h * h / 2.0);
    }
    //
    if (interaction->IsSkippingJPLPlanets()) {
      tmp_frame.SetTime(frame_in + timestep);
      tmp_frame.ForceJPLEphemerisData();
    }
    //
    interaction->Acceleration(tmp_frame,acc);
    //
    b4 = acc;
    
    frame_out = frame_in;
    for (j=0;j<frame_out.size();++j) {
      frame_out[j].AddToPosition(frame_in[j].velocity() * h + h * h / 6.0 * ( b1[j] + b2[j] + b3[j] ));
      frame_out[j].AddToVelocity(h / 6.0 * ( b1[j] + 2 * b2[j] + 2 * b3[j] + b4[j] ));
    }
    
    frame_out += timestep;
  }
  
  // DissipativeRungeKutta
  
  DissipativeRungeKutta::DissipativeRungeKutta() : FixedTimestepIntegrator() {
    type = DISSIPATIVERUNGEKUTTA;
  }
  
  DissipativeRungeKutta::DissipativeRungeKutta(const DissipativeRungeKutta & i) : FixedTimestepIntegrator() {
    type     = i.type;
    timestep = i.timestep;
    accuracy = i.accuracy;
    // m        = i.m;
  }
  
  DissipativeRungeKutta::~DissipativeRungeKutta() {
    
  }
  
  Integrator * DissipativeRungeKutta::clone() const {
    return new DissipativeRungeKutta(*this);
  }
  
  void DissipativeRungeKutta::Step(const Frame & frame_in, Frame & frame_out, Interaction * interaction) {
    
    // DISSIPATIVE (velocity dependent) version
    
    const unsigned int n =  frame_in.size();
    
    const double h = timestep.GetDouble();
    
    vector<Vector> acc(n);
    
    vector<Vector> b1(n), b2(n), b3(n), b4(n);
    
    unsigned int j;
    
    interaction->Acceleration(frame_in,acc);
    //
    b1 = acc;
    
    Frame tmp_frame = frame_in;
    for (j=0;j<tmp_frame.size();++j) {
      tmp_frame[j].AddToPosition(frame_in[j].velocity() * h / 2.0 + b1[j] * h * h / 8.0);
      tmp_frame[j].AddToVelocity(b1[j] * h / 2.0);
    }
    //
    if (interaction->IsSkippingJPLPlanets()) {
      tmp_frame.SetTime(frame_in + h / 2.0);
      tmp_frame.ForceJPLEphemerisData();
    }
    //
    interaction->Acceleration(tmp_frame,acc);
    //
    b2 = acc;
    
    tmp_frame = frame_in;
    for (j=0;j<tmp_frame.size();++j) {
      tmp_frame[j].AddToPosition(frame_in[j].velocity() * h / 2.0 + b1[j] * h * h / 8.0);
      tmp_frame[j].AddToVelocity(b2[j] * h / 2.0);
    }
    //
    if (interaction->IsSkippingJPLPlanets()) {
      tmp_frame.SetTime(frame_in + h / 2.0);
      tmp_frame.ForceJPLEphemerisData();
    }
    //
    interaction->Acceleration(tmp_frame,acc);
    //
    b3 = acc;
    
    tmp_frame = frame_in;
    for (j=0;j<tmp_frame.size();++j) {
      tmp_frame[j].AddToPosition(frame_in[j].velocity() * h + b3[j] * h * h / 2.0);
      tmp_frame[j].AddToVelocity(b3[j] * h);
    }
    //
    if (interaction->IsSkippingJPLPlanets()) {
      tmp_frame.SetTime(frame_in + timestep);
      tmp_frame.ForceJPLEphemerisData();
    }
    //
    interaction->Acceleration(tmp_frame,acc);
    //
    b4 = acc;
    
    frame_out = frame_in;
    for (j=0;j<frame_out.size();++j) {
      frame_out[j].AddToPosition(frame_in[j].velocity() * h + h * h / 6.0 * ( b1[j] + b2[j] + b3[j] ));
      frame_out[j].AddToVelocity(h / 6.0 * ( b1[j] + 2 * b2[j] + 2 * b3[j] + b4[j] ));
    }
    
    frame_out += timestep;
  }
  
} // namespace orsa
