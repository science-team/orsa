/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_body.h"
#include "orsa_error.h"
#include "orsa_file.h"

#include <algorithm> // sort()

#include <iostream>

using namespace std;

namespace orsa {
  
  unsigned int BodyConstants::used_body_id = 0;
  
  list<BodyConstants*> BodyConstants::list_bc;
  
  BodyConstants::BodyConstants() : name_(""), mass_(0.0), mu_(0.0), zero_mass_(mass_ == 0.0), radius_(0.0), planet_(NONE), J2_(0.0), J3_(0.0), J4_(0.0), C22_(0.0), C31_(0.0), C32_(0.0), C33_(0.0), C41_(0.0), C42_(0.0), C43_(0.0), C44_(0.0), S31_(0.0), S32_(0.0), S33_(0.0), S41_(0.0), S42_(0.0), S43_(0.0), S44_(0.0), id(used_body_id++) { 
    users = 1;
    list_bc.push_back(this);
  }
  
  BodyConstants::BodyConstants(const string & name, const double mass) : name_(name), mass_(mass), mu_(GetG()*mass_), zero_mass_(mass_ == 0.0), radius_(0.0), planet_(NONE), J2_(0.0), J3_(0.0), J4_(0.0), C22_(0.0), C31_(0.0), C32_(0.0), C33_(0.0), C41_(0.0), C42_(0.0), C43_(0.0), C44_(0.0), S31_(0.0), S32_(0.0), S33_(0.0), S41_(0.0), S42_(0.0), S43_(0.0), S44_(0.0), id(used_body_id++) {
    users = 1;
    list_bc.push_back(this);
  }
  
  BodyConstants::BodyConstants(const string & name, const double mass, const double radius) : name_(name), mass_(mass), mu_(GetG()*mass_), zero_mass_(mass_ == 0.0), radius_(radius), planet_(NONE), J2_(0.0), J3_(0.0), J4_(0.0), C22_(0.0), C31_(0.0), C32_(0.0), C33_(0.0), C41_(0.0), C42_(0.0), C43_(0.0), C44_(0.0), S31_(0.0), S32_(0.0), S33_(0.0), S41_(0.0), S42_(0.0), S43_(0.0), S44_(0.0), id(used_body_id++) {
    users = 1;
    list_bc.push_back(this);
  }
  
  BodyConstants::BodyConstants(const string & name, const double mass, const double radius, const JPL_planets p) : name_(name), mass_(mass), mu_(GetG()*mass_), zero_mass_(mass_ == 0.0), radius_(radius), planet_(p), J2_(0.0), J3_(0.0), J4_(0.0), C22_(0.0), C31_(0.0), C32_(0.0), C33_(0.0), C41_(0.0), C42_(0.0), C43_(0.0), C44_(0.0), S31_(0.0), S32_(0.0), S33_(0.0), S41_(0.0), S42_(0.0), S43_(0.0), S44_(0.0), id(used_body_id++) {
    users = 1;
    list_bc.push_back(this);
  }
  
  BodyConstants::BodyConstants(const string & name, const double mass, const double radius, const double J2, const double J3, const double J4) : name_(name), mass_(mass), mu_(GetG()*mass_), zero_mass_(mass_ == 0.0), radius_(radius), planet_(NONE), J2_(J2), J3_(J3), J4_(J4), C22_(0.0), C31_(0.0), C32_(0.0), C33_(0.0), C41_(0.0), C42_(0.0), C43_(0.0), C44_(0.0), S31_(0.0), S32_(0.0), S33_(0.0), S41_(0.0), S42_(0.0), S43_(0.0), S44_(0.0), id(used_body_id++) {
    users = 1;
    list_bc.push_back(this);
  }
  
  BodyConstants::BodyConstants(const string & name, const double mass, const double radius, const JPL_planets p, const double J2, const double J3, const double J4) : name_(name), mass_(mass), mu_(GetG()*mass_), zero_mass_(mass_ == 0.0), radius_(radius), planet_(p), J2_(J2), J3_(J3), J4_(J4), C22_(0.0), C31_(0.0), C32_(0.0), C33_(0.0), C41_(0.0), C42_(0.0), C43_(0.0), C44_(0.0), S31_(0.0), S32_(0.0), S33_(0.0), S41_(0.0), S42_(0.0), S43_(0.0), S44_(0.0), id(used_body_id++) {
    users = 1;
    list_bc.push_back(this);
  }
  
  BodyConstants::BodyConstants(const string & name, const double mass, const double radius, const double J2, const double J3, const double J4, const double C22, const double C31, const double C32, const double C33, const double C41, const double C42, const double C43, const double C44, const double S31, const double S32, const double S33, const double S41, const double S42, const double S43, const double S44) : name_(name), mass_(mass), mu_(GetG()*mass_), zero_mass_(mass_ == 0.0), radius_(radius), planet_(NONE), J2_(J2), J3_(J3), J4_(J4), C22_(C22), C31_(C31), C32_(C32), C33_(C33), C41_(C41), C42_(C42), C43_(C43), C44_(C44), S31_(S31), S32_(S32), S33_(S33), S41_(S41), S42_(S42), S43_(S43), S44_(S44), id(used_body_id++) {
    users = 1;
    list_bc.push_back(this);
  }
  
  BodyConstants::BodyConstants(const string & name, const double mass, const double radius, const JPL_planets p, const double J2, const double J3, const double J4, const double C22, const double C31, const double C32, const double C33, const double C41, const double C42, const double C43, const double C44, const double S31, const double S32, const double S33, const double S41, const double S42, const double S43, const double S44) : name_(name), mass_(mass), mu_(GetG()*mass_), zero_mass_(mass_ == 0.0), radius_(radius), planet_(p), J2_(J2), J3_(J3), J4_(J4), C22_(C22), C31_(C31), C32_(C32), C33_(C33), C41_(C41), C42_(C42), C43_(C43), C44_(C44), S31_(S31), S32_(S32), S33_(S33), S41_(S41), S42_(S42), S43_(S43), S44_(S44), id(used_body_id++) {
    users = 1;
    list_bc.push_back(this);
  }
  
  BodyConstants::~BodyConstants() { 
    list<BodyConstants*>::iterator it = list_bc.begin();
    while (it != list_bc.end()) {
      if (*it == this) {
        list_bc.erase(it);
        break;
      }
      ++it;
    }
  }
  
  //------//
  
  Body::Body() {
    bc = new BodyConstants();
  }
  
  Body::Body(const double mass) {
    bc = new BodyConstants("",mass,0);
  }
  
  Body::Body(const string & name) {
    bc = new BodyConstants(name,0,0);
  }
  
  Body::Body(const string & name, const double mass) {
    bc = new BodyConstants(name,mass,0);
  }
  
  Body::Body(const string & name, const double mass, const double radius) {
    bc = new BodyConstants(name,mass,radius);
  }
  
  Body::Body(const string & name, const double mass, const double radius, const JPL_planets p) {
    bc = new BodyConstants(name,mass,radius,p);
  }
  
  Body::Body(const string & name, const double mass, const Vector &position, const Vector &velocity) {
    bc = new BodyConstants(name,mass,0);
    _position = position;
    _velocity = velocity;
  }
  
  Body::Body(const string & name, const double mass, const double radius, const Vector &position, const Vector &velocity) {
    bc = new BodyConstants(name,mass,radius);
    _position = position;
    _velocity = velocity;
  }
    
  Body::Body(const string & name, const double mass, const double radius, const double J2, const double J3, const double J4) {
    bc = new BodyConstants(name,mass,radius,J2,J3,J4);
  }
  
  Body::Body(const string & name, const double mass, const double radius, const JPL_planets p, const double J2, const double J3, const double J4) {
    bc = new BodyConstants(name,mass,radius,p,J2,J3,J4);
  }
    
  Body::Body(const string & name, const double mass, const double radius, const double J2, const double J3, const double J4, const double C22, const double C31, const double C32, const double C33, const double C41, const double C42, const double C43, const double C44, const double S31, const double S32, const double S33, const double S41, const double S42, const double S43, const double S44) {
    bc = new BodyConstants(name,mass,radius,J2,J3,J4,C22,C31,C32,C33,C41,C42,C43,C44,S31,S32,S33,S41,S42,S43,S44);
  }
  
  Body::Body(const string & name, const double mass, const double radius, const JPL_planets p, const double J2, const double J3, const double J4, const double C22, const double C31, const double C32, const double C33, const double C41, const double C42, const double C43, const double C44, const double S31, const double S32, const double S33, const double S41, const double S42, const double S43, const double S44) {
    bc = new BodyConstants(name,mass,radius,p,J2,J3,J4,C22,C31,C32,C33,C41,C42,C43,C44,S31,S32,S33,S41,S42,S43,S44);
  }
  
  Body::Body(const string & name, const double mass, const Vector &position, const Vector &velocity, const double J2, const double J3, const double J4) {
    bc = new BodyConstants(name,mass,0,J2,J3,J4);
    _position = position;
    _velocity = velocity;
  }
  
  Body::Body(const string & name, const double mass, const double radius, const Vector &position, const Vector &velocity, const double J2, const double J3, const double J4) {
    bc = new BodyConstants(name,mass,radius,J2,J3,J4);
    _position = position;
    _velocity = velocity;
  }
  
  Body::Body(const Body & b) {     
    bc = b.bc; 
    bc->AddUser();
    _position = b._position;
    _velocity = b._velocity;
  }
  
  Body::Body(const BodyWithEpoch & b) {
    bc = b.bc; 
    bc->AddUser();
    _position = b._position;
    _velocity = b._velocity;
  }
  
  Body::Body(const JPLBody & b) {
    bc = b.bc; 
    bc->AddUser();
    _position = b.position();
    _velocity = b.velocity();
  }
  
  Body::~Body() {
    bc->RemoveUser();
    if (bc->Users() == 0) {
      delete bc;
      bc = 0;
    }
  }
  
  Body & Body::operator = (const Body & b) {  
    
    if (bc != b.bc) {
      bc->RemoveUser();
      if (bc->Users() == 0) {
	delete bc;
	bc = 0;
      }
      
      bc = b.bc; 
      bc->AddUser();
    }
    
    _position = b._position;
    _velocity = b._velocity;
    
    return * this;
  }
  
  //--------//
  
  BodyWithEpoch::BodyWithEpoch(const BodyWithEpoch & b) : Body(b), epoch(b.epoch) { }
  
  // BodyWithEpoch::BodyWithEpoch(const JPLBody & b) : Body(b), epoch(b.epoch) { }
  
  //--------//
  
  /* 
     static JPLFile * open_JPLFile() {
     
     JPLFile * jf = 0;
     
     if (config->paths[JPL_EPHEM_FILE]->GetValue() != "") {
     jf = new JPLFile(config->paths[JPL_EPHEM_FILE]->GetValue().c_str());
     } else {
     ORSA_WARNING("no JPL file found");
     }
     
     return jf;
     }
     
     static void close_JPLFile(JPLFile * jf) {
     delete jf;
     jf = 0;
     }
  */
  
  /* 
     static double local_mass(JPL_planets planet) {
     JPLFile * jf = open_JPLFile();
     const double mass = jf->GetMass(planet);
     close_JPLFile(jf);
     return mass;
     }
  */
  
  static double local_mass(const JPL_planets planet) {
    return jpl_file->GetMass(planet);
  }
  
  static double local_J2(const JPL_planets planet) {
    double J2_ = 0.0;
    switch (planet) {
    case SUN:   J2_ = jpl_file->GetTag("J2SUN"); break;
    case EARTH: J2_ = jpl_file->GetTag("J2E");   break;
    case MOON:  J2_ = jpl_file->GetTag("J2M");   break;
    default: /*********************************/ break;
    }
    return J2_;
  }
  
  static double local_J3(const JPL_planets planet) {
    double J3_ = 0.0;
    switch (planet) {
    case EARTH: J3_ = jpl_file->GetTag("J3E");   break;
    case MOON:  J3_ = jpl_file->GetTag("J3M");   break;
    default: /*********************************/ break;
    }
    return J3_;
  }
  
  static double local_J4(const JPL_planets planet) {
    double J4_ = 0.0;
    switch (planet) {
    case EARTH: J4_ = jpl_file->GetTag("J4E");   break;
    case MOON:  J4_ = jpl_file->GetTag("J4M");   break;
    default: /*********************************/ break;
    }
    return J4_;
  }
  
  static double local_C22(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("C22M");
    } else {
      return 0.0;
    }
  }
  
  static double local_C31(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("C31M");
    } else {
      return 0.0;
    }
  }
  
  static double local_C32(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("C32M");
    } else {
      return 0.0;
    }
  }
  
  static double local_C33(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("C33M");
    } else {
      return 0.0;
    }
  }
  
  static double local_C41(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("C41M");
    } else {
      return 0.0;
    }
  }
  
  static double local_C42(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("C42M");
    } else {
      return 0.0;
    }
  }
  
  static double local_C43(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("C43M");
    } else {
      return 0.0;
    }
  }
  
  static double local_C44(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("C44M");
    } else {
      return 0.0;
    }
  }
  
  static double local_S31(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("S31M");
    } else {
      return 0.0;
    }
  }
  
  static double local_S32(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("S32M");
    } else {
      return 0.0;
    }
  }
  
  static double local_S33(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("S33M");
    } else {
      return 0.0;
    }
  }
  
  static double local_S41(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("S41M");
    } else {
      return 0.0;
    }
  }
  
  static double local_S42(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("S42M");
    } else {
      return 0.0;
    }
  }
  
  static double local_S43(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("S43M");
    } else {
      return 0.0;
    }
  }
  
  static double local_S44(const JPL_planets planet) {
    if (planet == MOON) {
      return jpl_file->GetTag("S44M");
    } else {
      return 0.0;
    }
  }
  
  JPLBody::JPLBody(const JPL_planets p, const Date & d) : BodyWithEpoch(JPL_planet_name(p),
									local_mass(p),
									orsa::radius(p),
									p,
									d,
									local_J2(p),
									local_J3(p),
									local_J4(p),
									local_C22(p),
									local_C31(p),
									local_C32(p),
									local_C33(p),
									local_C41(p),
									local_C42(p),
									local_C43(p),
									local_C44(p),
									local_S31(p),
									local_S32(p),
									local_S33(p),
									local_S41(p),
									local_S42(p),
									local_S43(p),
									local_S44(p)) {
    jpl_file->GetEph(epoch,JPLPlanet(),_position,_velocity);    
  }
  
  JPLBody::JPLBody() : BodyWithEpoch(JPL_planet_name(NONE),local_mass(NONE),orsa::radius(NONE),NONE) {
    
  }
  
  /* 
     void JPLBody::SetEpoch(const UniverseTypeAwareTime & t) {
     epoch = t;
     JPLFile * jf = open_JPLFile();
     jf->GetEph(epoch,JPLPlanet(),_position,_velocity);    
     close_JPLFile(jf);
     }
  */
  
  void JPLBody::SetEpoch(const UniverseTypeAwareTime & t) {
    epoch = t;
    jpl_file->GetEph(epoch,JPLPlanet(),_position,_velocity);    
  }
  
  void Interpolate(const vector<BodyWithParameter> &b_in, const double x, Body &b_out, Body &err_b_out) {
    
    unsigned int n_points = b_in.size();
    
    Vector p_interpolated, err_p_interpolated;
    Vector v_interpolated, err_v_interpolated;
    
    unsigned int j;
    
    vector<VectorWithParameter> pp(n_points), vv(n_points);
    
    for(j=0;j<n_points;j++) {
      pp[j].Set(b_in[j].position());
      vv[j].Set(b_in[j].velocity());
      pp[j].par = vv[j].par = b_in[j].par; 
    }
    
    Interpolate(pp,x,p_interpolated,err_p_interpolated);
    Interpolate(vv,x,v_interpolated,err_v_interpolated);
    
    b_out = b_in[0];
    b_out.SetPosition(p_interpolated);
    b_out.SetVelocity(v_interpolated);
    
    err_b_out = b_in[0];
    err_b_out.SetPosition(err_p_interpolated);
    err_b_out.SetVelocity(err_v_interpolated);
    
  }

  void print(const Body &b) {
    cout << "body name:   " << b.name() << "   mass: " << b.mass() << endl;
    /* if (b.parent) {
       cout << "parent body: " << b.parent->name() << endl;
       }
    */

    cout << "position:    " << b.position().x << " " << b.position().y << " " << b.position().z << endl;
    cout << "velocity:    " << b.velocity().x << " " << b.velocity().y << " " << b.velocity().z << endl;
  }
  
 
  /* 
     template <class T> void AutoHierarchy(vector<T> &bb) {
     
     if (bb.size() < 2) return;
     // should also check if at least a massive body is present?
     
     cerr << "pre-AutoHierarchy frame:" << endl;
     PrintHierarchy(bb);
     
     // sort for decreasing mass
     {
     cerr << "pre-sort addresses:" << endl;
     unsigned int r;
     for (r=0;r<bb.size();++r) {
     // cerr << bb[r].name() << "  " << &bb[r] << endl;
     cerr << bb[r].name() << "  " << &bb[r] << endl;
     print(bb[r]);
     }
     }
     //
     sort(bb.begin(),bb.end());
     //
     {
     cerr << "post-sort addresses:" << endl;
     unsigned int r;
     for (r=0;r<bb.size();++r) {
     cerr << bb[r].name() << "  " << &bb[r] << endl;
     print(bb[r]);
     }
     }
     
     // the most massive body is always the root in the hierarchy
     bb[0].SetParent(0);
     
     // acc is a Newton acceleration rescaled with G=1
     double acc, acc_max;
     unsigned int j=1,k,k_candidate;
     while (j < bb.size()) {
     
     k=0;
     k_candidate=0;    
     acc_max = 0.0;
     while (k<j) {
     if (bb[k].mass() > 0) {
     acc = bb[k].mass()/LengthSquared(bb[j].distanceVector(bb[k]));
     if (acc > acc_max) {
     acc_max = acc;
     k_candidate = k;
     }
     }
     ++k;
     }
     
     if (acc_max > 0) {
     bb[j].SetParent(&(bb[k_candidate]));
     }
     
     ++j;
     }
     
     cerr << "post-AutoHierarchy frame:" << endl;
     PrintHierarchy(bb);
     //   
     {
     unsigned int r;
     for (r=0;r<bb.size();++r) {
     cerr << bb[r].name() << "  " << &bb[r] << endl;
     print(bb[r]);
     }
     }
     }
     
     // instantiation
     template void AutoHierarchy(vector<Body>&);
     template void AutoHierarchy(vector<BodyWithEpoch>&);
     
     //
     
     template <class T> void PrintHierarchy(const vector<T> &f) {
     
     fprintf(stderr," ----- \n");
     
     unsigned int k;
     for (k=0;k<f.size();++k) {
     
     if (f[k].HaveParent()) {
     fprintf(stderr,
     "[%i][%i] - [%i] --> [%i] %s \n",
     f[k].GetFamilyId(),
     f[k].GetFamilyTreeLevel(),
     f[k].BodyId(),
     f[k].ParentBodyId(),
     f[k].name().c_str()
     );
     } else {
     fprintf(stderr,
     "[%i][%i] - [%i] %s\n",
     f[k].GetFamilyId(),
     f[k].GetFamilyTreeLevel(),
     f[k].BodyId(),
     f[k].name().c_str()
     );
     }
     
     // fprint(stderr,f[k]);
     }
     
     } 
     
     // instantiation
     template void PrintHierarchy(const vector<Body>&);
     template void PrintHierarchy(const vector<BodyWithEpoch>&);
  */
  
} // namespace orsa
