/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_frame.h"

#include "orsa_universe.h"
#include "orsa_config.h"

#include <iostream>

using namespace std;

namespace orsa {
  
  Frame::Frame() : vector<Body>(), UniverseTypeAwareTime() { }
  
  Frame::Frame(const UniverseTypeAwareTime & t) : vector<Body>(), UniverseTypeAwareTime(t) { }
  
  Frame::Frame(const Frame & f) : vector<Body>(f), UniverseTypeAwareTime(f) {
    
  }
  
  bool Frame::operator < (const Frame & f) const {
    return (this->UniverseTypeAwareTime::operator < (f));
  }
  
  void Frame::ForceJPLEphemerisData() {
    if (universe->GetUniverseType() != Real) return;
    for (unsigned int k=0;k<size();++k) {
      const JPL_planets p = (*this)[k].JPLPlanet();
      if (p == NONE) continue;
      JPLBody jb(p,*this);
      (*this)[k].SetPosition(jb.position());
      (*this)[k].SetVelocity(jb.velocity());      
    }
  }
  
  void print(const Frame &f) {
    cout << "Frame time: " << f.Time() << endl;
    cout << "Frame size: " << f.size() << endl;
    unsigned int l;
    for (l=0;l<f.size();l++) print(f[l]);
  }
  
  Vector Frame::CenterOfMass() const {
    return (orsa::CenterOfMass(*this));
  }
  
  Vector Frame::CenterOfMassVelocity() const {
    return (orsa::CenterOfMassVelocity(*this));
  }
  
  Vector Frame::Barycenter() const {
    return (orsa::Barycenter(*this));
  }
  
  Vector Frame::BarycenterVelocity() const {
    return (orsa::BarycenterVelocity(*this));
  }
  
  // Relativistic Barycenter stuff, from:
  // "Explanatory Supplement to the Astronomical Almanac",
  // Edited by P. Kenneth Seidelmann, U.S. Naval Observatory,
  // 2nd edition, 1992, sect. 5.215, pag. 286.
  
  // Eq. 5.215-2 of the book.
  // mu = g * mass
  static double modified_mu(const vector<Body> & f, const unsigned int i) {
    if (f[i].has_zero_mass()) return 0.0;
    const double one_over_two_c = 1.0/(2.0*GetC());
    // const double g = GetG();
    //
    vector<double> mu(f.size());
    for (unsigned int j=0; j<f.size(); ++j) {
      if (f[j].has_zero_mass()) {
	mu[j] = 0.0;
      } else {
	mu[j] = f[j].mu();
      }
    }
    //
    double mod_mu = 0.0;
    double tmp_sum = 0.0;
    for (unsigned int j=0; j<f.size(); ++j) {	
      if (j == i) continue;
      tmp_sum += mu[j]/(f[j].position()-f[i].position()).Length();
    }
    //
    mod_mu += mu[i]*(1.0+one_over_two_c*(f[i].velocity().LengthSquared()-tmp_sum));
    return mod_mu;
  }
  
  Vector Frame::RelativisticBarycenter() const {
    return (orsa::RelativisticBarycenter(*this));
  }
  
  Vector Frame::RelativisticBarycenterVelocity() const {
    return (orsa::RelativisticBarycenterVelocity(*this));
  }
  
  Vector CenterOfMass(const vector<Body> & f) {
    Vector sum_vec(0,0,0);
    double sum_mass = 0.0;
    for (unsigned int k=0; k<f.size(); ++k) {
      const double mass = f[k].mass();
      if (mass > 0.0) {
	sum_vec  += mass*f[k].position();
	sum_mass += mass;
      }
    }
    return (sum_vec/sum_mass);
  }
  
  Vector CenterOfMassVelocity(const vector<Body> & f) {
    Vector sum_vec(0,0,0);
    double sum_mass = 0.0;
    for (unsigned int k=0; k<f.size(); ++k) {
      const double mass = f[k].mass();
      if (mass > 0.0) {
	sum_vec  += mass*f[k].velocity();
	sum_mass += mass;
      }
    }
    return (sum_vec/sum_mass);
  }
  
  Vector Barycenter(const vector<Body> & f) {
    return (orsa::CenterOfMass(f));
  }
  
  Vector BarycenterVelocity(const vector<Body> & f) {
    return (orsa::CenterOfMassVelocity(f));
  }
  
  Vector RelativisticBarycenter(const vector<Body> & f) {
    Vector sum_vec(0,0,0);
    double sum_mu = 0.0;
    for (unsigned int i=0; i<f.size(); ++i) {
      const double mod_mu = modified_mu(f,i);
      if (mod_mu > 0.0) {
	sum_vec += mod_mu * f[i].position();
	sum_mu  += mod_mu;
      }
    }
    //
    return (sum_vec/sum_mu);
  }
  
  Vector RelativisticBarycenterVelocity(const vector<Body> & f) {
    Vector sum_vec(0,0,0);
    double sum_mu = 0.0;
    for (unsigned int i=0; i<f.size(); ++i) {
      const double mod_mu = modified_mu(f,i);
      if (mod_mu > 0.0) {
	sum_vec += mod_mu * f[i].velocity();
	sum_mu  += mod_mu;
      }
    }
    //
    return (sum_vec/sum_mu);
  }
  
  Vector Frame::AngularMomentum(const Vector & center) const {
    return (orsa::AngularMomentum(*this,center));
  }
  
  Vector AngularMomentum(const vector<Body> & f, const Vector & center) {
    Vector vec_sum;
    unsigned int k;
    for (k=0;k<f.size();++k) {
      if (f[k].mass() > 0) {
	vec_sum += f[k].mass()*ExternalProduct(f[k].position()-center,f[k].velocity());
      }
    }
    return (vec_sum);
  }
  
  Vector Frame::BarycentricAngularMomentum() const {
    return (orsa::BarycentricAngularMomentum(*this));
  }
  
  Vector BarycentricAngularMomentum(const vector<Body> & f) {
    return (orsa::AngularMomentum(f,Barycenter(f)));
  }
  
  double KineticEnergy(const vector<Body> & f) {
    if (f.size()==0) return (0.0);
    double energy=0.0;
    unsigned int k=0;
    while (k<f.size()) {
      energy += f[k].KineticEnergy();
      k++;
    }
    return (energy);
  }
  
} // namespace orsa
