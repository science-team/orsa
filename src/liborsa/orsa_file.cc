/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_file.h"

#include <iostream>

#include <ctype.h>

#include "orsa_common.h"
#include "orsa_units.h"
#include "orsa_error.h"
#include "orsa_version.h"

#include "sdncal.h"

#include <cstdio>
#include "jpleph.h"
#include "jpl_int.h"

#ifndef _WIN32
#include <unistd.h> // sleep()
#else
#include <shlobj.h>
#include <direct.h>
#endif

#include "support.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif // HAVE_CONFIG_H

using namespace std;

namespace orsa {
  
  void ReadFile::Open() {
    if (status != CLOSE) return;
    
    file = OPEN_FILE(filename.c_str(),OPEN_READ);
    
    if (file == 0) { 
      ORSA_ERROR("Can't open file %s",filename.c_str());
    } else {
      status = OPEN_R;
    }
  }
  
  void WriteFile::Open() {
    if (status != CLOSE) return;
    
    file = OPEN_FILE(filename.c_str(),OPEN_WRITE);
    
    if (file == 0) { 
      ORSA_ERROR("Can't open file %s",filename.c_str());
    } else {
      status = OPEN_W;
    }
  }
  
  void ReadWriteFile::Open(const FILE_STATUS st) {
    
    // already in the right status
    if (status == st) return;
    
    // anomalous...
    if (st == CLOSE) {
      Close();
      return;
    }
    
    Close();
    
    if ((st == OPEN_R) && ((file = OPEN_FILE(filename.c_str(),OPEN_READ)) != 0)) {
      status = OPEN_R;
      return;
    }
    
    if ((st == OPEN_W) && ((file = OPEN_FILE(filename.c_str(),OPEN_WRITE)) != 0)) {
      status = OPEN_W;
      return;
    }
    
    if (file == 0) {
      ORSA_ERROR("Can't open file %s",filename.c_str());
    }
    
    status = CLOSE;
  }
    
    
  void File::Close() {
    if (status != CLOSE) {
      CLOSE_FILE(file);
      status = CLOSE;
    }
  }
  
  // AsteroidDatabaseFile
  
  // a copy of ReadFile::Open()
  /* 
     void AsteroidDatabaseFile::Open() {
     if (status != CLOSE) return;
     
     file = OPEN_FILE(filename.c_str(),OPEN_READ);
     
     if (file == 0) { 
     cerr << "Can't open file " << filename << endl;
     } else {
     status = OPEN_R;
     }
     }
  */
  
  // AstorbFile
  
  /* 
     int AstorbFile::GetSize() {
     
     if (status == CLOSE) Open();
     
     char line[300];
     
     REWIND_FILE(file);
     
     int lines = 0;
     // while ( (fgets(line,300,file)) != 0 ) {
     while ((GETS_FILE(line,300,file)) != 0) {
     lines++;
     }
     
     REWIND_FILE(file);
     
     return lines;
     }
  */
 
  /* 
     Date AstorbFile::GetEpoch() {
     
     if (status == CLOSE) Open();
     
     char line[1024];
     
     string epoch;
     
     REWIND_FILE(file);
     
     GETS_FILE(line,1024,file);
     while (strlen(line) < 100) {
     GETS_FILE(line,1024,file);
     }      
     
     epoch.assign(line,105,8);
     
     Date date(TDT);
     
     string year,month,day;
     int    y,m,d;
     
     year.assign(epoch,0,4);
     month.assign(epoch,4,2);
     day.assign(epoch,6,2);
     
     y = atoi(year.c_str());
     m = atoi(month.c_str());
     d = atoi(day.c_str());
     
     date.SetGregor(y,m,d);
     
     return (date);
     }
  */
  
  void AstorbFile::Read() {
    
    // if (db_updated) return;
    
    // if (status == CLOSE) Open();
    
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    // reset database 
    // cerr << "pre-resize..." << endl;
    db->clear();
    
    // db->reserve(GetSize()); // a little too time consuming...
    
    // cerr << "reading pass (1)" << endl;
    
    // int print_step = 0;
    // int print_step = 10000;
    
    char line[300];
    
    double a,e,i,omega_node,omega_pericenter,M;
    // int    n;
    string number,name,orbit_computer,absolute_magnitude,arc,numobs,epoch;
    string mean_anomaly,pericenter,node,inclination,eccentricity,semimajor_axis;
    // string ceu;
    
    string year,month,day;
    int    y,m,d;
    
    // Body orb_ref_body("",GetMSun());
    
    // AstorbDataEntry ast;
    Asteroid ast;
    
    // Date tmp_date(TDT);
    Date tmp_date;
    
    unsigned int local_index = 0;
    bool bool_stop=false;
    bool bool_pause=false;
    REWIND_FILE(file);
    while ((GETS_FILE(line,300,file)) != 0) {
      
      // cerr << "AstorbFile::Read() inside the while() loop..." << endl;
      
      if (strlen(line) < 100) continue; // not a good line, maybe a comment or a white line...
      
      // cerr << "inside while loop..." << endl;
      
      local_index++;
      read_progress(local_index,bool_pause,bool_stop);
      
      if (bool_stop) break;
      
      while (bool_pause) {
	// cerr << "AstorbFile::Read() sleeping..." << endl;
	sleep(1);
	read_progress(local_index,bool_pause,bool_stop);
      }
      
      // uncomment the ones used
      number.assign(line,0,5);
      name.assign(line,6,18);
      orbit_computer.assign(line,25,15);
      absolute_magnitude.assign(line,41,5);
      //
      arc.assign(line,94,5);
      numobs.assign(line,99,5);
      //
      epoch.assign(line,105,8);
      //
      mean_anomaly.assign(line,114,10);
      pericenter.assign(line,125,10);
      node.assign(line,136,10);
      inclination.assign(line,146,10);
      eccentricity.assign(line,157,10);
      semimajor_axis.assign(line,168,12);
      // ceu.assign(line,190,7);
      
      //////////////
      
      ast.n = atoi(number.c_str());
      
      ast.name = name;
      remove_leading_trailing_spaces(ast.name);
      
      // ast.ceu  = atof(ceu.c_str());
      
      ast.mag  = atof(absolute_magnitude.c_str());
      
      a                = atof(semimajor_axis.c_str());
      e                = atof(eccentricity.c_str());
      i                = (pi/180)*atof(inclination.c_str());
      omega_node       = (pi/180)*atof(node.c_str());
      omega_pericenter = (pi/180)*atof(pericenter.c_str());
      M                = (pi/180)*atof(mean_anomaly.c_str());
      
      ast.orb.a                = FromUnits(a,AU);
      ast.orb.e                = e;
      ast.orb.i                = i;
      ast.orb.omega_node       = omega_node;
      ast.orb.omega_pericenter = omega_pericenter;
      ast.orb.M                = M;
      
      year.assign(epoch,0,4);
      month.assign(epoch,4,2);
      day.assign(epoch,6,2);
      
      y = atoi(year.c_str());
      m = atoi(month.c_str());
      d = atoi(day.c_str());
      
      tmp_date.SetGregor(y,m,d,TDT);
      ast.orb.epoch.SetDate(tmp_date);
      // ast.orb.T = sqrt(4*pisq/(GetG()*GetMSun())*pow(FromUnits(ast.orb.a,AU),3));
      ast.orb.mu = GetG()*GetMSun();
      // ast.orb.ref_body = orb_ref_body;
      
      /* 
	 switch (universe->GetReferenceSystem()) {
	 case ECLIPTIC: break;
	 case EQUATORIAL:
	 { 
	 // cerr << "Rotating astorb orbit..." << endl;
	 const double obleq_rad = obleq(tmp_date).GetRad();
	 Vector position,velocity;
	 ast.orb.RelativePosVel(position,velocity);
	 position.rotate(0.0,obleq_rad,0.0);
	 velocity.rotate(0.0,obleq_rad,0.0);
	 ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	 }
	 break;
	 }
      */
      
      switch (universe->GetReferenceSystem()) {
      case ECLIPTIC: break;
      case EQUATORIAL:
	{ 
       	  Vector position,velocity;
	  ast.orb.RelativePosVel(position,velocity);
	  EclipticToEquatorial_J2000(position);
	  EclipticToEquatorial_J2000(velocity);
	  ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	}
	break;
      }
      
      db->push_back(ast);
      
    }
    
    read_finished();
    
  }
  
  /* 
     bool AstorbFile::GuessCorrectType() {
     return true;
     }	
  */
  
  // AstorbFile::AstorbFile() : ReadFile() {
  AstorbFile::AstorbFile() : AsteroidDatabaseFile() {
    // status = CLOSE;
    // db = new AstorbDatabase();
    db = new AsteroidDatabase();
  }
  
  AstorbFile::~AstorbFile() {
    delete db;
    db = 0;
  }
  
  // MPC orbit database
  MPCOrbFile::MPCOrbFile() : AsteroidDatabaseFile() {
    db = new AsteroidDatabase();
  }
  
  MPCOrbFile::~MPCOrbFile() {
    delete db;
    db = 0;
  }
  
  void MPCOrbFile::Read() {
    
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    db->clear();
    
    // Body orb_ref_body("",GetMSun());
    
    // unsigned int local_index;
    
    char line[300];
    
    double a,e,i,omega_node,omega_pericenter,M;
    // int    n;
    string number,name,orbit_computer,absolute_magnitude,arc,numobs,epoch;
    string mean_anomaly,pericenter,node,inclination,eccentricity,semimajor_axis;
    // string ceu;
    
    string year,month,day;
    int    y=0,m=0,d=0;
    
    // AstorbDataEntry ast;
    Asteroid ast;
    
    unsigned int local_index = 0;
    bool bool_stop=false;
    bool bool_pause=false;

    REWIND_FILE(file);
    
    // Date tmp_date(TDT);
    Date tmp_date;
    
    /* // skip header, not needed with the check on (strlen(line) < 201)...
       do { 
       GETS_FILE(line,300,file);
       } while (line[0] != '-');
       cerr << "below the header..." << endl;
    */
    
    while ( (GETS_FILE(line,300,file)) != 0 ) {
      
      // cerr << "line: -->" << line << "<--" << endl; 
      
      ++local_index;
      
      if (strlen(line) < 201) continue; // not a good line, maybe a comment or a white line...
      
      // cerr << "strlen(line): " << strlen(line) << endl;
      
      // cerr << "line: -->" << line << "<--" << endl; 
      
      read_progress(local_index,bool_pause,bool_stop);
      
      if (bool_stop) break;
      
      while (bool_pause) {
    	sleep(1);
	read_progress(local_index,bool_pause,bool_stop);
      }
      
      // uncomment the ones used
      number.assign(line,0,7);
      absolute_magnitude.assign(line,8,5);
      epoch.assign(line,20,5);
      //
      mean_anomaly.assign(line,26,9);
      pericenter.assign(line,37,9);
      node.assign(line,48,9);
      inclination.assign(line,59,9);
      eccentricity.assign(line,70,9);
      semimajor_axis.assign(line,92,11);
      //
      numobs.assign(line,117,5);
      orbit_computer.assign(line,150,10);
      
      if (strlen(line) > 160) {
	name.assign(line,166,28);
      } else {
	name = "";	
      }
      
      // conversions
      
      {
	// remove -->(NUMBER)<--
	
	string::size_type pos_open  = name.find("(",0);
	string::size_type pos_close = name.find(")",0);
	
	if ( (pos_open  != pos_close) &&
	     (pos_open  != string::npos) &&
	     (pos_close != string::npos) ) {
	  
	  name.erase(pos_open,pos_close-pos_open+1);
	}
      }    
      //
      ast.name = name;
      remove_leading_trailing_spaces(ast.name);
      
      // ast.n = 0; // arbitrary, for the moment
      ast.n = atoi(number.c_str());
      
      ast.mag  = atof(absolute_magnitude.c_str());
         
      a                = atof(semimajor_axis.c_str());
      e                = atof(eccentricity.c_str());
      i                = (pi/180)*atof(inclination.c_str());
      omega_node       = (pi/180)*atof(node.c_str());
      omega_pericenter = (pi/180)*atof(pericenter.c_str());
      M                = (pi/180)*atof(mean_anomaly.c_str());
      
      ast.orb.a                = FromUnits(a,AU);
      ast.orb.e                = e;
      ast.orb.i                = i;
      ast.orb.omega_node       = omega_node;
      ast.orb.omega_pericenter = omega_pericenter;
      ast.orb.M                = M;

      int ch;
      // cerr << "epoch string: " << epoch << endl;
      //// epoch
      // year
      year.assign(epoch,0,1);
      ch = (int)year.c_str()[0];
      // cerr << "ch: " << ch << "  " << ((int)('I')) << endl;
      switch (ch) {
      case 'I': y=1800; break; 
      case 'J': y=1900; break;
      case 'K': y=2000; break;
      }
      //
      year.assign(epoch,1,2);
      y += atoi(year.c_str());
      // month
      month.assign(epoch,3,1);
      ch = (int)month.c_str()[0];
      switch (ch) {
      case '1': m=1;  break;
      case '2': m=2;  break;
      case '3': m=3;  break;
      case '4': m=4;  break;
      case '5': m=5;  break;
      case '6': m=6;  break;
      case '7': m=7;  break;
      case '8': m=8;  break;
      case '9': m=9;  break;
      case 'A': m=10; break; 
      case 'B': m=11; break;
      case 'C': m=12; break;
      }
      // day
      day.assign(epoch,4,1);
      ch = (int)day.c_str()[0];
      switch (ch) {
      case '1': d=1;  break;
      case '2': d=2;  break;
      case '3': d=3;  break;
      case '4': d=4;  break;
      case '5': d=5;  break;
      case '6': d=6;  break;
      case '7': d=7;  break;
      case '8': d=8;  break;
      case '9': d=9;  break;
      case 'A': d=10; break; 
      case 'B': d=11; break;
      case 'C': d=12; break;
      case 'D': d=13; break;
      case 'E': d=14; break;
      case 'F': d=15; break;
      case 'G': d=16; break;
      case 'H': d=17; break;
      case 'I': d=18; break;
      case 'J': d=19; break;
      case 'K': d=20; break;
      case 'L': d=21; break;
      case 'M': d=22; break;
      case 'N': d=23; break;
      case 'O': d=24; break;
      case 'P': d=25; break;
      case 'Q': d=26; break;
      case 'R': d=27; break;
      case 'S': d=28; break;
      case 'T': d=29; break;
      case 'U': d=30; break;
      case 'V': d=31; break;
      }
      //
      // cerr << "MPC::Read() --> y: " << y << "  m: " << m << "  d: " << d << endl;
      // ast.orb.time = FromUnits(GregorianToSdn(y,m,d)-0.5,DAY);
      tmp_date.SetGregor(y,m,d,TDT);
      ast.orb.epoch.SetDate(tmp_date);
      
      // ast.orb.T = sqrt(4*pisq/(GetG()*GetMSun())*pow(FromUnits(ast.orb.a,AU),3));
      ast.orb.mu = GetG()*GetMSun();
      // ast.orb.ref_body.mass=GetMSun();
      // ast.orb.ref_body = Body("",GetMSun());
      // ast.orb.ref_body = orb_ref_body;
      
      /* 
	 switch (universe->GetReferenceSystem()) {
	 case ECLIPTIC: break;
	 case EQUATORIAL:
	 { 
	 // cerr << "Rotating astorb orbit..." << endl;
	 const double obleq_rad = obleq(tmp_date).GetRad();
	 Vector position,velocity;
	 ast.orb.RelativePosVel(position,velocity);
	 position.rotate(0.0,obleq_rad,0.0);
	 velocity.rotate(0.0,obleq_rad,0.0);
	 ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	 }
	 break;
	 }
      */
      
      switch (universe->GetReferenceSystem()) {
      case ECLIPTIC: break;
      case EQUATORIAL:
	{ 
	  Vector position,velocity;
	  ast.orb.RelativePosVel(position,velocity);
	  EclipticToEquatorial_J2000(position);
	  EclipticToEquatorial_J2000(velocity);
	  ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	}
	break;
      }
      
      // cerr << "adding object: " << ast.name << "  a: " << ast.orb.a << endl;
      
      db->push_back(ast);
      
    }
    
    read_finished();
  }
  
  
  // MPC Comets orbit database
  MPCCometFile::MPCCometFile() : AsteroidDatabaseFile() {
    db = new AsteroidDatabase();
    // status = CLOSE;
  }
  
  MPCCometFile::~MPCCometFile() {
    delete db;
    db = 0;
  }
  
  /* 
     bool MPCCometFile::GuessCorrectType() {
     return true;
     }
  */
  
  void MPCCometFile::Read() {
    
    // if (status == CLOSE) Open();
    
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    db->clear();
    
    char line[300];
    
    double a,e,i,omega_node,omega_pericenter,M;
    string number,type,name,orbit_computer,absolute_magnitude,arc,numobs,epoch;
    string mean_anomaly,pericenter,node,inclination,eccentricity,semimajor_axis;
    // string ceu;
    string pericenter_distance,pericenter_epoch;
    
    string year,month,day;
    int    y=0,m=0,d=0;
    double frac_day;
    
    Asteroid ast;
    
    double q;
    
    REWIND_FILE(file);
    
    // Date tmp_date(TDT);
    Date tmp_date;
    
    bool have_perturbed_solution_epoch;
    
    unsigned int local_index = 0;
    bool bool_stop=false;
    bool bool_pause=false;
    
    while ( (GETS_FILE(line,300,file)) != 0 ) {
      
      if (strlen(line) < 90) continue; // not a good line, maybe a comment or a white line...
      
      ++local_index;
      read_progress(local_index,bool_pause,bool_stop);
      
      if (bool_stop) break;
      
      while (bool_pause) {
	// cerr << "AstorbFile::Read() sleeping..." << endl;
	sleep(1);
	read_progress(local_index,bool_pause,bool_stop);
      }
      
      // uncomment the ones used
      number.assign(line,0,4);
      type.assign(line,4,1);
      // name.assign(line,5,7);
      name.assign(line,102,strlen(line)-102-1); // the last -1 is set to avoid the '\n' character in the name
      // cerr << "comet name: " << name << endl;
      pericenter_epoch.assign(line,14,15);
      pericenter_distance.assign(line,30,9);
      eccentricity.assign(line,41,8);
      pericenter.assign(line,51,8);
      node.assign(line,61,8);
      inclination.assign(line,71,8);
      //
      epoch.assign(line,81,8);
      
      // conversions
      
      ast.name = name;
      remove_leading_trailing_spaces(ast.name);
      
      ast.n = 0; // arbitrary, for the moment
      
      ast.mag  = atof(absolute_magnitude.c_str());
      
      // a                = atof(semimajor_axis.c_str());
      e                = atof(eccentricity.c_str());
      
      // to be tested...
      q = atof(pericenter_distance.c_str());
      if (e == 1.0) {
	a = q;
      } else {
	a = q/fabs(1.0-e);
      }
      
      i                = (pi/180)*atof(inclination.c_str());
      omega_node       = (pi/180)*atof(node.c_str());
      omega_pericenter = (pi/180)*atof(pericenter.c_str());
      // M                = (pi/180)*atof(mean_anomaly.c_str());
      
      year.assign(epoch,0,4);
      month.assign(epoch,4,2);
      day.assign(epoch,6,2);
      
      y = atoi(year.c_str());
      m = atoi(month.c_str());
      d = atoi(day.c_str());
      
      // check on the presence of the 'perturbed solutions' epoch...
      if (y < 1700) {
	have_perturbed_solution_epoch=false;
      } else {
	have_perturbed_solution_epoch=true;
      }	
      
      tmp_date.SetGregor(y,m,d,TDT);
      
      year.assign(pericenter_epoch,0,4);
      month.assign(pericenter_epoch,5,2);
      day.assign(pericenter_epoch,8,7);
      
      y = atoi(year.c_str());
      m = atoi(month.c_str());
      frac_day = atof(day.c_str());
      
      Date peri_date;
      peri_date.SetGregor(y,m,frac_day,TDT);
      UniverseTypeAwareTime pericenter_passage(peri_date);
      
      if (have_perturbed_solution_epoch) {
	ast.orb.epoch.SetDate(tmp_date);
      } else {
	ast.orb.epoch.SetDate(peri_date);
      }
      
      ast.orb.mu = GetG()*GetMSun();
      
      ast.orb.a                = FromUnits(a,AU);
      ast.orb.e                = e;
      ast.orb.i                = i;
      ast.orb.omega_node       = omega_node;
      ast.orb.omega_pericenter = omega_pericenter;
      //
      if (have_perturbed_solution_epoch) {
	M = ((ast.orb.epoch.Time() - pericenter_passage.Time())/ast.orb.Period())*twopi;  
	M = fmod(10*twopi+fmod(M,twopi),twopi);
	//
	ast.orb.M = M;
      } else {
	ast.orb.M = 0.0;
      }
      
      // cerr << "comet: " << ast.name << "  q: " << q << "  e: " << e << "  i: " << i*(180/pi) << endl;
      
      /* 
	 switch (universe->GetReferenceSystem()) {
	 case ECLIPTIC: break;
	 case EQUATORIAL:
	 { 
	 // cerr << "Rotating astorb orbit..." << endl;
	 const double obleq_rad = obleq(tmp_date).GetRad();
	 Vector position,velocity;
	 ast.orb.RelativePosVel(position,velocity);
	 position.rotate(0.0,obleq_rad,0.0);
	 velocity.rotate(0.0,obleq_rad,0.0);
	 ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	 }
	 break;
	 }
      */
      
      switch (universe->GetReferenceSystem()) {
      case ECLIPTIC: break;
      case EQUATORIAL:
	{ 
	  Vector position,velocity;
	  ast.orb.RelativePosVel(position,velocity);
	  EclipticToEquatorial_J2000(position);
	  EclipticToEquatorial_J2000(velocity);
	  ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	}
	break;
      }
      
      db->push_back(ast);
      
    }
    
    read_finished();
  }
  
  //! MPC observation file
  
  MPCObsFile::MPCObsFile() { }
  
  void MPCObsFile::Read() {
    
    // if (file == 0) Open();
    // if (status != OPEN_R) Open();
    // if (status == CLOSE) Open();
    
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    obs.clear();
    
    REWIND_FILE(file);
    
    // cerr << "...inside read_MPC()...\n";
    
    Observation dummy_obs;
    // dummy_obs.date.SetTimeScale(UTC); // checked, is UTC
    
    // double y,m,d;
    double gradi,primi,secondi;
    
    // double tmp;
    
    char line[256];
    
    // int entries = 0;
    
    string number,designation,discovery_asterisk,note1,note2;
    string date,ra,dec;
    string magnitude;
    string observatory_code;
    
    while (GETS_FILE(line,256,file) != 0) {
      
      if (strlen(line) < 80) continue; // not a good line, maybe a comment or a white line...
      // if (strlen(line) < 79) continue; // not a good line, maybe a comment or a white line...
      // if (strlen(line) < 78) continue; // not a good line, maybe a comment or a white line...
      
      // cerr << "inside while loop..." << endl;
      
      // entries--;
      
      number.assign(line,0,5);
      // cerr << "...pass (1)" << endl;
      // cerr <<"number: " << atoi(number.c_str()) << endl;
      designation.assign(line,5,7); 
      remove_leading_trailing_spaces(designation);
      // cerr <<"designation: " << designation << endl;
      discovery_asterisk.assign(line,12,1); 
      // cerr << "asterisk: [" << discovery_asterisk << "]\n";
      // cerr << "...pass (1/B)" << endl;
      note1.assign(line,13,1);
      // cerr << "...pass (1/C)" << endl;
      note2.assign(line,14,1);
      // cerr << "...pass (1/D)" << endl;
      date.assign(line,15,17);
      // cerr << "...pass (2)" << endl;
      // cerr << "date: " << date << endl;
      ra.assign(line,32,12);
      // cerr << "ra: " << ra << endl;
      dec.assign(line,44,12);
      // cerr << "dec: " << dec << endl;
      magnitude.assign(line,65,6);
      // cerr << "magnitude: " << magnitude << endl;
      observatory_code.assign(line,77,3);
      remove_leading_trailing_spaces(observatory_code);
      // cerr << "...pass (3)" << endl;
      
      dummy_obs.designation        = designation;
      dummy_obs.discovery_asterisk = discovery_asterisk;
      dummy_obs.obscode            = observatory_code;
      
      double _tmp = 0.0;
      sscanf(magnitude.c_str(),"%lf",&_tmp);
      dummy_obs.mag = _tmp;
      
      // cerr << "DES\n";
      
      // printf("DATE: %s\n",date.c_str());
      double y=0.0, m=0.0, d=0.0;
      sscanf(date.c_str(),"%lf %lf %lf",&y,&m,&d);
      // printf("LETTI: %f %f %f\n",y,m,d);
      //
      
      // dummy_obs.julian_date = GregorianToSdn((int)y,(int)m,(int)d) + ((d - floor(d)) - 0.5);
      // cerr << "JD\n";
      dummy_obs.date.SetGregor((int)y,(int)m,d,UTC);
      
      // cerr << "...pass (5)" << endl;
      
      // cout << "designation: " << designation << " " << ra << endl;      
      sscanf(ra.c_str(),"%lf %lf %lf",&gradi,&primi,&secondi);
      // angle_rad_hms(&tmp,gradi,primi,secondi);  //   dummy_obs.alpha
      // dummy_obs.alpha = tmp;
      dummy_obs.ra.SetHMS(gradi,primi,secondi);
      
      
      // cout <<"designation: " << designation << " " << dec << endl;      
      sscanf(dec.c_str(),"%lf %lf %lf",&gradi,&primi,&secondi);
      // angle_rad(&tmp,gradi,primi,secondi); //   dummy_obs.delta       
      // dummy_obs.delta = tmp;
      dummy_obs.dec.SetDPS(gradi,primi,secondi);
      
      // cerr << "OBS: " << dummy_obs.date.GetJulian() << "  " << dummy_obs.ra.GetRad() << "  " << dummy_obs.dec.GetRad() << endl;
      
      //check for a good observation
      if ((designation != "") && 
          (observatory_code != "") &&
	  (strlen(observatory_code.c_str())) == 3) {
        if ( (isalnum(observatory_code[0])) &&
	     (isalnum(observatory_code[1])) &&
	     (isalnum(observatory_code[2])) &&
	     (isspace(line[19])) &&
	     (isspace(line[22])) &&
	     (isspace(line[31])) &&
	     (isspace(line[34])) &&
	     (isspace(line[37])) &&
	     (isspace(line[43])) &&
	     (isspace(line[47])) &&
	     (isspace(line[50])) &&
	     (isspace(line[55])) ) {
	  obs.push_back(dummy_obs);
	}
      }
      // obs->push_back(dummy_obs);
      
      // cerr << "PB\n";
      
      // D_ar.Insert(dummy_obs.alpha);
      // D_dec.Insert(dummy_obs.delta);
      
    }
    
  }
  
  bool MPCObsFile::ReadNominalOrbit(OrbitWithEpoch &) {
    // true if succeed
    return false;
  }
  
  //! AstDyS observation file, usually with a .rwo extension
  
  RWOFile::RWOFile() { }
  
  void RWOFile::Read() {
    
    Open();
    
    obs.clear();
    
    REWIND_FILE(file);
    
    Observation dummy_obs;
    // dummy_obs.date.SetTimeScale(UTC); // checked, is UTC
    
    double y,m,d,p,s,h;
    
    char line[256];
    
    string designation;
    string date,ra,dec;
    string observatory_code;
    
    while (GETS_FILE(line,256,file) != 0) {
      
      // if (strlen(line) < 45) continue; // not a good line, maybe a comment or a white line...
      if (strlen(line) < 130) continue; // not a good line, maybe a comment or a white line... NOTE: radar observations are not read now, and have shorter lines
      if (line[0] == '!')     continue; // comment/header line
      
      designation.assign(line,1,9); 
      remove_leading_trailing_spaces(designation);
      
      date.assign(line,15,17);
      
      ra.assign(line,34,12);
      
      dec.assign(line,63,12);
      
      observatory_code.assign(line,114,3);
      remove_leading_trailing_spaces(observatory_code);
      
      dummy_obs.designation        = designation;
      // dummy_obs.discovery_asterisk = discovery_asterisk;
      // dummy_obs.obscode            = atoi(observatory_code.c_str());
      dummy_obs.obscode            = observatory_code;
      
      /* sscanf(magnitude.c_str(),"%lf",&tmp);
	 dummy_obs.mag = tmp;
      */
      
     
      sscanf(date.c_str(),"%lf %lf %lf",&y,&m,&d);
      dummy_obs.date.SetGregor((int)y,(int)m,d,UTC);
      
      sscanf(ra.c_str(),"%lf %lf %lf",&h,&m,&s);
      dummy_obs.ra.SetHMS(h,m,s);
      
      sscanf(dec.c_str(),"%lf %lf %lf",&d,&p,&s);
      dummy_obs.dec.SetDPS(d,p,s);
      
      //check for a good observation
      if ((designation != "") && 
          (observatory_code != "") ) {
        obs.push_back(dummy_obs);
      }
      
    }
    
  }
  
  // Locations of the observatories
  
  LocationFile::LocationFile() : ReadFile() { }
  
  void LocationFile::Read() {
    
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    string obscode,longitude,pos_xy,pos_z,name;
    
    char line[300];
    
    Location dummy_loc;
    
    // dummy_loc.present = true;
    
    REWIND_FILE(file);
    
    string stag, spre="<pre>", spreend="</pre>";
    char tag[256];
    
    // reach <pre> and skip header
    while (GETS_FILE(line,300,file) != 0) {
      sscanf(line,"%s",tag);
      stag = tag;
      if (stag == spre) {
	// skip another line (header) and break;
	GETS_FILE(line,300,file);
	break;
      }	
    }
    
    while (GETS_FILE(line,300,file) != 0) {
      
      sscanf(line,"%s",tag);
      stag = tag;
      // cerr << "stag: " << stag << endl;
      if (stag == spreend) break;
      
      if (strlen(line) < 30) continue;
      
      obscode.assign(line,0,3);
      longitude.assign(line,4,8);
      pos_xy.assign(line,13,7);
      pos_z.assign(line,21,8);
      name.assign(line,30,strlen(line)-30-1); // the last -1 is set to avoid the '\n' character in the name
      
      remove_leading_trailing_spaces(longitude);
      remove_leading_trailing_spaces(pos_xy);
      remove_leading_trailing_spaces(pos_z);
      remove_leading_trailing_spaces(name);
      
      // set zero values, for locations like SOHO, HST...
      dummy_loc.lon = dummy_loc.pxy = dummy_loc.pz = 0.0;
      
      if (longitude.size()) dummy_loc.lon  = (pi/180.0) * atof(longitude.c_str());
      if (pos_xy.size())    dummy_loc.pxy  = FromUnits(atof(pos_xy.c_str()),REARTH);
      if (pos_z.size())     dummy_loc.pz   = FromUnits(atof(pos_z.c_str()), REARTH);
      //
      dummy_loc.name = name;
      
      locations[obscode] = dummy_loc;
      
      codes.push_back(obscode);
    }
    
  }
  
  Vector LocationFile::ObsPos(const string obscode, const Date &date) {
    
    std::list<std::string>::iterator it_find = find(codes.begin(),codes.end(),obscode);
    if (it_find == codes.end()) {
      ORSA_ERROR("obscode %s not found in file %s",obscode.c_str(),GetFileName().c_str());      
      return Vector();
    }
    
    const double lon = locations[obscode].lon;
    const double pxy = locations[obscode].pxy;
    const double pz  = locations[obscode].pz;
    
#ifdef HAVE_SINCOS
    double s,c;
    ::sincos(lon,&s,&c);
    Vector obspos(pxy*c,pxy*s,pz);
#else // HAVE_SINCOS    
    Vector obspos(pxy*cos(lon),pxy*sin(lon),pz);
#endif // HAVE_SINCOS
    
    obspos.rotate(gmst(date).GetRad(),0,0);
    
    if (universe->GetReferenceSystem() == ECLIPTIC) {
      EquatorialToEcliptic_J2000(obspos);
    }
    
    return obspos;
  }
  
  // SWIFT files
  SWIFTFile::SWIFTFile(OrbitStream &osin) : ReadFile() {
    os = &osin;
    // status = CLOSE;
  }
  
  // SWIFT common data
  int    nast;
  double el[6],l_ts,w_ts,file_time;
  
  int SWIFTRawReadBinaryFile(FILE_TYPE file, const int version = 2) {
    
    // version == 1 --> not filetered data
    // version == 2 --> filtered data
    
    int good = 0;
    double dummy;
    
    if ( version == 1 ) {  
      READ_FILE(&dummy,        sizeof(int),    1,file);
      READ_FILE(&nast,         sizeof(int),    1,file);
      READ_FILE(&el,           sizeof(double), 6,file);
      READ_FILE(&file_time,    sizeof(double), 1,file);
      good = READ_FILE(&dummy, sizeof(int),    1,file);
    } else if ( version == 2 ) {
      READ_FILE(&dummy,        sizeof(int),    1,file);
      READ_FILE(&nast,         sizeof(int),    1,file);
      READ_FILE(&el,           sizeof(double), 6,file);
      READ_FILE(&l_ts,         sizeof(double), 1,file);
      READ_FILE(&w_ts,         sizeof(double), 1,file);
      READ_FILE(&file_time,    sizeof(double), 1,file);
      good = READ_FILE(&dummy, sizeof(int),    1,file);
    }
    
    // Units conversions
    file_time = FromUnits(file_time,YEAR);
    
    return (good);
  }
  
  int SWIFTFile::AsteroidsInFile() {
    
    // close and reopen to avoid odd zlib problems related to the gzseek function
    Close();
    Open();
    
    int number_of_asteroids_in_file=0;
    
    REWIND_FILE(file);
    
    int good;
    while ( (good = SWIFTRawReadBinaryFile(file,2)) != 0) {
      if (number_of_asteroids_in_file<nast) number_of_asteroids_in_file = nast;
      else if (number_of_asteroids_in_file!=0) break;
    }
    
    return (number_of_asteroids_in_file);
  }
  
  
  void SWIFTFile::Read() {
    
    // close and reopen to avoid odd zlib problems related to the gzseek function
    Close();
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    OrbitStream &ost = *os;
    
    const int version = 2;
    
    // cerr << "reading data from the input file...\n";
    
    OrbitWithEpoch fo;
    
    // double wwj=0,wj=0,cj=0;
    // double wtil,crit,wmix;
    double time_old = 0, timestep;
    
    int jump = 0, i_jump = 0;
    
    // reset!
    // ost.resize(0);
    ost.clear();
    ost.timestep = 0.0;
    const int asteroid_number =  ost.asteroid_number;
    // cerr << " SWIFTFile::Read() --> reading object: " << asteroid_number << endl;
    char label[10];
    sprintf(label,"%04i",ost.asteroid_number);
    ost.label = label;
    // cerr << "LABEL: [" << ost.label << "]" << endl;
    REWIND_FILE(file);
    
    if ( version == 1 )
      jump = 3*sizeof(int)+7*sizeof(double);
    else if ( version == 2 )
      jump = 3*sizeof(int)+9*sizeof(double);
    
    int  good = 1, check = 0, number_of_asteroids_in_file = 0;
    
    do {
      
      if (check == 0) {
	good = SWIFTRawReadBinaryFile(file,version);    
      } else {
	
	i_jump = (number_of_asteroids_in_file+asteroid_number-nast-1)%(number_of_asteroids_in_file);
	
	if (i_jump != 0) {
	  if ( (SEEK_FILE(file,jump*i_jump,SEEK_CUR)) == -1) {
	    cerr << "setting good=0 from SEEK_FILE..." << endl;
	    good = 0; 
	  }
	}
	
	if (good != 0) {
	  good = SWIFTRawReadBinaryFile(file,version);
	}
	
      }
      
      if ( number_of_asteroids_in_file < nast ) {
	number_of_asteroids_in_file = nast;
      } else {
	check = 1;
      }
      
      //// asteroid number too big!
      if ( (check == 1) && (asteroid_number > number_of_asteroids_in_file) ) {
	ORSA_ERROR("asteroid number too big (%d > %d)", asteroid_number, number_of_asteroids_in_file);
	return;
      }
      
      if (nast == asteroid_number && good != 0) {
	
	if ((file_time >= time_old) && 
	    (file_time >= ost.wp.window_start)) {
	  
	  fo.epoch.SetTime(file_time);
	  fo.a                = el[4];
	  fo.e                = el[3];
	  fo.i                = (pi/180.0)*el[2];
	  fo.omega_node       = (pi/180.0)*el[0];
	  fo.omega_pericenter = (pi/180.0)*el[1];
	  fo.M                = (pi/180.0)*el[5];
	  //
	  fo.libration_angle  = (pi/180.0)*l_ts; // temporary
	  
	  ost.push_back(fo);
	  
	  // QUICK AND DIRTY!
	  if (fo.e >= 1.0) {
	    cerr << "reading eccentricity > 1.0, returning." << endl;
	    return;
	  }
	  
	  
	  if ( ((file_time) > (ost.wp.window_amplitude+ost.wp.window_start)) && (ost.wp.window_step == 0.0) ) {
	    return;
	  }
     	}
	
	timestep = file_time - time_old;
	time_old = file_time;
	// one of all, but not the first...
	if (ost.size() == 2) {
	  ost.timestep = timestep;
	}
	
      }
      
    } while (good != 0);
  }
  
  //! orsa configuration file
  // OrsaConfigFile::OrsaConfigFile(Config *conf_in) : ReadWriteFile() {
  OrsaConfigFile::OrsaConfigFile() : ReadWriteFile() {
    
    // status = CLOSE;
    
    // conf = conf_in;
    
    char path[1024], command[1024];
    
    // needed to avoid some odd segfaults...
    // OrsaPaths p; // make sure the constructor gets called
    
    // cerr << "OrsaPaths::work_path() = " << OrsaPaths::work_path() << endl;
    
    strcpy(path, OrsaPaths::work_path());
#ifndef _WIN32    
    sprintf(command,"mkdir -p %s",path);
    system(command);
#else
    _mkdir(path);
#endif    
    strcat(path,"config");
    
    SetFileName(path);
    
    list_enum.push_back(JPL_EPHEM_FILE);
    list_enum.push_back(JPL_DASTCOM_NUM);
    list_enum.push_back(JPL_DASTCOM_UNNUM);
    list_enum.push_back(JPL_DASTCOM_COMET);
    list_enum.push_back(LOWELL_ASTORB);
    list_enum.push_back(MPC_MPCORB);
    list_enum.push_back(MPC_COMET);
    list_enum.push_back(MPC_NEA);
    list_enum.push_back(MPC_DAILY);
    list_enum.push_back(MPC_DISTANT);
    list_enum.push_back(MPC_PHA);
    list_enum.push_back(MPC_UNUSUALS);
    list_enum.push_back(ASTDYS_ALLNUM_CAT);
    list_enum.push_back(ASTDYS_ALLNUM_CTC);
    list_enum.push_back(ASTDYS_ALLNUM_CTM);
    list_enum.push_back(ASTDYS_UFITOBS_CAT);
    list_enum.push_back(ASTDYS_UFITOBS_CTC);
    list_enum.push_back(ASTDYS_UFITOBS_CTM);
    list_enum.push_back(NEODYS_CAT);
    list_enum.push_back(NEODYS_CTC);
    list_enum.push_back(OBSCODE);
    // TLE
    list_enum.push_back(TLE_NASA);
    list_enum.push_back(TLE_GEO);
    list_enum.push_back(TLE_GPS);
    list_enum.push_back(TLE_ISS);
    list_enum.push_back(TLE_KEPELE);
    list_enum.push_back(TLE_VISUAL);
    list_enum.push_back(TLE_WEATHER);
    // textures
    list_enum.push_back(TEXTURE_SUN);
    list_enum.push_back(TEXTURE_MERCURY);
    list_enum.push_back(TEXTURE_VENUS);
    list_enum.push_back(TEXTURE_EARTH);
    list_enum.push_back(TEXTURE_MOON);
    list_enum.push_back(TEXTURE_MARS);
    list_enum.push_back(TEXTURE_JUPITER);
    list_enum.push_back(TEXTURE_SATURN);
    list_enum.push_back(TEXTURE_URANUS);
    list_enum.push_back(TEXTURE_NEPTUNE);
  }
  
  void OrsaConfigFile::Read() {
    
    // if (file == 0) Open();
    // if (status == CLOSE) Open();
    
    Open(OPEN_R);
    
    // should improve this check
    // if (status != OPEN_R) return;
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    char line[1024];    
    string stag, svalue;
    
    REWIND_FILE(file);
    
    while (GETS_FILE(line,1024,file) != 0) {
      
      {
	// the first white space is the separator between tag and value
      	string s_line=line;
	string::size_type white_space_pos;
	white_space_pos = s_line.find(" ",0);
	if (white_space_pos != string::npos) {
	  stag.assign(s_line,0,white_space_pos);
	  svalue.assign(s_line,white_space_pos+1,s_line.size()-white_space_pos-2);
	  remove_leading_trailing_spaces(stag);
	  remove_leading_trailing_spaces(svalue);
	  // 
	  // cerr << "tag -->" << stag << "<--     value -->" << svalue << "<-- " << endl;
	}
      }
      
      if (svalue.size()>0) {
	
	list<ConfigEnum>::const_iterator it = list_enum.begin();
	while (it != list_enum.end()) {
	  if (stag == config->paths[(*it)]->tag) {
	    config->paths[(*it)]->SetValue(svalue);
	    break;
	  }
	  ++it;
	}
	
      }
      
    }
    
    Close();
  }
  
  void OrsaConfigFile::Write() {
    
    // this close is necessary to avoid multiple write of the same options
    Close();
    
    // *** TODO: make a backup copy before to save the new one! *** 
    
    Open(OPEN_W);
    
    if (status != OPEN_W) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    // cerr << "OrsaConfigFile::Write() ==> " << filename << endl;
    
    // rewind(file);
    
    char line[1024];
    
    list<ConfigEnum>::const_iterator it = list_enum.begin();
    while (it != list_enum.end()) {
      sprintf(line,"%s %s\n",config->paths[(*it)]->tag.c_str(),config->paths[(*it)]->GetValue().c_str());   
      PUTS_FILE(line,file);
      ++it;
    }
    
    FLUSH_FILE(file);
    
    Close(); 
  }
  
  
  //! OrsaFile
  
#define SWAP_MACRO( A, B, TEMP)   { (TEMP) = (A);  (A) = (B);  (B) = (TEMP); }
  
  static void swap_4(void *ptr) {
    char *tptr = (char *)ptr, tchar;
    
    SWAP_MACRO( tptr[0], tptr[3], tchar);
    SWAP_MACRO( tptr[1], tptr[2], tchar);
  }
  
  static void swap_8(void *ptr) {
    char *tptr = (char *)ptr, tchar;
    
    SWAP_MACRO( tptr[0], tptr[7], tchar);
    SWAP_MACRO( tptr[1], tptr[6], tchar);
    SWAP_MACRO( tptr[2], tptr[5], tchar);
    SWAP_MACRO( tptr[3], tptr[4], tchar);
  }
  
  static void swap(void *ptr, unsigned int size) {
    switch(size) {
    case 4: swap_4(ptr); break;
    case 8: swap_8(ptr); break;
    default:
      ORSA_WARNING("called read_swap with size = %i",size);
      break;
    }
  }
  
  size_t OrsaFile::read_swap(void *ptr, unsigned int size) {
    const size_t s = READ_FILE(ptr,size,1,file);
    if (swap_bytes) {
      swap(ptr,size);
    }
    return s;
  }
  
  OrsaFile::OrsaFile() : ReadWriteFile() { }
  
  void OrsaFile::Write() {
    
    Open(OPEN_W);
    
    if (status != OPEN_W) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    if (!universe) {
      ORSA_ERROR("cannot write a non-allocated universe!");
      return;
    }
    
    Write(&universe);
    
    FLUSH_FILE(file);
    
    Close();
  }
  
  void OrsaFile::Read() {
    
    Open(OPEN_R);
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    Read(&universe);
    
    Close();
    
    ORSA_DEBUG("ORSA file %s [ORSA version: %s, byte order: %i, evolutions: %i, units: [%s,%s,%s]]",
	       GetFileName().c_str(), orsa_version.c_str(), byte_order,universe->size(),
	       LengthLabel().c_str(), MassLabel().c_str(),TimeLabel().c_str());
  }
  
  void OrsaFile::Write(Universe **u) {
    
    // endian issues
    byte_order = BYTEORDER; // from config.h
    Write(&byte_order);
    
    // various info...
    orsa_version = ORSA_VERSION;
    Write(&orsa_version);
    
    time_unit   tu = units->GetTimeBaseUnit();
    length_unit lu = units->GetLengthBaseUnit();
    mass_unit   mu = units->GetMassBaseUnit();
    //
    Write(&tu); Write(&lu); Write(&mu);
    
    UniverseType ut = (*u)->GetUniverseType();
    Write(&ut);
    
    ReferenceSystem rs = (*u)->GetReferenceSystem();
    Write(&rs);
    
    TimeScale ts = (*u)->GetTimeScale();
    Write(&ts);
    
    Write(&((*u)->name));
    Write(&((*u)->description));
    
    unsigned int j;
    for(j=0;j<(*u)->size();j++) {
      if ((*(*u))[j]!=0) Write(&(*(*u))[j]);
    }
  }
  
  void OrsaFile::make_new_universe(Universe **u, length_unit lu, mass_unit mu, time_unit tu, UniverseType ut, ReferenceSystem rs, TimeScale ts) {    
    delete (*u);    
    (*u) = new Universe(lu,mu,tu,ut,rs,ts);
  }
  
  void OrsaFile::Read(Universe **u) {
    
    swap_bytes = false;
    Read(&byte_order);
    if (byte_order != BYTEORDER) {
      swap_bytes = true;
      swap(&byte_order,sizeof(byte_order));
    }
    
    Read(&orsa_version);
    
    time_unit   tu;
    length_unit lu;
    mass_unit   mu;
    //
    Read(&tu);
    Read(&lu);
    Read(&mu);
    
    UniverseType ut;
    Read(&ut);
    
    ReferenceSystem rs;;
    Read(&rs);
    
    TimeScale ts;
    Read(&ts);
    
    make_new_universe(u,lu,mu,tu,ut,rs,ts);
    
    Read(&((*u)->name));
    Read(&((*u)->description));
    
    Read(&last_ofdt_read); // the others are read by the Read(evol..)
    /* 
       { // debug
       ORSA_ERROR("Read(Universe *u)  ofdt = %i",last_ofdt_read);
       }
    */
    while (last_ofdt_read == OFDT_EVOLUTION) {
      Evolution * e = 0;  
      Read(&e);
      (*u)->push_back(e);
    }
  }
  
  void OrsaFile::Write(Evolution **e) {
    
    OrsaFileDataType t = OFDT_EVOLUTION; Write(&t);
    
    Write(&((*e)->name));
    UniverseTypeAwareTimeStep sp = (*e)->GetSamplePeriod(); Write(&sp);
    // const Integrator * itg = (*e)->GetIntegrator(); Write(&itg);
    Write((*e)->GetIntegrator());
    // Write(&((*e)->interaction));
    Write((*e)->GetInteraction());
    
    unsigned int n = (*e)->start_bodies.size();
    Write(&n);
    for(unsigned int j=0;j<n;j++) {
      Write(&((*e)->start_bodies[j]));
    }
    //
    if (universe->GetUniverseType() == Real) {
      n = (*e)->start_JPL_bodies.size();
      Write(&n);
      for(unsigned int j=0;j<n;++j) {
	Write(&((*e)->start_JPL_bodies[j]));
      }
    }
    
    // the first only
    if ((*e)->size() > 0) Write(&((*(*e))[0]));
    
    // from the second on, write only position and velocity
    for(unsigned int j=1;j<(*e)->size();++j) {
      Write(&((*(*e))[j]),true);
    }
  }
  
  void OrsaFile::make_new_evolution(Evolution **e) {
    delete (*e);
    (*e) = new Evolution;
  }
  
  void OrsaFile::Read(Evolution **e) {
    
    string name;
    Read(&name);
    
    // double sample_period;
    UniverseTypeAwareTimeStep sample_period;
    Read(&sample_period);
    
    Integrator * integrator = 0;
    Read(&integrator);
    
    Interaction * interaction = 0;
    Read(&interaction);
    
    make_new_evolution(e);
    
    (*e)->clear();
    
    (*e)->name          = name;
    (*e)->SetSamplePeriod(sample_period);
    (*e)->SetIntegrator(integrator);
    (*e)->SetInteraction(interaction);
    
    delete integrator;
    integrator = 0;
    
    delete interaction;
    interaction = 0;
    
    unsigned int n;
    Read(&n);
    (*e)->start_bodies.resize(n);
    for(unsigned int j=0;j<n;j++) {
      Read(&((*e)->start_bodies[j]));
    }
    //
    if (universe->GetUniverseType() == Real) {
      Read(&n);
      (*e)->start_JPL_bodies.clear();
      
      JPL_planets tmp_jp;
      for(unsigned int j=0;j<n;j++) {
	Read(&tmp_jp);
	(*e)->start_JPL_bodies.push_back(tmp_jp);
      }
    }
    
    // we REALLY need a Frame to keep all the constant values....
    Frame f;
    
    Read(&last_ofdt_read);
    /* 
       { // debug
       ORSA_ERROR("Read(Evolution *e)  ofdt = %i",last_ofdt_read);
       }
    */
    if (last_ofdt_read == OFDT_FRAME) {
      // the first is different from the others
      Read(&f);
      (*e)->push_back(f);
    }
    
    Read(&last_ofdt_read);
    /* 
       { // debug
       ORSA_ERROR("Read(Evolution *e)  ofdt = %i",last_ofdt_read);
       }
    */
    while (last_ofdt_read == OFDT_FRAME) {
      Read(&f,true);
      (*e)->push_back(f);
      Read(&last_ofdt_read);
    }
  }
  
  void OrsaFile::Write(Frame * f, bool write_only_r_v) {
    
    OrsaFileDataType t = OFDT_FRAME; Write(&t);
    
    UniverseTypeAwareTime f_time = *f;
    Write(&f_time);
    unsigned int n = f->size();
    Write(&n);
    // unsigned int j;
    Vector v;
    if (write_only_r_v) {
      for(unsigned int j=0;j<n;j++) {
	v = (*f)[j].position(); Write(&v);
	v = (*f)[j].velocity(); Write(&v);
      }
    } else {
      for(unsigned int j=0;j<n;j++) {
	Write(&((*f)[j]));
      }
    }
  }
  
  void OrsaFile::Read(Frame * f, bool read_only_r_v) {
    
    UniverseTypeAwareTime f_time;
    Read(&f_time);
    f->SetTime(f_time);
    unsigned int n = f->size();
    Read(&n);
    f->resize(n);
    unsigned int j;
    Vector v;
    if (read_only_r_v) {
      for(j=0;j<n;j++) {
	Read(&v); (*f)[j].SetPosition(v);
	Read(&v); (*f)[j].SetVelocity(v);
      }
    } else {
      for(j=0;j<n;j++) { 
	Read(&((*f)[j]));
      }
    }
  }
  
  void OrsaFile::Write(Body *b) {
    string b_name   = b->name();   Write(&b_name);
    double b_mass   = b->mass();   Write(&b_mass);
    double b_radius = b->radius(); Write(&b_radius);
    JPL_planets b_planet = b->JPLPlanet(); Write(&b_planet);
    Vector v;
    v = b->position(); Write(&v);
    v = b->velocity(); Write(&v);
  }
  
  void OrsaFile::Read(Body *b) {
    string b_name;   Read(&b_name);
    double b_mass;   Read(&b_mass);
    double b_radius; Read(&b_radius);
    JPL_planets b_planet; Read(&b_planet);
    
    *b = Body(b_name,b_mass,b_radius,b_planet);
    
    Vector v;
    Read(&v); b->SetPosition(v);
    Read(&v); b->SetVelocity(v);
  }
  
  void OrsaFile::Write(BodyWithEpoch * b) {
    Write((Body*)b);
    UniverseTypeAwareTime b_epoch = b->Epoch(); Write(&b_epoch);
  }
  
  void OrsaFile::Read(BodyWithEpoch * b) {
    Read((Body*)b);
    UniverseTypeAwareTime b_epoch; Read(&b_epoch); b->SetEpoch(b_epoch);
  }
  
  void OrsaFile::Write(Date *d) {
    double j = d->GetJulian(); Write(&j);
  }
  
  void OrsaFile::Read(Date *d) {
    double j; Read(&j); d->SetJulian(j);
  }
  
  void OrsaFile::Write(UniverseTypeAwareTime * t) {
    switch (universe->GetUniverseType()) {
    case Real: {
      Date d = t->GetDate(); Write(&d);
      break;
    }
    case Simulated: {
      double tt = t->GetTime(); Write(&tt);
      break;
    }
    }
  }
  
  void OrsaFile::Read(UniverseTypeAwareTime *t) {
    switch (universe->GetUniverseType()) {
    case Real: {
      Date d; Read(&d); t->SetDate(d);
      break;
    }
    case Simulated: {
      double tt; Read(&tt); t->SetTime(tt);
      break;
    }
    }
  }
  
  void OrsaFile::Write(UniverseTypeAwareTimeStep *ts_in) {
    switch (universe->GetUniverseType()) {
    case Real: {
      TimeStep _ts = ts_in->GetTimeStep(); Write(&_ts);
      break;
    }
    case Simulated: {
      double tt = ts_in->GetDouble(); Write(&tt);
      break;
    }
    }
  }
  
  void OrsaFile::Read(UniverseTypeAwareTimeStep *ts_in) {
    switch (universe->GetUniverseType()) {
    case Real: {
      TimeStep _ts; Read(&_ts); ts_in->SetTimeStep(_ts);
      break;
    }
    case Simulated: {
      double tt; Read(&tt); ts_in->SetDouble(tt);
      break;
    }
    }
  }
  
  // void OrsaFile::Write(Integrator **i) {
  void OrsaFile::Write(const Integrator * i) {
    
    //  IntegratorType it = (*i)->GetType();
    IntegratorType it = i->GetType();
    Write(&it);
    
    // UniverseTypeAwareTimeStep ts = (*i)->timestep;
    UniverseTypeAwareTimeStep ts = i->timestep;
    Write(&ts);
    
    // double a = (*i)->accuracy;
    double a = i->accuracy;
    Write(&a);
    
    // unsigned int m = (*i)->m;
    unsigned int m = i->m;
    Write(&m);
  }
  
  void OrsaFile::Read(Integrator **i) {
    
    IntegratorType type; Read(&type);
    make_new_integrator(i, type);
    
    UniverseTypeAwareTimeStep ts;
    Read(&ts);
    (*i)->timestep = ts;
    
    double a;       Read(&a);
    unsigned int m; Read(&m);
    
    (*i)->accuracy = a;
    (*i)->m        = m;
  }
  
  /* 
     void OrsaFile::Write(const Interaction * i) {
     InteractionType type = i->GetType(); Write(&type); 
     }
     
     void OrsaFile::Read(Interaction **i) {
     InteractionType type; Read(&type);
     make_new_interaction(i, type);
     }
  */
  
  void OrsaFile::Write(const Interaction * i) {
    InteractionType type = i->GetType(); Write(&type); 
    bool b = i->IsSkippingJPLPlanets();  Write(&b);
    if (type == NEWTON) {
      const Newton * newton = dynamic_cast <const Newton *> (i);
      if (newton) {
	b = newton->IsIncludingMultipoleMoments();        Write(&b);
  	b = newton->IsIncludingRelativisticEffects();     Write(&b);
  	b = newton->IsIncludingFastRelativisticEffects(); Write(&b);
      } else {
        b = false;
	Write(&b);
	Write(&b);
	Write(&b);
      }
    }
  }
  
  void OrsaFile::Read(Interaction **i) {
    InteractionType type; Read(&type);
    make_new_interaction(i, type);
    bool b; Read(&b); (*i)->SkipJPLPlanets(b);
    if (type == NEWTON) {
      Newton * newton = dynamic_cast <Newton *> (*i);
      if (newton) {
       	Read(&b); newton->IncludeMultipoleMoments(b);
	Read(&b); newton->IncludeRelativisticEffects(b);
	Read(&b); newton->IncludeFastRelativisticEffects(b);
      } else {
        b = false;
	Read(&b);
    	Read(&b);
	Read(&b);
      }
    }
  }
  
  void OrsaFile::Write(std::string * s) {
    const unsigned int size = s->size();
    unsigned int n = 1 + size;
    Write(&n);
    char * name = (char*)malloc(n*sizeof(char));
    // 
    // strcpy(name,s->c_str());
    {
      unsigned int i;
      for (i=0;i<size;++i) {
	name[i] = (*s)[i];
      }
      name[size] = '\0';
    }
    //
    WRITE_FILE(name,sizeof(char),n,file);
    /* 
    ORSA_ERROR("Write(std::string *s)  n = %i   s = %s   s->size() = %i   strlen(s->c_str()) = %i",
      n,s->c_str(),s->size(),strlen(s->c_str()));
    */
    free(name);
    { // check
      if (strlen(s->c_str()) > n) {
	ORSA_ERROR("string length problem...");
      }
    }
  }
  
  void OrsaFile::Read(std::string * s) {
    unsigned int n; 
    Read(&n);
    if (n > 0) {
      char * name = (char*)malloc(n*sizeof(char));
      READ_FILE(name,sizeof(char),n,file);
      *s = name;
      /* 
      ORSA_ERROR("Read(std::string *s)  n = %i   s = %s   s->size() = %i   strlen(s->c_str()) = %i   name = %s",
        n,s->c_str(),s->size(),strlen(s->c_str()),name);
      */
      free(name);
    }
  }
  
  void OrsaFile::Write(orsa::Vector *v) {
    Write(&v->x); 
    Write(&v->y);
    Write(&v->z);
  }
  
  void OrsaFile::Read(orsa::Vector *v) {
    Read(&v->x); 
    Read(&v->y); 
    Read(&v->z);
  }
  
  void OrsaFile::Write(bool * b) {
    WRITE_FILE(b,sizeof(bool),1,file);
  }
  
  void OrsaFile::Read(bool * b) {
    read_swap(b,sizeof(bool));
  }
  
  void OrsaFile::Write(unsigned int * i) {
    WRITE_FILE(i,sizeof(unsigned int),1,file);
  }
  
  void OrsaFile::Read(unsigned int * i) {
    read_swap(i,sizeof(unsigned int));
  }
  
  void OrsaFile::Write(int *i) {
    WRITE_FILE(i,sizeof(int),1,file);
  }
  
  void OrsaFile::Read(int *i) {
    read_swap(i,sizeof(int));
  }
  
  void OrsaFile::Write(double * d) {
    WRITE_FILE(d,sizeof(double),1,file);
  }
  
  void OrsaFile::Read(double * d) {
    read_swap(d,sizeof(double));
  }
  
  /* 
     void OrsaFile::Write(bool *b) {
     // WRITE_FILE(b,sizeof(bool),1,file);
     unsigned int i = *b;
     Write(&i);
     }
     
     void OrsaFile::Read(bool *b) {
     // read_swap(b,sizeof(bool));
     unsigned int i;
     Read(&i);
     *b = (bool)i; 
     }
  */
  
  void OrsaFile::Write(IntegratorType *it) {
    // WRITE_FILE(it,sizeof(IntegratorType),1,file);
    unsigned int i = *it;
    Write(&i);
  }
  
  void OrsaFile::Read(IntegratorType *it) {
    // read_swap(it,sizeof(IntegratorType));
    unsigned int i;
    Read(&i);
    convert(*it,i);
  }
  
  void OrsaFile::Write(InteractionType *it) {
    // WRITE_FILE(it,sizeof(InteractionType),1,file);
    unsigned int i = *it;
    Write(&i);
  }
  
  void OrsaFile::Read(InteractionType *it) {
    // read_swap(it,sizeof(InteractionType));
    unsigned int i;
    Read(&i);
    convert(*it,i);
  }
  
  void OrsaFile::Write(time_unit *tu) {
    // WRITE_FILE(tu,sizeof(time_unit),1,file);
    unsigned int i = *tu;
    Write(&i);
  }
  
  void OrsaFile::Read(time_unit *tu) {
    // read_swap(tu,sizeof(time_unit));
    unsigned int i;
    Read(&i);
    convert(*tu,i);
  }
  
  void OrsaFile::Write(length_unit *lu) {
    // WRITE_FILE(lu,sizeof(length_unit),1,file);
    unsigned int i = *lu;
    Write(&i);
  }
  
  void OrsaFile::Read(length_unit *lu) {
    // read_swap(lu,sizeof(length_unit));
    unsigned int i;
    Read(&i); 
    convert(*lu,i);
  }
  
  void OrsaFile::Write(mass_unit *mu) {
    // WRITE_FILE(mu,sizeof(mass_unit),1,file);
    unsigned int i = *mu;
    Write(&i);
  }
  
  void OrsaFile::Read(mass_unit *mu) {
    // read_swap(mu,sizeof(mass_unit));
    unsigned int i;
    Read(&i);
    convert(*mu,i);
  }
  
  void OrsaFile::Write(ReferenceSystem *rs) {
    // WRITE_FILE(rs,sizeof(ReferenceSystem),1,file);
    unsigned int i = *rs;
    Write(&i);
  }
  
  void OrsaFile::Read(ReferenceSystem *rs) {
    // read_swap(rs,sizeof(ReferenceSystem));
    unsigned int i;
    Read(&i);
    convert(*rs,i);
  }
  
  void OrsaFile::Write(UniverseType *ut) {
    // WRITE_FILE(ut,sizeof(UniverseType),1,file);
    unsigned int i = *ut;
    Write(&i);
  }
  
  void OrsaFile::Read(UniverseType *ut) {
    // read_swap(ut,sizeof(UniverseType));
    unsigned int i;
    Read(&i);
    convert(*ut,i);
  }
  
  void OrsaFile::Write(TimeScale *ts) {
    // WRITE_FILE(ts,sizeof(TimeScale),1,file);
    unsigned int i = *ts;
    Write(&i);
  }
  
  void OrsaFile::Read(TimeScale *ts) {
    // read_swap(ts,sizeof(TimeScale));
    unsigned int i;
    Read(&i);
    convert(*ts,i);
  }
  
  void OrsaFile::Write(OrsaFileDataType *ofdt) {
    // WRITE_FILE(ofdt,sizeof(OrsaFileDataType),1,file);
    unsigned int i = *ofdt;
    Write(&i);
  }
  
  /* 
     void OrsaFile::Read(OrsaFileDataType *ofdt) {
     const int val = read_swap(ofdt,sizeof(OrsaFileDataType));
     if (val==0) *ofdt = OFDT_END_OF_FILE;
     }
  */
  
  void OrsaFile::Read(OrsaFileDataType *ofdt) {
    // const int val = read_swap(ofdt,sizeof(OrsaFileDataType));
    // if (val==0) *ofdt = OFDT_END_OF_FILE;
    unsigned int i;
    const int val = read_swap(&i,sizeof(unsigned int));
    // convert(*ofdt,i);
    // if (val==0) *ofdt = OFDT_END_OF_FILE;
    if (val==0) {
      *ofdt = OFDT_END_OF_FILE;
    } else {
      convert(*ofdt,i);
    }
  }
  
  void OrsaFile::Write(JPL_planets *jp) {
    // WRITE_FILE(jp,sizeof(JPL_planets),1,file);
    unsigned int i = *jp;
    Write(&i);
  }
  
  void OrsaFile::Read(JPL_planets *jp) {
    // read_swap(jp,sizeof(JPL_planets));
    unsigned int i;
    Read(&i);
    convert(*jp,i);
  }
  
  void OrsaFile::Write(TimeStep * ts) {
    unsigned int days = ts->days(); 
    Write(&days);
    //
    unsigned int day_fraction = ts->day_fraction();
    Write(&day_fraction); 
    //
    int sign = ts->sign();
    Write(&sign);
  }
  
  void OrsaFile::Read(TimeStep * ts) {
    unsigned int days;
    Read(&days);
    unsigned int day_fraction;
    Read(&day_fraction);
    int sign;
    Read(&sign);
    *ts = TimeStep(days,day_fraction,sign);
  }
  
  // very simple check, better than nothing for the moment...
  bool OrsaFile::GoodFile(const string &filename) {
    
    // define locally this variables, to allow
    // the GoodFile() method to be static
    unsigned int byte_order;
    FILE_TYPE    file;
    
    if ((file = OPEN_FILE(filename.c_str(),OPEN_READ)) == 0) return false;
    
    READ_FILE(&byte_order,sizeof(byte_order),1,file);
    
    if ( (byte_order != 1234) &&
	 (byte_order != 4321) ) {
      
      swap(&byte_order,sizeof(byte_order));
      
      if ( (byte_order != 1234) &&
	   (byte_order != 4321) ) {
	
	CLOSE_FILE(file);
	return false;
	
      }
    }
    
    CLOSE_FILE(file);
    return true;
  }
  
  //! Modified Radau input files
  RadauModIntegrationFile::RadauModIntegrationFile(OrbitStream &osin) : ReadFile() {
    os = &osin;
  }
  
  void RadauModIntegrationFile::Read() {
    
    // if (status == CLOSE) Open();
    
    Open();
    
    if (status != OPEN_R){ 
      ORSA_ERROR("problems encountered when opening file.");
      return;
    }
    
    os->clear();
    os->timestep = 0.0;
    OrbitWithEpoch fo;
    REWIND_FILE(file); 
    
    double a,e,i,omega_per,omega_nod,M;
    double time,time_old=0,timestep;
    
    char line[1024];
    
    /* while ( (fscanf(file,"%lf %lf %lf %lf %lf %lf %lf",
       &time,&a,&e,&i,&M,&omega_per,&omega_nod)) != EOF ) {
    */
    
    while (GETS_FILE(line,1024,file) != 0) {
      
      sscanf(line,"%lf %lf %lf %lf %lf %lf %lf",
	     &time,&a,&e,&i,&M,&omega_per,&omega_nod);
      
      timestep  = time - time_old;
      time_old  = time;
      if (os->size() == 2) { 
	os->timestep = FromUnits(timestep,YEAR); // read in days, save in the current time units
	// cerr << "timestep set to: " << os->timestep << endl;
      }
      
      fo.epoch = FromUnits(time,YEAR); // read in days, save in the current time units
      
      fo.a                = FromUnits(a,AU);
      fo.e                = e;
      fo.i                = (pi/180.0)*i;
      fo.omega_node       = (pi/180.0)*omega_nod;
      fo.omega_pericenter = (pi/180.0)*omega_per;
      fo.M                = (pi/180.0)*M;
      
      os->push_back(fo);  
      
      // QUICK AND DIRTY!
      if (fo.e >= 1.0) {
       	ORSA_ERROR("reading eccentricity > 1.0, returning.");
	return;
      }
      
    }
    
  }
  
  // AstDySMatrixFile
  
  AstDySMatrixFile::AstDySMatrixFile() : AsteroidDatabaseFile() {
    db = new AsteroidDatabase();
  }
  
  AstDySMatrixFile::~AstDySMatrixFile() {
    delete db;
    db = 0;
  }
  
  void AstDySMatrixFile::Read() {
    
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    REWIND_FILE(file);
    
    char line[1024],tag[10];
    string stag;
    const string end_of_header="END_OF_HEADER";
    
    // skip header
    while (GETS_FILE(line,1024,file)!=0) {
      sscanf(line,"%s",tag);
      stag = tag;
      if (stag == end_of_header) {
	break;
      }
    }
    
    Asteroid ast;
    
    double cov_content[21];
    int cov_line;
    
    unsigned int local_index = 0;
    bool bool_stop=false;
    bool bool_pause=false;
    
    while (GETS_FILE(line,1024,file)!=0) {
      
      if (line[0] == '!') continue; // comment/header line
      
      if (line[0] == ' ') continue; // not the line number
      
      ++local_index;
      read_progress(local_index,bool_pause,bool_stop);
      
      if (bool_stop) break;
      
      while (bool_pause) {
    	sleep(1);
	read_progress(local_index,bool_pause,bool_stop);
      }
      
      sscanf(line,"%s",tag);
      stag = tag;
      remove_leading_trailing_spaces(stag);     
      
      ast.name = stag;
      
      {
	const string name=stag;
	char c;
	unsigned int ck;
	bool is_only_digit=true;
	for (ck=0;ck<name.size();++ck) {
	  c = name[ck];
	  if (isalpha(c)) { 
	    is_only_digit=false;
	    break;
	  }
	}
	
	if (is_only_digit) {
	  ast.n = atoi(name.c_str());
	} else {
	  ast.n = 0;
	}
      }
      
#ifdef HAVE_GSL
      OrbitWithCovarianceMatrixGSL orbit;
#else
      OrbitWithEpoch orbit;
#endif
      
      orbit.mu = GetG()*GetMSun();
      
      cov_line = 0;
      
      while (GETS_FILE(line,1024,file)!=0) {
	
	if (line[0] == '!') continue; // comment/header line
	
	if (line[0] != ' ') break; // new asteroid
	
	sscanf(line,"%s",tag);
	stag = tag;
	
	if (stag == "EQU") {
	  
	  double x[6];
	  
	  sscanf(line,"%s %lg %lg %lg %lg %lg %lg",tag,&x[0],&x[1],&x[2],&x[3],&x[4],&x[5]);
	  
	  const double omega_tilde = secure_atan2(x[1],x[2]);
	  
	  orbit.a                  = x[0];
	  orbit.e                  = sqrt(x[1]*x[1]+x[2]*x[2]);
	  orbit.i                  = 2.0*atan(sqrt(x[3]*x[3]+x[4]*x[4]));
	  orbit.omega_node         = fmod(10.0*twopi+secure_atan2(x[3],x[4]),twopi);
	  orbit.omega_pericenter   = fmod(10.0*twopi+omega_tilde-orbit.omega_node,twopi);
	  orbit.M                  = fmod(10.0*twopi+(pi/180)*x[5]-omega_tilde,twopi);
	  
	} else if (stag == "MJD") {
	  
	  double epoch_MJD;
	  sscanf(line,"%s %lg",tag,&epoch_MJD);
	  Date  epoch;
	  epoch.SetJulian(epoch_MJD+2400000.5,TDT);
	  
	  orbit.epoch.SetDate(epoch);
	  
	} else if (stag == "MAG") {
	  
	  double mag, m2;
	  sscanf(line,"%s %lg %lg",tag,&mag,&m2);
	  
	  ast.mag = mag;
	  
	} else if (stag == "COV") {
	  
	  double c0,c1,c2;	    
	  
	  sscanf(line,"%s %lg %lg %lg",tag,&c0,&c1,&c2);
	  
	  cov_content[cov_line*3]   = c0;
	  cov_content[cov_line*3+1] = c1;
	  cov_content[cov_line*3+2] = c2;
	  
	  cov_line++;
	}
	
	if (cov_line==7) break;
	
      }
      
#ifdef HAVE_GSL
      if (cov_line==7) {
	double covm[6][6];
	int i,k;
	int ik=0;
	for (i=0;i<6;++i) {
	  for (k=i;k<6;++k) {
	    // IMPORTANT: units correction
	    if (i==5) cov_content[ik] *= (pi/180);
	    if (k==5) cov_content[ik] *= (pi/180);
	    //
	    covm[i][k] = cov_content[ik];
	    covm[k][i] = covm[i][k];
	    //
	    ++ik;
	  }
	}	
	orbit.SetCovarianceMatrix((double**)covm,Equinoctal);
      } else {
	ORSA_ERROR("Cannot set covariance matrix for object %s from file %s.",ast.name.c_str(),filename.c_str());
      }      
#endif // HAVE_GSL
      
      ast.orb = orbit;
      
      // CHECK THIS CODE!
      // NB: DON'T KNOW HOW TO 'ROTATE' THE COVARIANCE MATRIX
      /* 
	 switch (universe->GetReferenceSystem()) {
	 case ECLIPTIC: break;
	 case EQUATORIAL:
	 { 
	 Date tmp_date(TDT);
	 // cerr << "Rotating astorb orbit..." << endl;
	 const double obleq_rad = obleq(tmp_date).GetRad();
	 Vector position,velocity;
	 ast.orb.RelativePosVel(position,velocity);
	 position.rotate(0.0,obleq_rad,0.0);
	 velocity.rotate(0.0,obleq_rad,0.0);
	 ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	 }
	 break;
	 }
      */
      //
      switch (universe->GetReferenceSystem()) {
      case ECLIPTIC: break;
      case EQUATORIAL:
	{ 
	  Vector position,velocity;
	  ast.orb.RelativePosVel(position,velocity);
	  EclipticToEquatorial_J2000(position);
	  EclipticToEquatorial_J2000(velocity);
	  ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	}
	break;
      }
      
      db->push_back(ast);
    }
    
    read_finished();
  }
  
  // JPL DASTCOM files
  
  JPLDastcomNumFile::JPLDastcomNumFile() : AsteroidDatabaseFile() {
    db = new AsteroidDatabase();
  }
  
  JPLDastcomNumFile::~JPLDastcomNumFile() {
    delete db;
    db = 0;
  }
  
  void JPLDastcomNumFile::Read() {
    
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    db->clear();
    
    char line[300];
    
    double a,e,i,omega_node,omega_pericenter,M;
    // int    n;
    string number,name,orbit_computer,absolute_magnitude,arc,numobs,epoch;
    string mean_anomaly,pericenter,node,inclination,eccentricity,semimajor_axis;
    // string ceu;
    
    string year,month,day;
    // int    y,m,d;
    
    Asteroid ast;
    
    // Date tmp_date(TDT);
    Date tmp_date;
    
    unsigned int local_index = 0;
    bool bool_stop=false;
    bool bool_pause=false;
    REWIND_FILE(file);
    while ((GETS_FILE(line,300,file)) != 0) {
      
      if (strlen(line) < 100) continue; // not a good line, maybe a comment or a white line...
      
      if (line[0]=='-') continue; // comment
      
      local_index++;
      read_progress(local_index,bool_pause,bool_stop);
      
      if (bool_stop) break;
      
      while (bool_pause) {
	sleep(1);
	read_progress(local_index,bool_pause,bool_stop);
      }
      
      // uncomment the ones used
      number.assign(line,0,5);
      name.assign(line,6,17); 
      
      epoch.assign(line,24,5);
      //
      semimajor_axis.assign(line,30,10);
      eccentricity.assign(line,41,10);
      inclination.assign(line,52,9);
      pericenter.assign(line,62,9);
      node.assign(line,72,9);
      mean_anomaly.assign(line,82,11);
      //////////////
      
      ast.n = atoi(number.c_str());
      
      ast.name = name;
      remove_leading_trailing_spaces(ast.name);
      
      a                = atof(semimajor_axis.c_str());
      e                = atof(eccentricity.c_str());
      i                = (pi/180)*atof(inclination.c_str());
      omega_node       = (pi/180)*atof(node.c_str());
      omega_pericenter = (pi/180)*atof(pericenter.c_str());
      M                = (pi/180)*atof(mean_anomaly.c_str());
      
      // checks
      if ((ast.n==0) || (a==0.0)) {
	// bad line...
	continue;
      }
      
      ast.orb.a                = FromUnits(a,AU);
      ast.orb.e                = e;
      ast.orb.i                = i;
      ast.orb.omega_node       = omega_node;
      ast.orb.omega_pericenter = omega_pericenter;
      ast.orb.M                = M;
      
      // year.assign(epoch,0,4);
      // month.assign(epoch,4,2);
      // day.assign(epoch,6,2);
      
      // y = atoi(year.c_str());
      // m = atoi(month.c_str());
      // d = atoi(day.c_str());
      
      tmp_date.SetJulian(2400000.5+atof(epoch.c_str()),TDT);
      ast.orb.epoch.SetDate(tmp_date);
      // ast.orb.T = sqrt(4*pisq/(GetG()*GetMSun())*pow(FromUnits(ast.orb.a,AU),3));
      ast.orb.mu = GetG()*GetMSun();
      // ast.orb.ref_body = orb_ref_body;
      
      /* 
	 switch (universe->GetReferenceSystem()) {
	 case ECLIPTIC: break;
	 case EQUATORIAL:
	 { 
	 // cerr << "Rotating astorb orbit..." << endl;
	 const double obleq_rad = obleq(tmp_date).GetRad();
	 Vector position,velocity;
	 ast.orb.RelativePosVel(position,velocity);
	 position.rotate(0.0,obleq_rad,0.0);
	 velocity.rotate(0.0,obleq_rad,0.0);
	 ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	 }
	 break;
	 }
      */
      
      switch (universe->GetReferenceSystem()) {
      case ECLIPTIC: break;
      case EQUATORIAL:
	{ 
	  Vector position,velocity;
	  ast.orb.RelativePosVel(position,velocity);
	  EclipticToEquatorial_J2000(position);
	  EclipticToEquatorial_J2000(velocity);
	  ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	}
	
	break;
      }
      
      db->push_back(ast);
    }
    
    read_finished();
  }
  
  ////

  JPLDastcomUnnumFile::JPLDastcomUnnumFile() : AsteroidDatabaseFile() {
    db = new AsteroidDatabase();
  }
  
  JPLDastcomUnnumFile::~JPLDastcomUnnumFile() {
    delete db;
    db = 0;
  }
  
  void JPLDastcomUnnumFile::Read() {
    
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    db->clear();
    
    char line[300];
    
    double a,e,i,omega_node,omega_pericenter,M;
    // int    n;
    string number,name,orbit_computer,absolute_magnitude,arc,numobs,epoch;
    string mean_anomaly,pericenter,node,inclination,eccentricity,semimajor_axis;
    // string ceu;
    
    string year,month,day;
    // int    y,m,d;
    
    Asteroid ast;
    
    // Date tmp_date(TDT);
    Date tmp_date;
    
    unsigned int local_index = 0;
    bool bool_stop=false;
    bool bool_pause=false;
    REWIND_FILE(file);
    while ((GETS_FILE(line,300,file)) != 0) {
      
      if (strlen(line) < 100) continue; // not a good line, maybe a comment or a white line...
      
      if (line[0]=='-') continue; // comment
      
      local_index++;
      read_progress(local_index,bool_pause,bool_stop);
      
      if (bool_stop) break;
      
      while (bool_pause) {
	sleep(1);
	read_progress(local_index,bool_pause,bool_stop);
      }
      
      // uncomment the ones used
      // number.assign(line,0,5);
      name.assign(line,0,11); 
      
      epoch.assign(line,12,5);
      //
      semimajor_axis.assign(line,18,11);
      eccentricity.assign(line,30,10);
      inclination.assign(line,41,9);
      pericenter.assign(line,51,9);
      node.assign(line,61,9);
      mean_anomaly.assign(line,71,11);
      //////////////
      
      ast.n = atoi(number.c_str());
      
      ast.name = name;
      remove_leading_trailing_spaces(ast.name);
      
      a                = atof(semimajor_axis.c_str());
      e                = atof(eccentricity.c_str());
      i                = (pi/180)*atof(inclination.c_str());
      omega_node       = (pi/180)*atof(node.c_str());
      omega_pericenter = (pi/180)*atof(pericenter.c_str());
      M                = (pi/180)*atof(mean_anomaly.c_str());
      
      // checks
      if ((a==0.0)) {
	// bad line...
	continue;
      }
      
      ast.orb.a                = FromUnits(a,AU);
      ast.orb.e                = e;
      ast.orb.i                = i;
      ast.orb.omega_node       = omega_node;
      ast.orb.omega_pericenter = omega_pericenter;
      ast.orb.M                = M;
      
      // year.assign(epoch,0,4);
      // month.assign(epoch,4,2);
      // day.assign(epoch,6,2);
      
      // y = atoi(year.c_str());
      // m = atoi(month.c_str());
      // d = atoi(day.c_str());
      
      tmp_date.SetJulian(2400000.5+atof(epoch.c_str()),TDT);
      ast.orb.epoch.SetDate(tmp_date);
      // ast.orb.T = sqrt(4*pisq/(GetG()*GetMSun())*pow(FromUnits(ast.orb.a,AU),3));
      ast.orb.mu = GetG()*GetMSun();
      // ast.orb.ref_body = orb_ref_body;
      
      /* 
	 switch (universe->GetReferenceSystem()) {
	 case ECLIPTIC: break;
	 case EQUATORIAL:
	 { 
	 // cerr << "Rotating astorb orbit..." << endl;
	 const double obleq_rad = obleq(tmp_date).GetRad();
	 Vector position,velocity;
	 ast.orb.RelativePosVel(position,velocity);
	 position.rotate(0.0,obleq_rad,0.0);
	 velocity.rotate(0.0,obleq_rad,0.0);
	 ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	 }
	 break;
	 }
      */
      
      switch (universe->GetReferenceSystem()) {
      case ECLIPTIC: break;
      case EQUATORIAL:
	{ 
       	  Vector position,velocity;
	  ast.orb.RelativePosVel(position,velocity);
	  EclipticToEquatorial_J2000(position);
	  EclipticToEquatorial_J2000(velocity);
	  ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	}
	break;
      }
      
      db->push_back(ast);
    }
    
    read_finished();
  }
  
  ////
  
  JPLDastcomCometFile::JPLDastcomCometFile() : AsteroidDatabaseFile() {
    db = new AsteroidDatabase();
  }
  
  JPLDastcomCometFile::~JPLDastcomCometFile() {
    delete db;
    db = 0;
  }
  
  void JPLDastcomCometFile::Read() {
      
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    db->clear();
    
    char line[300];
    
    double a,e,i,omega_node,omega_pericenter,M;
    string number,type,name,orbit_computer,absolute_magnitude,arc,numobs,epoch;
    string mean_anomaly,pericenter,node,inclination,eccentricity,semimajor_axis;
    // string ceu;
    string pericenter_distance,pericenter_epoch;
    
    string year,month,day;
    int    y=0,m=0;
    double frac_day;
    
    Asteroid ast;
    
    double q;
    
    REWIND_FILE(file);
    
    // Date tmp_date(TDT);
    Date tmp_date;
    
    unsigned int local_index = 0;
    bool bool_stop=false;
    bool bool_pause=false;
    
    while ( (GETS_FILE(line,300,file)) != 0 ) {
      
      // if (strlen(line) < 90) continue; // not a good line, maybe a comment or a white line...
      
      if (line[0]=='-') continue; // comment
      
      ++local_index;
      read_progress(local_index,bool_pause,bool_stop);
      
      if (bool_stop) break;
      
      while (bool_pause) {
	// cerr << "AstorbFile::Read() sleeping..." << endl;
	sleep(1);
	read_progress(local_index,bool_pause,bool_stop);
      }
      
      // uncomment the ones used
      // number.assign(line,0,4);
      // type.assign(line,4,1);
      // name.assign(line,5,7);
      name.assign(line,0,37);
      epoch.assign(line,38,5);
      // cerr << "comet name: " << name << endl;
      pericenter_distance.assign(line,44,10);
      eccentricity.assign(line,55,10);
      inclination.assign(line,66,9);
      pericenter.assign(line,76,9);
      node.assign(line,86,9);
      //
      pericenter_epoch.assign(line,96,14);
      
      // conversions
      
      ast.name = name;
      remove_leading_trailing_spaces(ast.name);
      
      // ast.n = 0; // arbitrary, for the moment
      ast.n = 0;
      
      // ast.mag  = atof(absolute_magnitude.c_str());
      
      // a                = atof(semimajor_axis.c_str());
      e  = atof(eccentricity.c_str());
      
      // to be tested...
      q = atof(pericenter_distance.c_str());
      if (e == 1.0) {
	a = q;
      } else {
	a = q/fabs(1.0-e);
      }
      
      // checks
      if ((q==0.0)) {
	// bad line...
	continue;
      }
      
      i                = (pi/180)*atof(inclination.c_str());
      omega_node       = (pi/180)*atof(node.c_str());
      omega_pericenter = (pi/180)*atof(pericenter.c_str());
      // M                = (pi/180)*atof(mean_anomaly.c_str());
      
      tmp_date.SetJulian(2400000.5+atof(epoch.c_str()),TDT);
      ast.orb.epoch.SetDate(tmp_date);
      
      year.assign(pericenter_epoch,0,4);
      month.assign(pericenter_epoch,4,2);
      day.assign(pericenter_epoch,6,8);
      
      y = atoi(year.c_str());
      m = atoi(month.c_str());
      frac_day = atof(day.c_str());
      
      Date peri_date;
      peri_date.SetGregor(y,m,frac_day,TDT);
      UniverseTypeAwareTime pericenter_passage(peri_date);
      
      ast.orb.mu = GetG()*GetMSun();
      
      ast.orb.a                = FromUnits(a,AU);
      ast.orb.e                = e;
      ast.orb.i                = i;
      ast.orb.omega_node       = omega_node;
      ast.orb.omega_pericenter = omega_pericenter;
      //
      M = ((ast.orb.epoch.Time() - pericenter_passage.Time())/ast.orb.Period())*twopi;  
      M = fmod(10*twopi+fmod(M,twopi),twopi);
      //
      ast.orb.M                = M;
      
      // cerr << "comet: " << ast.name << "  q: " << q << "  e: " << e << "  i: " << i*(180/pi) << endl;
      
      /* 
	 switch (universe->GetReferenceSystem()) {
	 case ECLIPTIC: break;
	 case EQUATORIAL:
	 { 
	 // cerr << "Rotating astorb orbit..." << endl;
	 const double obleq_rad = obleq(tmp_date).GetRad();
	 Vector position,velocity;
	 ast.orb.RelativePosVel(position,velocity);
	 position.rotate(0.0,obleq_rad,0.0);
	 velocity.rotate(0.0,obleq_rad,0.0);
	 ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	 }
	 break;
	 }
      */
      
      switch (universe->GetReferenceSystem()) {
      case ECLIPTIC: break;
      case EQUATORIAL:
	{ 
       	  Vector position,velocity;
	  ast.orb.RelativePosVel(position,velocity);
	  EclipticToEquatorial_J2000(position);
	  EclipticToEquatorial_J2000(velocity);
	  ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	}
	break;
      }
      
      db->push_back(ast);
      
    }
    
    read_finished();
  }
  
  ////
  
  // NEODyS .cat file
  
  // NEODYSCAT::NEODYSCAT() : ReadFile() {
  NEODYSCAT::NEODYSCAT() : AsteroidDatabaseFile() {
    // status = CLOSE;
    db = new AsteroidDatabase();
  }
  
  NEODYSCAT::~NEODYSCAT() {
    delete db;
    db = 0;
  }
  
  void NEODYSCAT::Read() {
    
    // if (status == CLOSE) Open();
    
    Open();
    
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    
    db->clear();
    
    char line[300];
    
    double a,e,i,omega_node,omega_pericenter,M;
    // int    n;
    string number,name,orbit_computer,absolute_magnitude,arc,numobs,epoch;
    string mean_anomaly,pericenter,node,inclination,eccentricity,semimajor_axis;
    // string ceu;
    
    string year,month,day;
    // int    y,m,d;
    
    Asteroid ast;
    
    // Date tmp_date(TDT);
    Date tmp_date;
    
    unsigned int local_index = 0;
    bool bool_stop=false;
    bool bool_pause=false;
    REWIND_FILE(file);
    while ((GETS_FILE(line,300,file)) != 0) {
      
      if (strlen(line) < 100) continue; // not a good line, maybe a comment or a white line...
      
      if (line[0]=='!') continue; // comment
      
      local_index++;
      read_progress(local_index,bool_pause,bool_stop);
      
      if (bool_stop) break;
      
      while (bool_pause) {
	sleep(1);
	read_progress(local_index,bool_pause,bool_stop);
      }
      
      // uncomment the ones used
      // number.assign(line,0,5);
      name.assign(line,0,12); 
      //
      {
	// remove -->'<--
      	string::size_type pos;
	while ((pos=name.find("'",0)) != string::npos) {
	  // cerr << "name: " << name << "  pos: " << pos << endl;
	  name.erase(pos,1);
	}
	// cerr << "final name: " << name << endl;
      }
      
      // orbit_computer.assign(line,25,15);
      // absolute_magnitude.assign(line,41,5);
      //
      // arc.assign(line,94,5);
      // numobs.assign(line,99,5);
      //
      epoch.assign(line,13,15);
      //
      semimajor_axis.assign(line,29,25);
      eccentricity.assign(line,54,25);
      inclination.assign(line,79,25);
      node.assign(line,104,25);
      pericenter.assign(line,129,25);
      mean_anomaly.assign(line,154,25);
      //////////////
      
      // ast.n = atoi(number.c_str());
      // ast.n = 0;
      {
	char c;
	unsigned int ck;
	bool is_only_digit=true;
	for (ck=0;ck<name.size();++ck) {
	  c = name[ck];
	  if (isalpha(c)) { 
	    is_only_digit=false;
	    break;
	  }
	}
	
	if (is_only_digit) {
	  ast.n = atoi(name.c_str());
	} else {
	  ast.n = 0;
	}
      }
      
      ast.name = name;
      remove_leading_trailing_spaces(ast.name);
      
      // ast.ceu  = atof(ceu.c_str());
      
      // ast.mag  = atof(absolute_magnitude.c_str());
      
      a                = atof(semimajor_axis.c_str());
      e                = atof(eccentricity.c_str());
      i                = (pi/180)*atof(inclination.c_str());
      omega_node       = (pi/180)*atof(node.c_str());
      omega_pericenter = (pi/180)*atof(pericenter.c_str());
      M                = (pi/180)*atof(mean_anomaly.c_str());
      
      ast.orb.a                = FromUnits(a,AU);
      ast.orb.e                = e;
      ast.orb.i                = i;
      ast.orb.omega_node       = omega_node;
      ast.orb.omega_pericenter = omega_pericenter;
      ast.orb.M                = M;
      
      // year.assign(epoch,0,4);
      // month.assign(epoch,4,2);
      // day.assign(epoch,6,2);
      
      // y = atoi(year.c_str());
      // m = atoi(month.c_str());
      // d = atoi(day.c_str());
      
      tmp_date.SetJulian(2400000.5+atof(epoch.c_str()),TDT);
      ast.orb.epoch.SetDate(tmp_date);
      // ast.orb.T = sqrt(4*pisq/(GetG()*GetMSun())*pow(FromUnits(ast.orb.a,AU),3));
      ast.orb.mu = GetG()*GetMSun();
      // ast.orb.ref_body = orb_ref_body;
      
      /* 
	 switch (universe->GetReferenceSystem()) {
	 case ECLIPTIC: break;
	 case EQUATORIAL:
	 { 
	 // cerr << "Rotating astorb orbit..." << endl;
	 const double obleq_rad = obleq(tmp_date).GetRad();
	 Vector position,velocity;
	 ast.orb.RelativePosVel(position,velocity);
	 position.rotate(0.0,obleq_rad,0.0);
	 velocity.rotate(0.0,obleq_rad,0.0);
	 ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	 }
	 
	 break;
	 }
      */
      
      switch (universe->GetReferenceSystem()) {
      case ECLIPTIC: break;
      case EQUATORIAL:
	{ 
       	  Vector position,velocity;
	  ast.orb.RelativePosVel(position,velocity);
	  EclipticToEquatorial_J2000(position);
	  EclipticToEquatorial_J2000(velocity);
	  ast.orb.Compute(position,velocity,ast.orb.mu,ast.orb.epoch);
	}
	break;
      }
      
      db->push_back(ast);
    }
    
    read_finished();
  }
  
  /// OrsaPaths
  
  char * OrsaPaths::path=0;
  char   OrsaPaths::_path_separator=0;
  
  OrsaPaths *orsa_paths=0;
  
  OrsaPaths::OrsaPaths() {
    if (orsa_paths) {
      ORSA_ERROR("cannot create two OrsaPaths from the same session");
      exit(0);
    }
    
    set_path_separator();
    set_path();
    
    orsa_paths = this;
  }
  
  OrsaPaths::OrsaPaths(const string &config_path) {
    if (orsa_paths) {
      ORSA_ERROR("cannot create two OrsaPaths from the same session");
      exit(0);
    }
    
    set_path_separator();
    path = strdup(config_path.c_str());
    
    orsa_paths = this;
  }
  
  void OrsaPaths::set_path_separator() {
    if (_path_separator!=0) return;
#ifdef _WIN32
    _path_separator = '\\';
#else
    _path_separator = '/';
#endif
  }
  
  void OrsaPaths::set_path() {
    char p[2048];
    char * eh = getenv("HOME");
    p[0] = 0;
    if (eh != 0) strcpy(p, eh);
#ifdef _WIN32
    char home[2048];
    if (SUCCEEDED(SHGetFolderPathA(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, home)))
      strcpy(p, home);
    strcat(p, "\\WINORSA\\");
#else
    strcat(p, "/.orsa/");
#endif
    path = strdup(p);
  }
  
  // static OrsaPaths p; // make sure the constructor gets called
  
  // TLE
  
  TLEFile::TLEFile() : ReadFile() {
    
  }
  
  void TLEFile::Read() {
    Open();
    if (status != OPEN_R) {
      ORSA_ERROR("Status error!");
      return;
    }
    sat.clear();
    string name;
    string s_tmp;
    int year=0;
    double days=0.0;
    double inclination=0.0,node=0.0,eccentricity=0.0,peri=0.0,M=0.0,period=0.0;
    bool have_one=false;
    bool have_two=false;
    char line[1024];
    unsigned int local_index = 0;
    while (GETS_FILE(line,1024,file) != 0) {
      
      if (line[0] == '1') {
	
	if (strlen(line) < 69) continue;
	
	if (isalpha(line[6])) continue; // test for single chars...
	
	s_tmp.assign(line,18,2);
	year = atoi(s_tmp.c_str());
	if (year > 70)
	  year += 1900;
	else 
	  year += 2000;
	
	s_tmp.assign(line,20,12);
	days = atof(s_tmp.c_str());
	
	have_one = true;
	have_two = false;
	
      } else if (line[0] == '2') {
	
	if (strlen(line) < 69) continue;
	
	if (!have_one) continue;
	
	if (isalpha(line[6])) continue; // test for single chars...
	
	s_tmp.assign(line,8,8);
	inclination = (pi/180.0)*atof(s_tmp.c_str());
	
	s_tmp.assign(line,17,8);
	node = (pi/180.0)*atof(s_tmp.c_str());
	
	s_tmp.assign(line,26,7);
        eccentricity = 1.0e-7*atof(s_tmp.c_str());	
	
	s_tmp.assign(line,34,8);
	peri = (pi/180.0)*atof(s_tmp.c_str());	
	
	s_tmp.assign(line,43,8);
	M = (pi/180.0)*atof(s_tmp.c_str());	
	
	s_tmp.assign(line,52,11);
	period = FromUnits(1.0/atof(s_tmp.c_str()),DAY);	
	
	have_two = true;
	
      } else {
	name.assign(line,0,MIN(24,strlen(line)-1)); // the last -1 is set to avoid the '\n' character in the name
	remove_leading_trailing_spaces(name);
	have_one = false;
	have_two = false;
      }
      
      if (have_one && have_two) {
	
	Date epoch;
       	epoch.SetGregor(year,1,1,UTC); // UTC?
	double jd = epoch.GetJulian(UTC);
	jd += days-1.0;
	epoch.SetJulian(jd,UTC);
	
	JPLBody Earth(EARTH,epoch);
	
	Orbit orbit;
	orbit.mu = GetG()*Earth.mass();
	orbit.a = cbrt(period*period*orbit.mu/(4*pisq));
	orbit.e = eccentricity;
	orbit.i = inclination;
	orbit.omega_node       = node;
	orbit.omega_pericenter = peri;
	orbit.M                = M;
	
	Vector position,velocity;
	orbit.RelativePosVel(position,velocity);
	
	if (universe->GetReferenceSystem() == ECLIPTIC) {
	  Angle obl = obleq_J2000();
	  position.rotate(0.0,-obl.GetRad(),0.0);
	  velocity.rotate(0.0,-obl.GetRad(),0.0);
	}
	
	position += Earth.position();
	velocity += Earth.velocity();
	
	sat.push_back(BodyWithEpoch(name,0.0,position,velocity,epoch));
	
	++local_index;
	read_progress(local_index);
	
	// cerr << name << " period[DAYS]: " << FromUnits(period,DAY,-1) << "  a[ER]: " << FromUnits(orbit.a,ER,-1) << endl;
	
	have_one = have_two = false;
      }
    }
  }
  
} // namespace orsa
