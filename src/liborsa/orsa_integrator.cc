/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_integrator.h"

namespace orsa {
  
  void make_new_integrator(Integrator ** i, const IntegratorType type) {
    
    delete (*i);
    (*i) = 0;
    
    switch (type) {
    case STOER:                 (*i) = new Stoer;                  break;
      // case BULIRSCHSTOER:         (*i) = new BulirschStoer;          break;
    case RUNGEKUTTA:            (*i) = new RungeKutta;             break;
    case DISSIPATIVERUNGEKUTTA: (*i) = new DissipativeRungeKutta;  break;
    case RA15:                  (*i) = new Radau15;                break;
    case LEAPFROG:              (*i) = new Leapfrog;               break;
    }
  }
  
  // Leapfrog
  
  Leapfrog::Leapfrog() : FixedTimestepIntegrator() {
    type = LEAPFROG;
  }
  
  Leapfrog::Leapfrog(const Leapfrog & i) : FixedTimestepIntegrator() {
    type     = i.type;
    timestep = i.timestep;
    accuracy = i.accuracy;
    // m        = i.m;
  }
  
  Integrator * Leapfrog::clone() const {
    return new Leapfrog(*this);
  }
  
  Leapfrog::~Leapfrog() {
    
  }
  
  /* 
     // OLD method: kick-drift-kick
     void Leapfrog::Step(const Frame & frame_in, Frame & frame_out, Interaction * interaction) {
     
     // NON-DISSIPATIVE (velocity indipendent) version
     
     const unsigned int n = frame_in.size();
     
     const double h  = timestep.GetDouble();
     const double h2 = 0.5*h;
     
     acc.resize(n);
     
     if (acc_time != frame_in) {
     interaction->Acceleration(frame_in,acc);
     acc_time = frame_in;
     }
     
     std::vector<Vector> vel_h2(n);
     
     for (unsigned int j=0;j<n;++j) {
     vel_h2[j] = frame_in[j].velocity()+acc[j]*h2;
     }
     
     frame_out = frame_in;
     frame_out += timestep;
     
     for (unsigned int j=0;j<n;++j) {
     frame_out[j].AddToPosition(vel_h2[j]*h);
     }
     
     interaction->Acceleration(frame_out,acc);
     acc_time = frame_out;
     
     for (unsigned int j=0;j<n;++j) {
     frame_out[j].SetVelocity(vel_h2[j]+acc[j]*h2);
     }
     
     }
  */
  
  // new method: drift-kick-drift
  void Leapfrog::Step(const Frame & frame_in, Frame & frame_out, Interaction * interaction) {
    
    // NON-DISSIPATIVE (velocity indipendent) version
    
    const unsigned int n = frame_in.size();
    
    const double h  = timestep.GetDouble();
    const double h2 = 0.5*h;
    
    frame_out = frame_in;
    
    frame_out += 0.5*timestep;
    for (unsigned int j=0;j<n;++j) {
      frame_out[j].AddToPosition(frame_out[j].velocity()*h2);
    }
    
    std::vector<Vector> acc(n);
    //
    if (interaction->IsSkippingJPLPlanets()) {
      frame_out.ForceJPLEphemerisData();
    }
    //
    interaction->Acceleration(frame_out,acc);
    
    for (unsigned int j=0;j<n;++j) {
      frame_out[j].AddToVelocity(acc[j]*h);
    }
    
    frame_out += 0.5*timestep;
    for (unsigned int j=0;j<n;++j) {
      frame_out[j].AddToPosition(frame_out[j].velocity()*h2);
    }
    
  }
  
} // namespace orsa
