/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_interaction.h"
#include "orsa_secure_math.h"
#include "orsa_universe.h"
#include "orsa_error.h"

#include <cmath>
#include <iostream>

using namespace std;

namespace orsa {
  
  std::string label(const InteractionType it) {
    
    std::string s("");
    
    switch (it) {
    case NEWTON:                                s = "Newton";                               break;
    case NEWTON_MPI:                            s = "Newton (MPI)";                         break;
    case ARMONICOSCILLATOR:                     s = "Armonic Oscillator";                   break;
    case GALACTIC_POTENTIAL_ALLEN:              s = "Galactic Potential (Allen)";           break; 
    case GALACTIC_POTENTIAL_ALLEN_PLUS_NEWTON:  s = "Galactic Potential (Allen) + Newton";  break;
    case JPL_PLANETS_NEWTON:                    s = "JPL planets + Newton";                 break; 
    case GRAVITATIONALTREE:                     s = "Gravitational TreeCode";               break;
    case RELATIVISTIC:                          s = "Newton + Relativistic effects";        break;
    }	
    
    return s;
  }
  
  void make_new_interaction(Interaction ** i, const InteractionType type) {
    
    delete (*i);
    (*i) = 0;
    
    switch (type) {
    case NEWTON:                               (*i) = new Newton;                           break;
    case NEWTON_MPI:                        
#ifdef HAVE_MPI
      (*i) = new Newton_MPI;                  
#else
      ORSA_WARNING("read NEWTON_MPI interaction from application without MPI support.");
#endif
      break;
    case ARMONICOSCILLATOR:                    (*i) = new ArmonicOscillator(1,1);           break;
    case GALACTIC_POTENTIAL_ALLEN:             (*i) = new GalacticPotentialAllen;           break;
    case GALACTIC_POTENTIAL_ALLEN_PLUS_NEWTON: (*i) = new GalacticPotentialAllenPlusNewton; break;
    case JPL_PLANETS_NEWTON:                   /*************************************/      break;
    case GRAVITATIONALTREE:                    (*i) = new GravitationalTree;                break;
    case RELATIVISTIC:                         (*i) = new Relativistic;                     break;
    }    
  }
  
  // MappedTable 
  
  void MappedTable::load(const std::vector<Body> & f, const bool skip_JPL_planets) {
    const unsigned int f_size = f.size();
    N = f_size;
    mapping.resize(N);
    M = 0;
    for (unsigned int k=0; k<f_size; ++k) {
      mapping[k] = k; 
      if (!f[k].has_zero_mass()) {
	mapping[k] = mapping[M];
	mapping[M] = k;
	++M;
      }
    }
    //
    MN = M*N;
    //
    if (MN != distance_vector.size()) {
      distance_vector.resize(MN);
      d1.resize(MN);
      d2.resize(MN);
      d3.resize(MN);
      d4.resize(MN);
      one_over_distance.resize(MN);
      one_over_distance_square.resize(MN);
      one_over_distance_cube.resize(MN);
      distance_vector_over_distance_cube.resize(MN);
    }
    //
    Vector d;
    Vector v;
    double l;
    double one_over_d;
    double one_over_d2;
    double one_over_d3;
    unsigned int index;
    //
    for (unsigned int i=0; i<(N-1); ++i) {
      for (unsigned int j=i+1; j<N; ++j) {
	// if (i == j) continue;
	if (f[i].has_zero_mass() && f[j].has_zero_mass()) continue;
	if (skip_JPL_planets && (f[i].JPLPlanet() != NONE) && (f[i].JPLPlanet() != NONE)) continue;
	//
	index = ij_to_index(i,j);
	//
	// unsing: inline Vector Body::distanceVector(const Body & b) const { return b.position()-position(); }
	// so: d = f[j].position() - f[i].position();
	d           = f[i].DistanceVector(f[j]);
	//
	if (distance_vector[index] == d) continue; // everything already computed!
	//
	l           = d.Length();
	one_over_d  = 1.0/l;
	one_over_d2 = one_over_d*one_over_d;
	one_over_d3 = one_over_d*one_over_d2;
	//
	// index = ij_to_index(i,j);
	//
	//
	distance_vector[index]        = d;
	d1[index]                     = l;
	d2[index]                     = l*l;
	d3[index]                     = d2[index]*l;
	d4[index]                     = d3[index]*l;
	one_over_distance[index]        = one_over_d;
	one_over_distance_square[index] = one_over_d2;
	one_over_distance_cube[index]   = one_over_d3;
	distance_vector_over_distance_cube[index] = d*one_over_d3;
      }
    }
  }
  
  // Newton
  
  Newton::Newton() : Interaction(), include_multipole_moments(false), include_relativistic_effects(false), include_fast_relativistic_effects(false), one_over_c2(1.0/(GetC()*GetC())) {
    skip_JPL_planets = false;
  }
  
  Newton::Newton(const Newton & n) : Interaction(), include_multipole_moments(n.include_multipole_moments), include_relativistic_effects(n.include_relativistic_effects), include_fast_relativistic_effects(n.include_fast_relativistic_effects), one_over_c2(1.0/(GetC()*GetC())) {
    skip_JPL_planets = n.skip_JPL_planets;
  }
  
  Interaction * Newton::clone() const {
    return new Newton(*this);
  }
  
  void Newton::fast_newton_acc(const Frame & f, std::vector<Vector> & a) {
    
    const unsigned int size = f.size();
    
    Vector d;
    
    double l;
    
    for (unsigned int i=0;i<size-1;++i) {
      for (unsigned int j=i+1;j<size;++j) {
	
	if (zero_mass[i] && zero_mass[j]) continue;

	if (skip[i] && skip[j]) continue;
	
	d = f[i].DistanceVector(f[j]);
	
	l = d.Length();
        
	if (d.IsZero()) {
	  ORSA_WARNING("two objects in the same position! (%s and %s)",f[i].name().c_str(),f[j].name().c_str());
	  continue;
        }
        
 	d /= l*l*l;
	
      	a[i] += d * f[j].mu();
        a[j] -= d * f[i].mu();
        
	// fprintf(stderr,"body %s: acc. due to %s = %g    force = %g\n",f[i].name().c_str(),f[j].name().c_str(),a[i].Length(),a[i].Length()*f[i].mass());
      } 
    } 
  }
  
  void Newton::Acceleration(const Frame & f, vector<Vector> & a) {
    
    const unsigned int size = f.size();
    
    if (size < 2) return;
    
    a.resize(size);
    /* 
       for (unsigned int i=0;i<size;++i)
       a[i].Set(0.0,0.0,0.0);
    */
    
    zero_mass.resize(size);
    for (unsigned int i=0;i<size;++i) {
      zero_mass[i] = f[i].has_zero_mass();
    }
    
    skip.resize(size);
    if (skip_JPL_planets) {
      for (unsigned int k=0;k<size;++k) {
	skip[k] = (f[k].JPLPlanet() != NONE);
      }
    } else {
      for (unsigned int k=0;k<size;++k) {
	skip[k] = false;
      }
    }
    
    const bool only_newton = !(include_multipole_moments || include_relativistic_effects || include_fast_relativistic_effects);
    
    if (only_newton) {
      for (unsigned int i=0;i<size;++i) {
	a[i].Set(0.0,0.0,0.0);
      }
      //
      fast_newton_acc(f,a);
      //
      return;
    }
    
    if (include_relativistic_effects && include_fast_relativistic_effects) {
      ORSA_WARNING("Both the accurate and the fast version of the Relativistic corrections are activated!");
    }
    
    if (a_newton.size() != size) {
      a_newton.resize(size);
      a_multipoles.resize(size);
      a_relativity.resize(size);
    }
    //
    for (unsigned int i=0;i<size;++i) {
      a_newton[i].Set(0.0,0.0,0.0);
      a_multipoles[i].Set(0.0,0.0,0.0);
      a_relativity[i].Set(0.0,0.0,0.0);
    }
    
    // mod_f initialization needed!
    Frame mod_f = f;
    //
    if (include_relativistic_effects) {
      // a_newton.resize(size);
      //
      // The RelativisticBarycenter is _very_ close to the normal Barycenter,
      // and the accuracy of a "double" is often barely enough to distinguish them.
      // I'll use the normal Barycenter...
      //
      const Vector barycenter          = f.Barycenter();
      const Vector barycenter_velocity = f.BarycenterVelocity();
      // 
      // mod_f already equalt to f
      // mod_f = f;
      //
      for (unsigned int k=0; k<f.size(); ++k) {
	mod_f[k].AddToPosition(-barycenter);
	mod_f[k].AddToVelocity(-barycenter_velocity);
      }
      //
      mapped_table.load(mod_f,skip_JPL_planets);
    } else {
      mapped_table.load(f,skip_JPL_planets);
    }
    
    // note: mapped(i,j) = something(i) - something(j)
    
    /* 
       {
       // test        
       for (unsigned int i=0;i<size-1;++i) {
       for (unsigned int j=i+1;j<size;++j) {
       if (zero_mass[i] && zero_mass[j]) continue;
       if (skip[i] && skip[j]) continue;
       if (fabs((mapped_table.Distance(i,j)-f[i].DistanceVector(f[j]).Length())/(mapped_table.Distance(i,j))) > 1.0e-10) {
       fprintf(stderr,
       "PROBLEM-1!! d(%10s,%10s): mapped=%20.12f num=%20.12f\n",
       f[i].name().c_str(),
       f[j].name().c_str(),
       mapped_table.Distance(i,j),
       f[i].DistanceVector(f[j]).Length());
       }
       if (fabs((mapped_table.DistanceVector(i,j).x+f[i].DistanceVector(f[j]).x)/mapped_table.DistanceVector(i,j).x) > 1.0e-10) {
       fprintf(stderr,
       "PROBLEM-2!! d(%10s,%10s): mapped=%20.12f num=%20.12f\n",
       f[i].name().c_str(),
       f[j].name().c_str(),
       mapped_table.DistanceVector(i,j).x,
       f[i].DistanceVector(f[j]).x);
       }
       }
       }
       }
    */
    
    if (include_multipole_moments) {
      axis.resize(size);
      x_axis.resize(size);
      R1.resize(size);
      R2.resize(size);
      R3.resize(size);
      R4.resize(size);
      //
      {
	Angle alpha,delta,meridian;
	Vector planet_axis;
	Vector planet_x_axis;
	for (unsigned int i=0;i<size;++i) {
	  if (f[i].JPLPlanet() == NONE) continue;
	  alpha_delta_meridian(f[i].JPLPlanet(),f,alpha,delta,meridian);
	  //
	  planet_axis.Set(0,0,1);
	  planet_x_axis.Set(1,0,0);
	  // glRotated(90.0+(180.0/pi)*W.GetRad(),0,0,1);
	  // no halfpi needed!
	  planet_axis.rotate(meridian.GetRad(),0,0);
	  planet_x_axis.rotate(meridian.GetRad(),0,0);
	  // glRotated(90.0-(180.0/pi)*delta.GetRad(),1,0,0);
	  planet_axis.rotate(0,halfpi-delta.GetRad(),0);
	  planet_x_axis.rotate(0,halfpi-delta.GetRad(),0);
	  // glRotated(90.0+(180.0/pi)*alpha.GetRad(),0,0,1);
	  planet_axis.rotate(halfpi+alpha.GetRad(),0,0);
	  planet_x_axis.rotate(halfpi+alpha.GetRad(),0,0);
	  //
	  if (universe->GetReferenceSystem() == ECLIPTIC) {
	    // glRotated(-(180.0/pi)*obleq_J2000().GetRad(),1,0,0);
	    planet_axis.rotate(0,-obleq_J2000().GetRad(),0);
	    planet_x_axis.rotate(0,-obleq_J2000().GetRad(),0);
	  }
	  //
	  axis[i] = planet_axis;
	  x_axis[i] = planet_x_axis;
	}
      }
      //
      {
	double R;
      	for (unsigned int i=0;i<size;++i) {
	  R = f[i].radius();
	  if (R1[i] != R) {
	    R1[i] = R;
	    R2[i] = R*R;
	    R3[i] = R2[i]*R;
	    R4[i] = R3[i]*R;
	  }
	}
      }
    }
    
    for (unsigned int i=0;i<size-1;++i) {
      for (unsigned int j=i+1;j<size;++j) {
	
	if (zero_mass[i] && zero_mass[j]) continue;
	
	if (skip[i] && skip[j]) continue;
	
	if (mapped_table.Distance(i,j) < (std::numeric_limits<double>::min() * 1.0e3)) {
	  ORSA_WARNING("two objects in the same position! (%s and %s)",f[i].name().c_str(),f[j].name().c_str());
	  continue;
	}
	
	a_newton[i] += mapped_table.DistanceVectorOverDistanceCube(j,i) * f[j].mu();
	a_newton[j] += mapped_table.DistanceVectorOverDistanceCube(i,j) * f[i].mu();
      }
    }
    
    if (include_relativistic_effects) {
      
      for (unsigned int i=0;i<size-1;++i) {	
	for (unsigned int j=i+1;j<size;++j) {
          
	  /* 
	     for (unsigned int i=0;i<size;++i) {	
	     for (unsigned int j=0;j<size;++j) {
	  */
	  
	  // if (i == j) continue;
	  
	  if (zero_mass[i] && zero_mass[j]) continue;
	  
	  if (skip[i] && skip[j]) continue;
	  
	  // Now mod_f should be the Frame in the relativistic barycenter reference frame
	  
	  // Relativistic interaction, from:
	  // "Explanatory Supplement to the Astronomical Almanac",
	  // Edited by P. Kenneth Seidelmann, U.S. Naval Observatory,
	  // 2nd edition, 1992, sect. 5.211, pag. 281.
	  
	  double sum_ik = 0.0;
	  double sum_jk = 0.0;
	  for (unsigned int k=0; k<size; ++k) {
	    if (i != k) sum_ik += f[k].mu()*mapped_table.OneOverDistance(i,k);
	    if (j != k) sum_jk += f[k].mu()*mapped_table.OneOverDistance(j,k);
	  }
	  
	  a_relativity[i] += one_over_c2*f[j].mu()*mapped_table.DistanceVectorOverDistanceCube(j,i)*( // missing Newton term...
												     - 4.0*sum_ik
												     - 1.0*sum_jk
												     + mod_f[i].velocity().LengthSquared()
												     + 2.0*mod_f[j].velocity().LengthSquared()
												     - 4.0*mod_f[i].velocity()*mod_f[j].velocity()
												     - 1.5*pow(mapped_table.DistanceVector(i,j)*mod_f[j].velocity()*mapped_table.OneOverDistance(i,j),2)
												     + 0.5*mapped_table.DistanceVector(j,i)*a_newton[j]
												     )
	    + one_over_c2*f[j].mu()*mapped_table.OneOverDistanceCube(i,j)*((mapped_table.DistanceVector(i,j)*(4.0*mod_f[i].velocity()-3.0*mod_f[j].velocity()))*(mod_f[i].velocity()-mod_f[j].velocity()))
	    + 3.5*one_over_c2*f[j].mu()*mapped_table.OneOverDistance(i,j)*a_newton[j];
	  
	  a_relativity[j] += one_over_c2*f[i].mu()*mapped_table.DistanceVectorOverDistanceCube(i,j)*( // missing Newton term...
												     - 4.0*sum_jk
												     - 1.0*sum_ik
												     + mod_f[j].velocity().LengthSquared()
												     + 2.0*mod_f[i].velocity().LengthSquared()
												     - 4.0*mod_f[j].velocity()*mod_f[i].velocity()
												     - 1.5*pow(mapped_table.DistanceVector(j,i)*mod_f[i].velocity()*mapped_table.OneOverDistance(j,i),2)
												     + 0.5*mapped_table.DistanceVector(i,j)*a_newton[i]
												     )
	    + one_over_c2*f[i].mu()*mapped_table.OneOverDistanceCube(j,i)*((mapped_table.DistanceVector(j,i)*(4.0*mod_f[j].velocity()-3.0*mod_f[i].velocity()))*(mod_f[j].velocity()-mod_f[i].velocity()))
	    + 3.5*one_over_c2*f[i].mu()*mapped_table.OneOverDistance(j,i)*a_newton[i];
	}
      }
    }
    
    if (include_fast_relativistic_effects) {
      for (unsigned int i=0;i<size-1;++i) {	
	for (unsigned int j=i+1;j<size;++j) {
	  
	  // if (i == j) continue;
	  
	  if (zero_mass[i] && zero_mass[j]) continue;
	  
	  if (skip[i] && skip[j]) continue;
	  
	  // tests...
	  // good old code...
	  
	  // const Vector r = mod_f[i].position() - mod_f[j].position();
	  const Vector r = mapped_table.DistanceVector(i,j);
	  //
	  const Vector v  = mod_f[i].velocity() - mod_f[j].velocity();
	  const double v2 = v.LengthSquared();
	  
	  // see http://www.projectpluto.com/relativi.htm
	  //
	  // acc = acc_{newton} - \frac{G M}{r^3 c^2} ( (\frac{4 G M}{r} - v^2) \vec(r) + 4 (v \cdot r) \vec(v) )
	  //
	  
	  a_relativity[i] += one_over_c2*f[j].mu()*mapped_table.OneOverDistanceCube(i,j)*((4.0*f[j].mu()*mapped_table.OneOverDistance(i,j)-v2)*r + 
											  4.0*(r*v)*v);
	  a_relativity[j] -= one_over_c2*f[i].mu()*mapped_table.OneOverDistanceCube(i,j)*((4.0*f[i].mu()*mapped_table.OneOverDistance(i,j)-v2)*r +
											  4.0*(r*v)*v);
	}
      }
    }
    
    if (include_multipole_moments) {
      
      /* for (unsigned int i=0;i<size-1;++i) {
	 for (unsigned int j=i+1;j<size;++j) {
      */
      // scan all i's and j's, becaues i is always the extended body,
      // while j is always the point mass body
      for (unsigned int i=0;i<size;++i) {
	for (unsigned int j=0;j<size;++j) {
	  
	  if (i == j) continue;
	  
	  if (zero_mass[i] && zero_mass[j]) continue;
	  
	  if (skip[i] && skip[j]) continue;
	  
	  /* 
	     const Vector      d = mapped_table.DistanceVector(i,j);
	     const Vector unit_d = d.Normalized();
	     
	     const double l2 = mapped_table.Distance2(i,j);
	     const double l3 = mapped_table.Distance3(i,j);
	     const double l4 = mapped_table.Distance4(i,j);
	  */
	  
	  if ((R1[i] > 0.0) && (fabs(f[i].J2()) > 0.0) && (f[i].JPLPlanet() != NONE)) {
	    
	    // cerr << "figure: extended body: " << f[i].name() << "  point-mass body: " << f[j].name() << endl;
	    
	    const Vector unit_x_local = mapped_table.DistanceVector(j,i).Normalized();
	    const Vector unit_y_local = ExternalProduct(axis[i],unit_x_local);
	    const Vector unit_z_local = ExternalProduct(unit_x_local,unit_y_local);
	    
	    const Vector tmp_y_axis = ExternalProduct(axis[i],x_axis[i]);
	    const double tmp_uy_y   = unit_y_local*tmp_y_axis;
	    const double tmp_uy_mx  = unit_y_local*x_axis[i];
	    const double lambda = atan2(tmp_uy_mx,tmp_uy_y);
	    //
	    double s1l,c1l; orsa::sincos(lambda,s1l,c1l);
	    double s2l,c2l; orsa::sincos(lambda,s2l,c2l);
	    double s3l,c3l; orsa::sincos(lambda,s3l,c3l);
	    double s4l,c4l; orsa::sincos(lambda,s4l,c4l);
	    
	    const double theta = acos(unit_x_local*axis[i]);
	    //
	    double st,ct; orsa::sincos(theta,st,ct);
	    const double cosec_theta = 1.0/st;	    
	    
	    const double ar  = R1[i]*mapped_table.OneOverDistance(i,j);
	    const double ar2 = ar*ar;
	    const double ar3 = ar*ar2;
	    const double ar4 = ar*ar3;
	    
	    const Legendre l(axis[i]*unit_x_local);
	    
	    // cerr << "x: " << axis[i]*unit_x_local << endl;
	    
	    // const double base_coefficient = - f[j].mu() * mapped_table.OneOverDistanceSquare(i,j);
	    const double base_coefficient = - mapped_table.OneOverDistanceSquare(i,j);
	    
	    const double acc_x_local = 
	      base_coefficient *
	      ( // zonal harmonics
	       f[i].J2()*ar2*3.0*l.P2 +
	       f[i].J3()*ar3*4.0*l.P3 + 
	       f[i].J4()*ar4*5.0*l.P4 +
	       // tesseral harmonics
	       ar2*(-3.0*l.P22*(f[i].C22()*c2l) ) +
	       ar3*(-4.0*l.P31*(f[i].C31()*c1l+f[i].S31()*s1l) +
		    -4.0*l.P32*(f[i].C32()*c2l+f[i].S32()*s2l) +
		    -4.0*l.P33*(f[i].C33()*c3l+f[i].S33()*s3l) ) +
	       ar4*(-5.0*l.P41*(f[i].C41()*c1l+f[i].S41()*s1l) +
		    -5.0*l.P42*(f[i].C42()*c2l+f[i].S42()*s2l) +
		    -5.0*l.P43*(f[i].C43()*c3l+f[i].S43()*s3l) + 
		    -5.0*l.P44*(f[i].C44()*c4l+f[i].S44()*s4l) ) );
	    
	    const double acc_y_local = 
	      base_coefficient *
	      ( // zonal harmonics
	       // no zonal harmonics along the y direction
	       // tesseral harmonics
	       ar2*(2.0*cosec_theta*l.P22*(-f[i].C22()*s2l) ) +
	       ar3*(1.0*cosec_theta*l.P31*(-f[i].C31()*s1l+f[i].S31()*c1l) +
		    2.0*cosec_theta*l.P32*(-f[i].C32()*s2l+f[i].S32()*c2l) +
		    3.0*cosec_theta*l.P33*(-f[i].C33()*s3l+f[i].S33()*c3l) ) +
	       ar4*(1.0*cosec_theta*l.P41*(-f[i].C41()*s1l+f[i].S41()*c1l) +
		    2.0*cosec_theta*l.P42*(-f[i].C42()*s2l+f[i].S42()*c2l) +
		    3.0*cosec_theta*l.P43*(-f[i].C43()*s3l+f[i].S43()*c3l) + 
		    4.0*cosec_theta*l.P44*(-f[i].C44()*s4l+f[i].S44()*c4l) ) );
	    
	    const double acc_z_local = 
	      base_coefficient *
	      ( // zonal harmonics
	       f[i].J2()*ar2*(-st)*l.dP2 +
	       f[i].J3()*ar3*(-st)*l.dP3 + 
	       f[i].J4()*ar4*(-st)*l.dP4 +
	       // tesseral harmonics
	       ar2*(st*l.dP22*(f[i].C22()*c2l) ) +
	       ar3*(st*l.dP31*(f[i].C31()*c1l+f[i].S31()*s1l) +
		    st*l.dP32*(f[i].C32()*c2l+f[i].S32()*s2l) +
		    st*l.dP33*(f[i].C33()*c3l+f[i].S33()*s3l) ) +
	       ar4*(st*l.dP41*(f[i].C41()*c1l+f[i].S41()*s1l) +
		    st*l.dP42*(f[i].C42()*c2l+f[i].S42()*s2l) +
		    st*l.dP43*(f[i].C43()*c3l+f[i].S43()*s3l) + 
		    st*l.dP44*(f[i].C44()*c4l+f[i].S44()*s4l) ) );
	    
	    const Vector local_acc =
	      unit_x_local * acc_x_local +
	      unit_y_local * acc_y_local +
	      unit_z_local * acc_z_local;
	    
	    // cerr << "F: i: " << f[i].name() << " j: " << f[j].name() << " ax: " << acc_x_local << " tot: " << local_acc.Length() << endl;
	    
	    a_multipoles[i] += f[j].mu() * local_acc;
	    
	    // reaction
	    a_multipoles[j] -= f[i].mu() * local_acc;
	    
	  }
	}
      }
    }
    
    for (unsigned int i=0;i<size;++i) {
      a[i] = a_newton[i] + (a_multipoles[i] + a_relativity[i]);
    }
    
    // done
  }
  
  double Newton::PotentialEnergy(const Frame &f) {
    
    if (f.size() < 2) return(0.0);
    
    double energy = 0.0;
    
    unsigned int i,j;
    
    Vector d;
    
    double l;
    
    for (i=1;i<f.size();++i) {
      
      if (f[i].mu()==0) continue;
      
      for (j=0;j<i;++j) {
        
	if (f[j].mu()==0) continue;
	
       	d = f[i].DistanceVector(f[j]);
	
        l = d.Length();
        
        if (d.IsZero()) {
	  ORSA_WARNING("two objects in the same position! (%s and %s)",f[i].name().c_str(),f[j].name().c_str());
	  continue;
        }
        
	energy -= f[i].mu()*f[j].mass()/l;
	
      } 
    }  
    
    return (energy);
  }
  
  // Relativistic
  
  Relativistic::Relativistic() : Interaction(), g(GetG()), c_2(GetC()*GetC()) {
    
  }
  
  Relativistic::Relativistic(const Relativistic &) : Interaction(), g(GetG()), c_2(GetC()*GetC()) {
    
  }
  
  Interaction * Relativistic::clone() const {
    return new Relativistic(*this);
  }
  
  // see http://www.projectpluto.com/relativi.htm
  //
  // acc = acc_{newton} - \frac{G M}{r^3 c^2} ( (\frac{4 G M}{r} - v^2) \vec(r) + 4 (v \cdot r) \vec(v) )
  //
  void Relativistic::Acceleration(const Frame &f, vector<Vector> &a) {
    
    if (f.size() < 2) return;
    
    a.resize(f.size());
    
    unsigned int i,j;
    
    for (i=0;i<a.size();++i)
      a[i].Set(0,0,0);
    
    Vector r;
    Vector v;
    
    double r_1, r_3;
    
    for (i=1;i<f.size();++i) {
      
      for (j=0;j<i;++j) {
	
	/*** acc. on 'i' due to 'j' ***/
	
        if ((f[i].mass()==0) && (f[j].mass()==0)) continue;
	
	// 'i' - 'j'
	r = f[i].position() - f[j].position();
	v = f[i].velocity() - f[j].velocity();
	
	if (r.IsZero()) {
	  ORSA_WARNING("two objects in the same position! (%s and %s)",f[i].name().c_str(),f[j].name().c_str());
	  continue;
	}
	
        r_1 = r.Length();
        r_3 = r.LengthSquared()*r_1;
	
	a[i] -= f[j].mass()/r_3*r + f[j].mass()/(r_3*c_2)*((4.0*g*f[j].mass()/r_1-v.LengthSquared())*r + 4.0*(r*v)*v);
	
	// now, acc. on 'j' due to 'i' so r -> -r and v -> -v ***/
	//
	a[j] += f[i].mass()/r_3*r + f[i].mass()/(r_3*c_2)*((4.0*g*f[i].mass()/r_1-v.LengthSquared())*r + 4.0*(r*v)*v);
	
      } 
      
    } 
    
    for (i=0;i<a.size();++i) a[i] *= g;
  }
  
  double Relativistic::PotentialEnergy(const Frame &f) {
    Newton newton;
    return (newton.PotentialEnergy(f));
  }
  
  // ArmonicOscillator
  
  ArmonicOscillator::ArmonicOscillator(const double free_length_in, const double k_in) : Interaction(), free_length(free_length_in), k(k_in) {
    
  }
  
  ArmonicOscillator::ArmonicOscillator(const ArmonicOscillator & i) : Interaction(), free_length(i.free_length), k(i.k) {
    
  }
  
  Interaction * ArmonicOscillator::clone() const {
    return new ArmonicOscillator(*this);
  }
  
  void ArmonicOscillator::Acceleration(const Frame& f, vector<Vector>& a) {
    
    if (f.size() < 2) return;
    
    a.resize(f.size());
    
    unsigned int i,j;
    
    Vector d, da;

    double ls;
    
    for (i=0;i<f.size();++i)
      a[i].Set(0,0,0);
    
    for (i=1;i<f.size();++i) {
      
      if (f[i].mass()==0) continue;
      
      for (j=0;j<i;++j) {
        
	// if ((f[i].mass==0) || (f[j].mass==0)) continue;
	
        // assert(i!=j);
        
	// d  =  f[i].position;
	// d -=  f[j].position;
	
	d = f[i].DistanceVector(f[j]);

	ls = d.Length();
        
        if (d.IsZero()) {
	  ORSA_WARNING("two objects in the same position! (%s and %s)",f[i].name().c_str(),f[j].name().c_str());
	  continue;
        }
        
	da = d*(ls-free_length)/ls;
	
	a[i] += da;
        a[j] -= da;
        
      } 
    }  
    
    for (i=0;i<a.size();++i) 
      if (f[i].mass() != 0) a[i] *= k/f[i].mass();
    
  }
  
  double ArmonicOscillator::PotentialEnergy(const Frame &f) {
    
    double energy=0.0;
    
    if (f.size() < 2) return(0.0);
    
    unsigned int i,j;
    
    Vector d;
    
    double ls;
    
    for (i=1;i<f.size();++i) {
      
      if (f[i].mass()==0) continue;
      
      for (j=0;j<i;++j) {
        
	// if ((f[i].mass==0) && (f[j].mass==0)) continue;
	
	// d  =  f[i].position;
	// d -=  f[j].position;
	
	d = f[i].DistanceVector(f[j]);
	
        if (d.IsZero()) {
	  ORSA_WARNING("two objects in the same position! (%s and %s)",f[i].name().c_str(),f[j].name().c_str());
	  continue;
        }
        
	// ls = secure_pow(d.Length()-free_length,2);
	ls = pow(d.Length()-free_length,2); // don't use the secure version for performance reasons
	
	energy += ls/2.0;
	
      } 
    }  
    
    return (energy*k);
    
  }
  
  // GalacticPotentialAllen
  // see: http://adsabs.harvard.edu/cgi-bin/nph-bib_query?bibcode=1991RMxAA..22..255A&db_key=AST&high=3e8aa73e3d07980
  /* @ARTICLE{1991RMxAA..22..255A,
     author = {{Allen}, C. and {Santillan}, A.},
     title = "{An improved model of the galactic mass distribution for orbit computations}",
     journal = {Revista Mexicana de Astronomia y Astrofisica},
     year = 1991,
     month = oct,
     volume = 22,
     pages = {255-263},
     adsurl = {http://adsabs.harvard.edu/cgi-bin/nph-bib_query?bibcode=1991RMxAA..22..255A&db_key=AST},
     adsnote = {Provided by the NASA Astrophysics Data System}
     }
  */
  
  GalacticPotentialAllen::GalacticPotentialAllen() : Interaction() {
    
    g = GetG();
    
    mb = FromUnits(1.40592e10,   MSUN);
    bb = FromUnits(0.3873e3,     PARSEC);
    md = FromUnits(8.5608e10,    MSUN);     
    ad = FromUnits(5.3178e3,     PARSEC);     
    bd = FromUnits(0.25e3,       PARSEC);
    mh = FromUnits(10.3836745e10,MSUN);
    ah = FromUnits(12.0e3,       PARSEC); 
    
  }
  
  
  GalacticPotentialAllen::GalacticPotentialAllen(const GalacticPotentialAllen &) : Interaction() {
    
    g = GetG();
    
    mb = FromUnits(1.40592e10,   MSUN);
    bb = FromUnits(0.3873e3,     PARSEC);
    md = FromUnits(8.5608e10,    MSUN);     
    ad = FromUnits(5.3178e3,     PARSEC);     
    bd = FromUnits(0.25e3,       PARSEC);
    mh = FromUnits(10.3836745e10,MSUN);
    ah = FromUnits(12.0e3,       PARSEC); 
    
  }
  
  Interaction * GalacticPotentialAllen::clone() const {
    return new GalacticPotentialAllen(*this);
  }
  
  void GalacticPotentialAllen::Acceleration(const Frame &f, vector<Vector> &a) {
    
    a.resize(f.size());
    
    unsigned int i;
    for (i=0;i<a.size();++i) a[i].Set(0,0,0);
    
    double r2,z2,r,rho;
    double fbr,fdr,fhr,fhrc;
    double fbz,fdz,fhz,fhzc;
    double fr,fz;
    Vector x;
    
    for (i=0;i<f.size();++i) {
      
      // if (!f[i].StillAlive()) continue;
      
      x = f[i].position();
      
      // auxiliary components
      r2  = x.x*x.x+x.y*x.y;
      z2  = x.z*x.z;
      r   = sqrt(r2);
      rho = x.Length();
      
      // accel. components along the r direction
      fbr  = -(mb*r/(secure_pow(r2+z2+bb*bb,1.5)));
      fdr  = -(md*r/(secure_pow(r2+secure_pow(ad+sqrt(z2+bd*bd),2),1.5)));
      fhr  = 1.02*mh*r/(ah*ah*secure_pow(1+1.0/secure_pow(rho/ah,1.02),2)*secure_pow(rho/ah,2.02)*rho);
      fhrc = -(mh/(1.02*ah)*(1.0404*(r*secure_pow(rho/ah,0.02))/(ah*rho*secure_pow(1.0+secure_pow(rho/ah,1.02),2))+1.02*(r*secure_pow(rho/ah,0.02))/(ah*rho*(1.0+secure_pow(rho/ah,1.02)))));
      
      // accel. components along the z direction
      fbz  = -(mb*x.z/(secure_pow(r2+z2+bb*bb,1.5)));
      fdz  = -(md*x.z*(ad+sqrt(z2+bd*bd))/(sqrt(z2+bd*bd)*secure_pow(r2+secure_pow(ad+sqrt(z2+bd*bd),2),1.5)));
      fhz  = 1.02*mh*x.z/(ah*ah*secure_pow(1.0+1.0/secure_pow(rho/ah,1.02),2)*secure_pow(rho/ah,2.02)*rho);
      fhzc = -(mh/(1.02*ah)*(1.0404*(x.z*secure_pow(rho/ah,0.02))/(ah*rho*secure_pow(1.0+secure_pow(rho/ah,1.02),2))+1.02*(x.z*secure_pow(rho/ah,0.02))/(ah*rho*(1.0+secure_pow(rho/ah,1.02)))));
      
      fr=fbr+fdr+fhr+fhrc;
      fz=fbz+fdz+fhz+fhzc;
      
      a[i].x = fr*x.x/r;
      a[i].y = fr*x.y/r;
      a[i].z = fz;
    } 
    
    for (i=0;i<a.size();++i) a[i] *= g;
    
    // for (i=0;i<a.size();++i) printf("a[%i] = %g %g %g\n",i,a[i].x,a[i].y,a[i].z);
    
  }
  
  double GalacticPotentialAllen::PotentialEnergy(const Frame &f) {
    
    // to be tested...
    
    double energy = 0.0;
    
    double pb,pd,ph;
    double r2,z2,r,rho;
    unsigned int i;
    Vector x;
    
    for (i=0;i<f.size();++i) {
      
      x = f[i].position();
      
      // auxiliary components
      r2  = x.x*x.x+x.y*x.y;
      z2  = x.z*x.z;
      r   = sqrt(r2);
      rho = x.Length();
      
      pb = mb/sqrt(r2+z2+bb*bb);
      pd = md/sqrt(r2+secure_pow(ad+sqrt(z2+bd*bd),2));
      ph = (mh/(rho/ah))*(secure_pow(rho/ah,2.02))/(1.0+secure_pow(rho/ah,1.02));
      
      energy -= (pb+pd+ph);
      
    } 
    
    return (energy*g);
  }
  
  // JPLPlanetsNewton
  
  JPLPlanetsNewton::JPLPlanetsNewton(list<JPL_planets> & l_in) : Interaction(), l(l_in) {
    if (universe->GetUniverseType() != Real) {
      cerr << "error: using the JPLPlanetsNewton interaction in a non-Real universe!" << endl;
      exit(0);
    }
    g = GetG();
  }
  
  JPLPlanetsNewton::JPLPlanetsNewton(const JPLPlanetsNewton & i) : Interaction(), l(i.l) {
    if (universe->GetUniverseType() != Real) {
      cerr << "error: using the JPLPlanetsNewton interaction in a non-Real universe!" << endl;
      exit(0);
    }
    g = GetG();
  }
  
  Interaction * JPLPlanetsNewton::clone() const {
    return new JPLPlanetsNewton(*this);
  }
  
  void JPLPlanetsNewton::Acceleration(const Frame & f, vector<Vector> & a) {
    
    a.resize(f.size());
    
    if (planets_frame.GetDate() != f.GetDate()) SetupSolarSystem(planets_frame,l,f);
    
    for (unsigned int i=0;i<a.size();++i)
      a[i].Set(0,0,0);
    
    for (unsigned int i=0;i<f.size();++i) {
      if (f[i].mass() > 0) {
	ORSA_ERROR("using the JPLPlanetsNewton interaction with massive objects!");
	// exit(0);
	return;
      }
    }
    
    Vector d;
    
    double l;
    
    for (unsigned int i=0;i<f.size();++i) {
      
      // if (!f[i].StillAlive()) continue;
      
      for (unsigned int j=0;j<planets_frame.size();++j) {
	
	// d  =              f[i].position;
	// d -=  planets_frame[j].position;
	
	d = f[i].DistanceVector(planets_frame[j]);
	
	// ls = d.LengthSquared();
	l = d.Length();
	
        if (d.IsZero()) {
	  ORSA_WARNING("two objects in the same position! (%s and %s)",
		  f[i].name().c_str(),
		  planets_frame[j].name().c_str());
	  continue;
        }
        
	// d *= secure_pow(ls,-1.5);
	d /= l*l*l;
	
	a[i] += d * planets_frame[j].mass();
	
      }
    }
    
    for (unsigned int i=0;i<a.size();++i) a[i] *= g;
  }
  
  double JPLPlanetsNewton::PotentialEnergy(const Frame & f) {
    SetupSolarSystem(planets_frame,l,f);
    return newton_itg.PotentialEnergy(planets_frame);
  }
  
} // namespace orsa
