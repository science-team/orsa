/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "orsa_orbit_gsl.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#ifdef HAVE_MPI
#include <mpi.h>
#endif // HAVE_MPI

#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_siman.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_diff.h>
#include <gsl/gsl_deriv.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_eigen.h>

#include "orsa_file.h"
#include "orsa_error.h"

#include <sys/time.h>
#include <time.h>

#include <string.h> // memcpy()
#include <algorithm> // sort()
#include <limits>

using namespace std;

namespace orsa {
  
  void PreliminaryOrbit::ComputeRMS(const vector<Observation> &obs) {
    rms = RMS_residuals(obs,*this);
  }
  
  //
  
  double Weight(const Observation &obs1, const Observation &obs2, const double optimal_dt=FromUnits(1,DAY)) {
    double w;
    double dt = fabs(FromUnits(obs1.date.GetJulian()-obs2.date.GetJulian(),DAY));
    /* 
       if (dt<optimal_dt) {
       w = optimal_dt/dt;
       } else {
       w = 1+dt/optimal_dt;
       }
    */
    //
    if (dt<optimal_dt) {
      w = dt/optimal_dt;
    } else {
      w = 1+optimal_dt/dt;
    }
    //
    return w;
  }
  
  double Weight(vector<Observation> &obs, const double optimal_dt=FromUnits(1,DAY)) {
    if (obs.size() < 2) return 0; // error...
    double w=0;
    sort(obs.begin(),obs.end());
    unsigned int k=1;
    while (k < obs.size()) {
      w += Weight(obs[k-1],obs[k],optimal_dt);
      ++k;
    }
    return w;
  }
  
  class ThreeObservations : public vector<Observation> {
  public:
    /* 
       inline bool operator < (const ThreeObservations &triobs) const {
       return (w < triobs.w);
    } 
    */
    //
    inline bool operator < (const ThreeObservations &triobs) const {
      return (triobs.w < w);
    }
  private:
    double w;
  public:
    double Weight() const { return w; }
  public:
    void ComputeWeight(const double optimal_dt=FromUnits(1,DAY)) {
      w = orsa::Weight(*this,optimal_dt);
    }
  };
  
  void BuildObservationTriplets(const vector<Observation> & obs, vector<ThreeObservations> &triplets, const double optimal_dt=FromUnits(1,DAY)) {
    // sort(obs.begin(),obs.end());
    triplets.clear();
    unsigned int l,m,n;
    const unsigned int s = obs.size();
    // build only triplets with observations delays smaller than and obs_delay_max
    const double obs_delay_max = FromUnits(180,DAY);
    // max ration between dalays
    const double obs_delay_ration_max = 10.0;
    //
    Observation ol,om,on;
    double dt12, dt23, dt_ratio;
    ThreeObservations triobs; triobs.resize(3);
    l=0;
    while (l<s) {
      ol=obs[l];
      triobs[0] = ol;
      m=l+1;
      while (m<s) {
	om=obs[m];
	triobs[1] = om;
	n=m+1;
	while (n<s) {
	  on=obs[n];
	  triobs[2] = on;
	  
	  triobs.ComputeWeight(optimal_dt);
	  
	  dt12 = FromUnits(om.date.GetJulian()-ol.date.GetJulian(),DAY);
	  dt23 = FromUnits(on.date.GetJulian()-om.date.GetJulian(),DAY);
	  
	  if (dt12>dt23) 
	    dt_ratio = dt12/dt23;
	  else           
	    dt_ratio = dt23/dt12;
	  
	  if ((dt12<obs_delay_max) && (dt23<obs_delay_max) && (dt_ratio<obs_delay_ration_max)) triplets.push_back(triobs);
	  
	  // debug
	  std::cerr << l << "-" << m << "-" << n << std::endl;
	  
	  ++n;
	}
	++m;
      }
      ++l;
    }
    
    sort(triplets.begin(),triplets.end());
  }
  
  // MOID
  
  class MOID_parameters {
  public:
    Orbit *o1, *o2;
  };
  
  double MOID_f (const gsl_vector *v, void *params) {
    
    double M1 = gsl_vector_get(v,0);
    double M2 = gsl_vector_get(v,1);
    
    MOID_parameters *p = (MOID_parameters*) params;
    
    p->o1->M = M1;
    p->o2->M = M2;
    
    Vector r1,r2,v1,v2;
    
    p->o1->RelativePosVel(r1,v1);
    p->o2->RelativePosVel(r2,v2);
    
    return (r1-r2).Length();
  }
  
  class MOID_solution {
  
  public:
    MOID_solution() { active=false; }
    
  public: 
    void Insert(const double moid, const double M1, const double M2) {
      if ((!active) || ((active) && (moid < _moid))) {
	_moid   = moid;
	_M1     = M1;
	_M2     = M2;
	active = true;
      }
    }
    
  public:  
    double M1()   const { return _M1; }
    double M2()   const { return _M2; }
    double moid() const { return _moid; }
    
  private:
    double _moid,_M1,_M2;
    bool   active;
  };
  
  double MOID(const Orbit &o1_in, const Orbit &o2_in, Vector &r1, Vector &r2) {
    
    Orbit o1(o1_in);
    Orbit o2(o2_in);
    
    MOID_parameters par;
    par.o1 = &o1;
    par.o2 = &o2;
    
    gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex,2);
    
    gsl_multimin_function moid_function;
    
    moid_function.f      = &MOID_f;
    moid_function.n      = 2;
    moid_function.params = &par;
    
    gsl_vector *x         = gsl_vector_alloc(2);
    gsl_vector *step_size = gsl_vector_alloc(2);
    
    // gsl_vector_set(step_size,0,pi);
    // gsl_vector_set(step_size,1,pi);
    
    MOID_solution best_solution;
    
    int gsl_solutions_iter=0,max_gsl_solutions_iter=7;
    while (gsl_solutions_iter<max_gsl_solutions_iter) {
      
      // gsl_vector_set(x,0,o1.M);
      // gsl_vector_set(x,1,o2.M);
      // gsl_vector_set(x,0,0.0);
      // gsl_vector_set(x,1,0.0);
      // gsl_vector_set(x,0,fmod(10*twopi   -o1.omega_node-o1.omega_pericenter,twopi));
      // gsl_vector_set(x,1,fmod(10*twopi+pi-o2.omega_node-o2.omega_pericenter,twopi));
      gsl_vector_set(x,0,fmod(10*twopi+(twopi/(2*max_gsl_solutions_iter))*gsl_solutions_iter   -o1.omega_node-o1.omega_pericenter,twopi));
      gsl_vector_set(x,1,fmod(10*twopi+(twopi/(2*max_gsl_solutions_iter))*gsl_solutions_iter+pi-o2.omega_node-o2.omega_pericenter,twopi));
      
      gsl_vector_set(step_size,0,pi);
      gsl_vector_set(step_size,1,pi);
      
      gsl_multimin_fminimizer_set (s, &moid_function, x, step_size);
      
      size_t iter = 0;
      int status;
      do {
	
	iter++;
	
	// iter status
	status = gsl_multimin_fminimizer_iterate(s);
	
	// convergence status
	status = gsl_multimin_test_size(gsl_multimin_fminimizer_size(s),1.0e-6);
	
	// if (status == GSL_SUCCESS) printf ("Minimum found at:\n");
	
	/* printf ("%5d %.5f %.5f %10.5f\n", iter,
	   (180/pi)*gsl_vector_get (s->x, 0), 
	   (180/pi)*gsl_vector_get (s->x, 1), 
	   gsl_multimin_fminimizer_minimum(s));
	*/
	
	// } while (status == GSL_CONTINUE && iter < 500);
      } while (status == GSL_CONTINUE && iter < 200);
      
      // const double moid = gsl_multimin_fminimizer_minimum(s);
      
      // cerr << "MOID iters: " << iter << endl;
      
      if (status == GSL_SUCCESS) {
	best_solution.Insert(gsl_multimin_fminimizer_minimum(s),gsl_vector_get(s->x,0),gsl_vector_get(s->x,1));
      }	
      
      ++gsl_solutions_iter;
    }
    
    const double moid = best_solution.moid();
    o1.M              = best_solution.M1();
    o2.M              = best_solution.M2();
    Vector v1, v2;
    // update r1 and r2 for output
    o1.RelativePosVel(r1,v1);
    o2.RelativePosVel(r2,v2);
    
    /*
      printf ("%5d %.5f %.5f %.5f %.5f %10.5f\n", iter,
      (180/pi)*gsl_vector_get (s->x, 0), (180/pi)*o1.M,
      (180/pi)*gsl_vector_get (s->x, 1), (180/pi)*o2.M,
      gsl_multimin_fminimizer_minimum(s));
    */
    
    // free
    gsl_multimin_fminimizer_free(s);
    gsl_vector_free(x);
    gsl_vector_free(step_size);
    
    // check 
    // WARNING! some vars can be modified by
    // this check area --> comment out when not debugging!
    if (0) {
      
      Orbit o1(o1_in);
      Orbit o2(o2_in);
      
      // random number generator
      // const int random_seed = 124323;
      struct timeval  tv;
      struct timezone tz;
      gettimeofday(&tv,&tz); 
      const int random_seed = tv.tv_usec;
      gsl_rng * rnd = gsl_rng_alloc(gsl_rng_gfsr4);
      gsl_rng_set(rnd,random_seed);
      int j,k;
      Vector r1,v1,r2,v2;
      double d, min_d=moid;
      //
      /* 
	 double angle;
	 j=0;
	 while (j < 500) {
	 // coordinated test
	 angle = twopi*gsl_rng_uniform(rnd);
	 o1.M = fmod(10*twopi+angle-o1.omega_node-o1.omega_pericenter,twopi);
	 o2.M = fmod(10*twopi+angle-o2.omega_node-o2.omega_pericenter,twopi);
	 o1.RelativePosVel(r1,v1);
	 o2.RelativePosVel(r2,v2);
	 d = (r1-r2).Length();
	 if (d < min_d) {
	 cerr << "(C) ";
	 min_d = d;
	 }
	 ++j;
	 }
      */
      //
      // random test
      j=0;
      while (j < 50) {
	o1.M = twopi*gsl_rng_uniform(rnd);
	o1.RelativePosVel(r1,v1);
	k=0;
	while (k < 50) {
	  // o1.M = twopi*gsl_rng_uniform(rnd);
	  o2.M = twopi*gsl_rng_uniform(rnd);
	  // o1.RelativePosVel(r1,v1);
	  o2.RelativePosVel(r2,v2);
	  d = (r1-r2).Length();
	  if (d < min_d) {
	    // cerr << "WARNING: found another solution for the MOID: " << d << " < " << moid << "  delta = " << d-moid << endl;
	    // printf("WARNING: found another solution for the MOID: %.10f < %.10f   delta: %.10f\n",d,moid,d-moid);
	    // exit(0);
	    std::cerr << "(R) ";
	    min_d = d;
	  }
	  ++k;
	}
	++j;
      }
      //
      if (min_d < moid) {
	// cerr << "WARNING: found another solution for the MOID: " << d << " < " << moid << "  delta = " << d-moid << endl;
	printf("\n WARNING: found another solution for the MOID: %.10f < %.10f   delta: %.10f\n",min_d,moid,min_d-moid);
	// exit(0);
      }
      // free random number generator
      gsl_rng_free(rnd);
      
    }
    
    return moid;
  }
  
  
  // MOID2RB
  
  class MOID2RB_parameters {
  public:
    Orbit *o1, *o2;
    Vector rb1, rb2;
  };
  
  double MOID2RB_f (const gsl_vector *v, void *params) {
    
    double M1 = gsl_vector_get(v,0);
    double M2 = gsl_vector_get(v,1);
    
    MOID2RB_parameters *p = (MOID2RB_parameters*) params;
    
    p->o1->M = M1;
    p->o2->M = M2;
    
    Vector r1,r2,v1,v2;
    
    p->o1->RelativePosVel(r1,v1);
    p->o2->RelativePosVel(r2,v2);
    
    return ((p->rb1+r1)-(p->rb2+r2)).Length();
  }
  
  class MOID2RB_solution {
  
  public:
    MOID2RB_solution() { active=false; }
    
  public: 
    void Insert(const double moid2rb, const double M1, const double M2) {
      if ((!active) || ((active) && (moid2rb < _moid2rb))) {
	_moid2rb   = moid2rb;
	_M1     = M1;
	_M2     = M2;
	active = true;
      }
    }
    
  public:  
    double M1()   const { return _M1; }
    double M2()   const { return _M2; }
    double moid2rb() const { return _moid2rb; }
    
  private:
    double _moid2rb,_M1,_M2;
    bool   active;
  };
  
  double MOID2RB(const Vector &rb1, const Vector &rb2, const Orbit &o1_in, const Orbit &o2_in, Vector &r1, Vector &r2) {
    
    Orbit o1(o1_in);
    Orbit o2(o2_in);
    
    MOID2RB_parameters par;
    par.o1 = &o1;
    par.o2 = &o2;
    par.rb1 = rb1;
    par.rb2 = rb2;
    
    gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex,2);
    
    gsl_multimin_function moid2rb_function;
    
    moid2rb_function.f      = &MOID2RB_f;
    moid2rb_function.n      = 2;
    moid2rb_function.params = &par;
    
    gsl_vector *x         = gsl_vector_alloc(2);
    gsl_vector *step_size = gsl_vector_alloc(2);
    
    // gsl_vector_set(step_size,0,pi);
    // gsl_vector_set(step_size,1,pi);
    
    MOID2RB_solution best_solution;
    
    int gsl_solutions_iter=0,max_gsl_solutions_iter=7;
    while (gsl_solutions_iter<max_gsl_solutions_iter) {
      
      gsl_vector_set(x,0,fmod(10*twopi+(twopi/(2*max_gsl_solutions_iter))*gsl_solutions_iter   -o1.omega_node-o1.omega_pericenter,twopi));
      gsl_vector_set(x,1,fmod(10*twopi+(twopi/(2*max_gsl_solutions_iter))*gsl_solutions_iter+pi-o2.omega_node-o2.omega_pericenter,twopi));
      
      gsl_vector_set(step_size,0,pi);
      gsl_vector_set(step_size,1,pi);
      
      gsl_multimin_fminimizer_set (s, &moid2rb_function, x, step_size);
      
      size_t iter = 0;
      int status;
      do {
	
	iter++;
	
	// iter status
	status = gsl_multimin_fminimizer_iterate(s);
	
	// convergence status
	status = gsl_multimin_test_size(gsl_multimin_fminimizer_size(s),1.0e-6);
	
	// if (status == GSL_SUCCESS) printf ("Minimum found at:\n");
	
	/* printf ("%5d %.5f %.5f %10.5f\n", iter,
	   (180/pi)*gsl_vector_get (s->x, 0), 
	   (180/pi)*gsl_vector_get (s->x, 1), 
	   gsl_multimin_fminimizer_minimum(s));
	*/
	
	// } while (status == GSL_CONTINUE && iter < 500);
      } while (status == GSL_CONTINUE && iter < 200);
      
      // const double moid2rb = gsl_multimin_fminimizer_minimum(s);
      
      // cerr << "MOID2RB iters: " << iter << endl;
      
      if (status == GSL_SUCCESS) {
	best_solution.Insert(gsl_multimin_fminimizer_minimum(s),gsl_vector_get(s->x,0),gsl_vector_get(s->x,1));
      }	
      
      ++gsl_solutions_iter;
    }
    
    const double moid2rb = best_solution.moid2rb();
    o1.M              = best_solution.M1();
    o2.M              = best_solution.M2();
    Vector v1, v2;
    // update r1 and r2 for output
    o1.RelativePosVel(r1,v1);
    o2.RelativePosVel(r2,v2);
    // not here...
    // r1 += rb1;
    // r2 += rb2;
    
    /*
      printf ("%5d %.5f %.5f %.5f %.5f %10.5f\n", iter,
      (180/pi)*gsl_vector_get (s->x, 0), (180/pi)*o1.M,
      (180/pi)*gsl_vector_get (s->x, 1), (180/pi)*o2.M,
      gsl_multimin_fminimizer_minimum(s));
    */
    
    // free
    gsl_multimin_fminimizer_free(s);
    gsl_vector_free(x);
    gsl_vector_free(step_size);
    
    return moid2rb;
  }
  
  // Simulated Annealing tests
  
  /* how many points do we try before stepping */
#define N_TRIES  10           
  
  /* how many iterations for each T? */
#define ITERS_FIXED_T 3
  
  /* max step size in random walk */
#define STEP_SIZE 1.0
  
  /* Boltzmann constant */
#define K        1.0e5             
  
  /* initial temperature */
#define T_INITIAL 0.01
  
  /* damping factor for temperature */
#define MU_T  1.005             
#define T_MIN 1.0e-6
  
  const gsl_siman_params_t params = {N_TRIES, ITERS_FIXED_T, STEP_SIZE, K, T_INITIAL, MU_T, T_MIN};
  
  struct data {
    OrbitWithEpoch      *orbit;
    vector<Observation> *obs;
  };
  
  double E1(void *p) {
    data *d = (data*) p;
    return (RMS_residuals(*d->obs,*d->orbit));
  }
  
  // metric coefficients
  const double d_a                = 0.1; // il comando FromUnits(0.1,AU) pare dare problemi...
  const double d_e                = 0.01;
  const double d_i                = 0.1*(pi/180);
  const double d_omega_node       = 1.0*(pi/180);
  const double d_omega_pericenter = 1.0*(pi/180);
  const double d_M                = 5.0*(pi/180);
  //
  const double c_a                = secure_pow(1.0/d_a,                2);
  const double c_e                = secure_pow(1.0/d_e,                2);
  const double c_i                = secure_pow(1.0/d_i,                2);
  const double c_omega_node       = secure_pow(1.0/d_omega_node,       2);
  const double c_omega_pericenter = secure_pow(1.0/d_omega_pericenter, 2);
  const double c_M                = secure_pow(1.0/d_M,                2);
  
  double M1(void *p1, void *p2) {
    data *d1 = (data*) p1;
    data *d2 = (data*) p2;
    
    double dist = sqrt( c_a                * secure_pow( d1->orbit->a                - d2->orbit->a,               2) +
			c_e                * secure_pow( d1->orbit->e                - d2->orbit->e,               2) +
			c_i                * secure_pow( d1->orbit->i                - d2->orbit->i,               2) +
			c_omega_node       * secure_pow( d1->orbit->omega_node       - d2->orbit->omega_node,      2) +
			c_omega_pericenter * secure_pow( d1->orbit->omega_pericenter - d2->orbit->omega_pericenter,2) +
			c_M                * secure_pow( d1->orbit->M                - d2->orbit->M,               2) ) / sqrt(6.0);
    
    std::cerr << "M1: " << dist << endl;
    
    return dist;
  }
  
  void S1(const gsl_rng *r, void *p, double step_size) {
    
    // double u = gsl_rng_uniform(r);
    // new_x = u * 2 * step_size - step_size + old_x;
    
    // memcpy(xp, &new_x, sizeof(new_x));
    
    data *d = (data*) p;
    
    // cerr << "d_a = " << d_a << endl;
    
    /* 
       data old_d;
       Orbit o = *d->orbit; old_d.orbit = &o;
       vector<Observation> obs = *d->obs; old_d.obs = &obs;
    */
    
    /* 
       double u, total = step_size;
       u = 0.40*total*(gsl_rng_uniform(r)-0.5); total -= u; new_d.orbit->a += u;
       u = 0.05*total*(gsl_rng_uniform(r)-0.5); total -= u; new_d.orbit->e += u;
       u = 0.50*total*(gsl_rng_uniform(r)-0.5); total -= u; new_d.orbit->i += u;
       u = 1.00*total*(gsl_rng_uniform(r)-0.5); total -= u; new_d.orbit->omega_node += u;
       u = 1.00*total*(gsl_rng_uniform(r)-0.5); total -= u; new_d.orbit->omega_pericenter += u; 
       u = 1.00*total*(gsl_rng_uniform(r)-0.5); total -= u; new_d.orbit->M += u;  
    */
    
    // cerr << "requested step size: " << step_size << endl;
    
    d->orbit->a                += step_size*(gsl_rng_uniform(r)-0.5)*d_a;
    d->orbit->e                += step_size*(gsl_rng_uniform(r)-0.5)*d_e;
    d->orbit->i                += step_size*(gsl_rng_uniform(r)-0.5)*d_i;
    d->orbit->omega_node       += step_size*(gsl_rng_uniform(r)-0.5)*d_omega_node;
    d->orbit->omega_pericenter += step_size*(gsl_rng_uniform(r)-0.5)*d_omega_pericenter;
    d->orbit->M                += step_size*(gsl_rng_uniform(r)-0.5)*d_M;
    
    // checks
    while (d->orbit->a < 0) d->orbit->a += 0.1*step_size*gsl_rng_uniform(r)*d_a;
    while (d->orbit->e < 0) d->orbit->e += 0.1*step_size*gsl_rng_uniform(r)*d_e;
    //
    do { d->orbit->i                = fmod(d->orbit->i+pi,pi);                      } while (d->orbit->i < 0);
    do { d->orbit->omega_node       = fmod(d->orbit->omega_node+twopi,twopi);       } while (d->orbit->omega_node < 0);
    do { d->orbit->omega_pericenter = fmod(d->orbit->omega_pericenter+twopi,twopi); } while (d->orbit->omega_pericenter < 0);
    do { d->orbit->M                = fmod(d->orbit->M+twopi,twopi);                } while (d->orbit->M < 0);
    
    // cerr << "proposed step size: " << M1(p,(void*)(&old_d)) << endl;
    
    // *(d->orbit) = *(new_d.orbit);
    // *(d->obs)   = *(new_d.obs);
    
  }
  
  /* 
     void P1(void *p) {
     data *d = (data*) p;
     printf(" [ %g %g %g ] ",d->orbit->a,d->orbit->e,d->orbit->i*(180/pi));
     }
  */
  
  /* 
     void OrbitSimulatedAnnealing(OrbitWithEpoch &orbit, vector<Observation> &obs) {
     
     // gsl_rng_env_setup();
     
     gsl_rng *r = gsl_rng_alloc(gsl_rng_gfsr4);
     
     data d;
     d.orbit = &orbit;
     d.obs   = &obs;
     
     gsl_siman_solve(r, &d, E1, S1, M1, P1,
     NULL, NULL, NULL, 
     sizeof(data), params);
     
     gsl_rng_free(r);
     }
  */
  
  // 
  
  /* 
     class par_class {
     public:
     UniverseTypeAwareTime epoch;
     vector<Observation>  *obs;
     };
  */
  
  /* 
     double OrbDiffCorr_f (const gsl_vector *v, void *params) {
     
     OrbitWithEpoch orbit;
     
     orbit.a                = gsl_vector_get(v,0);
     orbit.e                = gsl_vector_get(v,1);
     orbit.i                = gsl_vector_get(v,2);
     orbit.omega_node       = gsl_vector_get(v,3);
     orbit.omega_pericenter = gsl_vector_get(v,4);
     orbit.M                = gsl_vector_get(v,5);
     
     // orbit.ref_body = Body("Sun",GetMSun(),Vector(0,0,0),Vector(0,0,0));
     orbit.mu       = GetG()*GetMSun();
     
     par_class *par = (par_class*) params;
     
     orbit.epoch = par->epoch;
     
     vector<Observation> *obs = par->obs;
     
     // cerr << "f: obs->size() = " << obs->size() << endl;
     
     const double rms = RMS_residuals(*obs,orbit);
     
     // cerr << "f: RMS = " << rms << endl;
     
     return (rms);
     
     }
  */
  
  /* 
     void OrbitDifferentialCorrectionsMultimin(OrbitWithEpoch &orbit, vector<Observation> &obs) {
     
     par_class par;
     
     par.epoch = orbit.epoch;
     par.obs   = &obs;
     
     gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex,6);
     
     gsl_multimin_function diff_corr_function;
     
     diff_corr_function.f      = &OrbDiffCorr_f;
     diff_corr_function.n      = 6;
     diff_corr_function.params = &par;
     
     gsl_vector *x = gsl_vector_alloc(6);
     
     gsl_vector_set(x,0,orbit.a);
     gsl_vector_set(x,1,orbit.e);
     gsl_vector_set(x,2,orbit.i);
     gsl_vector_set(x,3,orbit.omega_node);
     gsl_vector_set(x,4,orbit.omega_pericenter);
     gsl_vector_set(x,5,orbit.M);
     
     gsl_vector *step_size = gsl_vector_alloc(6);
     
     gsl_vector_set(step_size,0, 10.0);
     gsl_vector_set(step_size,1,  1.0);
     gsl_vector_set(step_size,2, 10.0);
     gsl_vector_set(step_size,3,100.0);
     gsl_vector_set(step_size,4,100.0);
     gsl_vector_set(step_size,5,100.0);
     
     gsl_multimin_fminimizer_set (s, &diff_corr_function, x, step_size);
     
     size_t iter = 0;
     int status;
     do {
     
     iter++;
     
     status = gsl_multimin_fminimizer_iterate(s);
     
     if (status) break;
     
     status = gsl_multimin_test_size(gsl_multimin_fminimizer_size(s),1.0e-6);
     
     if (status == GSL_SUCCESS) printf ("Minimum found at:\n");
     
     printf ("%5d %.5f %.5f %.5f %10.5f\n", iter,
     gsl_vector_get (s->x, 0), 
     gsl_vector_get (s->x, 1), 
     gsl_vector_get (s->x, 2)*(180/pi), 
     // s->f);
     gsl_multimin_fminimizer_minimum(s));
     
     } while (status == GSL_CONTINUE && iter < 1000);
     
     // update orbit?
     orbit.a                = gsl_vector_get (s->x,0);
     orbit.e                = gsl_vector_get (s->x,1);
     orbit.i                = gsl_vector_get (s->x,2);
     orbit.omega_node       = gsl_vector_get (s->x,3);
     orbit.omega_pericenter = gsl_vector_get (s->x,4);
     orbit.M                = gsl_vector_get (s->x,5);
     
     // free
     gsl_multimin_fminimizer_free (s);
     gsl_vector_free (x);
     gsl_vector_free (step_size);
     
     }
  */
  
  // least sq.
  
  class par_class {
  public:
    vector<Observation> * obs;
  public:
    // IMPORTANT!
    // The orbit epoch must be placed here (as well as mu, if it can be
    // different from G*Msun) because is a constant part of the orbit.
    Date orbit_epoch;
    
    // another test, with the covariance matrix,
    // to have an estimate of the error on each orbial element
    gsl_matrix * covar;
    bool use_covar;
  };
  
  class least_sq_diff_par_class {
  public:
    OrbitWithEpoch orbit;
    Observation obs;
    int var_index;
  };
  
  int least_sq_f (const gsl_vector * v, void * params, gsl_vector * f) {
    
    OrbitWithEpoch orbit;
    
    orbit.a                = gsl_vector_get(v,0);
    orbit.e                = gsl_vector_get(v,1);
    orbit.i                = gsl_vector_get(v,2);
    orbit.omega_node       = gsl_vector_get(v,3);
    orbit.omega_pericenter = gsl_vector_get(v,4);
    orbit.M                = gsl_vector_get(v,5);
    
    {
      ORSA_DEBUG(
	      "least_sq_f():\n"
	      "orbit.a = %g\n"
	      "orbit.e = %g\n"
	      "orbit.i = %g\n",
	      orbit.a,orbit.e,orbit.i*(180/pi));
    }
    
    orbit.mu = GetG()*GetMSun();
    
    par_class * par = (par_class *) params;
    
    vector<Observation> * obs = par->obs;
    
    orbit.epoch = par->orbit_epoch;
    
    OptimizedOrbitPositions opt(orbit);
    
    size_t i;
    double arcsec;
    for (i=0;i<obs->size();++i) {
      arcsec = opt.PropagatedSky_J2000((*obs)[i].date,(*obs)[i].obscode).delta_arcsec((*obs)[i]);
      gsl_vector_set(f,i,arcsec);
      
      ORSA_ERROR("f[%02i] = %g",i,arcsec);
    }
    
    return GSL_SUCCESS;
  }
  
  double least_sq_diff_f(double x, void * params) {
    
    least_sq_diff_par_class * diff_par = (least_sq_diff_par_class *) params;
    
    OrbitWithEpoch orbit(diff_par->orbit);
    
    switch(diff_par->var_index) {
    case 0: orbit.a                = x; break;
    case 1: orbit.e                = x; break;
    case 2: orbit.i                = x; break;
    case 3: orbit.omega_node       = x; break;
    case 4: orbit.omega_pericenter = x; break;
    case 5: orbit.M                = x; break;
    }
    
    /* 
       ORSA_ERROR("least_sq_diff_f(...):   diff_par->var_index = %i   x = %26.20f",diff_par->var_index,x);
    */
    
    return (PropagatedSky_J2000(orbit,diff_par->obs.date,diff_par->obs.obscode).delta_arcsec(diff_par->obs));
  }
  
  int least_sq_df(const gsl_vector * v, void * params, gsl_matrix * J) {
    
    OrbitWithEpoch orbit;
    
    orbit.a                = gsl_vector_get(v,0);
    orbit.e                = gsl_vector_get(v,1);
    orbit.i                = gsl_vector_get(v,2);
    orbit.omega_node       = gsl_vector_get(v,3);
    orbit.omega_pericenter = gsl_vector_get(v,4);
    orbit.M                = gsl_vector_get(v,5);
    
    orbit.mu = GetG()*GetMSun();
    
    par_class * par = (par_class*) params;
    
    vector<Observation> * obs = par->obs;
    
    orbit.epoch = par->orbit_epoch;
    
    least_sq_diff_par_class diff_par;
    
    diff_par.orbit = orbit;
    
    gsl_function diff_f;
    double result, abserr;
    
    diff_f.function = &least_sq_diff_f;
    diff_f.params   = &diff_par;
    
    size_t i,j;
    for (i=0;i<obs->size();++i) {
      diff_par.obs = (*obs)[i];
      for (j=0;j<6;++j) {
	diff_par.var_index = j;
	
	/* 
	   // gsl_diff_central(&diff_f, gsl_vector_get(v,j), &result, &abserr);
	   gsl_deriv_central(&diff_f, gsl_vector_get(v,j), 1.0e-6, &result, &abserr);
	   gsl_matrix_set (J, i, j, result);  
	   //
	   fprintf(stderr,"[-6] diff[%03i][%i] = %20f +/- %20f at %20.12f\n",
	   i,j,result,abserr,gsl_vector_get(v,j));
	   
	   
	   
	   // gsl_diff_central(&diff_f, gsl_vector_get(v,j), &result, &abserr);
	   gsl_deriv_central(&diff_f, gsl_vector_get(v,j), 1.0e-8, &result, &abserr);
	   gsl_matrix_set (J, i, j, result);  
	   //
	   fprintf(stderr,"[-8] diff[%03i][%i] = %20f +/- %20f at %20.12f\n",
	   i,j,result,abserr,gsl_vector_get(v,j));
	   
	   
	   
	   // gsl_diff_central(&diff_f, gsl_vector_get(v,j), &result, &abserr);
	   gsl_deriv_central(&diff_f, gsl_vector_get(v,j), 1.0e-9, &result, &abserr);
	   gsl_matrix_set (J, i, j, result);  
	   //
	   fprintf(stderr,"[-9] diff[%03i][%i] = %20f +/- %20f at %20.12f\n",
	   i,j,result,abserr,gsl_vector_get(v,j));
	   
	   // gsl_diff_central(&diff_f, gsl_vector_get(v,j), &result, &abserr);
	   gsl_deriv_central(&diff_f, gsl_vector_get(v,j), 1.0e-12, &result, &abserr);
	   gsl_matrix_set (J, i, j, result);  
	   //
	   fprintf(stderr,"[-12]diff[%03i][%i] = %20f +/- %20f at %20.12f\n",
	   i,j,result,abserr,gsl_vector_get(v,j));
	*/
	
	/* 
	   // gsl_diff_central(&diff_f, gsl_vector_get(v,j), &result, &abserr);
	   gsl_deriv_central(&diff_f, gsl_vector_get(v,j), 1.0e-13, &result, &abserr);
	   gsl_matrix_set (J, i, j, result);  
	   //
	   fprintf(stderr,"[-13]diff[%03i][%i] = %20f +/- %20f at %20.12f\n",
	   i,j,result,abserr,gsl_vector_get(v,j));
	   
	   
	   // gsl_diff_central(&diff_f, gsl_vector_get(v,j), &result, &abserr);
	   gsl_deriv_central(&diff_f, gsl_vector_get(v,j), 1.0e-14, &result, &abserr);
	   gsl_matrix_set (J, i, j, result);  
	   //
	   fprintf(stderr,"[-14]diff[%03i][%i] = %20f +/- %20f at %20.12f\n",
	   i,j,result,abserr,gsl_vector_get(v,j));
	*/
	
	/* 
	   // a smarter one? (with numeric limits)
	   // gsl_diff_central(&diff_f, gsl_vector_get(v,j), &result, &abserr);
	   gsl_deriv_central(&diff_f, gsl_vector_get(v,j), gsl_vector_get(v,j)*100.0*std::numeric_limits<double>::epsilon(), &result, &abserr);
	   gsl_matrix_set (J, i, j, result);  
	   //
	   fprintf(stderr,"[lim]diff[%03i][%i] = %20f +/- %20f at %20.12f\n",
	   i,j,result,abserr,gsl_vector_get(v,j));
	*/
	
	/* 
	   if (par->use_covar) {
	   
	   cerr << "cov: j=" << j << " cov: " << sqrt(gsl_matrix_get(par->covar,j,j)) << endl;
	   
	   // again, but using the covar matrix to have an estimate of the step
	   gsl_deriv_central(&diff_f, gsl_vector_get(v,j), 0.001*sqrt(gsl_matrix_get(par->covar,j,j)), &result, &abserr);
	   gsl_matrix_set (J, i, j, result);  
	   //
	   fprintf(stderr,"[cov]diff[%03i][%i] = %20f +/- %20f at %20.12f\n",
	   i,j,result,abserr,gsl_vector_get(v,j));
	   } else {
	*/
	
	gsl_deriv_central(&diff_f, gsl_vector_get(v,j), gsl_vector_get(v,j)*1.0e4*std::numeric_limits<double>::epsilon(), &result, &abserr);
	gsl_matrix_set (J, i, j, result);  
	//
	fprintf(stderr,"[lim]diff[%03i][%i] = %20f +/- %20f at %20.12f\n",
		i,j,result,abserr,gsl_vector_get(v,j));
	
	// }
	
      }
    }
    
    return GSL_SUCCESS;
  }
  
  int least_sq_fdf (const gsl_vector *v, void *params, gsl_vector *f, gsl_matrix *J) {
    least_sq_f (v,params,f);
    least_sq_df(v,params,J);
    return GSL_SUCCESS;
  }
  
  // void OrbitDifferentialCorrectionsLeastSquares(OrbitWithEpoch &orbit, const vector<Observation> &obs) {
  void OrbitDifferentialCorrectionsLeastSquares(OrbitWithCovarianceMatrixGSL &orbit, const vector<Observation> &obs) {
    
    cerr << "inside odcls..." << endl;
    
    if (obs.size() < 6) {
      cerr << "at least 6 observations are needed for OrbitDifferentialCorrectionsLeastSquares..." << endl;
      return;
    }
    
    par_class par;
    
    par.orbit_epoch = orbit.epoch;
    
    par.obs = const_cast< vector<Observation>* > (&obs);
    
    par.covar = gsl_matrix_alloc(6,6);
    par.use_covar = false;
    
    /* 
       { // check par.obs
       cerr << "par.obs->size()=" << par.obs->size() << endl;
       int y,m,d;
       for (unsigned int k=0;k<par.obs->size();++k) {
       (*par.obs)[k].date.GetGregor(y,m,d);
       cerr << "(*par.obs)[" << k << "] date: " << y << " " << m << " " << d << "  t: " << (*par.obs)[k].date.GetJulian() << endl;
       }
       }
    */
    
    gsl_multifit_fdfsolver * s = gsl_multifit_fdfsolver_alloc(gsl_multifit_fdfsolver_lmsder, obs.size(), 6);
    // gsl_multifit_fdfsolver * s = gsl_multifit_fdfsolver_alloc(gsl_multifit_fdfsolver_lmder, obs.size(), 6);
    
    gsl_multifit_function_fdf least_sq_function;
    
    least_sq_function.f      = &least_sq_f;
    least_sq_function.df     = &least_sq_df;
    least_sq_function.fdf    = &least_sq_fdf;
    least_sq_function.n      = obs.size();
    least_sq_function.p      = 6;
    least_sq_function.params = &par;
    
    gsl_vector * x = gsl_vector_alloc(6);
    
    gsl_vector_set(x, 0, orbit.a);
    gsl_vector_set(x, 1, orbit.e);
    gsl_vector_set(x, 2, orbit.i);
    gsl_vector_set(x, 3, orbit.omega_node);
    gsl_vector_set(x, 4, orbit.omega_pericenter);
    gsl_vector_set(x, 5, orbit.M);
    
    gsl_multifit_fdfsolver_set(s,&least_sq_function,x);
    
    size_t iter = 0;
    int status;
    do {
      
      ++iter;
      
      status = gsl_multifit_fdfsolver_iterate(s);
      
      printf("itaration status = %s\n",gsl_strerror(status));
      
      /* 
	 printf ("iter: %3u  %15.8f  %15.8f  %15.8f   |f(x)| = %g\n",
	 iter,
	 gsl_vector_get (s->x, 0), 
	 gsl_vector_get (s->x, 1),
	 gsl_vector_get (s->x, 2), 
	 gsl_blas_dnrm2 (s->f));
      */
      
      // status = gsl_multifit_test_delta (s->dx, s->x, 0.0, 1e-4);      
      // status = gsl_multifit_test_delta (s->dx, s->x, 0.0, 1e-5);      
      // status = gsl_multifit_test_delta (s->dx, s->x, 0.0, 1e-6);
      status = gsl_multifit_test_delta (s->dx, s->x, 0.0, 1.0e-8);
      // status = gsl_multifit_test_delta (s->dx, s->x, 0.0, 1e-8);
      // status = gsl_multifit_test_delta (s->dx, s->x, 0.0, 1e-9);
      
      printf("convergence status = %s\n",gsl_strerror(status));
      
      { 
	gsl_matrix * covar = gsl_matrix_alloc(6,6);
	gsl_multifit_covar(s->J,0.0,covar);
	printf ("more info:\n"
		"a    = %12.6f +/- %12.6f\n"
		"e    = %12.6f +/- %12.6f\n"
		"i    = %12.6f +/- %12.6f\n"
		"\n",
		gsl_vector_get(s->x,0),sqrt(gsl_matrix_get(covar,0,0)),
		gsl_vector_get(s->x,1),sqrt(gsl_matrix_get(covar,1,1)),
		gsl_vector_get(s->x,2)*(180/pi),sqrt(gsl_matrix_get(covar,2,2))*(180/pi));
      }
      
      gsl_multifit_covar(s->J,0.0,par.covar);
      par.use_covar = true;
      
    } while ((status == GSL_CONTINUE) && (iter < 1024));
    
    gsl_matrix * covar = gsl_matrix_alloc(6,6);
    gsl_multifit_covar(s->J,0.0,covar);
    // gsl_matrix_fprintf(stdout, covar,"%g");
    
#define FIT(i) gsl_vector_get(s->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))
    
    printf("a    = %12.6f +/- %12.6f\n", FIT(0), ERR(0));
    printf("e    = %12.6f +/- %12.6f\n", FIT(1), ERR(1));
    printf("i    = %12.6f +/- %12.6f\n", FIT(2)*(180/pi), ERR(2)*(180/pi));
    printf("node = %12.6f +/- %12.6f\n", FIT(3)*(180/pi), ERR(3)*(180/pi));
    printf("peri = %12.6f +/- %12.6f\n", FIT(4)*(180/pi), ERR(4)*(180/pi));
    printf("M    = %12.6f +/- %12.6f\n", FIT(5)*(180/pi), ERR(5)*(180/pi));
    
    printf ("status = %s\n", gsl_strerror (status));
    
    // update orbit!
    orbit.a                = gsl_vector_get (s->x,0);
    orbit.e                = gsl_vector_get (s->x,1);
    orbit.i                = gsl_vector_get (s->x,2);
    orbit.omega_node       = gsl_vector_get (s->x,3);
    orbit.omega_pericenter = gsl_vector_get (s->x,4);
    orbit.M                = gsl_vector_get (s->x,5);
    
    // set covariance matrix
    {
      double covm[6][6];
      unsigned int i,j;
      for (i=0;i<6;++i) {
	for (j=0;j<6;++j) {
	  covm[i][j] = gsl_matrix_get(covar,i,j);
	}
      }
      orbit.SetCovarianceMatrix((double**)covm,Osculating);
    }
    
    // print errors and RMS.
    {
      OptimizedOrbitPositions opt(orbit);
      double arcsec, rms=0.0;
      for (unsigned int k=0;k<obs.size();++k) {
	arcsec = opt.PropagatedSky_J2000(obs[k].date,obs[k].obscode).delta_arcsec(obs[k]);
	rms += secure_pow(arcsec,2);
	fprintf(stderr,"delta_arcsec obs[%02i]: %g (%+.2f,%+.2f)\n",k,arcsec,
		(opt.PropagatedSky_J2000(obs[k].date,obs[k].obscode).ra().GetRad() -obs[k].ra.GetRad() )*(180/pi)*3600.0,
		(opt.PropagatedSky_J2000(obs[k].date,obs[k].obscode).dec().GetRad()-obs[k].dec.GetRad())*(180/pi)*3600.0);
      }
      rms = sqrt(rms/obs.size());
      fprintf(stderr,"\n RMS: %g\n\n",rms);
    }
    
    // free
    gsl_multifit_fdfsolver_free (s);
    gsl_vector_free (x);
    
    cerr << "out of odcls..." << endl;
  }
  
  //
  
  // gauss tool: search optimal velocity in the central point
  
  class g_v_class {
  public:
    vector<Observation> obs; // three observations
    Vector r; // position vector vector given at the epoch of the first observation (first in the obs vector...)
  };
  
  int gauss_v_f (const gsl_vector *v, void * params, gsl_vector *f) {
    
    Vector velocity;
    //
    velocity.x = gsl_vector_get(v,0);
    velocity.y = gsl_vector_get(v,1);
    velocity.z = gsl_vector_get(v,2);
    
    g_v_class *par = (g_v_class*) params;
    
    vector<Observation> &obs(par->obs);
    
    // cerr << "Length(r): " << (par->r).Length()   << endl;
    // cerr << "Length(v): " << (velocity).Length() << endl;
    
    OrbitWithEpoch orbit;
    orbit.Compute(par->r,velocity,GetG()*GetMSun(),obs[0].date);
    // orbit.epoch = obs[1].date;
    // orbit.epoch = obs[0].date;
    
    {
      // debug
      /* cerr << "gauss_v_f() ---> orbit.a = " << orbit.a << endl;
	 cerr << "gauss_v_f() ---> orbit.e = " << orbit.e << endl;
	 cerr << "gauss_v_f() ---> orbit.i = " << orbit.i*(180/pi) << endl;
      */
      ORSA_DEBUG(
	      "gauss_v_f():\n"
	      "orbit.a = %g\n"
	      "orbit.e = %g\n"
	      "orbit.i = %g\n",
	      orbit.a,orbit.e,orbit.i*(180/pi));
    }
    
    OptimizedOrbitPositions opt(orbit);
    
    size_t i;
    double arcsec;
    for (i=0;i<obs.size();++i) {
      arcsec = opt.PropagatedSky_J2000(obs[i].date,obs[i].obscode).delta_arcsec(obs[i]);
      gsl_vector_set(f,i,arcsec);
      // cerr << "f[" << i << "] = " << arcsec << endl;
    }
    
    return GSL_SUCCESS;
  }
  
  class gauss_v_diff_par_class {
  public:
    Vector r;
    Vector velocity;
    Observation obs;
    int var_index;
    Date orbit_epoch;
  };
  
  double gauss_v_diff_f(double x, void *params) {
    
    // cerr << "gauss_v_diff_f()..." << endl;
    
    gauss_v_diff_par_class *diff_par = (gauss_v_diff_par_class*) params;
    
    // cerr << "gauss_v_diff_f()... var_index = " << diff_par->var_index << endl;
    
    Vector velocity(diff_par->velocity);
    
    switch(diff_par->var_index) {
    case 0: velocity.x = x; break;
    case 1: velocity.y = x; break;
    case 2: velocity.z = x; break;
    }
    
    OrbitWithEpoch orbit;
    orbit.Compute(diff_par->r,velocity,GetG()*GetMSun(),diff_par->orbit_epoch);
    // orbit.epoch = diff_par->orbit_epoch;
    
    return (PropagatedSky_J2000(orbit,diff_par->obs.date,diff_par->obs.obscode).delta_arcsec(diff_par->obs));
  }
  
  int gauss_v_df (const gsl_vector *v, void *params, gsl_matrix *J) {
    
    Vector velocity;
    //
    velocity.x = gsl_vector_get(v,0);
    velocity.y = gsl_vector_get(v,1);
    velocity.z = gsl_vector_get(v,2);
    
    g_v_class *par = (g_v_class*) params;
    
    vector<Observation> &obs(par->obs);
    
    OrbitWithEpoch orbit;
    orbit.Compute(par->r,velocity,GetG()*GetMSun(),obs[0].date);
    // orbit.epoch = obs[1].date;
    // orbit.epoch = obs[0].date;
    
    gauss_v_diff_par_class diff_par;
    
    diff_par.r           = par->r;
    diff_par.velocity    = velocity;
    // diff_par.orbit_epoch = obs[1].date;
    diff_par.orbit_epoch = orbit.epoch;
    
    gsl_function diff_f;
    double result, abserr;
    
    diff_f.function = &gauss_v_diff_f;
    diff_f.params   = &diff_par;
    
    size_t i,j;
    for (i=0;i<obs.size();++i) {
      diff_par.obs = obs[i];
      for (j=0;j<3;++j) {
	diff_par.var_index = j;
	// gsl_diff_central (&diff_f, gsl_vector_get(v,j), &result, &abserr);
	gsl_deriv_central(&diff_f, gsl_vector_get(v,j), 1.0e-9, &result, &abserr);
	gsl_matrix_set (J, i, j, result);  
	// cerr << "diff: " << result << " +/- " << abserr << endl;
      }
    }
    
    return GSL_SUCCESS; 
  }
  
  int gauss_v_fdf (const gsl_vector *v, void *params, gsl_vector *f, gsl_matrix *J) {
    gauss_v_f (v,params,f);
    gauss_v_df(v,params,J);
    return GSL_SUCCESS;
  }
  
  // r and v are given at the epoch of the first obs, i.e. obs[0]
  void gauss_v(const Vector &r, Vector &v, const vector<Observation> &obs) {
    
    if (obs.size() < 2) {
      ORSA_ERROR("gauss_v(...) needs at least two observations...");
      return;
    }	
    
    // cerr << "..gauss_v..." << endl;
    
    g_v_class g_v;
    
    g_v.obs = obs;
    g_v.r   = r;
    
    gsl_multifit_fdfsolver *s = gsl_multifit_fdfsolver_alloc(gsl_multifit_fdfsolver_lmsder,obs.size(),3);
    
    gsl_multifit_function_fdf gauss_v_function;
    
    gauss_v_function.f      = &gauss_v_f;
    gauss_v_function.df     = &gauss_v_df;
    gauss_v_function.fdf    = &gauss_v_fdf;
    gauss_v_function.n      = obs.size();
    gauss_v_function.p      = 3;
    gauss_v_function.params = &g_v;
    
    gsl_vector *x = gsl_vector_alloc(3);
    
    cerr << "..inside, Length(r): " << (r).Length() << endl;
    
    gsl_vector_set(x,0,v.x);
    gsl_vector_set(x,1,v.y);
    gsl_vector_set(x,2,v.z);
    
    gsl_multifit_fdfsolver_set(s,&gauss_v_function,x);
    
    size_t iter = 0;
    int status;
    do {
      
      ++iter;
      
      status = gsl_multifit_fdfsolver_iterate (s);
      
      printf ("itaration status = %s\n", gsl_strerror (status));
      
      /* 
	 printf ("iter: %3u  %15.8f  %15.8f  %15.8f   |f(x)| = %g\n",
	 iter,
	 gsl_vector_get (s->x, 0), 
	 gsl_vector_get (s->x, 1),
	 gsl_vector_get (s->x, 2), 
	 gsl_blas_dnrm2 (s->f));
      */
      
      // status = gsl_multifit_test_delta (s->dx, s->x, 0.0, 1.0e-3);
      status = gsl_multifit_test_delta (s->dx, s->x, 0.0, 5.0e-2);
      
      printf ("convergence status = %s\n", gsl_strerror (status));
      
    } while ((status == GSL_CONTINUE) && (iter < 10));
    
    printf ("status = %s\n", gsl_strerror (status));
    
    // save results
    v.x = gsl_vector_get (s->x,0);
    v.y = gsl_vector_get (s->x,1);
    v.z = gsl_vector_get (s->x,2);
    
    // free
    gsl_multifit_fdfsolver_free (s);
    gsl_vector_free (x);
  }
  
  //
  
  // OrbitWithCovarianceMatrixGSL Compute(const vector<Observation> &obs_in) {
  OrbitWithCovarianceMatrixGSL Compute(const vector<Observation> & obs) {
    
    // vector<Observation> obs(obs_in);
    
    if (obs.size() < 3) {
      ORSA_ERROR("at least three observations are needed.");
      OrbitWithCovarianceMatrixGSL o;
      return o;
    }
    
    if (universe->GetUniverseType() != Real) {
      ORSA_ERROR("trying to compute an orbit using the wrong universe type: return.");
      OrbitWithCovarianceMatrixGSL o;
      return o;
    }	
    
    // test 
    /* 
       {
       OrbitWithEpoch orbit;
       
       orbit.a = 1.75;
       orbit.e = 0.39705;
       orbit.i = 54.52*(pi/180);
       orbit.omega_node       = 47.46   *(pi/180);
       orbit.omega_pericenter = 95.47   *(pi/180);
       orbit.M                = 322.918 *(pi/180);
       //
       Date date;
       date.SetJulian(52950+2400000.5);
       orbit.epoch.SetDate(date);
       //
       OrbitDifferentialCorrectionsLeastSquares(orbit,obs);
       exit(0);
       }
    */
    
    // needed?
    // sort(obs.begin(),obs.end());
    
    // cerr << "Orbit::Compute() observations: " << obs.size() << endl;
    
    // NOTE: check the reference system somewhere near here!!
    
    /* 
       if (0) {
       unsigned int k=0;
       while (k<obs.size()) {
       printf("obs. %i: JD = %f\n",k,obs[k].date.GetJulian());
       ++k;
       }
      }
    */
    
    // vector<OrbitWithEpoch> preliminary_orbits;
    vector<PreliminaryOrbit> preliminary_orbits;
    
    if (0) {
      vector<Observation> test_obs;
      //
      test_obs.push_back(obs[0]);
      test_obs.push_back(obs[obs.size()/2]);
      test_obs.push_back(obs[obs.size()-1]);
      //
      Compute_TestMethod(test_obs,preliminary_orbits);
    }
    
    if (0) {
      Compute_TestMethod(obs,preliminary_orbits);
    }
    
    // yet another test...
    if (0) {
      vector<Observation> test_obs;
      //
      test_obs.push_back(obs[0]);
      test_obs.push_back(obs[obs.size()/2]);
      test_obs.push_back(obs[obs.size()-1]);
      //
      Compute_Gauss(test_obs,preliminary_orbits);
    }
    
    if (1) {
      vector<ThreeObservations> triplet;
      triplet.clear();
      // BuildObservationTriplets(obs, triplet, FromUnits(2,HOUR));
      // BuildObservationTriplets(obs, triplet, FromUnits(20,DAY));
      BuildObservationTriplets(obs, triplet, FromUnits(270,DAY));
      //
      cerr << "triplets: " << triplet.size() << endl;
      
      unsigned int trial=0;
      while (trial<triplet.size()) {
	// cerr << "using triplet with weight = " << triplet[trial].Weight() << endl;
	//
	cerr << "delay 1->2: " << triplet[trial][1].date.GetJulian()-triplet[trial][0].date.GetJulian() << " days" << endl;
	cerr << "delay 2->3: " << triplet[trial][2].date.GetJulian()-triplet[trial][1].date.GetJulian() << " days" << endl;
	//
	
	Compute_Gauss(triplet[trial],preliminary_orbits);
	// Compute_TestMethod(triplet[trial],preliminary_orbits);
	
	++trial;
	//
	// if (trial > 1) break;
	// if (trial > 1) break;
	// if (preliminary_orbits.size() > 7) break;	
	if (trial > 500) break;
	
	cerr << "----trial: " << trial << endl;
      }
      
    }   
    
    // compute rms for each preliminary orbit
    { // don't remove THIS!!
      unsigned int r=0;
      while (r<preliminary_orbits.size()) {
	preliminary_orbits[r].ComputeRMS(obs);
	printf("%20.12f %20.12f %20.12f %20.12f\n",preliminary_orbits[r].a,preliminary_orbits[r].e,preliminary_orbits[r].i*(180/pi),preliminary_orbits[r].GetRMS());
	++r;
      }
    }
    //
    // sort preliminary orbits
    sort(preliminary_orbits.begin(),preliminary_orbits.end());
    
    {
      cerr << "total prelim. orbits: " << preliminary_orbits.size() << endl;
      unsigned int r=0;
      while (r<preliminary_orbits.size()) {
      	printf("%20.12f %20.12f %20.12f %20.12f\n",preliminary_orbits[r].a,preliminary_orbits[r].e,preliminary_orbits[r].i*(180/pi),preliminary_orbits[r].GetRMS());
	++r;
      }
    }
    
    // only the first
    OrbitDifferentialCorrectionsLeastSquares(preliminary_orbits[0],obs);
    
    if (0) {
      unsigned int r=0;
      // double rms;
      while (r<preliminary_orbits.size()) {
	// rms = RMS_residuals(obs,preliminary_orbits[r]);
	// printf("%20.12f %20.12f %20.12f %20.12f\n",preliminary_orbits[r].a,preliminary_orbits[r].e,preliminary_orbits[r].i*(180/pi),rms);
	// if (rms < 1.0*3600) OrbitSimulatedAnnealing(preliminary_orbits[r],obs);
	// if (rms < 500.0) OrbitDifferentialCorrectionsMultimin(preliminary_orbits[r],obs);
	// if (rms < 200.0) OrbitDifferentialCorrectionsLeastSquares(preliminary_orbits[r],obs);
	
	OrbitDifferentialCorrectionsLeastSquares(preliminary_orbits[r],obs);
	
	// test...
	if (0) {
	  
	  OptimizedOrbitPositions opt(preliminary_orbits[r]);
	  
	  vector<Observation> good_obs;
	  unsigned int k;
	  for (k=0;k<obs.size();++k) {
	    // if (PropagatedSky(preliminary_orbits[r],obs[k].date,obs[k].obscode).delta_arcsec(obs[k]) < 60.0) { // 5.0
	    // if (PropagatedSky(preliminary_orbits[r],obs[k].date,obs[k].obscode).delta_arcsec(obs[k]) < 60.0) { // 5.0
	    if (opt.PropagatedSky_J2000(obs[k].date,obs[k].obscode).delta_arcsec(obs[k]) < 300.0) { // 5.0, 60.0
	      good_obs.push_back(obs[k]);
	    }
	    if (good_obs.size()>12) break;
	  }
	  
	  cerr << "good obs.: " << good_obs.size() << endl;
	  
	  // improve orbit using 'good observations'
	  if (good_obs.size() > 3) OrbitDifferentialCorrectionsLeastSquares(preliminary_orbits[r],good_obs);
	}  
	
	++r;
      }
      
    }
    
    // WARNING: check on the preliminary_orbits dimension...
    // to be refined!
    return preliminary_orbits[0];
  }
  
  // gsl stuff
  struct poly_8_params {
    double coeff_6, coeff_3, coeff_0;
  };
  
  double poly_8     (double x, void *params);
  double poly_8_df  (double x, void *params);
  void   poly_8_fdf (double x, void *params, double *y, double *dy);
  
  double poly_8 (double x, void *params) {
    struct poly_8_params *p = (struct poly_8_params *) params;
    return (secure_pow(x,8) - p->coeff_6*secure_pow(x,6) - p->coeff_3*secure_pow(x,3) - p->coeff_0);
  }
  
  double poly_8_df (double x, void *params) {
    struct poly_8_params *p = (struct poly_8_params *) params;
    return (8*secure_pow(x,7) - 6*p->coeff_6*secure_pow(x,5) - 3*p->coeff_3*secure_pow(x,2));
  }
  
  void poly_8_fdf (double x, void *params, double *y, double *dy) {
    struct poly_8_params *p = (struct poly_8_params *) params;
    *y  = (secure_pow(x,8) - p->coeff_6*secure_pow(x,6) - p->coeff_3*secure_pow(x,3) - p->coeff_0);
    *dy = (8*secure_pow(x,7) - 6*p->coeff_6*secure_pow(x,5) - 3*p->coeff_3*secure_pow(x,2));
  }
  
  class poly_8_solution {
  public:
    double value, error;
  };
  
  void poly_8_gsl_solve(poly_8_params &params, vector<poly_8_solution> &solutions) {
    
    const int    max_iter = 100;
    const double x_start  = FromUnits(0.0,AU);
    const double x_incr   = FromUnits(0.2,AU);
    
    const double nominal_relative_accuracy = 1.0e-5;
    
    solutions.clear();
    
    poly_8_solution tmp_solution;
    
    double x, x0; // this value is not used
    int    gsl_status;
    // const  gsl_root_fdfsolver_type *T;
    
    gsl_root_fdfsolver *s;
    gsl_function_fdf FDF;
    
    /* 
       cerr << " poly_8_gsl_solve(): params.coeff_6 = " << params->coeff_6 << endl;
       cerr << " poly_8_gsl_solve(): params.coeff_3 = " << params->coeff_3 << endl;
       cerr << " poly_8_gsl_solve(): params.coeff_0 = " << params->coeff_0 << endl;
    */
    
    FDF.f      = &poly_8;
    FDF.df     = &poly_8_df;
    FDF.fdf    = &poly_8_fdf;
    FDF.params = &params;
    
    // T = gsl_root_fdfsolver_steffenson;
    // s = gsl_root_fdfsolver_alloc (T);
    s = gsl_root_fdfsolver_alloc(gsl_root_fdfsolver_steffenson);
    
    int iter = 0;
    while (iter<max_iter) {
      
      ++iter;
      x = x_start+iter*x_incr;
      gsl_root_fdfsolver_set (s, &FDF, x);
      
      int iter_gsl=0;
      const int max_iter_gsl = 100;
      do {
	++iter_gsl;
	gsl_status = gsl_root_fdfsolver_iterate(s);
	x0 = x;
	x = gsl_root_fdfsolver_root(s);
	gsl_status = gsl_root_test_delta (x, x0, nominal_relative_accuracy, 0);
	// if (gsl_status == GSL_SUCCESS) printf ("Converged:\n");
	// printf ("%5d %10.7f %10.7f\n",iter_gsl, x, x - x0);
      } while ((gsl_status == GSL_CONTINUE) && (iter_gsl < max_iter_gsl));
      
      if (gsl_status == GSL_SUCCESS) {
	tmp_solution.value = x;
      	// gsl_root_fdfsolver_iterate(s); tmp_solution.error = fabs(x-gsl_root_fdfsolver_root(s)); // GSL doc: ...the error can be estimated more accurately by taking the difference between the current iterate and next iterate rather than the previous iterate.
	tmp_solution.error = nominal_relative_accuracy;
	
	unsigned int k=0;
	bool duplicate=false;
	while (k<solutions.size()) {
	  if (fabs(solutions[k].value-tmp_solution.value) < (solutions[k].error+tmp_solution.error)) {
	    duplicate= true;
	    break;
	  }
	  ++k;
	}
	
	if (!duplicate) {
	  solutions.push_back(tmp_solution);
      	}
      }
    }
    
    gsl_root_fdfsolver_free(s);
    
    if (0) { 
      cerr << "solutions found: " << solutions.size() << endl;
      unsigned int k=0;
      while (k<solutions.size()) {
	cerr << k << ": "  << solutions[k].value << " accuracy: " << solutions[k].error << endl;
	++k;
      }
    }
  }
  
  void Compute_Gauss(const vector<Observation> & obs_in, vector<PreliminaryOrbit> & preliminary_orbits) {
    
    cerr << "inside Compute_Gauss(...)" << endl;
    
    // see A. E. Roy, "Orbital Motion", section 13.4
    
    PreliminaryOrbit orbit;
    
    vector<Observation> obs(obs_in);
    
    if (obs.size() != 3) {
      ORSA_ERROR("Compute_Gauss(): three observations needed.");
      return;
    }
    
    unsigned int j;
    
    // TODO: the obs. are sorted in time?
    
    sort(obs.begin(),obs.end());
    
    // debug
    /* {
       int l;
       for (l=0;l<3;++l) {
       printf("observation %i: julian=%14.5f ra=%g dec=%g\n",l,obs[l].date.GetJulian(),obs[l].ra.GetRad(),obs[l].dec.GetRad());
       }
       }
    */
    
    double tau[3];
    const double sqrt_GM = sqrt(GetG()*GetMSun());
    //
    tau[2] = sqrt_GM*FromUnits(obs[1].date.GetJulian()-obs[0].date.GetJulian(),DAY);
    tau[0] = sqrt_GM*FromUnits(obs[2].date.GetJulian()-obs[1].date.GetJulian(),DAY);
    tau[1] = sqrt_GM*FromUnits(obs[2].date.GetJulian()-obs[0].date.GetJulian(),DAY);
    
    /* 
       Config conf;
       OrsaConfigFile ocf(&conf);
       ocf.Read();
       ocf.Close();
    */
    
    Vector r;
    Vector R[3]; // heliocentric earth position
    {
      /* 
	 Vector tmp_v;
	 JPLFile jf(config->paths[JPL_EPHEM_FILE]->GetValue().c_str());
	 for (j=0;j<3;++j) { 
	 jf.GetEph(obs[j].date,EARTH,SUN,r,tmp_v);
	 R[j] = r;
	 }
      */
      //
      for (j=0;j<3;++j) { 
	R[j] = jpl_cache->GetJPLBody(EARTH,obs[j].date).position() - jpl_cache->GetJPLBody(SUN  ,obs[j].date).position();
      }
    }
    
    // add the observer location to the earth position
    {
      /* 
	 LocationFile lf;
	 lf.SetFileName(config->paths[OBSCODE]->GetValue().c_str());
	 lf.Read();
	 lf.Close();
      */
      for (j=0;j<3;++j) { 
	R[j] += location_file->ObsPos(obs[j].obscode,obs[j].date);
      }
    }
    
    Vector u_rho[3]; // object direction from the observer (unit vectors)
    for (j=0;j<3;++j) { 
      u_rho[j].x = cos(obs[j].dec) * cos(obs[j].ra);
      u_rho[j].y = cos(obs[j].dec) * sin(obs[j].ra);
      u_rho[j].z = sin(obs[j].dec);
      
      /* 
	 if (universe->GetReferenceSystem() == ECLIPTIC) {
	 u_rho[j].rotate(0.0,-obleq(obs[j].date).GetRad(),0.0);
	 }
      */
      
      /* 
	 if (universe->GetReferenceSystem() == ECLIPTIC) {
	 EquatorialToEcliptic_J2000(u_rho[j]);
	 }
      */
      
      if (universe->GetReferenceSystem() == ECLIPTIC) {
	// EquatorialToEcliptic_J2000(u_rho[j]);
	Angle obl = obleq_J2000();
	u_rho[j].rotate(0.0,-obl.GetRad(),0.0);
      }
      
    }
    
    Vector cross = Cross(u_rho[0],u_rho[2]);
    // const Vector f = cross/cross.Length();
    const Vector f = cross.Normalized();
    
    const double rho_1_f = u_rho[1]*f;
      
    const double R_0_f = R[0]*f;
    const double R_1_f = R[1]*f;
    const double R_2_f = R[2]*f;
    
    const double A = (tau[0]/tau[1]*R_0_f + tau[2]/tau[1]*R_2_f - R_1_f)/rho_1_f;
    // const double B = (tau[0]/tau[1]*(secure_pow(tau[1],2)-secure_pow(tau[0],2))*R_0_f + tau[2]/tau[1]*(secure_pow(tau[1],2)-secure_pow(tau[2],2))*R_2_f)/rho_1_f/6.0;
    const double B = (tau[0]/tau[1]*(tau[1]*tau[1]-tau[0]*tau[0])*R_0_f + tau[2]/tau[1]*(tau[1]*tau[1]-tau[2]*tau[2])*R_2_f)/rho_1_f/6.0;
    
    const double Xl_Ym_Zn = R[1]*u_rho[1];
    
    poly_8_params params;
    params.coeff_6 = R[1].LengthSquared() + A*A + 2*A*Xl_Ym_Zn;
    params.coeff_3 = 2*A*B + 2*B*Xl_Ym_Zn;
    params.coeff_0 = B*B;
    vector<poly_8_solution> solutions;
    poly_8_gsl_solve(params,solutions);
    
    if (solutions.size() > 0) {
      
      Vector rho[3];
      Vector r[3];
      Vector v; // v[0]
      double c[3];
      
      double tmp_length;
      double tmp_value;
      unsigned int p;
      for (p=0;p<solutions.size();++p) {
	
	// rho[1] = u_rho[1]*(A+(B/secure_pow(solutions[p].value,3)));
	// check
	// tmp_length = A + (B/secure_pow(solutions[p].value,3));
	tmp_value = solutions[p].value;
	tmp_length = A + (B/(tmp_value*tmp_value*tmp_value));
	// cerr << "tmp_length: " << tmp_length << endl;
	if (tmp_length <= 0.0) {
	  // cerr << "out..." << endl;
	  continue;
	}
	//
	rho[1] = u_rho[1]*tmp_length;
	
	r[1] = R[1] + rho[1];
	
	// standard relation
	for (j=0;j<3;++j) { 
	  // c[j] = tau[j]/tau[1]*(1+(secure_pow(tau[1],2)-secure_pow(tau[j],2))/(6*secure_pow(r[1].Length(),3)));
	  c[j] = tau[j]/tau[1]*(1+(tau[1]*tau[1]-tau[j]*tau[j])/(6*secure_pow(r[1].Length(),3)));
	  // printf("OLD c[%i] = %g\n",j,c[j]);
	}
	
	{
	  
	  const Vector v_k = rho[1] - (c[0]*R[0] + c[2]*R[2] - R[1]);
	  const double   k = v_k.Length();
	  // const Vector u_k = v_k/v_k.Length();
	  const Vector u_k = v_k.Normalized();
	  
	  const double s02 = u_rho[0]*u_rho[2];
	  const double s0k = u_rho[0]*u_k;
	  const double s2k = u_rho[2]*u_k;
	  
	  // rho[0] = u_rho[0]*(k*(s0k-s02*s2k)/(1-secure_pow(s02,2)))/c[0];
	  // tmp_length = (k*(s0k-s02*s2k)/(1-secure_pow(s02,2)))/c[0];
	  tmp_length = (k*(s0k-s02*s2k)/(1-s02*s02))/c[0];
	  if (tmp_length <= 0.0) {
	    // cerr << "out..." << endl;
	    continue;
	  }
	  //
	  rho[0] = u_rho[0]*tmp_length;
	  
	  // rho[2] = u_rho[2]*(k*(s2k-s02*s0k)/(1-secure_pow(s02,2)))/c[2];
	  // tmp_length = (k*(s2k-s02*s0k)/(1-secure_pow(s02,2)))/c[2];
	  tmp_length = (k*(s2k-s02*s0k)/(1-s02*s02))/c[2];
	  if (tmp_length <= 0.0) {
	    // cerr << "out..." << endl;
	    continue;
	  }
	  //
	  rho[2] = u_rho[2]*tmp_length;
	  
	  r[0] = rho[0] + R[0];
	  r[2] = rho[2] + R[2];
	  
	}
	
	if (0) { 
	  // refinements
	  int iter_gibbs=0;
	  double old_c[3];
	  double delta_c = 1.0;
	  while ((iter_gibbs<10) && (delta_c > 1.0e-6)) {
	    
	    // Gibbs relation (see i.e. F. Zagar, "Astronomia Sferica e teorica", chap. XVIII)
	    double Gibbs_B[3];
	    Gibbs_B[0] = (tau[1]*tau[2]-secure_pow(tau[0],2))/12;
	    Gibbs_B[1] = (tau[0]*tau[2]-secure_pow(tau[1],2))/12;
	    Gibbs_B[2] = (tau[0]*tau[1]-secure_pow(tau[2],2))/12;
	    double Brm3[3];
	    for (j=0;j<3;++j) { 
	      Brm3[j] = Gibbs_B[j]/secure_pow(r[j].Length(),3);
	    }
	    delta_c = 0.0;
	    for (j=0;j<3;++j) { 
	      old_c[j] = c[j];
	      c[j] = tau[j]/tau[1]*(1+Brm3[j])/(1-Brm3[1]);
	      delta_c += secure_pow(c[j]-old_c[j],2);
	      // printf("NEW c[%i] = %g\n",j,c[j]);
	    }
	    delta_c /= 3;
	    delta_c  = sqrt(delta_c);
	    
	    {
	      const Vector v_k = rho[1] - (c[0]*R[0] + c[2]*R[2] - R[1]);
	      const double   k = v_k.Length();
	      // const Vector u_k = v_k/Length(v_k);
	      const Vector u_k = v_k.Normalized();
	      
	      const double s02 = u_rho[0]*u_rho[2];
	      const double s0k = u_rho[0]*u_k;
	      const double s2k = u_rho[2]*u_k;
	      
	      rho[0] = u_rho[0]*(k*(s0k-s02*s2k)/(1-secure_pow(s02,2)))/c[0];
	      rho[2] = u_rho[2]*(k*(s2k-s02*s0k)/(1-secure_pow(s02,2)))/c[2];
	      
	      r[0] = rho[0] + R[0];
	      r[2] = rho[2] + R[2];
	    }
	    
	    ++iter_gibbs;
	  }
	 
	}
	
	// try a simpler rule
	// v = (r[1]-r[0])/(FromUnits(obs[0].date.GetJulian()-obs[1].date.GetJulian(),DAY));
	/* v = ( (r[1]-r[0])/(FromUnits(obs[1].date.GetJulian()-obs[0].date.GetJulian(),DAY)) + 
	   (r[2]-r[1])/(FromUnits(obs[2].date.GetJulian()-obs[1].date.GetJulian(),DAY)) ) / 2.0;
	*/
	// v = velocity at the epoch of the first obs, obs[0]
	// Vector v = (r[1]-r[0])/(FromUnits(obs[0].date.GetJulian()-obs[1].date.GetJulian(),DAY));
	Vector v = (r[1]-r[0])/(FromUnits(obs[1].date.GetJulian()-obs[0].date.GetJulian(),DAY));
	
	// light-time correction [to be checked!]
	r[0] += v*(r[0]-R[0]).Length()/GetC();
	
	// orbit.ref_body = Body("Sun",GetMSun(),Vector(0,0,0),Vector(0,0,0));
	// orbit.Compute(r[1],v,GetG()*GetMSun());
	orbit.Compute(r[0],v,GetG()*GetMSun(),obs[0].date);
	
	// cerr << "HHH ref body mass : " << ref_body.mass() << endl;
	
	// orbit.epoch = obs[1].date;
	// orbit.epoch = obs[0].date;
	
	// quick test
	if (orbit.e < 1.0) {
	  
	  // const Vector old_v = v;
	  // test!
	  //
	  // test... maybe is needed...
	  // gauss_v(r[0],v,obs);
	  //
	  //
	  // update the orbit with the improved velocity
	  // orbit.Compute(r[1],v,GetG()*GetMSun());
	  orbit.Compute(r[0],v,GetG()*GetMSun(),obs[0].date);
	  
	  // debug
	  cerr << " ---> orbit.a = " << orbit.a << endl;
	  cerr << " ---> orbit.e = " << orbit.e << endl;
	  cerr << " ---> orbit.i = " << orbit.i*(180/pi) << endl;
	  
	  // Sky sky;
	  // UniverseTypeAwareTime utat; utat.SetDate(obs[1].date);
	  // sky.Compute(rho[1],utat);
	  // cerr << obs[1].ra.GetRad() << "  " << obs[1].dec.GetRad() << "  " << sky.ra().GetRad() << "  " << sky.dec().GetRad() << "  " << RMS_residuals(obs) << "  " << obs[1].date.GetJulian()-obs[0].date.GetJulian() << "  " << obs[2].date.GetJulian()-obs[1].date.GetJulian() << endl;
	  
	  // cerr << "better..." << endl;
       	  /* 
	     int j;
	     Sky sky;
	     UniverseTypeAwareTime utat;
	     for (j=0;j<3;++j) {
	     utat.SetDate(obs[j].date); 
	     // sky.Compute(rho[j],utat);
	     sky.Compute(r[j]-R[j],utat);
	     fprintf(stderr,"SKY     ra = %30.20f   dec = %30.20f\n",obs[j].ra.GetRad(),obs[j].dec.GetRad());
	     fprintf(stderr,"COMP    ra = %30.20f   dec = %30.20f\n",sky.ra().GetRad(),sky.dec().GetRad());
	     }
	  */
	  
	  fprintf(stderr,"limited RMS: %g\n",RMS_residuals(obs,orbit));
	  
	  if (0) {
	    UniverseTypeAwareTime utat;
	    Sky sky;
	    double hand_RMS=0;
	    for (j=0;j<3;++j) {
	      // sky.Compute(r[j]-R[j],utat);
	      sky.Compute_J2000(r[j]-R[j]);
	      hand_RMS += ( secure_pow((obs[j].ra.GetRad()-sky.ra().GetRad())*cos(obs[j].dec.GetRad()),2) + 
			    secure_pow(obs[j].dec.GetRad()-sky.dec().GetRad(),2) );
	    }	
	    hand_RMS /= 3.0;
	    hand_RMS  = sqrt(hand_RMS);
	    hand_RMS *= (180/pi)*3600;
	    fprintf(stderr,"RMS by hands: %g\n",hand_RMS);
	    
	    // tests
	    if (hand_RMS > 1.0) {
	      cerr << "controlled exit..." << endl;
	      exit(0);
	    }
	    
	  }
	  
	} else {
	  // cerr << "orbit eccentricity > 1.0: a=" << orbit.a << "  e=" << orbit.e << "  i=" << orbit.i*180/pi << endl;
	}	
	
	/* 
	   if (orbit.e > 0.8) {
	   cerr << "prelim. orbit:   a = " << orbit.a << "   e = " << orbit.e << "  i = " << orbit.i*(180/pi) << endl;
	   } else {
	   cerr << "prelim. orbit:   a = " << orbit.a << "   e = " << orbit.e << "  i = " << orbit.i*(180/pi) << " RMS residuals (arcsec): " << RMS_residuals(obs,orbit) << endl;
	   }
	*/
	
	if (orbit.e < 1.0) preliminary_orbits.push_back(orbit);
      }
    }
    
    cerr << "out of Compute_Gauss(...)" << endl;
  }
  
  
  // 
  
  void Compute_TestMethod(const vector<Observation> &obs_in, vector<PreliminaryOrbit> &preliminary_orbits) {
    
    cerr << "inside Compute_TestMethod(...)" << endl;
    
    PreliminaryOrbit orbit;
    
    vector<Observation> obs(obs_in);
    
    unsigned int j;
    
    sort(obs.begin(),obs.end());
    
    /* 
       Config conf;
       OrsaConfigFile ocf(&conf);
       ocf.Read();
       ocf.Close();
    */
    
    const unsigned int size = obs.size();
    
    Vector R[size]; // heliocentric earth position
    {
      Vector tmp_r, tmp_v;
      JPLFile jf(config->paths[JPL_EPHEM_FILE]->GetValue().c_str());
      for (j=0;j<size;++j) { 
	jf.GetEph(obs[j].date,EARTH,SUN,tmp_r,tmp_v);
     	R[j] = tmp_r;
      }
    }
    
    // add the observer location to the earth position
    {
      
      /* 
	 LocationFile lf;
	 lf.SetFileName(config->paths[OBSCODE]->GetValue().c_str());
	 lf.Read();
	 lf.Close();
      */
      for (j=0;j<size;++j) { 
	R[j] += location_file->ObsPos(obs[j].obscode,obs[j].date);
      }
    }
    
    Vector u_rho[size]; // object direction from the observer (unit vectors)
    for (j=0;j<size;++j) { 
      u_rho[j].x = cos(obs[j].dec) * cos(obs[j].ra);
      u_rho[j].y = cos(obs[j].dec) * sin(obs[j].ra);
      u_rho[j].z = sin(obs[j].dec);
      
      /* 
	 if (universe->GetReferenceSystem() == ECLIPTIC) {
	 u_rho[j].rotate(0.0,-obleq(obs[j].date).GetRad(),0.0);
	 } 
      */
      
      /* 
	 if (universe->GetReferenceSystem() == ECLIPTIC) {
	 EquatorialToEcliptic_J2000(u_rho[j]);
	 }
      */
      
      if (universe->GetReferenceSystem() == ECLIPTIC) {
	// EquatorialToEcliptic_J2000(u_rho[j]);
	Angle obl = obleq_J2000();
	u_rho[j].rotate(0.0,-obl.GetRad(),0.0);
      }
      
    }
    
    Vector rho[size];
    Vector r[size];
    Vector v; // v[0]
    
    /* 
       double tmp_rho = FromUnits(0.2,EARTHMOON);
       while (tmp_rho < FromUnits(50,AU)) {
    */
    
    /* 
       double tmp_rho = FromUnits(0.5 ,AU);
       while (tmp_rho < FromUnits(0.55,AU)) {
    */
    
    double tmp_rho = FromUnits(0.535,AU);
    while (tmp_rho < FromUnits(0.536,AU)) {
      
      cerr << "[******] tmp_rho: " << tmp_rho << endl;
      
      for (j=0;j<size;++j) { 
	rho[j] = u_rho[j]*tmp_rho;	
   	r[j] = R[j] + rho[j];
      }
      
      v = (r[1]-r[0])/(FromUnits(obs[0].date.GetJulian()-obs[1].date.GetJulian(),DAY));
      
      // least sq. v
      gauss_v(r[0],v,obs);
      
      orbit.Compute(r[0],v,GetG()*GetMSun(),obs[0].date);
      // orbit.epoch = obs[0].date;
      
      // debug
      cerr << " ---> orbit.a = " << orbit.a << endl;
      cerr << " ---> orbit.e = " << orbit.e << endl;
      cerr << " ---> orbit.i = " << orbit.i*(180/pi) << endl;
      
      if (orbit.e < 1.0) preliminary_orbits.push_back(orbit);
      
      // tmp_rho *= 1.3;
      tmp_rho += 0.005;
    }
    
  }
  
  // OrbitWithCovarianceMatrixGSL
  
  OrbitWithCovarianceMatrixGSL::OrbitWithCovarianceMatrixGSL() : OrbitWithEpoch(), bool_have_covariance_matrix(false) {
    
  }
  
  OrbitWithCovarianceMatrixGSL::OrbitWithCovarianceMatrixGSL(Orbit &nominal_orbit) : OrbitWithEpoch(nominal_orbit), bool_have_covariance_matrix(false) {
    
  }
  
  OrbitWithCovarianceMatrixGSL::OrbitWithCovarianceMatrixGSL(Orbit &nominal_orbit, double **covariance_matrix, CovarianceMatrixElements base) : OrbitWithEpoch(nominal_orbit) {
    SetCovarianceMatrix(covariance_matrix,base);
  }
  
  void OrbitWithCovarianceMatrixGSL::SetCovarianceMatrix(double **covariance_matrix, CovarianceMatrixElements base) {
    memcpy((void*)covm, (const void*)covariance_matrix, 6*6*sizeof(double));
    cov_base = base;
    bool_have_covariance_matrix = true;
  }
  
  void OrbitWithCovarianceMatrixGSL::GetCovarianceMatrix(double **covariance_matrix, CovarianceMatrixElements &base) const {
    if (have_covariance_matrix()) {
      memcpy((void*)covariance_matrix, (const void*)covm, 6*6*sizeof(double));
      base = cov_base;
    } else {
      ORSA_ERROR("called method OrbitWithCovarianceMatrixGSL::GetCovarianceMatrix() from an orbit with undefined covariance matrix");
    }
  }
  
  // void OrbitWithCovarianceMatrixGSL::generate(vector<OrbitWithEpoch> &list, const int num, const int random_seed) const {
  void OrbitWithCovarianceMatrixGSL::GenerateUsingCholeskyDecomposition(vector<OrbitWithEpoch> & list, const int num, const int random_seed) const {
    
    list.clear();
    
    if (num < 1) return;
    
    if (!have_covariance_matrix()) {
      ORSA_ERROR("called method OrbitWithCovarianceMatrixGSL::GenerateUsingCholeskyDecomposition() from an orbit with undefined covariance matrix.");
      return;
    }
    
    gsl_vector * mu = gsl_vector_alloc(6);
    gsl_vector * x  = gsl_vector_alloc(6); // random vector
    gsl_vector * y  = gsl_vector_alloc(6);
    
    switch (cov_base) {
    case Osculating: {
      
      gsl_vector_set(mu,0,a);
      gsl_vector_set(mu,1,e);
      gsl_vector_set(mu,2,i);
      gsl_vector_set(mu,3,omega_node);
      gsl_vector_set(mu,4,omega_pericenter);
      gsl_vector_set(mu,5,M);
      
      break;
    }
    case Equinoctal: {
      
      gsl_vector_set(mu,0,a);
      gsl_vector_set(mu,1,e*sin(omega_node+omega_pericenter));
      gsl_vector_set(mu,2,e*cos(omega_node+omega_pericenter));
      gsl_vector_set(mu,3,tan(i/2)*sin(omega_node));
      gsl_vector_set(mu,4,tan(i/2)*cos(omega_node));
      gsl_vector_set(mu,5,fmod(omega_node+omega_pericenter+M,twopi));
      
      break;
    }
    }
    
    gsl_matrix * A = gsl_matrix_alloc(6,6);
    
    int i,k;
    for (i=0;i<6;++i) {
      for (k=0;k<6;++k) {
	gsl_matrix_set(A,i,k,covm[i][k]);
      }
    }
    
    // Cholesky decomposition
    int decomp_status = gsl_linalg_cholesky_decomp(A);
    
    if (decomp_status) {
      
      ORSA_WARNING("Cholesky decomposition failed.");
      
      gsl_vector_free(mu);
      gsl_vector_free(x);
      gsl_vector_free(y);
      gsl_matrix_free(A);
      
      return;
    }
    
    // remove L^T, the upper triangle (keep the diagonal elements!)
    for (i=0;i<6;++i) {
      for (k=i+1;k<6;++k) {
	gsl_matrix_set(A,i,k,0.0);
      }
    }
    
    // check
    // gsl_matrix_fprintf(stderr,A,"%g");
    
    OrbitWithEpoch gen_orb = (*this);
    
    int generated = 0;
    
    // random number generator
    gsl_rng * rnd = gsl_rng_alloc(gsl_rng_gfsr4);
    gsl_rng_set(rnd,random_seed);
    
    switch (cov_base) {
    case Osculating: {
      while (1) {
	
	for (i=0;i<6;++i) gsl_vector_set(x,i,gsl_ran_ugaussian(rnd));
	
	// y = A x + mu
	gsl_vector_memcpy(y,mu);
	gsl_blas_dgemv(CblasNoTrans,1.0,A,x,1.0,y);
	
	++generated;
	
	gen_orb.a                 = gsl_vector_get (y,0);
	gen_orb.e                 = gsl_vector_get (y,1);
     	gen_orb.i                 = gsl_vector_get (y,2);
	gen_orb.omega_node        = gsl_vector_get (y,3);
	gen_orb.omega_pericenter  = gsl_vector_get (y,4);
	gen_orb.M                 = gsl_vector_get (y,5);
	
	list.push_back(gen_orb);
	
	if (generated>=num) break;
      }
      break;
    }
    case Equinoctal: {
      
      while (1) {
	
	for (i=0;i<6;++i) gsl_vector_set(x,i,gsl_ran_ugaussian(rnd));
	
	// y = A x + mu
	gsl_vector_memcpy(y,mu);
     	gsl_blas_dgemv(CblasNoTrans,1.0,A,x,1.0,y);
	
	++generated;
	
	const double omega_tilde  = secure_atan2(gsl_vector_get(y,1),gsl_vector_get(y,2));
	
	gen_orb.a                 = gsl_vector_get(y,0);
	gen_orb.e                 = sqrt(gsl_vector_get(y,1)*gsl_vector_get(y,1)+gsl_vector_get(y,2)*gsl_vector_get(y,2));
	gen_orb.i                 = 2.0*atan(sqrt(gsl_vector_get(y,3)*gsl_vector_get(y,3)+gsl_vector_get(y,4)*gsl_vector_get(y,4)));
	gen_orb.omega_node        = fmod(10.0*twopi+secure_atan2(gsl_vector_get(y,3),gsl_vector_get(y,4)),twopi);
	gen_orb.omega_pericenter  = fmod(10.0*twopi+omega_tilde-omega_node,twopi);
	gen_orb.M                 = fmod(10.0*twopi+gsl_vector_get(y,5)-omega_tilde,twopi);
	
	list.push_back(gen_orb);
	
	if (generated>=num) break;
      }
      break;
    }
    }
    
    gsl_vector_free(mu);
    gsl_vector_free(x);
    gsl_vector_free(y);
    gsl_matrix_free(A);
    
    // free random number generator
    gsl_rng_free(rnd);
    
  }
  
  /*******/
  
  void OrbitWithCovarianceMatrixGSL::GenerateUsingPrincipalAxisTransformation(vector<OrbitWithEpoch> & list, const int num, const int random_seed) const {
    
    list.clear();
    
    if (num < 1) return;
    
    if (!have_covariance_matrix()) {
      ORSA_ERROR("called method OrbitWithCovarianceMatrixGSL::GenerateUsingPrincipalAxisTransformation() from an orbit with undefined covariance matrix.");
      return;
    }
    
    gsl_vector * mu = gsl_vector_alloc(6);
    gsl_vector * x  = gsl_vector_alloc(6); // random vector
    gsl_vector * y  = gsl_vector_alloc(6);
    
    switch (cov_base) {
    case Osculating: {
      
      gsl_vector_set(mu,0,a);
      gsl_vector_set(mu,1,e);
      gsl_vector_set(mu,2,i);
      gsl_vector_set(mu,3,omega_node);
      gsl_vector_set(mu,4,omega_pericenter);
      gsl_vector_set(mu,5,M);
      
      break;
    }
    case Equinoctal: {
      
      gsl_vector_set(mu,0,a);
      gsl_vector_set(mu,1,e*sin(omega_node+omega_pericenter));
      gsl_vector_set(mu,2,e*cos(omega_node+omega_pericenter));
      gsl_vector_set(mu,3,tan(i/2)*sin(omega_node));
      gsl_vector_set(mu,4,tan(i/2)*cos(omega_node));
      gsl_vector_set(mu,5,fmod(omega_node+omega_pericenter+M,twopi));
      
      break;
    }
    }
    
    gsl_matrix * A = gsl_matrix_alloc(6,6);
    
    for (unsigned int i=0;i<6;++i) {
      for (unsigned int k=0;k<6;++k) {
	gsl_matrix_set(A,i,k,covm[i][k]);
      }
    }
    
    // covariance matrix view
    // const gsl_matrix_view m = gsl_matrix_view_array(A,6,6);
    
    gsl_permutation * p = gsl_permutation_alloc(6);
    gsl_matrix * LU = gsl_matrix_alloc(6,6);
    gsl_matrix_memcpy(LU,A);  
    int s;
    
    // covariance matrix LU decomposition
    gsl_linalg_LU_decomp(LU, p, &s);
    
    // covariance matrix inverse
    gsl_matrix * inv = gsl_matrix_alloc(6,6);
    gsl_linalg_LU_invert(LU, p, inv);
    
    // covariance matrix determinant
    // const double det = gsl_linalg_LU_det(LU, s);
    
    // covariance eigenvalues and eigenvectors
    gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc(6); // workspace for eigenvectors/values
    gsl_vector * eval = gsl_vector_alloc(6);    // eigenvalues
    gsl_matrix * evec = gsl_matrix_alloc(6,6);  // eigenvectors
    //
    gsl_eigen_symmv (A, eval, evec, w); // NOTE: The diagonal and lower triangular part of A are destroyed during the computation
    
    /********/
    
    {
      // debug
      for (unsigned int i=0;i<6;++i) {
	cerr << "eval[" << i << "] = " << gsl_vector_get(eval,i) << endl;
      }
    }
    
    OrbitWithEpoch gen_orb = (*this);
    
    int generated = 0;
    
    // random number generator
    gsl_rng * rnd = gsl_rng_alloc(gsl_rng_gfsr4);
    gsl_rng_set(rnd,random_seed);
    
    double sigma[6];
    for (unsigned int i=0;i<6;++i) {
      if (gsl_vector_get(eval,i) < 0.0) {
	
	ORSA_ERROR("problems with the covariance matrix: negative eigenvalue found.");
	
	// free GSL stuff
	gsl_vector_free(mu);
	gsl_vector_free(x);
	gsl_vector_free(y);
	gsl_matrix_free(A);
	gsl_permutation_free(p);
	gsl_matrix_free(LU); 
	gsl_matrix_free(inv);
	gsl_eigen_symmv_free(w);
	gsl_vector_free(eval);
	gsl_matrix_free(evec);
	gsl_rng_free(rnd);
	
	return;
      }
      
      sigma[i] = secure_sqrt(gsl_vector_get(eval,i));
    }
    
    switch (cov_base) {
    case Osculating: {
      while (1) {
	
	for (unsigned int i=0;i<6;++i) {
       	  gsl_vector_set(x,i,gsl_ran_gaussian(rnd,sigma[i]));
	}
	
	gsl_vector_memcpy(y,mu);
	gsl_blas_dgemv(CblasNoTrans,1.0,evec,x,1.0,y);
	
	++generated;
	
	gen_orb.a                 = gsl_vector_get (y,0);
	gen_orb.e                 = gsl_vector_get (y,1);
     	gen_orb.i                 = gsl_vector_get (y,2);
	gen_orb.omega_node        = gsl_vector_get (y,3);
	gen_orb.omega_pericenter  = gsl_vector_get (y,4);
	gen_orb.M                 = gsl_vector_get (y,5);
	
	list.push_back(gen_orb);
	
	if (generated>=num) break;
      }
      break;
    }
    case Equinoctal: {
      while (1) {
	
	for (unsigned int i=0;i<6;++i) {
	  gsl_vector_set(x,i,gsl_ran_gaussian(rnd,sigma[i]));
	}
	
	gsl_vector_memcpy(y,mu);
	gsl_blas_dgemv(CblasNoTrans,1.0,evec,x,1.0,y);
	
	++generated;
	
	const double omega_tilde  = secure_atan2(gsl_vector_get(y,1),gsl_vector_get(y,2));
	
	gen_orb.a                 = gsl_vector_get(y,0);
	gen_orb.e                 = sqrt(gsl_vector_get(y,1)*gsl_vector_get(y,1)+gsl_vector_get(y,2)*gsl_vector_get(y,2));
	gen_orb.i                 = 2.0*atan(sqrt(gsl_vector_get(y,3)*gsl_vector_get(y,3)+gsl_vector_get(y,4)*gsl_vector_get(y,4)));
	gen_orb.omega_node        = fmod(10.0*twopi+secure_atan2(gsl_vector_get(y,3),gsl_vector_get(y,4)),twopi);
	gen_orb.omega_pericenter  = fmod(10.0*twopi+omega_tilde-omega_node,twopi);
	gen_orb.M                 = fmod(10.0*twopi+gsl_vector_get(y,5)-omega_tilde,twopi);
	
	list.push_back(gen_orb);
	
	if (generated>=num) break;
      }
      break;
    }
    }
    
    // free GSL stuff
    gsl_vector_free(mu);
    gsl_vector_free(x);
    gsl_vector_free(y);
    gsl_matrix_free(A);
    gsl_permutation_free(p);
    gsl_matrix_free(LU); 
    gsl_matrix_free(inv);
    gsl_eigen_symmv_free(w);
    gsl_vector_free(eval);
    gsl_matrix_free(evec);
    gsl_rng_free(rnd);
    
  } 
  
  // Close Approaches
  
  class CloseApproaches_gsl_parameters {
  public:
    Frame f; // reference frame, never changed
    unsigned int obj_index, index;
    Evolution * e;
  };
  
  double CloseApproaches_gsl_f(const gsl_vector * v, void * params) {
    
    CloseApproaches_gsl_parameters * p = (CloseApproaches_gsl_parameters *) params;
    
    Frame f = p->f;
    
    const UniverseTypeAwareTime gsl_time(gsl_vector_get(v,0));
    
    p->e->clear();
    p->e->push_back(f);
    p->e->SetSamplePeriod(f - gsl_time); 
    p->e->SetMaxUnsavedSubSteps(10000);
    p->e->Integrate(gsl_time,true);
    
    f = (*(p->e))[p->e->size()-1];
    
    return (f[p->obj_index].position() - f[p->index].position()).Length();
  }
  
  // void ComputeCloseApproaches(const Frame & f, const unsigned int obj_index, const UniverseTypeAwareTime & utat_start, const UniverseTypeAwareTime & utat_stop, vector<CloseApproach> & clapp, const double distance_threshold, const double sample_period) {
  
  /* 
     void ComputeCloseApproaches(const Frame & f, const unsigned int obj_index, const UniverseTypeAwareTime & utat_start, const UniverseTypeAwareTime & utat_stop, vector<CloseApproach> & clapp, const double distance_threshold, const UniverseTypeAwareTimeStep & sample_period) {
     
     clapp.clear();
     
     if (obj_index >= f.size()) return;
     
     // double t_start, t_stop;
     UniverseTypeAwareTime t_start, t_stop;
     if (utat_start < utat_stop) {
     t_start = utat_start;
     t_stop  = utat_stop;
     } else if (utat_start > utat_stop) {
     t_start = utat_stop;
     t_stop  = utat_start;
     } else {
     // same times
     return;
     }
     
     vector<Frame> frames;
     
     Radau15 itg; 
     itg.accuracy = 1.0e-12;
     itg.timestep = FromUnits(1,DAY);
     
     // Newton newton;
     Interaction * itr = 0;
     
     #ifdef HAVE_MPI
     int size;
     MPI_Comm_size( MPI_COMM_WORLD, &size );
     if (size > 1) {
     itr = new Newton_MPI();
     } else {
     itr = new Newton();
     }
     #else
     itr = new Newton();
     #endif
     
     Evolution evol;
     evol.SetInteraction(itr);
     evol.SetIntegrator(&itg);
     evol.SetMaxUnsavedSubSteps(100000);
     evol.SetSamplePeriod(sample_period);
     
     delete itr;
     
     Frame running_frame;
     
     cerr << "clapp [1]" << endl;
     
     if (t_start < f) {
     if (t_stop < f) {
     evol.push_back(f);
     evol.Integrate(t_stop);
     running_frame = evol[evol.size()-1];
     evol.clear();
     evol.push_back(running_frame);
     evol.Integrate(t_start);
     frames.resize(evol.size());
     for (unsigned int k=0;k<evol.size();++k) {
     frames[k] = evol[k];
     }
     } else {
     evol.push_back(f);
     evol.Integrate(t_start);
     // frames = evol;
     frames.resize(evol.size());
     for (unsigned int k=0;k<evol.size();++k) {
     frames[k] = evol[k];
     }
     evol.clear();
     evol.push_back(f);
     evol.Integrate(t_stop);
     unsigned int k=1; // skip first frame, already saved in frames
     while (k<evol.size()) {
     frames.push_back(evol[k]);
     ++k;
     }
     }
     } else {
     evol.push_back(f);
     evol.Integrate(t_start);
     running_frame = evol[evol.size()-1];
     evol.clear();
     evol.push_back(running_frame);
     evol.Integrate(t_stop);
     frames.resize(evol.size());
     for (unsigned int k=0;k<evol.size();++k) {
     frames[k] = evol[k];
     }
     }
     
     cerr << "clapp [2]" << endl;
     
     evol.clear();
     
     sort(frames.begin(),frames.end());
     
     // gsl init
     CloseApproaches_gsl_parameters par;
     par.e = &evol;
     //
     gsl_multimin_fminimizer * s = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex,1);
     //
     gsl_multimin_function clapp_function;
     clapp_function.f      = &CloseApproaches_gsl_f;
     clapp_function.n      = 1;
     clapp_function.params = &par;
     //
     gsl_vector * x         = gsl_vector_alloc(1);
     gsl_vector * step_size = gsl_vector_alloc(1);
     
     cerr << "clapp [3]" << endl;
     
     vector<double> distance;
     distance.resize(frames.size());
     CloseApproach ca;
     unsigned int index=0, j;
     while (index < f.size()) {
     if (index != obj_index) {
     
     j=0;
     while (j<frames.size()) {
     distance[j] = (frames[j][obj_index].position() - frames[j][index].position()).Length();
     ++j;
     } 
     
     // skip first and last
     j=1;
     while (j<(frames.size()-1)) {
     
     if ( (distance[j] < 1.5*distance_threshold) &&
     (distance[j] < distance[j-1]) &&
     (distance[j] < distance[j+1]) ) {
     
     // gsl stuff
     par.f         = frames[j];
     par.obj_index = obj_index;
     par.index     = index;
     
     gsl_vector_set(x,0,frames[j].GetTime());
     
     gsl_vector_set(step_size,0,sample_period.GetDouble());
     
     gsl_multimin_fminimizer_set(s, &clapp_function, x, step_size);
     
     cerr << "clapp [4]" << endl;
     
     size_t iter = 0;
     int status;
     do {
     
     ++iter;
     
     // iter status
     status = gsl_multimin_fminimizer_iterate(s);
     
     // convergence status
     // status = gsl_multimin_test_size(gsl_multimin_fminimizer_size(s),1.0e-6);
     status = gsl_multimin_test_size(gsl_multimin_fminimizer_size(s),FromUnits(1,MINUTE));
     
     } while (status == GSL_CONTINUE && iter < 200);
     
     // const double minimum = gsl_multimin_fminimizer_minimum(s);
     
     if (status == GSL_SUCCESS) {
     if (gsl_multimin_fminimizer_minimum(s) < distance_threshold) {
     ca.name = frames[j][index].name();
     ca.epoch.SetTime(gsl_vector_get(s->x,0));
     ca.distance = gsl_multimin_fminimizer_minimum(s);
     ca.relative_velocity = (frames[j][obj_index].velocity() - frames[j][index].velocity()).Length(); // should use a better value
     clapp.push_back(ca);
     }
     }
     
     cerr << "clapp [5]" << endl;
     
     }
     ++j;
     }
     
     }
     ++index;
     }
     
     // gsl free
     gsl_multimin_fminimizer_free(s);
     gsl_vector_free(x);
     gsl_vector_free(step_size);
     }
  */
  
  void SearchCloseApproaches(const Evolution * evol, const unsigned int obj_index, const unsigned int index, std::vector<CloseApproach> & clapp, const double distance_threshold, const double time_accuracy) {
    
    if (index == obj_index) return;
    
    // gsl init
    CloseApproaches_gsl_parameters par;
    par.e = new Evolution(*evol);
    //
    gsl_multimin_fminimizer * s = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex,1);
    //
    gsl_multimin_function clapp_function;
    clapp_function.f      = &CloseApproaches_gsl_f;
    clapp_function.n      = 1;
    clapp_function.params = &par;
    //
    gsl_vector * x         = gsl_vector_alloc(1);
    gsl_vector * step_size = gsl_vector_alloc(1);
    
    // cerr << "clapp [3]" << endl;
    
    vector<double> distance;
    distance.resize(evol->size());
    
    CloseApproach ca;
    
    for (unsigned int j=0;j<evol->size();++j) {
      distance[j] = ((*evol)[j][obj_index].position() - (*evol)[j][index].position()).Length();
    } 
    
    // skip first and last
    for (unsigned int j=1;j<(evol->size()-1);++j) {
      
      if ( (distance[j] <= 1.5*distance_threshold) &&
	   (distance[j] <= distance[j-1]) &&
	   (distance[j] <= distance[j+1]) ) {
	
	// gsl stuff
	par.f         = (*evol)[j];
	par.obj_index = obj_index;
	par.index     = index;
	
	gsl_vector_set(x,0,(*evol)[j].GetTime());
	
	gsl_vector_set(step_size,0,evol->GetSamplePeriod().GetDouble());
	
	gsl_multimin_fminimizer_set(s, &clapp_function, x, step_size);
	
	// cerr << "clapp [4]" << endl;
	
	size_t iter = 0;
	int status;
	do {
	  
	  ++iter;
	  
	  // iter status
	  status = gsl_multimin_fminimizer_iterate(s);
	  
	  // convergence status
	  // status = gsl_multimin_test_size(gsl_multimin_fminimizer_size(s),FromUnits(1,SECOND));
	  // status = gsl_multimin_test_size(gsl_multimin_fminimizer_size(s),FromUnits(10.0,MINUTE));
	  // status = gsl_multimin_test_size(gsl_multimin_fminimizer_size(s),FromUnits(1,HOUR));
	  status = gsl_multimin_test_size(gsl_multimin_fminimizer_size(s),time_accuracy);
	  
	} while (status == GSL_CONTINUE && iter < 200);
	
	// const double minimum = gsl_multimin_fminimizer_minimum(s);
	
	if (status == GSL_SUCCESS) {
	  if (gsl_multimin_fminimizer_minimum(s) < distance_threshold) {
	    ca.name = (*evol)[j][index].name();
	    ca.epoch.SetTime(gsl_vector_get(s->x,0));
	    ca.distance = gsl_multimin_fminimizer_minimum(s);
	    
	    {
	      Frame f = par.f;
	      //
	      const UniverseTypeAwareTime gsl_time(gsl_vector_get(s->x,0));
	      //
	      par.e->clear();
	      par.e->push_back(f);
	      par.e->SetSamplePeriod(f - gsl_time); 
	      par.e->SetMaxUnsavedSubSteps(10000);
	      par.e->Integrate(gsl_time,true);
	      //
	      f = (*(par.e))[par.e->size()-1];
	      //
	      ca.relative_velocity = (f[obj_index].velocity() - f[index].velocity()).Length(); 
	      
	      // target plane
	      {
		const Vector unit_v = (f[obj_index].velocity() - f[index].velocity()).Normalize();
		const Vector d      =  f[obj_index].position() - f[index].position();
		const Vector unit_d = d.Normalized();
		const Vector unit_q = ExternalProduct(unit_v,unit_d).Normalize();
		
		// d' = \alpha d + \beta q
		// and d'.z = 0.0
		
		if (unit_d.z != 0.0) {
		  const double beta = sqrt(+ unit_q.x*unit_q.x 
					   + unit_q.y*unit_q.y
					   + unit_q.z*unit_q.z/(unit_d.z*unit_d.z)*(unit_d.x*unit_d.x+unit_d.y*unit_d.y)
					   - 2*(unit_q.z/unit_d.z)*(unit_q.x*unit_d.y+unit_q.y*unit_d.x));
		  const double alpha = - beta * unit_q.z / unit_d.z;
		  
		  const Vector new_unit_d = (alpha*unit_d+beta*unit_q).Normalize();
		  const Vector new_unit_q = ExternalProduct(unit_v,new_unit_d).Normalize();
		  
		  // this solves the ambiguity in beta, that has 2 solutions: +/- sqrt(...)
		  if (new_unit_q.z > 0.0) {
		    ca.tp_xy = d*new_unit_d;
		    ca.tp_z  = d*new_unit_q;
		  } else {
		    ca.tp_xy = -d*new_unit_d;
		    ca.tp_z  = -d*new_unit_q;
		  }
		  
		  /* 
		     {
		     // debug 
		     cerr << "new_unit_d: " << new_unit_d.x << "  " << new_unit_d.y << "  " << new_unit_d.z << endl;
		     cerr << "new_unit_q: " << new_unit_q.x << "  " << new_unit_q.y << "  " << new_unit_q.z << endl;
		     cerr << "d*unit_v:   " << d*unit_v << endl;
		     cerr << "sqrt((d*new_unit_d)*(d*new_unit_d)+(d*new_unit_q)*(d*new_unit_q)): " << sqrt((d*new_unit_d)*(d*new_unit_d)+(d*new_unit_q)*(d*new_unit_q)) << endl;
		     }
		  */
		  
		} else {
		  ca.tp_xy = d.Length();
		  ca.tp_z  = 0.0;
		}
		
		// ca.tp_xy = d*unit_xy;
		// ca.tp_z  = d*unit_z;
		
	      }
	      
	    }
	    //
	    clapp.push_back(ca);
	  }
	  
	}
	
	// cerr << "clapp [5]" << endl;
	
      }
      
    }
    
    // }
    
    // gsl free
    gsl_multimin_fminimizer_free(s);
    gsl_vector_free(x);
    gsl_vector_free(step_size);
    
    delete par.e;
  }
  
} // namespace orsa
