/* 
   ORSA - Orbit Reconstruction, Simulation and Analysis
   Copyright (C) 2002-2004 Pasquale Tricarico
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   As a special exception, Pasquale Tricarico gives permission to
   link this program with Qt commercial edition, and distribute the
   resulting executable, without including the source code for the Qt
   commercial edition in the source distribution.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef _ORSA_SECURE_MATH_H_
#define _ORSA_SECURE_MATH_H_

namespace orsa {
  
  // avoids domain errors when x<0 and non-integer y
  double secure_pow(double x, double y);
  
  // avoids domain errors when x<=0
  double secure_log(double x);
  
  // avoids domain errors when x<=0
  double secure_log10(double x);
  
  // avoids domain errors when x=y=0
  double secure_atan2(double x, double y);
  
  // avoids domain errors when x is not in [-1,1]
  double secure_asin(double x);
  
  // avoids domain errors when x is not in [-1,1]
  double secure_acos(double x);
  
  // avoids domain errors when x<0
  double secure_sqrt(double x);
  
} // namespace orsa 

#endif // _ORSA_SECURE_MATH_H_
