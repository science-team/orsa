#ifndef _SUPPORT_H
#define _SUPPORT_H

#ifdef _WIN32
#ifndef __GNUC__
double rint(double);
double copysign(double, double);
double cbrt(double);
#endif
void sleep(int secs);
#endif

#endif

