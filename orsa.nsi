; orsa.nsi
;

; The name of the installer
Name "WinOrsa 0.7.0"

; The file to write
OutFile "winorsa-0.7.0.exe"

; show the installation details
ShowInstDetails "show"
ShowUninstDetails "show"

; The default installation directory
InstallDir $PROGRAMFILES\WinOrsa

; Registry key to check for directory
; If you install again, it will overwrite the old one automatically
InstallDirRegKey HKLM "SOFTWARE\GPL\WinOrsa" "Install_Dir"

; The text to prompt the user to enter a directory
;ComponentText "This will install ORSA software on your computer."\
;  " Select which optional things you want installed."

; The text to prompt the user to enter a directory
DirText "Choose a directory to install into:"

; The stuff to install
Section "WinOrsa"

  SetOutPath $INSTDIR

  File "winorsa.exe"
  File "c:\bc6\bin\cc3260mt.dll"
  File "c:\bc6\bin\stlpmt45.dll"
  File "c:\qt\3.3.3-bc6\bin\qtmt333.dll"

  WriteRegStr HKLM "Software\GPL\WinOrsa" "Install_Dir" "$INSTDIR"

  ; Write the uninstall keys for Windows
  WriteRegStr HKLM \
    "Software\Microsoft\Windows\CurrentVersion\Uninstall\GPL\WinOrsa" \
    "DisplayName" "WinOrsa"
  WriteRegStr HKLM \
    "Software\Microsoft\Windows\CurrentVersion\Uninstall\GPL\WinOrsa" \
    "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteUninstaller "uninstall.exe"

SectionEnd

; optional section

Section "Start Menu Shortcuts"
  ; we install for all users
  SetShellVarContext "all"
  CreateDirectory "$SMPROGRAMS\WinOrsa"
  CreateShortCut "$SMPROGRAMS\WinOrsa\Uninstall.lnk" "$INSTDIR\uninstall.exe" \
    "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\WinOrsa\WinOrsa.lnk" "$INSTDIR\winorsa.exe" "" \
    "$INSTDIR\winorsa.exe" 0
SectionEnd

Section "Desktop Shortcut"
  SetShellVarContext "all"
  CreateShortCut "$DESKTOP\WinOrsa.lnk" "$INSTDIR\winorsa.exe" "" \
    "$INSTDIR\winorsa.exe" 0
SectionEnd

; uninstall stuff

UninstallText "This will uninstall WinOrsa. Hit next to continue."

; special uninstall section.
Section "Uninstall"

  SetShellVarContext "all" 
 
  ; remove registry keys
  DeleteRegKey HKLM \
    "Software\Microsoft\Windows\CurrentVersion\Uninstall\GPL\WinOrsa"
  DeleteRegKey HKLM "Software\GPL\WinOrsa"

  ; remove files
  Delete $INSTDIR\winorsa.exe
  Delete $INSTDIR\qtmt33?.dll
  Delete $INSTDIR\cc32?0mt.dll
  Delete $INSTDIR\stlpmt45.dll

  ; MUST REMOVE UNINSTALLER, too
  Delete $INSTDIR\uninstall.exe

  ; remove shortcuts, if any.
  Delete "$SMPROGRAMS\WinOrsa\*.*"
  Delete "$DESKTOP\WinOrsa.lnk"

  ; remove directories used.
  RMDir "$SMPROGRAMS\WinOrsa"
  RMDir "$INSTDIR"

SectionEnd

; eof
